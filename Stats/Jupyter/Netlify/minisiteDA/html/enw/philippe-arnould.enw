%0 Conference Paper
%F Oral 
%T Mobile Robot Navigation in Indoor Environnements by using the Odometer and Ultrasonic data
%+ Laboratoire d'automatique humaine et sciences comportementales (LASC)
%A Masunga, Nsingi
%A Arnould, Philippe
%< avec comité de lecture
%B the 3rd World Multiconference on: Circuits, Systems, Communications and Computers (IEEE/WSES/IMACS)
%C Athens, Greece
%8 1999-07-04
%D 1999
%Z Computer Science [cs]/Robotics [cs.RO]Conference papers
%X In this work we examine the strategy and the control architecture to allow an autonomous mobile robot to navigate in indoor environment. One fundamental property a really useful autonomous mobile robot equipped with is the capability to effectively avoid collisions in realtime. The robot's control s system includes several processes which run in parallel by using a specialized hardware. The navigation subsystem of the mobile robot integrates the position estimation obtained by ultrasonic system with the position the estimated by odometry, using the extended Kalman filter. Obstacle detection is performed by means of set of ultrasonic sensors. The results presented whether experimented or simulation, show that our method is well adapted to this type of problem.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01856642/document
%2 https://hal.archives-ouvertes.fr/hal-01856642/file/282.pdf
%L hal-01856642
%U https://hal.archives-ouvertes.fr/hal-01856642
%~ UNIV-LORRAINE
%~ UNIV-METZ

%0 Conference Proceedings
%T HSSN: an ontology for hybrid semantic sensor networks
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%A Mansour, Elio
%A Chbeir, Richard
%A Arnould, Philippe
%< avec comité de lecture
%( the 23rd International Database Applications & Engineering Symposium
%B the 23rd International Database Applications & Engineering Symposium
%C Athènes, Greece
%I ACM Press
%P 8
%8 2019-06-10
%D 2019
%R 10.1145/3331076.3331102
%K Sensor Mobility
%K Semantic Sensor Networks
%K Ontology
%Z Computer Science [cs]Conference papers
%X Semantic web techniques (e.g., ontologies) have been recently adopted for sensor network modeling. However, existing works do not fully address these challenges: (i) representing different sensor types (e.g., mobile/static sensors) to enrich the network with different data and ensure better coverage; (ii) representing a variety of platforms (e.g., environments, devices) for sensor deployment, thus, integrating new components (e.g., mobile phones); (iii) representing the diverse data (scalar/multimedia) needed for various applications (e.g., event detection); and (iv) proposing a generic model to allow re-usability in various application domains. In this paper, we propose HSSN, an ontology that extends the Semantic Sensor Network (SSN) ontology which is already re-usable and considers various platforms. We extend the representation of sensors, sensed data, and deployment environments to cope with these challenges. We evaluate the consistency, accuracy, clarity, and performance of HSSN.
%G English
%2 https://hal.archives-ouvertes.fr/hal-02295950v2/document
%2 https://hal.archives-ouvertes.fr/hal-02295950v2/file/HSSNElioMansour.pdf
%L hal-02295950
%U https://hal.archives-ouvertes.fr/hal-02295950
%~ UNIV-PAU
%~ LIUPPA
%~ TESTUPPA2
%~ UPPA-OA

%0 Conference Proceedings
%T Couleur et ROI: Deux Options de JPEG2000. Investigations et Simulateur MATLAB
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%A Luthon, Franck
%A Arnould, Philippe
%A Baillot, C.
%A Navarro, X.
%A Coutellier, J.M.
%< avec comité de lecture
%B 8èmes Journées Compression et Représentation Des Signaux Audiovisuels (CORESA'03)
%C Lyon, France
%P 219-222
%8 2003
%D 2003
%Z Computer Science [cs]
%Z Computer Science [cs]/Signal and Image Processing
%Z Computer Science [cs]/Computer Vision and Pattern Recognition [cs.CV]Conference papers
%X Pour une application de télésurveillance reposant sur la transmissionà très bas débit d'images acquises par une caméra fixe, nous présentons deux innovations concernant deux fonctionnalités de JPEG2000 : le choix d'une trans-formée couleur non-linéaire pour améliorer le rendu cou-leurà très fort taux de compression, et l'extraction auto-matique de la ROI par détection de mouvement. Mots clefs compression, transformée couleur, région d'intérêt, détection de mouvement, JasPer, Matlab.
%G French
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-01912838/document
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-01912838/file/coresa03.pdf
%L hal-01912838
%U https://hal-univ-pau.archives-ouvertes.fr/hal-01912838
%~ UNIV-PAU
%~ EVS_UMR5600
%~ LIUPPA
%~ TESTUPPA
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T In-situ measurements of wave impact pressure on a composite breakwater: preliminary results
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%A Larroque, Benoit
%A Arnould, Philippe
%A Luthon, Franck
%A PONCET, Pierre-Antoine
%A Rahali, A
%A Abadie, Stéphane
%< avec comité de lecture
%@ 0749-0208
%J Journal of Coastal Research
%I Coastal Education and Research Foundation
%V 85
%P 1 - 5
%8 2018
%D 2018
%R 10.2112/SI85-001.1
%K statistical analysis
%K coastal structure
%K supervision
%K embedded sensor
%K environmental variables
%K field data
%K flip-through impact
%Z Engineering Sciences [physics]/Signal and Image processing
%Z Engineering Sciences [physics]/Electronics
%Z Physics [physics]/Physics [physics]/Instrumentation and Detectors [physics.ins-det]Journal articles
%X A detached horizontally composite breakwater protecting the Saint Jean de Luz bay (French Basque Coast, Bay of Biscay, France) has been equipped with two pressure sensors displayed vertically onto the vertical wall. The two sensors recorded wave impact pressure in ten minute burst each hour at 10 kHz. In parallel, incoming waves were also recorded about 1 km off the structure with a directional wave buoy giving access to spectral wave parameters as well as raw data. Finally water level, wind magnitude and direction were also acquired. The whole dataset covers winter 2015-2016 from January to March. The results show first the statistical distribution of the measured parameters confirming preceding similar studies in which significantly lower impact pressure values were obtained compared to physical experiments or numerical simulations. A linear multivariate model has then been adjusted showing the overwhelming influence of wave heights to explain maximum pressure variability, followed by water level. Nevertheless this result has still to be confirmed for data reduced to impulsive impacts. Finally, interesting events supposedly corresponding to, or approaching flip-through impact type, have been identified and need further investigations.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01809649/document
%2 https://hal.archives-ouvertes.fr/hal-01809649/file/ICS2018_HAL.pdf
%L hal-01809649
%U https://hal.archives-ouvertes.fr/hal-01809649
%~ UNIV-PAU
%~ LIUPPA
%~ SIAME
%~ TESTUPPA
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Context-aware System for Dynamic Privacy Risk Inference
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%+ Service Oriented Computing (SOC)
%A Bou Chaaya, Karam
%A Barhamgi, Mahmoud
%A Chbeir, Richard
%A Arnould, Philippe
%A Benslimane, Djamal
%< avec comité de lecture
%@ 0167-739X
%J Future Generation Computer Systems
%I Elsevier
%V 101
%P 1096-1111
%8 2019-07
%D 2019
%R 10.1016/j.future.2019.07.011
%K Privacy Engineering
%K Privacy Risk
%K Context-aware computing
%K Semantic reasoning
%K Ontology
%K Internet of Things
%Z Computer Science [cs]/Cryptography and Security [cs.CR]Journal articles
%X With the rapid expansion of smart cyber–physical systems and environments, users become more and more concerned about their privacy, and ask for more involvement in the protection of their data. However, users may not be necessarily aware of the direct and indirect privacy risks they take to properly protect their privacy. In this paper, we propose a context-aware semantic reasoning system, denoted as the Privacy Oracle, capable of providing users with a dynamic overview of the privacy risks taken as their context evolves. To do so, the system continuously models, according to a proposed Semantic User Environment Modeling (SUEM) ontology, the knowledge (received by the system) about the user of interest and his surrounding cyber–physical environment. In parallel, it performs continuous reasoning over modeled information, by relying on set of privacy rules, in order to dynamically infer the privacy risks taken by the user. To validate our approach, we developed a prototype based on the semantic web tools such as OWL API, SWRL API and the inference engine Pellet. We evaluated the system performance by considering multiple use cases. Our experimental results show that the Privacy Oracle can assist users by dynamically detecting their incurred privacy risks, and by tracking, in real-time, the evolution of those risks as user context changes.
%G English
%2 https://hal.archives-ouvertes.fr/hal-02236774/document
%2 https://hal.archives-ouvertes.fr/hal-02236774/file/FGCS_paper%20%2843%29.pdf
%L hal-02236774
%U https://hal.archives-ouvertes.fr/hal-02236774
%~ UNIV-LYON1
%~ INSA-GROUPE
%~ UNIV-LYON2
%~ LYON2
%~ LIUPPA
%~ LIRIS
%~ INSA-LYON
%~ CNRS
%~ EC-LYON
%~ UNIV-PAU
%~ UDL
%~ UNIV-LYON
%~ UNIV-BOURGOGNE
%~ LE2I
%~ TESTUPPA2

%0 Conference Proceedings
%T EQL-CE: An Event Query Language for Connected Environments
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%A Mansour, Elio
%A Chbeir, Richard
%A Arnould, Philippe
%< avec comité de lecture
%( ICPS Proceedings
%B 23rd International Database Applications & Engineering Symposium (IDEAS 2019)
%C Athènes, Greece
%I ACM Press
%3 IDEAS '19: Proceedings of the 23rd International Database Applications & Engineering Symposium
%P 7
%8 2019-06-10
%D 2019
%R 10.1145/3331076.3331103
%K Internet of Things
%K Event Query Language
%K Sensor Networks
%Z Computer Science [cs]/Information Retrieval [cs.IR]Conference papers
%X Recent advances in sensor technology and information processing have allowed connected environments to impact various application domains. In order to detect events in these environments, existing works rely on the sensed data. However, these works are not re-usable since they statically define the targeted events (i.e., the definitions are hard to modify when needed). Here, we present a generic framework for event detection composed of (i) a representation of the environment; (ii) an event detection mechanism; and (iii) an Event Query Language (EQL) for user/framework interaction. This paper focuses on detailing the EQL which allows the definition of the data model components, handles instances of each component, protects the security/privacy of data/users, and defines/detects events. We also propose a query optimizer in order to handle the dynamicity of the environment and spatial/temporal constraints. We finally illustrate the EQL and conclude the paper with some future works.
%G English
%2 https://hal.archives-ouvertes.fr/hal-02390620/document
%2 https://hal.archives-ouvertes.fr/hal-02390620/file/EQLCE.pdf
%L hal-02390620
%U https://hal.archives-ouvertes.fr/hal-02390620
%~ LIUPPA
%~ UNIV-PAU
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T eVM: An Event Virtual Machine Framework
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%A Mansour, Elio
%A Chbeir, Richard
%A Arnould, Philippe
%< avec comité de lecture
%@ 1869-1994
%J Transactions on Large-Scale Data- and Knowledge-Centered Systems
%I Springer Berlin / Heidelberg
%V 11310
%P 130-168
%8 2018-11-23
%D 2018
%K Formal concept analysis
%K Event Detection
%K Semantic Clustering
%K Formal Concept Anal- ysis
%Z Computer Science [cs]/Artificial Intelligence [cs.AI]
%Z Computer Science [cs]/Computation and Language [cs.CL]
%Z Computer Science [cs]/Information Retrieval [cs.IR]Journal articles
%X Information and communication technology (ICT) is impacting our daily lives more than ever before. Many existing applications guide users in their daily activities (e.g., navigation through traffic, health monitoring, managing home comfort, socializing with others). Although these applications are different in terms of purpose and application domain, they all detect events and propose actions and decision making aid to users. However, there is no usage of a common backbone for event detection that can be instantiated, re-used, and reconfigured in different use cases. In this paper, we propose eVM, a generic event Virtual Machine able to detect events in different contexts while allowing domain experts to model and define the targeted events prior to detection. eVM simultaneously considers the various features of the defined events (e.g., temporal, geographical), and uses the latter to detect different feature-centric events (e.g., time-centric, location-centric). eVM is based on different components (an event query language, a query compiler, an event detection core, etc.), but mainly the event detection modules are detailed here. We show that eVM is re-usable in different contexts and that the performance of our prototype is quasi-linear in most cases. Our experimental results showed that the detection accuracy is improved when, besides spatio-temporal information, other features are considered.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01940572/document
%2 https://hal.archives-ouvertes.fr/hal-01940572/file/eVM__an_event_Virtual_Machine_Framework%20%281%29.pdf
%L hal-01940572
%U https://hal.archives-ouvertes.fr/hal-01940572
%~ LIUPPA
%~ UNIV-PAU
%~ TESTUPPA2

%0 Conference Proceedings
%T F-SED: Feature-Centric Social Event Detection
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%+ Laboratoire Telecom Claude Chappe (LT2C)
%+ Universidad Simon Bolivar (USB)
%A Mansour, Elio
%A Tekli, Gilbert
%A Arnould, Philippe
%A Chbeir, Richard
%A Cardinale, Yudith
%< avec comité de lecture
%B 28th International Conference on Database and Expert Systems Applications - DEXA 2017
%C Lyon, France
%3 Database and Expert Systems Applications 28th International Conference, DEXA 2017, Lyon, France, August 28-31, 2017, Proceedings,
%8 2017-08-28
%D 2017
%R 10.1007/978-3-319-64471-4_33
%K Social Event Detection
%K Social Networks
%K Semantic Cluster- ing
%K Multimedia Sharing
%K Formal Concept Analysis
%Z Computer Science [cs]/Social and Information Networks [cs.SI]
%Z Computer Science [cs]/Artificial Intelligence [cs.AI]Conference papers
%X In the context of social media, existent works offer social-event-based organization of multimedia objects (e.g., photos, videos) by mainly considering spatio-temporal data, while neglecting other user-related information (e.g., people, user interests). In this paper we propose an automated, extensible, and incremental Feature-centric Social Event Detection (F-SED) approach, based on Formal Concept Analysis (FCA), to organize shared multimedia objects on social media platforms and sharing applications. F-SED simultaneously considers various event features (e.g., temporal, geographical, social (user related)), and uses the latter to detect different feature-centric events (e.g., user-centric, location-centric). Our experimental results show that detection accuracy is improved when, besides spatio-temporal information, other features, such as social, are considered. We also show that the performance of our prototype is quasi-linear in most cases.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01905727/document
%2 https://hal.archives-ouvertes.fr/hal-01905727/file/f-sed-feature%20%288%29.pdf
%L hal-01905727
%U https://hal.archives-ouvertes.fr/hal-01905727
%~ LIUPPA
%~ UNIV-PAU
%~ UNIV-ST-ETIENNE
%~ UDL
%~ UNIV-LYON
%~ TESTUPPA2
%~ UPPA-OA

%0 Conference Paper
%F Oral 
%T EQL-CE: An Event Query Language for Connected Environment Management
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%A Mansour, Elio
%A Chbeir, Richard
%A Arnould, Philippe
%< avec comité de lecture
%B 15th ACM International Symposium on QoS and Security for Wireless and Mobile Networks
%C Miami Beach, United States
%I ACM Press
%P 43-51
%8 2019-11-25
%D 2019
%R 10.1145/3345837.3355950
%K CCS CONCEPTS • General and reference → General conference proceedings
%K • Information systems → Query representation
%K • Theory of computation → Grammars and context-free languages
%K • Computer systems organization → Sensor networks
%Z Computer Science [cs]Conference papers
%X Recent technological advances have fueled the rise of connected environments (e.g., smart buildings and cities). Event Query Languages (EQL) have been used to define (and later detect) events in these environments. However, existing languages are limited to the definition of event patterns. They share the following limitations: (i) lack of consideration of the environment, sensor network, and application domain in their queries; (ii) lack of provided query types for the definition/handling of components/component instances; (iii) lack of considered data and datatypes (e.g., scalar, multimedia) needed for the definition of specific events; and (iv) difficulty in coping with the dynamicity of the environments. To address the aforementioned limitations, we propose here an EQL specifically designed for connected environments, denoted EQL-CE. We describe its framework, detail the used language, syntax, and queries. Finally, we illustrate the usage of EQL-CE in a smart mall example.
%G English
%2 https://hal.archives-ouvertes.fr/hal-02390650/document
%2 https://hal.archives-ouvertes.fr/hal-02390650/file/EQL_CE-8.pdf
%L hal-02390650
%U https://hal.archives-ouvertes.fr/hal-02390650
%~ UNIV-PAU
%~ LIUPPA
%~ TESTUPPA
%~ TESTUPPA2

