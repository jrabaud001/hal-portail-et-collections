%0 Journal Article
%T Assessment of background concentrations of organometallic compounds (methylmercury, ethyllead and butyl- and phenyltin) in French aquatic environments
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Universidad de Navarra [Pamplona] (UNAV)
%+ Institut National de l'Environnement Industriel et des Risques (INERIS)
%A Cavalheiro, Joana
%A Sola, C.
%A Baldanza, J.
%A Tessier, Emmanuel
%A Lestremau, F.
%A Botta, F.
%A Preud'Homme, H.
%A Monperrus, Mathilde
%A Amouroux, David
%< avec comité de lecture
%Z EN-2016-028
%@ 0043-1354
%J Water Research
%I IWA Publishing
%V 94
%P 32-41
%8 2016
%D 2016
%R 10.1016/j.watres.2016.02.010
%K Speciation analysis
%K Organometallic compounds
%K aquatic environment
%K Background concentration
%K Screening study
%Z Chemical Sciences/Other
%Z Environmental Sciences/Biodiversity and EcologyJournal articles
%X The aim of this work is to estimate background concentrations of organometallic compounds, such as tributyltin (TBT), dibutyltin (DBT), monobutyltin (MBT), triphenyltin (TPhT), diphenyltin (DPhT), monophenyltin (MPhT), methylmercury (MeHg), inorganic mercury (iHg) and diethyllead (Et2Pb) in the aquatic environment at the French national scale.Both water and sediment samples were collected all over the country, resulting in 152 water samples and 123 sediment samples collected at 181 sampling points. Three types of surface water bodies were investigated: rivers (140 sites), lakes (19 sites) and coastal water (42 sites), spread along the 11 French river basins. The choice of sites was made on the basis of previous investigation results and the following target criteria: reference, urban sites, agricultural and industrial areas. The analytical method was properly validated for both matrices prior to analysis, resulting in low limits of quantification (LOQ), good precision and linearity in agreement with the Water Framework Directive demands. The results were first evaluated as a function of their river basins, type of surrounding pressure and water bodies. Later, background concentrations at the French national scale were established for both water and sediment matrices, as well as their threshold, i.e., the concentration that distinguishes background from anomalies or contaminations. Background concentrations in water are ranging between &lt;0.04-0.14 ng Hg. L-1 for MeHg, &lt;0.14-2.10 ng Hg. L-1 for iHg, &lt;1.0-8.43 ng Pb. L-1 for Et2Pb and 0.49-151 ng Sn. L-1, &lt;0.08-3.04 ng Sn. L-1 and &lt;0.08-0.25 ng Sn. L-1 for MBT, DBT and TBT, respectively. For sediments, background concentrations were set as &lt;0.09-1.11 ng Hg. g-1 for MeHg, &lt;0.06-24.3 ng Pb. g-1 for Et2Pb and &lt;1.4-13.4 ng Sn. g-1, &lt;0.82-8.54 ng Sn. g-1, &lt;0.25-1.16 ng Sn. g-1 and &lt;0.08-0.61 ng Sn. g-1 for MBT, DBT, TBT and DPhT, respectively. TBT occurs in higher concentrations than the available environmental protection values in 24 and 38 sampling sites for both water and sediment samples, respectively. Other phenyltins (MPhT and TPhT) did not occur above their LOQ and therefore no background was possible to establish. Throughout this work, which is the first assessment of background concentrations for organometallic compounds at the French national level ever being published, it was possible to conclude that over the last 10-20 years organotin concentrations in French river basins have decreased while MeHg concentration remained stable.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01498754/document
%2 https://hal.archives-ouvertes.fr/hal-01498754/file/2016-028_post-print.pdf
%L hal-01498754
%U https://hal.archives-ouvertes.fr/hal-01498754
%~ INC-CNRS
%~ INERIS
%~ CNRS
%~ UNIV-PAU
%~ UNIV-PSUD
%~ UNIV-PSUD-SACLAY
%~ IPREM
%~ UNIV-PARIS-SACLAY
%~ I2BC
%~ SDE
%~ GIP-BE
%~ IPREM-CME
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T A chemical speciation insight into the palladium(ii) uptake and metabolism by Sinapis alba. Exposure to Pd induces the synthesis of a Pd–histidine complex
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ University of Warsaw (UW)
%A Kińska, Katarzyna
%A Bierla, Katarzyna
%A Krasnodębska-Ostręga, Beata
%A Godin, Simon
%A Kowalska, Joanna
%A Preud'Homme, Hugues
%A Lobinski, Ryszard
%A Szpunar, Joanna
%< avec comité de lecture
%@ 1756-5901
%J Metallomics
%I Royal Society of Chemistry
%V 11
%N 9
%P 1498 - 1505
%8 2019
%D 2019
%R 10.1039/c9mt00126c
%Z Life Sciences [q-bio]/Vegetal Biology/Botanics
%Z Life Sciences [q-bio]/Toxicology/EcotoxicologyJournal articles
%X Palladium is recognized as a technologically critical element (TCE) because of its massive use in automobile exhaust gas catalytic converters. The release of Pd into the environment in the form of nanoparticles of various size and chemical composition requires an understanding of their metabolism by leaving organisms. We provide here for the first time a chemical speciation insight into the identity of the ligands produced or used by a plant Sinapis alba L. exposed in hydropony to Pd nanoparticles and soluble Pd (nitrate). The analytical method developed was based on the concept of 2D HPLC with parallel inductively coupled plasma mass spectrometry (ICP MS) and electrospray MS detection. Size exclusion chromatography-ICP MS of the plant extracts showed no difference between the speciation of Pd after the exposure to nanoparticles and after that to Pd 2+ which indicated the reactivity and dissolution of Pd nanoparticles. A comparative investigation of the Pd speciation in a control plant extract spiked with Pd 2+ and of an extract of a plant having metabolized palladium indicated the response of the Sinapis alba by the formation of a Pd-histidine complex. The complex was identified via Orbitrap MS; the HPLC-MS chromatogram produced two peaks at m/z 415.0341 each corresponding to a Pd-His 2 complex. An investigation by ion-mobility MS revealed a difference in their collision cross section indicating that the complexes present varied in terms of spatial conformation. A number of other Pd complexes with different ligands (including nicotianamine) circulating in the plant were detected but these ligands were already observed in a control plant and their concentrations were not affected by the exposure to Pd.
%G English
%2 https://hal.archives-ouvertes.fr/hal-03133522/document
%2 https://hal.archives-ouvertes.fr/hal-03133522/file/Metallomics%20Pd%20-%20submitted.pdf
%L hal-03133522
%U https://hal.archives-ouvertes.fr/hal-03133522
%~ UNIV-PAU
%~ IPREM
%~ IPREM-CAPT

%0 Journal Article
%T A chemical speciation insight into the palladium( ii ) uptake and metabolism by Sinapis alba . Exposure to Pd induces the synthesis of a Pd–histidine complex
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Kińska, Katarzyna
%A Bierla, Katarzyna
%A Godin, Simon
%A Preud'Homme, Hugues
%A Kowalska, Joanna
%A Krasnodębska-Ostręga, Beata
%A Lobinski, Ryszard
%A Szpunar, Joanna
%Z Poland, Grant No. 2014/15/N/NZ8/00326
%< avec comité de lecture
%@ 1756-5901
%J Metallomics
%I Royal Society of Chemistry
%8 2019
%D 2019
%R 10.1039/C9MT00126C
%Z Chemical Sciences/Analytical chemistry
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Material chemistryJournal articles
%X Palladium is recognized as a technologically critical element (TCE) because of its massive use in automobile exhaust gas catalytic converters. The release of Pd into the environment in the form of nanoparticles of various size and chemical composition requires an understanding of their metabolism by leaving organisms. We provide here for the first time a chemical speciation insight into the identity of the ligands produced or used by a plant Sinapis alba L. exposed in hydropony to Pd nanoparticles and soluble Pd (nitrate). The analytical method developed was based on the concept of 2D HPLC with parallel inductively coupled plasma mass spectrometry (ICP MS) and electrospray MS detection. Size exclusion chromatography – ICP MS of the plant extracts showed no difference between the speciation of Pd after the exposure to nanoparticles and after that to Pd2+ which indicated the reactivity and dissolution of Pd nanoparticles. A comparative investigation of the Pd speciation in a control plant extract spiked with Pd2+ and of an extract of a plant having metabolized palladium indicated the response of the Sinapis alba by the formation of a Pd–histidine complex. The complex was identified via Orbitrap MS; the HPLC-MS chromatogram produced two peaks at m/z 415.0341 each corresponding to a Pd–His2 complex. An investigation by ion-mobility MS revealed a difference in their collision cross section indicating that the complexes present varied in terms of spatial conformation. A number of other Pd complexes with different ligands (including nicotianamine) circulating in the plant were detected but these ligands were already observed in a control plant and their concentrations were not affected by the exposure to Pd.
%G English
%L hal-02282683
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02282683
%~ UNIV-PAU
%~ IPREM
%~ IPREM-CAPT
%~ CNRS
%~ INC-CNRS
%~ ANR

