%0 Journal Article
%T Theoretical and Experimental Studies of Off-the-Shelf V-Dot Probes
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%+ Université de Pau et des Pays de l'Adour (UPPA)
%A Novac, B.M.
%A Xiao, R.
%A Huiskamp, T.
%A PECASTAING, Laurent
%A Wang, M.
%A Senior, P.
%A de Ferron, Antoine Silvestre
%A Pemen, A.J.M.
%A Rivaletto, Marc
%< avec comité de lecture
%@ 0093-3813
%J IEEE Transactions on Plasma Science
%I Institute of Electrical and Electronics Engineers
%V 46
%N 8
%P 2985-2992
%8 2018
%D 2018
%R 10.1109/TPS.2018.2854971
%K Pulse forming lines (PFLs)
%K pulsed-power systems
%K voltage probes
%Z Physics [physics]Journal articles
%X This paper introduces the work undertaken to reliably use off-the-shelf differentiating voltage probes attached to coaxial transmission lines. The results obtained prove that indeed such probes are a valid and simple instrument for measuring nanosecond and subnanosecond voltage impulses. As a bonus, this paper also highlighted an important challenging phenomenon that appears whenever an attempt is made to measure fast voltage impulses with a differentiating probe positioned too close to the closing switch of a pulse forming line generator. \textcopyright 1973-2012 IEEE.
%G English
%L hal-02146092
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02146092
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Generation of Intense PEFs Using a Prolate Spheroidal Reflector Attached to the Bipolar Former of a 10-GW Pulsed Power Generator
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%A Novac, B.M.
%A Xiao, R.
%A Senior, P.
%A PECASTAING, Laurent
%A Smith, I.R.
%< avec comité de lecture
%@ 0093-3813
%J IEEE Transactions on Plasma Science
%I Institute of Electrical and Electronics Engineers
%V 46
%N 10
%P 3547-3551
%8 2018
%D 2018
%R 10.1109/TPS.2018.2844160
%K Antenna
%K pulse compression circuits
%K pulse modulation
%K pulsed electric fields (PEFs)
%K pulsed power systems
%Z Physics [physics]Journal articles
%X A prolate spheroidal reflector was designed, manufactured, and attached to a bipolar former at the output of a 10-GW-Tesla-driven Blumlein pulsed power generator. The reflector, operated in water, is capable of producing intense pulsed electric fields of the order of 50 kV/cm. Constructional details are provided, together with experimental results, and a detailed analysis using 3-D software modeling of the reflector that provides results in good agreement with experimental data. \textcopyright 1973-2012 IEEE.
%G English
%L hal-02146093
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02146093
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T A Subnanosecond Pulsed Electric Field System for Studying Cells Electropermeabilization
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%+ Aspects métaboliques et systémiques de l'oncogénèse pour de nouvelles approches thérapeutiques (METSY)
%+ Institut Gustave Roussy (IGR)
%A Ibrahimi, Njomza
%A Vallet, Leslie
%A André, Franck, M.
%A Ariztia, Laurent
%A Rivaletto, Marc
%A de Ferron, Antoine Silvestre
%A Novac, Bucur
%A Mir, Lluis
%A PECASTAING, Laurent
%< avec comité de lecture
%@ 0093-3813
%J IEEE Transactions on Plasma Science
%I Institute of Electrical and Electronics Engineers
%V 48
%N 12
%P 4242-4249
%8 2020
%D 2020
%R 10.1109/TPS.2020.3034286
%Z Engineering Sciences [physics]/Electric power
%Z Engineering Sciences [physics]/Electronics
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%G English
%L hal-03025849
%U https://hal-univ-pau.archives-ouvertes.fr/hal-03025849
%~ UNIV-PAU
%~ SIAME
%~ FNCLCC
%~ CNRS
%~ METSY
%~ UNIVERSITE-PARIS-SACLAY
%~ UNIV-PARIS-SACLAY
%~ IGR
%~ TESTUPPA2
%~ UPPA-OA
%~ GS-BIOSPHERA

%0 Journal Article
%T Temperature Dependence of Kerr Constant for Water at 658 Nm and for Pulsed Intense Electric Fields
%+ Department of Physics [Berkeley]
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%A Wang, Meng
%A de Ferron, Antoine Silvestre
%A PECASTAING, Laurent
%A Smith, Ivor R.
%A Yin, Jiahui
%A Novac, Bucur M.
%A Ruscassie, R.
%< avec comité de lecture
%@ 0093-3813
%J IEEE Transactions on Plasma Science
%I Institute of Electrical and Electronics Engineers
%V 44
%N 6
%P 963-967
%8 2016
%D 2016
%R 10.1109/TPS.2016.2558470
%Z Physics [physics]Journal articles
%X The temperature dependence of the Kerr constant for water has been determined over the range 19 °C-45 °C at a wavelength of 658 nm. This paper presents the experimental arrangement used for this purpose and the data obtained, for which a polynomial fit is provided. A formula is also suggested to help estimate the variation of the Kerr constant for water with both temperature and wavelength. \textcopyright 2016 IEEE.
%G English
%L hal-02153683
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02153683
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Demonstration of a Novel Pulsed Electric Field Technique Generating Neither Conduction Currents nor Joule Effects
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%+ Université de Pau et des Pays de l'Adour (UPPA)
%A Novac, B.M.
%A Banakhr, F.A.
%A Smith, I.R.
%A PECASTAING, Laurent
%A Ruscassie, R.
%A de Ferron, Antoine Silvestre
%A Pignolet, P.
%< avec comité de lecture
%@ 0093-3813
%J IEEE Transactions on Plasma Science
%I Institute of Electrical and Electronics Engineers
%V 42
%N 1
%P 216-228
%8 2014
%D 2014
%R 10.1109/TPS.2013.2293915
%K pulsed electric field (PEF) sterilization
%K Electro-optic devices
%K microbial inactivation
%K Kerr effect
%Z Physics [physics]Journal articles
%X This paper is devoted to a detailed presentation of all aspects involved in a demonstration of a novel pulsed electric field (PEF) technique that does not generate neither conduction currents nor Joule effects. Details are given of both the experimental arrangement and the electro-optic Kerr-effect-based diagnostic used in the measurement of the intense PEFs in water. The results show unequivocally that the novel technique is effective in significantly reducing the initial concentration of Escherichia coli bacteria. Finally, a brief comment on the way ahead is provided. \textcopyright 2013 IEEE.
%G English
%L hal-02153369
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02153369
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Bipolar Modulation of the Output of a 10-GW Pulsed Power Generator
%+ Department of Physics [Berkeley]
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%A Wang, Meng
%A Novac, Bucur M.
%A PECASTAING, Laurent
%A Smith, Ivor R.
%< avec comité de lecture
%@ 0093-3813
%J IEEE Transactions on Plasma Science
%I Institute of Electrical and Electronics Engineers
%V 44
%N 10
%P 1971-1977
%8 2016
%D 2016
%R 10.1109/TPS.2016.2569461
%Z Physics [physics]Journal articles
%X A bipolar Blumlein former has been designed and successfully implemented as an extension to an existing 10-GW-Tesla-driven Blumlein pulsed power generator. The new system is capable of generating a voltage impulse with a peak-to-peak value reaching 650 kV and having a high-frequency limit of the bandwidth well in excess of 1 GHz. Constructional details are provided, together with experimental results and analysis using the 3-D software modeling of the bipolar former that provides the results in good agreement with experimental data. \textcopyright 2016 IEEE.
%G English
%L hal-02153371
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02153371
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Design and Development of High Efficiency 150 kW Very Compact PLA Core Electromagnetic Coupler for Highly Resonant Power Transfer Technology
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%A Zahra Boudara, Fatima
%A Rivaletto, Marc
%A PECASTAING, Laurent
%A de Ferron, Antoine Silvestre
%A Paquet, Sylvain
%A Brasile, Jean-Pierre
%< avec comité de lecture
%@ 1942-0749
%J Journal of Electromagnetic Analysis and Applications
%I Scientific Research Publishing
%8 2020
%D 2020
%R 10.4236/jemaa.2020.129011
%K *-*
%K PLA Core
%K Toroidal Coupler
%K Resonant System
%K Low Stray Emission
%K tions
%K 12
%Z Engineering Sciences [physics]/Electric power
%Z Engineering Sciences [physics]/Electronics
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%X Highly Resonant Power Transfer (HRPT) technology is currently receiving very significant attention from the industry and the smart power grid distribution community in particular. This technology ensures electrical power transmission between two points while controlling the level of transmitted power and ensures the immediate shutdown of the transmitted power in the event of a problem. This paper reviews the inductive power transfer method and describes the design of an ultra-compact PLA core electromagnetic coupler. The proposed architecture confines the magnetic field in a toroidal PLA core transformer, and by avoiding the use of heavy and bulky shielding plates, reduces magnetic losses and avoids the Curie point. As a result, the overall unit has a weight of 5 kg and a volume of only 0.013 m 3. The electromagnetic coupler is capable of transferring a peak power of 150 kW with an operating frequency of 193 kHz, giving a satisfactory efficiency of 95%. The proposed novel system was first investigated through CST 3D numerical modelling to determine the electrical parameters of the coupler's equivalent circuit and its efficiency, to verify its compatibility with the ICNIRP 2010 standard and to evaluate its temperature rise with an air-cooling system. Afterwards, the designed coupler was built with a 3D printing device and finally tested experimentally. Simulation and experimental results are compared and show a good agreement.
%G English
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-03025797/document
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-03025797/file/-9801879-9.30.pdf
%L hal-03025797
%U https://hal-univ-pau.archives-ouvertes.fr/hal-03025797
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Transportable High-Energy High-Current Inductive Storage GW Generator
%+ Loughborough University 
%+ Ethox Centre, Department of Public Health and Primary Health Care
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%A Novac, Bucur, M
%A Smith, Ivor
%A Senior, Peter
%A Parker, Michael
%A Louverdis, Gerry
%A PECASTAING, Laurent
%A de Ferron, Antoine Silvestre
%A Pignolet, Pascal
%A SOUAKRI, SONIA
%< avec comité de lecture
%@ 0093-3813
%J IEEE Transactions on Plasma Science
%I Institute of Electrical and Electronics Engineers
%V 42
%N 10
%P 2919-2933
%8 2014-10
%D 2014
%R 10.1109/TPS.2014.2340014
%Z Engineering Sciences [physics]/Electric power
%Z Engineering Sciences [physics]/Electronics
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%G English
%L hal-02374481
%U https://hal.archives-ouvertes.fr/hal-02374481
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Thesis
%T Study and construction of modulators with resonant or impulse transformers associated with a system of high current triggered spark gaps
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%A PECASTAING, Laurent
%I Université de Pau et des Pays de l'Adour - Laboratoire SIAME
%Y Pecastaing Laurent
%8 2019-07-18
%D 2019
%K pulsed power
%K puissances pulsées
%K Transformateurs électriques
%K éclateurs forts courants
%K synchronisation d'éclateurs
%Z Engineering Sciences [physics]/Electromagnetism
%Z Engineering Sciences [physics]/Plasmas
%Z Engineering Sciences [physics]/ElectronicsTheses
%X De nos jours, pour accroître le potentiel applicatif des machines de hautes puissances pulsées, il est nécessaire de développer des modulateurs compacts capables de délivrer des impulsions de l’ordre de plusieurs Mégawatts de durée pouvant atteindre plusieurs centaines de microsecondes. Cette amélioration requiert le développement de structures innovantes dont le but est de produire aussi bien des puissances moyennes que des puissances crêtes importantes. Les modulateurs étudiés dans ce mémoire sont basés sur l’utilisation de divers transformateurs pour la génération d’impulsions de très forte puissance. Le projet AGIR (acronyme de « Architecture pour la Génération d’Impulsions Rectangulaires de forte de puissance ») est réalisé dans le cadre d’un RAPID (Régime d’Appui Pour l’Innovation Duale) financé par la Direction Générale de l’Armement (DGA). Le projet est une collaboration avec EFFITECH, une entreprise spécialisée dans les puissances pulsées. L’objectif est de développer deux générateurs pour deux gammes de puissance crête (jusqu’à 10MW pour l’un et 1GW pour l’autre). Le premier modulateur « AGIR1 » repose sur l’association d’un convertisseur AC-DC et de 12 convertisseurs résonants DC-DC qui permettent la génération de plusieurs types d’impulsions (fort courant ou forte tension) en fonction de la configuration choisie. Le second modulateur repose sur le développement d’un transformateur impulsionnel à quatre primaires synchronisés. Chaque primaire est relié à un système de mise en forme de type Blumlein dont le déclenchement est assuré par un éclateur pressurisé à trois électrodes. La synchronisation des quatre éclateurs est assurée par un générateur impulsionnel innovant à faible gigue. La principale difficulté du travail effectué au laboratoire réside dans l’étude des différents transformateurs haute-tension utilisés (résonant ou impulsionnel) et du système de synchronisation des éclateurs. Chaque élément constituant le système est étudié et simulé de manière électrostatique, électromagnétique ou électrique avant d’être réalisé et assemblé. Des essais ponctue l’étude afin de valider le fonctionnement en récurrent avec un système de dissipation thermique adapté.
%X Nowadays, to increase the application potential of high power pulsed machines, it is necessary to develop compact modulators able to deliver pulses in the range of several megawatts with duration of up to several hundred microseconds. This improvement requires the development of innovative structures whose purpose is to produce both average power and large peak power. Modulators studied in this thesis are based on the use of various transformers for the generation of very high power pulses. The AGIR project (French acronym for "Architecture for Rectangular High Pulse power generation") is achieved within the framework of a RAPID (Dual Innovation Support Regime) funded by the French Defense (DGA). The project is carried on by a collaboration with EFFITECH, a company specialized in pulsed powers. The goal is to develop two generators for two peak power ranges (up to 10MW for one and 1GW for the other). The first modulator "AGIR1" is based on the association of an AC-DC converter and 12 DC-DC resonant converters allowing the generation of several types of pulses (high current or high voltage) depending on the chosen configuration. The second modulator is based on the development of a four synchronized primary pulse transformer. Each primary is connected to a Blumlein pulse forming line triggered by a three-electrode pressurized spark gap. The synchronization of the four spark gaps is ensured by an innovative pulse generator with low jitter. The main difficulty of the work which was completed in the laboratory relies in the study of the different high-voltage transformers used (resonant or pulse) and the spark gap synchronization system. Each element constituting the system is studied and simulated electrostatically, electromagnetically or electrically before being realized and assembled. Trials punctuate the study to validate the recurrent operation with a suitable heat dissipation system.
%G English
%2 https://hal.archives-ouvertes.fr/tel-02370397/document
%2 https://hal.archives-ouvertes.fr/tel-02370397/file/manuscrit_allard_florian%20final.pdf
%L tel-02370397
%U https://hal.archives-ouvertes.fr/tel-02370397
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Thesis
%T A Tesla-Blumlein PFL-Bipolar Pulsed Power Generator
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%+ The Institute of Sustainable Development [Shandong]
%A PECASTAING, Laurent
%A Wang, Meng
%I Loughborough University
%Y Bucur Novac
%8 2016-03-01
%D 2016
%K Pulsed Power
%K Puissances Pulsées
%Z Engineering Sciences [physics]/Electromagnetism
%Z Engineering Sciences [physics]/Plasmas
%Z Engineering Sciences [physics]/ElectronicsTheses
%X A Tesla-Blumlein PFL-Bipolar pulsed power generator, has been successfully designed, manufactured and demonstrated. The compact Tesla transformer that it employs has successfully charged capacitive loads to peak voltages up to 0.6 MV with an overall energy efficiency in excess of 90%. The Tesla–driven Blumlein PFL generator is capable of producing a voltage impulse approaching 0.6 MV with a rise time close to 2 ns, generating a peak electrical power of up to 10 GW for 5 ns when connected to a 30 Ω resistive load. Potentially for medical application, a bipolar former has been designed and successfully implemented as an extension to the system and to enable the generation of a sinusoid-like voltage impulse with a peak-to-peak value reaching 650 kV and having a frequency bandwidth beyond 1 GHz.This thesis describes the application of various numerical techniques used to design a successful generator, such as filamentary modelling, electrostatic and transient (PSpice) circuit analysis, Computer Simulation Technology (CST) simulation. All the major parameters of both the Tesla transformer, the Blumlein pulse forming line and the bipolar former were determined, enabling accurate modelling of the overall unit to be performed. The wide bandwidth and ultrafast embedded sensors used to monitor the dynamic characteristics of the overall system are also presented. Experimental results obtained during this major experimental programme are compared with theoretical predictions and the way ahead towards connecting to an antenna for medical application is considered.
%X Un générateur de puissances pulsées basé sur une architecture Tesla-Blumlein PFL Bipolaire a été conçu, fabriqué et démontré avec succès à l'Université de Loughborough. Le transformateur compact Tesla qu'il utilise a réussi à charger avec succès des charges capacitives à des tensions de pointe allant jusqu'à 0,6 MV avec un rendement énergétique global supérieur à 90%. Le générateur Blumlein-PFL piloté par Tesla est capable de produire une impulsion de tension approchant 0,6 MV avec un temps de montée proche de 2 ns, générant une puissance électrique crête pouvant atteindre 10 GW pendant 5 ns lorsque il est connecté à une charge résistive 30 Ω. Potentiellement pour des applications médicales, une ligne de mise en forme bipolaire a été conçue et mise en œuvre avec succès comme extension du système et pour permettre la génération d'une impulsion de tension de type bipolaire avec une valeur crête à crête atteignant 650 kV et ayant une bande passante supérieure à 1 GHz.Cette thèse décrit l'application de diverses techniques numériques utilisées pour concevoir un tel générateur, telles que la modélisation filamentaire, l'analyse des circuits électrostatiques et transitoires (PSpice), la simulation électromagnétique (CST). Tous les paramètres majeurs du transformateur Tesla, de la ligne de formation d'impulsions Blumlein et de la ligne de mise en forme bipolaire ont été déterminés, permettant une modélisation précise de l'ensemble de l'unité à réaliser. La large bande passante et les capteurs embarqués ultra-rapides utilisés pour surveiller les caractéristiques dynamiques de l'ensemble du système sont également présentés. Les résultats expérimentaux obtenus au cours de ce programme expérimental sont comparés aux prévisions théoriques et la voie à suivre pour la connexion à une antenne à usage médical est envisagée.
%G English
%2 https://hal-univ-pau.archives-ouvertes.fr/tel-02372305/document
%2 https://hal-univ-pau.archives-ouvertes.fr/tel-02372305/file/Thesis_Final%20Version%20Meng%20Wang.pdf
%L tel-02372305
%U https://hal-univ-pau.archives-ouvertes.fr/tel-02372305
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Feed Signal Influence and Potential Performances of a Compact Radiation Source Based on a Helical Antenna
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%A PECASTAING, Laurent
%A Rivaletto, Marc
%A de Ferron, Antoine Silvestre
%A Pecquois, Romain
%A Vézinet, René
%A Diot, Jean-Christophe
%A Tortel, Stéphane
%< avec comité de lecture
%@ 1942-0749
%J Journal of Electromagnetic Analysis and Applications
%I Scientific Research Publishing
%V 07
%N 07
%P 199-208
%8 2015
%D 2015
%R 10.4236/jemaa.2015.77021
%K Pulse Transformer
%K Coaxial Damped Oscillator
%K Electric Field
%K Helical Antenna
%K High Voltage
%K Peaking Switch
%Z Physics [physics]Journal articles
%X In the class of emerging high power electromagnetic sources, a complete pulsed power source, named MOUNA (French acronym of ``Module Oscillant Utilisant une Nouvelle Architecture'') has been developed. This device must transmit waveforms with a wide frequency band and a high figure-of-merit. To improve the overall performance of the MOUNA system while maintaining its compact size, two approaches are being explored in the paper: the replacement of the dipole antenna by a helical antenna and its feeding signal influence. Helical antenna is cylindrical shape and relatively compact. It offers relatively good gain factor and directivity. The waveform delivered to the antenna is directly related to the amplitude of the radiated electric field. Therefore, different waveforms (step pulse, Gaussian pulse, bipolar pulse and damped sinusoid) are compared to point out the feed signal influence on the radiated electric field. Switch oscillators appear to be considered as interesting resonant sources for driving an antenna. The novel radiating source consists of a primary power source, a resonant transformer, a coaxial transmission line damped oscillator (also termed as coaxial resonator), and a helical antenna. This high voltage pulsed source is very compact (volume of only 2500 cc without the antenna). Our study aims at designing the antenna (number of turns, size\ldots) and a coaxial damped oscillator directly implemented at the output of the transformer. A CST-based simulation is proposed to predict the performances of this wideband source.
%G English
%L hal-02153450
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02153450
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T A Tesla Transformer and a Coaxial Peaking Switch as a UWB Pulse Source
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%+ Commissariat à l'Energie Atomique et aux Energies Alternatives (CEA-CESTA)
%A Amal, Y
%A PECASTAING, Laurent
%A Rivaletto, Marc
%A REESS, Thierry
%A Pignolet, P
%A Cassany, B
%A de Ferron, Antoine Silvestre
%A Courtois, L
%A Merle, E
%< avec comité de lecture
%@ 0587-4246
%J Acta Physica Polonica A
%I Polish Academy of Sciences. Institute of Physics
%8 2008
%D 2008
%K numbers: 84.70.+p
%K 07.57.Hm
%K 84.30.Ng
%Z Engineering Sciences [physics]/Electric power
%Z Engineering Sciences [physics]/Electronics
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%X This paper presents a high voltage pulse source which is able to generate ultra wideband (UWB) pulses during about 1 ns through a 16 antennas array. This UWB source is composed of a 50 kV DC voltage supply, a Tesla transformer to amplify this voltage up to 400 kV, a gaseous pressurized peaking switch and an impedance transformer (50 Ω → 3.125 Ω). This output impedance value corresponds to the input impedance value of a sixteen 50 Ω antennas array. That is why a distributor is needed in order to feed the antenna array. In this paper, the peaking switch and the capacitive line divider used to characterise the generated pulses are particularly described. The peaking switch is based on the principle of a line discharge by means of a high pressure gas switch. It is loaded with a Tesla transformer to obtain a good pulse reproducibility. The main characteristics of the output pulse waveform (amplitude and rise time) are linked to the properties of the gas switch and particularly to the gap distance, the pressure and the nature of the gas used in the switch filling. The aim is to find a good compromise between various parameters as the output pulse amplitude, the rise time and the repetition rate in order to ensure a better efficiency of the UWB source. Classical voltage measurement techniques do not allow us an estimation of the main characteristics of such an output signal. Therefore a voltage probe was designed and realised to measure both the amplitude and the rise time of the pulses delivered by the generator. This device is based on the principle of a capacitive line divider. Calibration tests (transient and frequency tests) were performed and show that the high cutoff frequency, around 2.5 GHz, is consistent with the transient response of the output high voltage waveform. The design, realisation and calibration tests are also presented.
%G English
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-03020521/document
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-03020521/file/A_Tesla_Transformer_and_a_Coaxial_Peaking_Switch_a%20%281%29.pdf
%L hal-03020521
%U https://hal-univ-pau.archives-ouvertes.fr/hal-03020521
%~ UNIV-PAU
%~ CEA
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Development of a 0.6-MV Ultracompact Magnetic Core Pulsed Transformer for High-Power Applications
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%+ Loughborough University 
%A PECASTAING, Laurent
%A Rivaletto, Marc
%A de Ferron, Antoine Silvestre
%A Pecquois, Romain
%A Novac, Bucur, M
%< avec comité de lecture
%@ 0093-3813
%J IEEE Transactions on Plasma Science
%I Institute of Electrical and Electronics Engineers
%V 46
%N 1
%P 156-166
%8 2018
%D 2018
%R 10.1109/tps.2017.2781620
%K Index Terms-High voltage techniques
%K pulse power systems
%K transformers
%Z Engineering Sciences [physics]/Electric powerJournal articles
%X The generation of high-power electromagnetic waves is one of the major applications in the field of high-intensity pulsed power. The conventional structure of a pulsed power generator contains a primary energy source and a load separated by a power-amplification system. The latter performs time compression of the slow input energy pulse and delivers a high-intensity power output to the load. Usually, either a Marx generator or a Tesla transformer is used as a power amplifier. In the present case, a system termed "module oscillant utilisant une nouvelle architecture" (MOUNA) uses an innovative and very compact resonant pulsed transformer to drive a dipole antenna. This paper describes the ultracompact multiprimary winding pulsed transformer developed in common by the Université de Pau and Hi Pulse Company that can generate voltage pulses of up to 0.6 MV, with a rise time of less than 270 ns. The transformer design has four primary windings, with two secondary windings in parallel, and a Metglas 2605SA1 amorphous iron magnetic core with an innovative biconic geometry used to optimize the leakage inductance. The overall unit has a weight of 6 kg and a volume of only 3.4 L, and this paper presents in detail its design procedure, with each of the main characteristics being separately analyzed. In particular, simple but accurate analytical calculations of both the leakage inductance and the stray capacitance between the primary and secondary windings are presented and successfully compared with CST-based results. Phenomena such as the core losses and saturation induction are also analyzed. The resonant power-amplifier output characteristics are experimentally studied when attached to a compact capacitive load, coupled to a capacitive voltage probe developed jointly with Loughborough University. Finally, an LTspice-based model of the power amplifier is introduced and its predictions are compared with results obtained from a thorough experimental study.
%G English
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-02014189/document
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-02014189/file/Transfo%20Mouna_HAL.pdf
%L hal-02014189
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02014189
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Guest Editorial Special Issue on Selected Papers From EAPPC/BEAMS 2018
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%A PECASTAING, Laurent
%A Cho, Chuhyun
%A Zhang, Zicheng
%< avec comité de lecture
%@ 0093-3813
%J IEEE Transactions on Plasma Science
%I Institute of Electrical and Electronics Engineers
%V 47
%N 10
%P 4432-4432
%8 2019-10
%D 2019
%R 10.1109/TPS.2019.2943198
%Z Engineering Sciences [physics]/Electric power
%Z Engineering Sciences [physics]/Electronics
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%G English
%L hal-02373857
%U https://hal.archives-ouvertes.fr/hal-02373857
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Thesis
%T Design and performance of an ultra short high voltage pulse generating system Application to wideband radars
%+ Laboratoire de Génie Electrique
%A PECASTAING, Laurent
%Z MEMBRES DU JURY : B. JECKO Université de Limoges (IRCOM) Président - Rapporteur; B. DE FORNEL Université de Toulouse (ENSEEIHT) - Rapporteur; J. ANDRIEU Université de Limoges (IRCOM Brive); P. DOMENS Université de Pau (LGE); M. LE GOFF DGA - Bruz (CELAR); E. MERLE CEA - Moronvilliers; J. PAILLOL Université de Pau (LGE); V. PUECH Université de Paris XI (LPGP);
%Z N°ordre: 01PAUU3023
%I Université de Pau et des Pays de l'Adour
%Y DOMENS Pierre(pierre.domens@univ-pau.fr)
%8 2001-12-07
%D 2001
%K high voltage
%K pulsed power
%K ferrite bead
%K wideband radar
%K haute tension
%K radar
%K applications militaires
%K dispositif à ferrite
%K générateur de puissance à impulsions
%Z Engineering Sciences [physics]/OtherTheses
%X This thesis is about the improvement of the performances of an ultra wideband (UWB) radar set up for landmines detection. An ultra short high voltage (HV) pulse generator and an impedance matcher device (balun) to be inserted between the 50 Ohm; generator and two-wire antennas 200 Ohm; are particularly detailed. The first chapter shows the principal characteristics of UWB radars and a bibliographical synthesis of the experimental tools necessary for the UWB. The second chapter is devoted to the HV pulse generator based on the technology of a pressurised gas switch (hydrogen). The principle of operation and the characterizations of probes produced for these experiments are also deferred. After various parameter settings, the impulses rise-time can be reduced down to 90 ps, the minimal time duration is of 600 ps, the output voltage and the repetition rate can reach respectively 30 kV and 2,5 kHz. A simulation of the device is compared to the practical results of commutation. In chapter 3, we gathered the studies of the balun and of a voltage amplifier able to supply the generator because these two devices are two transmission line transformers (TLT). The principal defect of the TLT lies in the losses due to the secondary lines. We solved this problem by using ferrite for which we explain process. This study leads to the realization of a ten stages TLT of perfect yield and of a balun whose bandwidth lies between 30 kHz and 3 GHz. A simulation of the experimental results completes the interpretation of the phenomena. Lastly, in chapter 4, the experimental results realized in anechoic chamber relating to the whole emission system thus a few prospects relating to the advance for the system are presented.
%X Le travail présenté dans cette thèse concerne l'amélioration des performances d'un démonstrateur radar ultra large bande (ULB) pour la détection de mines dans le sol. Un générateur d'impulsions haute tension (HT) ultra brèves et d'un dispositif d'adaptation d'impédances (balun) destiné à être inséré entre le générateur 50 Ohm; et des antennes bifilaires 200 Ohm; sont particulièrement détaillés. Le premier chapitre présente les caractéristiques principales des radars ULB et une synthèse bibliographique des outils expérimentaux nécessaires pour l'ULB. Le second chapitre est consacré au générateur d'impulsions HT ultra brèves basé sur la technologie d'un commutateur à gaz pressurisé (hydrogène). Le principe de fonctionnement et les caractérisations d'atténuateurs réalisés pour ces expériences sont également reportés. Après divers paramétrages, le temps de montée des impulsions peut être réduit à 90 ps, la durée à mi-hauteur minimale est de 600 ps, la tension de sortie et la fréquence de répétition peuvent atteindre respectivement 30 kV et 2,5 kHz. Une simulation du dispositif est comparée aux résultats pratiques de commutation. Dans le chapitre 3, nous avons regroupé les études du balun et d'un amplificateur de tension pouvant alimenter le générateur car ces deux dispositifs sont deux transformateurs à lignes de transmission (TLT). Le principal défaut des TLT réside dans les pertes dues aux lignes secondaires. Nous avons solutionné ce problème par l'utilisation de ferrites dont nous explicitons le fonctionnement. Cette étude débouche sur la réalisation d'un TLT 10 étages de gain 10 et d'un balun dont la bande passante est comprise entre 30 kHz et 3 GHz. Une simulation de l'ensemble des résultats expérimentaux vient compléter l'interprétation des phénomènes. Enfin, dans le chapitre 4, les résultats d'essais réalisés en chambre anéchoïde portant sur l'ensemble du système d'émission ainsi que des perspectives relatives à l'évolution de l'ensemble sont présentés.
%G French
%2 https://tel.archives-ouvertes.fr/tel-00010696/document
%2 https://tel.archives-ouvertes.fr/tel-00010696/file/tel-00010696.pdf
%L tel-00010696
%U https://tel.archives-ouvertes.fr/tel-00010696
%~ UNIV-PAU
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Removal of diesel soot particules using an esp supplied by a hybrid voltage
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%A de Ferron, Antoine Silvestre
%A REESS, Thierry
%A PECASTAING, Laurent
%A Gibert, Alain
%A Domens, Pierre
%A Pignolet, P.
%< avec comité de lecture
%@ 1286-0042
%J European Physical Journal: Applied Physics
%I EDP Sciences
%8 2008
%D 2008
%R 10.1051/epjap:2008125
%Z Engineering Sciences [physics]/Electric power
%Z Engineering Sciences [physics]/Electronics
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%X Electrostatic filtration is a process that has been studied for more than a century. One of the most recent applications is Diesel exhaust gas aftertreatment. More specifically, car manufacturers are very interested in using electrical discharge phenomena to remove the soot particles from exhaust gases. This process could be a feasible alternative to the Diesel particulate filter used at the present time. The aim of this paper is to investigate the effects of repetitive voltage impulses on the treatment efficiency on soot particles and to compare results with those achieved when a DC supply is used. The study was divided into two steps: in the first, a study of the modes of discharge which develop in an electrostatic precipitator allows us to optimize both the geometry of an ESP and a hybrid pulsed power/direct voltage supply. This study was performed without gas flow. In the second step, investigations concerning treatment efficiency were carried out under practical conditions using an engine test bench. Results show that the combination of direct voltage (-7 kV) and pulsed voltage (-14 kV, 3 kHz) provides treatment efficiency close to 75% using electrical power three times lower than that required using a DC supply. PACS. 52. Physics of plasmas and electric discharges-07.89.+b Environmental effects on instruments
%G English
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-03020508/document
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-03020508/file/PUBLI%20MOTEUR%20EPJ-AP.pdf
%L hal-03020508
%U https://hal-univ-pau.archives-ouvertes.fr/hal-03020508
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Optimizing the operation of an electrostatic precipitator by developing a multipoint electrode supplied by a hybrid generator
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%+ Commissariat à l'Energie Atomique, Centre de Marcoule (CEA)
%A de Ferron, Antoine Silvestre
%A Pécastaing, Laurent
%A REESS, Thierry
%A Pignolet, P
%A Lemont, F
%< avec comité de lecture
%@ 0022-3727
%J Journal of Physics D: Applied Physics
%I IOP Publishing
%8 2009
%D 2009
%R 10.1088/0022-3727/42/10/105504
%Z Engineering Sciences [physics]/Electric power
%Z Engineering Sciences [physics]/Electronics
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%X The authors investigated and improved the filtration efficiency of an electrostatic precipitator (ESP). A laboratory-scale pilot unit was developed to allow experimentation under conditions approaching those of the industrial ESPs used by the CEA at Marcoule (France). After elucidating the electrical phenomena and optically analysing the physical processes occurring inside the precipitator, a specific electrode was developed for use with a hybrid power supply. The experiments were based on analysing the variation over time of the electric charge injected into the particle separator, the particle mass collected at the ground electrode and the charges quantity measured on a grid in the airstream after the electrode unit. Photos were also taken under different electrical discharge conditions. The results show that combining a multipoint electrode and a hybrid generator (30 kV dc and 30 kV, 1 kHz) improves the process efficiency and significantly extends the time frame (more than 10 h) during which the process operates at optimum efficiency.
%G English
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-03020514/document
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-03020514/file/publi%20Marcoule%20Version%20d%C3%A9finitive%20%281%29.pdf
%L hal-03020514
%U https://hal-univ-pau.archives-ouvertes.fr/hal-03020514
%~ UNIV-PAU
%~ CEA
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

