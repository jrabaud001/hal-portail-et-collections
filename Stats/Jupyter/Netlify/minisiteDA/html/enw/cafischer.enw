%0 Journal Article
%T La retorica sconfitta dalla forza del contesto etico-politico Riflessioni sulla ricezione di due traduzioni
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Fischer, Caroline
%< avec comité de lecture
%@ 2039-6597
%J Between. Rivista dell’Associazione italiana di teoria e storia comparata della letteratura
%I University of Cagliari
%V IV
%8 2014
%D 2014
%K Ricezione
%Z Humanities and Social Sciences/LiteratureJournal articles
%X Quando parliamo di retorica, abbiamo in mente la forza della parola, la sua capacità di fare effetto su chi la recepisce 1 , e, nello stesso tempo, la retorica appare spesso come un ente quasi assoluto. Ci concentriamo sulle mere parole, dimenticando a volte che il loro effetto non emana unicamente da una specifica forma retorica o dall'intenzione dell'autore, ma altrettanto dal pubblico con il quale andranno confrontandosi. In quello che segue, studieremo due esempi che dimostrano palesemente come una stessa espressione retorica possa essere accolta in modo ben diverso a seconda del discorso etico e politico prevalente nel quale l'opera viene compresa. Ci interesseremo a due romanzi francesi che hanno suscitato, per motivi diversi, delle reazioni stupefacenti al momento della loro pubblicazione in tedesco. Vedremo come una stessa espressione letteraria può far nascere dibattiti che variano a seconda del contesto della loro ricezione. Quando Les particules élémentaires di Michel Houellebecq 2 e Les Bienveillantes di Jonathan Littell 3 hanno fatto la loro 1 Effetto descritto fin dalla Poetica di Aristotele come catarsi.
%G Italian
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-01995817/document
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-01995817/file/Houellebecq%20Littel%20ital.pdf
%L hal-01995817
%U https://hal-univ-pau.archives-ouvertes.fr/hal-01995817
%~ UNIV-PAU
%~ SHS
%~ ALTER
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Dove vai, dottor Dapertutto? Ovvero dal campus novel al romanzo del professore
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Fischer, Caroline
%< avec comité de lecture
%@ 2039-6597
%J Between. Rivista dell’Associazione italiana di teoria e storia comparata della letteratura
%I University of Cagliari
%8 2013
%D 2013
%K campus novel
%Z Humanities and Social Sciences/LiteratureJournal articles
%X Nel romanzo di Javier Marías Tutte le anime (tit. orig. Todas las al-mas, 1989), il cui titolo allude al luogo dell'azione, ovvero il college "All Souls" di Oxford, ogni mattina Will, il portiere, saluta l'Io narrante ri-volgendosi a lui con un nome sempre diverso. Il vecchio portiere, quasi novantenne, immagina di ritrovarsi a vivere giorno dopo giorno in un'epoca diversa della sua lunga vita e si rivolge pertanto al visiting professor spagnolo tutte le mattine come se si trattasse di un collega d'altri tempi. Nell'arco di una settimana, Will crede di vedere in lui una serie di persone vissute in decenni diversi. Questi scambi portano alla conclusione che i professori di Oxford sono rimasti invariati alme-no sin dall'epoca della Grande Guerra e che essi risultano in fondo in-terscambiabili. Situazioni del tutto diverse si ritrovano, invece, nei cinque roman-zi oggetto del presente contributo: nati tutti in un'unica epoca, la "no-stra epoca postmoderna", essi possono essere annoverati nel genere del romanzo universitario, pur mostrando differenze talmente palesi l'uno rispetto all'altro da rendere anzitutto necessaria l'elaborazione di alcu-ni criteri in grado di rendere stringente un confronto reciproco. Poiché il successo riscosso dai cinque romanzi qui selezionati a ti-tolo d'esempio è stato molto vario-si passa da bestseller internaziona-li a un Premio Goncourt fino a un'opera pubblicata solo in prima edi-zione-si procederà anzitutto a una loro breve presentazione in ordine cronologico. Seguiranno poi alcune riflessioni sul genere del romanzo
%G Italian
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-01995818/document
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-01995818/file/Dove%20vai%20dottor%20Dapertutto.pdf
%L hal-01995818
%U https://hal-univ-pau.archives-ouvertes.fr/hal-01995818
%~ UNIV-PAU
%~ SHS
%~ ALTER
%~ TESTUPPA2
%~ UPPA-OA

