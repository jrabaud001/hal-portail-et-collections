%0 Conference Proceedings
%T Amélioration de la dispersion des nanotubes de carbone dans une résine époxyde à l'aide de copolymères à bloc = Improvement of the dispersion of carbon nanotube in epoxy resin using block copolymers
%+ Institut pluridisciplinaire de recherche sur l'environnement et les matériaux (IPREM)
%A Courbaron, Anne-Claude
%A Derail, Christophe
%A Billon, Laurent
%A Péré, Eve
%A El Bounia, Nour-Eddine
%< avec comité de lecture
%Z 148
%B JNC 16
%C Toulouse, France
%Y Philippe OLIVIER et Jacques LAMON
%I AMAC
%P 6 p.
%8 2009-06-10
%D 2009
%K nanotubes de carbone
%K copolymères
%K dispersion
%K époxyde
%K carbon nanotubes
%K copolymers
%K epoxy
%Z Engineering Sciences [physics]/Materials
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]Conference papers
%X Avec un module de Young proche du TPa, les nanotubes de carbone (NTC) semblent des charges idéales pour l'amélioration des propriétés mécaniques d'une résine époxyde. Cependant, une bonne dispersion des nanotubes dans la matrice ne suffit pas à obtenir une augmentation des propriétés mécaniques. La nature de l'interface nanotube-matrice doit être modifiée pour assurer une bonne adhésion entre les deux composants. Pour cela, des copolymères à bloc « polyacide acrylique - polyméthacrylate de méthyle » sont synthétisés par polymérisation radicalaire contrôlée par voie nitroxyde. Le bloc de polyacide acrylique a une bonne affinité avec le nanotube, alors que le polyméthacrylate de méthyle est compatible avec le prépolymère époxyde. Cette double affinité permet au copolymère à bloc de se placer préférentiellement à l'interface entre la matrice et les nanotubes facilitant et stabilisant la dispersion des nanotubes de carbone dans la matrice. La polymérisation des copolymères peut être réalisée en présence de NTC dans le but d'assurer le greffage des copolymères sur les nanotubes. L'influence des copolymères sur la dispersion est visible aux échelles macroscopique et microscopique. Les copolymères greffés sont les plus performants du point de vue de la dispersion. D'autre part, l'augmentation de la densité de greffage des copolymères sur les NTC favorise leur dispersion.
%G French
%Z Session : Modélisation du matériau (DOUCHIN B. -- OLIVIER P.)
%2 https://hal.archives-ouvertes.fr/hal-00387597/document
%2 https://hal.archives-ouvertes.fr/hal-00387597/file/148b.pdf
%L hal-00387597
%U https://hal.archives-ouvertes.fr/hal-00387597
%~ CNRS
%~ UNIV-PAU
%~ JNC16
%~ IPREM
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Influence of the addition of PEG into PCL‐based waterborne polyurethane‐urea dispersions and films properties
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Polytechnic School. University of the Basque Country
%A Vadillo, Julen
%A Larraza, Izaskun
%A Arbelaiz, Aitor
%A Corcuera, Maria Angeles
%A Save, Maud
%A Derail, Christophe
%A Eceiza, Arantxa
%< avec comité de lecture
%@ 0021-8995
%J Journal of Applied Polymer Science
%I Wiley
%V 137
%N 26
%P 48847
%8 2020-07-10
%D 2020
%R 10.1002/app.48847
%K synthesis and processing techniques
%K polyurethane
%K morphology
%K emulsion polymerization
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Material chemistry
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Analytical chemistryJournal articles
%X In this work, segmented waterborne polyurethane‐urea (WBPUU) dispersions containing hydrophobic polycaprolactone (PCL) and hydrophilic poly(ethylene glycol) (PEG) in different molar ratios are synthesized and used for the subsequent preparation of films by casting. The molar ratio of PEG is gradually increased up to 0.20 in order to analyze the effect of low hydrophilic PEG content (from 1.7 to 7.1 wt %) on the properties of resulting dispersions and films. Regarding the dispersions, the addition of PEG results in an increase of the particle size, from 86 ± 1 in the case of pure PCL‐based system to 112 ± 15 for systems containing 7.1 wt % of PEG, as well as in the formation of a core–shell structure in the particles. Films show different behaviors depending on their PEG content. WBPUUs containing just PCL or very low content of PEG in the soft segment present higher stiffness. However, the addition of PEG in a content of 3.4 wt % or higher hinder the ability of the short‐range ordering of the hard segment, increasing the elongation at break from 842 ± 102 MPa for PCL‐based WBPUU to 1312 ± 84 MPa for the system with the highest PEG content. Systems with higher PEG content form nanoparticles with more segregated core–shell structures inducing to the film a higher hydrophilicity. Hence, the addition of PEG to a PCL‐based WBPUU allows to tune the properties of the resulting film increasing the range of application of these materials
%G English
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-02429598/document
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-02429598/file/16639931.pdf
%L hal-02429598
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02429598
%~ CNRS
%~ UNIV-PAU
%~ IPREM
%~ INC-CNRS
%~ IPREM-PCM
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Influence of specific mechanical energy during compounding in a co-kneader on electrical and rheological properties of multiscale composite materials
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Instituto de Nanociencia de Aragón [Saragoza, España] (INA)
%A Salinier, Axel
%A Dagréou, Sylvie
%A Visse, Arthur
%A Navascués, Nuria
%A Léonardi, Frédéric
%A Derail, Christophe
%< avec comité de lecture
%@ 2452-2139
%J Composites Communications
%I Elsevier
%V 17
%P 154-162
%8 2020-02
%D 2020
%R 10.1016/j.coco.2019.12.003
%K Polymer
%K Rheology
%K poly(etherimide)
%K Multiwalled carbon nanotubes
%K Co-kneading
%K Composite materials
%K Electrical threshold
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Material chemistry
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Analytical chemistryJournal articles
%X This paper focuses on the effect of different scales of fillers, from nano to macrometer, on the rheological and electrical behaviour of thermoplastic-based composites. Composites based on poly (etherimide) matrix (PEI) with multiscale reinforcement are processed by melting in a co-kneader to disperse homogeneously carbon nanotubes and glass fibers. We clearly demonstrate the strong link between the liquid-solid transition and the electrical conductivity. We show that the specific mechanical energy is a key parameter. Glass fibers allow to increase the electrical conductivity of blends based on carbon nanotubes and to obtain a lower decreasing of conductivity at the liquid transition which takes place in the same time as rheological and electrical parameters.
%G English
%2 https://hal.archives-ouvertes.fr/hal-02429046/document
%2 https://hal.archives-ouvertes.fr/hal-02429046/file/1-s2.0-S2452213919301871-main.pdf
%L hal-02429046
%U https://hal.archives-ouvertes.fr/hal-02429046
%~ CNRS
%~ UNIV-PAU
%~ IPREM
%~ INC-CNRS
%~ IPREM-PCM
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Adhesion of surgical sealants used in cardiothoracic and vascular surgery
%+ Science et Ingénierie des Matériaux et Procédés (SIMaP )
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Team 1 LCPO : Polymerization Catalyses & Engineering
%+ Laboratoire de Chimie des polymères organiques (LCPO)
%A Perrin, B.R.M.
%A Braccini, M.
%A Bidan, C.M.
%A Derail, Christophe
%A Papon, E.
%A Leterrier, Y.
%A Barrandon, Y.
%< avec comité de lecture
%@ 0143-7496
%J International Journal of Adhesion and Adhesives
%I Elsevier
%V 70
%P 81-89
%8 2016
%D 2016
%R 10.1016/j.ijadhadh.2016.05.009
%K Cyanoacrylates
%K Bulge and blister tests
%K Adhesion in surgery and medicine
%K Realistic conditions
%K Scanning electron microscopy
%K Mechanical properties of adhesives
%K Environmental scanning electron microscopies (ESEM)
%Z Chemical SciencesJournal articles
%X Surgical sealants are widely used in cardiothoracic and vascular surgery essentially for hemostasis and sealing. Their adhesive properties have mainly been studied by clinical experiments. The objective of this study is to measure adhesion of the three main types of surgical sealant used in cardiothoracic and vascular surgery under normalized realistic conditions. The bulge-and-blister test was used to quantify adhesive performances of three types of surgical adhesives: cyanoacrylates, polyethylene glycol (PEG), and aldehydes. Samples were composed of two circular layers of equine pericardium glued by the surgical sealant studied. Comparative adhesion testing was carried out in eight samples bonded with a Dermabond® (cyanoacrylate), five samples with Bioglue® (aldehyde), four samples with Coseal® (PEG), and thirteen samples bonded with an industrial cyanoacrylate. Scanning electron microscope (SEM) observations of cross-sections of samples glued by Dermabond® and environmental scanning electron microscopy (ESEM) images of samples composed of equine pericardium glued by Bioglue® were also performed. The average value of the adhesion energy is 2.3+/-1.5 J.m-2 for samples glued with Dermabond®, 6.04+/-1.61 J.m-2 with Bioglue®, 2.37+/-1.25 J m-2 with Coseal®, 3.74+/-1.33 J m-2 with the industrial cyanoacrylate glue in surgical conditions. SEM observations of cross-sections of samples glued by Dermabond® showed a failure at the interface between the glue and the pericardial layer. ESEM observations have revealed a majority of regions where the glue is not linked to the pericardium. Adhesive performance measurements and microscopy observations in surgical conditions show that surgical sealants adhesion is weak and explain their poor efficacy in clinical practice. To improve adhesion in the surgical field, we need to focus on achieving a better cohesion between the adhesive and the substrate by modifying conditions adhesive bonding and consequently tend toward cohesive failure.
%G English
%L hal-01495527
%U https://hal.archives-ouvertes.fr/hal-01495527
%~ CNRS
%~ UNIV-BORDEAUX
%~ INC-CNRS
%~ IPREM
%~ INPG
%~ SIMAP
%~ IPREM-PCM
%~ UGA-COMUE
%~ LCPO
%~ UGA
%~ TESTUPPA2
%~ UPPA-OA
%~ UNIV-PAU

