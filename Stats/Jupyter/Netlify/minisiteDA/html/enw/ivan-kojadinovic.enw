%0 Journal Article
%T Coastal flooding event definition based on damages: Case study of Biarritz Grande Plage on the French Basque coast
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%+ LIttoral ENvironnement et Sociétés - UMRi 7266 (LIENSs)
%A Arnoux, Florian
%A Abadie, Stéphane
%A Bertin, Xavier
%A Kojadinovic, Ivan
%< avec comité de lecture
%@ 0378-3839
%J Coastal Engineering
%I Elsevier
%V 166
%P 103873
%8 2021-06
%D 2021
%R 10.1016/j.coastaleng.2021.103873
%K coastal flooding
%K return period
%K storm
%K event definition
%K damage
%K historical data
%K risk
%K Basque coast
%Z Engineering Sciences [physics]/Civil Engineering/Risques
%Z Mathematics [math]/Statistics [math.ST]
%Z Sciences of the Universe [physics]/Ocean, AtmosphereJournal articles
%X This paper presents a method to include damage at the initial stage of coastal flooding events definition and in return periods computation. The methodology is illustrated within a local study carried out in Biarritz Grande Plage, a meso-tidal, wave dominated beach located on the french basque coast in the south west of France. The work is based on two datasets covering the period 1949-2015 : a first one, consisting of observation and synthetic data on wave characteristics and water level, and a second one, gathering storm dates and related damage intensities obtained through investigations in the press and in archives. A statistical analysis was first carried out to find the best combination of source variables explaining the reported damages for the identified storms. Maximal, mean and accumulated values were calculated over storm duration, considering source and aggregated variables based on the empirical run-up formula or the wave energy flux. Most rules combining a wave parameter and water level are found to provide satisfactory damage prediction as soon as maxima variables are considered. Rules based on mean variables are less accurate and those based on accumulated variable values are not relevant. The best results are obtained with the wave energy flux and water level maxima over the storm duration. The ability of the rules to be used as generic event definition rules is then tested by performing a retrospective analysis on the whole dataset, checking their efficiency in detecting historical storms (i.e., with damages) without finding too many false positives. Most of the rules formerly studied, except the ones using wave period only as wave parameter, were able to correctly perform this task. Coastal flood event return periods (RP) were then calculated by applying three of the best rules identified previously. The rule using non simultaneous maxima of wave energy flux and water level gives encouraging results for the RP values. Nevertheless, the discrepancy still observed among the different rules calls for further work in this direction.
%G English
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-03200009/document
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-03200009/file/Coastal%20flooding%20event%20definition%20based%20on%20damages%3A%20Case%20study%20of%20Biarritz%20Grande%20Plage%20on%20the%20French%20Basque%20coast_author_version.pdf
%L hal-03200009
%U https://hal-univ-pau.archives-ouvertes.fr/hal-03200009
%~ UNIV-PAU
%~ SIAME
%~ LMA-PAU
%~ INSMI

%0 Thesis
%T Contributions à l'interprétation de mesures non additives et `a l'identification de modèles décisionnels fondés sur l'intégrale de Choquet
%+ Laboratoire d'Informatique de Nantes Atlantique (LINA)
%A Kojadinovic, Ivan
%I Université de Nantes
%Y Michel Grabisch
%8 2006-11-21
%D 2006
%K Intégral Choquet
%Z Computer Science [cs]Habilitation à diriger des recherches
%X Ce document présente une synthèse des activités de recherche que j'ai menées depuis dé- but 2002 sur les mesures non additives. J'ai choisi, par souci de clarté, de présenter en détails les résultats concernant l'interprétation et l'identication de mesures non additives obtenus dans le cadre de diverses collaborations et de ne décrire que succinctement les recherches connexes que je mène de façon moins soutenue sur la classication et la sélection de variables dans le contexte de l'analyse de données et de la modélisation statistique (cf. Annexes A et A). Les travaux sur les mesures et intégrales non additives décrits dans ce document s'organisent selon deux axes : le premier axe porte sur l'interprétation de mesures non additives dans le contexte de la théorie des jeux coopératifs et de l'aide multicritère à la décision fondée sur les intégrales non additives ; le deuxième axe concerne l'identication de mesures non additives dans le contexte de l'agrégation par intégrale de Choquet. Ces deux directions de recherche sont fortement complémentaires dans la mesure où l'utilisation d'intégrales non additives dans un contexte décisionnel nécessite, dans un premier temps, l'identication d'une mesure non additive et, dans un deuxième temps, son interprétation an de rendre le modèle de décision plus transparent pour le décideur.
%G French
%2 https://tel.archives-ouvertes.fr/tel-00481256/document
%2 https://tel.archives-ouvertes.fr/tel-00481256/file/hdr.pdf
%L tel-00481256
%U https://tel.archives-ouvertes.fr/tel-00481256
%~ CNRS
%~ UNIV-NANTES-THESES
%~ LINA-COD
%~ UNIV-NANTES
%~ LINA
%~ LINA-DUKE

%0 Journal Article
%T Detecting breaks in the dependence of multivariate extreme-value distributions
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%A Bücher, A.
%A Kinsvater, P.
%A Kojadinovic, Ivan
%Z ACL
%< avec comité de lecture
%@ 1386-1999
%J Extremes
%I Springer Verlag (Germany)
%V 20
%N 1
%P 53--89
%8 2017
%D 2017
%R 10.1007/s10687-016-0268-y
%Z Mathematics [math]
%Z Mathematics [math]/Numerical Analysis [math.NA]
%Z Mathematics [math]/Analysis of PDEs [math.AP]
%Z Mathematics [math]/Geometric Topology [math.GT]
%Z Mathematics [math]/Probability [math.PR]Journal articles
%X no abstract
%G English
%L hal-01581294
%U https://hal.archives-ouvertes.fr/hal-01581294
%~ LMA-PAU
%~ CNRS
%~ UNIV-PAU
%~ INSMI
%~ TDS-MACS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T A goodness-of-fit test for bivariate extreme-value copulas
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Center for Human Genetics, University of Leuven School of Medicine
%A Genest, C.
%A Kojadinovic, Ivan
%A Nešlehová, J.
%A Yan, J.
%Z cited By (since 1996)12
%< avec comité de lecture
%@ 1350-7265
%J Bernoulli
%I Bernoulli Society for Mathematical Statistics and Probability
%V 17
%N 1
%P 253-275
%8 2011
%D 2011
%R 10.3150/10-BEJ279
%Z Mathematics [math]/Probability [math.PR]Journal articles
%X It is often reasonable to assume that the dependence structure of a bivariate continuous distribution belongs to the class of extreme-value copulas. The latter are characterized by their Pickands dependence function. In this paper, a procedure is proposed for testing whether this function belongs to a given parametric family. The test is based on a Cramér-von Mises statistic measuring the distance between an estimate of the parametric Pickands dependence function and either one of two nonparametric estimators thereof studied by Genest and Segers [Ann. Statist. 37 (2009) 2990-3022]. As the limiting distribution of the test statistic depends on unknown parameters, it must be estimated via a parametric bootstrap procedure, the validity of which is established. Monte Carlo simulations are used to assess the power of the test and an extension to dependence structures that are left-tail decreasing in both variables is considered. © 2011 ISI/BS.
%G English
%L hal-00865060
%U https://hal.archives-ouvertes.fr/hal-00865060
%~ CNRS
%~ LMA-PAU
%~ INSMI
%~ UNIV-PAU
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Goodness-of-fit testing based on a weighted bootstrap: A fast large-sample alternative to the parametric bootstrap
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Center for Human Genetics, University of Leuven School of Medicine
%A Kojadinovic, Ivan
%A Yan, J.
%Z cited By (since 1996)0
%< avec comité de lecture
%@ 0319-5724
%J Canadian Journal of Statistics
%I Wiley
%V 40
%N 3
%P 480-500
%8 2012
%D 2012
%R 10.1002/cjs.11135
%Z Mathematics [math]/Probability [math.PR]Journal articles
%X The process comparing the empirical cumulative distribution function of the sample with a parametric estimate of the cumulative distribution function is known as the empirical process with estimated parameters and has been extensively employed in the literature for goodness-of-fit testing. The simplest way to carry out such goodness-of-fit tests, especially in a multivariate setting, is to use a parametric bootstrap. Although very easy to implement, the parametric bootstrap can become very computationally expensive as the sample size, the number of parameters, or the dimension of the data increase. An alternative resampling technique based on a fast weighted bootstrap is proposed in this paper, and is studied both theoretically and empirically. The outcome of this work is a generic and computationally efficient multiplier goodness-of-fit procedure that can be used as a large-sample alternative to the parametric bootstrap. In order to approximately determine how large the sample size needs to be for the parametric and weighted bootstraps to have roughly equivalent powers, extensive Monte Carlo experiments are carried out in dimension one, two and three, and for models containing up to nine parameters. The computational gains resulting from the use of the proposed multiplier goodness-of-fit procedure are illustrated on trivariate financial data. A by-product of this work is a fast large-sample goodness-of-fit procedure for the bivariate and trivariate t distribution whose degrees of freedom are fixed. © 2012 Statistical Society of Canada.
%G English
%L hal-00865050
%U https://hal.archives-ouvertes.fr/hal-00865050
%~ CNRS
%~ LMA-PAU
%~ INSMI
%~ UNIV-PAU
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Nonparametric tests for change-point detection à la Gombay and Horváth
%+ University of Connecticut (UCONN)
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%A Holmes, M.
%A Kojadinovic, Ivan
%A Quessy, J.-F.
%< avec comité de lecture
%@ 0047-259X
%J Journal of Multivariate Analysis
%I Elsevier
%V 115
%P 16-32
%8 2013
%D 2013
%R 10.1016/j.jmva.2012.10.004
%Z Mathematics [math]/Probability [math.PR]Journal articles
%X The nonparametric test for change-point detection proposed by Gombay and Horváth is revisited and extended in the broader setting of empirical process theory. The resulting testing procedure for potentially multivariate observations is based on a sequential generalization of the functional multiplier central limit theorem and on modifications of Gombay and Horváth's seminal approach that appears to improve the finite-sample behavior of the tests. A large number of candidate test statistics based on processes indexed by lower-left orthants and half-spaces are considered and their performance is studied through extensive Monte Carlo experiments involving univariate, bivariate and trivariate data sets. Finally, practical recommendations are provided and the tests are illustrated on trivariate hydrological data. © 2012 Elsevier Inc.
%G English
%L hal-00867036
%U https://hal.archives-ouvertes.fr/hal-00867036
%~ CNRS
%~ LMA-PAU
%~ INSMI
%~ UNIV-PAU
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Large-sample tests of extreme-value dependence for multivariate copulas
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Institut de Statistique, Biostatistique et Sciences Actuarielles (ISBA)
%+ Center for Human Genetics, University of Leuven School of Medicine
%A Kojadinovic, Ivan
%A Segers, J.
%A Yan, J.
%Z cited By (since 1996)3
%< avec comité de lecture
%@ 0319-5724
%J Canadian Journal of Statistics
%I Wiley
%V 39
%N 4
%P 703-720
%8 2011
%D 2011
%R 10.1002/cjs.10110
%Z Mathematics [math]/Probability [math.PR]Journal articles
%X Starting from the characterization of extreme-value copulas based on max-stability, large-sample tests of extreme-value dependence for multivariate copulas are studied. The two key ingredients of the proposed tests are the empirical copula of the data and a multiplier technique for obtaining approximate p-values for the derived statistics. The asymptotic validity of the multiplier approach is established, and the finite-sample performance of a large number of candidate test statistics is studied through extensive Monte Carlo experiments for data sets of dimension two to five. In the bivariate case, the rejection rates of the best versions of the tests are compared with those of the test of Ghoudi et al. (1998) recently revisited by Ben Ghorbal et al. (2009). The proposed procedures are illustrated on bivariate financial data and trivariate geological data. © 2011 Statistical Society of Canada.
%G English
%L hal-00865055
%U https://hal.archives-ouvertes.fr/hal-00865055
%~ CNRS
%~ LMA-PAU
%~ INSMI
%~ UNIV-PAU
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Nonparametric sequential change-point detection for multivariate time series based on empirical distribution functions
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%A Kojadinovic, Ivan
%A Verdier, Ghislain
%< avec comité de lecture
%@ 1935-7524
%J Electronic journal of statistics
%I Shaker Heights, OH : Institute of Mathematical Statistics
%V 15
%N 1
%P 773-829
%8 2021
%D 2021
%R 10.1214/21-EJS1798
%Z Mathematics [math]/Statistics [math.ST]Journal articles
%G English
%L hal-03125109
%U https://hal-univ-pau.archives-ouvertes.fr/hal-03125109
%~ UNIV-PAU
%~ LMA-PAU
%~ INSMI
%~ CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Subsampling (weighted smooth) empirical copula processes
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%A Kojadinovic, Ivan
%A Stemikovskaya, Kristina
%< avec comité de lecture
%@ 0047-259X
%J Journal of Multivariate Analysis
%I Elsevier
%V 173
%P 704-723
%8 2019-09
%D 2019
%R 10.1016/j.jmva.2019.05.007
%Z Mathematics [math]/Statistics [math.ST]Journal articles
%G English
%L hal-02369325
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02369325
%~ UNIV-PAU
%~ LMA-PAU
%~ INSMI
%~ CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Unpublished work
%T Semiparametric estimation of a mixture of two linear regressions in which one component is known
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Probabilité et Statistique
%A Bordes, Laurent
%A Kojadinovic, Ivan
%A Vandekerkhove, Pierre
%8 2013-01-15
%D 2013
%Z Mathematics [math]/Statistics [math.ST]
%Z Statistics [stat]/Statistics Theory [stat.TH]Preprints, Working Papers, ...
%X A new estimation method for the two-component mixture model introduced in \cite{Van12} is proposed. This model, which consists of a two-component mixture of linear regressions in which one component is entirely known while the proportion, the slope, the intercept and the error distribution of the other component are unknown, seems to be of interest for the analysis of large datasets produced from two-color ChIP-chip high-density microarrays. In spite of good performance for datasets of reasonable size, the method proposed in \cite{Van12} suffers from a serious drawback when the sample size becomes large, as it is based on the optimization of a contrast function whose pointwise computation requires $O(n^2)$ operations. The range of applicability of the method derived in this work is substantially larger as it is based on a method-of-moment estimator whose computation only requires $O(n)$ operations. From a theoretical perspective, the asymptotic normality of both the estimator of the Euclidean parameter vector and of the semiparametric estimator of the c.d.f.\ of the error is proved under weak conditions not involving the zero-symmetry assumption typically used this last decade. The finite-sample performance of the latter estimators is studied under various scenarios through Monte Carlo experiments. From a more practical perspective, the proposed method is applied to the tone data analyzed, among others, by \cite{HunYou12}, and to the ChIPmix data studied by \cite{MarMarBer08}. An extension of the considered model involving an unknown scale parameter for the first component is discussed in the final section.
%G English
%2 https://hal.archives-ouvertes.fr/hal-00796198/document
%2 https://hal.archives-ouvertes.fr/hal-00796198/file/mr.pdf
%L hal-00796198
%U https://hal.archives-ouvertes.fr/hal-00796198
%~ CNRS
%~ LMA-PAU
%~ INSMI
%~ CV_LAMA_UMR8050
%~ UPEC
%~ LAMA_UMR8050
%~ CV_UNIV-MLV
%~ UNIV-MLV
%~ CV_UPEC
%~ LAMA_PS
%~ UPEC-UPEM
%~ UNIV-PAU
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Distribution functions of linear combinations of lattice polynomials from the uniform distribution
%A Marichal, Jean-Luc
%A Kojadinovic, Ivan
%< avec comité de lecture
%@ 0167-7152
%J Statistics and Probability Letters
%I Elsevier
%V 78
%N 8
%P 985
%8 2010-02-23
%D 2010
%R 10.1016/j.spl.2007.09.059
%K Physical SciencesJournal articles
%G English
%2 https://hal.archives-ouvertes.fr/hal-00616536/document
%2 https://hal.archives-ouvertes.fr/hal-00616536/file/PEER_stage2_10.1016%252Fj.spl.2007.09.059.pdf
%L hal-00616536
%U https://hal.archives-ouvertes.fr/hal-00616536
%~ PEER

%0 Journal Article
%T Detecting distributional changes in samples of independent block maxima using probability weighted moments
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Laboratoire des Sciences du Climat et de l'Environnement [Gif-sur-Yvette] (LSCE)
%A Kojadinovic, Ivan
%A Naveau, P.
%Z ACL
%< avec comité de lecture
%@ 1386-1999
%J Extremes
%I Springer Verlag (Germany)
%V 20
%N 2
%P 417--450
%8 2017
%D 2017
%R 10.1007/s10687-016-0273-1
%Z Mathematics [math]Journal articles
%G English
%L hal-02148136
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02148136
%~ LMA-PAU
%~ UNIV-PAU
%~ INSMI
%~ UVSQ
%~ CNRS
%~ CEA
%~ CEA-UPSAY
%~ LSCE
%~ UVSQ-SACLAY
%~ CEA-UPSAY-SACLAY
%~ UNIV-PARIS-SACLAY
%~ INSU
%~ LSCE-CEA
%~ TESTUPPA2
%~ UPPA-OA
%~ GS-ENGINEERING
%~ GS-BIOSPHERA

%0 Journal Article
%T A review of methods for capacity identification in Choquet integral based multi-attribute utility theory: Applications of the Kappalab R package
%+ Centre d'économie de la Sorbonne (CES)
%+ Laboratoire d'Informatique de Nantes Atlantique (LINA)
%+ Lab-STICC_TB_CID_DECIDE
%A Grabisch, Michel
%A Kojadinovic, Ivan
%A Meyer, Patrick
%< avec comité de lecture
%@ 0377-2217
%J European Journal of Operational Research
%I Elsevier
%V 186
%N 2
%P 766-785
%8 2008-04
%D 2008
%R 10.1016/j.ejor.2007.02.025
%K Multi-criteria decision aiding
%K Multi-attribute utility theory
%K Choquet integral
%K Free software
%Z Humanities and Social Sciences/Economics and Finance
%Z Computer Science [cs]/Operations Research [cs.RO]Journal articles
%X The application of multi-attribute utility theory whose aggregation process is based on the Choquet integral requires the prior identification of a capacity. The main approaches to capacity identification proposed in the literature are reviewed and their advantages and inconveniences are discussed. All the reviewed methods have been implemented within the Kappalab R package. Their application is illustrated on a detailed example.
%G English
%2 https://halshs.archives-ouvertes.fr/halshs-00187175/document
%2 https://halshs.archives-ouvertes.fr/halshs-00187175/file/ejor06-kojameyer.pdf
%L halshs-00187175
%U https://halshs.archives-ouvertes.fr/halshs-00187175
%~ CNRS
%~ UNIV-PARIS1
%~ CES
%~ SHS
%~ AO-ECONOMIE
%~ UNIV-NANTES
%~ LINA
%~ INSTITUT-TELECOM
%~ LAB-STICC
%~ LAB-STICC_TB
%~ UNIV-UBS
%~ ENIB
%~ LAB-STICC_ENIB
%~ UNIV-BREST
%~ LAB-STICC_IMTA_CID_DECIDE
%~ INSTITUTS-TELECOM

