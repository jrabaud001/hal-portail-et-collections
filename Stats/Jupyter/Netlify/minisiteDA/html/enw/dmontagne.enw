%0 Master thesis
%T Les données localisées en accès libre sur la biodiversité en France : variation géographique de disponibilité et principales logiques de constitution
%+ Université Paris 1 Panthéon-Sorbonne - UFR Géographie (UP1 UFR08)
%A Montagne, Delphine
%P 101
%Y Pierre Gautreau
%Z Laurent Simon
%8 2011-07-05
%D 2011
%K Data about biodiversity
%K Open geographical data bank
%K Open data basis about environment
%K Environmental data dissemination
%K INSPIRE directive
%K Aarhus convention
%K Directive INSPIRE
%K Convention d’Aarhus
%K Diffusion de données sur l'environnement
%K Données sur la biodiversité
%K Bases de données géographiques en accès libre (BDGAL)
%K Bases de données sur l’environnement en accès libre (BDEAL)
%Z Humanities and Social Sciences/GeographyMaster thesis
%X This thesis offers a contribution to the ongoing global process of disseminating data from the INSPIRE project (European Commission, Aarhus). It seeks to identify the data freely available on biodiversity in ten French regions and to explain their disparities. To this end, the research has produced an inventory of sites from key word queries entered into search engines. In addition, it contributes an analysis of existing networks between the survey sites and interviews with broadcasters. The study proposes a typology of broadcasting sites, their analysis (e.g. maps and case studies) and lists of some of the factors contributing to the disparities between regions.
%X Inscrit dans un processus mondial de diffusion de la donnée (INSPIRE, Aarhus), ce mémoire cherche à inventorier les données disponibles en accès libre sur la biodiversité dans dix régions françaises et à en expliquer les disparités. À cette fin, la recherche a réalisé un inventaire des sites à partir de requêtes entrées sur un moteur de recherche, une analyse des réseaux existants entre les sites relevés et des entretiens auprès des diffuseurs. L’étude propose une typologie des sites diffuseurs, leur analyse (cartes et études de cas) et liste quelques facteurs de disparités entre régions.
%G French
%2 https://dumas.ccsd.cnrs.fr/dumas-01715395/document
%2 https://dumas.ccsd.cnrs.fr/dumas-01715395/file/M%C3%A9moire-M1-Delphine-Montagne-2011.pdf
%L dumas-01715395
%U https://dumas.ccsd.cnrs.fr/dumas-01715395
%~ AO-GEOGRAPHIE
%~ UNIV-PARIS1
%~ GIP-BE
%~ MEM-UNIV-PARIS1
%~ DUMAS

%0 Unpublished work
%T La tulipe noire de Chine
%+ Passages
%A Montagne, Delphine
%9 Article de vulgarisation sur la première carte sino-centrée réalisée par Matteo Ricci
%8 2010-02-14
%D 2010
%K carte
%K Chine
%K Matteo Ricci
%Z Humanities and Social Sciences/Geography
%Z Humanities and Social Sciences/HistoryOther publications
%X Histoire de la première carte du monde sino-centrée par Matteo Ricci, connue sous le terme de "Carte générale des Monts, des Mers et des Continents"
%G French
%2 https://hal.archives-ouvertes.fr/hal-01686025/document
%2 https://hal.archives-ouvertes.fr/hal-01686025/file/2011_Article_culture_scientifique_asso_oekoumene_Matteo_ricci.pdf
%L hal-01686025
%U https://hal.archives-ouvertes.fr/hal-01686025
%~ CNRS
%~ UNIV-PAU
%~ AO-GEOGRAPHIE
%~ AO-HISTOIRE
%~ HISTOIRE
%~ GIP-BE
%~ UMR5319
%~ SHS
%~ UNIV-BORDEAUX-MONTAIGNE
%~ PASSAGES-UPPA
%~ TESTUPPA2
%~ UPPA-OA

%0 Unpublished work
%T An Pombie's mountain hut history
%+ Passages
%A Montagne, Delphine
%9 Posters présentés dans le cadre d'une exposition pour les 50 ans du refuge de Pombie
%8 2017-06-25
%D 2017
%K refuge
%K Club Alpin Français
%K CAF
%K Pombie
%K Ossau
%K moutain hut
%K Pyrénées
%K Pyrénées Occidentales
%K Pombie
%K montagne
%K Pyrénées France
%Z Humanities and Social Sciences/Architecture, space management
%Z Humanities and Social Sciences/Geography
%Z Humanities and Social Sciences/HistoryOther publications
%X Posters from the "history" part of the exhibition on the Pombie mountain hut. Through the geographical context, the architecture, the different uses over time and statistics, the posters retrace the history of the first refuge and the current second mountain hut of Pombie.
%X Posters issus de la partie "histoire" de l'exposition sur le refuge de Pombie. A travers le contexte géographique, l'architecture, les différents usages au cours du temps et des statistiques, les posters retracent l’histoire du premier refuge et de l'actuel second refuge de Pombie.
%G French
%Z Club Alpin Français de Pau et de la Vallée d'Ossau
%2 https://hal.archives-ouvertes.fr/hal-01698170/document
%2 https://hal.archives-ouvertes.fr/hal-01698170/file/Pombie_affiche_HAL.pdf
%L hal-01698170
%U https://hal.archives-ouvertes.fr/hal-01698170
%~ CNRS
%~ UNIV-PAU
%~ ARCHITECTURE
%~ AO-ARCHITECTURE
%~ AO-HISTOIRE
%~ AO-GEOGRAPHIE
%~ HISTOIRE
%~ GIP-BE
%~ UMR5319
%~ SHS
%~ UNIV-BORDEAUX-MONTAIGNE
%~ PASSAGES-UPPA
%~ TESTUPPA2
%~ UPPA-OA

%0 Conference Paper
%F Oral 
%T Se référer au passé, se projeter dans l’avenir. Analyses textuelles et fouille spatiale pour appréhender les horizons temporels des contributions au Grand et au Vrai Débat
%+ Passages
%A Deletraz, Gaëlle
%A Le Campion, Grégoire
%A Montagne, Delphine
%A Pierson, Julie
%A Pissoat, Olivier
%< avec comité de lecture
%B Journées d’études "Quels outils pour appréhender et analyser les mobilisations de Gilets Jaunes et les données issues du Débat national ?"
%C Paris, France
%8 2020-01-16
%D 2020
%Z Humanities and Social Sciences/Geography
%Z Humanities and Social SciencesConference papers
%X Se référer au passé, se projeter dans l'avenir. Analyses textuelles et fouille spatiale pour appréhender les horizons temporels des contributions au Grand et au Vrai Débat.
%G English
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-02444836/document
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-02444836/file/Pass%C3%A9-avenir-Grand-D%C3%A9bat-2020.pdf
%L hal-02444836
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02444836
%~ UNIV-PAU
%~ PASSAGES-UPPA
%~ SHS
%~ CNRS
%~ UNIV-BORDEAUX-MONTAIGNE
%~ GIP-BE
%~ UMR5319
%~ AO-GEOGRAPHIE
%~ TESTUPPA2
%~ UPPA-OA

%0 Report
%T Décideurs et Citoyens dans un contexte urbain de Signaux Faibles (Décisif). Rapport final. Ademe, APR TEES : Transition écologique économique et sociale
%+ Université de Pau et des Pays de l'Adour (UPPA)
%+ Transitions Energétiques et Environnementales (TREE)
%+ Passages
%+ Association Ecocène
%+ APESA [Pau]
%A Bouisset, Christine
%A Arnauld De Sartre, Xavier
%A Baggioni, Vincent
%A Bousquet, Aurélie
%A Cousteau, Evelyne
%A Daléas, Jacques
%A Degrémont, Isabelle
%A Deletraz, Gaëlle
%A Douence, Hélène
%A Khamis, Rim
%A Laplace-Treyture, Danièle
%A Montagne, Delphine
%A Pottier, Aude
%A Silva, Magali
%A Vaucelle, Sandrine
%A Wast, Marie
%Z Ademe, Université de Pau et des Pays de l'Adour (UPPA), Agglomération Pau Béarn Pyrénées
%I Ademe
%8 2021-02-15
%D 2021
%K Transition écologique
%K Changement climatique
%K échelle
%K gestion
%Z Humanities and Social Sciences/GeographyReports
%G French
%2 https://hal.archives-ouvertes.fr/hal-03207109/document
%2 https://hal.archives-ouvertes.fr/hal-03207109/file/Decisif_rapport_final-light2.pdf
%L hal-03207109
%U https://hal.archives-ouvertes.fr/hal-03207109
%~ UMR6031TREE
%~ AO-GEOGRAPHIE
%~ UNIV-PAU
%~ GIP-BE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ UMR5319
%~ CNRS
%~ PASSAGES-UPPA
%~ LARA
%~ TESTUPPA2
%~ SHS
%~ UPPA-OA

%0 Journal Article
%T Fédération de botanistes, synthèse de contributions citoyennes et transfert de la connaissance naturaliste L'exemple de l'association Tela Botanica
%+ Passages
%A Montagne, Delphine
%< avec comité de lecture
%@ 0987-6014
%J NETCOM : Réseaux, communication et territoires / Networks and Communications Studies
%I Netcom Association
%S Les données environnementales en libre accès : politiques, expériences, usages
%V 27
%N 1-2
%8 2013
%D 2013
%R 10.4000/netcom.1312
%K Botanical network
%K francophone network
%K crowdsourcing
%K naturalist community
%K Réseau botanique
%K réseau francophone
%K communauté naturaliste
%Z Humanities and Social Sciences/Geography
%Z Humanities and Social Sciences/Environmental studiesJournal articles
%X Tela Botanica is an association dedicated to botany. It relies on the Internetand new technologies to develop a French naturalist network. This note recalls its construction,technical and organizational features. Which have enabled the success and development of multithematicprojects in the service of naturalist knowledge.
%X Tela Botanica est une association dédiée à la botanique. Elle s’appuie surInternet et les nouvelles technologies pour développer un réseau naturaliste francophone. Cette noterappelle sa construction, ses particularités techniques et organisationnelles. Ces dernières ont permisson succès et le développement de projets pluri-thématiques au service de la connaissance naturaliste.
%G French
%2 https://hal.archives-ouvertes.fr/hal-01443619/document
%2 https://hal.archives-ouvertes.fr/hal-01443619/file/netcom-1312.pdf
%L hal-01443619
%U https://hal.archives-ouvertes.fr/hal-01443619
%~ CNRS
%~ UNIV-PAU
%~ AO-GEOGRAPHIE
%~ SHS
%~ GIP-BE
%~ UMR5319
%~ UNIV-BORDEAUX-MONTAIGNE
%~ PASSAGES-UPPA
%~ TESTUPPA2
%~ UPPA-OA

%0 Unpublished work
%T The map and the captives
%+ Passages
%A Montagne, Delphine
%9 A la Maison du Parc National des Écrins se trouve une maquette du massif des Écrins. Elle a été réalisée par des prisonniers de guerre 39-45, dans l'OFLAG IV-D à Hoyerswerda (Allemagne). Cette note de blog retrace l'histoire de cette maquette.
%8 2010
%D 2010
%K carte
%K maquette
%K oflag
%K oflag IVD
%K Ecrins
%K Vallouise
%K Elsterhorst
%K Hoyerswerda
%Z Humanities and Social Sciences/Geography
%Z Humanities and Social Sciences/HistoryOther publications
%X A la Maison du Parc National des Ecrins se trouve une maquette du massif des Écrins. Elle a été réalisée par des prisonniers de guerre 39-45, dans l'OFLAG IV-D. Cette note de blog retrace l'histoire de cette maquette.
%G French
%2 https://hal.archives-ouvertes.fr/hal-01686022/document
%2 https://hal.archives-ouvertes.fr/hal-01686022/file/2011_Article_culture_scientifique_asso_oekoumene_carte_et_prisonniers.pdf
%L hal-01686022
%U https://hal.archives-ouvertes.fr/hal-01686022
%~ CNRS
%~ UNIV-PAU
%~ AO-HISTOIRE
%~ AO-GEOGRAPHIE
%~ HISTOIRE
%~ GIP-BE
%~ UMR5319
%~ SHS
%~ UNIV-BORDEAUX-MONTAIGNE
%~ PASSAGES-UPPA
%~ TESTUPPA2
%~ UPPA-OA

%0 Audiovisual Material
%T Replim Panticosa Valley 2017
%+ Passages
%A Montagne, Delphine
%Z Interreg POCTEFA
%8 2017-10
%D 2017
%K climate change
%K REPLIM
%K changement climatique
%K mesure chimique
%Z Humanities and Social Sciences/Environmental studiesVideos
%X From the start, from the logistics to the manipulations on the lakes, glimpsed in pictures and music of the #ResearchLearn REPLIM. REPLIM, it is a European program bringing together about thirty researchers, engineers and PhD students from three countries (France, Spain, Andorra) . One field: the Pyrenees. The goal? Studying climate change in high altitude lakes and peat bogs, environments that are very sensitive to climate change. To study, you have to collect data. Back on the mission.
%X Du départ, de la logistique aux manipulations sur les lacs, aperçu tout en images et en musique du #TerainDeRecherche REPLIM. REPLIM, c'est un programme européen rassemblant une trentaine de chercheurs, ingénieurs et doctorants de trois pays (France, Espagne, Andorre). Un seul terrain : les Pyrénées. L'objectif ? Étudier le changement climatique dans les lacs et tourbières d'altitude, des milieux très sensibles aux évolutions du climat. Pour pouvoir étudier, il faut collecter des données. Retour sur la mission.
%G French
%2 https://halshs.archives-ouvertes.fr/medihal-01810364/document
%2 https://halshs.archives-ouvertes.fr/medihal-01810364/file/REPLIMV3.mp4
%L medihal-01810364
%U https://halshs.archives-ouvertes.fr/medihal-01810364
%~ SHS
%~ CNRS
%~ UNIV-PAU
%~ GIP-BE
%~ UMR5319
%~ UNIV-BORDEAUX-MONTAIGNE
%~ PASSAGES-UPPA
%~ OPENAIRE
%~ TESTUPPA2

%0 Unpublished work
%T Exploration of the website Navigae: treasure map of Colombia
%+ Passages
%A Montagne, Delphine
%9 Article de blog en libre accès recensant quelques trésors du fonds "Colombie" dont les cartes ont été géoréférencées lors de la mise en ligne sur la plateforme Navigae.
%8 2018-10-03
%D 2018
%K map
%K Colombia
%K carte
%K Colombie
%Z Humanities and Social Sciences/Geography
%Z Humanities and Social Sciences/History
%Z Humanities and Social Sciences/Architecture, space managementOther publications
%X This article offers an exploration of the map database on Colombia posted on the Navigae website in the form of notes for researchers / professors and examples of the possibilities of the website itself.
%X Cet article propose une exploration du fonds de cartes sur la Colombie mis en ligne sur le site web Navigae sous forme de notes pour les chercheurs/ professeurs et des exemples des possibilités du site internet en lui-même.
%G French
%Z Consortium ImaGEO
%2 https://hal.archives-ouvertes.fr/hal-01888076/document
%2 https://hal.archives-ouvertes.fr/hal-01888076/file/Exploration_Navigae_Imageo_2018_10_03.pdf
%L hal-01888076
%U https://hal.archives-ouvertes.fr/hal-01888076
%~ CNRS
%~ SHS
%~ UMR5319
%~ GIP-BE
%~ HISTOIRE
%~ AO-GEOGRAPHIE
%~ AO-HISTOIRE
%~ ARCHITECTURE
%~ AO-ARCHITECTURE
%~ UNIV-PAU
%~ UNIV-BORDEAUX-MONTAIGNE
%~ PASSAGES-UPPA
%~ TESTUPPA2

