%0 Journal Article
%T Effects of aboveground grazing on coupling among nitrifier activity, abundance and community structure
%+ Laboratoire d'Ecologie Microbienne - UMR 5557 (LEM)
%+ Macaulay Institute
%+ GSF National Research Center for Environment and Health
%+ School of Biological Sciences
%+ Unité de recherche d'Agronomie (UA)
%A Le Roux, Xavier
%A Poly, Franck
%A Currey, Pauline
%A Commeaux, Claire
%A Hai, Brigitte
%A W Nicol, Graeme
%A I Prosser, James
%A Schloter, Michael
%A Attard, Eléonore
%A Klumpp, Katja
%< avec comité de lecture
%@ 1751-7362
%J ISME Journal
%I Nature Publishing Group
%V 2
%N 2
%P 221-232
%8 2008
%D 2008
%R 10.1038/ismej.2007.109
%M 18049458
%K microbial diversity–functioning relationship
%K nitrogen cycle
%K herbivory
%K ammonia-oxidizing archeae
%K ammonia-oxidizing bacteria
%K ammonia oxidizers
%Z Environmental Sciences/Biodiversity and Ecology
%Z Life Sciences [q-bio]/Biodiversity
%Z Life Sciences [q-bio]/Ecology, environmentJournal articles
%X The influence of switches in grassland management to or from grazing on the dynamics of nitrifier activity, as well as the abundance of ammonia-oxidizing bacteria, AOB and ammonia-oxidizing archeae, AOA, was analyzed for two years after changing management. Additionally community structure of AOB was surveyed. Four treatments were compared in mesocosms: grazing on previously grazed grassland (G-G); no grazing on ungrazed grassland (U-U); grazing on ungrazed grassland (U-G) and cessation of grazing on grazed grassland (G-U). Nitrifier activity and abundance were always higher for G-G than U-U treatments and AOB community structure differed between these treatments. AOA abundance was in the same range as AOB abundance and followed the same trend. Grazing led to a change in AOB community structure within <5 months and a subsequent (5–12 months) increase in nitrifier activity and abundance. In contrast, cessation of grazing led to a decrease in nitrifier activity and abundance within <5 months and to a later (5–12 months) change in AOB community structure. Activity in G-U and U-G was similar to that in U-U and G-G, respectively, after 12 months. Sequence analysis of 16S rRNA gene clones showed that AOB retrieved from soils fell within the Nitrosospira lineage and percentages of AOB related to known Nitrosospira groups were affected by grazing. These results demonstrate that AOB and AOA respond quickly to changes in management. The selection of nitrifiers adapted to novel environmental conditions was a prerequisite for nitrification enhancement in U-G, whereas nitrification decrease in G-U was likely due to a partial starvation and decrease in the abundance of nitrifiers initially present. The results also suggest that taxonomic affiliation does not fully infer functional traits of AOB.
%G English
%L halsde-00345209
%U https://hal.archives-ouvertes.fr/halsde-00345209
%~ SDE
%~ CNRS
%~ INRA
%~ GIP-BE
%~ BIOENVIS
%~ UNIV-LYON1
%~ UDL
%~ UNIV-LYON
%~ INRAE
%~ ECOMIC
%~ AGREENIUM

%0 Thesis
%T Etude de la réponse des communautés bactériennes du sol aux changements des modes de gestion dans les agrosystèmes
%+ Agrosystèmes et impacts environnementaux carbone-azote (Agro-Impact)
%A Attard, Eléonore
%Z Diplôme : Dr. d'Université
%P 167 p.
%I Université de Poitiers
%Y Amblès
%Y Leroux
%Y Recous
%8 2008
%D 2008
%K these
%K these
%Z Life Sciences [q-bio]
%Z Environmental SciencesTheses
%G French
%2 https://hal.inrae.fr/tel-02818108/document
%2 https://hal.inrae.fr/tel-02818108/file/49469_20120301022727416_1.pdf
%L tel-02818108
%U https://hal.inrae.fr/tel-02818108
%~ INRAE
%~ INRA
%~ SDE
%~ GIP-BE
%~ AGREENIUM

%0 Conference Paper
%T Cloud condensation and ice nucleation activity of bacteria isolated from cloud water
%+ Karlsruhe Institute of Technology (KIT)
%+ Université d'Auvergne - Clermont-Ferrand I (UdA)
%+ Institute for Atmospheric and Climate Science [Zürich] (IAC)
%+ Unité de Pathologie Végétale (PV)
%+ Institute of Environmental Physics [Heidelberg] (IUP)
%A Oehm, Caroline
%A Attard, Eleonore
%A Chou, Cédric
%A Stetzer, Olaf
%A Delort, Anne-Marie
%A Morris, Cindy E.
%A Möhler, Ottmar
%A Leisner, Thomas
%< sans comité de lecture
%B 25. International Union of Geodesy and Geophysics (IUGG) General Assembly
%C Melbourne, Australia
%S Earth on the edge: science for a sustainable planet
%8 2011-06-28
%D 2011
%K CHAMBER AIDA
%K METHODOLOGIE
%K ICE-NUCLEATION
%K AEROSOL
%K CLOUD
%Z Life Sciences [q-bio]Conference papers
%G English
%2 https://hal.inrae.fr/hal-02748417/document
%2 https://hal.inrae.fr/hal-02748417/file/45372_20110719114753769_1.pdf
%L hal-02748417
%U https://hal.inrae.fr/hal-02748417
%~ INRAE
%~ INRA
%~ AGROPOLIS
%~ AGREENIUM
%~ UNIV-CLERMONT1
%~ PRES_CLERMONT
%~ PV

%0 Journal Article
%T Survival and ice nucleation activity of bacteria as aerosol in a cloud simulation chamber.
%+ Institut de Chimie de Clermont-Ferrand (ICCF)
%+ Laboratoire de météorologie physique (LaMP)
%+ Institute for Meteorology and Climate Research (IMK)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Unité de Pathologie Végétale (PV)
%+ Interactions Sol Plante Atmosphère (UMR ISPA)
%A Amato, Pierre
%A Joly, M.
%A Schaupp, C.
%A Attard, Eléonore
%A Möhler, Ottmar
%A Morris, Cindy E.
%A Brunet, Y.
%A Delort, A.-M.
%Z DFG-CNRS project “BIOCLOUDS” 10 (DFG contract MO 668/2-1) and international program EUROCHAMP (Integration of Eu- ropean Simulation Chambers for Investigating Atmospheric Processes)
%Z Version finale révisée de : Amato, P., Joly, M., Schaupp, C., Attard, E., Möhler, O., Morris, C. E., Brunet, Y.., and Delort, A.-M.: Survival and ice nucleation activity of bacteria as aerosols in a cloud simulation chamber, Atmos. Chem. Phys. Discuss., 15, 4055-4082, doi:10.5194/acpd-15-4055-2015, 2015.
%< avec comité de lecture
%@ 1680-7316
%J Atmospheric Chemistry and Physics
%I European Geosciences Union
%V 15
%N 11
%P 6455-6465
%8 2015
%D 2015
%R 10.5194/acp-15-6455-2015
%Z Chemical Sciences
%Z Life Sciences [q-bio]/Microbiology and Parasitology/Bacteriology
%Z Environmental Sciences/Biodiversity and EcologyJournal articles
%X The residence time of bacterial cells in the atmosphere is predictable by numerical models. However, estimations of their aerial dispersion as living entities are limited by a lack of information concerning survival rates and behavior in relation to atmospheric water. Here we investigate the viability and ice nucleation (IN) activity of typical atmospheric ice nucleation active bacteria (Pseudomonas syringae and P. fluorescens) when airborne in a cloud simulation chamber (AIDA, Karlsruhe, Germany). Cell suspensions were sprayed into the chamber and aerosol samples were collected by impingement at designated times over a total duration of up to 18 h, and at some occasions after dissipation of a cloud formed by depressurization. Aerosol concentration was monitored simultaneously by online instruments. The cultivability of airborne cells decreased exponentially over time with a half-life time of 250 ± 30 min (about 3.5 to 4.5 h). In contrast, IN activity remained unchanged for several hours after aerosolization, demonstrating that IN activity was maintained after cell death. Interestingly, the relative abundance of IN active cells still airborne in the chamber was strongly decreased after cloud formation and dissipation. This illustrates the preferential precipitation of IN active cells by wet processes. Our results indicate that from 106 cells aerosolized from a surface, one would survive the average duration of its atmospheric journey estimated at 3.4 days. Statistically, this corresponds to the emission of 1 cell that achieves dissemination every ~ 33 min m−2 of cultivated crops fields, a strong source of airborne bacteria. Based on the observed survival rates, depending on wind speed, the trajectory endpoint could be situated several hundreds to thousands of kilometers from the emission source. These results should improve the representation of the aerial dissemination of bacteria in numeric models.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01206705/document
%2 https://hal.archives-ouvertes.fr/hal-01206705/file/acp-15-6455-2015.pdf
%L hal-01206705
%U https://hal.archives-ouvertes.fr/hal-01206705
%~ INSU
%~ CNRS
%~ UNIV-PAU
%~ INRA
%~ ACL-SF
%~ UNIV-BPCLERMONT
%~ SIGMA-CLERMONT
%~ PRES_CLERMONT
%~ IPREM
%~ AGREENIUM
%~ INC-CNRS
%~ ICC
%~ LAMP
%~ IPREM-CME
%~ INRAE
%~ SDE
%~ AGROPOLIS
%~ ACL-SVSAE
%~ GIP-BE
%~ TESTUPPA2
%~ UPPA-OA
%~ PV

%0 Journal Article
%T Impacts de changements d’occupation et de gestion des sols sur la dynamique des matières organiques, les communautés microbiennes et les flux de carbone et d’azote
%+ Laboratoire d'Ecologie Microbienne - UMR 5557 (LEM)
%+ Université Claude Bernard Lyon 1 (UCBL)
%+ ARVALIS - Institut du végétal [Paris]
%+ Unité de Recherche Pluridisciplinaire Prairies et Plantes Fourragères (P3F)
%+ Biologie et Gestion des Adventices (BGA)
%+ Fractionnement des AgroRessources et Environnement (FARE)
%A Attard, Eléonore
%A Le Roux, Xavier
%A Laurent, F.
%A Chabbi, Abad
%A Nicolardot, Bernard
%A Recous, Sylvie
%< avec comité de lecture
%@ 1252-6851
%J Etude et Gestion des Sols
%I Association Française pour l'Etude des Sols
%V 18
%N 3
%P 147-159
%8 2011
%D 2011
%K labour
%K minéralisation de l’azote
%K carbone organique du sol
%K communauté microbienne
%K dénitrification
%K semis direct
%K occupation des sols
%K nitrification
%K prairie
%Z Life Sciences [q-bio]/Agricultural sciences/Soil studyJournal articles
%X Les modes d’occupation des sols et de gestion des terres ont des effets importants sur la nature des restitutions organiques et, par les techniques culturales qui en découlent, sur un certain nombre de facteurs qui affectent les cycles biogéochimiques et les communautés microbiennes du sol. Dans ce contexte, le projet COSMOS-flux (programme GESSOL2), avait pour objectif d’étudier deux premiers cas d’évolution de la gestion des sols : la conversion labour-non labour (étudiée sur le site Arvalis de Boigneville, Essonne, sur des parcelles en semis direct depuis 14 ans) et la conversion prairie-rotation de cultures annuelles étudiée sur le site INRA de l’ORE de Lusignan (Vienne), dans le cadre de la problématique d’introduction de prairies temporaires dans les rotations. Les évolutions constatées pendant 18 à 36 mois après l’application des changements ont été suivies à la fois sur les compartiments organiques des couches superficielles du sol (0-20 et 0-30 cm pour Boigneville et Lusignan, respectivement), sur les biotransformations de l’azote (minéralisation, organisation, nitrification) et sur les communautés microbiennes impliquées dans la nitrification (nitratants) et la dénitrification en termes d’activité, d’effectif et de diversité. Les résultats obtenus ont tout d’abord confirmé des situations initiales bien contrastées du point de vue des compartiments organiques et des activités microbiennes et flux d’azote entre sol sous prairie âgée de 5ans et sol en rotation de cultures annuelles d’une part, sol en semis direct et sol labouré d’autre part. Ces contrastes sont dus à l’accumulation significative de matière organique en surface. Les seconds cas concernent la conversion réciproque, pour lesquels nous avons observé que le travail du sol appliqué sur les parcelles en semis direct (Boigneville) ou pour détruire la prairie (Lusignan) constitue une perturbation très forte ; celleci conduit à l’évolution très rapide des caractéristiques des compartiments organiques et activités microbiennes vers celles observées dans les sols annuellement labourés ou en rotation de cultures annuelles. Par contre, les changements de pratique inverses (passage du labour au non labour ou implantation d’une prairie) ne conduisent pas ou peu à l’évolution des caractéristiques du sol à l’échelle de 2 à 3 ans. Les résultats obtenus permettent aussi de discuter la hiérarchie des facteurs expliquant la réponse de la nitrification et de la dénitrification. La diversité des communautés microbiennes semble mal expliquer les niveaux d’activité. Les effectifs expliquent bien les changements de niveaux de nitrification, mais pas ceux de dénitrification. Enfin, le carbone organique est la variable environnementale qui joue le rôle de facteur-clé pour les réponses observées sur la dénitrification.
%G French
%2 https://hal.inrae.fr/hal-02648639/document
%2 https://hal.inrae.fr/hal-02648639/file/50742_20120210120721919_1.pdf
%L hal-02648639
%U https://hal.inrae.fr/hal-02648639
%~ INRAE
%~ UNIV-PICARDIE
%~ UNIV-BOURGOGNE
%~ INRA
%~ BIOENVIS
%~ GIP-BE
%~ AGREENIUM
%~ UNIV-LYON1
%~ URCA
%~ FARE
%~ CNRS
%~ UNIV-LYON
%~ UDL
%~ ECOMIC
%~ U-PICARDIE

%0 Journal Article
%T Grazing triggers soil carbon loss by altering plant roots and their control on soil microbial community
%+ UR 0874 Unité de recherche sur l'Ecosystème Prairial 
%+ Laboratoire d'Ecologie Microbienne - UMR 5557 (LEM)
%+ Max Planck Institute for Biogeochemistry (MPI-BGC)
%A Klumpp, Katja
%A Fontaine, Sébastien
%A Attard, Eléonore
%A Le Roux, Xavier
%A Gleixner, Gerd
%A Soussana, Jean-François, J.-F.
%< avec comité de lecture
%@ 0022-0477
%J Journal of Ecology
%I Wiley
%V 97
%N 5
%P 876-885
%8 2009
%D 2009
%R 10.1111/j.1365-2745.2009.01549.x
%K carbon cycling
%K grassland
%K microbial community
%K nitrogen cycling
%K particulate organic
%K PLFA
%K disturbance
%K ARISA
%K decomposition
%K management
%K matter
%Z Life Sciences [q-bio]
%Z Environmental SciencesJournal articles
%X 1. Depending on grazing intensity, grasslands tend towards two contrasting systems that differ in terms of species diversity and soil carbon (C) storage. To date, effects of grazing on C cycling have mainly been studied in grasslands subject to constant grazing regimes, whereas little is known for grasslands experiencing a change in grazing intensity. Analysing the transition between C-storing and C-releasing grasslands under low- and high-grazing regimes, respectively, will help to identify key plant-soil interactions for C cycling. 2. The transition was studied in a mesocosm experiment with grassland monoliths submitted to a change in grazing after 14 years of constant high and low grazing. Plant-soil interactions were analysed by following the dynamics of plant and microbial communities, roots and soil organic matter fractions over 2 years. After disturbance change, mesocosms were continuously exposed to (13)C-labelled CO(2), which allowed us to trace both the incorporation of new litter C produced by a modified plant community in soil and the fate of old unlabelled litter C. 3. Changing disturbance intensity led to a cascade of events. After shift to high disturbance, photosynthesis decreased followed by a decline in root biomass and a change in plant community structure 1.5 months later. Those changes led to a decrease of soil fungi, a proliferation of Gram(+) bacteria and accelerated decomposition of old particulate organic C (< 6 months). At last, accelerated decomposition released plant available nitrogen and decreased soil C storage. Our results indicate that intensified grazing triggers proliferation of Gram(+) bacteria and subsequent faster decomposition by reducing roots adapted to low disturbance. 4. Synthesis. Plant communities exert control on microbial communities and decomposition through the activity of their living roots: slow-growing plants adapted to low disturbance reduce Gram(+) bacteria, decomposition of low and high quality litter, nitrogen availability and, thus, ingress of fast-growing plants. Our results indicate that grazing impacts on soil carbon storage by altering plant roots and their control on the soil microbial community and decomposition, and that these processes will foster decomposition and soil C loss in more productive and disturbed grassland systems.
%G English
%L hal-02666383
%U https://hal.inrae.fr/hal-02666383
%~ INRAE
%~ INRA
%~ SDE
%~ GIP-BE
%~ UNIV-LYON1
%~ CNRS
%~ UDL
%~ UNIV-LYON
%~ BIOENVIS
%~ ECOMIC

%0 Journal Article
%T Survival and ice nucleation activity of bacteria as aerosols in a cloud simulation chamber
%+ Institut de Chimie de Clermont-Ferrand (ICCF)
%+ Laboratoire de Météorologie Physique (LaMP)
%+ Karlsruhe Institute of Technology (KIT)
%A Amato, P.
%A Joly, M.
%A Schaupp, C.
%A Attard, E.
%A Möhler, O.
%A Morris, C.
%A Brunet, Y..
%A Delort, A.-M.
%< avec comité de lecture
%@ 1680-7367
%J Atmospheric Chemistry and Physics Discussions
%I European Geosciences Union
%V 15
%N 3
%P 4055 - 4082
%8 2015
%D 2015
%R 10.5194/acpd-15-4055-2015
%Z Physics [physics]
%Z Environmental Sciences/Biodiversity and Ecology
%Z Life Sciences [q-bio]/Ecology, environment/EcosystemsJournal articles
%G English
%L hal-01898951
%U https://hal.archives-ouvertes.fr/hal-01898951
%~ CNRS
%~ SDE
%~ ICC
%~ GIP-BE
%~ SIGMA-CLERMONT
%~ INC-CNRS
%~ PRES_CLERMONT
%~ INSU
%~ LAMP

%0 Journal Article
%T Effects of atmospheric conditions on ice nucleation activity of <em>Pseudomonas</em>
%+ Université d'Auvergne - Clermont-Ferrand I (UdA)
%+ Institut de Chimie de Clermont-Ferrand (ICCF)
%+ Max-Planck-Gesellschaft
%+ Unité de Pathologie Végétale (PV)
%+ Universität Bielefeld = Bielefeld University
%A Attard, Eleonore
%A Yang, H.
%A Delort, Anne-Marie
%A Amato, Pierre
%A Poschl, Ulrich
%A Glaux, Catherine
%A Koop, T.
%A Morris, Cindy E.
%< avec comité de lecture
%@ 1680-7316
%J Atmospheric Chemistry and Physics
%I European Geosciences Union
%V 12
%P 10667-10677
%8 2012
%D 2012
%R 10.5194/acp-12-10667-2012
%Z Life Sciences [q-bio]Journal articles
%G English
%2 https://hal.inrae.fr/hal-02652669/document
%2 https://hal.inrae.fr/hal-02652669/file/2012-Attard-Atmos%20Chem%20Phys_1.pdf
%L hal-02652669
%U https://hal.inrae.fr/hal-02652669
%~ INRAE
%~ INRA
%~ ICC
%~ AGROPOLIS
%~ UNIV-BPCLERMONT
%~ ACL-SVSAE
%~ SIGMA-CLERMONT
%~ AGREENIUM
%~ UNIV-CLERMONT1
%~ INC-CNRS
%~ CNRS
%~ PRES_CLERMONT
%~ PV

%0 Journal Article
%T Medium-term effects of Ag supplied directly or via sewage sludge to an agricultural soil on Eisenia fetida earthworm and soil microbial communities
%+ Laboratoire de Génie Civil et Géo-Environnement (LGCgE) - ULR 4515 (LGCgE)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Centre européen de recherche et d'enseignement des géosciences de l'environnement (CEREGE)
%+ Laboratoire d'Ecologie Microbienne - UMR 5557 (LEM)
%A Courtois, Pauline
%A Rorat, Agnieszka
%A Lemiere, Sébastien
%A Guyoneaud, Remy
%A Attard, Eléonore
%A Longepierre, Manon
%A Rigal, François
%A LEVARD, Clement
%A Chaurand, Perrine
%A Grosser, Anna
%A Grobelak, Anna
%A Kacprzak, Malgorzata
%A Lors, Christine
%A Richaume, Agnes
%A Vandenbulcke, Franck
%Z ANSES
%< avec comité de lecture
%@ 0045-6535
%J Chemosphere
%I Elsevier
%V 269
%P 128761
%8 2021-04
%D 2021
%R 10.1016/j.chemosphere.2020.128761
%K earthworms
%K ecotoxicology
%K silver sulfide
%K Silver nanoparticles
%K microorganisms
%K speciation
%Z Environmental Sciences/Biodiversity and EcologyJournal articles
%X The widespread use of silver nanoparticles (AgNPs) in consumer products that release Ag throughout their life cycle has raised potential environmental concerns. AgNPs primarily accumulate in soil through the spreading of sewage sludge (SS). In this study, the effects of direct exposure to AgNPs or indirect exposure via SS contaminated with AgNPs on the earthworm Eisenia fetida and soil microbial communities were compared, through 3 scenarios offering increasing exposure concentrations. The effects of Ag speciation were analyzed by spiking SS with AgNPs or AgNO3 before application to soil. SS treatment strongly impacted Ag speciation due to the formation of Ag2S species that remained sulfided after mixing in the soil. The life traits and expression of lysenin, superoxide dismutase, cd-metallothionein genes in earthworms were not impacted by Ag after 5 weeks of exposure, but direct exposure to Ag without SS led to bioaccumulation of Ag, suggesting transfer in the food chain. Ag exposure led to a decrease in potential carbon respiration only when directly added to the soil. The addition of SS had a greater effect on soil microbial diversity than the form of Ag, and the formation of Ag sulfides in SS reduced the impact of AgNPs on E. fetida and soil microorganisms compared with direct addition.
%G English
%2 https://hal.archives-ouvertes.fr/hal-03149939/document
%2 https://hal.archives-ouvertes.fr/hal-03149939/file/CHEM76031_Courtois%20et%20al.pdf
%L hal-03149939
%U https://hal.archives-ouvertes.fr/hal-03149939
%~ GIP-BE
%~ PSL
%~ BIOENVIS
%~ CDF-PSL
%~ UNIV-LYON1
%~ UNIV-LILLE
%~ IMT-LILLE-DOUAI
%~ UNIV-ARTOIS
%~ CNRS
%~ UDL
%~ UNIV-LYON
%~ INSU
%~ CEREGE
%~ SDE
%~ INC-CNRS
%~ INSTITUT-TELECOM
%~ IPREM
%~ OSU-INSTITUT-PYTHEAS
%~ UNIV-PAU
%~ AGREENIUM
%~ INRAE
%~ INSTITUTS-TELECOM
%~ UNIV-AMU
%~ CDF
%~ INRA
%~ ED
%~ IPREM-CME
%~ ECOMIC-EQUIPE5
%~ ECOMIC
%~ TESTUPPA2
%~ IRD
%~ UPPA-OA
%~ LGCGE

