%0 Journal Article
%T Promouvoir l’apprentissage des langues en Ecosse : Exemple d’un projet scolaire qui valorise les identités plurilingues
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Pedley, Malika
%< avec comité de lecture
%@ 2417-4211
%J Essais : revue interdisciplinaire d'Humanités
%I École doctorale Montaigne-Humanités/Presses universitaires de Bordeaux
%S Plurilinguismes en construction
%N 14
%P 117-133
%8 2018
%D 2018
%R 10.4000/essais.346
%K language policies
%K education
%K plurilingualism
%K multilingualism
%K creative writing
%K politiques linguistiques
%K éducation
%K plurilinguisme
%K multilinguisme
%K écriture créative
%Z Humanities and Social Sciences/LinguisticsJournal articles
%X This paper precedes fieldwork conducted in Scotland between 2015 and 2016. 'Mother Tongue Other Tongue' is a multilingual poetry competition which aims to promote language learning at school. By exposing and valuing local multilingual reality, all languages included, our hypothesis was that this competition enables children to adopt a more positive and inclusive approach to languages in a minority situation –a necessary vision for language learning, in step with a world where mobility as a phenomenon becomes more and more commonplace and more and more complex.
%X L’article pose les jalons d’une enquête menée en Écosse entre 2015 et 2016. 'Mother Tongue Other Tongue' est un concours de poésie multilingue inscrit dans une démarche de promotion de l’apprentissage des langues à l’école. En exposant et en valorisant la réalité multilingue locale, toutes langues confondues, nous faisions l’hypothèse que ce concours permet aux enfants d’adopter une approche plus positive et inclusive des langues en situation minoritaire ; une vision nécessaire pour l’apprentissage des langues dans un monde où la mobilité est un phénomène de plus en plus courant et de plus en plus complexe.
%G French
%L hal-02553912
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02553912
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T <i>Mother Tongue Other Tongue</i> : approche créative des langues pour une (ré)appropriation de son répertoire langagier
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Pedley, Malika
%< avec comité de lecture
%@ 1146-6480
%J LIDIL - Revue de linguistique et de didactique des langues
%I UGA Editions
%S Démarches créatives, détours artistiques et appropriation des langues
%N 57
%8 2018
%D 2018
%R 10.4000/lidil.4820
%K poetry
%K children
%K creative writing
%K multilingualism
%K pratiques plurilingues
%K poésie
%K écriture créative
%K enfants
%Z Humanities and Social Sciences/LinguisticsJournal articles
%X 'Mother Tongue Other Tongue' is a poetry competition open to all languages in Scottish primary and secondary schools. Over the course of its 2015–2016 edition, semi-guided interviews were conducted with children who participated in the competition from four primary schools and 97 bilingual productions (English/other language) were retained as a corpus. The individual and personal dimension of this project encourages the child to think about how he/she relates to his/her languages. The collective dimension of the project and its aim of public promotion also impact this personal reflection. We will show how this experience—which involves a process of autonomy and a mediation activity—allows in itself an opportunity for linguistic 'appropriation' or 'reappropriation'.
%X 'Mother Tongue Other Tongue' est un concours de poésie ouvert à toutes les langues proposées dans les écoles primaires et secondaires en Écosse. Une enquête a été réalisée lors de l’édition 2015-2016 par le biais d’entretiens oraux semi-directifs dans quatre écoles ayant participé et par la constitution d’un corpus de 97 productions bilingues (anglais/autre langue). La dimension individuelle et personnelle de ce projet impose à l’élève un travail de réflexion sur son rapport aux langues. La dimension collective de ce projet et son objectif de diffusion auprès d’un large public influent également de manière significative sur ce travail. Ainsi, nous verrons comment cette expérience, à travers le processus d’autonomisation qu’elle implique et le travail de médiation que l’enfant doit réaliser, se présente comme une opportunité d’appropriation ou de réappropriation linguistique pour l’enfant.
%G English
%L hal-02553911
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02553911
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS
%~ TESTUPPA2
%~ UPPA-OA

