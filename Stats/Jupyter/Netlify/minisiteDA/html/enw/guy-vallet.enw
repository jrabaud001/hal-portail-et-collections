%0 Journal Article
%T Propriétés mécaniques dynamiques des polymères semi-cristallins. Rôle de la phase amorphe
%A Nemoz, G.
%A Jarrigeon, M.
%A Vallet, Guy
%< avec comité de lecture
%J Revue de Physique Appliquee
%V 12
%N 5
%P 849-854
%8 1977
%D 1977
%R 10.1051/rphysap:01977001205084900
%K amorphous state
%K anelastic relaxation
%K polymers
%K amorphous phase
%K dynamic mechanical properties
%K relaxation spectra
%K morphology
%K glass transition temperature
%K semicrystalline polymers
%K anelasticity
%Z Physics [physics]/Physics archivesJournal articles
%X L'étude des propriétés mécaniques dynamiques des polymères semi-cristallins permet de mettre en évidence des mécanismes de relaxation qui sont provoqués par des mouvements plus ou moins importants d'éléments cinétiques situés essentiellement dans les régions amorphes. L'étude expérimentale des spectres de relaxation caractérisés par les courbes tg δ = f( T) est réalisée en changeant la structure (polyesters, polyoléfines) et la morphologie (cristallinité, orientation) des polymères. Trois types de relaxation sont décrits : la relaxation γ (à T < Tg) liée à des éléments cinétiques de courte longueur ; la relaxation β au voisinage de la température de transition vitreuse, associée au passage des macromolécules de l'état vitreux à l'état caoutchouteux ; la relaxation α située à haute température.
%G French
%2 https://hal.archives-ouvertes.fr/jpa-00244251/document
%2 https://hal.archives-ouvertes.fr/jpa-00244251/file/ajp-rphysap_1977_12_5_849_0.pdf
%L jpa-00244251
%U https://hal.archives-ouvertes.fr/jpa-00244251
%~ AJP

%0 Journal Article
%T A closedness condition and its applications to DC programs with convex constraints
%+ Department of Mathematics, International University
%+ Department of Mathematics and Computer Science
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%A Dinh, N.
%A Nghia, T.T.A.
%A Vallet, Guy
%Z 19 pages
%< avec comité de lecture
%Z 0622
%@ 0233-1934
%J Optimization
%I Taylor & Francis
%V 59
%N 4
%P 541-560
%8 2010
%D 2010
%K DC programs
%K closedness conditions
%K Farkas-Minkovski systems
%K Farkas lemmas
%K Fenchel-Lagrange duality
%K Toland-Fenchel-Lagrange duality
%Z AMS: 90C25, 90C26, 90C46, 49K30.
%Z Mathematics [math]/Optimization and Control [math.OC]Journal articles
%X This paper concerns a closedness condition called (CC), requiring a convex function and a convex constrained system. This type of condition has played an important role in the study of convex optimization problems. Our aim is to establish several characterizations of this condition and to apply them to study minimizing problems involving a DC function under a cone-convex constraint and a set constraint. First, we establish several so-called “Toland-Fenchel-Lagrange” duality theorems. As consequences of these results, various versions of generalized Farkas lemmas in dual forms and optimality conditions for DC problem are obtained. A class of DC programs with semi-definite constraints is examined as an illustration. Most of these results are established under the (CC) and our paper serves as a link between several corresponding known ones published recently for DC programs and for convex programs.
%G English
%2 https://hal.archives-ouvertes.fr/hal-00221255/document
%2 https://hal.archives-ouvertes.fr/hal-00221255/file/0622.pdf
%L hal-00221255
%U https://hal.archives-ouvertes.fr/hal-00221255
%~ LMA-PAU
%~ CNRS
%~ INSMI
%~ TDS-MACS
%~ UNIV-PAU
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T A global existence and uniqueness result for a stochastic Allen-Cahn equation with constraint
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ University of Milan
%+ Department DICATAM
%+ Laboratoire de Mécanique et d'Acoustique [Marseille] (LMA )
%A Bauzet, Caroline
%A Bonetti, Elena
%A Bonfanti, Giovanna
%A Lebon, Frédéric
%A Vallet, Guy
%< avec comité de lecture
%@ 0170-4214
%J Mathematical Methods in the Applied Sciences
%I Wiley
%V 40
%N 14
%P 5241-5261
%8 2017-09-30
%D 2017
%R 10.1002/mma.4383
%K existence and uniqueness
%K nonlinear parabolic equations
%K stochastic PDEs
%Z Physics [physics]/Mechanics [physics]/Mechanics of the solides [physics.class-ph]Journal articles
%X This paper addresses the analysis of a time noise‐driven Allen-Cahn equation modelling the evolution of damage in continuum media in the presence of stochastic dynamics. The nonlinear character of the equation is mainly due to a multivoque maximal monotone operator representing a constraint on the damage variable, which is forced to take physically admissible values. By a Yosida approximation and a time‐discretization procedure, we prove a result of global‐in‐time existence and uniqueness of the solution to the stochastic problem.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01694069/document
%2 https://hal.archives-ouvertes.fr/hal-01694069/file/bauzet2017.pdf
%L hal-01694069
%U https://hal.archives-ouvertes.fr/hal-01694069
%~ CNRS
%~ UNIV-AMU
%~ UNIV-PAU
%~ LMA-PAU
%~ LMA_UPR7051
%~ EC-MARSEILLE
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T The Dirichlet problem for a conservation law with a multiplicative stochastic perturbation
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Fakultät für Mathematik der Universität Duisburg-Essen
%A Bauzet, Caroline
%A Vallet, Guy
%A Wittbold, Petra
%< avec comité de lecture
%@ 0022-1236
%J Journal of Functional Analysis
%I Elsevier
%8 2014
%D 2014
%R 10.1016/j.jfa.2013.06.022
%K Stochastic PDE
%K First-order hyperbolic equation
%K Dirichlet problem
%K Multiplicative stochastic perturbation
%K Young measures
%K Kruzhkovʼs semi-entropy
%Z Mathematics [math]/Analysis of PDEs [math.AP]Journal articles
%X In this paper, we are interested in the Dirichlet boundary value problem for a multi-dimensional nonlinear conservation law with a multiplicative stochastic perturbation. Using the concept of measure-valued solutions and Kruzhkovʼs semi-entropy formulations, a result of existence and uniqueness of the entropy solution is proved.
%G English
%L hal-01309556
%U https://hal.archives-ouvertes.fr/hal-01309556
%~ CNRS
%~ UNIV-PAU
%~ TDS-MACS
%~ LMA-PAU
%~ INSMI
%~ TESTUPPA2
%~ UPPA-OA

%0 Unpublished work
%T On the hydrostatic Stokes approximation with non homogeneous Dirichlet boundary conditions
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%A Amrouche, Chérif
%A Dahoumane, Fabien
%A Luce, Robert
%A Vallet, Guy
%Z 27 pages.
%8 2009
%D 2009
%K Hydrostatic approximation
%K De Rham's lemma
%K shifting operator
%K Primitive Equations
%K non homogeneous Dirichlet conditions
%Z AMS 35Q30, 35B40, 76D05, 34C35
%Z Mathematics [math]/Analysis of PDEs [math.AP]Preprints, Working Papers, ...
%X We deal with the hydrostatic Stokes approximation with non homogeneous Dirichlet boundary conditions. After having investigated the homogeneous case, we build a shifting operator of boundary values related to the divergence operator, and solve the non homogeneous problem in a cylindrical type domain.
%G English
%2 https://hal.archives-ouvertes.fr/hal-00428960/document
%2 https://hal.archives-ouvertes.fr/hal-00428960/file/DEA_Revis_Sept_2009.pdf
%L hal-00428960
%U https://hal.archives-ouvertes.fr/hal-00428960
%~ LMA-PAU
%~ INSMI
%~ CNRS
%~ UNIV-PAU
%~ TDS-MACS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T The stochastic $p(ω,t,x)$-Laplace equation with cylindrical Wiener process
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ University of Duisbourg-Essen
%A Vallet, Guy
%A Zimmermann, Aleksandra
%Z ACL
%< avec comité de lecture
%@ 0022-247X
%J Journal of Mathematical Analysis and Applications
%I Elsevier
%V 444
%N 2
%P 1359-1371
%8 2016
%D 2016
%R 10.1016/j.jmaa.2016.07.018
%K Stochastic forcing
%K p-Laplace
%K Random variable exponent
%Z Mathematics [math]/Numerical Analysis [math.NA]
%Z Mathematics [math]/Analysis of PDEs [math.AP]
%Z Mathematics [math]/Geometric Topology [math.GT]
%Z Mathematics [math]/Probability [math.PR]Journal articles
%X We propose the analysis of a non-linear parabolic problem of $p(ω,t,x)$-Laplace type in the framework of Orlicz Lebesgue and Sobolev spaces with variable random exponents and a stochastic forcing by a cylindrical Wiener process. We give a result of well-posedness: existence, uniqueness and stability of the solution, for additive and multiplicative problems.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01581274/document
%2 https://hal.archives-ouvertes.fr/hal-01581274/file/vallet2016.pdf
%L hal-01581274
%U https://hal.archives-ouvertes.fr/hal-01581274
%~ UNIV-PAU
%~ TDS-MACS
%~ INSMI
%~ LMA-PAU
%~ CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Modelling and mathematical analysis of the glass eel migration in the adour river estuary
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%A Odunlami, Marc
%A Vallet, Guy
%Z cited By (since 1996)0
%< avec comité de lecture
%@ 0973-5348
%J Mathematical Modelling of Natural Phenomena
%I EDP Sciences
%V 7
%N 3
%P 168-185
%8 2012
%D 2012
%R 10.1051/mmnp/20127311
%Z Mathematics [math]/Analysis of PDEs [math.AP]Journal articles
%X In this paper we are interested in a mathematical model of migration of grass eels in an estuary. We first revisit a previous model proposed by O. Arino and based on a degenerate convection-diffusion equation of parabolic-hyperbolic type with time-varying subdomains. Then, we propose an adapted mathematical framework for this model, we prove a result of existence of a weak solution and we propose some numerical simulations. © EDP Sciences, 2012.
%G English
%L hal-00865033
%U https://hal.archives-ouvertes.fr/hal-00865033
%~ CNRS
%~ LMA-PAU
%~ INSMI
%~ TDS-MACS
%~ UNIV-PAU
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T On a class of quasilinear Barenblatt equations
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%A Bauzet, Caroline
%A Giacomoni, Jacques
%A Vallet, Guy
%< avec comité de lecture
%J Monografias de la Real Academia de Ciencias de Zaragoza
%I Real Academia de Ciencias Exactas, Físicas, Químicas y Naturales de Zaragoza
%S A special tribute to Professor Monique Madaune-Tort
%N 38
%P 35-51
%8 2012
%D 2012
%K Quasilinear parabolic equation
%K Barenblatt equation
%K time-semi-discretization
%K stochastic perturbation
%K pseudo-parabolic equation
%K p-Laplace operator
%Z Mathematics [math]/Analysis of PDEs [math.AP]Journal articles
%X In this paper, we first investigate a quasilinear parabolic equation of Barenblatt type. We prove the existence of a weak solution and give some related regularity results. Next, we are concerned by the study of a Barenblatt problem involving a stochastic perturbation and we prove the existence and the uniqueness of the stochastic weak solution.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01309558/document
%2 https://hal.archives-ouvertes.fr/hal-01309558/file/035.pdf
%L hal-01309558
%U https://hal.archives-ouvertes.fr/hal-01309558
%~ CNRS
%~ UNIV-PAU
%~ TDS-MACS
%~ LMA-PAU
%~ INSMI
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T On abstract Barenblatt equations
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%A Bauzet, Caroline
%A Vallet, Guy
%Z ACL
%< avec comité de lecture
%@ 1021-9722
%J Nonlinear Differential Equations and Applications
%I Springer Verlag
%V 3
%N 4
%P 487-502
%8 2011
%D 2011
%R 10.7153/dea-03-31
%Z Mathematics [math]/Analysis of PDEs [math.AP]Journal articles
%X In this paper we are interested in abstract problems of Barenblatt's type. In a first part, we investigate the problem f (∂ t u) + Au = g where f and A are maximal monotone operators and by assuming that A derives from a potential J. With general assumptions on the operators, we prove the existence of a solution. In the second part of the paper, we examine a stochastic version of the above problem: f [∂ t (u − t 0 hdw)] + Au = 0 , with some restrictive assumptions on the data due principally to the framework of the Itô integral.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01309490/document
%2 https://hal.archives-ouvertes.fr/hal-01309490/file/dea-03-31.pdf
%L hal-01309490
%U https://hal.archives-ouvertes.fr/hal-01309490
%~ CNRS
%~ UNIV-PAU
%~ TDS-MACS
%~ LMA-PAU
%~ INSMI
%~ TESTUPPA2
%~ UPPA-OA

%0 Unpublished work
%T A Bilateral Obstacle Problem for a Class of Degenerate Parabolic-Hyperbolic Operators.
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%A Levi, Laurent
%A Vallet, Guy
%Z 20 pages.
%Z 0604
%8 2006
%D 2006
%Z Mathematics [math]/Analysis of PDEs [math.AP]Preprints, Working Papers, ...
%X We investigate some inner bilateral obstacle problems for a class of strongly degenerate parabolic-hyperbolic quasilinear operators associated with homogeneous Dirichlet data in a multidimensional bounded domain. We first introduce the concept of an entropy process solution, more convenient and generalizing the notion of an entropy solution. Moreover, the boundary conditions are expressed by using the background of Divergence Measure Fields. We ensure that proposed definition warrants uniqueness. The existence of an entropy process solution is obtained through the vanishing viscosity and penalization methods.
%G English
%2 https://hal.archives-ouvertes.fr/hal-00220963/document
%2 https://hal.archives-ouvertes.fr/hal-00220963/file/0604.pdf
%L hal-00220963
%U https://hal.archives-ouvertes.fr/hal-00220963
%~ LMA-PAU
%~ CNRS
%~ UNIV-PAU
%~ INSMI
%~ TDS-MACS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Lewy-Stampacchia’s inequality for a pseudomonotone parabolic problem
%+ Laboratoire de Mathématiques Raphaël Salem (LMRS)
%+ Laboratoire d'équations aux dérivées partielles non linéaires et histoires des mathématiques
%+ Laboratoire d"équations aux dérivées partielles non linéaires et histoire des mathématiques
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%A Guibé, Olivier
%A Mokrane, Abdelhafid
%A Tahraoui, Yacine
%A Vallet, Guy
%< avec comité de lecture
%@ 2191-950X
%J Advances in Nonlinear Analysis
%I De Gruyter
%V 9
%N 1
%P 591-612
%8 2019-06-23
%D 2019
%R 10.1515/anona-2020-0015
%Z Mathematics [math]
%Z Mathematics [math]/Analysis of PDEs [math.AP]Journal articles
%G English
%L hal-02366483
%U https://hal.archives-ouvertes.fr/hal-02366483
%~ UNIV-PAU
%~ LMA-PAU
%~ LMRS
%~ INSMI
%~ CNRS
%~ UNIROUEN
%~ COMUE-NORMANDIE
%~ TDS-MACS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T The Cauchy problem for fractional conservation laws driven by Lévy noise
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%A Bhauryal, Neeraj
%A Koley, Ujjwal
%A Vallet, Guy
%< avec comité de lecture
%@ 0304-4149
%J Stochastic Processes and their Applications
%I Elsevier
%V 130
%N 9
%P 5310-5365
%8 2020-09
%D 2020
%R 10.1016/j.spa.2020.03.009
%Z Mathematics [math]/Analysis of PDEs [math.AP]
%Z Mathematics [math]/Probability [math.PR]Journal articles
%G English
%L hal-03125104
%U https://hal.archives-ouvertes.fr/hal-03125104
%~ LMA-PAU
%~ INSMI
%~ UNIV-PAU
%~ TDS-MACS
%~ CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T On a p(t,x)-Laplace evolution equation with a stochastic force
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Fakultät für Mathematik der Universität Duisburg-Essen
%A Bauzet, Caroline
%A Vallet, Guy
%A Wittbold, Petra
%A Zimmermann, Aleksandra
%< avec comité de lecture
%@ 2194-0401
%J Stochastics and Partial Differential Equations: Analysis and Computations
%I Springer US
%V 1
%N 3
%P 552-570
%8 2013
%D 2013
%R 10.1007/s40072-013-0017-z
%K p(t
%K x) Laplace
%K Variable exponent
%K Stochastic forcing
%K Monotonicity
%Z Mathematics [math]/Analysis of PDEs [math.AP]Journal articles
%X In this paper, we are interested in the stochastic forcing of a nonlinear singular/degenerated parabolic problem of p(t,x)-Laplace type. Since the Lebesgue and Sobolev spaces with variable exponents of variables t and x are Orlicz type spaces and do not fit into the classical framework of Bochner spaces, we have to adapt to this framework classical methods based on monotonicity arguments.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01309575/document
%2 https://hal.archives-ouvertes.fr/hal-01309575/file/Bauzet%20Vallet%20Wittbold%20Zimmenmann%20final.pdf
%L hal-01309575
%U https://hal.archives-ouvertes.fr/hal-01309575
%~ CNRS
%~ UNIV-PAU
%~ TDS-MACS
%~ LMA-PAU
%~ INSMI
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T THE CAUCHY PROBLEM FOR CONSERVATION LAWS WITH A MULTIPLICATIVE STOCHASTIC PERTURBATION
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Fakultät für Mathematik der Universität Duisburg-Essen
%A Bauzet, Caroline
%A Vallet, Guy
%A Wittbold, Petra
%< avec comité de lecture
%@ 0219-8916
%J Journal of Hyperbolic Differential Equations
%I World Scientific Publishing
%8 2012-12
%D 2012
%R 10.1142/S0219891612500221
%K Stochastic PDE
%K first-order hyperbolic equation
%K Cauchy problem
%K multiplicative stochastic perturbation
%K Young measures
%K Kruzhkov's entropy
%Z 35R60, 35L60, 60H15
%Z Mathematics [math]/Analysis of PDEs [math.AP]Journal articles
%X We study the Cauchy problem for multi-dimensional nonlinear conservation laws with multiplicative stochastic perturbation. Using the concept of measure-valued solutions and Kruzhkov's entropy formulation, the existence and uniqueness of an entropy solution is established.
%G English
%L hal-01309552
%U https://hal.archives-ouvertes.fr/hal-01309552
%~ CNRS
%~ UNIV-PAU
%~ TDS-MACS
%~ LMA-PAU
%~ INSMI
%~ TESTUPPA2
%~ UPPA-OA

%0 Unpublished work
%T Farkas-type results and duality for DC programs with convex constraints
%+ Department of Mathematics, International University
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Department of Mathematics and Computer Science
%A Dinh, N.
%A Vallet, Guy
%A Nghia, T.T.A.
%Z 27 pages
%Z 0621
%8 2006
%D 2006
%K Generalized Farkas lemmas
%K DC-programs
%K Toland
%K Fenchel
%K Lagrange duality
%K approximate normal cones
%Z AMS subject classifications: 49K30, 90C25, 90C26, 90C46.
%Z Mathematics [math]/Optimization and Control [math.OC]Preprints, Working Papers, ...
%X In this paper, we are interested in new versions of Farkas lemmas for systems involving convex and DC-inequalities. These versions extend well-known Farkas-type results published recently, which were used as main tools in the study of convex optimization problems. The results are used to derive several strong duality results such as: Lagrange, Fenchel-Lagrange or Toland-Fenchel-Lagrange duality for DC and convex problems. Moreover, it is shown that for this class of problems, these versions of Farkas lemma are actually equivalent to several strong duality results of Lagrange, Fenchel-Lagrange or Toland- Fenchel-Lagrange types.
%G English
%2 https://hal.archives-ouvertes.fr/hal-00221223/document
%2 https://hal.archives-ouvertes.fr/hal-00221223/file/0621.pdf
%L hal-00221223
%U https://hal.archives-ouvertes.fr/hal-00221223
%~ LMA-PAU
%~ CNRS
%~ UNIV-PAU
%~ INSMI
%~ TDS-MACS
%~ TESTUPPA2
%~ UPPA-OA

%0 Unpublished work
%T Test of special relativity using a fiber network of optical clocks
%+ Laboratoire national de métrologie et d'essais - Systèmes de Référence Temps-Espace (LNE - SYRTE)
%+ Laboratoire de Physique des Lasers (LPL)
%+ Korea Research Institute of Standards and Science [Daejon] (KRISS)
%+ Physikalisch-Technische Bundesanstalt [Braunschweig] (PTB)
%+ National Physical Laboratory [Teddington] (NPL)
%A Delva, P
%A Lodewyck, J
%A Bilicki, S
%A Bookjans, E
%A Vallet, Guy
%A Le Targat, R
%A Pottie, P-E
%A Guerlin, C
%A Meynadier, F
%A Le Poncin-Lafitte, C
%A Lopez, O
%A Amy-Klein, A
%A Lee, W-K
%A Quintin, N
%A Lisdat, C
%A Al-Masoudi, A
%A Dörscher, S
%A Grebing, C
%A Grosche, G
%A Kuhl, A
%A Raupach, S, M F
%A Sterr, U
%A Hill, I, R
%A Hobson, R
%A Bowden, W
%A Kronjäger, J
%A Marra, G
%A Rolland, A
%A Baynes, P, N
%A Margolis, H, S
%A Gill, P
%Z DIM Nano'K - région Ile de France
%Z Action spécifique GRAM du CNRS
%Z CNES
%Z German Research Foundation DFG
%Z Korea Research Institute of Standards and Science under the project \Research on Time and Space Measurements", Grant No. 16011007
%Z R&D Convergence Program of NST (National Research Council of Science and Technology) of Republic of Korea (Grant No. CAP-15-08-KRISS).
%8 2017-03-16
%D 2017
%Z 1703.04426
%K optical link
%K special relativity
%K optical clock
%Z Physics [physics]/Quantum Physics [quant-ph]
%Z Physics [physics]/Astrophysics [astro-ph]Preprints, Working Papers, ...
%X Phase compensated optical fiber links enable high accuracy atomic clocks separated by thousands of kilometers to be compared with unprecedented statistical resolution. By searching for a daily variation of the frequency difference between four strontium optical lattice clocks in different locations throughout Europe connected by such links, we improve upon previous tests of time dilation predicted by special relativity. We obtain a constraint on the Robertson–Mansouri–Sexl parameter |α| 1.1 × 10 −8 quantifying a violation of time dilation, thus improving by a factor of around two the best known constraint obtained with Ives–Stilwell type experiments, and by two orders of magnitude the best constraint obtained by comparing atomic clocks. This work is the first of a new generation of tests of fundamental physics using optical clocks and fiber links. As clocks improve, and as fiber links are routinely operated, we expect that the tests initiated in this paper will improve by orders of magnitude in the near future.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01491475/document
%2 https://hal.archives-ouvertes.fr/hal-01491475/file/es2017feb20_565.pdf
%L hal-01491475
%U https://hal.archives-ouvertes.fr/hal-01491475
%~ CNRS
%~ OBSPM
%~ SYRTE
%~ UPMC
%~ LPL_P13
%~ UNIV-PARIS13
%~ INSU
%~ LNE
%~ USPC
%~ UPMC_POLE_2
%~ SORBONNE-UNIVERSITE
%~ SU-SCI
%~ SU-SCIENCES
%~ SORBONNE-PARIS-NORD
%~ PSL
%~ OBSPM-PSL
%~ SU-TI
%~ ANR

%0 Journal Article
%T On the hydrostatic Stokes approximation with non homogeneous boundary conditions
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Complex Flow Simulation Codes based on High-order and Adaptive methods (CONCHA)
%A Amrouche, Chérif
%A Dahoumane, Fabien
%A Luce, Robert
%A Vallet, Guy
%Z accepted in Differential Equations and Applications - DEA
%< avec comité de lecture
%@ 1847-120X
%J Differential Equations and Applications
%I Element
%V 2
%N 3
%P 419-446
%8 2010
%D 2010
%R 10.7153/dea-02-26
%Z Mathematics [math]/Numerical Analysis [math.NA]Journal articles
%G English
%L inria-00438538
%U https://hal.inria.fr/inria-00438538
%~ CNRS
%~ INRIA
%~ UNIV-PAU
%~ INRIA-BORDEAUX
%~ LMA-PAU
%~ INSMI
%~ TESTBORDEAUX
%~ TDS-MACS
%~ INRIA_TEST
%~ TESTUPPA2
%~ UPPA-OA

%0 Unpublished work
%T Une approche analytique de modèles géologiques à flux gravitaire asservi
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%A Gagneux, Gérard
%A Vallet, Guy
%Z 23 pages
%8 2010
%D 2010
%Z Mathematics [math]/Analysis of PDEs [math.AP]Preprints, Working Papers, ...
%X Les modèles géologiques d'érosion et de sédimentation élaborés à l'Institut Français du Pétrole introduisent des systèmes d'équations de continuité sous des contraintes originales d'asservissement des flux. La modélisation des phénomènes de la seule sédimentation conduit à des problèmes de type hyperbolique-parabolique dégénéré faisant apparaître des frontières libres à la manière des problèmes de Bernoulli. Ces formulations sont mal posées au sens d'Hadamard et nécessitent de définir un critère de sélection pour isoler la solution signicative. On dégage l'idée que le critère de discrimination d'entropie "à la Kruzkhov" n'est pas ici pertinent et qu'il convient de définir un critère de maximalité en un certain sens pour limiter de façon optimale les flux gravitaires asservis. On présente un tour d'horizon de ces questions très largement ouvertes pour susciter d'autres travaux et d'autres approches de ces problèmes mathématiques nouveaux et d'abord difficile, tant du point de vue analytique que numérique. Le thème central est l'étude d'inclusions différentielles $0 \in \partial{s}{t} - div( H (\partial{s}{t}) \Nabla S) où $H$ est le graphe maximal monotone de Heaviside.
%G French
%2 https://hal.archives-ouvertes.fr/hal-00466085/document
%2 https://hal.archives-ouvertes.fr/hal-00466085/file/geo.pdf
%L hal-00466085
%U https://hal.archives-ouvertes.fr/hal-00466085
%~ CNRS
%~ UNIV-PAU
%~ LMA-PAU
%~ INSMI
%~ TDS-MACS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Comparison of two training programmes in chronic airway limitation patients: standardized versus individualized protocols
%+ Euromov (EuroMov)
%+ Les Cliniques du Souffle, Groupe 5-Santé, 34700 Lodève, France
%+ Université de Picardie Jules Verne (UPJV)
%+ Université de Pau et des Pays de l'Adour (UPPA)
%+ Université de Lille
%+ Physiologie & médecine expérimentale du Cœur et des Muscles [U 1046] (PhyMedExp)
%+ Centre Hospitalier Régional Universitaire [Montpellier] (CHRU Montpellier)
%A Varray, Alain
%A Vallet, Guy
%A Ahmaidi, S.
%A Serres, I.
%A Fabre, C.
%A Bourgouin, D.
%A Desplan, J.
%A Préfaut, Ch.
%< avec comité de lecture
%@ 0903-1936
%J European Respiratory Journal
%I European Respiratory Society
%V 10
%N 1
%P 114 - 122
%8 1997-01
%D 1997
%R 10.1183/09031936.97.10010114
%Z Life Sciences [q-bio]/Human health and pathology/Tissues and Organs [q-bio.TO]
%Z Life Sciences [q-bio]/Human health and pathology/Pulmonology and respiratory tractJournal articles
%G English
%L hal-01625251
%U https://hal.umontpellier.fr/hal-01625251
%~ CNRS
%~ UNIV-PICARDIE
%~ UNIV-PAU
%~ BS
%~ UNIV-MONTPELLIER
%~ UNIV-LILLE
%~ TESTUPPA2
%~ UPPA-OA
%~ PHYMEDEXP
%~ U-PICARDIE

%0 Journal Article
%T Potential prolificacy and winter breeding in free-living wild sows (Sus scrofa scrofa)
%A AUMAITRE, A.
%A Morvan, Catherine
%A QUERE, J.P.
%A PEINIAU, Jany
%A Vallet, Guy
%< avec comité de lecture
%@ 0003-424X
%J Annales de zootechnie
%I INRA/EDP Sciences
%V 31
%N 3
%P 324-325
%8 1982
%D 1982
%Z Life Sciences [q-bio]/Agricultural sciences/ZootechnyJournal articles
%G French
%2 https://hal.archives-ouvertes.fr/hal-00888160/document
%2 https://hal.archives-ouvertes.fr/hal-00888160/file/hal-00888160.pdf
%L hal-00888160
%U https://hal.archives-ouvertes.fr/hal-00888160
%~ ARINRAE-ANNAZOO
%~ ARINRAE-ARCHIVES

%0 Journal Article
%T A degenerate parabolic-hyperbolic Cauchy problem with a stochastic force
%+ Institut de Mathématiques de Marseille (I2M)
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Essen, Fakultaet fuer Mathematik
%A Bauzet, Caroline
%A Vallet, Guy
%A Wittbold, Petra
%< avec comité de lecture
%@ 0219-8916
%J Journal of Hyperbolic Differential Equations
%I World Scientific Publishing
%V 12
%N 03
%P 501-533
%8 2015-09
%D 2015
%R 10.1142/S0219891615500150
%K Cauchy problem
%K multiplicative stochastic perturbation
%K Carrillo-Kruzhkov's entropy
%K Stochastic PDE
%K degenerate parabolic-hyperbolic equation
%K Carrillo-Kruzhkov's entropy.
%Z 35K65, 60H15, 35L65.
%Z Mathematics [math]/Analysis of PDEs [math.AP]Journal articles
%X In this paper we are interested in the Cauchy problem for a nonlinear degenerate parabolic-hyperbolic problem with multiplicative stochastic forcing. Using an adapted entropy formulation a result of existence and uniqueness of a solution is proved.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01003069/document
%2 https://hal.archives-ouvertes.fr/hal-01003069/file/A_degenerate_parabolic-hyperbolic_Cauchy_problem_with_a_stochastic_force.pdf
%L hal-01003069
%U https://hal.archives-ouvertes.fr/hal-01003069
%~ CNRS
%~ LMA-PAU
%~ INSMI
%~ EC-MARSEILLE
%~ I2M
%~ I2M-2014-
%~ UNIV-AMU
%~ UNIV-PAU
%~ TDS-MACS
%~ TESTUPPA2
%~ UPPA-OA

%0 Unpublished work
%T Study of a singular equation set in the half-space
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%A Amrouche, Chérif
%A Dahoumane, Fabien
%A Vallet, Guy
%Z 18 pages
%8 2010
%D 2010
%K Elliptic equation
%K singular equation
%K half-space
%K weighted spaces
%K weak solutions
%K strong solutions
%K Hardy's inequality
%Z AMS 35Q30, 35B40, 76D05, 34C35
%Z Mathematics [math]/Analysis of PDEs [math.AP]Preprints, Working Papers, ...
%X This work is dedicated to the resolution of a singular equation set in the half-space, with a diffusion coe cient that blows up on the boundary. More precisely, for a datum g : R3 → R, our problem consists in seeking u : R3 → R formally solution to: −div(1/x3 grad u) = g in R3, u = 0 on Γ = R2 × {0} . We give existence and uniqueness results of weak and strong solutions in suitable weighted spaces, where the weight depends on x3 .
%G English
%2 https://hal.archives-ouvertes.fr/hal-00629120/document
%2 https://hal.archives-ouvertes.fr/hal-00629120/file/ProceedEdimb.pdf
%L hal-00629120
%U https://hal.archives-ouvertes.fr/hal-00629120
%~ CNRS
%~ UNIV-PAU
%~ LMA-PAU
%~ INSMI
%~ TDS-MACS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T On the Cauchy problem of a degenerate parabolic-hyperbolic PDE with Lévy noise
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%A Biswas, Imran
%A Majee, Ananta
%A Vallet, Guy
%< avec comité de lecture
%@ 2191-950X
%J Advances in Nonlinear Analysis
%I De Gruyter
%V 8
%N 1
%P 809-844
%8 2017-09-12
%D 2017
%R 10.1515/anona-2017-0113
%Z Mathematics [math]/Analysis of PDEs [math.AP]Journal articles
%G English
%L hal-02369302
%U https://hal.archives-ouvertes.fr/hal-02369302
%~ UNIV-PAU
%~ LMA-PAU
%~ INSMI
%~ CNRS
%~ TDS-MACS
%~ TESTUPPA2
%~ UPPA-OA

