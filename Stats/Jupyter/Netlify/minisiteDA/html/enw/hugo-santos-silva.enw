%0 Journal Article
%T Towards the synthesis of poly(azafulleroid)s: main chain fullerene oligomers for organic photovoltaic devices
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Bregadiolli, B.A.
%A Ramanitra, H.H.
%A Ferreira, R.M.
%A Corcoles, L.
%A Gomes, M.S.
%A Kang, L.
%A Combe, C.M.S.
%A Santos Silva, Hugo
%A Lavarda, F.C.
%A Bégué, Didier
%A Dagron-Lartigau, C.
%A Rocco, M.L.M.
%A Luscombe, C.K.
%A Olivati, C.A.
%A Graeff, C.F.O.
%A Hiorns, R.C.
%Z cited By 0
%< avec comité de lecture
%@ 0959-8103
%J Polymer International
%I Wiley
%V 66
%N 10
%P 1364-1371
%8 2017
%D 2017
%R 10.1002/pi.5419
%Z Chemical Sciences/PolymersJournal articles
%X For the first time, imino chemistry is explored for the synthesis of main chain fullerene oligomers and low-molecular-weight polymers. Oligo(aziridinofullerene)s and low-molecular-weight poly(aziridinofullerene)s (PAFs) are prepared by 1,3-dipolar cycloadditions of bis(azidoalkyl) comonomers to C60. The effect of the steric bulk of the comonomer is studied and it is found that PAF solubilities and molecular weights are increased with longer comonomer side-chains. Initial and promising photovoltaic characterizations show commensurate improvements in properties. Interestingly, the PAFs exhibit lowest unoccupied molecular orbitals close to those of phenyl-C61-butyric acid methyl ester. Routine characterizations are correlated with DFT/B3LYP/6-31G(d) to understand structural and electronic behaviours. It is expected that higher molecular weights may be obtained through comonomer variations and that this novel methodology will open a route to interesting materials for optoelectronics. © 2017 Society of Chemical Industry. © 2017 Society of Chemical Industry
%G English
%L hal-01601972
%U https://hal.archives-ouvertes.fr/hal-01601972
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ IPREM-CAPT
%~ IPREM-PCM
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T In Situ Generation of Fullerene from a Poly(fullerene)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Santos Silva, Hugo
%A Ramanitra, Hasina
%A Bregadiolli, Bruna
%A Tournebize, Aurélien
%A Bégué, Didier
%A Dowland, Simon
%A Lartigau-Dagron, Christine
%A Graeff, Carlos
%A Distler, Andreas
%A Peisert, Heiko
%A Chassé, Thomas
%A Hiorns, Roger C.
%< avec comité de lecture
%@ 0887-6266
%J Journal of Polymer Science Part B: Polymer Physics
%I Wiley
%V 57
%N 21
%P 1434-1452
%8 2019-10-16
%D 2019
%R 10.1002/polb.24888
%K atom transfer radical addition degradation Fullerenes organic photovoltaic organic solar cell photoreactive effects poly(fullerene)
%K poly(fullerene)
%K organic solar cell
%K organic photovoltaic
%K atom transfer radical addition polymerisation (ATRAP)
%Z Chemical Sciences/Polymers
%Z Chemical Sciences
%Z Chemical Sciences/Theoretical and/or physical chemistryJournal articles
%X This article appraises the thermal, photo‐ and photo‐oxidative stability of poly(fullerene)‐alt‐[bismethylbenzene)]s (PFBMBs) prepared by the atom transfer radical addition polymerization (ATRAP) with particular attention paid to their use as additives in organic photovoltaic devices. PFBMBs are of interest due to their well‐defined structures based on alternating, main‐chain, fullerene‐methylene links. This work shows by way of a wide range of characterization techniques and a small library of PFBMBs with varying side chains, however, that PFBMBs are relatively unstable. Given that prior work has shown that other main‐chain fullerene polymers, such as poly(pyrrolidinofullerene)s, are inherently stable, we suggest a degradation mechanism specific to the fullerene‐methylene links of PFBMBs, which explains their unusual behavior. This work suggests that polymers based on fullerene each have their own specific stabilities and qualities and that PFBMBs might be of more use in purposes other than OPVs where in situ delivery of fullerene is required, for example, in medical applications.
%G English
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-03043615/document
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-03043615/file/ATRAP%20revised%20to%20production.pdf
%L hal-03043615
%U https://hal-univ-pau.archives-ouvertes.fr/hal-03043615
%~ UNIV-PAU
%~ IPREM
%~ IPREM-CAPT
%~ IPREM-PCM
%~ INC-CNRS
%~ CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Redox activity of nickel and vanadium porphyrins: a possible mechanism behind petroleum genesis and maturation?
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Munoz, G.
%A Gunessee, B.
%A Bégué, D.
%A Bouyssiere, B.
%A Baraille, Isabelle
%A Vallverdu, Germain
%A Santos Silva, Hugo
%< avec comité de lecture
%@ 2046-2069
%J RSC Advances
%I Royal Society of Chemistry
%V 9
%N 17
%P 9509-9516
%8 2019-03-26
%D 2019
%R 10.1039/C9RA01104H
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Analytical chemistry
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Material chemistryJournal articles
%X The presence of metalloporphyrins in crude oil has been known for many years. In contrast, their role on the physical–chemical properties is only now beginning to be understood. In this study, we test using high-level ab initio calculations, the hypothesis of a possible redox catalytic activity of vanadium and nickel metalloporphyrins in crude oil, illustrated by the oxidation of methanol to formaldehyde and hydrogen dissociation, respectively. This process which may take place during petroleum genesis and maturation, explains some of its physical–chemical properties, such as polar chains, the absence of alcohols, the trapping of porphyrins within macromolecular aggregates.
%G English
%L hal-02095223
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02095223
%~ UNIV-PAU
%~ IPREM
%~ IPREM-CAPT
%~ CNRS
%~ INC-CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Asphaltene aggregation studied by molecular dynamics simulations: role of the molecular architecture and solvents on the supramolecular or colloidal behavior
%+ Instituto de Física
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Santos Silva, Hugo
%A Alfarra, A.
%A Vallverdu, Germain
%A Bégué, D.
%A Bouyssiere, B.
%A Baraille, Isabelle
%Z Isifor-Carnot Institute
%Z Total Refining & Chemicals
%< avec comité de lecture
%@ 1091-6466
%J Petroleum Science and Technology
%I Taylor & Francis
%V 16
%N 3
%P 669-684
%8 2019-06
%D 2019
%R 10.1007/s12182-019-0321-y
%K Heavy oil
%K Asphaltenes Molecular dynamics Aggregation Heavy oil
%K Asphaltenes
%K Molecular dynamics
%K Aggregation
%Z Chemical Sciences/Theoretical and/or physical chemistryJournal articles
%X This paper addresses molecular dynamic simulations to correlate the aggregation properties of asphaltenes, their molecular structure and the concentration of these solvents. We show that the formation of the nanoaggregate depends, primarily, on the size of the conjugated core and on the eventual presence of polar groups capable of forming H-bonds. Heteroatoms on the conjugated core do not change their shape or type of aggregation but may induce stronger π−π interactions. The macroaggregation formation depends upon the length of the lateral chains of asphaltenes and also on the presence of polar groups at its end. Moreover, n-heptane and water may interact selectively with asphaltenes in function of their molecular architecture. Given this fact and the aggregation behavior observed, we advocate toward the assumption that a colloidal behavior of asphaltenes might be a particular case of a more general model, based on a supramolecular description.
%G English
%2 https://hal.archives-ouvertes.fr/hal-02172587/document
%2 https://hal.archives-ouvertes.fr/hal-02172587/file/SantosSilva2019_Article_AsphalteneAggregationStudiedBy.pdf
%L hal-02172587
%U https://hal.archives-ouvertes.fr/hal-02172587
%~ UNIV-PAU
%~ CNRS
%~ IPREM
%~ INC-CNRS
%~ IPREM-CAPT
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T A universal route to improving conjugated macromolecule photostability
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Santos Silva, Hugo
%A Tournebize, A.
%A Bégué, Didier
%A Peisert, H.
%A Chassé, T.
%A Gardette, J.-L.
%A Therias, S.
%A Rivaton, A.
%A Hiorns, R.C.
%Z cited By 10
%< avec comité de lecture
%@ 2046-2069
%J RSC Advances
%I Royal Society of Chemistry
%V 4
%N 97
%P 54919-54923
%8 2014
%D 2014
%R 10.1039/c4ra10806j
%Z Chemical Sciences/PolymersJournal articles
%X This predictive study demonstrates that the introduction of aromatic-oxy-alkyl links surprisingly makes materials more resistant to photo-oxidative degradation by reducing hydrogen abstraction. This revelation makes it possible, for the first time, to design a toolbox of substituents for soluble, photostable conjugated materials. This journal is © the Partner Organisations 2014.
%G English
%L hal-01601975
%U https://hal.archives-ouvertes.fr/hal-01601975
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Influence of traces of oxidized polymer on the performances of bulk heterojunction solar cells
%+ Institut de Chimie de Clermont-Ferrand (ICCF)
%+ Laboratoire de l'intégration, du matériau au système (IMS)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Perthue, Anthony
%A Gorisse, Thérèse
%A Santos Silva, Hugo
%A Bégué, Didier
%A Rivaton, Agnès
%A Wantz, Guillaume
%< avec comité de lecture
%@ 2052-1537
%J Materials Chemistry Frontiers
%I Royal Society of Chemistry
%V 3
%N 8
%P 1632-1641
%8 2019-07-25
%D 2019
%R 10.1039/C9QM00191C
%Z Chemical Sciences
%Z Chemical Sciences/Theoretical and/or physical chemistryJournal articles
%X A key challenge in the field of organic photovoltaics (OPVs) is making them efficient and stable devices despite their being composed of organic materials, which are susceptible to becoming photodegraded in the presence of atmospheric oxygen. It is therefore essential to determine to what extent the donor material used in the active layer can be oxidized before the oxidation results in a loss of solar cell performance. Here we mainly focused on thieno[3,4-b]thiophene-alt-benzodithiophene polymer (PTB7), and compared it to the well-known poly(3-hexylthiophene) (P3HT). The complexity of the PTB7 chemical structure, based on an alternation of benzodithiophene (BDT) and thienothiophene (TT) and flanked with alkoxy and alkyl side chains, necessitated a re-investigation of the first step of the photooxidative process. Neither the intrinsic photochemical process nor the presence of an alkoxy side chain was found to be critical for the photostability. The high initial sensitivity of PTB7 in photooxidative conditions was instead related to attack of singlet oxygen on the conjugated backbone, with this attack shown to give rise to the formation of carbonylated species. In addition, traces of PTB7 oxidation, resulting from processing or very short durations of irradiation under ambient air, were found to result in a significant drop in solar cell performance. Also in this work, PTB7 was found to be more susceptible to photooxidation than was P3HT, in line with the higher instability of PTB7-based solar cells. The novel bottom-up approach implemented in this work revealed the importance of the formation of traces of polymer oxidation products in altering solar cell efficiency. The use of unstable materials is suspected to play a key role in the poor initial performances and/or reduced lifetimes of organic solar cells.
%G English
%L hal-02279282
%U https://hal.archives-ouvertes.fr/hal-02279282
%~ UNIV-PAU
%~ IPREM
%~ IPREM-CAPT
%~ ICC
%~ IMS-BORDEAUX-FUSION
%~ CNRS
%~ SIGMA-CLERMONT
%~ INC-CNRS
%~ PRES_CLERMONT
%~ IMS-BORDEAUX
%~ ANR
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Designing intrinsically photostable low band gap polymers: a smart tool combining EPR spectroscopy and DFT calculations.
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Institut de Chimie de Clermont-Ferrand (ICCF)
%+ Chemical Engineering and Applied Chemistry, Aston University, Birmingham
%+ Aston University [Birmingham]
%+ Structures et propriétés d'architectures moléculaire (SPRAM - UMR 5819)
%A Silva, Hugo Santos
%A Domínguez, Isabel Fraga
%A Perthué, Anthony
%A Topham, Paul D.
%A Bussière, Pierre-Olivier
%A Hiorns, Roger C.
%A Lombard, Christian
%A Rivaton, Agnès
%A Bégué, Didier
%A Pépin-Donat, Brigitte
%< avec comité de lecture
%@ 2050-7488
%J Journal of Materials Chemistry A
%I Royal Society of Chemistry
%V 4
%P 15647-15654
%8 2016
%D 2016
%R 10.1039/C6TA05455B
%Z Chemical SciencesJournal articles
%X A rapid and efficient method to identify the weak points of the complex chemical structure of low band gap (LBG) polymers, designed for efficient solar cells, when submitted to light exposure is reported. This tool combines Electron Paramagnetic Resonance (EPR) using the ‘spin trapping method’ coupled with density functional theory modelling (DFT). First, the nature of the short life-time radicals formed during the early-stages of photo-degradation processes are determined by a spin-trapping technique. Two kinds of short life-time radical (R˙ and R′O˙) are formed after ‘short-duration’ illumination in an inert atmosphere and in ambient air, respectively. Second, simulation allows the identification of the chemical structures of these radicals revealing the most probable photochemical process, namely homolytical scission between the Si atom of the conjugated skeleton and its pendent side-chains. Finally, DFT calculations confirm the homolytical cleavage observed by EPR, as well as the presence of a group that is highly susceptible to photooxidative attack. Therefore, the synergetic coupling of a spin trapping method with DFT calculations is shown to be a rapid and efficient method for providing unprecedented information on photochemical mechanisms. This approach will allow the design of LBG polymers without the need to trial the material within actual solar cell devices, an often long and costly screening procedure.
%G English
%L hal-01477694
%U https://hal.archives-ouvertes.fr/hal-01477694
%~ CNRS
%~ UNIV-PAU
%~ DSM-INAC
%~ INAC-SPRAM
%~ CEA
%~ UNIV-BPCLERMONT
%~ INC-CNRS
%~ ICC
%~ SIGMA-CLERMONT
%~ PRES_CLERMONT
%~ IPREM
%~ IPREM-CAPT
%~ IPREM-PCM
%~ CEA-DRF
%~ IRIG
%~ CEA-GRE
%~ UGA-COMUE
%~ UGA
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Increased thermal stabilization of polymer photovoltaic cells with oligomeric PCBM
%+ Instituto de Technologia de Materiales (ITM)
%+ Instituto de Física
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Ramanitra, H.H.
%A Dowland, S.A.
%A Bregadiolli, B.A.
%A Salvador, M.
%A Santos Silva, Hugo
%A Bégué, D.
%A Graeff, C.F.O.
%A Peisert, H.
%A Chassé, T.
%A Rajoelson, S.
%A Osvet, A.
%A Brabec, C.J.
%A Egelhaaf, H.-J.
%A Morse, G.E.
%A Distler, A.
%A Hiorns, R.C.
%< avec comité de lecture
%@ 2050-7526
%J Journal of Materials Chemistry C
%I Royal Society of Chemistry
%V 4
%N 34
%P 8121--8129
%8 2016
%D 2016
%R 10.1039/c6tc03290g
%K Photovoltaic devices
%K Polymer photovoltaic cells
%K Degree of crystallinity
%K Electronic properties
%K Energy gap
%K Esters
%K Free radical reactions
%K Glass transition
%K Heterojunctions
%K Oligomers
%K Optoelectronic properties
%K Organic photovoltaic devices
%K Photoelectrochemical cells
%K Photovoltaic cells
%K Butyric acid
%K Bulk heterojunction
%K Atom transfer radical polymerization
%K Atom transfer radical addition
%K Semiconducting polymers
%K Stabilization
%K Substrates
%K Thermal stabilization
%Z Chemical SciencesJournal articles
%X The first oligomerisation of phenyl-C61-butyric acid methyl ester (PCBM) using a facile atom transfer radical addition polymerization (ATRAP) and its exploitation for organic photovoltaic devices is described. Oligo\(phenyl-C61-butyric acid methyl ester)-alt-[1,4-bis(bromomethyl)-2,5-bis(octyloxy)benzene]\ (OPCBMMB) shows opto-electronic properties equivalent to those of PCBM but has a higher glass transition temperature. When mixed with various band gap semiconducting polymers, OPCBMMB delivers performances similar to PCBM but with an enhanced stabilization of the bulk heterojunction in photovoltaic devices on plastic substrates under thermal stress, regardless of the degree of crystallinity of the polymer and without changing opto-electronic properties. © 2016 The Royal Society of Chemistry.
%G English
%L hal-01495770
%U https://hal.archives-ouvertes.fr/hal-01495770
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ IPREM-CAPT
%~ IPREM-PCM
%~ TESTUPPA2
%~ UPPA-OA

