%0 Unpublished work
%T The relationship between meaning and intonation in non-exhaustive answers: evidence from Basque
%+ Euskal Herriko Unibertsitatea [Guipúzcoa] (EHU)
%A Elordieta, Gorka
%A Irurtzun, Aritz
%8 2011-11-27
%D 2011
%K intonation
%K focus
%K non-exhaustivity
%Z Humanities and Social Sciences/Linguistics/domain_shs.langue.phon
%Z Humanities and Social Sciences/Linguistics/domain_shs.langue.semanPreprints, Working Papers, ...
%X In this paper we analyze the intonational properties of a type of focus construction that has been understudied, represented by answers to wh-questions in which the constituent that fills the variable does not do so exhaustively, that is, it does not provide an exhaustive answer because the speaker cannot commit to asserting that the other potential alternative candidates to fill the variable are cancelled. This type of narrow focus, Non-Exhaustive Narrow Focus (NENF), is different from Exhaustive Narrow Focus (ENF), in which a constituent fills the variable of the question exhaustively, with a concomitant cancellation of the rest of the focal alternatives. Our claim is that natural languages have the means to distinguish ENF and NENF unambiguously through prosodic means. In the present study, we show that speakers of (Northern Bizkaian) Basque assign particular intonational features to answers to wh-questions that should be interpreted non-exhaustively. In our experiment, we measured peak scaling of accents in the subject and the verb in ENF and NENF utterances. The results show that NENF is distinguished from ENF in having a pitch accent on the verb with a higher F0 value, almost as if the verb were focalized. In fact, we compared the intonational patterns of NENF with Verum Focus constructions, in which the polarity of the event expressed by the verb is focalized, and there were no significant differences in the verbal peaks in NENF and VF. There were no significant differences in peak scaling in the subject's stressed syllable between ENF and NENF, and neither were there any differences between NENF and VF. The paper offers a semantic analysis of the differences between ENF and NENF, by claiming that NENF is a split focus construction, in which both the subject and the polarity (or rather, the pairing between the subject and the polarity) constitute the focus of the utterance.
%G English
%2 https://artxiker.ccsd.cnrs.fr/artxibo-00645207/document
%2 https://artxiker.ccsd.cnrs.fr/artxibo-00645207/file/Elordieta_Irurtzun_2010.pdf
%L artxibo-00645207
%U https://artxiker.ccsd.cnrs.fr/artxibo-00645207
%~ AO-LINGUISTIQUE
%~ SHS
%~ OR-COLLCODE
%~ ARTXIKER

%0 Journal Article
%T Galdegaia Bere Bakardadean
%+ Hizkuntzalaritza Teorikorako Taldea (HITT)
%+ Euskal Herriko Unibertsitatea [Guipúzcoa] (EHU)
%A Irurtzun, Aritz
%< sans comité de lecture
%@ 1273-3830
%J Lapurdum
%I Centre de recherche sur la langue et les textes basques IKER UMR 5478 CNRS
%N X
%P 95-125
%8 2005
%D 2005
%K izaera saemantiko-pragmatikoa
%K galdegaia
%Z Humanities and Social Sciences/Linguistics/Morphosyntax
%Z Humanities and Social Sciences/Linguistics/Semantics - PragmaticsJournal articles
%X Lan honetan galdegaiaren izaera semantiko-pragmatikoa aztertuko dut literaturan aipatu izan diren galdegai motak galdegai bakarrera ekartzeko proposatuz. 2. atalean galdegaiaren afera aurkeztuko dut autore desberdinek proposatu dituzten galdegaiaren natura kontrastibo, informatibo, aurresuposizional, ez-aurresuposizional eta abarrak laburbilduz.<br />Lan honetan, bi « forma logiko » desberdinduko dira : Forma Logikoa eta forma logikoa. (...) Autoreon iritzien aurkezpenaren ostean, heien argudiaketen kritika bat aurkeztuko dut eta nire proposamena egin : galdegai bat besterik ez dago, eta interpretazio-diferentziak epifenomenalak dira. Honela, 3. atalean Herburger (2000) lanean oinarrituz, galdegaiarentzat proposatzen dudan forma logikoa aurkeztuko dut. Galdegaien interpretazioen arteko aldea, ez da galdegaian berean egongo, egitura bakoitzaren kuantifikatzailean baizik : testuingururik gabeko esaldiek informazio berria besterik ez dute emango, hala gertakarien gaineko kuantifikatzaile existentzial batez definituak izango dira. Testuingurudun galdegaietan ordea, galdegai aurresuposizioa eta exhaustibitatea izango ditugu, testuinguruan aipatuak diren gertakarien deskripzio definituak direlako.
%G Basque
%2 https://artxiker.ccsd.cnrs.fr/artxibo-00291730v2/document
%2 https://artxiker.ccsd.cnrs.fr/artxibo-00291730/file/10-2005_art5.pdf
%L artxibo-00291730
%U https://artxiker.ccsd.cnrs.fr/artxibo-00291730
%~ SHS
%~ AO-LINGUISTIQUE
%~ OR-COLLCODE
%~ ARTXIKER

%0 Unpublished work
%T A Derivational Approach to the Focus Structure
%+ Euskal Herriko Unibertsitatea [Guipúzcoa] (EHU)
%A Irurtzun, Aritz
%8 2006-09-15
%D 2006
%K focus
%K nuclear stress
%K derivation
%K bare phrase structure
%Z Humanities and Social Sciences/Linguistics/MorphosyntaxOther publications
%X I present a derivational analysis of the construal of the focus structure of the<br />sentence. I propose that the [+F] feature is an optional formal feature that can be assigned<br />to multiple tokens of the numeration. Hence, the focus structure is derived via Merge<br />(creation of a set) in a strictly compositional way. I argue that with the adoption of this<br />theory we can circumvent many of the empirical and theoretical shortcomings of previous<br />Nuclear Stress Rule-based approaches, and, furthermore, that it allows us to account for<br />split focus constructions (answers to multiple Wh questions) in a natural way.
%G English
%2 https://artxiker.ccsd.cnrs.fr/artxibo-00108178/document
%2 https://artxiker.ccsd.cnrs.fr/artxibo-00108178/file/DAFS.pdf
%L artxibo-00108178
%U https://artxiker.ccsd.cnrs.fr/artxibo-00108178
%~ SHS
%~ AO-LINGUISTIQUE
%~ OR-COLLCODE
%~ ARTXIKER

%0 Book Section
%T Ohar batzuk nafar-lapurterazko galdera eta galdegai indartuez
%+ Euskal Herriko Unibertsitatea [Guipúzcoa] (EHU)
%+ Université de Nantes (UN)
%A Duguine, Maia
%A Irurtzun, Aritz
%B Gramatika Jaietan: Patxi Goenagaren Omenez
%E X. Artiagoitia & J.A. Lakarra
%I Gipuzkoako Foru Aldundia & UPV/EHU
%S ASJUren gehigarriak LI
%P 195-208
%8 2008-01-01
%D 2008
%K galdegaia
%K nafar-lapurtera
%K kontrastea
%K aurresuposizionalitatea
%K NZ-galderak
%K galdera
%Z Humanities and Social Sciences/Linguistics/Morphosyntax
%Z Humanities and Social Sciences/Linguistics/Semantics - PragmaticsBook sections
%X Lan honetan, nafar-lapurterazko galdera eta galdegai egitura berezi batzuk aztertzen ditugu, 'egitura indartuak' izendatzen ditugunak. Haien sintaxia eta semantikaren puntu nagusien deskribapen bat egiten dugu lehenik. Hitz hurrenkeraren aldetik, galdera eta galdegai egitura estandarrengandik hurbilak dira. Esaldi konplexu eta NZ edo galdegai anitzeko egiturei dagokienez, aldiz, oso jokamolde desberdina erakusten dute. Interpretazioaren aldetik, frantsesezko galdera eta erantzun estrategiekiko egiten dugun konparazioak erdibitutako perpausek bezalako jokabidea dutela erakusten du. Azkenik, egitura hauen analisi baterako ideiak iradokitzen ditugu. Egitura hauetan gertakarien deskripzio definitu bat dagoela proposatzen dugu, kuantifikatzaile exhaustibo batek sortzen duena. Analisi honek, erakusten dugu, lanaren lehen partean aurkeztutako egitura indartuen ezaugarri sintaktiko eta semantikoak azaltzen ditu
%G Basque
%2 https://artxiker.ccsd.cnrs.fr/artxibo-00607120/document
%2 https://artxiker.ccsd.cnrs.fr/artxibo-00607120/file/Duguine_Irurtzun_2007.Ohar_batzuk_Nafar-Lapurterazko_galdera_eta_galdegai_indartuen_inguruan.pdf
%L artxibo-00607120
%U https://artxiker.ccsd.cnrs.fr/artxibo-00607120
%~ SHS
%~ UNIV-NANTES
%~ AO-LINGUISTIQUE
%~ OR-COLLCODE
%~ ARTXIKER

%0 Conference Proceedings
%T Stress on Accent; Errenteria Basque Revisited
%+ Euskal Herriko Unibertsitatea [Guipúzcoa] (EHU)
%A Irurtzun, Aritz
%< avec comité de lecture
%J Proceedings of the International Congress of Phonetic Sciences
%I M.J. Solé, D. Recasens, eta J. Romero
%P 2075-2078
%8 2006-02-09
%D 2006
%K stress
%K accent
%K pitch
%K variation
%Z Humanities and Social Sciences/Linguistics/domain_shs.langue.phon
%Z Humanities and Social Sciences/Linguistics/domain_shs.langue.diagConference papers
%X In this paper we report an experiment of accentual<br />placement in Errenteria Basque. We compare data from<br />three generations of speakers with the data described in<br />previous literature. The results show a mixed accentual<br />system with marked and unmarked words (as previously<br />addressed in the literature). The relevant fact is that the<br />phonological shape of the current unmarked words is<br />different from the descriptions in the literature since the<br />current one is a stress-accent system and the previous one suggests to be a pitch-accent system. Based on diachronic data, we show the direction of the phonological change and point a possible explanation for the development of the new accentual system based on Hualde's hypothesis.
%G English
%2 https://artxiker.ccsd.cnrs.fr/artxibo-00000083/document
%2 https://artxiker.ccsd.cnrs.fr/artxibo-00000083/file/SOA.pdf
%L artxibo-00000083
%U https://artxiker.ccsd.cnrs.fr/artxibo-00000083
%~ SHS
%~ AO-LINGUISTIQUE
%~ OR-COLLCODE
%~ ARTXIKER

%0 Conference Proceedings
%T The Structure & Derivation of Split Focalization
%+ Hizkuntzalaritza Teorikorako Taldea (HITT)
%+ Euskal Herriko Unibertsitatea [Guipúzcoa] (EHU)
%A Irurtzun, Aritz
%< avec comité de lecture
%J Proceedings of the ESSLLI Workshop on Discourse Domains & Information Structure
%I C. Umbach & K. von Heusinger
%P 21-33
%8 2006-02-09
%D 2006
%K focus
%K topic
%K pair-list
%K single pair
%K question
%K answer
%Z Humanities and Social Sciences/Linguistics/domain_shs.langue.morp
%Z Humanities and Social Sciences/Linguistics/domain_shs.langue.phonConference papers
%X In this paper I propose a minimalist and derivational theory of the Focus Structure that explains in a straightforward way the focal patterns of the answers of multiple-Wh questions as instances of split focus structures.
%G English
%2 https://artxiker.ccsd.cnrs.fr/artxibo-00000084/document
%2 https://artxiker.ccsd.cnrs.fr/artxibo-00000084/file/SDSF.pdf
%L artxibo-00000084
%U https://artxiker.ccsd.cnrs.fr/artxibo-00000084
%~ SHS
%~ AO-LINGUISTIQUE
%~ OR-COLLCODE
%~ ARTXIKER

%0 Journal Article
%T How Children Develop Their Ability to Combine Words : A Network-Based Approach
%+ Universitat de les Illes Balears (UIB)
%+ Centre de recherche sur la langue et les textes basques (IKER)
%+ Universitat Autònoma de Barcelona (UAB)
%A Barceló-Coblijn, Lluís
%A Irurtzun, Aritz
%A López-Navarro, Emilio
%A Real Pugdollers, Cristina
%A Gomila, Antoni
%< avec comité de lecture
%@ 1059-7123
%J Adaptive Behavior
%I SAGE Publications
%V 27
%N 5
%P 307--330
%8 2019
%D 2019
%R 10.1177/1059712319847993
%Z Humanities and Social Sciences/LinguisticsJournal articles
%G English
%L hal-02553637
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02553637
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T De-Syntacticising Syntax ? Concerns on the Architecture of Grammar and the Role of Interface Components
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Irurtzun, Aritz
%< avec comité de lecture
%@ 1695-6885
%J Catalan Journal of Linguistics
%I UAB édition
%S Generative Syntax. Questions, Crossroads, and Challenges
%P 165--202
%8 2019
%D 2019
%R 10.5565/rev/catjl.231
%K syntax
%K interfaces
%K architecture of grammar
%K bare output conditions
%K modularity
%Z Humanities and Social Sciences/LinguisticsJournal articles
%X This article discusses different ways in which interface components could potentially affect syntax (or what have traditionally been analysed as syntactic phenomena). I will distinguish four types of potential effects that the interface components could have onto syntax: (i) no real interaction, since almost nothing pertains to syntax: everything (beyond Merge) is externalization; (ii) computations at interface components actively affect the syntactic computation; (iii) Properties of interface representations function to inform biases for language acquisition; (iv) interface components impose Bare Output Conditions (legibility conditions) that constrain the range of possible syntactic representations at the interface. I argue that the first two are problematic, whereas the latter two may help us understanding a range of universal and variable phenomena.
%G English
%L hal-02553893
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02553893
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS
%~ TESTUPPA2
%~ UPPA-OA

%0 Book Section
%T Pitch Accent Tonal Alignment in Declarative Sentences in the Spanish of the Basque Country : A Study of Language Contact
%+ Euskal Herriko Unibertsitatea [Guipúzcoa] (EHU)
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Elordieta, Gorka
%A Irurtzun, Aritz
%@ 978-90-272-5805-2
%B Intonational Grammar in Ibero-Romance : Approaches across Linguistic Subfields
%E Henriksen Nicholas
%E Armstrong Meghan
%E Vanrell María del Mar
%I John Benjamins
%P 25--44
%8 2016
%D 2016
%R 10.1075/ihll.6.02elo
%Z Humanities and Social Sciences/LinguisticsBook sections
%G English
%L hal-02551861
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02551861
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T The emergence of scalar meanings
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Etxeberria, Urtzi
%A Irurtzun, Aritz
%< avec comité de lecture
%@ 1664-1078
%J Frontiers in Psychology
%I Frontiers
%N 6:141
%8 2015
%D 2015
%R 10.3389/fpsyg.2015.00141
%Z Humanities and Social Sciences/LinguisticsJournal articles
%G English
%L hal-02496972
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02496972
%~ IKER
%~ UNIV-PAU
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS
%~ TESTUPPA2
%~ UPPA-OA

%0 Book Section
%T Why Questions Break the Residual V2 Restriction (in Basque and Beyond)
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Irurtzun, Aritz
%Z PGC2018-096870-B-I00 and FFI2017-87140-C4-1-P (MINECO)
%@ 9783110675115
%B Why is 'Why' Unique? Its Syntactic and Semantic Properties
%E Gabriela Soare
%I De Gruyter Mouton
%8 2021
%D 2021
%Z Humanities and Social Sciences/LinguisticsBook sections
%G English
%2 https://hal.archives-ouvertes.fr/hal-03181218/document
%2 https://hal.archives-ouvertes.fr/hal-03181218/file/Irurtzun_why.pdf
%L hal-03181218
%U https://hal.archives-ouvertes.fr/hal-03181218

%0 Journal Article
%T Gravettian hand stencils as sign language formatives
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Etxepare, Ricardo, Rikardo, R.
%A Irurtzun, Aritz
%< avec comité de lecture
%@ 0962-8436
%J Philosophical Transactions of the Royal Society B: Biological Sciences
%I Royal Society, The
%V 376
%N 1824
%8 2021-05-10
%D 2021
%R 10.1098/rstb.2020.0205
%Z Humanities and Social Sciences/LinguisticsJournal articles
%X Several Upper Palaeolithic archaeological sites from the Gravettian period display hand stencils with missing fingers. On the basis of the stencils that Leroi-Gourhan identified in the cave of Gargas (France) in the late sixties, we explore the hypothesis that those stencils represent hand signs with deliberate folding of fingers, intentionally projected as a negative figure onto the wall. Through a study of the biomechanics of handshapes, we analyze the articulatory effort required for producing the handshapes under the stencils in the Gargas cave, and show that only handshapes that are articulable in the air can be found among the existing stencils. In other words, handshape configurations that would have required using the cave wall as a support for the fingers are not attested. We argue that the stencils correspond to the type of handshape that one ordinarily finds in sign language phonology. More concretely, we claim that they correspond to signs of an 'alternate' or 'non-primary' sign language, like those still employed by a number of bi-modal (speaking and signing) human groups in hunter-gatherer populations, like the Australian first nations or the Plains Indians. In those groups, signing is used for hunting and for a rich array of ritual purposes, including mourning and traditional story-telling. We discuss further evidence, based on typological generalizations about the phonology of non-primary sign-languages and comparative ethnographic work that points to such a parallelism. This evidence includes the fact that for some of those groups, stencil and petroglyph art has independently been linked to their sign language expressions.
%G English
%2 https://hal.archives-ouvertes.fr/hal-03181202/document
%2 https://hal.archives-ouvertes.fr/hal-03181202/file/Etx%26Iru2021.pdf
%L hal-03181202
%U https://hal.archives-ouvertes.fr/hal-03181202
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS

%0 Journal Article
%T Noam Chomsky: Hizkuntza Teoria eta Hizkuntzaren Filosofia
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Etxepare, Ricardo, Rikardo, R.
%A Irurtzun, Aritz
%< sans comité de lecture
%@ 1577-9424
%J Gogoa
%I Bilbao : Institute for Logic, Cognition, Language and Information (ILCLI) and University of the Basque Country (Publisher Services)
%V VII
%P 1-65
%8 2007
%D 2007
%K Universal Grammar
%K I-language
%K language faculty
%K language acquisition
%K generative grammar
%K hizkuntza jabekuntza
%K I-hizkuntza
%K hizkuntza gaitasuna
%K gramatika unibertsala
%K Chomsky
%K gramatika generatiboa
%Z Humanities and Social Sciences/Linguistics/domain_shs.langue.morpJournal articles
%G Basque
%2 https://artxiker.ccsd.cnrs.fr/artxibo-00167304/document
%2 https://artxiker.ccsd.cnrs.fr/artxibo-00167304/file/Noam_Chomsky_-_Hizkuntzaren_teoria_eta_hizkuntzaren_filosofia.pdf
%L artxibo-00167304
%U https://artxiker.ccsd.cnrs.fr/artxibo-00167304
%~ CNRS
%~ UNIV-PAU
%~ SHS
%~ AO-LINGUISTIQUE
%~ IKER
%~ UNIV-BORDEAUX-MONTAIGNE
%~ OR-COLLCODE
%~ ARTXIKER
%~ TESTUPPA2
%~ UPPA-OA

%0 Unpublished work
%T Focus & Clause Structuration in the Minimalist Program
%+ Centre de recherche sur la langue et les textes basques (IKER)
%+ Hizkuntzalaritza Teorikorako Taldea (HITT)
%+ Euskal Herriko Unibertsitatea [Guipúzcoa] (EHU)
%A Irurtzun, Aritz
%8 2005
%D 2005
%K Focus
%K Quantification
%K Logical Form
%K Event
%K Reprojection
%Z Humanities and Social Sciences/Linguistics/domain_shs.langue.morp
%Z Humanities and Social Sciences/Linguistics/domain_shs.langue.semanOther publications
%X This article explores the possibility that the derivation of focus from syntax to LF can be implemented in a direct and transparent way. Assuming the principles of Bare Phrase Structure, I develop a derivational construal of the focus structure and provide a syntax of focus in Basque combining it with an eventish logical form whereby the focus falls in the scope of a binary existential quantifier (cf. Herburger (2000)). Adopting recent ideas of Hornstein & Uriagereka (2002), this<br />transparent interface is accomplished by allowing binary quantifiers to reproject at LF to get their nuclear scope.
%G English
%2 https://artxiker.ccsd.cnrs.fr/artxibo-00000022/document
%2 https://artxiker.ccsd.cnrs.fr/artxibo-00000022/file/FCSMP.pdf
%L artxibo-00000022
%U https://artxiker.ccsd.cnrs.fr/artxibo-00000022
%~ CNRS
%~ UNIV-PAU
%~ SHS
%~ AO-LINGUISTIQUE
%~ IKER
%~ UNIV-BORDEAUX-MONTAIGNE
%~ OR-COLLCODE
%~ ARTXIKER
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T The “Globularization Hypothesis” of the Language-ready Brain as a Developmental Frame for Prosodic Bootstrapping Theories of Language Acquisition
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Irurtzun, Aritz
%< avec comité de lecture
%@ 1664-1078
%J Frontiers in Psychology
%I Frontiers
%V 6
%P 1817
%8 2015
%D 2015
%R 10.3389/fpsyg.2015.01817
%K postnatal development
%K language acquisition
%K language development
%K prosodic bootstrapping
%K globularization
%Z Humanities and Social Sciences/LinguisticsJournal articles
%X In recent research (Boeckx and Benítez-Burraco, 2014a,b) have advanced the hypothesis that our species-specific language-ready brain should be understood as the outcome of developmental changes that occurred in our species after the split from Neanderthals-Denisovans, which resulted in a more globular braincase configuration in comparison to our closest relatives, who had elongated endocasts. According to these authors, the development of a globular brain is an essential ingredient for the language faculty and in particular, it is the centrality occupied by the thalamus in a globular brain that allows its modulatory or regulatory role, essential for syntactico-semantic computations. Their hypothesis is that the syntactico-semantic capacities arise in humans as a consequence of a process of globularization, which significantly takes place postnatally (cf. Neubauer et al., 2010). In this paper, I show that Boeckx and Benítez-Burraco's hypothesis makes an interesting developmental prediction regarding the path of language acquisition: it teases apart the onset of phonological acquisition and the onset of syntactic acquisition (the latter starting significantly later, after globularization). I argue that this hypothesis provides a developmental rationale for the prosodic bootstrapping hypothesis of language acquisition (cf. i.a. Gleitman and Wanner, 1982; Mehler et al., 1988, et seq.; Gervain and Werker, 2013), which claim that prosodic cues are employed for syntactic parsing. The literature converges in the observation that a large amount of such prosodic cues (in particular, rhythmic cues) are already acquired before the completion of the globularization phase, which paves the way for the premises of the prosodic bootstrapping hypothesis, allowing babies to have a rich knowledge of the prosody of their target language before they can start parsing the primary linguistic data syntactically.
%G English
%2 https://hal.archives-ouvertes.fr/hal-02017982/document
%2 https://hal.archives-ouvertes.fr/hal-02017982/file/fpsyg-06-01817.pdf
%L hal-02017982
%U https://hal.archives-ouvertes.fr/hal-02017982
%~ IKER
%~ CNRS
%~ UNIV-PAU
%~ AO-LINGUISTIQUE
%~ SHS
%~ UNIV-BORDEAUX-MONTAIGNE
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Hots Ahostunak Euskal Hiztegian
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Irurtzun, Aritz
%Z Lan hau ondoko dirulaguntzei esker burutu da: GIC07/144-IT-210-07 (EJ), 2011 JSH2 004 1 (ANR),FFI2011-29218, (MICINN), FFI2012-38064-C02-01 (MINECO), BCS-1147083 (NSF), HiTeDi, UFI11/14(UPV/EHU). Eskerrik asko Maia Duguine eta Gorka Elordietari haien iruzkinengatik.
%< avec comité de lecture
%@ 1273-3830
%J Lapurdum
%I Centre de recherche sur la langue et les textes basques IKER UMR 5478 CNRS
%S Lapurdum XV
%V XV
%P 101-141
%8 2011
%D 2011
%R 10.4000/lapurdum.2332
%K euskara
%K hiztegia
%K intonazioa
%K soinu ahostunak
%K 5.535 hitz
%Z Humanities and Social Sciences/Linguistics/domain_shs.langue.phon
%Z Humanities and Social Sciences/Linguistics/domain_shs.langue.lexicoJournal articles
%X Intonazioaren inguruko ikerketetan, arazo metodologiko nagusi bat izaten da galdeketetarako corpusa osatzean, izan ere, corpusa ezaugarri fonetiko-fonologiko hertsien arabera eraiki behar da uhinaren oinarrizko maiztasuna (F0, Hertz-etan neurtzen dena) jaso ahal izateko. Behaztopa-harri nagusia fonema ahoskabeen presentzia izaten da, fonazioa etetean, F0 uhina eteten da eta etenak irauten duen milisegundoetan ezin da neurketarik egin. Areago, mikroprosodiaren eraginez arazoa ez da etenetara mugatzen, eta etenaren osteko fonazioan uhinak etenik izango ez balitz izango ez lituzkeen balioak izaten ditu (nabarmenki, leherkarien osteko balioak igo egiten dira cf., i.a., Hanson (2009)). Hori dela-eta, laborategiko fonologia-ikerketak egiten dira intonazioan, hizketa libreak, oro har, zarata esperimental handia izaten baitu eta honenbestez, ez dituelako eskaintzen konparaziorako egokiak diren datuak. Horretarako, soinu ahostunez osatutako galdetegiak behar izaten dira, eta batzuetan zaila gertatzen da horiek egiteko hitz egokiak aurkitzea. Honela bada, jarraian datorrena intonazioaren ikerketan baliagarri izan daitekeen corpus bat da, soilik ahostunak diren soinuez osaturikoa.PERLen inguruan ikasi nahi izanez gero begiratu bat eman Astigarraga-eta (2009) lanari. Hemen oina (...)Corpusa osatzeko Xuxen hiztegia arakatu dut eta soilik ahostunez osatutako hitzak erauzi (horretarako Unix eta PERL script pare bat baliatu ditut).1 80.778 hitzez osatutako corpusetik 5.535 hitz erauzi dira ; behean ematen ditut intonazioaren ikerketarako galdetegien prestaketan lagungarri izan daitezkeelakoan.
%G Basque
%2 https://artxiker.ccsd.cnrs.fr/artxibo-01201025/document
%2 https://artxiker.ccsd.cnrs.fr/artxibo-01201025/file/lapurdum-2332-15-hots-ahostunak-euskal-hiztegian.pdf
%L artxibo-01201025
%U https://artxiker.ccsd.cnrs.fr/artxibo-01201025
%~ CNRS
%~ UNIV-PAU
%~ AO-LINGUISTIQUE
%~ SHS
%~ IKER
%~ UNIV-BORDEAUX-MONTAIGNE
%~ OR-COLLCODE
%~ ARTXIKER
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Editorial: Approaches to Language: Data, Theory, and Explanation
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Gallego, Ángel
%A Irurtzun, Aritz
%< avec comité de lecture
%@ 1664-1078
%J Frontiers in Psychology
%I Frontiers
%V 11
%8 2020-10-22
%D 2020
%R 10.3389/fpsyg.2020.576244
%Z Humanities and Social Sciences/LinguisticsJournal articles
%G English
%L hal-03092938
%U https://hal.archives-ouvertes.fr/hal-03092938
%~ AO-LINGUISTIQUE
%~ UNIV-PAU
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ IKER
%~ SHS
%~ TESTUPPA2
%~ UPPA-OA

%0 Book Section
%T Strategies for argument and adjunct focalization in Basque
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Irurtzun, Aritz
%B Microparameters in the grammar of Basque
%I John Benjamins
%P 243-264
%8 2016
%D 2016
%R 10.1075/lfab.13.09iru
%K wh in situ
%K left periphery
%K focus
%K Wh-movement
%Z Humanities and Social Sciences/LinguisticsBook sections
%X In this chapter, I provide an overview of the different strategies employed for Wh-questions and focalizations across Basque dialects. I argue that a core property of Basque syntax is the fact that both Wh-and focus phrases undergo syntactic (A'-type) displacements, and that they exhibit the main characteristics of syntactic displacements (locality, successive cyclicity, sensitivity to islands, etc.). After analysing the " standard strategy " , which is available across all dialects, I provide an overview of the new in situ Wh-question strategy of the young speakers of Navarro-Labourdin and two different strategies that are employed across different dialects to generate reinforced foci: the highly contrastive rightward focus constructions (specific to Southern dialects, and particularly common in High Navarrese), and the 'reinforced movement' strategy of Navarro-Labourdin (a Northern variety). I finish with a brief description of some other constructions involving foci: the mirative focus constructions of substandard Basque, and the dialectal distribution of different types of split interrogatives.
%G English
%2 https://artxiker.ccsd.cnrs.fr/artxibo-01491771/document
%2 https://artxiker.ccsd.cnrs.fr/artxibo-01491771/file/Irurtzun%202016.Strategies%20for%20argument%20and%20adjunct%20focalization%20in%20Basque.pdf
%L artxibo-01491771
%U https://artxiker.ccsd.cnrs.fr/artxibo-01491771
%~ CNRS
%~ UNIV-PAU
%~ AO-LINGUISTIQUE
%~ SHS
%~ IKER
%~ UNIV-BORDEAUX-MONTAIGNE
%~ OR-COLLCODE
%~ ARTXIKER
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T The prosody and interpretation of non-exhaustive narrow focus in Basque
%+ Centre de recherche sur la langue et les textes basques (IKER)
%+ Euskal Herriko Unibertsitatea [Guipúzcoa] (EHU)
%A Irurtzun, Aritz
%A Elordieta, Gorka
%< sans comité de lecture
%@ 0582-6152
%J Anuario del Seminario de Filología Vasca "Julio de Urquijo"
%I ASJU-Universidad del Pais Vasco
%N XLIII, 1-2
%P 205-230
%8 2009
%D 2009
%K interpretation
%K non-exhaustive narrow focus
%K Basque
%K prosody
%Z Humanities and Social Sciences/Linguistics/domain_shs.langue.phon
%Z Humanities and Social Sciences/Linguistics/domain_shs.langue.semanJournal articles
%G English
%2 https://artxiker.ccsd.cnrs.fr/artxibo-00699173/document
%2 https://artxiker.ccsd.cnrs.fr/artxibo-00699173/file/Irurtzun_ASJU_2009.pdf
%L artxibo-00699173
%U https://artxiker.ccsd.cnrs.fr/artxibo-00699173
%~ SHS
%~ CNRS
%~ UNIV-PAU
%~ AO-LINGUISTIQUE
%~ IKER
%~ UNIV-BORDEAUX-MONTAIGNE
%~ OR-COLLCODE
%~ ARTXIKER
%~ TESTUPPA2
%~ UPPA-OA

