%0 Journal Article
%T Gas solubility of carbon dioxide in poly(lactic acid) at high pressures: Thermal treatment effect
%+ CICECO
%+ Colorado School of Mines
%+ Thermodynamique et Energétique des fluides complexes (TEFC)
%A Oliveira, N.S.
%A Dorgan, J.
%A Coutinho, J., A.P.
%A Ferreira, A.
%A Daridon, Jean-Luc
%A Marrucho, Isabel, M.
%< avec comité de lecture
%@ 0449-2986
%J Journal of polymer science. Part B: Polymer letters
%I John Wiley & Sons
%V 45
%N 5
%P 616-625
%8 2007-01-24
%D 2007
%R 10.1002/polb.20969
%K biodegradable polymer • high pressure • poly(lactic acid) (PLA) • quartz crystal microbalance • sorption • thermal treatment
%Z Physics [physics]/Physics [physics]/Chemical Physics [physics.chem-ph]Journal articles
%X The sorption of carbon dioxide in glassy Poly(lactic acid) (PLA) films was studied by quartz crystal microbalance (QCM) at high pressures. Two thermal treatments, melted and quenched, were performed in PLA with two different L:D contents, 80:20 and 98:2, films and compared with a third thermal protocol, annealed, and used in a previous work. The results obtained show that for pressures higher than 2 MPa, the carbon dioxide solubility is larger in PLA 80:20 than in PLA 98:2, indicating that the L:D plays a dominant role on this property. The thermal treatments only affect the gas solubility in PLA 98:2. Sorption isotherms at temperatures 303, 313, and 323 K, below the glass transition temperature of the polymer, and pressures up to 5 MPa were measured and analyzed with three different models, the dual-mode sorption model, the Flory-Huggins equation, and a modified dual-mode sorption model where the Henry's law term was substituted by the Flory-Huggins equation. This last model performs especially well for CO2 in PLA 80:20, due to the convex upward curvature of the solubility isotherms for that system.
%G English
%L hal-00339088
%U https://hal.archives-ouvertes.fr/hal-00339088
%~ CNRS
%~ UNIV-PAU
%~ LFCR
%~ LFCR-PTP
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Determination of bubble point pressure of two live oils with injected nitrogen by quartz crystal resonator
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Orlandi, Ezequiel
%A Daridon, Jean-Luc
%A Carrier, Hervé
%Z ACL
%< avec comité de lecture
%@ 1951-6355
%J The European Physical Journal. Special Topics
%I EDP Sciences
%V 226
%N 5
%P 1065-1073
%8 2017
%D 2017
%R 10.1140/epjst/e2016-60271-5
%Z Physics [physics]/Condensed Matter [cond-mat]/Materials Science [cond-mat.mtrl-sci]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Fluids mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of materials [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Mechanical engineering [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Mechanics of materials [physics.class-ph]
%Z Sciences of the Universe [physics]/Earth Sciences/Geophysics [physics.geo-ph]Journal articles
%X An experimental investigation of bubble pressure determination of two live oils with injected nitrogen is presented in this work. Conventional bubble point measurement is done with the aid of a constant expansion test. In such method the bubble point is characterized by an inflection in the PV curve as the oil becomes saturated. For some oils the PV curve is smoothed and it becomes difficult or impossible to accurately determine the bubble point. This work demonstrates that in such cases, the use of quartz crystal resonator technique makes it possible. This technique is successfully applied to determine the bubble point pressure of two live oils in which nitrogen is injected up to 40 mol%.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01816746/document
%2 https://hal.archives-ouvertes.fr/hal-01816746/file/orlandi2017.pdf
%L hal-01816746
%U https://hal.archives-ouvertes.fr/hal-01816746
%~ CNRS
%~ UNIV-PAU
%~ LFCR
%~ LFCR-PTP
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Reference Correlation of the Viscosity of Squalane from 273 to 373 K at 0.1 MPa
%+ Laboratorio de Propiedades Termofísicas
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%+ School of Physical, Environmental and Mathematical Sciences
%+ Laboratory of Thermophysical Properties and Environmental Processes
%A Comunas, M.J.P.
%A Paredes, X.
%A Gacino, F.M.
%A Fernandez, J.
%A Bazile, Jean-Patrick
%A Boned, Christian
%A Daridon, Jean-Luc
%A Galliero, Guillaume
%A Pauly, Jérôme
%A R. Harris, K.
%A J. Assael, M.
%A K. Mylona, S.
%< avec comité de lecture
%@ 0047-2689
%J Journal of Physical and Chemical Reference Data
%I American Institute of Physics
%V 42
%N 3
%8 2013
%D 2013
%R 10.1063/1.4812573
%K reference correlation
%K squalane
%K transport properties
%K viscosity
%Z Physics [physics]/Physics [physics]/Chemical Physics [physics.chem-ph]Journal articles
%X The paper presents a new reference correlation for the viscosity of squalane at 0.1 MPa. The correlation should be valuable as it is the first to cover a moderately high viscosity range, from 3 to 118 mPa s. It is based on new viscosity measurements carried out for this work, as well as other critically evaluated experimental viscosity data from the literature. The correlation is valid from 273 to 373 K at 0.1 MPa. The average absolute percentage deviation of the fit is 0.67, and the expanded uncertainty, with a coverage factor k = 2, is 1.5%.
%G English
%2 https://hal.archives-ouvertes.fr/hal-00856696/document
%2 https://hal.archives-ouvertes.fr/hal-00856696/file/Comunas2013.pdf
%L hal-00856696
%U https://hal.archives-ouvertes.fr/hal-00856696
%~ CNRS
%~ UNIV-PAU
%~ LFCR
%~ LFCR-PTP
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Reference Correlations for the Density and Viscosity of Squalane from 273 to 473 K at Pressures to 200 MPa
%+ Laboratory of Thermophysical Properties and Environmental Processes
%+ Laboratorio de Propiedades Termofísicas
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%+ University of New South Wales - Australian Defence Force Academy (UNSW@ADFA)
%A K. Mylona, S.
%A J. Assael, M.
%A Comunas, M.J.P.
%A Paredes, X.
%A Gacino, F.M.
%A Fernandez, Josefa
%A Bazile, Jean-Patrick
%A Boned, Christian
%A Daridon, Jean-Luc
%A Galliero, Guillaume
%A Pauly, Jérôme
%A Harris, Kenneth, R.
%< avec comité de lecture
%@ 0047-2689
%J Journal of Physical and Chemical Reference Data
%I American Institute of Physics
%P 013104-1
%8 2014-03-07
%D 2014
%R 10.1063/1.4863984
%Z Physics [physics]/Physics [physics]/Chemical Physics [physics.chem-ph]Journal articles
%X This paper presents new reference correlations for both the density and viscosity of squalane at high pressure. These correlations are based on critically evaluated experimental data taken from the literature. In the case of the density, the correlation, based on the Tait equation, is valid from 273 to 473 K at pressures to 200 MPa. At 0.1 MPa, it has an average absolute deviation of 0.03%, a bias of 0.01%, and an expanded uncertainty (at the 95% confidence level) of 0.06%. Over the whole range of pressures, the density correlation has an average absolute deviation of 0.05%, a bias of 0.004%, and an expanded uncertainty (at the 95% confidence level) of 0.18%. In the case of the viscosity, two correlations are presented, one a function of density and temperature, based on the Assael-Dymond model, and the other a function of temperature and pressure, based on a modified Vogel-Fulcher- Tammann equation. The former is slightly superior to the latter at high temperatures (above 410 K), whereas the reverse is true at low temperatures, where the viscosity is strongly temperature dependent. In the temperature range from 320 to 473 K at pressures to 200 MPa, the first correlation has an average absolute deviation of 1.41%, a bias of 0.09%, and an expanded uncertainty (at the 95% confidence level) of 3%. Below 320 K, deviations from the present scheme rise to a maximum of 20%. In the temperature range from 278 to 473 K at pressures to 200 MPa, the second viscosity correlation has an average absolute deviation of 1.7%, a bias of 0.04%, and an expanded uncertainty (at the 95% confidence level) of 4.75%.
%G English
%L hal-01144397
%U https://hal.archives-ouvertes.fr/hal-01144397
%~ CNRS
%~ UNIV-PAU
%~ LFCR
%~ LFCR-PTP
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T High pressure solubility data of carbon dioxide in (tri-iso-butyl(methyl)phosphonium tosylate + water) systems
%+ CICECO - Departemento de quimica
%+ Thermodynamique et Energétique des fluides complexes (TEFC)
%+ TecMinho e-Learning Center
%+ Centre of Biological Engineering [Univ. Minho] (IBB-CBE)
%A Ventura, Sonia, P.M.
%A Pauly, Jérôme
%A Daridon, Jean-Luc
%A Lopes Da Silva, J.A.
%A Marrucho, Isabel, M.
%A Dias, Ana, M. A.
%A Coutinho, J., A.P.
%< avec comité de lecture
%@ 0021-9614
%J Journal of Chemical Thermodynamics
%I Elsevier
%V 40
%N 8
%P 1187-1192
%8 2008-04-12
%D 2008
%R 10.1016/j.jct.2008.04.012
%K Gas solubility
%K High pressure
%K Ternary system
%K Carbon dioxide
%K Phosphonium
%K Water
%Z Physics [physics]/Physics [physics]/Chemical Physics [physics.chem-ph]Journal articles
%X Ionic liquids are attracting great attention nowadays due to their interesting properties which make them useful in a broad range of applications including reaction media or separation/capture of environmentally hazardous gases such as carbon dioxide. In many cases, for practical and/or economical reasons, the use of aqueous solutions of ILs would be preferable to their use as pure compounds. In this work, high pressure equilibrium data for the {carbon dioxide (CO2) + tri-iso-butyl(methyl)phosphonium tosylate [iBu3MeP][TOS] + water system were measured at temperatures ranging from (276 to 370) K and pressures up to 100 MPa. Measurements were performed using a high-pressure cell with a sapphire window that allows direct observation of the liquid–vapour transition. Mixtures with different IL concentrations were studied in order to check the influence of the amount of IL on the solubility of CO2 in the aqueous mixture. The results show that the presence of IL enhances the solubility of CO2 in the (IL + water) system revealing a salting-in effect of the IL on the solubility of CO2. The appearance of a three phase region was observed for IL concentrations higher than 4 mol% of IL in water when working at pressures between 4 and 8 MPa and temperatures between (280 and 305) K. In this range, the upper limit of the VLE region observed is shown to increase with the temperature being almost independent of the IL initial concentration in the mixture.
%G English
%L hal-00339108
%U https://hal.archives-ouvertes.fr/hal-00339108
%~ CNRS
%~ UNIV-PAU
%~ LFCR
%~ LFCR-PTP
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T A quartz crystal microbalance technique to study wax crystallization in the presence of gas
%+ Thermodynamique et Energétique des fluides complexes (TEFC)
%+ Laboratoire de Génie Electrique (LGE)
%+ Centro de Investigacao em Materiais Ceramicaos e Compositos (CICECO)
%A Cassiede, M.
%A Pauly, Jérôme
%A Milhet, Michel
%A Rivaletto, Marc
%A Marrucho, Isabel, M.
%A Coutinho, J., A.P.
%A Daridon, Jean-Luc
%< avec comité de lecture
%@ 0957-0233
%J Measurement Science and Technology
%I IOP Publishing
%V 19
%N 6
%P art. no. 065704
%8 2008-05-08
%D 2008
%R 10.1088/0957-0233/19/6/065704
%K quartz crystal microbalance
%K phase transitions
%K solid–liquid–vapour equilibria
%K n-alkanes
%K carbon dioxide
%Z Physics [physics]/Physics [physics]/Chemical Physics [physics.chem-ph]Journal articles
%X A quartz crystal microbalance (QCM) was designed to measure the melting temperature in the presence of gas under high pressure. The capacity of the experimental technique to determine the liquid–solid as well as the solid–solid transitions was first tested by measuring the phase transition temperatures of several pure n-alkanes from n-C19 to n-C30 under atmospheric pressure and by comparing the results with literature data. Then, the wax disappearance conditions were determined for linear alkanes from n-C20 to n-C22 in the presence of carbon dioxide under pressure. The resulting solid–liquid–vapour three-phase equilibrium curves correctly match those obtained in earlier studies.
%G English
%L hal-00339107
%U https://hal.archives-ouvertes.fr/hal-00339107
%~ CNRS
%~ UNIV-PAU
%~ LFCR
%~ LFCR-PTP
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Corresponding-States Modeling of the Speed of Sound of Long-Chain Hydrocarbons
%+ CICECO
%+ Thermodynamique et Energétique des fluides complexes (TEFC)
%A Qeimada, A.J.
%A Coutinho, J., A.P.
%A Marrucho, Isabel, M.
%A Daridon, Jean-Luc
%< avec comité de lecture
%@ 0195-928X
%J International Journal of Thermophysics
%I Springer Verlag
%V 27
%N 4
%P 1095-1109
%8 2006-07-04
%D 2006
%R 10.1007/s10765-006-0105-7
%K corresponding-states
%K high pressure
%K model
%K n-alkanes
%K paraffinic wax
%K speed of sound
%Z Physics [physics]/Physics [physics]/Chemical Physics [physics.chem-ph]Journal articles
%X Models based on the corresponding-states principle have been extensively used for several equilibrium and transport properties of different pure and mixed fluids. Some limitations, however, have been encountered with regard to its application to long chain or polar molecules. Following previous studies, where it was shown that the corresponding-states principle could be used to predict thermophysical properties such as vapor–liquid interfacial tension, vapor pressure, liquid density, viscosity, and thermal conductivity of long-chain alkanes, the application of the corresponding-states principle to the estimation of speeds of sound, with a special emphasis on the less studied heavier n-alkane members, is presented. Results are compared with more than four thousand experimental data points as a function of temperature and pressure for n-alkanes ranging from ethane up to n-hexatriacontane. Average deviations are less than 2%, demonstrating the reliability of the proposed model for the estimation of speeds of sound.
%G English
%L hal-00338162
%U https://hal.archives-ouvertes.fr/hal-00338162
%~ CNRS
%~ UNIV-PAU
%~ LFCR
%~ LFCR-PTP
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Modeling high-pressure wax formation in petroleum fluids
%+ Thermodynamique et Energétique des fluides complexes (TEFC)
%+ CICECO
%A Sansot, Jean-Marc
%A Pauly, Jérôme
%A Daridon, Jean-Luc
%A Coutinho, J., A.P.
%< avec comité de lecture
%@ 0001-1541
%J AIChE Journal
%I Wiley
%V 51
%N 7
%P 2089-2097
%8 2005-05-16
%D 2005
%R 10.1002/aic.10434
%K solid-fluid equilibrium • wax • petroleum • high-pressure • wax appearance temperature (WAT)
%Z Physics [physics]/Physics [physics]/Chemical Physics [physics.chem-ph]Journal articles
%X procedure is presented for the simultaneous prediction of fluid-fluid and solid-fluid equilibrium of light gases/heavy hydrocarbons systems under pressure. The objective of this work is to simplify the predictive approach previously proposed by the authors, making it more useful and accessible to users at the petroleum industry. The main modification with respect to the complex LCVM (linear combination of Huron-Vial and Michelsen) mixing rule that was previously used for the attractive term a of the equation of state (EOS). To avoid the use of a group contribution method, which is difficult to bring into play with real fluids, the LCVM mixing rule is here replaced by the classical quadratic van der Walls one-fluid mixing rule. The deviation that using this mixing rule introduces into the gamma-phi approach is cancelled by a new parameter , introduced in the calculation of the solid-phase activity coefficient. This procedure was tested for a large range of multicomponent systems where the parameter was fitted to match the wax appearance temperature (WAT) at atmospheric pressure. From the values obtained a correlation relating the parameter to the carbon number in the heavy fraction was developed, allowing a prediction of wax formation in petroleum fluids. It is shown that this approach can successfully be applied to the prediction of wax formation at high pressure for live oils.
%G English
%L hal-00338122
%U https://hal.archives-ouvertes.fr/hal-00338122
%~ UNIV-PAU
%~ CNRS
%~ LFCR
%~ LFCR-PTP
%~ TESTUPPA
%~ TESTUPPA2

%0 Journal Article
%T In Pursuit of a High-Temperature, High-Pressure, High-Viscosity Standard: The Case of Tris(2-ethylhexyl) Trimellitate
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%+ Laboratorio de Propiedades Termofísicas
%+ Departamento de Física Aplicada I, Escuela Superior de Ingeniería, Universidad del País Vasco UPV-EHU, Bilbao, Spain
%+ Materials Physics Center CSIC-UPV/EHU and Donostia International Physics Center, San Sebastián, Spain
%+ CEMUC, Departamento de Engenharia Mecânica, Universidade de Coimbra, Polo II, 3030-201 Coimbra, Portugal
%A Wakeham, W.A.
%A Assael, M.J.
%A Avelino, H.M.N.T.
%A Bair, S.
%A Baled, H.O.
%A Bamgbade, B.A.
%A Bazile, Jean-Patrick
%A Caetano, F.J.P.
%A Comuñas, M.J.P.
%A Daridon, Jean-Luc
%A Diogo, J.C.F.
%A Enick, R.M.
%A Fareleira, J.M.N.A.
%A Fernández, J.
%A Oliveira, M.C.
%A Santos, T.V.M.
%A Tsolakidou, C.M.
%Z ACL
%< avec comité de lecture
%@ 0021-9568
%J Journal of Chemical and Engineering Data
%I American Chemical Society
%V 62
%N 9
%P 2884-2895
%8 2017
%D 2017
%R 10.1021/acs.jced.7b00170
%Z Physics [physics]/Condensed Matter [cond-mat]/Materials Science [cond-mat.mtrl-sci]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Fluids mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of materials [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Mechanical engineering [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Mechanics of materials [physics.class-ph]
%Z Sciences of the Universe [physics]/Earth Sciences/Geophysics [physics.geo-ph]Journal articles
%X This paper presents a reference correlation for the viscosity of tris(2-ethylhexyl) trimellitate designed to serve in industrial applications for the calibration of viscometers at elevated temperatures and pressures such as those encountered in the exploration of oil reservoirs and in lubrication. Tris(2-ethylhexyl) trimellitate has been examined with respect to the criteria necessary for an industrial standard reference material such as toxicity, thermal stability, and variability among manufactured lots. The viscosity correlation has been based upon all of the data collected in a multinational project and is supported by careful measurements and analysis of all the supporting thermophysical property data that are needed to apply the standard for calibration to a wide variety of viscometers. The standard reference viscosity data cover temperatures from 303 to 473 K, pressures from 0.1 to 200 MPa, and viscosities from approximately 1.6 to 755 mPa s. The uncertainty in the data provided is of the order of 3.2% at 95% confidence level, which is thought to be adequate for most industrial applications. © 2017 American Chemical Society.
%G English
%L hal-01816747
%U https://hal.archives-ouvertes.fr/hal-01816747
%~ CNRS
%~ UNIV-PAU
%~ LFCR
%~ LFCR-PTP
%~ TESTUPPA2
%~ UPPA-OA

