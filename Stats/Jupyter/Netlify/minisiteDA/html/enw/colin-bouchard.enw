%0 Conference Proceedings
%F Poster 
%T Heterogeneity of space use in tactics in salmon parr along breeding season in relation to the individual maturity level
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%A BOUCHARD, Colin
%A Rives, Jacques
%A Lange, Frédéric
%A Tentelier, Cédric
%< avec comité de lecture
%B EEEF 2018 - Ecology and Evolutionary Ethology of Fishes Conference
%C Montreal, Canada
%3 Program
%P 56 p.
%8 2018-06-17
%D 2018
%K genêtic variation
%K ripening
%K atlantic
%K salmon
%K diversité génétique
%K reproduction
%K maturité
%K tacon
%K utilisation de l'espace
%K salmo salar
%Z Environmental Sciences/Biodiversity and EcologyPoster communications
%G English
%2 https://hal.archives-ouvertes.fr/hal-01874034/document
%2 https://hal.archives-ouvertes.fr/hal-01874034/file/2018_Bouchard_EEEF2018Oral_1.pdf
%L hal-01874034
%U https://hal.archives-ouvertes.fr/hal-01874034
%~ INRAE
%~ SDE
%~ ECOBIOP
%~ GIP-BE
%~ INRA
%~ UNIV-PAU
%~ AGREENIUM
%~ TESTUPPA2
%~ UPPA-OA
%~ COLISA

%0 Thesis
%T In Atlantic salmon, space use of potential breeders stabilises population dynamics and shapes sexual selection
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%A BOUCHARD, Colin
%P 349 p.
%I Université de Pau et des Pays de l'Adour
%Y Agnès Bardonnet
%Y Cédric Tentelier
%8 2018-12-14
%D 2018
%K competition sociale
%K stock-recrutement
%K agregation spatiale
%K competition sexuelle
%K habitat de reproduction
%K saumon atlantique
%K systeme d'appariement
%K fragmentation de l'habitat
%K reseau sexuel
%Z Environmental Sciences/Biodiversity and EcologyTheses
%G English
%2 https://hal.inrae.fr/tel-02786312/document
%2 https://hal.inrae.fr/tel-02786312/file/2018Th%C3%A8seBouchard_1.pdf
%L tel-02786312
%U https://hal.inrae.fr/tel-02786312
%~ INRAE
%~ INRA
%~ SDE
%~ UNIV-PAU
%~ GIP-BE
%~ AGREENIUM
%~ ECOBIOP
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T A risk-based forecast of extreme mortality events in small cetaceans: Using stranding data to inform conservation practice
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%+ Observatoire Pelagis (UMS 3462)
%+ Université de La Rochelle (ULR)
%+ Centre National de la Recherche Scientifique (CNRS)
%+ Bonneville Power Administration
%+ Centre d'Etudes Biologiques de Chizé
%+ ADERA
%A Bouchard, Colin
%A Bracken, Cameron
%A Dabin, Willy
%A Van Canneyt, Olivier
%A Ridoux, Vincent
%A Spitz, Jérôme
%A Authier, Matthieu
%Z The authors thank the French Ministry of Ecology (“Ministère de la Transition Ecologique et Solidaire”) for financial support
%< avec comité de lecture
%@ 1755-263X
%J Conservation Letters
%I Wiley
%P 10 p.
%8 2019
%D 2019
%R 10.1111/conl.12639
%K marine mammal
%K marine strategic framework directive
%K mortality
%K cetacean
%K nature conservation
%K extreme event
%K extreme value theory
%K bycatch
%K forecasting
%K cetace
%K mammifère marin
%K mortalité
%K conservation
%Z Environmental Sciences/Biodiversity and EcologyJournal articles
%X Effective conservation requires monitoring and pro-active risk assessments.We studied the effects of at-sea mortality events (ASMEs) in marine mammals over two decades (1990–2012) and built a risk-based indicator for the European Union's Marine Strategy Framework Directive. Strandings of harbor porpoises (Phocoena phocoena), short-beaked common dolphins (Delphinus delphis), and striped dolphins (Stenella coeruleoalba) along French coastlines were analyzed using Extreme Value Theory (EVT). EVT operationalizes what is an extreme ASME, and allows the probabilistic forecasting of the expected maximum number of dead animals assuming constant pressures. For the period 2013–2018, we forecast the strandings of 80 harbor porpoises, 860 common dolphins, and 57 striped dolphins in extreme ASMEs. Comparison of these forecasts with observed strandings informs whether pressures are increasing, decreasing, or stable. Applying probabilistic methods to stranding data facilitates the building of risk-based indicators, required under the Marine Strategy Framework Directive, to monitor the effect of pressures on marine mammals.
%G English
%2 https://hal.archives-ouvertes.fr/hal-02056885/document
%2 https://hal.archives-ouvertes.fr/hal-02056885/file/2019_Bouchard_ConservationLetters.pdf
%L hal-02056885
%U https://hal.archives-ouvertes.fr/hal-02056885
%~ CNRS
%~ UNIV-PAU
%~ INRA
%~ SDE
%~ GIP-BE
%~ AGREENIUM
%~ ECOBIOP
%~ UNIV-ROCHELLE
%~ INRAE
%~ TESTUPPA2
%~ UPPA-OA

%0 Thesis
%T In Atlantic salmon, space use of potential breeders stabilises population dynamics and shapes sexual selection
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%A Bouchard, Colin
%I Université de Pau et des Pays de l'Adour
%Y Agnes Bardonnet
%Y Cedric Tentelier (co-directeur)
%Z Dany Garant
%Z Alexandre Courtiol
%Z Emmanuelle Cam
%Z Julien Coucherousset
%8 2018-12-14
%D 2018
%K habitat fragmentation
%K mature parr
%K mating system
%K breeding sites
%K Atlantic salmon
%K sexual network
%K sexual competition
%K social competition
%K spatial aggregation
%K stock-recruitment
%K fragmentation de l’habitat
%K habitats de reproduction
%K saumon Atlantique
%K système d’appariement
%K réseau sexuel
%K stock-recrutement
%K Agrégation spatiale
%K compétition sexuelle
%K compétition sociale
%Z Environmental Sciences/Biodiversity and Ecology
%Z Life Sciences [q-bio]/Animal biologyTheses
%X Habitat fragmentation and reduction in the availability of suitable habitats are significant threats to ecosystems in particularly for freshwater ones. For instance, accessibility, availability, and quality of breeding sites of Atlantic salmon, \textit{Salmo salar}, can be restricting in some rivers due to human activities and dams. Such threats may affect distribution of potential breeders because after their up-river migration, salmon females distribute within available breeding sites. Spatial distribution of females determines the spatial distribution of breeder males. Dominant males try to monopolise several females, whereas subordinate males adopt a sneaky behaviour. Access to females by males depends on the spatial distribution of females and on males' displacements within a river. The spatial distribution of females generates the spatial distribution of juveniles, aggregating them at some breeding sites. The latter aggregation of juveniles may raise the density-dependent mortality with potential consequences on population dynamics. The thesis aims to assess how space use of potential breeders, namely mature individuals able to spawn or fertilise eggs, influence population dynamics and sexual selection.I combined different approaches and different temporal and spatial scales to potential effects of space use of potential breeders. Time series of stock (deposited eggs) and of recruitment (juveniles) for the salmon population of the Nivelle River were combined with measures of spatial aggregation of nests. The effects of displacements and spatial distribution of potential breeders on their participation in reproduction were tested through inferring the sexual network of the population.I found that spatial aggregation of nests improved the recruitment variability but did not affect the average recruitment. The spatial aggregation of nests resulting from female aggregation was also linked to the number of effective breeders through a U-shaped curve. Such relationships suggested mate monopolization dependent on the spatial aggregation of females. The negative effect of the spatial isolation of females on their number of mates also suggest such mate monopolization. Then, the inferred sexual network enabled me to highlight a local social structure within the population. The latter structure and social competitors impacted the reproductive success of anadromous males. The participation of mature parr was one of the factors diminishing the sexual competition faced by anadromous males. Finally, mature parr were more mobile and moved more upstream than immature parr. Spatial behaviours of mature parr were linked to their participation in reproduction, individuals exhibiting wider home ranges had a higher probability to encounter females.Altogether my results show that space use of potential breeders affect both population dynamics and sexual selection. Then, spatial aggregation of females and of their nests can be beneficial for population dynamics as shown by combining different temporal and spatial scales. Females seemed to aggregate within the best breeding sites with a better environmental stability. The spatial distribution of females affected the ability of males to monopolise several females and participation of sneaky males. The availability of suitable good breeding habitats seemed restricting in the Nivelle, potentially due to the presence of impassable upstream dams. The lack of suitable breeding sites seemed impacting the mating system of the population and the sexual selection operating in. Some management actions to improve the quality of breeding sites and their availability could be planned
%X La fragmentation des habitats ainsi que la réduction de leur disponibilité sont des menaces importantes pour les écosystèmes, notamment aquatiques. La disponibilité et la qualité des sites de fraie du saumon Atlantique peut donc être limitante dans certaines rivières. Après la migration en rivière, les femelles saumon vont chercher à s'établir dans un des sites de fraie disponibles. La distribution spatiale des femelles va alors influer sur la distribution spatiale des mâles reproducteurs qui vont chercher soit à monopoliser plusieurs femelles (pour les mâles dominants), soit à les approcher en adoptant un comportement "furtif" (pour les mâles satellites). L'accès des mâles aux femelles va donc dépendre de la distribution spatiale des femelles et des déplacements des mâles dans la rivière. La distribution des femelles va également jouer sur la distribution spatiale des juvéniles, les concentrant à certains endroits. Cette concentration peut augmenter la mortalité densité-dépendante qui modifie la dynamique de la population. Cette thèse avait pour but d'évaluer comment l'utilisation de l'espace des potentiels reproducteurs, c'est à dire les individus matures, influence la dynamique de la population et la sélection sexuelle.Des séries temporelles de stock (oeufs déposés) et de recrutement (juvéniles) pour la population de saumon de la Nivelle ont été associées à des mesures de l'agrégation spatiale des nids creusés par les femelles. L'effet des déplacements et de la distribution des reproducteurs sur leur participation à la reproduction a notamment été testé via la reconstruction du réseau d'interactions sexuelles dans la Nivelle.J'ai ainsi démontré que l'agrégation spatiale des nids influa sur la variabilité du recrutement mais pas sur le recrutement moyen. Cette agrégation qui résulte de l'agrégation des femelles a également été liée au nombre de reproducteurs ayant engendrés des juvéniles. Le réseau d'interactions sexuelles a permis de mettre en évidence une structure sociale locale au sein de la population. Cette structure et les compétiteurs présents pouvaient par exemple diminuer le succès reproducteur des mâles. Notamment, les tacons matures qui participent à la reproduction avant de migrer en mer renforçaient la compétition sexuelle dont les mâles dominants faisaient l'objet. Enfin, ces mêmes tacons matures adoptèrent des comportements spatiaux du fait de leur statut de maturité avec des individus bougeant peu et des individus se déplaçant vers l'amont. Ces comportements spatiaux influaient leur participation à la reproduction, l'étendue des domaines de vie et les déplacements vers l'amont augmentaient la probabilité de rencontrer une femelle.Mes résultats m'ont permis de mettre en évidence l'effet de l'utilisation de l'espace des potentiels reproducteurs sur la dynamique de la population et sur la sélection sexuelle. Ainsi, en combinant différentes échelles spatiales et temporelles il a été montré que l'agrégation des femelles pouvait être bénéfique pour la dynamique de la population. Il semble que les femelles tendent à s'agréger d'abord sur les meilleurs sites de fraie avec une plus forte stabilité environnementale. Cette distribution des femelles affecta la capacité des mâles à monopoliser plusieurs femelles, une agrégation modérée permettant une monopolisation. La participation des mâles furtifs anadromes ou tacons matures était également dépendante de l'agrégation des femelles. Il apparait que la disponibilité d'habitats adéquats pour la reproduction semble limitante dans la Nivelle et notamment du fait de la présence de barrages. Ce manque semble affecter le système d'appariement de la population et la sélection sexuelle y opérant. Des mesures visant à améliorer la qualité des sites de fraie déjà disponibles ainsi que leur accessibilité pourraient être envisagées
%G English
%2 https://hal.archives-ouvertes.fr/tel-02292776/document
%2 https://hal.archives-ouvertes.fr/tel-02292776/file/Bouchard.pdf
%L tel-02292776
%U https://hal.archives-ouvertes.fr/tel-02292776
%~ UNIV-PAU
%~ ECOBIOP
%~ INRA
%~ SDE
%~ GIP-BE
%~ AGREENIUM
%~ INRAE
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Effects of spatial aggregation of nests on population recruitment: the case of a small population of Atlantic salmon
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%A BOUCHARD, Colin
%A Bardonnet, Agnes
%A Buoro, Mathieu
%A Tentelier, Cédric
%< avec comité de lecture
%@ 2150-8925
%J Ecosphere
%I Ecological Society of America
%V 9
%N 4
%P 18 p.
%8 2018
%D 2018
%R 10.1002/ecs2.2178
%K clustering
%K breeding site
%K beverton-holt
%K density compensation
%K environmental stochasticity
%K patchiness
%K population dynamic
%K spatial distribution
%K spatial heterogeneity
%K salmon
%K atlantic
%K population dynamics
%K nest
%K spawning grounds
%K distribution spatiale
%K salmo salar
%K dynamique des populations
%K nivelle
%K site de reproduction
%K frayère
%K nid
%Z Environmental Sciences/Biodiversity and EcologyJournal articles
%X Spatial aggregation within a population is a widespread phenomenon which may both exacerbate local competition and the stochastic effect of local environmental perturbations. In particular, the spatial aggregation of nests may strongly affect recruitment and hence population dynamics. Although the negative effect of local density on local recruitment has often been theoretically extended to population dynamics, very few studies have demonstrated the effect of local aggregation on the whole population recruitment. Using a long-term survey of a small Atlantic salmon population, we tested the effect of spatial aggregation on the whole population recruitment and whether accounting for population stock is important or not when explaining the population recruitment. We found that accounting for population stock is necessary and that spatial aggregation of nests improved estimates of population recruitment. The spatial aggregation of nests did not impact the average population recruitment; however, a stronger aggregationdiminished the variability of population recruitment. Our findings suggest that the aggregation of nests among some breeding areas does not necessarily impair the whole population recruitment and significantly reduces the stochasticity of the recruitment. In addition, the aggregation of nests seems to be the result of an ideal distribution of females, selecting the best-breeding sites. Our results also indicate that females select breeding sites on environmental risk to spawn within the safest sites. This study warns against the extrapolation of local density dependence observations to the population level, and advocates for investigating the effect of aggregation on the demographic and evolutionary population dynamics, a clear contribution of aggregation on population dynamic processes being found in the Nivelle population.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01773150/document
%2 https://hal.archives-ouvertes.fr/hal-01773150/file/2018_Bouchard_Ecosphere.pdf
%L hal-01773150
%U https://hal.archives-ouvertes.fr/hal-01773150
%~ UNIV-PAU
%~ INRA
%~ SDE
%~ GIP-BE
%~ AGREENIUM
%~ ECOBIOP
%~ INRAE
%~ TESTUPPA2
%~ UPPA-OA
%~ COLISA

%0 Conference Proceedings
%F Poster 
%T Effects of spatial aggregation of nests on population recruitment: the case of a small population of Atlantic salmon
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%A BOUCHARD, Colin
%A Bardonnet, Agnes
%A Buoro, Mathieu
%A Tentelier, Cédric
%Z Résumé
%< sans comité de lecture
%B International Statistical Ecology Conference (ISEC 2018)
%C St Andrews, United Kingdom
%3 ISEC 2018 Conference Book
%P 328 p.
%8 2018-07-02
%D 2018
%R 10.13140/RG.2.2.35939.04643
%K population dynamics
%K spatial distribution
%K atlantic
%K salmon
%K dynamique des populations
%K distribution spatiale
%K site de reproduction
%K répartition spatiale agrégative
%K habitat
%K salmo salar
%Z Environmental Sciences/Biodiversity and EcologyPoster communications
%G English
%2 https://hal.archives-ouvertes.fr/hal-01860020/document
%2 https://hal.archives-ouvertes.fr/hal-01860020/file/2018_Bouchard_ISEC2018Poster_1.pdf
%2 https://hal.archives-ouvertes.fr/hal-01860020/file/2018_Bouchard_ISEC2018Poster2_2.pdf
%L hal-01860020
%U https://hal.archives-ouvertes.fr/hal-01860020
%~ INRAE
%~ SDE
%~ ECOBIOP
%~ GIP-BE
%~ INRA
%~ UNIV-PAU
%~ AGREENIUM
%~ TESTUPPA2
%~ UPPA-OA
%~ COLISA

%0 Unpublished work
%T The dynamics of spawning acts by a semelparous fish and its associated energetic costs
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%+ pôle OFB-INRAE- Agrocampus Ouest-UPPA pour la gestion des migrateurs amphihalins dans leur environnement
%+ Institut de recherche de la Tour du Valat
%A Tentelier, Cédric
%A Bouchard, Colin
%A Bernardin, Anaïs
%A Tauzin, Amandine
%A Aymes, Jean-Christophe
%A Lange, Frédéric
%A Récapet, Charlotte
%A Rives, Jacques
%< avec comité de lecture
%J Peer Community in Ecology
%I Peer Community In
%P 436295v7
%8 2020-12-02
%D 2020
%R 10.1101/436295
%K Accelerometer
%K biologging
%K clupeid
%K egg retention
%K energy budget
%K reproductive effort
%K semelparity
%K temperature
%K slimming
%Z Environmental Sciences/Biodiversity and Ecology
%Z Life Sciences [q-bio]/Animal biology/Vertebrate Zoology
%Z Life Sciences [q-bio]/Biodiversity/Populations and Evolution [q-bio.PE]
%Z Statistics [stat]/Applications [stat.AP]Preprints, Working Papers, ...
%X During the reproductive season, animals have to manage both their energetic budget and gamete stock. In particular, for semelparous capital breeders with determinate fecundity and no parental care other than gametic investment, the depletion of energetic stock must match the depletion of gametic stock, so that individuals get exhausted just after their last egg is laid and fertilized. Although these budgets are managed continuously, monitoring the dynamics of mating acts and energy expenditure at a fine temporal scale in the wild is challenging. This study aimed to quantify the individual dynamics of spawning acts and the concomitant energy expenditure of female Allis shad ( Alosa alosa ) throughout their mating season. Using eight individual-borne accelerometers for one month, we collected tri-axial acceleration, temperature, and pressure data that we analysed to i) detect the timing of spawning acts, ii) estimate energy expenditure from tail beat frequency and water temperature, and iii) monitor changes in body roundness from the position of the dorsally-mounted tag relative to the vertical plane. Female shad had a higher probability to spawn during warmer nights, and their spawning acts were synchronized (both individually and inter-individually) within each active night. They experienced warmer temperature, remained deeper, swan more slowly and spent less energy during daytime than night time. Over one month of spawning, they performed on average 15.75 spawning acts, spent on average 6 277 kJ and died with a significant portion of residual oocytes. The acceleration-based indicator of body roundness was correlated to condition coefficient measured at capture, and globally decreased through the spawning season, although the indicator was noisy and was not correlated to changes in estimated energy expenditure. Despite significant individual variability, our results indicate that female shad exhausted their energetic stock faster than their egg stock. Water warming will increase the rate of energy expenditure, which might increase the risk that shad die with a large stock of unspent eggs. Although perfectible, the three complementary analyses of acceleration data are promising for in situ monitoring of energy expenditure related to specific behaviour.
%G English
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-03032629/document
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-03032629/file/2020_Tentelier_PCIEcology100060.pdf
%L hal-03032629
%U https://hal-univ-pau.archives-ouvertes.fr/hal-03032629
%~ INRAE
%~ UNAM
%~ POLE_MIGRATEURS_AMPHIHALINS
%~ TOUR-DU-VALAT
%~ GIP-BE
%~ SDE
%~ UNIV-PAU
%~ ECOBIOP
%~ AGREENIUM
%~ TESTUPPA2

