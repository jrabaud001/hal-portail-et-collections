%0 Conference Paper
%F Oral 
%T A Generic Solution for Weaving Business Code into Executable Models
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%A Le Goaer, Olivier
%A Cariou, Eric
%A Brunschwig, Léa
%A Barbier, Franck
%< avec comité de lecture
%B 4th International Workshop on Executable Modeling at MoDELS (EXE 2018)
%C Copenhagen, Denmark
%S CEUR Worshop Proceedings
%V 2245
%P 251-256
%8 2018
%D 2018
%K Executable DSL
%K Xmodeling
%K operational semantics
%K CASE tool
%K EM
%K EMF
%Z Computer Science [cs]Conference papers
%X The separation of concerns is a fundamental principle that allowsto build a software with separate parts, thereby improving theirmaintainability and evolutivity. Executable models are good poten-tial representatives of this principle since they capture the behaviorof a software-intensive system, that is, when, why and how callingbusiness operations, while the latter are specified apart. EMF is thede facto framework used to create an executable DSL (xDSL) but asolution to weave business operations into it is still missing. Thisis compounded by the fact that such business operations can betied to specific technological platforms that stand outside the EMFworld (e.g. Android SDK). To that purpose, in this paper we describea solution for managing business operations both at design-time(creation of executable models with EMF) and at run-time (oper-ation calls from the deployed execution engine). This solution isgeneric enough to be integrated into any Java-based environmentand for any xDSL
%G English
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-01912827/document
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-01912827/file/cariou-exe18.pdf
%L hal-01912827
%U https://hal-univ-pau.archives-ouvertes.fr/hal-01912827
%~ LIUPPA
%~ UNIV-PAU
%~ OPENAIRE
%~ TESTUPPA2
%~ UPPA-OA

%0 Conference Proceedings
%T World Wide Modeling Made Easy - A Simple, Lightweight Model Server
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%+ Laboratoire d'Automatique, de Mécanique et d'Informatique industrielles et Humaines - UMR 8201 (LAMIH)
%A Le Goaer, Olivier
%A Cariou, Eric
%A Barbier, Franck
%< avec comité de lecture
%B Proceedings of the 5th International Conference on Model-Driven Engineering and Software Development, MODELSWARD 2017, Porto, Portugal, February 19-21, 2017.
%C Porto, Portugal
%I SciTePress
%P 269-276
%8 2017-02-19
%D 2017
%R 10.5220/0006110802690276
%Z Computer Science [cs]Conference papers
%G English
%L hal-01912336
%U https://hal-univ-pau.archives-ouvertes.fr/hal-01912336
%~ UNIV-PAU
%~ LIUPPA
%~ CNRS
%~ UNIV-VALENCIENNES
%~ TESTUPPA2
%~ UPPA-OA
%~ LAMIH

%0 Conference Proceedings
%T Model Execution Adaptation?
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%+ Université de Valenciennes et du Hainaut-Cambrésis (UVHC)
%A Cariou, Eric
%A Barbier, Franck
%A Le Goaer, Olivier
%< avec comité de lecture
%B Proceedings of the 7th Workshop on Models@run.Time, Innsbruck, Austria, October 02, 2012
%C Innsbruck, Austria
%I ACM
%3 MRT '12
%P 60-65
%8 2012-10-02
%D 2012
%R 10.1145/2422518.2422528
%Z Computer Science [cs]Conference papers
%X One of the main goals of model-driven engineering (MDE) is the manipulation of models as exclusive software artifacts. Model execution is in particular a means to substitute models for code. On another way, MDE is a promising discipline for building adaptable systems thanks to models at runtime. When the model is directly executed, the system becomes the model, then, this is the model that is adapted. In this paper, we investigate the adaptation of the model itself in the context of model execution. We present a first experimentation where we study the constraints on a model to be able to determine if it is consistent (that is, adapted) with an execution environment, possibly including fail-stop modes. Then, we state some perspectives and open issues about model execution adaptation. \textcopyright 2012 ACM.
%G English
%L hal-01909112
%U https://hal-univ-pau.archives-ouvertes.fr/hal-01909112
%~ UNIV-PAU
%~ LIUPPA
%~ UNIV-VALENCIENNES
%~ TESTUPPA2
%~ UPPA-OA

%0 Conference Proceedings
%T Characterization of Adaptable Interpreted-DSML
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%+ Université de Valenciennes et du Hainaut-Cambrésis (UVHC)
%A Cariou, Eric
%A Le Goaer, Olivier
%A Barbier, Franck
%A Pierre, Samson
%< avec comité de lecture
%B Modelling Foundations and Applications - 9th European Conference, ECMFA 2013, Montpellier, France, July 1-5, 2013. Proceedings
%C Montpellier, France
%I Springer
%3 Lecture Notes in Computer Science
%V 7949
%P 37--53
%8 2013-07-01
%D 2013
%R 10.1007/978-3-642-39013-5_4
%Z Computer Science [cs]Conference papers
%X One of the main goals of model-driven engineering (MDE) is the manipulation of models as exclusive software artifacts. Model execution is in particular a means to substitute models for code. More precisely, as models of a dedicated domain-specific modeling language (DSML) are interpreted through an execution engine, such a DSML is called interpreted-DSML (i-DSML for short). On another way, MDE is a promising discipline for building adaptable systems based on models at runtime. When the model is directly executed, the system becomes the model: This is the model that is adapted. In this paper, we propose a characterization of adaptable i-DSML where a single model is executed and directly adapted at runtime. If model execution only modifies the dynamical elements of the model, we show that the adaptation can modify each part of the model and that the execution and adaptation semantics can be changed at runtime.
%G English
%L hal-01909109
%U https://hal-univ-pau.archives-ouvertes.fr/hal-01909109
%~ UNIV-PAU
%~ LIUPPA
%~ UNIV-VALENCIENNES
%~ TESTUPPA2
%~ UPPA-OA

%0 Thesis
%T Evolution styles within software architectures
%+ Laboratoire d'Informatique de Nantes Atlantique (LINA)
%A Le Goaer, Olivier
%I Université de Nantes
%I Ecole Centrale de Nantes (ECN)
%Y Mourad Chabane Oussalah(mourad.oussalah@univ-nantes.fr)
%Z Philippe Aniorté (rapporteur)
%Z Henri Basson (rapporteur)
%Z Benoit Baudry (examinateur)
%Z Laurent Granvilliers (président du jury)
%Z Dalila Tamzalit (co-encadrante)
%Z Abdelhak Seriai (co-encadrant)
%8 2009-10-09
%D 2009
%K Evolution Style
%K Evolution Pattern
%K Evolution Reuse
%K Library
%K Software Architecture
%K Architecture Description Language.
%K Style d'évolution
%K Patron d'évolution
%K Réutilisation de l'évolution
%K Bibliothèque
%K Architecture Logicielle
%K Langage de Description d'Architecture.
%Z Computer Science [cs]/Software Engineering [cs.SE]Theses
%X Software architectures have been introduced in response to the increasing complexity of systems, by leveraging their descriptions at a high level of abstraction. In this thesis, we propose to tackle the problem of their evolutions with the aim of capitalizing the recurrent evolution and fostering their reuse. Our contribution is divided into two major parts. The first part concerns with proposing the evolution model SAEM (Style-based Architectural Evolution Model), allowing abstraction, specification and management of evolution in software architectures through the concept of evolution style. SAEM is a generic evolution model, consistent and independent of any architectural description language. The proposed formalism describes evolution style's concepts according to a triptych: domain, header and competence. The second part concerns with the development of a reuse-driven approach on top of SAEM to try to make evolutions more cost-effective activities. We propose an approach for the construction of libraries dedicated to evolution styles, orchestrated by several stakeholders. Libraries are developed with two complementary types of processes: « for reuse » and « by reuse ». We explain a classification-based reasoning technique to enable libraries to be enriched and queryied in order to manage the knowledge and know-how related to architectural evolution.
%X Les architectures logicielles ont été introduites en réponse à l'accroissement de la complexité des systèmes, en favorisant leurs descriptions à un haut niveau d'abstraction. Dans cette thèse, nous proposons d'aborder la problématique de leurs évolutions avec comme objectif, de capitaliser les évolutions récurrentes et de favoriser leur réutilisation. Notre contribution se décline en deux volets majeurs. Le premier volet concerne la proposition du modèle d'évolution SAEM (Style-based Architectural Evolution Model), permettant l'abstraction, la spécification et la gestion de l'évolution dans les architectures logicielles au travers du concept de style d'évolution. SAEM se veut un modèle d'évolution générique, uniforme et indépendant de tout langage de description d'architecture. Le formalisme proposé décrit les concepts du style d'évolution selon un tryptique : domaine, entête et compétence. Le deuxième volet concerne le développement d'une approche de réutilisation par dessus SAEM pour tenter de rendre les activités d'évolution plus rentables. Nous proposons une démarche pour la construction de bibliothèques pour les styles d'évolution, orchestrée par différentes catégories d'intervenants. Les bibliothèques sont élaborées selon deux types de processus complémentaires : « pour la réutilisation » et « par la réutilisation ». Nous présentons une technique de raisonnement classificatoire pour permettre aux bibliothèques d'être peuplées et interrogées dans le but de gérer les savoir et savoir-faire relatifs à l'évolution architecturale.
%G French
%2 https://tel.archives-ouvertes.fr/tel-00459925/document
%2 https://tel.archives-ouvertes.fr/tel-00459925/file/these.pdf
%L tel-00459925
%U https://tel.archives-ouvertes.fr/tel-00459925
%~ LINA
%~ LINA-MODAL
%~ CNRS
%~ UNIV-NANTES
%~ UNIV-NANTES-THESES
%~ LINA-AELOS

%0 Journal Article
%T Ingénierie dirigée par les modèles : quels supports à l'interopérabilité des systèmes d'information ?
%+ Laboratoire d'Informatique, Signaux, et Systèmes de Sophia-Antipolis (I3S) / Equipe MODALIS
%+ Laboratoire Informatique, Image et Interaction - EA 2118 (L3I)
%+ Laboratoire d'Intégration des Systèmes et des Technologies (LIST)
%+ Analyses and Languages Constructs for Object-Oriented Application Evolution (RMOD)
%+ Systèmes d’Information - inGénierie et Modélisation Adaptables (SIGMA)
%+ Web-Instrumented Man-Machine Interactions, Communities and Semantics (WIMMICS)
%+ Laboratoire d'Informatique de Nantes Atlantique (LINA)
%A Authosserre-Cavarero, Annie
%A Bertrand, Frédéric
%A Fornarino, Mireille
%A Collet, Philippe
%A Dubois, Hubert
%A Ducasse, Stéphane
%A Dupuy-Chessa, Sophie
%A Faron Zucker, Catherine
%A Faucher, Cyril
%A Lafaye, Jean-Yves
%A Lahire, Philippe
%A Le Goaer, Olivier
%A Montagnat, Johan
%A Pinna-Déry, Anne-Marie
%< avec comité de lecture
%@ 1633-1311
%J Revue des Sciences et Technologies de l'Information - Série ISI : Ingénierie des Systèmes d'Information
%I Lavoisier
%8 2013-04-26
%D 2013
%Z Computer Science [cs]/Programming Languages [cs.PL]Journal articles
%X Information systems are more and more often based on aggregation of other systems that must be maintained and evolved in an agile way and without un- controlled entropy. This is not without interoperability problems! Among others, the aim of Model-Driven Engineering (MDE) is to provide solutions for interoperability issues between systems. This paper summarizes thoughts that have come up from the specific action "Interoperability of information systems and model-driven engineering: What challenges? What solutions?" supported by inforsid. We propose a summary of approaches that are based on MDE and knowledge engineering and that tackle in- teroperability issues in the industry. Open questions and limitations that raised during the meetings are also reported.
%G French
%2 https://hal.inria.fr/hal-00813675v2/document
%2 https://hal.inria.fr/hal-00813675v2/file/Auth13a-interop-si-JournalVersion.pdf
%L hal-00813675
%U https://hal.inria.fr/hal-00813675
%~ CNRS
%~ INRIA
%~ UNIV-GRENOBLE1
%~ UNIV-PMF_GRENOBLE
%~ INRIA-SOPHIA
%~ INRIA-LILLE
%~ I3S
%~ INRIASO
%~ UNIV-NANTES
%~ LINA
%~ LINA-AELOS
%~ WIMMICS
%~ LIG_GLSI_SIGMA
%~ LIG_GLSI
%~ LIG_SIC_IIHM
%~ LIG_SIC
%~ LIG
%~ UNICE
%~ CRISTAL
%~ CRISTAL-RMOD
%~ CEA
%~ INRIA_TEST
%~ INRIA2
%~ UNIV-LILLE3
%~ DRT
%~ UNIV-ROCHELLE
%~ UCA-TEST
%~ UNIV-COTEDAZUR
%~ LIST
%~ INPG
%~ UGA

%0 Conference Proceedings
%T Interopérabilité des systèmes d'information : approches dirigées par les modèles
%+ Laboratoire d'Informatique, Signaux, et Systèmes de Sophia-Antipolis (I3S) / Equipe MODALIS
%+ Laboratoire Informatique, Image et Interaction - EA 2118 (L3I)
%+ Laboratoire d'Intégration des Systèmes et des Technologies (LIST)
%+ Analyses and Languages Constructs for Object-Oriented Application Evolution (RMOD)
%+ Systèmes d’Information - inGénierie et Modélisation Adaptables (SIGMA)
%+ Ingénierie de l’Interaction Homme-Machine (IIHM)
%+ Web-Instrumented Man-Machine Interactions, Communities and Semantics (WIMMICS)
%+ Laboratoire d'Informatique de Nantes Atlantique (LINA)
%+ Laboratoire d'Informatique, Signaux, et Systèmes de Sophia-Antipolis (I3S) / Equipe RAINBOW
%A Authosserre-Cavarero, Annie
%A Bertrand, Frederic
%A Blay- Fornarino, Mireille
%A Collet, Philippe
%A Dubois, Hubert
%A Ducasse, Stéphane
%A Dupuy-Chessa, Sophie
%A Faron Zucker, Catherine
%A Faucher, Cyril
%A Lafaye, Jean-Yves
%A Lahire, Philippe
%A Le Goaer, Olivier
%A Montagnat, Johan
%A Pinna-Dery, Anne-Marie
%< avec comité de lecture
%B Inforsid 2012
%C Montpellier, France
%P 11-30
%8 2012-05-29
%D 2012
%Z Computer Science [cs]/Programming Languages [cs.PL]Conference papers
%X Information systems are more and more often based on aggregation of other systems that must be maintained and evolved in an agile way and with no entropy creation. This is not without interoperability problems! Among others, the aim of Model-Driven Engineering (MDE) is to provide solutions for interoperability issues between systems. This paper summarizes thoughts that have come up from the specific action "Interoper- ability of information systems and model-driven engineering: What challenges? What solutions?" supported by inforsid. We propose a summary of approaches that are based on MDE and knowledge engineering and that tackle interoperability issues in the industry. Open questions and limitations that raised during the meetings are also reported.
%G French
%2 https://hal.inria.fr/hal-00707536/document
%2 https://hal.inria.fr/hal-00707536/file/Auth12a-interop-si-2012.pdf
%L hal-00707536
%U https://hal.inria.fr/hal-00707536
%~ CNRS
%~ INRIA
%~ UNIV-GRENOBLE1
%~ UNIV-PMF_GRENOBLE
%~ INRIA-SOPHIA
%~ UNIV-NANTES
%~ LINA
%~ INRIA-LILLE
%~ I3S
%~ INRIASO
%~ L3I
%~ UNICE
%~ WIMMICS
%~ LIG_SIC_IIHM
%~ LIG_SIC
%~ LIG_GLSI
%~ LIG_GLSI_SIGMA
%~ UGA
%~ LIG
%~ CRISTAL-RMOD
%~ CRISTAL
%~ CEA
%~ INRIA_TEST
%~ INRIA2
%~ UNIV-LILLE3
%~ DRT
%~ UNIV-ROCHELLE
%~ UCA-TEST
%~ UNIV-COTEDAZUR
%~ LIST
%~ INPG

%0 Conference Proceedings
%T Xmodeling Studio : Un Outil Pour Définir Des DSL Exécutables
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%A Brunschwig, Léa
%A Le Goaer, Olivier
%A Cariou, Eric
%< avec comité de lecture
%B 7ème Conférence En IngénieriE Du Logiciel (CIEL 2018)
%C Grenoble, France
%8 2018
%D 2018
%Z Computer Science [cs]
%Z Computer Science [cs]/Software Engineering [cs.SE]Conference papers
%X This article introduces an Eclipse plugin called Xmodeling Studio. This tool assists, initially, the language engineers for the design and implementation of executable DSL and their execution engine considering the business methods and the management of their data flow, then, secondly, Xmodeling is giving the possibility to the software engineers to integrate the language engineers' work in the environment of their choice.
%X Cet article présente un plugin Eclipse appelé Xmodeling Studio. Cet outil permet d'assister, dans un premier temps, les ingénieurs de langages dans la conception et l'implémentation de DSL exécutables et de leur moteur d'exécution en prenant en compte les opérations métier et la gestion de leur flot de données, puis, dans un second temps, Xmodeling donne la possibilité aux ingénieurs logiciel d'intégrer le travail produit par les ingénieurs de langages dans l'environnement Java de leur choix.
%G English
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-01912828/document
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-01912828/file/brunschwig-ciel18.pdf
%L hal-01912828
%U https://hal-univ-pau.archives-ouvertes.fr/hal-01912828
%~ UNIV-PAU
%~ LIUPPA
%~ TESTUPPA
%~ TESTUPPA2

%0 Journal Article
%T HCIDL: Human-Computer Interface Description Language for Multi-Target, Multimodal, Plastic User Interfaces
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%+ Service Oriented Computing (SOC)
%A Gaouar, Lamia
%A Benamar, Abdelkrim
%A Le Goaer, Olivier
%A Biennier, Frédérique
%< avec comité de lecture
%@ 2314-7288
%J Future Computing and Informatics Journal
%I Elsevier
%V 3
%N 1
%P 110-130
%8 2018-06
%D 2018
%R 10.1016/j.fcij.2018.02.001
%Z Computer Science [cs]Journal articles
%G English
%L hal-01906795
%U https://hal-univ-pau.archives-ouvertes.fr/hal-01906795
%~ UNIV-PAU
%~ LIUPPA
%~ CNRS
%~ EC-LYON
%~ UNIV-LYON1
%~ INSA-LYON
%~ LIRIS
%~ UNIV-LYON2
%~ LYON2
%~ INSA-GROUPE
%~ LABEXIMU
%~ UDL
%~ UNIV-LYON
%~ TESTUPPA2
%~ UPPA-OA

