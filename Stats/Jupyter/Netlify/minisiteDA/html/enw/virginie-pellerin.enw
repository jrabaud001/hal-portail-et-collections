%0 Journal Article
%T Hierarchical honeycomb-structured films by directed self-assembly in "breath figure" templating of ionizable "clicked" PH3T-b-PMMA diblock copolymers: an ionic group/counter-ion effect on porous polymer film morphology.
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Ji, Eunkyung
%A Pellerin, Virginie
%A Ehrenfeld, Francis
%A Laffore, Anthony
%A Bousquet, Antoine
%A Billon, Laurent
%< avec comité de lecture
%@ 1359-7345
%J Chemical Communications
%I Royal Society of Chemistry
%V 53
%P 1876-1879
%8 2017
%D 2017
%R 10.1039/C6CC09898C
%K polythiophene diblock directed self assembly
%Z Chemical Sciences/Analytical chemistryJournal articles
%X The self-assembly of 1,2,3-triazole and ionic 1,2,3-triazolium "clicked" poly(3-hexylthiophene)-b-poly(methylmethacrylate) (P3HT-b-PMMA) rod-coil diblock copolymers was used to fabricate honeycomb-patterned porous films via "breath figure" templating. The surface and inner morphologies of the honeycomb films can be both controlled by either ionizing the 1,2,3-triazole linker or changing the counter-ion nature.
%G English
%L hal-01481306
%U https://hal.archives-ouvertes.fr/hal-01481306
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ IPREM-PCM
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Characterization Study of CO2, CH4, and CO2/CH4 Hydroquinone Clathrates Formed by Gas–Solid Reaction
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Développement de méthodologies expérimentales (DMEX)
%A Coupan, Romuald
%A Péré, Eve
%A Dicharry, Christophe
%A Plantier, Frédéric
%A Diaz, Joseph
%A Khoukh, Abdel
%A Allouche, Joachim
%A Labat, Stéphane
%A Pellerin, V.
%A Grenet, Jean-Paul
%A Sotiropoulos, Jean-Marc
%A Senechal, Pascale
%A Guerton, Fabrice
%A Moonen, Peter
%A Torré, Jean-Philippe
%Z ISIFoR Carnot Institute
%Z cited By 0
%< avec comité de lecture
%@ 1932-7447
%J Journal of Physical Chemistry C
%I American Chemical Society
%V 121
%N 41
%P 22883-22894
%8 2017-09-26
%D 2017
%R 10.1021/acs.jpcc.7b07378
%K Clathrates
%K CO2
%Z Chemical Sciences/Chemical engineering
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%X Hydroquinone (HQ) is known to form organic clathrates with some gaseous species such as CO2 and CH4. This work presents spectroscopic data, surface and internal morphologies, gas storage capacities, guest release temperatures, and structural transition temperatures for HQ clathrates obtained from pure CO2, pure CH4, and an equimolar CO2/CH4 mixture. All analyses are performed on clathrates formed by direct gas–solid reaction after 1 month’s reaction at ambient temperature conditions and under a pressure of 3.0 MPa. A collection of spectroscopic data (Raman, FT-IR, and 13C NMR) is presented, and the results confirm total conversion of the native HQ (α-HQ) into HQ clathrates (β-HQ) at the end of the reaction. Optical microscopy and SEM analyses reveal morphology changes after the enclathration reaction, such as the presence of surface asperities. Gas porosimetry measurements show that HQ clathrates and native HQ are neither micro- nor mesoporous materials. However, as highlighted by TEM analyses and X-ray tomography, α- and β-HQ contain unsuspected macroscopic voids and channels, which create a macroporosity inside the crystals that decreases due to the enclathration reaction. TGA and in situ Raman spectroscopy give the guest release temperatures as well as the structural transition temperatures from β-HQ to α-HQ. The gas storage capacity of the clathrates is also quantified by means of different types of gravimetric analyses (mass balance and TGA). After having been formed under pressure, the characterized clathrates exhibit exceptional metastability: the gases remain in the clathrate structure at ambient conditions over time scales of more than 1 month. Consequently, HQ gas clathrates display very interesting properties for gas storage and sequestration applications.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01679472/document
%2 https://hal.archives-ouvertes.fr/hal-01679472/file/Coupan_21615.pdf
%L hal-01679472
%U https://hal.archives-ouvertes.fr/hal-01679472
%~ SORBONNE-UNIVERSITE
%~ CDF
%~ LORIA2
%~ INC-CNRS
%~ PARISTECH
%~ CNRS
%~ UNIV-TLN
%~ UNIV-AMU
%~ IPREM-CAPT
%~ PSL
%~ UPMC_POLE_2
%~ IM2NP
%~ LFCR
%~ UPMC
%~ ENSCP
%~ ENSC-PARIS
%~ TEST-AMU
%~ UNIV-PAU
%~ IPREM
%~ SU-SCIENCES
%~ DMEX
%~ LFCR-ISD
%~ IPREM-PCM
%~ SU-TI
%~ TESTUPPA2
%~ UPPA-OA

