%0 Journal Article
%T Slow dynamics in glass-forming materials
%+ Laboratoire de dynamique et structure des matériaux moléculaires (LDSMM)
%+ Cornell University
%A Affouard, Frederic
%A Bordat, Patrice
%A Lerbret, Adrien
%A Descamps, Marc
%< avec comité de lecture
%@ 0892-7022
%J Molecular Simulation
%I Taylor & Francis
%V 32
%N 12-13
%P 1057-1068
%8 2007-02-17
%D 2007
%R 10.1080/08927020600900329
%K Physical SciencesJournal articles
%X Dynamical properties of glass-formers such as glassy crystals, molecular liquids and model atomic liquids have been investigated in the ps-ns regime with dl_poly in order to check the Mode-Coupling Theory and the Coupling Model. The microscopic origin of the fragility, the characteristic parameter involved in the liquid-glass transition, is also highlighted: the interaction potential, especially its anharmonicity and capacity for intermolecular coupling, is the key parameter controlling both the long time dynamics in supercooled systems and the short time dynamics in their glassy states.
%G English
%2 https://hal.archives-ouvertes.fr/hal-00514999/document
%2 https://hal.archives-ouvertes.fr/hal-00514999/file/PEER_stage2_10.1080%252F08927020600900329.pdf
%L hal-00514999
%U https://hal.archives-ouvertes.fr/hal-00514999
%~ PEER
%~ CNRS

%0 Journal Article
%T Exploring conformational energy landscape of glassy disaccharides by CPMAS 13C NMR and DFT/GIAO simulations. I. Methodological aspects.
%+ Groupe matière condensée et matériaux (GMCM)
%+ LABORATOIRE DE CHIMIE THEORIQUE ET PHYSICO-CHIMIE MOLECULAIRE (LCTPCM)
%+ Laboratory of Physical and Macromolecular Chemistry ; Department BBCM and UdR INSTM (LPMC)
%+ Laboratoire de dynamique et structure des matériaux moléculaires (LDSMM)
%A Lefort, Ronan
%A Bordat, Patrice
%A Cesaro, Attilio
%A Descamps, Marc
%< avec comité de lecture
%@ 0021-9606
%J Journal of Chemical Physics
%I American Institute of Physics
%V 126
%N 1
%P 014511
%8 2007-01-07
%D 2007
%Z cond-mat/0606405
%R 10.1063/1.2409935
%K trehalose
%K solid state magnetic resonance
%K molecular mechanics simulation
%Z PACS : 61.43.Bn, 61.18.-Fs , 76.60.Cq
%Z Physics [physics]/Condensed Matter [cond-mat]/Materials Science [cond-mat.mtrl-sci]Journal articles
%X The aim of this article is to assess the ability of chemical shift surfaces to provide structural information on conformational distributions of disaccharides in glassy solid state. The validity of the general method leading to a simulation of inhomogeneous 13C chemical shift distributions is discussed in detail. In particular, a proper consideration of extrema and saddle points of the chemical shift map correctly accounts for the observed discontinuities in the experimental CPMAS spectra. Provided that these basic requirements are met, DFT/GIAO chemical shift maps calculated on relaxed conformations lead to a very satisfactory description of the experimental lineshapes. On solid-state trehalose as a model of amorphous disaccharide, this simulation approach defines unambiguously the most populated sugar conformation in the glass, and can help in discriminating the validity of different models of intramolecular energy landscape. Application to other molecular systems with broad conformational populations is foreseen to produce a larger dependence of the calculated chemical shift distribution on the conformational map.
%G English
%2 https://hal.archives-ouvertes.fr/hal-00080174/document
%2 https://hal.archives-ouvertes.fr/hal-00080174/file/P1cm.pdf
%L hal-00080174
%U https://hal.archives-ouvertes.fr/hal-00080174
%~ CNRS
%~ UNIV-PAU
%~ UNIV-LITTORAL
%~ UNIV-RENNES1
%~ ZZ-IPR-MN
%~ IPR
%~ IPR-MN
%~ UR1-SPM
%~ UR1-UFR-SPM
%~ UR1-SDLM
%~ UR1-HAL
%~ IPREM
%~ OSUR
%~ TEST-UNIV-RENNES
%~ TEST-UR-CSS
%~ UNIV-RENNES
%~ TESTUPPA2
%~ UR1-MMS
%~ UPPA-OA

%0 Journal Article
%T How homogeneous are the trehalose, maltose, and sucrose water solutions? An insight from molecular dynamics simulations
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Lerbret, Adrien
%A Bordat, Patrice
%A Affouard, Frederic
%A Descamps, Michel
%A Migliardo, F.
%< avec comité de lecture
%@ 1520-6106
%J Journal of Physical Chemistry B
%I American Chemical Society
%V 109
%N 21
%P 11046--11057
%8 2005
%D 2005
%R 10.1021/jp0468657
%Z Chemical SciencesJournal articles
%X The structural properties resulting from the reciprocal influence between water and three well-known homologous disaccharides, namely, trehalose, maltose, and sucrose, in aqueous solutions have been investigated in the 4-66 wt % concentration range by means of molecular dynamics computer simulations. Hydration numbers clearly show that trehalose binds to a larger number of water molecules than do maltose or sucrose, thus affecting the water structure to a deeper extent. Two-dimensional radial distribution functions of trehalose solutions definitely reveal that water is preferentially localized at the hydration sites found in the trehalose dihydrate crystal, this tendency being enhanced when increasing trehalose concentration. Over a rather wide concentration range (4-49 wt %), the fluctuations of the radius of gyration and of the glycosidic dihedral angles of trehalose indicate a higher flexibility with respect to maltose and sucrose. At sugar concentrations between 33 and 66 wt %, the mean sugar cluster size and the number of sugar-sugar hydrogen bonds formed within sugar clusters reveal that trehalose is able to form larger clusters than sucrose but smaller than maltose. These features suggest that trehalose-water mixtures would be more homogeneous than the two others, thus reducing both desiccation stresses and ice formation. © 2005 American Chemical Society.
%G English
%L hal-01567180
%U https://hal.archives-ouvertes.fr/hal-01567180
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Coherent Electronic Coupling versus Localization in Individual Molecular Dimers
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Lippitz, M.
%A Hübner, C.G.
%A Christ, T.
%A Eichner, H.
%A Bordat, Patrice
%A Herrmann, A.
%A Mullen, K.
%A Basché, T.
%< avec comité de lecture
%@ 0031-9007
%J Physical Review Letters
%I American Physical Society
%V 92
%N 10
%P 103001--1
%8 2004
%D 2004
%R 10.1103/PhysRevLett.92.103001
%Z Chemical SciencesJournal articles
%X The electronic excitation transfer and coherent electronic coupling strength were investigated in molecular dimers by spectrally resolved confocal fluorescence spectroscopy. The direct probe of electronic coupling strength distribution was possible due to static disorder in polymer host by single molecule measurements. The dimers with delocalized excitation were also found in comparison to emission originated from localized states. The delocalized and localized state transitions were observed for dimers which were attributed to structural fluctuations of guest-host system.
%G English
%L hal-01567185
%U https://hal.archives-ouvertes.fr/hal-01567185
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Does the interaction potential determine both the fragility of a liquid and the vibrational properties of its glassy state?
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Bordat, Patrice
%A Affouard, Frederic
%A Descamps, Michel
%A Ngai, K.L.
%< avec comité de lecture
%@ 0031-9007
%J Physical Review Letters
%I American Physical Society
%V 93
%N 10
%P 105502--1--105502--4
%8 2004
%D 2004
%R 10.1103/PhysRevLett.93.105502
%Z Chemical SciencesJournal articles
%X The responsibility for the correlations between various dynamic, thermodynamic and vibrational properties of glass formers of the capacity for intermolecular coupling and the anharmonicity of the potential was analyzed. The analysis was done by performing molecular dynamics simulations of binary Lennard-Jones (LJ) systems with three different potentials. It was observed that the increase of the capacity for intermolecular coupling and anharmonicity has the effects of increasing the kinetic fragility and the nonexponentiality parameter in the liquid state and the Tg-scaled temperature dependence of the nonergodicity parameter in the glassy state. The results show that these parameters correlate with each other.
%G English
%L hal-01567184
%U https://hal.archives-ouvertes.fr/hal-01567184
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T How do trehalose, maltose, and sucrose influence some structural and dynamical properties of lysozyme? Insight from molecular dynamics simulations
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Lerbret, Adrien
%A Bordat, Patrice
%A Affouard, Frederic
%A Hédoux, Alain
%A Guinet, Y.
%A Descamps, Michel
%< avec comité de lecture
%@ 1520-6106
%J Journal of Physical Chemistry B
%I American Chemical Society
%V 111
%N 31
%P 9410--9420
%8 2007
%D 2007
%R 10.1021/jp071946z
%Z Chemical SciencesJournal articles
%X The influence of three well-known disaccharides, namely, trehalose, maltose, and sucrose, on some structural and dynamical properties of lysozyme has been investigated by means of molecular dynamics computer simulations in the 37-60 wt % concentration range. The effects of sugars on the protein conformation are found to be relatively weak, in agreement with the preferential hydration of lysozyme. Conversely, sugars seem to increase significantly the relaxation times of the protein. These effects are shown to be correlated to the fractional solvent accessibilities of lysozyme residues and further support the slaving of protein dynamics. Moreover, a significant increase in the relaxation times of lysozyme, sugars, and water molecules is observed within the studied concentration range and may result from the percolation of the hydrogen-bond network of sugar molecules. This percolation appears to be of primary importance to explain the influence of sugars on the dynamical properties of lysozyme and water. © 2007 American Chemical Society.
%G English
%L hal-01567173
%U https://hal.archives-ouvertes.fr/hal-01567173
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T The IR spectrum of supercritical water: Combined molecular dynamics/quantum mechanics strategy and force field for cluster sampling
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Laboratoire Ondes et Matière d'Aquitaine (LOMA)
%A Bordat, Patrice
%A Bégué, Didier
%A Brown, Ross
%A Marbeuf, Alain
%A Cardy, Henri
%A Baraille, Isabelle
%Z CINES, Région Aquitaine, Ministère de la Recherche et de la Technologie
%< avec comité de lecture
%@ 0020-7608
%J International Journal of Quantum Chemistry
%I Wiley
%V 112
%N 13
%P 2578-2584
%8 2012
%D 2012
%R 10.1002/qua.23286
%K water clusters
%K supercritical fluid
%K quantum chemistry
%K molecular dynamics
%Z Chemical Sciences/Theoretical and/or physical chemistryJournal articles
%X Supercritical water was analyzed recently as a gas of small clusters of waters linked to each other by intermolecular hydrogen-bonds, but unexpected "linear" conformations of clusters are required to reproduce the infra-red (IR) spectra of the supercritical state. Aiming at a better understanding of clusters in supercritical water, this work presents a strategy combining classical molecular dynamics to explore the potential energy landscape of water clusters with quantum mechanical calculation of their IR spectra. For this purpose, we have developed an accurate and flexible force field of water based on the TIP5P 5-site model. Water dimers and trimers obtained with this improved force field compare well with the quantum mechanically optimized clusters. Exploration by simulated annealing of the potential energy surface of the classical force field reveals a new trimer conformation whose IR response determined from quantum calculations could play a role in the IR spectra of supercritical water.
%G English
%2 https://hal.archives-ouvertes.fr/hal-00713263/document
%2 https://hal.archives-ouvertes.fr/hal-00713263/file/BordatIJQC2012_Postprint.pdf
%L hal-00713263
%U https://hal.archives-ouvertes.fr/hal-00713263
%~ CNRS
%~ UNIV-PAU
%~ LOMA
%~ IPREM
%~ INC-CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Slowing down of water dynamics in disaccharide aqueous solutions
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Lerbret, Adrien
%A Affouard, Frederic
%A Bordat, Patrice
%A Hédoux, Alain
%A Guinet, Y.
%A Descamps, Michel
%< avec comité de lecture
%@ 0022-3093
%J Journal of Non-Crystalline Solids
%I Elsevier
%V 357
%N 2
%P 695--699
%8 2011
%D 2011
%R 10.1016/j.jnoncrysol.2010.05.092
%K Bioprotection
%K Carbohydrates
%K Hydrogen bonds
%K Water structural relaxation
%Z Chemical SciencesJournal articles
%X The dynamics of water in aqueous solutions of three homologous disaccharides, namely trehalose, maltose and sucrose, has been analyzed by means of molecular dynamics simulations in the 0-66 wt.% concentration range. The low-frequency vibrational densities of states (VDOS) of water were compared with the susceptibilities χ″ of 0-40 wt.% solutions of trehalose in D 2O obtained from complementary Raman scattering experiments. Both reveal that sugars significantly stiffen the local environments experienced by water. Accordingly, its translational diffusion coefficient decreases when the sugar concentration increases, as a result of an increase of water-water hydrogen bond lifetimes and of the corresponding activation energies. This induced slowing down of water dynamics, ascribed to the numerous hydrogen bonds that sugars form with water, is strongly amplified at concentrations above 40 wt.% by the percolation of the hydrogen bond network of sugars, and may partially explain their well-known stabilizing effect on proteins in aqueous solutions. © 2010 Elsevier B.V.All rights reserved.
%G English
%L hal-01567167
%U https://hal.archives-ouvertes.fr/hal-01567167
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Exploring conformational energy landscape of glassy disaccharides by CPMAS 13C NMR and DFT/GIAO simulations. II. Enhanced molecular flexibility in amorphous trehalose.
%+ Groupe matière condensée et matériaux (GMCM)
%+ LABORATOIRE DE CHIMIE THEORIQUE ET PHYSICO-CHIMIE MOLECULAIRE (LCTPCM)
%+ Laboratory of Physical and Macromolecular Chemistry ; Department BBCM and UdR INSTM (LPMC)
%+ Laboratoire de dynamique et structure des matériaux moléculaires (LDSMM)
%A Lefort, Ronan
%A Bordat, Patrice
%A Cesaro, Attilio
%A Descamps, Marc
%< avec comité de lecture
%@ 0021-9606
%J Journal of Chemical Physics
%I American Institute of Physics
%V 126
%N 1
%P 014510
%8 2007-01-07
%D 2007
%Z cond-mat/0606408
%R 10.1063/1.2409934
%K trehalose
%K solid state nuclear magnetic resonance
%K molecular mechanics simulation
%Z PACS : 61.43.Bn, 61.18.-Fs , 76.60.Cq
%Z Physics [physics]/Condensed Matter [cond-mat]/Materials Science [cond-mat.mtrl-sci]Journal articles
%X This paper deals with the comparative use of the chemical shift surfaces to simulate experimental 13C CPMAS data on amorphous solid state disaccharides, paying particular attention to Α-1-1 linkage of trehalose, to Β-1,4 linkage between pyranose rings (lactose) and to linkage implying a furanose ring (sucrose). The combination of molecular mechanics with DFT/GIAO ab-initio methods provides reliable structural information on the conformational distribution in the glass. The results are interpreted in terms of an enhanced flexibility that trehalose experiences in amorphous solid state compared to the other sugars. An attempt to relate this property to the balance between intra- and inter-molecular hydrogen bonding network in the glass is presented.
%G English
%2 https://hal.archives-ouvertes.fr/hal-00080175/document
%2 https://hal.archives-ouvertes.fr/hal-00080175/file/P2cm.pdf
%L hal-00080175
%U https://hal.archives-ouvertes.fr/hal-00080175
%~ CNRS
%~ UNIV-PAU
%~ UNIV-LITTORAL
%~ UNIV-RENNES1
%~ ZZ-IPR-MN
%~ IPR
%~ IPR-MN
%~ UR1-SPM
%~ UR1-UFR-SPM
%~ UR1-SDLMJONCH
%~ UR1-SDLM
%~ UR1-HAL
%~ IPREM
%~ OSUR
%~ TEST-UNIV-RENNES
%~ TEST-UR-CSS
%~ UNIV-RENNES
%~ TESTUPPA2
%~ UR1-MMS
%~ UPPA-OA

%0 Journal Article
%T Molecular Simulation of Solid-Solid Phase Transitions
%+ LABORATOIRE DE CHIMIE THEORIQUE ET PHYSICO-CHIMIE MOLECULAIRE (LCTPCM)
%+ Centre de physique moléculaire optique et hertzienne (CPMOH)
%A Bordat, Patrice
%A Marbeuf, Alain
%A Brown, Ross
%< avec comité de lecture
%@ 0892-7022
%J Molecular Simulation
%I Taylor & Francis
%V 32
%N 12-13
%P 971-984
%8 2007-02-17
%D 2007
%R 10.1080/08927020600863022
%K Physical SciencesJournal articles
%X Applications of dl_poly to solid-solid phase transitions are reviewed, with particular attention to how details of the mechanisms of the transitions may be extracted from molecular dynamics simulations. Two examples in molecular crystals are discussed: the order-disorder transition of p-terphenyl initiated at around 200K by the unlocking of ring flipping; and the rotator phases of n-alkanes with around 20 carbon atoms per chain, showing distinct molecular mechanisms in the dynamics just below the melting points of odd and even chains. Covalent-ionic materials are represented by an application to an aluminophophate molecular sieve, AlPO_4-5.
%G English
%2 https://hal.archives-ouvertes.fr/hal-00514993/document
%2 https://hal.archives-ouvertes.fr/hal-00514993/file/PEER_stage2_10.1080%252F08927020600863022.pdf
%L hal-00514993
%U https://hal.archives-ouvertes.fr/hal-00514993
%~ PEER
%~ LOMA
%~ CNRS
%~ IPREM
%~ UNIV-PAU
%~ TESTUPPA2

%0 Report
%T Extensions of the Siesta DFT Code for Simulation of Molecules
%+ High-End Parallel Algorithms for Challenging Numerical Simulations (HiePACS)
%+ Institut Pluridisciplinaire de Recherche sur l'Environnement et les Matériaux (IPREM)
%A Coulaud, Olivier
%A Bordat, Patrice
%A Fayon, Pierre
%A Le Bris, Vincent
%A Baraille, Isabelle
%A Brown, Ross
%N RR-8221
%P 25
%I INRIA
%8 2013-02-12
%D 2013
%Z 1302.4617
%K adaptative algorithm
%K siesta
%K molecular systems
%K dft /tddft computation
%Z Computer Science [cs]/Distributed, Parallel, and Cluster Computing [cs.DC]
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Physics [physics]/Condensed Matter [cond-mat]/Materials Science [cond-mat.mtrl-sci]Reports
%X We describe extensions to the siesta density functional theory (dft) code [30], for the simulation of isolated molecules and their absorption spectra. The extensions allow for: - Use of a multi-grid solver for the Poisson equation on a finite dft mesh. Non-periodic, Dirichlet boundary conditions are computed by expansion of the electric multipoles over spherical harmonics. - Truncation of a molecular system by the method of design atom pseudo- potentials of Xiao and Zhang[32]. - Electrostatic potential fitting to determine effective atomic charges. - Derivation of electronic absorption transition energies and oscillator stren- gths from the raw spectra produced by a recently described, order O(N3), time-dependent dft code[21]. The code is furthermore integrated within siesta as a post-processing option.
%X Nous décrivons les extensions au code siesta [30] de la théorie de la fonctionnelle de densité (dft), pour la simulation des molécules isolées et leurs spectres d'absorption. Ces extensions permettent : * l'utilisation d'un solveur multigrille pour l'équation de Poisson sur le maillage dft L ̇es conditions aux limites de Dirichlet sont calculées par un développement en harmoniques sphériques du potentiel électrique ; * la coupure du système moléculaire à l'aide du pseudo-potentiels de l'atome sur mesure de Xiao Zhang[32] ; * le calcul des charges effectives atomiques par la méthode de l'ajustement du potentiel électrostatique ; * Calcul des énergies de transition d'absorption électroniques et des forces d'oscillateur à partir des spectres bruts obtenus par un code dft dépendant du temps[21]. Le code est en outre intégré dans siesta comme une option de post-traitement. code[21].
%G English
%2 https://hal.inria.fr/hal-00787088/document
%2 https://hal.inria.fr/hal-00787088/file/RR-8221.pdf
%L hal-00787088
%U https://hal.inria.fr/hal-00787088
%~ CNRS
%~ INRIA
%~ UNIV-PAU
%~ INRIA-BORDEAUX
%~ LABRI
%~ INRIA-RRRT
%~ IPREM
%~ IFR140
%~ TESTBORDEAUX
%~ ENSEIRB
%~ UNIV-BORDEAUX
%~ INRIA_TEST
%~ INC-CNRS
%~ INRIA2
%~ LARA
%~ ANR
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Association of indigo with zeolites for improved colour stabilization
%+ Matériaux, Rayonnements, Structure (MRS)
%+ Centre de recherche et de restauration des musées de France (C2RMF)
%+ Institut pluridisciplinaire de recherche sur l'environnement et les matériaux (IPREM)
%+ Institut Pluridisciplinaire de Recherche sur l'Environnement et les Matériaux (IPREM)
%+ Laboratoire Léon Brillouin (LLB - UMR 12)
%A Dejoie, Catherine
%A Martinetto, Pauline
%A Dooryhee, Eric
%A Van Eslande, Elsa
%A Blanc, Sylvie
%A Bordat, Patrice
%A Brown, Ross
%A Porcher, Florence
%A Anne, Michel
%< avec comité de lecture
%@ 0003-7028
%J Applied Spectroscopy
%I Society for Applied Spectroscopy
%V 64
%N 10
%P 1131-1138
%8 2010
%D 2010
%Z 1207.3236
%R 10.1366/000370210792973622
%Z Physics [physics]/Condensed Matter [cond-mat]/Materials Science [cond-mat.mtrl-sci]Journal articles
%X The durability of an organic colour and its resistance against external chemical agents and exposure to light can be significantly enhanced by hybridizing the natural dye with a mineral. In search for stable natural pigments, the present work focuses on the association of indigo blue with several zeolitic matrices (LTA zeolite, mordenite, MFI zeolite). The manufacturing of the hybrid pigment is tested under varying oxidising conditions, using Raman and UV-visible spectrometric techniques. Blending indigo with MFI is shown to yield the most stable composite in all of our artificial indigo pigments. In absence of defects and substituted cations such as aluminum in the framework of the MFI zeolite matrix, we show that matching the pore size with the dimensions of the guest indigo molecule is the key factor. The evidence for the high colour stability of indigo@MFI opens a new path for modeling the stability of indigo in various alumino-silicate substrates such as in the historical Maya Blue pigment.
%G English
%2 https://hal.archives-ouvertes.fr/hal-00717732/document
%2 https://hal.archives-ouvertes.fr/hal-00717732/file/Dejoie_ApplSpec_2010.pdf
%L hal-00717732
%U https://hal.archives-ouvertes.fr/hal-00717732
%~ CNRS
%~ NEEL
%~ NEEL-MCMF-SPMCE
%~ NEEL-MCMF
%~ IPREM
%~ CEA
%~ UNIV-PAU
%~ IRAMIS-LLB
%~ INC-CNRS
%~ NEEL-MRS
%~ CEA-DRF
%~ UNIV-GRENOBLE1
%~ C2RMF
%~ INPG
%~ UGA
%~ TESTUPPA2
%~ UPPA-OA
%~ IRAMIS

%0 Unpublished work
%T Revisiting Maya Blue and Designing Hybrid Pigments by Archaeomimetism
%+ Lawrence Berkeley National Laboratory [Berkeley] (LBNL)
%+ Matériaux, Rayonnements, Structure (MRS)
%+ Institut pluridisciplinaire de recherche sur l'environnement et les matériaux (IPREM)
%+ Laboratoire Léon Brillouin (LLB - UMR 12)
%+ European Synchrotron Radiation Facility (ESRF)
%+ Centre de recherche et de restauration des musées de France (C2RMF)
%A Dejoie, Catherine
%A Dooryhee, Eric
%A Martinetto, Pauline
%A Blanc, Sylvie
%A Bordat, Patrice
%A Brown, Ross
%A Porcher, Florence
%A Sanchez Del Rio, Manolo
%A Strobel, Pierre
%A Anne, Michel
%A Van Eslande, Elsa
%A Walter, Philippe
%Z CIBLE and MACODEV grants from Région Rhône-Alpes
%8 2010-06-25
%D 2010
%Z 1007.0818
%K indigo
%K pigment
%K Maya Blue
%K zeolites
%K organic-inorganic hybrid composites
%Z Physics [physics]/Condensed Matter [cond-mat]/Materials Science [cond-mat.mtrl-sci]
%Z Chemical Sciences/Cristallography
%Z Chemical Sciences/Material chemistry
%Z Humanities and Social Sciences/Archaeology and PrehistoryPreprints, Working Papers, ...
%X Maya Blue is actually one of the best known examples of an organic-inorganic hybrid material. Yet despite 50 years of sustained interest, its microscopic structure and its relation to durability remain open questions. We address the issue by archaeomimetism: engineering an archaeoinspired pigment, satisfactorily reproducing the colour and chemical stability of Maya Blue. By comparing and contrasting ancient pigment and the new analogue, we deduce a new explanation for this durability.
%G English
%2 https://hal.archives-ouvertes.fr/hal-00495128/document
%2 https://hal.archives-ouvertes.fr/hal-00495128/file/Dejoie_HAL_publi1.pdf
%L hal-00495128
%U https://hal.archives-ouvertes.fr/hal-00495128
%~ CNRS
%~ UNIV-GRENOBLE1
%~ NEEL
%~ NEEL-MCMF-SPMCE
%~ NEEL-MCMF
%~ SHS
%~ IPREM
%~ CEA
%~ UNIV-PAU
%~ FRANTIQ
%~ IRAMIS-LLB
%~ NEEL-MRS
%~ CEA-DRF
%~ C2RMF
%~ INPG
%~ UGA
%~ ANR
%~ TESTUPPA2
%~ UPPA-OA
%~ IRAMIS

%0 Journal Article
%T Diffusion Of Indigo Molecules Inside The Palygorskite Clay Channels
%+ Matériaux, Rayonnements, Structure (MRS)
%+ Centre de recherche et de restauration des musées de France (C2RMF)
%+ Institut pluridisciplinaire de recherche sur l'environnement et les matériaux (IPREM)
%+ Institut Pluridisciplinaire de Recherche sur l'Environnement et les Matériaux (IPREM)
%+ Laboratoire Léon Brillouin (LLB - UMR 12)
%A Dejoie, Catherine
%A Martinetto, Pauline
%A Dooryhee, Eric
%A Van Eslande, Elsa
%A Blanc, Sylvie
%A Bordat, Patrice
%A Brown, Ross
%A Porcher, Florence
%A Anne, Michel
%< avec comité de lecture
%J Materials Research Society Symposia Proceedings
%V 1319
%P mrsf10-1319-ww06-01
%8 2011
%D 2011
%Z 1207.3242
%R 10.1557/opl.2011.924
%Z Physics [physics]/Condensed Matter [cond-mat]/Materials Science [cond-mat.mtrl-sci]Journal articles
%X The search for durable dyes led several past civilizations to develop artificial pigments. Maya Blue (MB), manufactured in pre-Columbian Mesoamerica, is one of the best known examples of an organic-inorganic hybrid material. Its durability is due to the unique association of indigo molecule and palygorskite, a particular fibrous clay occurring in Yucatan. Despite 50 years of sustained interest, the microscopic structure of MB and its relation to the durability remain open questions. Combining new thermogravimetric and synchrotron X-ray diffraction analyses, we show that indigo molecules can diffuse into the channel of the palygorskite during the heating process, replacing zeolitic water and stabilizing the room temperature phases of the clay.
%G English
%2 https://hal.archives-ouvertes.fr/hal-00717741/document
%2 https://hal.archives-ouvertes.fr/hal-00717741/file/dejoie_MRS-BM_envoi-referee.pdf
%L hal-00717741
%U https://hal.archives-ouvertes.fr/hal-00717741
%~ CNRS
%~ NEEL
%~ NEEL-MCMF-SPMCE
%~ NEEL-MCMF
%~ IPREM
%~ CEA
%~ UNIV-PAU
%~ IRAMIS-LLB
%~ INC-CNRS
%~ NEEL-MRS
%~ CEA-DRF
%~ UNIV-GRENOBLE1
%~ C2RMF
%~ INPG
%~ UGA
%~ TESTUPPA2
%~ UPPA-OA
%~ IRAMIS

%0 Journal Article
%T Investigation of molecular dimers by ensemble and single molecule spectroscopy
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Christ, Th.
%A Petzke, F.
%A Bordat, Patrice
%A Herrmann, A.
%A Reuther, E.
%A Mullen, K.
%A Basché, Th.
%< avec comité de lecture
%@ 0022-2313
%J Journal of Luminescence
%I Elsevier
%V 98
%N 1-4
%P 23--33
%8 2002
%D 2002
%R 10.1016/S0022-2313(02)00247-8
%K Electronic interactions
%K Excitons
%K Molecular aggregates
%K Single molecule spectroscopy
%Z Chemical SciencesJournal articles
%X We have investigated molecular dimers with different electronic coupling strengths by bulk and single molecule spectroscopy. In one of the dimers the two monomers (perylene-monoimide) are directly connected via a single bond while in the other one they are separated by the benzil motif. The close proximity of the monomers in the first case gives rise to excitonic band splitting which is clearly observable in the bulk absorption spectra. For the benzil structure the electronic interactions are governed by Förster-type energy hopping between the monomers. Fluorescence intensity trajectories at the single molecule level show one-step and two-step bleaching behaviour which appears to be very similar for both dimers. However, emission spectra recorded simultaneously with the trajectories indicate spectral changes which allow to distinguish between weakly and strongly coupled dimers. In the latter case the spectral shape changes significantly when excitonic coupling has been lifted because of photochemical transformation of one of the monomers. © 2002 Elsevier Science B.V. All rights reserved.
%G English
%L hal-01567189
%U https://hal.archives-ouvertes.fr/hal-01567189
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA2
%~ UPPA-OA

