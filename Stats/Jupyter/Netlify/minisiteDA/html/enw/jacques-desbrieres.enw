%0 Journal Article
%T Synthesis and biological activity of some new 1,3,4-thiadiazole and 1,2,4-triazole compounds containing a phenylalanine moiety
%+ Department of Computer Science
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Moise, M.
%A Sunel, V.
%A Profire, L.
%A Popa, M.
%A Desbrieres, J.
%A Peptu, C.
%Z cited By 48
%< avec comité de lecture
%@ 1420-3049
%J Molecules
%I MDPI
%V 14
%N 7
%P 2621-2631
%8 2009
%D 2009
%R 10.3390/molecules14072621
%Z Chemical Sciences/PolymersJournal articles
%X New 1,3,4-thiadiazole, 6, 7 and 1,2,4-triazole derivatives, 8, 9 containing a phenylalanine moiety have been synthesized by intramolecular cyclization of 1,4- disubstituted thiosemicarbazides, 4, 5, in acid and alkaline media, respectively; the thiosemicarbazides were obtained by reaction of hydrazide 3 with appropriate aromatic isothiocyanates. The toxicity of the synthesized compounds was evaluated and the antiinflammatory study of the triazole compound 9 established an appreciable antiinflammatory activity that is comparable with that of other nonsteroidal anti-inflammatory agents. © 2009 by the authors; licensee Molecular Diversity Preservation International, Basel, Switzerland.
%G English
%L hal-01585412
%U https://hal.archives-ouvertes.fr/hal-01585412
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Novel transparent nanocomposite films based on chitosan and bacterial cellulose
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Fernandes, S.C.M.
%A Oliveira, L.
%A Freire, C.S.R.
%A Silvestre, A.J.D.
%A Neto, C.P.
%A Gandini, A.
%A Desbrieres, J.
%Z cited By 94
%< avec comité de lecture
%@ 1463-9262
%J Green Chemistry
%I Royal Society of Chemistry
%V 11
%N 12
%P 2023-2029
%8 2009
%D 2009
%R 10.1039/b919112g
%Z Chemical Sciences/PolymersJournal articles
%X New nanocomposite films based on different chitosan matrices (two chitosans with different DPs and one water soluble derivative) and bacterial cellulose were prepared by a fully green procedure by casting a water based suspension of chitosan and bacterial cellulose nanofibrils. The films were characterized by several techniques, namely SEM, AFM, X-ray diffraction, TGA, tensile assays and visible spectroscopy. They were highly transparent, flexible and displayed better mechanical properties than the corresponding unfilled chitosan films. These new renewable nanocomposite materials also presented reasonable thermal stability and low O2 permeability. © 2009 The Royal Society of Chemistry.
%G English
%L hal-01585417
%U https://hal.archives-ouvertes.fr/hal-01585417
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Structural Features and Rheological Properties of a Sulfated Xylogalactan-Rich Fraction Isolated from Tunisian Red Seaweed Jania adhaerens
%+ Institut Pascal (IP)
%+ Institut Universitaire de France (IUF)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Université de Rouen Normandie (UNIROUEN)
%+ Polymères Biopolymères Surfaces (PBS)
%+ École Nationale d'Ingénieurs de Sfax | National School of Engineers of Sfax (ENIS)
%A Hentati, Faiez
%A Delattre, Cédric
%A Gardarin, Christine
%A Desbrières, Jacques
%A Le Cerf, Didier
%A Rihouey, Christophe
%A Michaud, Philippe
%A Abdelkafi, Slim
%A Pierre, Guillaume
%< avec comité de lecture
%@ 2076-3417
%J Applied Sciences
%I MDPI
%V 10
%N 5
%P 1655
%8 2020-03
%D 2020
%R 10.3390/app10051655
%K Rheology
%K Sulfated xylogalactan
%K Polysaccharide
%K Jania adhaerens
%K Corallinales
%K sulfated xylogalactan
%K polysaccharide
%K rheology
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Material chemistry
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Analytical chemistryJournal articles
%X A novel sulfated xylogalactan-rich fraction (JSP for J. adhaerens Sulfated Polysaccharide) was extracted from the red Tunisian seaweed Jania adhaerens. JSP was purified using an alcoholic precipitation process and characterized by Attenuated Total Reflectance-Fourier-transform infrared spectroscopy (ATR-FTIR), high-pressure size exclusion chromatography (HPSEC) with a multi-angle laser light scattering (MALLS), gas chromatography coupled to mass spectrometry (GC-MS) and nuclear magnetic resonance spectroscopy (NMR, 1D and 2D). JSP was then evaluated regarding its physicochemical and rheological properties. Results showed that JSP was mainly composed of an agar-like xylogalactan sharing the general characteristics of corallinans. The structure of JSP was mainly composed of agaran disaccharidic repeating units (→3)-β-D-Galp-(1,4)-α-L-Galp-(1→)n and (→3)-β-D-Galp-(1,4)-3,6-α-l-AnGalp-(1→)n, mainly substituted on O-6 of (1,3)-β-D-Galp residues by β-xylosyl side chains, and less with sulfate or methoxy groups. (1,4)-α-L-Galp residues were also substituted by methoxy and/or sulfate groups in the O-2 and O-3 positions. Mass-average and number-average molecular masses (Mw) and (Mn), intrinsic viscosity ([η]) and hydrodynamic radius (Rh) for JSP were, respectively, 8.0 x 105 g/mol, 1.0 x 105 g/mol, 76 mL/g and 16.8 nm, showing a flexible random coil conformation in solution. The critical overlap concentration C* of JSP was evaluated at 7.5 g/L using the Williamson model. In the semi-diluted regime, JSP solutions displayed a shear-thinning behavior with a great viscoelasticity character influenced by temperature and monovalent salts. The flow characteristics of JSP were described by the Ostwald model
%G English
%2 https://hal.archives-ouvertes.fr/hal-02524249/document
%2 https://hal.archives-ouvertes.fr/hal-02524249/file/applsci-10-01655-v2.pdf
%L hal-02524249
%U https://hal.archives-ouvertes.fr/hal-02524249
%~ INC-CNRS
%~ ENSICAEN
%~ ENSI-CAEN
%~ PRES_CLERMONT
%~ INSTITUT_PASCAL
%~ UNIV-BPCLERMONT
%~ SIGMA-CLERMONT
%~ UNICAEN
%~ COMUE-NORMANDIE
%~ INSA-ROUEN
%~ UNIROUEN
%~ INSA-GROUPE
%~ CNRS
%~ PBS
%~ IPREM
%~ UNIV-PAU
%~ IPREM-PCM
%~ UNILEHAVRE
%~ ACL-SF
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Alkyl-Chitosan-Based Adhesive: Water Resistance Improvement
%+ Institut Pascal (IP)
%+ Laboratoire de Glycobiologie et Matrice Extracellulaire Végétale (Glyco-MEV)
%+ Laboratoire d'ingénierie pour les systèmes complexes (UR LISC)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Mati-Baouche, Narimane
%A Delattre, Cédric
%A De Baynast, Hélène
%A Grediac, Michel
%A Mathias, Jean-Denis
%A Ursu, Alina, Violeta
%A Desbrieres, Jacques
%A Michaud, Philippe
%< avec comité de lecture
%@ 1420-3049
%J Molecules
%I MDPI
%V 24
%N 10
%8 2019
%D 2019
%R 10.3390/molecules24101987
%M 31126129
%K adhesive
%K alkylation
%K chitosan
%K water resistance
%Z Life Sciences [q-bio]/Vegetal Biology
%Z Life Sciences [q-bio]/Biotechnology
%Z Life Sciences [q-bio]/Cellular Biology
%Z Life Sciences [q-bio]/Biochemistry, Molecular Biology/Biomolecules [q-bio.BM]
%Z Life Sciences [q-bio]/Development Biology
%Z Life Sciences [q-bio]/Cellular Biology/Cell Behavior [q-bio.CB]
%Z Life Sciences [q-bio]/Cellular Biology/Subcellular Processes [q-bio.SC]
%Z Chemical Sciences/Polymers
%Z Life Sciences [q-bio]/Biochemistry, Molecular Biology/Genomics [q-bio.GN]
%Z Life Sciences [q-bio]/Vegetal Biology/Phytopathology and phytopharmacy
%Z Life Sciences [q-bio]/Vegetal Biology/Plant breeding
%Z Life Sciences [q-bio]/Biochemistry, Molecular Biology/Molecular biologyJournal articles
%X A chemical modification by grafting alkyl chains using an octanal (C8) on chitosan was conducted with the aim to improve its water resistance for bonding applications. The chemical structure of the modified polymers was determined by NMR analyses revealing two alkylation degrees (10 and 15%). In this study, the flow properties of alkyl-chitosans were also evaluated. An increase in the viscosity was observed in alkyl-chitosan solutions compared with solutions of the same concentration based on native chitosan. Moreover, the evaluation of the adhesive strength (bond strength and shear stress) of both native and alkyl-chitosans was performed on two different double-lap adherends (aluminum and wood). Alkyl-chitosans (10 and 15%) maintain sufficient adhesive properties on wood and exhibit better water resistance compared to native chitosan.
%G English
%2 https://hal-normandie-univ.archives-ouvertes.fr/hal-02138130/document
%2 https://hal-normandie-univ.archives-ouvertes.fr/hal-02138130/file/2019-Mati-Baouche-et-al-Molecules.pdf
%L hal-02138130
%U https://hal-normandie-univ.archives-ouvertes.fr/hal-02138130
%~ COMUE-NORMANDIE
%~ GLYCOMEV
%~ UNIROUEN
%~ UNIV-PAU
%~ IRSTEA
%~ INSTITUT_PASCAL
%~ IPREM
%~ CNRS
%~ UNIV-BPCLERMONT
%~ ACL-SVSAE
%~ SIGMA-CLERMONT
%~ AGREENIUM
%~ INC-CNRS
%~ PRES_CLERMONT
%~ ACL-SF
%~ IPREM-PCM
%~ INRAE
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Polymer conjugates with potential biological activity based on new derivatives of 2-mercaptobenzoxazole-synthesis and characterization
%+ "Gheorghe Asachi" Technical University of Iaşi - Faculty of Chemical Engineering and Environmental Protection
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Aparaschivei, R.
%A Şunel, V.
%A Holban, M.
%A Popa, M.
%A Desbrieres, J.
%Z cited By 1
%< avec comité de lecture
%@ 1895-1066
%J Central European Journal of Chemistry
%I Springer Verlag
%V 11
%N 11
%P 1808-1815
%8 2013
%D 2013
%R 10.2478/s11532-013-0310-8
%Z Chemical Sciences/PolymersJournal articles
%X New potentially biologically active compounds derived from 2-mercapto-benzoxazole were synthesized and coupled on polymeric support of poly (maleic anhydride-alt-vinyl acetate) for the preparation of polymer-drug conjugates with controlled drug release. All compounds were characterized by elemental and spectroscopy (FT-IR, 1H-NMR) analysis. The toxicological tests recommend the products for further laboratory screening. [Figure not available: see fulltext.] © 2013 Versita Warsaw and Springer-Verlag Wien.
%G English
%L hal-01585388
%U https://hal.archives-ouvertes.fr/hal-01585388
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Low Molecular Weight Chitosan (LMWC)-based Polyplexes for pDNA Delivery: From Bench to Bedside
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Agirre, Mireia
%A Zarate, Jon
%A Ojeda, Edilberto
%A Puras, Gustavo
%A Desbrières, Jacques
%A Luis Pedraz, Jose
%Z Basque Country (UPV/EHU, Unidad de Formación e Investigación UFI 11/32), Basque Government (Department of Education, University and Research, predoctoral BFI-2011-2226 grant), and Mexican Government (Grant of the National Council of Science and Technology (CONACYT) )
%< avec comité de lecture
%@ 2073-4360
%J Polymers
%I MDPI
%V 6
%N 6
%P 1727-1755
%8 2014
%D 2014
%R 10.3390/polym6061727
%K low molecular weight chitosan (LMWC)
%K pDNA
%K polyplex
%K non-viral vector
%K gene delivery
%K transfection efficiency
%Z Chemical SciencesJournal articles
%X Non-viral gene delivery vectors are emerging as a safer alternative to viral vectors. Among natural polymers, chitosan (Ch) is the most studied one, and low molecular weight Ch, specifically, presents a wide range of advantages for non-viral pDNA delivery. It is crucial to determine the best process for the formation of Low Molecular Weight Chitosan (LMWC)-pDNA complexes and to characterize their physicochemical properties to better understand their behavior once the polyplexes are administered. The transfection efficiency of Ch based polyplexes is relatively low. Therefore, it is essential to understand all the transfection process, including the cellular uptake, endosomal escape and nuclear import, together with the parameters involved in the process to improve the design and development of the non-viral vectors. The aim of this review is to describe the formation and characterization of LMWC based polyplexes, the in vitro transfection process and finally, the in vivo applications of LMWC based polyplexes for gene therapy purposes.
%G English
%L hal-01560503
%U https://hal.archives-ouvertes.fr/hal-01560503
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T New di-(β-chloroethyl)-α-amides on N-(meta-Acylaminobenzoyl)-D, L-aminoacid supports with antitumoral activity
%+ Department of Computer Science
%+ Centre de Recherches sur les Macromolécules Végétales (CERMAV)
%A Sunel, V.
%A Popa, M.
%A Desbrieres, J.
%A Profire, L.
%A Otilia, P.
%A Catalina, L.
%Z cited By 8
%< avec comité de lecture
%@ 1420-3049
%J Molecules
%I MDPI
%V 13
%N 1
%P 177-189
%8 2008
%D 2008
%R 10.3390/molecules13010177
%Z Chemical Sciences/PolymersJournal articles
%X In order to obtain new compounds with antitumoural action the N-(meta-acylaminobenzoyl)-α-acylaminobenzoyl)-α-aminoacids 4-9 were prepared. These compounds were subsequently converted into the corresponding Δ2-oxazolin-5-ones 10-15, which in turn were submitted to a ring opening reaction with di-(β-chloroethyl)amine to afford the peptide supported N-mustards 16-21, which showed low toxicity and cytostatic activity similar to that of sarcolisine against the Ehrlich ascite and Walker 253 carcinosarcoma. © 2007 by MDPI.
%G English
%L hal-01585420
%U https://hal.archives-ouvertes.fr/hal-01585420
%~ CNRS
%~ UNIV-GRENOBLE1
%~ CERMAV
%~ INC-CNRS

%0 Journal Article
%T Kinetics aspects, rheological properties and mechanoelectrical effects of hydrogels composed of polyacrylamide and polystyrene nanoparticles.
%+ Institut Pluridisciplinaire de Recherche sur l'Environnement et les Matériaux (IPREM)
%+ Physico-chimie des polymères (PCP)
%A Thévenot, Caroline
%A Khoukh, Abdel
%A Reynaud, Stéphanie
%A Desbrières, Jacques
%A Grassl, Bruno
%Z Communauté d'Agglomération de Pau (France)
%< avec comité de lecture
%J Softmatter
%V 3
%P 437-447
%8 2006-11-13
%D 2006
%R 10.1039/b614166h
%K nanoparticles
%K hydrogel
%K kinetics
%K rheological properties
%K NMR spectroscopy
%K polyacrylamide
%K mechanoelectrical effect
%K smart material
%Z Chemical Sciences/PolymersJournal articles
%X Snake-cage gels were prepared using monodisperse polystyrene (PS) nano-size particles (R = 200 nm) in place of the more usually used linear polymer. The kinetics of the formation of the complementary polyacrylamide (PAM) hydrogel was studied alone or in the presence of the PS nanoparticles by 1H-NMR. Without PS, the reactivity ratios of the acrylamide (Am) and the crosslinking agent N,N'-methylene-bisacrylamide (BisAm) were computed using the initial kinetics of PAM gel at high crosslinker concentration (rA= 0.52, rB = 5.2, for Am and BisAm respectively). In the presence of PS, during the formation of nanoparticles composite gels (NPC gels), there was a decrease of the conversion rate with an increasing fraction of PS nanoparticles. This could be explained by steric effects which induce an increase of the elastic modulus of the matrix with the increasing fraction of PS nanoparticles. There was a considerable increase of the rheological properties of the NPC gels (i.e. tensile modulus) which was more pronounced at higher fractions of nanoparticles. We used the particular mechanical properties to develop a stimulus responsive (“smart”) material, i.e. a mechanoelectrical effect which may be used in the development of soft and wet tactile sensing devices
%G English
%2 https://hal.archives-ouvertes.fr/hal-00469606/document
%2 https://hal.archives-ouvertes.fr/hal-00469606/file/Soft_Matter_2007_3_437_447.pdf
%L hal-00469606
%U https://hal.archives-ouvertes.fr/hal-00469606
%~ CNRS
%~ UNIV-PAU
%~ IPREM
%~ INC-CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Microwave-assisted modifications of polysaccharides
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Desbrieres, J.
%A Petit, Charlotte
%A Reynaud, Stéphanie
%Z cited By 2
%< avec comité de lecture
%@ 0033-4545
%J Pure and Applied Chemistry
%I De Gruyter
%V 86
%N 11
%P 1695-1706
%8 2014
%D 2014
%R 10.1515/pac-2014-0711
%K grafting
%K modifications
%K microwave chemistry
%K POC-2014
%K polysaccharides
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Material chemistryJournal articles
%X Polysaccharides are a natural and renewable feed stock for synthesizing high performance macromolecular materials. However, their structure does not allow reaching all properties required for specific applications and chemical modifications are necessary to reach such objectives. Despite the use of natural polymers, the chemistry and processes employed are not environment-friendly due to the nature of chemicals, solvents or because the conventional chemical process are energy-consuming. On the other hand, microwave assisted processes were developed in organic chemistry since the 1980s and more recently for polymer chemistry (polymer formation and modification). Within the chemistry of natural polymers, the use of microwave irradiation has been exploited in the past two decades to alleviate limitations in the synthesis of graft modified polysaccharide materials. Microwave heating is described as more homogeneous, selective and efficient as compared to conventional heating resulting in faster reactions with fewer or no side products as example. Different results reported within the recent literature will be discussed considering the role of microwave irradiation and its consequence on the reaction parameters and properties of final materials. Grafting of polysaccharides, specific modification of polysaccharides or fibers particularly for preparing smart textiles or medical products as well as reactions of polysaccharides to valuable bio-platform molecule will be discussed. © 2014 IUPAC & De Gruyter.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01564099/document
%2 https://hal.archives-ouvertes.fr/hal-01564099/file/42591864.pdf
%L hal-01564099
%U https://hal.archives-ouvertes.fr/hal-01564099
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T New Grafted Copolymers Carrying Betaine Units Based on Gellan and N-Vinylimidazole as Precursors for Design of Drug Delivery Systems
%+ “Petru Poni” Institute of Macromolecular Chemistry 
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Racovita, Stefania
%A Baranov, Nicolae
%A Macsim, Ana, Maria
%A Lionte, Catalina
%A Cheptea, Corina
%A Sunel, Valeriu
%A Popa, Marcel
%A Vasiliu, Silvia
%A Desbrieres, Jacques
%< avec comité de lecture
%@ 1420-3049
%J Molecules
%I MDPI
%V 25
%N 22
%8 2020-11-20
%D 2020
%R 10.3390/molecules25225451
%K graft polymerization
%K N-vinylimidazole
%K gellan gum
%K betaine structure
%Z Chemical Sciences
%Z Chemical Sciences/PolymersJournal articles
%X New grafted copolymers possessing structural units of 1-vinyl-3-(1-carboxymethyl) imidazolium betaine were obtained by graft copolymerization of N-vinylimidazole onto gellan gum followed by the polymer-analogous reactions on grafted polymer with the highest grafting percentage using sodium chloroacetate as the betainization agent. The grafted copolymers were prepared using ammonium persulfate/N,N,N',N' tetramethylethylenediamine in a nitrogen atmosphere. The grafting reaction conditions were optimized by changing one of the following reaction parameters: initiator concentration, monomer concentration, polymer concentration, reaction time or temperature, while the other parameters remained constant. The highest grafting yield was obtained under the following reaction conditions: ci = 0.08 mol/L, cm = 0.8 mol/L, cp = 8 g/L, tr = 4 h and T = 50 °C. The kinetics of the graft copolymerization of N-vinylimidazole onto gellan was discussed and a suitable reaction mechanism was proposed. The evidence of the grafting reaction was confirmed through FTIR spectroscopy, X-ray diffraction, 1H-NMR spectroscopy and scanning electron microscopy. The grafted copolymer with betaine structure was obtained by a nucleophilic substitution reaction where the betainization agent was sodium chloroacetate. Preliminary results prove the ability of the grafted copolymers to bind amphoteric drugs (cefotaxime) and, therefore, the possibility of developing the new sustained drug release systems.
%G English
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-03043282/document
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-03043282/file/molecules-25-05451-v2.pdf
%L hal-03043282
%U https://hal-univ-pau.archives-ouvertes.fr/hal-03043282
%~ UNIV-PAU
%~ IPREM
%~ IPREM-PCM
%~ INC-CNRS
%~ CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T The Benefits of Smart Nanoparticles in Dental Applications
%+ Laboratory of Natural Polymers, Bioactive and Biocompatible Materials, Petru Poni Institute of Macromolecular Chemistry of Romanian Academy, Iasi, Romania (ICMPP)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Gugoasa, Ionela Aurica
%A Vasiliu, Silvia
%A Racovita, Stefania
%A Gugoasa, Ionela, Aurica
%A Lungan, Maria-Andreea
%A Popa, Marcel
%A Desbrieres, Jacques
%< avec comité de lecture
%@ 1661-6596
%J International Journal of Molecular Sciences
%I MDPI
%V 22
%N 5
%P 2585
%8 2021-03-04
%D 2021
%R 10.3390/ijms22052585
%K smart polymers
%K dental applications
%K nanoparticles
%Z Chemical Sciences/Material chemistry
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Analytical chemistryJournal articles
%X Dentistry, as a branch of medicine, has undergone continuous evolution over time. Thescientific world has focused its attention on the development of new methods and materials withimproved properties that meet the needs of patients. For this purpose, the replacement of so-called“passive” dental materials that do not interact with the oral environment with “smart/intelligent”materials that have the capability to change their shape, color, or size in response to an externallystimulus, such as the temperature, pH, light, moisture, stress, electric or magnetic fields, and chemicalcompounds, has received much attention in recent years. A strong trend in dental applications isto apply nanotechnology and smart nanomaterials such as nanoclays, nanofibers, nanocomposites,nanobubbles, nanocapsules, solid-lipid nanoparticles, nanospheres, metallic nanoparticles, nanotubes,and nanocrystals. Among the nanomaterials, the smart nanoparticles present several advantagescompared to other materials, creating the possibility to use them in various dental applications,including preventive dentistry, endodontics, restoration, and periodontal diseases. This review isfocused on the recent developments and dental applications (drug delivery systems and restorationmaterials) of smart nanoparticles.
%G English
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-03169023/document
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-03169023/file/ijms-22-02585-v2.pdf
%L hal-03169023
%U https://hal-univ-pau.archives-ouvertes.fr/hal-03169023
%~ UNIV-PAU
%~ IPREM
%~ IPREM-PCM

