%0 Journal Article
%T fLEX: multilingual test for assessing inFlection and LEXicon (results from ten Basque-speaking children, ages 5 through 10)
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Pourquié, Marie
%< avec comité de lecture
%@ 0214-9001
%J EKAIA Euskal Herriko Unibertsitateko Zientzi eta Teknologi Aldizkaria
%I Universidad del País Vasco
%N 36
%P 331--351
%8 2019
%D 2019
%R 10.1387/ekaia.19723
%K lexicon
%K morphosyntax
%K inflection
%K argument structure
%K language production and comprehension
%K psycholinguistics
%K Basque
%K multilingualism
%K language assessment tools
%K lexikoa
%K morfosintaxia
%K flexioa
%K egitura argumentala
%K hizkuntzaren ekoizpena eta ulermena
%K psikolinguistika
%K euskara
%K eleaniztasuna
%K hizkuntza aztertzeko tresnak
%Z Humanities and Social Sciences/LinguisticsJournal articles
%X The aim of this paper is to present the purpose, structure and functioning of the fLEX software and to examine the data collected from the assessment of ten Basque-speaking children. 160 stimuli divided in five oral tasks make up fLEX, which assesses lexical and inflectional processing in production and comprehension: object and action naming; sentence production and comprehension; and, case and postpositional phrase production. It was predicted that no difficulty would emerge in performing these tasks for children older than 5. Ten Basquespeaking children, ages 5 through 10, participated in the study. Of the five tasks, the poorest results were found in the sentence production task. The verb auxiliaries «ditu» and «die» were specifically prone to errors in production but not in comprehension. Ergative, destinative and local genitive case production was more problematic than the production of other cases. Data are described quantitatively and qualitatively, by pointing out weaknesses in lexical and inflectional processing. Defining typically developing children’ weaknesses is crucial in order to diagnose language disorders. In addition to providing a useful corpus from a psycholinguistic perspective, the goal of this paper is to emphasize the need for developing language assessment tools in Basque.
%X Artikulu honen helburua da fLEX aplikazio eleanitzaren motibazioa, egitura, eta funtzionamendua aurkeztea eta, horretan oinarrituta, hamar haur euskaldunengandik bildutako datuak aztertzea. Ahozko bost probak eta orotara 160 estimuluk osatzen dute fLEX, prozesaketa lexikala zein flexionala, ekoizpena eta ulermenean aztertzeko: gauzak eta ekintzak izendatzea; esaldiak ekoiztea eta ulertzea; kasu eta posposizio sintagmak ekoiztea. Aurreikusten zen proba hauek egiteko zailtasunik ez agertzea bost urtetik gorako haurrentzat. 5-10 urte tarteko hamar haur euskaldunek parte hartu dute ikerketan. Emaitzek erakusten dute esaldiak ekoiztea dela emaitza ahulenak dakartzan proba; «ditu» eta «die» aditz laguntzaileak ekoizteko emaitzak apalak direla forma horiek ulertzeko arazo handirik ez den arren; ergatiboa, destinatiboa eta lekuzko genitiboa ekoizteko, beste kasu eta posposizioak ekoizteko baino huts gehiago agertzen dira. Datuen kantitatezko eta kalitatezko deskribapena egiten da, prozesaketa flexionalean eta lexikalean puntu ahulak erakutsiz. Haurren garapen tipikoaren ahuleziak definitzea ezinbestekoa da hizkuntza-arazoak diagnostikatzeko garaian. Psikohizkuntzalaritzaren ikuspegitik corpus baliagarri bat eskaintzeaz gain, euskaraz hizkuntza aztertzeko tresnak garatzeko beharra azpimarratzen da.
%G Basque
%L hal-02553892
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02553892
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Afasia euskaraz aztertzeko tresna berria bidean: CAT testaren euskal egokitzapenaren gakoak eta estandarizaziorako urratsak
%+ Centre de recherche sur la langue et les textes basques (IKER)
%+ Universidad del Pais Vasco / Euskal Herriko Unibertsitatea [Espagne] (UPV/EHU)
%A Pourquié, Marie
%A Munarriz Ibarrola, Amaia
%< avec comité de lecture
%@ 2530-9412
%J Osagaiz : osasun-zientzien aldizkaria
%I Osasungoa Euskalduntzeko Erakundea
%V 2
%N 2
%P 13--24
%8 2018
%D 2018
%R 10.26876/osagaiz.2.2018.149
%K aphasia
%K assessment-tool
%K Basque
%K bilingualism
%K cognitive neuroscience
%K afasia
%K ebaluazio-tresna
%K euskara
%K elebitasuna
%K neurozientzia kognitiboak
%Z Humanities and Social Sciences/LinguisticsJournal articles
%X In the field of aphasia research and treatment, it is necessary to take into account world language diversity, because language-specific grammatical properties condition aphasia symptom manifestations. In addition, aphasia assessment and treatment needs to be adapted to the linguistic environment of patients and their families, and in the case of bilingual individuals, the assessment should be done in both languages. Considering Europe’s language diversity and in order to carry out comparative studies and enable the assessment of bilingual people with aphasia, the Comprehensive Aphasia Test (CAT) is currently being adapted for fourteen languages, one of which is Basque. There is an urgent need in the case of Basque people with aphasia, as there is as yet no standardized language assessment tool. As a consequence, although Basque people are bilingual, they are generally only assessed in their non-Basque language. In this paper, we present the Basque adaptation of CAT, the main challenges we faced during this process, the decisions we took, and the steps perfomed to standardize the test. Based on concrete examples, we show that adapting a language assessment tool does not simply consist in translating the orginal test. Indeed, we want to emphasize that in order to create a standardized test usable in the Basque country, it is essential to take into account both psycholinguistic and sociolinguistic parameters, in addition to drawing on Basque grammar. This assessment tool will be of particular relevance to many aspects of both healthcare and research, and it will provide Basque speakers with aphasia the possibility of being assessed in their first or second language, on the basis of a standardized tool.
%X Afasiaren inguruko ikerketan eta tratamenduan ezinbestekoa da munduko hizkuntza-aniztasuna kontuan hartzea; izan ere, hizkuntzen ezaugarri gramatikalek afasian agertzen diren sintomak baldintzatzen dituzte. Bestalde, komeni da azterketa eta tratamendua gaixoaren eta haren senideen ingurune linguistikoari egokitzea; eta afasia duten elebidunen kasuan, gainera, hizkuntza guztiak aztertzea. Europako hizkuntza-aniztasuna kontuan harturik, hizkuntzen arteko ikerketa konparatiboak eta afasia duten elebidunen azterketa ahalbidetzeko asmoz, afasia ebaluatzeko Comprehensive Aphasia Test (CAT) (1) tresna hamalau hizkuntzatara egokitzeko bidean da, eta euskara dago hizkuntza horien artean. Afasia duten euskal hiztunen kasuan premia berezia dago, ez baitago hizkuntzaren azterketa egiteko tresna estandarizaturik. Ondorioz, euskaldunak elebidunak izan arren, askotan euskara ez den hizkuntza soilik ebaluatzen zaie. Artikulu honetan, CAT tresna euskarara egokitzeko ekimenaren berri ematen dugu, eta aurkezten ditugu prozesu horretan izaten ari garen erronka nagusiak, hartutako erabakiak eta tresna estandarizatzeko emandako urratsak. Zenbait adibide zehatzetan oinarrituta, erakusten dugu test bat egokitzea ez dela jatorrizko testa itzultzea soilik itzultzea. Hain zuzen, aditzera eman nahi dugu Euskal Herri osoan erabilgarria den tresna estandarizatu bat egin ahal izateko, ezinbestekoa dela aldagai psikolinguistiko eta soziolinguistikoak kontuan hartzea, euskal gramatikan oinarritzeaz gain. Ebaluazio-tresna hori oso garrantzizkoa izango da hainbat mailatan, bai osasunari eta ikerketari begira, eta aukera eskainiko die afasiadun euskaldunei ebaluazioa euren lehen edo bigarren hizkuntzan egiteko tresna estandarizatu baten oinarrian.
%G Basque
%L hal-02553890
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02553890
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Investigating Vulnerabilities in Grammatical Processing of Bilinguals: Insights from Basque-Spanish Adults and Children
%+ Centre de recherche sur la langue et les textes basques (IKER)
%+ University of Oslo (UiO)
%A Pourquié, Marie
%A Lacroix, Hugues
%A Kartushina, Natalia
%< avec comité de lecture
%@ 1879-9264
%J Linguistic Approaches to Bilingualism
%I John Benjamins Pub. Co.
%V 9
%N 4-5
%P 600--627
%8 2019
%D 2019
%R 10.1075/lab.17035.pou
%Z Humanities and Social Sciences/LinguisticsJournal articles
%X Bilinguals show a large gap in their expressive-receptive abilities, in both languages. To date, most studies have examined lexical processing. The current study aimed to assess comprehension and production of verb agreement, i.e., grammatical processing, in bilinguals, and to examine the factors that might modulate them: exposure, age and language-specific morphological complexity. Twenty balanced Basque-Spanish bilinguals (10 adults and 10 children) were assessed on comprehension and production of subject-verb agreement in both languages and object-verb agreement in Basque. Twenty age-matched Spanish-dominant Basque-Spanish bilinguals were assessed in Spanish only. The results revealed a consistent gap in Basque in both children and adults, with an advantage for comprehension. In Spanish, a gap appeared in children only, with an advantage for production. The gap size did not vary with the amount of language exposure but with age and morphological complexity, suggesting that these factors modulate bilinguals’ grammatical processing.
%G English
%L hal-02533288
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02533288
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Cross-linguistic adaptations of The Comprehensive Aphasia Test : Challenges and solutions
%+ National and Kapodistrian University of Athens (NKUA)
%+ Savoirs, Textes, Langage (STL) - UMR 8163 (STL)
%+ Université de Lille - UFR des Humanités (Lille UFRH)
%A Fyndanis, Valantis
%A Lind, Marianne
%A Varlokosta, Spyridoula
%A Kambanaros, Maria
%A Soroli, Efstathia
%A Ceder, Klaudia
%A Grohmann, Kleanthes
%A Rofes, Adrià
%A Simonsen, Hanne Gram
%A Bjekić, Jovana
%A Gavarró, Anna
%A Kuvač Kraljević, Jelena
%A Martínez-Ferreiro, Silvia
%A Munarriz, Amaia
%A Pourquié, Marie
%A Vuksanović, Jasmina
%A Zakariás, Lilla
%A Howard, David
%< avec comité de lecture
%@ 0269-9206
%J Clinical Linguistics & Phonetics
%I Taylor & Francis
%V 31
%N 7-9
%P 697-710
%8 2017-03-31
%D 2017
%R 10.1080/02699206.2017.1310299
%Z Humanities and Social Sciences
%Z Cognitive scienceJournal articles
%G English
%L hal-03131281
%U https://hal.archives-ouvertes.fr/hal-03131281

