%0 Thesis
%T The extra-juridical protection of fundamental rights in constitutional law
%+ Droit public comparé - Droit international et Droit européen (DPCDIDE)
%A LÖHRER, Dimitri
%I Université de Pau et des pays de l'Adour
%Y Olivier Lecucq
%8 2013-06-05
%D 2013
%K extra-juridical protection
%K ombudsman
%K fundamental rights
%K constitutionnel law
%K protection non juridictionnelle
%K droits fondamentaux
%K droit constitutionnel comparé
%K Défenseur des droits
%K Défenseur du Peuple
%K Provedor de Justiça
%Z Humanities and Social Sciences/LawTheses
%X The figure of the Human rights ombudsman appeared into the Iberian Peninsula at the demise of Franco and Salazar dictatorships in order to facilitate the transition to democracy. In France this figure finds its contemporary justification due to the insufficiency of the classic mechanisms of guarantee of the fundamental rights.The Human rights ombudsman is specially designed for the protection of fundamental rights, and is indeed a form of non jurisdictional protection in a perspective of complementarity of the traditional ways, especially the court of appeal and, as such, favors the emergence of an institutional system of complete protection.The protection proposed by the Human rights ombudsman contributes to an indisputable consolidation of the fundamental rights however it is essential that it remains relative. Yet, it does not fill all the inadequacies affecting the other instances of guarantee as the Human rights ombudsman suffers from imperfections likely to prejudice the effectiveness of its mission of protection of the person’s human rights.
%X Apparue au sein de la péninsule ibérique à la sortie des dictatures salazariste et franquiste en vue de faciliter la transition vers la démocratie, la figure de l’ombudsman spécialisé, désormais réceptionnée par la France à travers l’institution du Défenseur des droits, trouve sa raison d’être contemporaine à l’aune de l’insuffisance des mécanismes classiques de garantie des droits fondamentaux.Spécialement aménagé pour la défense des droits et libertés, l’human rights ombudsman se présente, en effet, comme une forme de protection non juridictionnelle s’inscrivant dans une perspective de complémentarité des traditionnelles voies, spécialement juridictionnelles, de recours et, à ce titre, favorise l’émergence d’un système institutionnel de protection complet. Contribuant en pratique à une consolidation indiscutable des droits fondamentaux, la protection proposée par l’ombudsman spécialisé, aussi indispensable soit-elle, n’en demeure pas moins relative. Outre qu’il ne permet pas de combler l’ensemble des insuffisances affectant les autres instances de garantie, l’human rights ombudsman souffre lui-même d’imperfections de nature à préjudicier à l’effectivité de sa mission de sauvegarde des droits de la personne humaine.
%G French
%2 https://halshs.archives-ouvertes.fr/tel-01465159/document
%2 https://halshs.archives-ouvertes.fr/tel-01465159/file/th%C3%A8se%20%282%29.pdf
%L tel-01465159
%U https://halshs.archives-ouvertes.fr/tel-01465159
%~ SHS
%~ CNRS
%~ UNIV-PAU
%~ DPCDIDE
%~ AO-DROIT
%~ DICE
%~ IE2IA
%~ UNIV-AMU
%~ TESTUPPA2
%~ UPPA-OA

