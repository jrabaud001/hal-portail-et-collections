%0 Journal Article
%T Jean Etxepare eta Pío Baroja
%+ University of the Basque Country [Bizkaia] (UPV/EHU)
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Ortiz de Pinedo, Aitor
%A Urtea, Año, L
%A Etxepare, Jean
%A Baroja, Pío
%< avec comité de lecture
%@ 0046-435X
%J Fontes Linguae Vasconum : Studia et documenta
%I Institución Príncipe de Viana - Gobierno de Navarra
%P 85-112
%8 2018-06-30
%D 2018
%K Jean Etxepare
%K laicity
%K Pio Bareja
%K comparative literature
%K heterodoxia
%K laicidad
%K literatura comparada
%Z Humanities and Social Sciences/LinguisticsJournal articles
%X this article comes after a thorough study for a PhD dissertation on a book by Jean Etxepare (Beribilez, 1931). A comparative study is drawn between Jean Etxepare and Pío Baroja with the aim of finding their coincidences and Differences. this research is based mostly in their basque identitary ideology, even though some attention is also paid to some philosophical issues. Both writers are part of the same European intellectual ambiance, and both are empathetic to the Basque Country. Broadly speaking, it can be said that their literary work was heavily determined by the profile of their hypothetical readers, as Jean Etxepare wrote mainly for Basque language speakers and Pío Baroja addressed his writings to spanish ones.
%X Este artículo es continuación de un completo estudio para una tesis doctoral sobre el libro de Jean Etxepare Beribilez (1931). se realiza un estudio comparativo entre Jean Etxepare y Pío Baroja con el objeto de encontrar coincidencias y diferencias. Esta investigación se basa principalmente en su ideología identitaria vasca, aunque también se presta atención a cuestiones filosóficas. Los dos escritores son parte del mismo ambiente intelectual europeo, y ambos son empáticos con el País Vasco. se puede decir que suobra literaria estuvo determinada por el perfil de sus lectores hipotéticos, ya que Jean Etxepare escribió principalmente para hablantes de euskera y Pío Baroja dirigió sus escritos a los españoles
%G Basque
%2 https://artxiker.ccsd.cnrs.fr/artxibo-01965789/document
%2 https://artxiker.ccsd.cnrs.fr/artxibo-01965789/file/Jean%20Etxepare%20eta%20Pio%20Baroja.pdf
%L artxibo-01965789
%U https://artxiker.ccsd.cnrs.fr/artxibo-01965789
%~ CNRS
%~ UNIV-PAU
%~ AO-LINGUISTIQUE
%~ SHS
%~ IKER
%~ UNIV-BORDEAUX-MONTAIGNE
%~ OR-COLLCODE
%~ ARTXIKER
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Jean Etxepare Bidegorri mediator
%+ University of the Basque Country [Bizkaia] (UPV/EHU)
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Ortiz de Pinedo, Aitor
%< avec comité de lecture
%J Euskera. Euskaltziandia'ren lan eta agiriak. Trabajos y actas de la Academia de la Lengua Vasca. Travaux et actes de l'Académie de la Langue Basque
%I Real Academia de la Lengua Vasca
%V 2
%P 365 - 391
%8 2019
%D 2019
%Z Humanities and Social Sciences/LinguisticsJournal articles
%X Jean Etxepare heldua saiatzen da Beribilez (1910) bidaia-kontakizunean hil aurreko pentsamoldea adierazten. Azpimarratu nahi da medikuak egin duen ahalegina gizartean topa dituen jarrera kontrajarri batzuk bateratzeko: hain zuzen, euskal batasunari dagozkionak, eta laikotzearen ingurukoak, XX. mendearen hasieran muntakoak ziren aferak; desadostasun horien inguruan aipagarriak dira, hurrenez hurren, Euskal Herriko historiaren ikuspegi orohartzailea, eta, bestetik, jesuiten gaineko estereotipo batzuk kolokan jartzea. Gako-hitzak: bidaia-kontakizuna, euskaltasuna, laikotzea, literatura konparatua.
%G Basque
%2 https://artxiker.ccsd.cnrs.fr/artxibo-02095924v2/document
%2 https://artxiker.ccsd.cnrs.fr/artxibo-02095924v2/file/Jean%20Etxepare%20Bidegorri%20zubigile.pdf
%L artxibo-02095924
%U https://artxiker.ccsd.cnrs.fr/artxibo-02095924
%~ AO-LINGUISTIQUE
%~ UNIV-PAU
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ IKER
%~ SHS
%~ OR-COLLCODE
%~ ARTXIKER
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Society in the literature of JeanEtxepare
%+ University of the Basque Country [Bizkaia] (UPV/EHU)
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Ortiz de Pinedo, Aitor
%< avec comité de lecture
%@ 0422-7328
%J Egan
%I Euskalerriko Adiskideen Elkartea-Real Sociedad Vascongada de Amigos del País
%N 3/4
%P 123-148
%8 2019-01-17
%D 2019
%Z Humanities and Social Sciences
%Z Humanities and Social Sciences/LiteratureJournal articles
%X Approach to the social understanding reflected in the writings of Jean Etxepare (1877-1935), in relation to his aesthetics and ethics views.
%X Jean Etxepareren (1877-1935) gizarte ikuspegia areago zehazteko urratsak dira. Haren aukera estetiko eta etikoek gizartearekin zuten lotura batzuk jasoko dira.
%G Basque
%2 https://artxiker.ccsd.cnrs.fr/artxibo-01986603/document
%2 https://artxiker.ccsd.cnrs.fr/artxibo-01986603/file/egan_2018_3-4.pdf
%L artxibo-01986603
%U https://artxiker.ccsd.cnrs.fr/artxibo-01986603
%~ CNRS
%~ UNIV-PAU
%~ SHS
%~ IKER
%~ UNIV-BORDEAUX-MONTAIGNE
%~ OR-COLLCODE
%~ ARTXIKER
%~ TESTUPPA2
%~ UPPA-OA

%0 Unpublished work
%T Letrak kolokan berriz?
%+ University of the Basque Country [Bizkaia] (UPV/EHU)
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Ortiz de Pinedo, Aitor
%J Elinberri
%I elinberri
%8 2019-10-01
%D 2019
%Z Humanities and Social Sciences/LiteratureOther publications
%X Artikulu honetan globalizazioaren aurrerakadari begiratuko diogu, geure moduko gizakiaren aldetik. Izan ere, XX. mendean AEB hurbilagoa egin zitzaigun europarrei, besteak beste Bigarren Mundu Gerraren ondorioz. Ameriketatik ekarri ziren tramankulu intelektualen artean, pentsamendu positiboarena izan zen bat; beste zer batzuk ere inportatu ziren: Fred Astaire, "chewing-gum", Marilyn Monroe, Elvis Presley, "peanut butter", Carlitos eta Snoopy… bakar batzuk aipatzearren. Europa zahar honetan, bote prontoan esanda, bestelako modak genituen: absurdoaren antzerkia, existentzialismoa, pertsonalismoa, eta Espainia frankistan, neoklasizismoa. Gogoratu ditzagun gizarteko bestelako mitologia-itemak zein izan ziren: Tourra, Seat 600, bikinia, telebista, etab. Horra arte sarrera: baina zer dela eta psikologia positiboak egin duen gorakada azken aldi honetan, Mendebaldeko herrialdeetan, behinik behin? Izan daiteke, akaso, psikologiaren gorakada zabalaren barneko fenomeno bat. Arte tradizionalek (literatura, arte plastikoak…) betetzen zuten tokia murriztu egin da, dagoeneko, itxura batean. Arthur Schopenhauerrek gogokoa zuen artea iraganeko edergailua bihurtu ote da?; filosofoaren ustez, artea zen bizitza zentzuz eta taiuz eramaten laguntzen zigun baliabide nagusietako bat, bizimodu latzari aurre eginda. Psikologiaren auzia dela eta, dramatiko jarrita, esango dugu gizarte berriak atomoak behar dituela, hau da, gizaki atomizatuak: hiritar noranahi, nonahi eramangarriak estandar orokor baten barruan, sustrairik gabeak, eredu globalizatzaile eta neoliberal baten eredura. Agustín García Calvo filosofoak aspaldi iragarri zuen estatuak bere asmoak aurrera atera ahal izateko banako lokabeak behar dituela. Delako psikologia positiboak mugatu ditzake gizartearen eta giza taldeen eraginkortasuna. Agian komeniko litzateke psikologia soziologiarekin orekatzea. Hau da, psikologiaren ahuleziak soziologiaren indar guneekin orekatu. Ez dirudi ausartegia esatea psikologia positiboak mugatu egin duela literaturaren eremua ere, nolabait iragazita literaturak zuen eskumena sentimenduen adierazpenean eta diziplinartekotasunean. Kinka larria: lan bila bazoaz, jarrera positiboa erakutsi ezazu. Hala ere, kontsumitzaile automata baino zer edo zer gehiago izan gura baduzu, kontuan izan denetariko arte-produktu autoktonoak ohiko autolaguntza amerikarreko produktuak baino hurbilagoak eta eraginkorragoak, zerrenda oparo eta ederrean; eralgi sos horiek modu burutsuan.
%G Basque
%2 https://artxiker.ccsd.cnrs.fr/artxibo-02294173v2/document
%2 https://artxiker.ccsd.cnrs.fr/artxibo-02294173v2/file/Letrak%20kolokan%20berriz.pdf
%L artxibo-02294173
%U https://artxiker.ccsd.cnrs.fr/artxibo-02294173
%~ UNIV-PAU
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ IKER
%~ SHS
%~ OR-COLLCODE
%~ ARTXIKER
%~ TESTUPPA2
%~ UPPA-OA

%0 Book
%T GABON-KANTAK
%+ University of the Basque Country [Bizkaia] (UPV/EHU)
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Ortiz de Pinedo, Aitor
%8 2020
%D 2020
%Z Humanities and Social Sciences/LinguisticsBooks
%G Basque
%2 https://artxiker.ccsd.cnrs.fr/artxibo-02304029v2/document
%2 https://artxiker.ccsd.cnrs.fr/artxibo-02304029v2/file/Gabon-kantak%20Euskaltzaindiaren%20Literatur%20Antologia.pdf
%L artxibo-02304029
%U https://artxiker.ccsd.cnrs.fr/artxibo-02304029
%~ AO-LINGUISTIQUE
%~ UNIV-PAU
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ IKER
%~ SHS
%~ OR-COLLCODE
%~ ARTXIKER
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Two opposed narratives: Uztaro and Beribilez
%+ University of the Basque Country [Bizkaia] (UPV/EHU)
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Ortiz de Pinedo, Aitor
%< avec comité de lecture
%@ 1130-5738
%J Uztaro. Giza eta gizarte-zientzien aldizkaria
%I Udako Euskal Unibersitatea
%8 2020
%D 2020
%K values
%K active life
%K contemplative life
%K laicism
%Z Humanities and Social Sciences/LiteratureJournal articles
%X Abstract: this article compares two narratives of the thirty´s, each from a different frame of mind taking into account the predominant moral values, somehow a critical period because of the evolving Basque society. By car is about the difficulties for modern values to insert in traditional society; Harvest is about the impossibility of virtuous life to accommodate to the changing behaviours of people because of the bad results of the effort, with a high cost for the woman protagonist. By car is not well received in his time because of the new values that he promotes; on the other hand, Harvest shows that it is difficult for the tradition to join the new wave of modernity. Even nowadays, there is a social contest about the preference of active life and contemplative life. It seems that the huge fluxus of information and images hinders a moral appraisal of new information and images.Key words: laicism, contemplative life, active life, values
%X Laburpena: artikulu honetan 1930eko hamarkadan plazaratu ziren bi eleberri konparatzen dira, bi balio-paradigma ezberdinekoak. Beribilez bidaia-kontakizunak adierazten duen kezka da nola egokitu daitekeen euskal gizartea tradizioarekin haustera datorren mundu modernoarekin. Uztaro ohiturazko eleberriak adierazten du amildegi gaindiezina balio tradizionalen eta modernoen artean. Beribilezeko kontalariak bere burua babesgabe ikusten du, gizarte tradizionalean somatu duen harrera epelagatik, kontraesan latzean; Uztaroko protagonista gizarteko esparrua baztertzera doa, aldaketa sozialek dakarten arimako narrioagatik, zoritxarreko maitasun baten ondorioz. Oraindik ere gaur egun jokoan da kontenplazioaren eta ekintzaren arteko joan-etorria, bizimodu orekatua lortzeko markoaren barruan. Ikusten da euskal gizarteak duen zailtasuna bere baliabide ideologikoak doitzeko, askoz anitzagoa eta aldakorragoa den gizarte laikora berregokitzeko, bi ikuspegi kontrajarritik, gura bada, baina beti ere euskal problematikaren barnean.Gako-hitzak: laikotasuna, lasaitasuna, balioak, ekintza
%G Basque
%2 https://hal.archives-ouvertes.fr/hal-02574068/document
%2 https://hal.archives-ouvertes.fr/hal-02574068/file/Uztaro%20eta%20Beribilez%20HALS.pdf
%L hal-02574068
%U https://hal.archives-ouvertes.fr/hal-02574068
%~ IKER
%~ IKER_UMR5478
%~ UNIV-PAU
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS
%~ ARTXIKER
%~ OR-COLLCODE
%~ TESTUPPA2
%~ UPPA-OA

%0 Book
%T Josetxo Azkona biobibliografia
%+ University of the Basque Country [Bizkaia] (UPV/EHU)
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Ortiz de Pinedo, Aitor
%8 2020
%D 2020
%Z Humanities and Social Sciences/Linguistics/domain_shs.langue.textBooks
%G Basque
%2 https://artxiker.ccsd.cnrs.fr/artxibo-02865872/document
%2 https://artxiker.ccsd.cnrs.fr/artxibo-02865872/file/Josetxo%20Azkona%20BIOBIBLIOGRAFIA%20%282020-6-11%29.pdf
%L artxibo-02865872
%U https://artxiker.ccsd.cnrs.fr/artxibo-02865872
%~ ARTXIKER
%~ AO-LINGUISTIQUE
%~ UNIV-PAU
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ IKER
%~ SHS
%~ OR-COLLCODE
%~ TESTUPPA2
%~ UPPA-OA

%0 Unpublished work
%T Euskara unibertsala (Elinberri blogean)
%+ University of the Basque Country [Bizkaia] (UPV/EHU)
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Ortiz de Pinedo, Aitor
%8 2020-05-13
%D 2020
%Z Humanities and Social Sciences/LiteraturePreprints, Working Papers, ...
%X Kontu jakina, da, baina euskara kultura-tresna eraginkor gisa irauanarazteko premia dago etengabe. Euskaldunak arrisku bat daukagu, gure partikularismoan itota geratzea: identitate berriak sortzeko premian gaude. Hala ere, globalizazioaren garaian oreka berria aurkitu behar dugu lokalaren eta globalaren artean. Kike Amonarrizek azken liburuan euskalgintzaren impassea azpimarratu du. Historian zehar olatu globalizatzaileak izan dira, etengabe, orain artekoan ere. Bernard Etxeparek inprentaren zabalkundeari erantzun zion. Joanes Leizarragak protestantismoa etxekotu zuen. Euskalerriko adiskideek Ilustrazioa inportatu zuten (Juan Antonio Mogel, etab.) Jose María Iparragirrek ere bertsolaritza apala osatzeko saio arrakastatsua egin zuen, eta musika jasoaren baliabideak euskarako kantetan erabili zituen. Iparragirrerena da Eman eta zabal ezazu munduan fruitua goiburua. Xenpelarrek baserritik erantzun zion. Ziur aski, bi langintzak beharko dira, hala jasoa nola basikoa. Gerora, Orixek Proventzari mailegatu zion idazlan unibertsal bat egitearen irtenbidea hizkuntza sustatzeko, eta Euskaldunak sortu zuen, euskaldunen freskoa. Arrakasta ez zen hainbestekoa izan, besteak beste 36ko gerraren ondorenengatik. Errepublika garaian uste zuten literatur maisu-lan batek euskara salbatzeko modua zela; Fréderic Mistralek Mireio idatzi zuen, Orixek euskaratua bide horretatik. Okzitania sustatzeko ahalegina egin zuen Mistral Nobel Sariak, baita Pariseko parlamentuan ere. XX. mende hasierako jeltzaleek Irlandaren eredua hurbiletik aztertu zuten erlijioa eta burujabetasuna lotzen zituelako, bereziki Jagi-jagi erakundeko partaideek. Francoren garaian, Argeliako eredua ere mailegatu zen. Frantz Fanon eta Albert Memmi ere gureganatu genituen: Txillardegik Kolonizatuaren ezaugarria izenburuarekin euskaratu zuen Memmiren frantsesezko lana. Txillardegik Austriako marxismoa (austromarxismoa) hobetsi zuen bizitzako azken urteetan, hau da, Mendebaldeko gizartearekin adosgarria izan zitekeen erreformismo soziala.
%G Basque
%2 https://halshs.archives-ouvertes.fr/halshs-02572853/document
%2 https://halshs.archives-ouvertes.fr/halshs-02572853/file/Euskara%20unibertsala.pdf
%L halshs-02572853
%U https://halshs.archives-ouvertes.fr/halshs-02572853
%~ SHS
%~ IKER
%~ IKER_UMR5478
%~ UNIV-PAU
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ ARTXIKER
%~ OR-COLLCODE
%~ TESTUPPA2
%~ UPPA-OA

%0 Thesis
%T Beribilez (By car) (1931) de Jean Etxepare : travel and ideology
%+ University of the Basque Country [Bizkaia] (UPV/EHU)
%+ Centre de recherche sur la langue et les textes basques (IKER)
%+ Université Bordeaux Montaigne
%A Ortiz de Pinedo, Aitor
%I Université Bordeaux-Montaigne III
%I Euskal Herriko Unibertsitatea-Universidad del País Vasco
%Y Jon Casenave
%Y Jon Kortazar
%8 2018-07-05
%D 2018
%K travel literature
%K ideology
%K semiotics
%K Jean Etxepare
%K Basque literature
%K XX Century
%K imagology
%K ethics
%K aesthetics
%K history of literature
%K German philosophy
%K the Basque Country
%K dissent
%K modernity
%K laicism
%K autobiography
%K Christianity
%K existential quest.
%K littérature de voyage
%K idéologie
%K sémiotique
%K Jean Etchepare
%K littérature basque
%K XX. Siècle
%K imagologie
%K éthique
%K esthétique
%K histoire de la littérature
%K philosophie allemande
%K Pays Basque
%K dissidence
%K modernité
%K laïcisme
%K autobiographie
%K christianisme
%K quête existentielle.
%Z Humanities and Social Sciences/Linguistics/domain_shs.langue.textTheses
%X The second book ((original Basque title) Beribilez; (engl.) By car) by Jean Etxepare(1877-1935) is analysed in this doctoral thesis. The aim of this research is to define theauthor’s ideology/ in his intellectual maturity and his image of the Basque Country, along thejourney made by car through the Southern Provinces. Semiotic Methodology (Philippe Hamon)and Imagology (Daniel-Henri Pageaux) have been used as a study method.We have looked at the academic training of Jean Etxepare, and we have seen that in theNorthern Basque Country he was confronted with the hegemonic traditional catholic way ofthinking. During the years of dissent between the State and the Church, Etxepare chose the freethinkingpath, inspired by German philosophy.In the area of aesthetics, his view of the Basque landscape is mostly economicist. Hisperspective of the appreciation of rural lands combines with a poetic mysticism. He considersthe display of Baroque religious luxury that he sees in the Sanctuary of Loyola, spirituallydeficient; we can see that, on the one hand, we are dealing with an intellectual author of petitbourgeois standing and sound artistic judgement... On the other hand, even if he proves to be aknowledgeable person, he is very capable of enjoying the small pleasures of life as gastronomy,wines, dances and eroticism in a harmonious and measured way. He moves moderately awayfrom traditional confessional asceticism, giving in his book to the pleasures of the body anunusual and enlightening place unknown to the catholic dominant literature.In the field of ethics, he criticizes the urban growth of San Sebastian: he proclaimssecular spirituality against the materialism and degeneration of the high bourgeoisie. Heprophesied that there would be the Second World War, because he saw the humanbeings lacking mutual respect. As a positivist, he criticizes any irrational belief. Finally, heindicates that in adulthood he has been aligned with the Greek philosophy of measure, in spiteof his non-conformist youth. About the Basque Country, he intends to promote solidaritybetween the two sides of the frontier, reinterpreting the history passages in a diplomaticway. His attitude towards the South is of a filia one (Pageaux). As a conclusion, we can say thatthe writer measured the expression of his dissident way of thinking, because he did not wanthis second book to be rejected as had been the case with the first one (Buruxkak, 1910).
%X Résumé : Dans cette thèse est analysé le deuxième et dernier livre Beribilez (En voiture) de Jean Etchepare (1877-1935). Le but de la recherche est d’établir l'idéologie de l'auteur à l’âge adulte et l’image qu’il donne tout au long de ce récit de voyage, fait en voiture à travers le Pays Basque. On a utilisé deux méthodologies principalement : la sémiotique (Philippe Hamon) et l´imagologie (Daniel-Henri Pageaux). On a relu les critiques portant sur l´œuvre de cet auteur pour les comparer avec nos intuitions. Nous avons sondé la formation intellectuelle de Jean Etchepare, et nous avons vu qu'au Pays Basque du Nord, il était confronté à la pensée traditionnelle, dominante à cette époque. Durant la confrontation entre l'État et l'Église autour de la laïcisation, Etchepare a choisi une voie indépendante inspirée souvent par la philosophie allemande. Dans le domaine de la pensée esthétique, la vision du paysage basque est plutôt économiciste, l'appréciation des terres rurales se combine avec un mysticisme poétique. Au sein des beaux-arts, il critique l'enthousiasme théâtral du luxe religieux baroque qu'il voit dans le Sanctuaire de Loyola; dans l´aspect du savoir-vivre il s'éloigne modérément de l'ascétisme confessionnel traditionnel, donnant dans le texte une place inhabituelle jusqu’alors, aux petits plaisirs des sens (gastronomie, danse…). Dans le domaine de l'éthique, il critique la croissance urbaine de Saint-Sébastien : il proclame la spiritualité laïque contre le matérialisme et la dégénérescence de la haute bourgeoisie. Il prophétisa la Seconde Guerre mondiale qui s´approchait, parce qu'il voyait les êtres humains sans respect mutuel. En tant que positiviste, il critique aussi toute croyance irrationnelle. Enfin, il indique qu'à l'âge adulte il s'est aligné sur la philosophie grecque de la mesure. A propos du Pays Basque, il entend promouvoir la solidarité entre les deux versants basques des Pyrénées, en réinterprétant les avatars de l'Histoire de manière diplomatique. L'attitude envers le Sud-est de filia (Pageaux). En conclusion, on peut dire que l'écrivain dans ce livre a modélisé soigneusement l´expression de sa pensée, car il ne voulait pas que l´ouvrage soit rejeté, comme ce fut le cas avec le premier (Glanes, 1910).
%G Basque
%2 https://artxiker.ccsd.cnrs.fr/tel-01986633/document
%2 https://artxiker.ccsd.cnrs.fr/tel-01986633/file/These_Aitor_ORTIZ_DE_PINEDO.pdf
%L tel-01986633
%U https://artxiker.ccsd.cnrs.fr/tel-01986633
%~ CNRS
%~ UNIV-PAU
%~ AO-LINGUISTIQUE
%~ SHS
%~ IKER
%~ UNIV-BORDEAUX-MONTAIGNE
%~ STAR
%~ OR-COLLCODE
%~ ARTXIKER
%~ TESTUPPA2
%~ UPPA-OA
%~ MONTAIGNE_TABLE

%0 Book
%T Euskal literatura Gernika aldizkari liberalean (1945-53)
%+ University of the Basque Country [Bizkaia] (UPV/EHU)
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Ortiz de Pinedo, Aitor
%8 2009-12-03
%D 2009
%Z Humanities and Social Sciences/Linguistics/domain_shs.langue.textBooks
%X Gernika izeneko aldizkari kulturala ezagutarazi nahi dugu lanhonetan. Zortzi urtez iraun zuen gorabeherak gorabehera. Espainiako Gerra Zibilaren ondorioz sortu zen giro latzaren ume da.Bertan idazten zuten: erbesteratuek nagusiki, iparraldeko zenbait herritarrek, zenbait euskalarik eta Francoren diktaduraren menpe bizi ziren beste batzuek. Bere lerro editoriala eta asmo politikokulturalak ez ziren kabitzen diktadura ezarri berriaren koordenaden barnean. Espainiako Gerra Zibila (1936-39) Hegoaldean, eta II. Mundu Gerra (1939-45) Iparraldean elkarren segidakoak izan ziren kronologikoki. Horrek esplika lezake zelanbait desertu kulturalaren modukoa zabaltzea, eta XXI. mendeko gizakiak garai zail haietako ezer gutxi izatea oroimenean sarri, letren esparruan behinik behin. Amnesia hori eten ohi dutenak komunzki Egan (1948an sortua) eta Euzko-Gogoa (1950an sortua) aldizkariak izaten dira. Argitaratze kontuez ari garela, plazaratutako liburuak benetan urriak izanziren, eta geroago azalduko dira. Izan ere, aldizkariko artikulugintzak idazleari ahalegin txikiagoa eskatzen dio. Kontuan izan euretako asko militarrek “etxetik bidalitakoak” zirela, Gernikako fundatzailea eta lehen zuzendaria izan zen Rafael Picaveak ironikoki zioen moduan lehen zenbakian. Publikazioaren eskribatzaileak erromes zebiltzan maiz, legezkotasun irauliaren intelektual ugari bezala: A. Ibinagabeitia errepresiotikihes eginda; Orixe kazetaria, Errepublikarekin euskal idazleentzat posiblea izan zen patxada galduta; I. López Mendizabal, A. M. Labaien, Basarri eta F. Krutwig politika-arrazoiengatik atzerriratuta.
%G Basque
%2 https://artxiker.ccsd.cnrs.fr/artxibo-01965790/document
%2 https://artxiker.ccsd.cnrs.fr/artxibo-01965790/file/Gernika%20aldizkariko%20literatura%20%281945-53%29.pdf
%L artxibo-01965790
%U https://artxiker.ccsd.cnrs.fr/artxibo-01965790
%~ CNRS
%~ UNIV-PAU
%~ AO-LINGUISTIQUE
%~ SHS
%~ IKER
%~ UNIV-BORDEAUX-MONTAIGNE
%~ OR-COLLCODE
%~ ARTXIKER
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Résumé de thèse en français: Beribilez de Jean Etxepare: voyage et idéologie
%+ University of the Basque Country [Bizkaia] (UPV/EHU)
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Ortiz de Pinedo, Aitor
%< avec comité de lecture
%@ 1273-3830
%J Lapurdum
%I Centre de recherche sur la langue et les textes basques IKER UMR 5478 CNRS
%8 2020
%D 2020
%Z Humanities and Social Sciences/LinguisticsJournal articles
%G French
%2 https://artxiker.ccsd.cnrs.fr/artxibo-02304395/document
%2 https://artxiker.ccsd.cnrs.fr/artxibo-02304395/file/These_Aitor_ORTIZ_DE_PINEDO_Resume_francais_annexe.pdf
%L artxibo-02304395
%U https://artxiker.ccsd.cnrs.fr/artxibo-02304395
%~ UNIV-PAU
%~ AO-LINGUISTIQUE
%~ CNRS
%~ SHS
%~ IKER
%~ UNIV-BORDEAUX-MONTAIGNE
%~ OR-COLLCODE
%~ ARTXIKER
%~ TESTUPPA2
%~ UPPA-OA

%0 Book
%T Humanism after the Spanish Civil War: Gernika (1945-53), a Humanities and Social Sciencesbasque magazine
%+ University of the Basque Country [Bizkaia] (UPV/EHU)
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Ortiz de Pinedo, Aitor
%8 2011-02-20
%D 2011
%Z Humanities and Social Sciences/Linguistics/domain_shs.langue.textBooks
%G Basque
%2 https://artxiker.ccsd.cnrs.fr/artxibo-01965792/document
%2 https://artxiker.ccsd.cnrs.fr/artxibo-01965792/file/Humanismoaren%20ideia%20Gerra%20Zibilaren%20ostean.pdf
%L artxibo-01965792
%U https://artxiker.ccsd.cnrs.fr/artxibo-01965792
%~ CNRS
%~ UNIV-PAU
%~ AO-LINGUISTIQUE
%~ SHS
%~ IKER
%~ UNIV-BORDEAUX-MONTAIGNE
%~ OR-COLLCODE
%~ ARTXIKER
%~ TESTUPPA2
%~ UPPA-OA

