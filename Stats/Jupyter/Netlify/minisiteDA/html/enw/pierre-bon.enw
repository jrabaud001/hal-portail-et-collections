%0 Journal Article
%T Wrapping-free numerical refocusing of scalar electromagnetic fields
%+ Laboratoire de Neurophotonique (UMR 8250)
%+ Laboratoire Photonique, Numérique et Nanosciences (LP2N)
%A Berto, Pascal
%A Guillon, Marc
%A Bon, Pierre
%< avec comité de lecture
%@ 0003-6935
%J Applied optics
%I Optical Society of America
%V 57
%N 22
%P 6582
%8 2018-07-31
%D 2018
%R 10.1364/AO.57.006582
%Z Physics [physics]/Physics [physics]/Optics [physics.optics]Journal articles
%X Numerical refocusing in any plane is one powerful feature granted by measuring both the amplitude and the phase of a coherent light beam. Here, we introduce a method based on the first Rytov approximation of scalar electromagnetic fields that (i) allows numerical propagation without requiring phase unwrapping after propagation and (ii) limits the effect of artificial phase singularities that appear upon numerical defocusing when the measurement noise is mixing with the signal. We demonstrate the feasibility of this method with both scalar electromagnetic field simulations and real acquisitions of microscopic biological samples imaged at high numerical aperture.
%G English
%2 https://hal.archives-ouvertes.fr/hal-02380410/document
%2 https://hal.archives-ouvertes.fr/hal-02380410/file/Berto%20et%20al%20-%20Wrapping%20free.pdf
%L hal-02380410
%U https://hal.archives-ouvertes.fr/hal-02380410
%~ IOGS
%~ LP2N
%~ IOGS-SACLAY
%~ UNIV-PARIS5
%~ PARISTECH
%~ USPC
%~ CNRS
%~ UNIV-PARIS-SACLAY
%~ UNIV-PARIS
%~ UP-SCIENCES

