%0 Journal Article
%T EgMYB1, an R2R3 MYB transcription factor from eucalyptus negatively regulates secondary cell wall formation in Arabidopsis and poplar
%+ Département EVA
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Fluides, automatique, systèmes thermiques (FAST)
%A Legay, S.
%A Sivadon, Pierre
%A Blervacq, A.-S.
%A Pavy, N.
%A Baghdady, A.
%A Tremblay, L.
%A Levasseur, C.
%A Ladouce, N.
%A Lapierre, C.
%A Seguin, A.
%A Hawkins, S.
%A Mackay, J.
%A Grima-Pettenati, J.
%Z cited By 68
%< avec comité de lecture
%@ 0028-646X
%J New Phytologist
%I Wiley
%V 188
%N 3
%P 774-786
%8 2010
%D 2010
%R 10.1111/j.1469-8137.2010.03432.x
%Z Life Sciences [q-bio]/Biochemistry, Molecular Biology/Genomics [q-bio.GN]Journal articles
%X Summary: The eucalyptus R2R3 transcription factor, EgMYB1 contains an active repressor motif in the regulatory domain of the predicted protein. It is preferentially expressed in differentiating xylem and is capable of repressing the transcription of two key lignin genes in vivo. In order to investigate in planta the role of this putative transcriptional repressor of the lignin biosynthetic pathway, we overexpressed the EgMYB1 gene in Arabidopsis and poplar. Expression of EgMYB1 produced similar phenotypes in both species, with stronger effects in transgenic Arabidopsis plants than in poplar. Vascular development was altered in overexpressors showing fewer lignified fibres (in phloem and interfascicular zones in poplar and Arabidopsis, respectively) and reduced secondary wall thickening. Klason lignin content was moderately but significantly reduced in both species. Decreased transcript accumulation was observed for genes involved in the biosynthesis of lignins, cellulose and xylan, the three main polymers of secondary cell walls. Transcriptomic profiles of transgenic poplars were reminiscent of those reported when lignin biosynthetic genes are disrupted. Together, these results strongly suggest that EgMYB1 is a repressor of secondary wall formation and provide new opportunities to dissect the transcriptional regulation of secondary wall biosynthesis. © The Authors (2010). Journal compilation © New Phytologist Trust (2010).
%G English
%L hal-01565046
%U https://hal.archives-ouvertes.fr/hal-01565046
%~ CNRS
%~ UNIV-PAU
%~ UNIV-PSUD
%~ IPREM
%~ INC-CNRS
%~ UPMC
%~ SORBONNE-UNIVERSITE
%~ SU-TI
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Functional assessment of the yeast Rvs161 and Rvs167 protein domains
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Institut de biochimie et génétique cellulaires (IBGC)
%A Sivadon, Pierre
%A Crouzet, M.
%A Aigle, M.
%Z cited By 27
%< avec comité de lecture
%@ 0014-5793
%J FEBS Letters
%I Wiley
%V 417
%N 1
%P 21-27
%8 1997
%D 1997
%R 10.1016/S0014-5793(97)01248-9
%Z Life Sciences [q-bio]/Biochemistry, Molecular Biology/Genomics [q-bio.GN]Journal articles
%X Mutations in RVS161 and RVS167 yeast genes induce identical phenotypes associated to actin cytoskeleton disorders. The whole Rvs161 protein is similar to the amino-terminal part of Rvs167p, thus defining a RVS domain. In addition to this domain, Rvs167D contains a central glycine-proline-alanine rich domain and a SH3 domain. To assess the function of these different domains we have expressed recombinant Rvs proteins in rvs mutant strains. Phenotype analysis has shown that the RVS and SH3 domains are necessary for phenotypical complementation, whereas the GPA domain is not. Moreover, we have demonstrated that the RVS domains from Rvs161p and Rvs167p have distinct roles, and that the SH3 domain needs the specific RVS domain of Rvs167p to function. These results suggest that Rvs161p and Rvs167p play distinct roles, while acting together in a common function.
%G English
%L hal-01565054
%U https://hal.archives-ouvertes.fr/hal-01565054
%~ CNRS
%~ UNIV-PAU
%~ IPREM
%~ INC-CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Book Section
%T Assimilation of Hydrocarbons and Lipids by Means of Biofilm Formation
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Sivadon, Pierre
%A Grimaud, Regis.
%B Cellular Ecophysiology of Microbe
%8 2017
%D 2017
%Z Life Sciences [q-bio]/Ecology, environment
%Z Life Sciences [q-bio]/Biotechnology
%Z Life Sciences [q-bio]/Biochemistry, Molecular Biology/Genomics [q-bio.GN]
%Z Life Sciences [q-bio]/Biochemistry, Molecular Biology/Molecular biology
%Z Life Sciences [q-bio]/Microbiology and Parasitology/BacteriologyBook sections
%G English
%2 https://hal.archives-ouvertes.fr/hal-01676300/document
%2 https://hal.archives-ouvertes.fr/hal-01676300/file/Sivadon%20and%20Grimaud%202017_depot%20hal.pdf
%L hal-01676300
%U https://hal.archives-ouvertes.fr/hal-01676300
%~ CNRS
%~ UNIV-PAU
%~ GIP-BE
%~ IPREM
%~ INC-CNRS
%~ IPREM-CME
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T The marine bacterium Marinobacter hydrocarbonoclasticus SP17 degrades a wide range of lipids and hydrocarbons through the formation of oleolytic biofilms with distinct gene expression profiles.
%+ Institut Pluridisciplinaire de Recherche sur l'Environnement et les Matériaux (IPREM)
%A Mounier, Julie
%A Camus, Arantxa
%A Mitteau, Isabelle
%A Vaysse, Pierre-Joseph
%A Goulas, Philippe
%A Grimaud, Régis
%A Sivadon, Pierre
%Z French Ministere de l'Enseignement Superieur et de la Recherche
%< avec comité de lecture
%@ 0168-6496
%J FEMS Microbiology Ecology
%I Wiley-Blackwell
%V 90
%N 3
%P 816-831
%8 2014
%D 2014
%R 10.1111/1574-6941.12439
%K oceans
%K Transcriptome
%K degradation of hydrophobic organic carbon
%K Carbon cycle
%Z Life Sciences [q-bio]/Microbiology and Parasitology/Bacteriology
%Z Life Sciences [q-bio]/Biochemistry, Molecular Biology/Genomics [q-bio.GN]
%Z Life Sciences [q-bio]/Biochemistry, Molecular Biology/Molecular biology
%Z Life Sciences [q-bio]/Ecology, environment/Ecosystems
%Z Life Sciences [q-bio]/BiotechnologyJournal articles
%X Hydrophobic organic compounds (mainly lipids and hydrocarbons) represent a significant part of the organic matter in the marine waters and their degradation has thus an important impact in the carbon fluxes within the oceans. However, because they are nearly insoluble in the water phase, their degradation by microorganisms occurs at the interface with water therefore requiring specific adaptations like biofilm formation. We show that Marinobacter hydrocarbonoclasticus SP17 develops biofilms, referred as oleolytic biofilms, on a larger variety of hydrophobic substrates than suspected before, including hydrocarbons, fatty alcohols, fatty acids, triglycerides and wax esters. A microarray analysis confirmed that biofilm growth on n-hexadecane or triolein involved distinct genetic responses together with a core of common genes that might concern general mechanisms of biofilm formation. Biofilm growth on triolein modulated the expression of hundreds of genes in comparison to n36hexadecane. Processes related to primary metabolism and genetic information processing were down-regulated. Most of the genes over-expressed on triolein had unknown function. Surprisingly, their genome localization is restricted to a few regions identified as putative genomic islands or mobile elements. These results are discussed with regard to the adaptive responses triggered by M. hydrocarbonoclasticus SP17 to occupy a specific niche in marine ecosystems.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01294113/document
%2 https://hal.archives-ouvertes.fr/hal-01294113/file/Manuscript_Mounier%20et%20al_FEMS%20Microbiol%20Ecol.pdf
%L hal-01294113
%U https://hal.archives-ouvertes.fr/hal-01294113
%~ CNRS
%~ UNIV-PAU
%~ GIP-BE
%~ IPREM
%~ INC-CNRS
%~ ANR
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Identification of novel transcription factors regulating secondary cell wall formation in Arabidopsis
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Cassan-Wang, H.
%A Goué, N.
%A Saidi, M.N.
%A Legay, S.
%A Sivadon, Pierre
%A Goffner, D.
%A Grima-Pettenati, J.
%Z cited By 26
%< avec comité de lecture
%@ 1664-462X
%J Frontiers in Plant Science
%I Frontiers
%V 4
%N JUN
%8 2013
%D 2013
%R 10.3389/fpls.2013.00189
%Z Life Sciences [q-bio]/Biochemistry, Molecular Biology/Genomics [q-bio.GN]Journal articles
%X The presence of lignin in secondary cell walls (SCW) is a major factor preventing hydrolytic enzymes from gaining access to cellulose, thereby limiting the saccharification potential of plant biomass. To understand how lignification is regulated is a prerequisite for selecting plant biomass better adapted to bioethanol production. Because transcriptional regulation is a major mechanism controlling the expression of genes involved in lignin biosynthesis, our aim was to identify novel transcription factors (TFs) dictating lignin profiles in the model plant Arabidopsis. To this end, we have developed a post-genomic approach by combining four independent in-house SCW-related transcriptome datasets obtained from (1) the fiber cell wall-deficient wat1 Arabidopsis mutant, (2) Arabidopsis lines over-expressing either the master regulatory activator EgMYB2 or (3) the repressor EgMYB1 and finally (4) Arabidopsis orthologs of Eucalyptus xylem-expressed genes. This allowed us to identify 502 up- or down-regulated TFs. We preferentially selected those present in more than one dataset and further analyzed their in silico expression patterns as an additional selection criteria. This selection process led to 80 candidates. Notably, 16 of them were already proven to regulate SCW formation, thereby validating the overall strategy. Then, we phenotyped 43 corresponding mutant lines focusing on histological observations of xylem and interfascicular fibers. This phenotypic screen revealed six mutant lines exhibiting altered lignification patterns. Two of them [Bel-like HomeoBox6 (blh6) and a zinc finger TF] presented hypolignified SCW. Three others (myb52, myb-like TF, hb5) showed hyperlignified SCW whereas the last one (hb15) showed ectopic lignification. In addition, our meta-analyses highlighted a reservoir of new potential regulators adding to the gene network regulating SCW but also opening new avenues to ultimately improve SCW composition for biofuel production. © 2013 Cassan-Wang, Goué, Saidi, Legay, Sivadon, Goffner and Grima-Pettenati.
%G English
%L hal-01565043
%U https://hal.archives-ouvertes.fr/hal-01565043
%~ CNRS
%~ UNIV-PAU
%~ IPREM
%~ INC-CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Transcript profiling of a xylem vs phloem cDNA subtractive library identifies new genes expressed during xylogenesis in Eucalyptus
%+ Laboratoire de Recherche en Sciences Végétales (LRSV)
%+ Surfaces Cellulaires et Signalisation chez les Végétaux (SCSV)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Génétique Diversité et Ecophysiologie des Céréales (GDEC)
%A Foucart, C.
%A Ladouce, N.
%A San-Clemente, H.
%A Grima-Pettenati, J.
%A Sivadon, Pierre
%A Paux, Etienne
%Z cited By 45
%< avec comité de lecture
%@ 0028-646X
%J New Phytologist
%I Wiley
%V 170
%N 4
%P 739-752
%8 2006
%D 2006
%R 10.1111/j.1469-8137.2006.01705.x
%M 16684235
%K suppression subtractive hybridization (SSH) library
%K phloem
%K Eucalyptus
%K cambium
%K xylem
%K cDNA arrays
%Z Life Sciences [q-bio]/Biochemistry, Molecular Biology/Genomics [q-bio.GN]Journal articles
%X • Eucalyptus is one of the world's main sources of biomass. The genus includes species representing the principle hardwood trees used for pulp and paper. Here, we aimed to identify genes specifically expressed in differentiating secondary xylem compared with phloem. • We constructed a xylem vs phloem subtractive library (Xp) that generated 263 unique sequences. By transcript profiling of xylem, phloem, vascular cambium and leaves using macroarrays, we classified the 263 unigenes into distinct tissue-specific groups. Reverse transcription-polymerase chain reaction (RT-PCR) confirmed the differential expression of representative expressed sequence tags (ESTs). • A total of 87 unigenes were preferentially expressed in xylem. They were involved in functional categories known to play roles in xylogenesis, such as hormone signaling and metabolism, secondary cell wall thickening and proteolysis. Some of these genes, including unknown genes, may be considered xylem-specific and they are likely to control important functions in xylogenesis. • These data shed light on the cellular functions of xylem cells and, importantly, provide us with a portfolio of Eucalyptus xylem genes that may be major players in the control of wood formation and quality. © New Phytologist (2006).
%G English
%L hal-01565049
%U https://hal.archives-ouvertes.fr/hal-01565049
%~ CNRS
%~ UNIV-PAU
%~ IPREM
%~ INC-CNRS
%~ UNIV-TLSE3
%~ INRAE
%~ INRA
%~ UNIV-BPCLERMONT
%~ PRES_CLERMONT
%~ GDEC
%~ TESTUPPA2
%~ UPPA-OA
%~ AGREENIUM

%0 Journal Article
%T AupA and AupB Are Outer and Inner Membrane Proteins Involved in Alkane Uptake in Marinobacter hydrocarbonoclasticus SP17
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ MICrobiologie de l'ALImentation au Service de la Santé  (MICALIS)
%A Mounier, Julie
%A Hakil, Florence
%A BRANCHU, Priscilla
%A Naïtali, Muriel
%A Goulas, Philippe
%A Sivadon, Pierre
%A Grimaud, Regis.
%< avec comité de lecture
%@ 2150-7511
%J mBio
%I American Society for Microbiology
%V 9
%N 3
%P 493
%8 2018
%D 2018
%R 10.1128/mBio.00520-18
%M 29871914
%K biofilms
%K biodegradation
%K alkanes
%K marine microbiology
%Z Life Sciences [q-bio]/Microbiology and Parasitology/BacteriologyJournal articles
%X This study describes the functional characterization of two proteins, AupA and AupB, which are required for growth on alkanes in the marine hydrocarbonoclastic bacterium Marinobacter hydrocarbonoclasticus. The aupA and aupB genes form an operon whose expression was increased upon adhesion to and biofilm formation on n-hexadecane. AupA and AupB are outer and inner membrane proteins, respectively, which are able to interact physically. Mutations in aupA or/and aupB reduced growth on solid paraffin and liquid n-hexadecane, while growth on nonalkane substrates was not affected. In contrast, growth of aup mutants on n-hexadecane solubilized in Brij 58 micelles was completely abolished. Mutant cells had also lost the ability to bind to n-hexadecane solubilized in Brij 58 micelles. These results support the involvement of AupA and AupB in the uptake of micelle-solubilized alkanes and provide the first evidence for a cellular process involved in the micellar uptake pathway. The phylogenetic distribution of the aupAB operon revealed that it is widespread in marine hydrocarbonoclastic bacteria of the orders Oceanospirillales and Alteromonadales and that it is present in high copy number (up to six) in some Alcanivorax strains. These features suggest that Aup proteins probably confer a selective advantage in alkane-contaminated seawater. IMPORTANCE Bacteria are the main actors of the biological removal of hydrocarbons in seawater, and so, it is important to understand how they degrade hydrocarbons and thereby mitigate marine environmental damage. Despite a considerable amount of literature about the dynamic of microbial communities subjected to hydrocarbon exposure and the isolation of strains that degrade hydrocarbons, most of the genetic determinants and molecular mechanisms of bacterial hydrocarbon uptake remain unknown. This study identifies two genes, aupA and aupB, in the hydrocarbonoclastic bacterium Marinobacter hydrocarbonoclasticus that are present frequently in multiple copies in most of the marine hydrocarbon-degrading bacteria for which the genomic sequence is available. AupA and AupB are two novel membrane proteins interacting together that are involved in the uptake of alkanes dissolved in surfactant micelles. The function and the phylogenetic distribution of aupA and aupB suggest that they might be one attribute of the remarkable adaptation of marine hydrocarbonoclastic bacteria that allow them to take advantage of hydrocarbons.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01809338/document
%2 https://hal.archives-ouvertes.fr/hal-01809338/file/mBio-2018-Mounier-.pdf
%L hal-01809338
%U https://hal.archives-ouvertes.fr/hal-01809338
%~ AGROPARISTECH
%~ AGROPARISTECH-ORG
%~ AGROPARISTECH-SPAB
%~ CNRS
%~ UNIV-PAU
%~ INRA
%~ AGROPARISTECH-SACLAY
%~ UNIV-PARIS-SACLAY
%~ AGREENIUM
%~ INRA-SACLAY
%~ INC-CNRS
%~ IPREM
%~ INRAE
%~ IPREM-CME
%~ ANR
%~ TESTUPPA2
%~ UPPA-OA
%~ GS-BIOSPHERA

%0 Journal Article
%T Environmental microbiology as a mosaic of explored ecosystems and issues
%+ Institut de Biologie Intégrative de la Cellule (I2BC)
%+ Institut méditerranéen d'océanologie (MIO)
%+ Université de Toulon (UTLN)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Institut de Chimie de Clermont-Ferrand (ICCF)
%+ Génétique moléculaire, génomique, microbiologie (GMGM)
%+ Ecologie des systèmes marins côtiers (Ecosym)
%+ Agroécologie [Dijon]
%+ Ecosystèmes, biodiversité, évolution [Rennes] (ECOBIO)
%+ Laboratoire d'Ecologie Microbienne - UMR 5557 (LEM)
%+ Laboratoire de Géologie de Lyon - Terre, Planètes, Environnement [Lyon] (LGL-TPE)
%+ Laboratoire d'Ecogéochimie des environnements benthiques (LECOB)
%+ Laboratoire d'Ecologie Microbienne - UMR 5557 (LEM)
%+ AgroParisTech
%+ Institut d'écologie et des sciences de l'environnement de Paris (IEES (UMR_7618 / UMR_D_242 / UMR_A_1392 / UM_113) )
%+ LIttoral ENvironnement et Sociétés - UMRi 7266 (LIENSs)
%+ Laboratoire des Sciences de l'Environnement Marin (LEMAR) (LEMAR)
%+ Biogéochimie-Traceurs-Paléoclimat (BTP)
%A Faure, Denis
%A Bonin, Patricia
%A Duran, Robert
%A Amato, Pierre
%A Arsene-Ploetze, Florence
%A Auguet, Jean-Christophe
%A Legrand, Bernard
%A Bertin, Philippe N.
%A Bettarel, Yvan
%A BIGOT-CLIVOT, Aurélie
%A Blot, Nicolas
%A BLOUIN, Manuel
%A Bormans, Myriam
%A Bouvy, Marc
%A Bruneel, O
%A Cébron, Aurélie
%A Christaki, Urania
%A Couée, Ivan
%A Cravo-Laureau, Cristiana
%A Danger, Michael
%A De Lorgeril, Julien
%A Desdevises, Yves
%A Dessaux, Yves
%A Destoumieux-Garzón, D
%A Duprat, Élodie
%A Erauso, Gaël
%A Haichar, Feth El Zahar
%A Fouilland, Éric
%A Francez, Andre-Jean
%A Fromin, Nathalie
%A Geffard, Alain
%A Ghiglione, Jean-François
%A Huguet, Arnaud
%A Grossi, Vincent
%A Guizien, Katell
%A Jardillier, Ludwig
%A Jouquet, Pascal
%A Joux, Fabien
%A Kaisermann, Aurore
%A Kaltz, Oliver
%A Lata, Jean-Christophe
%A Lecerf, Antoine
%A Leyval, Corinne
%A Luis, Patricia
%A Masseret, Estelle
%A NIBOYET, Audrey
%A Normand, Philippe
%A Plewniak, Frédéric
%A Poly, Franck
%A Prado, Soizic
%A Quaiser, Achim
%A Ratet, Pascal
%A Richaume, Agnès
%A Rolland, Jean-Luc
%A Rols, Jean-Luc
%A Rontani, JF
%A Rossi, Francesca
%A Sablé, Sophie
%A Sivadon, P
%A Soudant, Philippe
%A Tamburini, Christian
%A Tribollet, Aline
%A Valiente Moro, Claire
%A Van Wambeke, France
%A Vandenkoornhuyse, Philippe
%A Vuilleumier, Stéphane
%A Vandenkoornhuyse, Philippe
%Z CNRS Mission pour l'Interdisciplinarite; Incentive Action Ecosphere Continentale et Cotiere (EC2CO)
%< avec comité de lecture
%@ 0944-1344
%J Environmental Science and Pollution Research
%I Springer Verlag
%V 22
%N 18
%P 13577–13598
%8 2015-09
%D 2015
%R 10.1007/s11356-015-5164-5
%M 26310700
%K Critical zone
%K Environmental microbiology
%K Microbial ecology
%K ACL
%K Microbial ecosystems
%Z Environmental Sciences/Biodiversity and Ecology
%Z Environmental Sciences/Environmental and SocietyJournal articles
%X Microbes are phylogenetically (Archaea, Bacteria, Eukarya, and viruses) and functionally diverse. They colonize highly varied environments and rapidly respond to and evolve as a response to local and global environmental changes, including those induced by pollutants resulting from human activities. This review exemplifies the Microbial Ecology EC2CO consortium’s efforts to explore the biology, ecology, diversity, and roles of microbes in aquatic and continental ecosystems.
%G English
%Z Ecosphère Continentale et Côtière (EC2CO)
%2 https://hal.archives-ouvertes.fr/hal-01247096/document
%2 https://hal.archives-ouvertes.fr/hal-01247096/file/Faure-EC2CO-2015-EnvironSciPollutRes-Environmental-MANUSCRIT.pdf
%L hal-01247096
%U https://hal.archives-ouvertes.fr/hal-01247096
%~ UNIV-PAU
%~ CNRS
%~ THESES_IUEM
%~ MNHN
%~ UNIV-ROCHELLE
%~ IFREMER
%~ OSUR
%~ IPREM
%~ SDE
%~ ICC
%~ GIP-BE
%~ AGROPOLIS
%~ PRES_CLERMONT
%~ INC-CNRS
%~ SIGMA-CLERMONT
%~ AGROPARISTECH
%~ UDL
%~ ECOBIO
%~ UR1-SDV
%~ UR1-HAL
%~ UR1-UFR-SVE
%~ UNIV-RENNES1
%~ ECOBIO-RBPE
%~ ECOBIO-RITME
%~ ECOBIO-PHENOME
%~ ECOBIO-DYNAMO
%~ UNIV-LYON
%~ INRA
%~ UPEC-UPEM
%~ IEES
%~ UPMC
%~ UPMC_POLE_3
%~ AGROPARISTECH-ORG
%~ AGROPARISTECH-SIAFEE
%~ LOCEAN
%~ BTP
%~ TEST-UNIV-RENNES
%~ TEST-UR-CSS
%~ UNIV-RENNES
%~ UNIV-AMU
%~ UNIV-TLN
%~ MIO
%~ OSU-INSTITUT-PYTHEAS
%~ INSU
%~ SORBONNE-UNIVERSITE
%~ UNIV-STRASBG
%~ SU-SCIENCES
%~ UNIV-LYON1
%~ ENS-LYON
%~ UPEC
%~ UNIV-MONTPELLIER
%~ IHPE
%~ IPREM-CME
%~ UNIV-BREST
%~ IUEM
%~ LEMAR
%~ LGL-TPE
%~ BIOENVIS
%~ INRAE
%~ SITE-ALSACE
%~ UP-SCIENCES
%~ CEA
%~ DSV
%~ CEA-UPSAY
%~ CEA-UPSAY-SACLAY
%~ JOLIOT
%~ I2BC
%~ CEA-DRF
%~ SU-INF-2018
%~ UNIV-PARIS-SACLAY
%~ MIO-CEM
%~ LECOB
%~ UMS-2348
%~ MIO-MEB
%~ MIO-CYBELE
%~ ECOMIC
%~ UR1-ENV
%~ AGROSUP-AE
%~ AGROSUP-DIJON
%~ AGREENIUM
%~ FRANTIQ
%~ ACL-SVSAE
%~ IRD
%~ SU-TI
%~ TESTUPPA2
%~ UPPA-OA
%~ UNIV-BOURGOGNE
%~ CNES
%~ X-DEP
%~ PSL
%~ X
%~ FR-636
%~ UNIV-PARIS7
%~ ENS-PSL
%~ UVSQ
%~ UVSQ-SACLAY
%~ X-SACLAY
%~ USPC
%~ UNIV-PARIS
%~ GS-ENGINEERING
%~ GS-BIOSPHERA

%0 Journal Article
%T A new genomic resource dedicated to wood formation in Eucalyptus
%+ Visual servoing in robotics, computer vision, and augmented reality (Lagadic)
%+ Génétique Diversité et Ecophysiologie des Céréales (GDEC)
%+ Genoscope - Centre national de séquençage [Evry] (GENOSCOPE)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ inconnu
%A Rengel, D.
%A Clemente, H.S.
%A Servant, F.
%A Ladouce, N.
%A Paux, E.
%A Wincker, P.
%A Couloux, A.
%A Sivadon, Pierre
%A Grima-Pettenati, J.
%Z RAIZ, ENCE, EU
%Z cited By 45
%< avec comité de lecture
%@ 1471-2229
%J BMC Plant Biology
%I BioMed Central
%V 9
%N 36
%P n.p.
%8 2009
%D 2009
%R 10.1186/1471-2229-9-36
%M 19327132
%Z Life Sciences [q-bio]/Biochemistry, Molecular Biology/Genomics [q-bio.GN]Journal articles
%X Background. Renowned for their fast growth, valuable wood properties and wide adaptability, Eucalyptus species are amongst the most planted hardwoods in the world, yet they are still at the early stages of domestication because conventional breeding is slow and costly. Thus, there is huge potential for marker-assisted breeding programs to improve traits such as wood properties. To this end, the sequencing, analysis and annotation of a large collection of expressed sequences tags (ESTs) from genes involved in wood formation in Eucalyptus would provide a valuable resource. Results. We report here the normalization and sequencing of a cDNA library from developing Eucalyptus secondary xylem, as well as the construction and sequencing of two subtractive libraries (juvenile versus mature wood and vice versa). A total of 9,222 high quality sequences were collected from about 10,000 cDNA clones. The EST assembly generated a set of 3,857 wood-related unigenes including 2,461 contigs (Cg) and 1,396 singletons (Sg) that we named 'EUCAWOOD'. About 65% of the EUCAWOOD sequences produced matches with poplar, grapevine, Arabidopsis and rice protein sequence databases. BlastX searches of the Uniref100 protein database allowed us to allocate gene ontology (GO) and protein family terms to the EUCAWOOD unigenes. This annotation of the EUCAWOOD set revealed key functional categories involved in xylogenesis. For instance, 422 sequences matched various gene families involved in biosynthesis and assembly of primary and secondary cell walls. Interestingly, 141 sequences were annotated as transcription factors, some of them being orthologs of regulators known to be involved in xylogenesis. The EUCAWOOD dataset was also mined for genomic simple sequence repeat markers, yielding a total of 639 putative microsatellites. Finally, a publicly accessible database was created, supporting multiple queries on the EUCAWOOD dataset. Conclusion. In this work, we have identified a large set of wood-related Eucalyptus unigenes called EUCAWOOD, thus creating a valuable resource for functional genomics studies of wood formation and molecular breeding in this economically important genus. This set of publicly available annotated sequences will be instrumental for candidate gene approaches, custom array development and marker-assisted selection programs aimed at improving and modulating wood properties.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01565048/document
%2 https://hal.archives-ouvertes.fr/hal-01565048/file/2015_Rengel_BMC_Plant%20Biology%20_1.htm
%L hal-01565048
%U https://hal.archives-ouvertes.fr/hal-01565048
%~ INRAE
%~ DSV-IG
%~ IRISA-D5
%~ DSV
%~ INRIA2
%~ INC-CNRS
%~ UR1-HAL
%~ UR1-MATH-STIC
%~ UR1-UFR-ISTIC
%~ JACOB
%~ CEA-DRF
%~ TEST-UNIV-RENNES
%~ TEST-UR-CSS
%~ UNIV-RENNES
%~ INRIA-RENGRE
%~ GENOSCOPE
%~ INSTITUTS-TELECOM
%~ IPREM
%~ GDEC
%~ CEA
%~ UNIV-RENNES1
%~ PRES_CLERMONT
%~ CNRS
%~ INRIA
%~ UNIV-PAU
%~ UNIV-BPCLERMONT
%~ UNIV-UBS
%~ INRIASO
%~ IRISA_SET
%~ INRA
%~ INRIA-SOPHIA
%~ INRIA-RENNES
%~ INRIA_TEST
%~ IRISA
%~ ACL-SVSAE
%~ AGREENIUM
%~ INSTITUT-TELECOM
%~ INSA-RENNES
%~ UR1-MATH-NUM
%~ TESTUPPA2
%~ UPPA-OA

%0 Conference Paper
%T CCR gene in Eucalyptus: a model of functional variability in forest trees
%T Un modèle de variabilité fonctionnelle chez les arbres forestiers : le gène CCR d'eucalyptus
%+ Biodiversité, Gènes & Communautés (BioGeCo)
%+ Centre de Coopération Internationale en Recherche Agronomique pour le Développement (Cirad)
%+ Mathématiques et Informatique Appliquées (MIA-Paris)
%+ Laboratoire de Recherche en Sciences Végétales (LRSV)
%+ Independent
%+ Génie et Microbiologie des Procédés Alimentaires (GMPA)
%+ Institut Jean-Pierre Bourgin (IJPB)
%A Gion, Jean-Marc, J.-M.
%A Mortier, Fréderic
%A Mandrou, Eric
%A Hein Gherardi, Paulo Ricardo
%A Costecalde, Tristan
%A Chaix, Gilles
%A Etienne, Marie Pierre
%A Sivadon, Pierre
%A Grima-Pettenati, Jacqueline
%A Villar, Emilie
%A Saya, Aubin
%A Pollet, Brigitte, B.
%A Lapierre, Catherine, C.
%A Vigneron, Philippe
%F Invité
%< avec comité de lecture
%B 7. Colloque National: Ressources Génétiques
%C Strasbourg, France
%P 17 p.
%8 2008-10-13
%D 2008
%K CCR gene, eucalyptus, variability, lignins, association study
%K gène CCR, eucalyptus, variabilité, lignines, test d’association
%Z Life Sciences [q-bio]
%Z Mathematics [math]
%Z Computer Science [cs]
%Z Life Sciences [q-bio]/Food engineering
%Z Engineering Sciences [physics]/Chemical and Process EngineeringConference papers
%X Nucleotidic polymorphism of Cinnamoyl CoA Reductase (CCR) gene and its relation with lignin content is studied within a breeding population of Eucalyptus urophylla S.T. Blake (“Timor Mountain Gum”). The nearly full sequence (94%) are obtained for 15 parental trees. This gene (3220 bp) is highly polymorphic showing 131 single nucleotide polymorphism (SNP), 17 insertion-deletions (INDEL), 1 polyA sequence and a microsatellite site. Exons fragments encompass 10 non-synonymous SNPs, half of them within exon 5 (194 bp). Fifteen different haplotypes are reconstructed based on the polymorphism of exon 4 and intron 4. CCR promoting sequence (694 bp) including all the known regulatory sequences is described for the two alleles of one of the genitor trees displaying QTL and CCR gene colocalization in its genetic map. Five SNPs are present. Functional variability of the promoting sequence will be studied in planta through genetic modification of Arabidopsis thaliana. Lignin content was assessed within a sample of 35 full sib families (348 individuals) generated with the 15 parental trees, showing a high genetic additive control for this trait (h²=0.76). A new algorithm based on Reversible-jump MCMC was developed in order to implement association studies. Half of the progeny trees (208) were genotyped using the microsatellite fragment. The results show that a significant part of the observed genetic variance of lignin content is due to the nucleotide polymorphism of the studied gene. Those preliminary results look promising in order to develop early gene assisted selection for eucalyptus clones used as raw material in charcoal and paper production.
%X La variabilité nucléotidique du gène codant la Cinnamoyl CoA Reductase (CCR) et ses effets sur le taux de lignine est étudiée au sein d’une population d’Eucalyptus urophylla S.T. Blake. La presque totalité de la séquence (94%, 3220 paires de bases) est décrite pour 15 individus. Le gène est hautement polymorphe et présente 131 mutations ponctuelles (SNP) ainsi que divers autres types de mutations. Les fragments exoniques présentent 10 SNP non synonymes dont 5 dans l’exon 5. La séquence promotrice (694 pb) est décrite pour les deux allèles d’un des géniteurs. Elle regroupe 5 SNPs. La variabilité fonctionnelle de ce promoteur sera étudiée grâce à son expression dans Arabidopsis thaliana. L’analyse de la teneur en lignine de 348 arbres appartenant à 35 familles de pleins frères obtenues avec ces 15 géniteurs montre que ce caractère présente un fort contrôle génétique additif (h²=0.76). Un nouvel algorithme type MCMC a été développé pour procéder aux études d’association sur 208 descendants génotypés grâce à un marqueur microsatellite présent dans le gène CCR. Les résultats montrent qu’une part importante de la variance du taux de lignine est due au polymorphisme du gène CCR. Ces résultats laissent envisager le développement d’une sélection précoce assistée par marqueurs.
%G French
%2 https://hal.inrae.fr/hal-02821259/document
%2 https://hal.inrae.fr/hal-02821259/file/48800_20120315034153910_1.pdf
%L hal-02821259
%U https://hal.inrae.fr/hal-02821259
%~ INRAE
%~ UNIV-TLSE3
%~ INRA
%~ INSMI
%~ CIRAD
%~ AGREENIUM
%~ AGROPARISTECH
%~ CNRS
%~ MIA-PARIS
%~ GS-COMPUTER-SCIENCE
%~ UNIV-PARIS-SACLAY

