%0 Conference Proceedings
%T Toutes mes publications dans ProdInra en quelques clics
%T Déposer mes publications dans ProdInra à chaque production de document, c'est simple comme un mail
%+ Hydrobiologie (HYDROBIOLOGIE)
%+ Physiologie, Environnement et Génétique pour l'Animal et les Systèmes d'Elevage [Rennes] (PEGASE)
%+ Laboratoire de Physiologie et Génomique des Poissons (LPGP)
%+ Physiologie de la reproduction et des comportements [Nouzilly] (PRC)
%+ Unité de Recherches Avicoles (URA)
%A Avril, Pascale
%A Bernard, Emilie
%A Corvaisier, Maryse
%A Girard, Agnès
%A Touze, Marie-Laure
%A Wacrenier, Nathaële
%< avec comité de lecture
%B 5. Journées d'Animation Scientifique du département Phase (JAS Phase 2013)
%C Paris, France
%P np
%8 2013-10-03
%D 2013
%Z Life Sciences [q-bio]Conference papers
%X Pourquoi déposer mes publications ? et comment faire un dépôt rapide dans ProdInra ?
%G French
%2 https://hal.archives-ouvertes.fr/hal-01210468/document
%2 https://hal.archives-ouvertes.fr/hal-01210468/file/Gira2013Jas_1.pdf
%L hal-01210468
%U https://hal.archives-ouvertes.fr/hal-01210468
%~ INRAE
%~ UNIV-TOURS
%~ CNRS
%~ UR1-SDV
%~ PEGASE
%~ BIOSIT
%~ IFR140
%~ UNAM
%~ INRA
%~ LPGP
%~ AGREENIUM
%~ BOA

%0 Journal Article
%T Analyser la production scientifique d'un département de recherche : construction d'une ressource termino-ontologique par des documentalistes
%+ Hydrobiologie (HYDROBIOLOGIE)
%+ Physiologie, Environnement et Génétique pour l'Animal et les Systèmes d'Elevage [Rennes] (PEGASE)
%+ Laboratoire de Physiologie et Génomique des Poissons (LPGP)
%+ Mathématiques et Informatique Appliquées du Génome à l'Environnement [Jouy-En-Josas] (MaIAGE)
%+ Physiologie de la reproduction et des comportements [Nouzilly] (PRC)
%+ Elevage Alternatif et Santé des Monogastriques (UE EASM)
%A Avril, Pascale
%A Bernard, Emilie
%A Corvaisier, Maryse
%A Girard, Agnès
%A Golik, Wiktoria
%A Nédellec, Claire
%A Touze, Marie-Laure
%A Wacrenier, Nathaële
%< sans comité de lecture
%@ 0762-7339
%J Cahier des Techniques de l'INRA
%I INRA
%N 89
%P 1-12
%8 2016
%D 2016
%K pratique documentaire
%K documentation
%K domaine de connaissance
%K termino-ontologie
%K analyse stratégique
%K analyse des publications
%K publication scientifique
%K corpus bibliographique
%K ressource sémantique
%K ressource termino-ontologique
%K ontologie
%Z Humanities and Social Sciences/Library and information sciences
%Z Computer Science [cs]/Information Retrieval [cs.IR]
%Z Life Sciences [q-bio]/Agricultural sciences/Animal production studiesJournal articles
%X Le département Physiologie animale et systèmes d’élevage (Phase) de l'Inra a souhaité développer un outil d’analyse stratégique pour suivre l’évolution de ses publications scientifiques. Pour répondre à cette demande, le choix d'une approche basée sur l'indexation automatique des publications par une ressource termino-ontologique (RTO) spécifique et adaptée au domaine du département a été fait. Cette ressource est nommée Triphase pour «Terminologie pour la recherche d’information du département Phase». Cet article décrit l’implication des documentalistes du département dans le projet de construction de Triphase et les compétences qu’elles ont développées. Il précise l’organisation du travail, les outils utilisés, les pratiques et le savoir-faire documentaires nécessaires à la construction d’une ressource termino-ontologique, ainsi qu'à son utilisation.
%G French
%2 https://hal.archives-ouvertes.fr/hal-01607558/document
%2 https://hal.archives-ouvertes.fr/hal-01607558/file/Avril_2016_Cahier_Techniques_Inra_1.pdf
%L hal-01607558
%U https://hal.archives-ouvertes.fr/hal-01607558
%~ INRAE
%~ CNRS
%~ UNIV-TOURS
%~ UR1-SDV
%~ SHS
%~ INRA-SACLAY
%~ BIOSIT
%~ PEGASE
%~ INRA
%~ UNAM
%~ IFR140
%~ LPGP
%~ AGREENIUM
%~ UNIV-PARIS-SACLAY
%~ ARINRAE-CDT
%~ ARINRAE
%~ MAIAGE
%~ GS-COMPUTER-SCIENCE

