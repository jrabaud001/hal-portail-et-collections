%0 Journal Article
%T A Geotechnical Perspective of Raw Earth Building
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%A Gallipoli, Domenico
%A Bruno, Agostino Walter
%A Perlot-Bascoules, Céline
%A Mendes, Joao
%< avec comité de lecture
%@ 1861-1125
%J Acta Geotechnica
%I Springer Verlag
%V 12
%N 3
%P 463-478
%8 2017
%D 2017
%R 10.1007/s11440-016-0521-1
%Z Physics [physics]Journal articles
%X Much research has been devoted over the past 30~years to the development of construction materials that can lower the environmental and economic costs of buildings over their entire life by reducing embodied energy, minimizing air conditioning needs and cutting down demolition waste. In this respect, raw earth is an attractive material because it is natural and largely available. In its simplest form, this material consists of a compacted mixture of soil and water which is put in place with the least possible transformation. Raw earth construction has been practised in ancient times but has only recently been rediscovered thanks to modern technology, which has improved fabrication efficiency. If properly manufactured, raw earth exhibits comparable mechanical characteristics and better hygro-thermal properties than concrete or fired bricks. After a brief historical overview, we discuss the advantages of raw earth construction in terms of environmental impact, energy consumption and indoor air quality together with the main obstacles to its wider dissemination. We also review the hydro-thermo-mechanical behaviour of raw earth in the context of the recent geotechnical literature, by examining the dependency of key parameters such as strength, stiffness and moisture retention on: (a) material variables (e.g. particle size and mineralogy), (b) manufacturing variables (e.g. density and stabilization) and (c) environmental variables (e.g. pore suction, ambient humidity and temperature). \textcopyright 2017, Springer-Verlag Berlin Heidelberg.
%G English
%L hal-02153325
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02153325
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA

%0 Thesis
%T Impact of cementitious materials decalcification on transfer properties: application to radioactive waste deep repository
%+ Laboratoire Matériaux et Durabilité des constructions (LMDC)
%A Perlot-Bascoules, Céline
%Z Thèse préparée en cotutelle entre l'UPS de Toulouse au LMDC et l'Université de Sherbrooke au Québec au LMRGA pour l'obtention du doctorat et du Ph.D.
%Z LMRGA
%I Université Paul Sabatier - Toulouse III
%Y Pr Myriam Carcassès et PR Gérard Ballivy(carcasse@insa-toulouse.fr)
%Z Mr Gérard BALLIVY Co-directeur
%Z Mr Xavier BOURBON Examinateur
%Z Mme Myriam CARCASSES Co-directrice
%Z Mr Jean-Pierre OLLIVIER Examinateur
%Z Mr Patrice RIVARD Examinateur
%Z Mr Jean-Michel TORRENTI Rapporteur
%Z Mr Frédéric SKOCZYLAS Rapporteur
%8 2005-09-23
%D 2005
%K OPC
%K blended cement
%K diffusivity
%K gas permeability
%K water permeability
%K mercury intrusion porosimetry
%K ammonium nitrate attack
%K environmental permeameter
%K pressure drop
%K radioactive waste repository.
%K stockage nucléaire souterrain
%K décalcification
%K CEM I
%K CEM V/A
%K diffusivité
%K perméabilité au gaz
%K perméabilité à l'eau
%K porosimétrie mercure
%K dégradation par nitrate d'ammonium
%K perméamètre environnemental
%K gradient de pression
%K température
%K stockage nucléaire souterrain.
%Z Engineering Sciences [physics]Theses
%X Cementitious materials have been selected to compose the engineering barrier system (EBS) of the french radioactive waste deep repository, because of concrete physico-chemical properties: the hydrates of the cementitious matrix and the pH of the pore solution contribute to radionuclides retention; furthermore the compactness of these materials limits elements transport. <br />The confinement capacity of the system has to be assessed while a period at least equivalent to waste activity (up to 100.000 years). His durability was sustained by the evolution of transfer properties in accordance with cementitious materials decalcification, alteration that expresses structure long-term behavior. <br /><br />Then, two degradation modes were carried out, taking into account the different physical and chemical solicitations imposed by the host formation.<br /><br />The first mode, a static one, was an accelerated decalcification test using nitrate ammonium solution. It replicates the EBS alteration dues to underground water. Degradation kinetic was estimated by the amount of calcium leached and the measurement of the calcium hydroxide dissolution front. <br />To evaluate the decalcification impact, samples were characterized before and after degradation in term of microstructure (porosity, pores size distribution) and of transfer properties (diffusivity, gas and water permeability).<br />The influence of cement nature (ordinary Portland cement, blended cement) and aggregates type (lime or siliceous) was observed: experiments were repeated on different mortars mixes.<br />On this occasion, an essential reflection on this test metrology was led.<br /><br />The second mode, a dynamical degradation, was performed with an environmental permeameter. It recreates the EBS solicitations ensured during the resaturation period, distinguished by the hydraulic pressure imposed by the geologic layer and the waste exothermicity.<br />This apparatus, based on triaxial cell functioning, allows applying on samples pressure drop between 2 and 10 MPa and temperature from 20 to 80°C. Water permeability evolution relating to experimental parameters, uncoupled or coupled, was relied to mortars microstructural modifications.
%X Les matériaux cimentaires ont été retenus afin de composer la barrière ouvragée du site français de stockage des déchets nucléaires en formation géologique profonde. Ce choix se justifie par les capacités physico-chimiques propres aux bétons : les hydrates de la matrice cimentaire (CSH) et le pH de sa solution interstitielle contribuent à la rétention des radionucléides ; d'autre part la compacité de ces matériaux limite le transport d'éléments. <br />Il convient de s'assurer de la pérennité de cette structure pendant une durée au moins égale à celle de la vie des déchets (jusqu'à 100 000 ans). Sa durabilité a été éprouvée par l'évolution des propriétés de transfert en fonction de la décalcification de matériaux cimentaires, altération traduisant le vieillissement de l'ouvrage. <br /><br />Deux modes de dégradation ont ainsi été appliqués tenant compte des différentes interactions physico-chimiques induites par la formation hôte. <br /><br />Le premier, de type statique, a consisté en une décalcification accélérée par le nitrate d'ammonium. Il simule l'altération de la barrière ouvragée par les eaux souterraines. La cinétique de la dégradation a été estimée par le suivi du calcium lixivié et l'avancée du front de dissolution de l'hydroxyde de calcium.<br />Pour évaluer l'impact de la décalcification, les échantillons ont été caractérisés à l'état sain puis dégradé, en termes de microstructure (porosité, distribution porosimétrique) et de propriétés de transfert (diffusivité, perméabilité au gaz et à l'eau). <br />L'influence de la nature du liant (CEM I et CEM V/A) et des granulats (calcaires et siliceux) a été observée en répétant les essais sur différentes formulations de mortiers.<br />A cette occasion, une importante réflexion sur la métrologie de cet essai a été menée.<br /><br />Le deuxième mode de dégradation, dynamique, a été réalisé par un perméamètre environnemental. Il recrée les sollicitations subies par l'ouvrage lors de sa phase de resaturation post-fermeture (pression hydraulique imposée par la couche géologique et exothermicité des déchets).<br />Cet appareillage, basé sur le principe d'une cellule triaxiale, a permis de fixer un gradient de pression entre 2 et 10 MPa et une température de 20 à 80°C. La variation de la perméabilité à l'eau en fonction de ces deux paramètres expérimentaux, découplés et couplés, a été mesurée et reliée aux modifications microstructurales des échantillons. <br /><br />Mots clés : décalcification, CEM I, CEM V/A, diffusivité, perméabilité au gaz, perméabilité à l'eau, porosimétrie mercure, dégradation par nitrate d'ammonium, perméamètre environnemental, gradient de pression, température, stockage nucléaire souterrain.
%G French
%Z ANDRA - Stockage profond
%2 https://tel.archives-ouvertes.fr/tel-00274268/document
%2 https://tel.archives-ouvertes.fr/tel-00274268/file/These_Celine_PERLOT.pdf
%L tel-00274268
%U https://tel.archives-ouvertes.fr/tel-00274268
%~ INSA-TOULOUSE
%~ LMDC
%~ TEL-INSATOULOUSE
%~ INSA-GROUPE
%~ LMDC-THESES
%~ UNIV-TLSE3

%0 Conference Proceedings
%T Advances in the Use of Biological Stabilisers and Hyper-Compaction for Sustainable Earthen Construction Materials
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%A Muguda, Sravan
%A Lucas, George
%A Hughes, Paul
%A Augarde, Charles
%A Cuccurullo, Alessia
%A Bruno, Agostino Walter
%A Perlot-Bascoules, Céline
%A Gallipoli, Domenico
%< avec comité de lecture
%( International Symposium on Earthen Structures (ISES-2018)
%B Earthen Dwellings and Structures: Current Status in Their Adoption
%C Bangalore, India, India
%Y Reddy
%Y B. V. Venkatarama and Mani
%Y Monto and Walker
%Y Pete
%I Springer Singapore
%3 Springer Transactions in Civil and Environmental Engineering
%P 191-201
%8 2018-08-22
%D 2018
%R 10.1007/978-981-13-5883-8_17
%K Unsaturated soils
%K Rammed earth
%K Biopolymers
%K Hyper-compaction
%K Stabilisers
%Z Physics [physics]Conference papers
%X In the majority of cases, earthen construction materials for real buildings require amendment to deliver suitable material properties, which could be some additional strength or resilience to erosion. In modern earthen construction, in India, Australia and other parts of the world, cement and lime have been successfully used as stabilisers, providing both strength and durability benefits. However, the use of cement is detrimental to the green credentials of earthen construction materials, due to the large carbon footprint of that material's manufacture and, for some time, researchers have been motivated to find more appropriate stabilisers and manufacturing methods. In this paper, we present recent findings from two projects that are linked by this motivation and involve the study of bio-based stabilisers and alternative manufacturing methods for in situ and unit-based materials. Results are presented from laboratory testing of strength and durability of a range of materials, bio-stabilisers and manufacturing processes, indicating that there could be viable alternatives to cement and lime, certainly for many current uses of earthen construction materials.
%G English
%L hal-02154031
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02154031
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T A Microstructural Insight into the Hygro-Mechanical Behaviour of a Stabilised Hypercompacted Earth
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%A Bruno, Agostino Walter
%A Perlot-Bascoules, Céline
%A Mendes, Joao
%A Gallipoli, Domenico
%< avec comité de lecture
%J Materials and Structures/Materiaux et Constructions
%V 51
%N 1
%8 2018
%D 2018
%R 10.1617/s11527-018-1160-9
%K Moisture buffering
%K Rammed earth
%K Earth stabilisation
%K Mechanical behaviour
%K Passive air conditioning
%K Porosimetry
%Z Physics [physics]Journal articles
%X The use of raw earth as construction material can save embodied and operational energy because of low processing costs and passive regulation of indoor ambient conditions. Raw earth must however be mechanically and/or chemically stabilised to enhance stiffness, strength and water durability. In this work, stiffness and strength are enhanced by compacting raw earth to very high pressures up to 100~MPa while water durability is improved by using alkaline solutions and silicon based admixtures. The effect of these stabilisation methods on hygro-mechanical behaviour is explored and interpreted in terms of the microstructural features of the material. Stiffness and strength are defined at different humidity levels by unconfined compression tests while the moisture buffering capacity is measured by humidification/desiccation cycles as prescribed by the norm ISO 24353 (Hygrothermal performance of building materials and products determination of moisture adsorption/desorption properties in response to humidity variation. International Organization for Standardization, Geneva, 2008). As for the microstructural characterisation, different tests (i.e. X-ray diffractometry, Infrared Spectroscopy, Mercury Intrusion Porosimetry, Nitrogen Adsorption) are performed to analyse the effect of stabilisation on material fabric and mineralogy. Results indicate that the use of alkaline activators and silicon based admixtures significantly improves water durability while preserving good mechanical and moisture buffering properties. Similarly, the compaction to very high pressures results in high levels of stiffness and strength, which are comparable to those of standard masonry bricks. This macroscopic behaviour is then linked to the microscopic observations to clarify the mechanisms through which stabilisation affects the properties of raw earth at different scales. \textcopyright 2018, RILEM.
%G English
%L hal-02153328
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02153328
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Effect of Stabilisation on Mechanical Properties, Moisture Buffering and Water Durability of Hypercompacted Earth
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%A Bruno, Agostino Walter
%A Gallipoli, Domenico
%A Perlot-Bascoules, Céline
%A Mendes, Joao
%< avec comité de lecture
%@ 0950-0618
%J Construction and Building Materials
%I Elsevier
%V 149
%P 733-740
%8 2017
%D 2017
%R 10.1016/j.conbuildmat.2017.05.182
%K Alkaline activation
%K Compacted earth
%K Compressive strength
%K Moisture buffering
%K Raw earth
%K Silane-siloxane emulsion
%K Stabilisation
%K Stiffness
%K Water durability
%Z Physics [physics]Journal articles
%X This paper investigates the effect of stabilisation by alkaline activation and silicon based admixture on the mechanical properties, moisture buffering capacity and durability of an earthen material for building construction. The stiffness and strength of both unstabilised and stabilised cylindrical samples were measured, under different humidity conditions, by means of unconfined compression tests. The effect of stabilisation on moisture buffering capacity was instead explored by subjecting samples to cyclic variations of relative humidity at constant temperature, according to the experimental procedures prescribed by the norm ISO 24353 [21]. Finally, durability against water erosion was assessed by performing immersion, suction and liquid contact tests on both unstabilised and stabilised samples according to the norm DIN 18945 [16]. Results from this extensive experimental campaign highlighted that the chosen stabilisation methods improved the durability of the material while maintaining a relatively good mechanical performance and a good to excellent moisture buffering capacity.
%G English
%L hal-02153462
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02153462
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Conference Paper
%F Oral 
%T Effects of compaction stress on the physical, hygroscopic and mechanical properties of an earthen construction material
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%A Bruno, Agostino Walter
%A Gallipoli, Domenico
%A Perlot-Bascoules, Céline
%A Mendes, Joao
%< avec comité de lecture
%Z Caractérisation
%B 13èmes Journéess d'études des Milieux Poreux 2016
%C Anglet, France
%8 2016-10-11
%D 2016
%K Compressed earth
%K pore size distribution
%K compressive strength
%K moisture buffer value
%Z Physics [physics]/Mechanics [physics]Conference papers
%X Effects of compaction stress on the physical, hygroscopic and mechanical properties of an earthen construction material
%G French
%2 https://hal.archives-ouvertes.fr/hal-01394536/document
%2 https://hal.archives-ouvertes.fr/hal-01394536/file/awb_dg_cpb_jm_JEMP.pdf
%L hal-01394536
%U https://hal.archives-ouvertes.fr/hal-01394536
%~ JEMP2016
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Mechanical Properties of Biopolymer-Stabilised Soil-Based Construction Materials
%+ School of Engineering and Computing Sciences
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%A Muguda, S.
%A Booth, S.J.
%A Hughes, P.N.
%A Augarde, C.E.
%A Perlot-Bascoules, Céline
%A Bruno, A.W.
%A Gallipoli, Domenico
%< avec comité de lecture
%J Géotechnique Letters
%I ICE Publishing
%V 7
%N 4
%P 309--314
%8 2017
%D 2017
%R 10.1680/jgele.17.00081
%K suction
%K laboratory tests
%K partial saturation
%Z Physics [physics]Journal articles
%X Soil-based construction materials are of interest as structural building materials due to their green credentials, as well as being present in many historical structures. For effective conservation of the latter, and to motivate greater uptake for new construction, understanding of the mechanical and hydraulic properties of these materials is in need of improvement. Earthen construction materials can be considered to be manufactured unsaturated soils, and advances in understanding can be made by considering them from a geotechnical point of view. This paper presents initial results from a major programme of testing, seeking improved properties for earthen construction materials, where unusual organic compounds have been employed as stabilisers. Two gums (guar and xanthan) used as stabilisers for a soil mixture are shown to have significant effects on certain mechanical properties, some of which can be explained, and other aspects which are in need of further investigation.
%G English
%L hal-02153539
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02153539
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Conference Proceedings
%T Effect of Very High Compaction Pressures on the Physical and Mechanical Properties of Earthen Materials
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%+ Instituto Superior de Engenharia de Lisboa (ISEL)
%A Bruno, A.W.
%A Gallipoli, Domenico
%A Perlot-Bascoules, Céline
%A Mendes, J.
%< avec comité de lecture
%( 3rd European Conference on Unsaturated Soils
%B 3rd European Conference on Unsaturated Soils \textendash ``E-UNSAT 2016''
%C Paris, France, France
%I EDP Sciences
%3 E3S Web of Conferences
%V 9
%P 14004
%8 2016-09-12
%D 2016
%R 10.1051/e3sconf/20160914004
%Z Physics [physics]Conference papers
%G English
%L hal-02153967
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02153967
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Durability and hygroscopic behaviour of biopolymer stabilised earthen construction materials
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%A Muguda, S.
%A Lucas, G.
%A Hughes, P.N.
%A Augarde, C.E.
%A Perlot-Bascoules, Céline
%A Bruno, A.W.
%A Gallipoli, Domenico
%< avec comité de lecture
%@ 0950-0618
%J Construction and Building Materials
%I Elsevier
%V 259
%P 119725
%8 2020-10
%D 2020
%R 10.1016/j.conbuildmat.2020.119725
%Z Engineering Sciences [physics]Journal articles
%G English
%L hal-03004920
%U https://hal.archives-ouvertes.fr/hal-03004920
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Optimization of bricks production by earth hypercompaction prior to firing
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%A Bruno, Agostino Walter
%A Gallipoli, Domenico
%A Perlot-Bascoules, Céline
%A Mendes, Joao
%< avec comité de lecture
%@ 0959-6526
%J Journal of Cleaner Production
%I Elsevier
%V 214
%P 475-482
%8 2019-03
%D 2019
%R 10.1016/j.jclepro.2018.12.302
%Z Engineering Sciences [physics]/Materials
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of materials [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Materials and structures in mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of the solides [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of the structures [physics.class-ph]
%Z Engineering Sciences [physics]/Civil Engineering/Construction durable
%Z Engineering Sciences [physics]/Civil Engineering/Eco-conception
%Z Engineering Sciences [physics]/Civil Engineering/Géotechnique
%Z Engineering Sciences [physics]/Civil Engineering/Génie civil nucléaire
%Z Environmental Sciences/Environmental EngineeringJournal articles
%G English
%L hal-02354295
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02354295
%~ UNIV-PAU
%~ SDE
%~ GENIECIVIL
%~ GIP-BE
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Conference Paper
%F Oral 
%T Briques de terre crue : procédure de compactage haute pression et influence sur les propriétés mécaniques
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%+ Nobatek
%A Bruno, Agostino Walter
%A Gallipoli, Domenico
%A Perlot-Bascoules, Céline
%A Mendes, Joao
%A Salmon, Nicolas
%< avec comité de lecture
%Z Sols et géotechnique
%B Rencontres Universitaires de Génie Civil
%C Bayonne, France
%8 2015-05-26
%D 2015
%K Terre crue compressée
%K compaction
%K module d'Young
%K résistance mécanique
%Z Engineering Sciences [physics]/Civil EngineeringConference papers
%X <div> <p>Au cours des dernières décennies, un intérêt croissant est porté à la terre crue en tant qu' éco-matériau de construction pour son caractère local et la faible consommation d'énergie qui caractérise son cycle de vie global. Toutefois, sa résistance mécanique relativement faible et la sensibilité à l'eau freinent le développement de ce matériau. Le principal but de cette étude est d'améliorer les propriétés mécaniques de briques de terre crue par l'emploi d'une nouvelle procédure de compactage à pression élevée pour une durée correspondante au temps de consolidation. Un système de compactage a été élaboré afin de compacter le sol à haute pression en permettant le drainage de l'eau interstitielle. Dans un premier temps, les caractéristiques géotechniques du matériau d'étude ont été déterminées. Après compactage à 25, 50 ou 100 MPa, les échantillons ont été placés dans une enceinte climatique (25°C, humidité relative de 62 %) jusqu'à équilibre hydrique, puis testés afin d'évaluer leur module d'Young et leur résistance mécanique en compression. Cette nouvelle méthode de compactage à haute pression a permis d'atteindre des performances mécaniques élevées. Ainsi, un des principaux freins à l'utilisation de la terre crue comme matériau de construction serait levé.</p> </div>
%G French
%2 https://hal.archives-ouvertes.fr/hal-01167676/document
%2 https://hal.archives-ouvertes.fr/hal-01167676/file/augc2015_bruno_2_CB.pdf
%L hal-01167676
%U https://hal.archives-ouvertes.fr/hal-01167676
%~ RUGC15
%~ UNIV-PAU
%~ GENIECIVIL
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Mechanical Behaviour of Hypercompacted Earth for Building Construction
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%A Bruno, Augostino Walter
%A Gallipoli, Domenico
%A Perlot-Bascoules, Céline
%A Mendes, Joao
%< avec comité de lecture
%J Material and Structures, Springer RILEM
%V 50
%N 2
%8 2017
%D 2017
%R 10.1617/s11527-017-1027-5
%K Compressive strength
%K Raw earth
%K Bio-sourced construction materials
%K Compacted earth bricks
%K Earth construction
%K Hypercompaction
%K Rammed earth
%K Young modulus
%Z Physics [physics]Journal articles
%X This paper investigates the mechanical behaviour of a hypercompacted unstabilized earth material manufactured by compressing a moist soil to very high pressures up to 100 MPa. The hypercompaction procedure increases material density, which in turn improves mechanical characteristics. Samples were manufactured at the scale of both small cylinders and masonry bricks. The effect of ambient humidity on the mechanical characteristics of the material was investigated at the scale of cylindrical samples, showing that both strength and stiffness are sensitive to environmental conditions and tend to increase as ambient humidity reduces. The strength of the bricks was instead investigated under laboratory ambient conditions by using different experimental configurations to assess the influence of sample slenderness and friction confinement. Additional tests were also performed to evaluate the influence of mortar joints and compaction-induced anisotropy. Overall, the hypercompacted earth material exhibits mechanical characteristics that are comparable with those of traditional building materials, such as fired bricks, concrete blocks or stabilized compressed earth.
%G English
%L hal-02153536
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02153536
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA2
%~ UPPA-OA

