%0 Book Section
%T The Mediterranean: between globalisation, De-globalisation and Re-globalisation
%+ Centre d'Analyse Théorique et de Traitement des données économiques (CATT)
%A Regnault, Henri
%B . Mediterranean Yearbook MED.2010
%I Mediterranean Yearbook, Barcelone
%P 94-100
%8 2010
%D 2010
%Z Humanities and Social Sciences/Economics and FinanceBook sections
%G English
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-01880260/document
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-01880260/file/35F_2010_Regnault_Globalisation_en.pdf
%L hal-01880260
%U https://hal-univ-pau.archives-ouvertes.fr/hal-01880260
%~ UNIV-PAU
%~ CATT
%~ AO-ECONOMIE
%~ SHS
%~ TESTUPPA2
%~ UPPA-OA

%0 Book Section
%T Les révolutions agricoles en perspective - Introduction
%+ Centre d'Analyse Théorique et de Traitement des données économiques (CATT)
%+ Société, environnement, territoire (SET)
%+ Institut Pluridisciplinaire de Recherche sur l'Environnement et les Matériaux (IPREM)
%A Regnault, Henri
%A Arnauld De Sartre, Xavier
%A Regnault-Roger, Catherine
%B Les révolutions agricoles en perspective
%I Editions France agricole
%P 204
%8 2012
%D 2012
%K Révolutions agricoles
%K OGM
%K Biotechnologie
%Z Humanities and Social Sciences/Geography
%Z Humanities and Social Sciences/Economics and Finance
%Z Life Sciences [q-bio]/Biotechnology
%Z Computer Science [cs]/Biotechnology
%Z Humanities and Social Sciences/HistoryBook sections
%G French
%2 https://halshs.archives-ouvertes.fr/halshs-00768291/document
%2 https://halshs.archives-ouvertes.fr/halshs-00768291/file/Intro_RCRA.pdf
%L halshs-00768291
%U https://halshs.archives-ouvertes.fr/halshs-00768291
%~ SHS
%~ CNRS
%~ UNIV-PAU
%~ AO-HISTOIRE
%~ AO-GEOGRAPHIE
%~ AO-ECONOMIE
%~ HISTOIRE
%~ GIP-BE
%~ IPREM
%~ INC-CNRS
%~ CATT
%~ PASSAGES-UPPA
%~ UMR5319
%~ UNIV-BORDEAUX-MONTAIGNE
%~ TESTUPPA2
%~ UPPA-OA

%0 Book Section
%T Les révolutions agricoles en perspective (Introduction)
%+ Centre d'Analyse Théorique et de Traitement des données économiques (CATT)
%+ Société, environnement, territoire (SET)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Regnault, Henri
%A Arnauld De Sartre, Xavier
%A Regnault-Roger, Catherine
%B Les révolutions agricoles en perspective
%E Regnault H.
%E Arnauld de Sartre X.
%E Regnault-Roger C.
%I Editions France Agricole
%8 2012
%D 2012
%Z Humanities and Social Sciences/Economics and FinanceBook sections
%X Dans son histoire récente, l'humanité a connu deux révolutions agricoles et une troisième est en cours : Au XVIIIe le siècle, l'abandon de la jachère et la mise en oeuvre d'assolements. Au XXe siècle, la généralisation de la mécanisation et de la motorisation, l'utilisation d'engrais chimiques et de produits phytosanitaires, le saut qualitatif en matière de semences. Au XXIe siècle, l'avènement des biotechnologies associé à celui de nouvelles techniques culturales.Chacune de ces révolutions agricoles (hasard ou nécessité) intervient dans une période de forte croissance démographique d'une partie du monde. Ces révolutions ont permis de gagner la course de vitesse entre les ressources agricoles et les besoins alimentaires. La troisième révolution agricole se profile dans une période où les démographes nous prédisent une humanité à 9 milliards d'ici 2050 et donc une augmentation encore très conséquente des besoins alimentaires.Ces révolutions s'accompagnent d'incompréhensions, d'oppositions tant elles bousculent l'ordre établi des routines productives, techniques ou juridiques. Elles provoquent également de grandes peurs alimentaires et environnementales (spontanées ou manipulées). Cet ouvrage montre la complexité de l'interaction entre les innovations technologiques, leur perception par les acteurs sociaux qui les mettent en oeuvre activement ou les subissent passivement, ainsi que les transformations juridiques, sociales, environnementales ou paysagères qu'elles nécessitent ou induisent.
%G French
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-01880245/document
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-01880245/file/F_revolutions_agricoles_en_perspective_2012.pdf
%L hal-01880245
%U https://hal-univ-pau.archives-ouvertes.fr/hal-01880245
%~ UNIV-PAU
%~ CATT
%~ IPREM
%~ CNRS
%~ AO-ECONOMIE
%~ INC-CNRS
%~ SHS
%~ PASSAGES-UPPA
%~ UNIV-BORDEAUX-MONTAIGNE
%~ UMR5319
%~ TESTUPPA2
%~ UPPA-OA
%~ MONTAIGNE_TABLE

%0 Journal Article
%T Libéralisation agricole et développement. Introduction
%+ Centre International de Hautes Études Agronomiques Méditerranéennes (CIHEAM)
%+ Marchés, Organisations, Institutions et Stratégies d'Acteurs (UMR MOISA)
%+ Centre d'Analyse Théorique et de Traitement des données économiques (CATT)
%A Petit, Michel
%A RASTOIN, Jean-Luc
%A Regnault, Henri
%< avec comité de lecture
%@ 1267-5059
%J Région et Développement
%I L'Harmattan
%S LIBÉRALISATION AGRICOLE ET PAYS EN DÉVELOPPEMENT
%N 23
%P 7-14
%8 2006
%D 2006
%Z Humanities and Social Sciences/Economics and FinanceJournal articles
%G French
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-01885317/document
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-01885317/file/33F_2006_Regnault_Intro_Liberalisation_agricole_et_pays_en_dvlpt.pdf
%L hal-01885317
%U https://hal-univ-pau.archives-ouvertes.fr/hal-01885317
%~ UNIV-PAU
%~ CATT
%~ INRA
%~ AO-ECONOMIE
%~ AGROPOLIS
%~ CIRAD
%~ AGREENIUM
%~ SHS
%~ CIHEAM
%~ CIHEAM-TEST
%~ MONTPELLIER-SUPAGRO
%~ INRAE
%~ INRAEOCCITANIEMONTPELLIER
%~ MOISA
%~ TESTUPPA2
%~ UPPA-OA

