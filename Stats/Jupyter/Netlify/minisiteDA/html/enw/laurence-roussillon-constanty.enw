%0 Edited Book
%T Converging Lines: Needlework in English Literature and Visual Arts
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Laurence, Roussillon-Constanty
%@ 1638-1718
%J E-rea - Revue électronique d’études sur le monde anglophone
%I Laboratoire d’Études et de Recherche sur le Monde Anglophone
%N 16.1
%8 2018-12-11
%D 2018
%R 10.4000/erea.6451
%Z Humanities and Social Sciences/Art and art history
%Z Humanities and Social Sciences/Cultural heritage and museology
%Z Humanities and Social Sciences/LiteratureDirections of work or proceedings
%G English
%L halshs-02024809
%U https://halshs.archives-ouvertes.fr/halshs-02024809
%~ SHS
%~ UNIV-PAU
%~ ALTER
%~ TESTUPPA2
%~ UPPA-OA

%0 Edited Book
%T Crossings – The South – Commitment
%+ Centre de Recherches Anglophones (CREA (EA 370))
%A Bury, Laurent
%A Moine, Fabienne
%A Roussillon-Constanty, Laurence
%A Thornton, Sara
%@ 0220-5610
%J Cahiers Victoriens et Edouardiens
%I Montpellier : Centre d'études et de recherches victoriennes et édouardiennes
%S Cahiers victoriens et édouardiens
%N 83
%8 2016
%D 2016
%R 10.4000/cve.2435
%Z Humanities and Social Sciences/LiteratureDirections of work or proceedings
%X Volume 83 of Cahiers victoriens et édouardiens is a four-section issue with a collection of articles selected from three different conferences and a section with a complementary article on Eliza Cook. Two contributions came from the SAES Conference which was held in Caen in May 2014 (‘Crossings’), six papers were given during the annual SFEVE conference which was held in Perpignan in January 2015 (‘The South’), convened by Isabelle Cases, and five more articles were selected from the SAES Conference which was held in Toulon in June 2015 (‘Commitment’). Happy reading!
%X Le volume 83 des Cahiers victoriens et édouardiens est un numéro consacré essentiellement aux actes de trois colloques au cours desquels ont été sélectionnées les meilleures contributions. L’atelier de la SFEVE lors du 54e congrès de la SAES à Caen en mai 2014, sur le thème « Traversées », a fourni deux des articles. Le Colloque de la SFEVE à Perpignan en janvier 2015 sur le thème « Le Sud », organisé par Isabelle Cases, nous a permis de recueillir 6 articles et l’atelier de la SFEVE lors du 55e congrès de la SAES à Toulon en juin 2015 portant sur le thème de « l'engagement » a donné 5 articles. Vient s’ajouter à ces 13 contributions, un article complémentaire sur Eliza Cook. Nous vous en souhaitons bonne lecture.
%G French
%L hal-01725666
%U https://hal.parisnanterre.fr//hal-01725666
%~ SHS
%~ UNIV-PARIS10
%~ UNIV-PARIS-LUMIERES
%~ UNIV-PARIS-NANTERRE

%0 Edited Book
%T Becoming Animal
%+ Centre de Recherches Anglophones (CREA (EA 370))
%A Bury, Laurent
%A Moine, Fabienne
%A Roussillon-Constanty, Laurence
%A Thornton, Sara
%@ 0220-5610
%J Cahiers Victoriens et Edouardiens
%Y Bury
%Y Laurent and Moine
%Y Fabienne and Roussillon-Constanty
%Y Laurence and Thornton
%Y Sara
%I Montpellier : Centre d'études et de recherches victoriennes et édouardiennes
%N 85
%8 2017
%D 2017
%R 10.4000/cve.3149
%K Direction de numéro de revue
%Z Humanities and Social Sciences/LiteratureDirections of work or proceedings
%G French
%L hal-01725653
%U https://hal.parisnanterre.fr//hal-01725653
%~ SHS
%~ UNIV-PARIS10
%~ UNIV-PARIS-LUMIERES
%~ UNIV-PARIS-NANTERRE

%0 Journal Article
%T La topographie selon Ruskin : saillance du visible et du lisible dans Modern Painters
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Roussillon-Constanty, Laurence
%< avec comité de lecture
%@ 0035-1121
%J Revue de Géographie Alpine / Journal of Alpine Research
%I Association pour la diffusion de la recherche alpine
%N 104-2
%8 2016
%D 2016
%R 10.4000/rga.3397
%Z Humanities and Social Sciences/LiteratureJournal articles
%G French
%L hal-02170618
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02170618
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T From the "House of Life" to the "Decorative Arts": Dante Gabriel Rossetti and Ceramics
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Roussillon-Constanty, Laurence
%< avec comité de lecture
%@ 2108-6559
%J Miranda : Revue pluridisciplinaire sur le monde anglophone. Multidisciplinary peer-reviewed journal on the English-speaking world
%I Laboratoire CAS (Cultures anglo-saxonnes)
%S Ceramics / Submorphemics
%N 7
%8 2012
%D 2012
%R 10.4000/miranda.4436
%Z Humanities and Social Sciences/LiteratureJournal articles
%G English
%L hal-02171652
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02171652
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Of Ruskinian Topography: Visible and Legible Salience in Modern Painters
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Roussillon-Constanty, Laurence
%< avec comité de lecture
%@ 0035-1121
%J Revue de Géographie Alpine / Journal of Alpine Research
%I Association pour la diffusion de la recherche alpine
%8 2016-09-18
%D 2016
%R 10.4000/rga.3407
%Z Humanities and Social Sciences/Geography
%Z Humanities and Social Sciences/Art and art history
%Z Humanities and Social Sciences/LiteratureJournal articles
%G English
%L halshs-02024798
%U https://halshs.archives-ouvertes.fr/halshs-02024798
%~ SHS
%~ UNIV-PAU
%~ AO-GEOGRAPHIE
%~ GIP-BE
%~ ALTER
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Comforting Creatures: Changing Visions of Animal Otherness in the Victorian Period
%+ Laboratoire de Recherche sur les Cultures Anglophones (LARCA UMR 8225)
%A Roussillon-Constanty, Laurence
%A Thornton, Sara
%< avec comité de lecture
%@ 0220-5610
%J Cahiers Victoriens et Edouardiens
%I Montpellier : Centre d'études et de recherches victoriennes et édouardiennes
%C Montpellier, France
%N 88
%8 2018-11-05
%D 2018
%K fashion
%K master-slave
%K cultural studies
%K history
%K literature
%K science
%K creatures
%K otherness
%K subjectivity
%K animals
%K speciesism
%K vegan
%K Victorian
%Z Humanities and Social Sciences/Literature
%Z Humanities and Social Sciences/HistoryJournal articles
%X This volume enquires into the different ways the Victorians express their tricky relationships with animals, their speciesism, as well as into their attempts to rethink the question of species and the values and rights attached to creatures of all kinds. The collection follows on from Becoming Animal (No 85) edited by Fabienne Moine in 2017. Both volumes gather papers given at the Société Française d’Etudes Victoriennes et Edouardiennes annual conference ‘Becoming Animal with the Victorians’ held at Paris Diderot University in February 2016. The focus of the present volume is on the individual relationship between pets and their owners and on the emotional grounding of animal/human interactions. It also considers the aestheticizing of animal figures and artefacts that may either connect or alienate animals from their human companions or owners and shift companionship into potentially damaging interactions and towards cruelty as evidenced in today’s fashion design as much as in Victorian taxidermy practices. Les articles réunis dans ce volume explorent les différentes facettes de la relation entre les animaux et les humains à l’époque victorienne et les tentatives menées pour repenser les catégories et refonder la valeur et les droits attachés aux différentes familles d’animaux. Second volume d’articles consacré à ce thème suite à la publication du numéro 85 des Cahiers Victoriens édité par Fabienne Moine en 2017, ce volume est également le fruit du colloque « Becoming Animal with the Victorians » organisé par la S.F.E.V.E. à l’université Paris Diderot en 2016. Au sein de ce vaste thème d’étude que constitue la condition animale, le présent volume porte sur la relation intime que les individus ont nouée avec leurs animaux de compagnie et sur les liens affectifs qui les rapprochent. Une partie des articles réunis ici s’intéresse également à la façon dont les objets manufacturés à partir d’animaux morts et les objets représentant des animaux ont influé sur les rapports entre ces derniers et les hommes, menant à certaines pratiques abusives de cruauté envers les animaux, un traitement que l’on retrouve parfois aujourd’hui dans la mode contemporaine mais qui remonte à des modes plus anciennes telle celle de la taxidermie très en vogue au dix-neuvième siècle.
%G English
%2 https://hal.archives-ouvertes.fr/hal-02054966/document
%2 https://hal.archives-ouvertes.fr/hal-02054966/file/cve-3790.pdf
%L hal-02054966
%U https://hal.archives-ouvertes.fr/hal-02054966
%~ HISTOIRE
%~ AO-HISTOIRE
%~ CNRS
%~ SHS
%~ UNIV-PARIS7
%~ USPC
%~ UNIV-PARIS
%~ UP-SOCIETES-HUMANITES

