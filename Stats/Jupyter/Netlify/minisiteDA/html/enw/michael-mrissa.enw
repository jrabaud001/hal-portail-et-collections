%0 Conference Proceedings
%T An Approach for Composing RESTful Linked Services on the Web
%+ Service Oriented Computing (SOC)
%A Bennara, Mahdi
%A Mrissa, Michael
%A Amghar, Youssef
%< avec comité de lecture
%B World Wide Web
%C Seoul, South Korea
%I ACM Press
%3 Campanion Volume (WS-REST 2014)
%8 2014-04-07
%D 2014
%R 10.1145/2567948.2579222
%K composition
%K RESTful Web services
%K linked services
%K semantic Web
%K Service discovery and interfaces
%Z H.3.5.2 [Web-based Services]: RESTful Web services; Service discovery and interfaces
%Z Computer Science [cs]
%Z Computer Science [cs]/WebConference papers
%X In this paper, we present an approach to compose linked services on the Web based on the principles of linked data and REST. Our contribution is a unified method for discovering both the interaction possibilities a service offers and the available semantic links to other services. Our composition engine is implemented as a generic client that allows exploring a service API and interacting with other services to answer user's goal. We rely on a typical scenario in order to illustrate the benefits of our composition approach. We implemented a prototype to demonstrate the applicability of our proposal, experiment and discuss the results obtained.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01212722v2/document
%2 https://hal.archives-ouvertes.fr/hal-01212722/file/2014-WS-REST.pdf
%L hal-01212722
%U https://hal.archives-ouvertes.fr/hal-01212722
%~ CNRS
%~ EC-LYON
%~ UNIV-LYON2
%~ UNIV-LYON1
%~ INSA-LYON
%~ LIRIS
%~ INSA-GROUPE
%~ LABEXIMU
%~ UDL
%~ UNIV-LYON

%0 Report
%T Towards An Avatar Architecture for the Web of Things
%+ Service Oriented Computing (SOC)
%+ Traces, Web, Education, Adaptation, Knowledge (TWEAK)
%+ Laboratoire de Conception et d'Intégration des Systèmes (LCIS)
%+ Disruption-Tolerant Networking & Computing (CASA)
%+ Génération Robots
%A Mrissa, Michael
%A Médini, Lionel
%A Jamont, Jean-Paul
%A Le Sommer, Nicolas
%A Laplace, Jérôme
%I Université Lyon 1 - Claude Bernard
%8 2015-01-01
%D 2015
%Z Computer Science [cs]/Software Engineering [cs.SE]
%Z Computer Science [cs]/Web
%Z Computer Science [cs]/Robotics [cs.RO]
%Z Computer Science [cs]/Emerging Technologies [cs.ET]Reports
%X The Web of Things (WoT) extends the Internet of Things considering that each physical object can be accessed and controlled using Web-based languages and protocols. In this paper, we summarize ongoing work promoting the concept of avatar as a new virtual abstraction to extend physical objects on the Web. An avatar is an extensible and distributed runtime environment endowed with an autonomous behaviour. Avatars rely on Web languages, protocols and reasoning about semantic annotations to dynamically drive connected objects, exploit their capabilities and expose their functionalities as Web services. Avatars are also able to collaborate together in order to achieve complex tasks.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01376637/document
%2 https://hal.archives-ouvertes.fr/hal-01376637/file/Liris-7036.pdf
%L hal-01376637
%U https://hal.archives-ouvertes.fr/hal-01376637
%~ CNRS
%~ UNIV-PMF_GRENOBLE
%~ UNIV-UBS
%~ EC-LYON
%~ INSTITUT-TELECOM
%~ IRISA_SET
%~ UNIV-RENNES1
%~ INRIA
%~ LCIS
%~ IRISA
%~ UNIV-LYON2
%~ LIRIS
%~ INPG
%~ LARA
%~ UR1-HAL
%~ UR1-MATH-STIC
%~ UR1-UFR-ISTIC
%~ CENTRALESUPELEC
%~ INSA-LYON
%~ UNIV-LYON1
%~ TEST-UNIV-RENNES
%~ TEST-UR-CSS
%~ UNIV-RENNES
%~ INRIA-AUT
%~ INSA-GROUPE
%~ IRISA-D2
%~ LABEXIMU
%~ UDL
%~ UNIV-LYON
%~ INSTITUTS-TELECOM
%~ IRISA_UBS
%~ UBS
%~ INSA-RENNES
%~ UGA
%~ ANR

%0 Conference Paper
%F Oral 
%T Towards a Meta-model for Context in the Web of Things
%+ Service Oriented Computing (SOC)
%+ Traces, Web, Education, Adaptation, Knowledge (TWEAK)
%A Terdjimi, Mehdi
%A Médini, Lionel
%A Mrissa, Michael
%< avec comité de lecture
%B Karlsruhe Service Summit Workshop
%C Karlsruhe, Germany
%8 2016-02-25
%D 2016
%K Web of Things
%K Context-awareness
%K Context modeling
%Z Computer Science [cs]/Web
%Z Computer Science [cs]/Artificial Intelligence [cs.AI]
%Z Computer Science [cs]/Ubiquitous Computing
%Z Computer Science [cs]/Mobile ComputingConference papers
%X The Web of Things (WoT) uses Web technologies to engage connected objects in applications. However, building context-aware WoT applications requires accurate context description. In this paper, we overview different context modeling approaches in various fields related to the WoT, before studying the architecture of WoT applications. Based on our study, we propose a multi-level and multi-dimensional context meta-model to help identify and organize context information WoT applications need to deal with adaptation.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01255479/document
%2 https://hal.archives-ouvertes.fr/hal-01255479/file/Session_1C2_KSS_2016_paper_36.pdf
%L hal-01255479
%U https://hal.archives-ouvertes.fr/hal-01255479
%~ CNRS
%~ EC-LYON
%~ LIRIS
%~ INSA-LYON
%~ UNIV-LYON1
%~ LYON2
%~ INSA-GROUPE
%~ LABEXIMU
%~ UNIV-LYON2
%~ UDL
%~ UNIV-LYON

%0 Conference Proceedings
%T Context-Aware Interaction Approach to Handle Users Local Contexts in Web 2.0
%+ PReCISE Research Centre in Information Systems Engineering (PReCISE)
%+ Service Oriented Computing (SOC)
%A Al-Jabari, Mohanad
%A Mrissa, Michael
%A Thiran, Philippe
%< avec comité de lecture
%Z 4664
%B International Conference on Web Engineering
%C Vienna, Austria
%I Springer
%P 248-262
%8 2010-07-05
%D 2010
%R 10.1007/978-3-642-13911-6_17
%Z Computer Science [cs]Conference papers
%X Users sharing and authoring of Web contents via different Web sites is the main idea of the Web 2.0. However, Web users belong to different communities and follow their own semantics (referred to as contexts) to represent and interpret Web contents. Therefore, they encounter discrepancies when they have to interpret Web contents authored by different persons. This paper proposes a context-aware interaction approach that helps Web authors annotate Web contents with their local context information, so that it becomes possible for Web browsers to personalize these contents according to different users’ local contexts.
%G English
%L hal-01381473
%U https://hal.archives-ouvertes.fr/hal-01381473
%~ CNRS
%~ EC-LYON
%~ UNIV-LYON2
%~ LIRIS
%~ INSA-LYON
%~ UNIV-LYON1
%~ INSA-GROUPE
%~ LABEXIMU
%~ UDL
%~ UNIV-LYON

%0 Conference Proceedings
%T Privacy model and annotation for DaaS
%+ Service Oriented Computing (SOC)
%+ Technical University of Vienna [Vienna] (TU WIEN)
%A Mrissa, Michael
%A Tbahriti, Salah-Eddine
%A Truong, Hong-Linh
%< avec comité de lecture
%Z 4823
%B European Conference on Web Services (ECOWS)
%C Ayia Napa, Cyprus
%Y Antonio Brogi
%Y Cesare Pautasso
%Y George Angelos Papadopoulos
%I IEEE
%P 3-10
%8 2010-12-01
%D 2010
%R 10.1109/ECOWS.2010.11
%Z Computer Science [cs]Conference papers
%X Data as a Service (DaaS) builds on service-oriented technologies to enable fast access to data resources on the Web. However, this paradigm raises several new concerns that traditional privacy models for Web services do not handle. First, the distinction between the roles of service providers and data providers is unclear, leaving the latter helpless for specifying and verifying the enforcement of their data privacy requirements. Second, traditional models for privacy policies focus only on the service interface without taking into account privacy policies related to data resources. Third, unstructured data resources, as well as user permissions and obligations related to data that are utilized in DaaS are not taken into account.In this paper, we study data privacy as one of these concerns, which relates to the management of private information. The main contribution of this paper consists in: 1)~devising a model for making explicit privacy constraints of DaaS, and 2)~on the basis of the proposed privacy model, developing techniques that allow handling the privacy concern when querying DaaS. We validate the applicability of our proposal with some experiments.
%G English
%L hal-01381558
%U https://hal.archives-ouvertes.fr/hal-01381558
%~ CNRS
%~ EC-LYON
%~ UNIV-LYON2
%~ LIRIS
%~ INSA-LYON
%~ UNIV-LYON1
%~ INSA-GROUPE
%~ LABEXIMU
%~ UDL
%~ UNIV-LYON

%0 Conference Proceedings
%T Exchanging Data Agreements in the DaaS Model
%+ Technical University of Vienna [Vienna] (TU WIEN)
%+ Kaiserslautern University of Technology
%+ Service Oriented Computing (SOC)
%A Truong, Hong-Linh
%A Dustdar, Schahram
%A Goetze, Joachim
%A Fleuren, Tino
%A Mueller, Paul
%A Tbahriti, Salah-Eddine
%A Mrissa, Michael
%A Ghedira, Chirine
%< avec comité de lecture
%Z 5269
%B The 2011 IEEE Asia-Pacific Services Computing Conference
%C Jeju, South Korea
%I IEEE
%P 153-160
%8 2011-12-12
%D 2011
%R 10.1109/APSCC.2011.59
%Z Computer Science [cs]Conference papers
%X Rich types of data offered by data as a service (DaaS) in the cloud are typically associated with different and complex data concerns that DaaS service providers, data providers and data consumers must carefully examine and agree with before passing and utilizing data. Unlike service agreements, data agreements, reflecting conditions established on the basis of data concerns, between relevant stakeholders have got little attention. However, as data concerns are complex and contextual, given the trend of mixingdata sources by automated techniques, such as data mashup, data agreements must be associated with data discovery, retrieval and utilization. Unfortunately, exchanging data agreements so far has not been automated and incorporated into service and data discovery and composition. In this paper, we analyze possible steps and propose interactions among data consumers, DaaS service providers and data providers in exchanging data agreements. Based on that, we present a novel service for composing, managing, analyzing data agreements for DaaS in cloud environments and data marketplaces.
%G English
%L hal-01354510
%U https://hal.archives-ouvertes.fr/hal-01354510
%~ CNRS
%~ EC-LYON
%~ UNIV-LYON2
%~ LIRIS
%~ INSA-LYON
%~ UNIV-LYON1
%~ INSA-GROUPE
%~ LABEXIMU
%~ UDL
%~ UNIV-LYON

%0 Conference Paper
%F Oral 
%T Managing Web Resource Compositions
%+ Service Oriented Computing (SOC)
%A Bennara, Mahdi
%A Amghar, Youssef
%A Mrissa, Michael
%< avec comité de lecture
%B International Conference on Enabling Technologies: Infrastructure for Collaborative Enterprises
%C Larnaca, Cyprus
%8 2015-06-15
%D 2015
%R 10.1109/WETICE.2015.46
%K RESTful Web services
%K linked services
%K semantic Web
%K composition
%Z Computer Science [cs]/Web
%Z Computer Science [cs]Conference papers
%X Nowadays, the use of RESTful Web services promotes stateless service interaction and decentralized hypermedia-driven discovery and composition. However, there is a need for models and tools to drive user interaction as well as description, discovery and composition of RESTful services. In this paper, we provide a solution to help users manage, share and discover workflows of RESTful Web services. We annotate RESTful Web services with semantic information, and introduce the notion of composition directory as a Web resource that assists a user in sharing, managing and discovering workflows. Users' composition directories form a decentralized repository of service workflows connected by hypermedia links. We illustrate the benefits of our approach with a typical scenario and show through a set of experiments that the breadth-first search algorithm combined with the exploitation of semantic annotations efficiently answers users' goals by crawling through composition directories.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01212728/document
%2 https://hal.archives-ouvertes.fr/hal-01212728/file/Bennara.pdf
%L hal-01212728
%U https://hal.archives-ouvertes.fr/hal-01212728
%~ CNRS
%~ EC-LYON
%~ LIRIS
%~ UNIV-LYON2
%~ INSA-LYON
%~ UNIV-LYON1
%~ INSA-GROUPE
%~ LABEXIMU
%~ UDL
%~ UNIV-LYON

%0 Journal Article
%T How to Enhance Privacy within DaaS service Composition?
%+ Service Oriented Computing (SOC)
%+ Centre de Recherche Magellan
%+ Department of Computer Science and Engineering (CSE)
%A Tbahriti, Salah-Eddine
%A Ghedira, Chirine
%A Medjahed, Brahim
%A Mrissa, Michael
%A Benslimane, Djamal
%Z 12p.
%< avec comité de lecture
%@ 1937-9234
%J IEEE Systems Journal
%I IEEE
%V 7
%N 99
%P 442-454
%8 2013-01
%D 2013
%R 10.1109/JSYST.2013.2244971
%K privacy.
%K privacy
%K DaaS
%K composition
%K service
%K adaptation
%Z IEEE
%Z Computer Science [cs]/WebJournal articles
%X The composition of DaaS (Data-as-a-Service) services is a powerful solution for building value-added applications on top of existing ones. However, privacy concerns are still among the key challenges that keep hampering DaaS composition. Indeed services may follow different, conflicting privacy specifications with respect to the data they use and provide. In this paper, we propose an approach for privacyaware composition of DaaS services. Our approach allows specifying privacy requirements and policies and verifying the compatibility of services involved in a composition. We propose an adaptation protocol that makes it possible to reconcile the privacy specifications of services when incompatibilities arise in a composition. We validate the applicability of our proposal through a set of experiments.
%G English
%2 https://hal.archives-ouvertes.fr/hal-00814394/document
%2 https://hal.archives-ouvertes.fr/hal-00814394/file/How_to_Enhance_Privacy_within_DaaS_service_Composition_HAL.pdf
%L hal-00814394
%U https://hal.archives-ouvertes.fr/hal-00814394
%~ CNRS
%~ UNIV-LYON2
%~ EC-LYON
%~ LIRIS
%~ MAGELLAN
%~ INSA-LYON
%~ UNIV-LYON1
%~ UNIV-LYON3
%~ INSA-GROUPE
%~ LABEXIMU
%~ UDL
%~ UNIV-LYON

%0 Unpublished work
%T A Resource Oriented Architecture to Handle Data Volume Diversity
%+ Service Oriented Computing (SOC)
%A DE VETTOR, Pierre
%A Mrissa, Michael
%A Benslimane, Djamal
%9 Forum Caise
%P 161--168
%8 2015-06-10
%D 2015
%K resource oriented architecture
%K data integration
%K data semantics
%K smart data
%Z Computer Science [cs]
%Z Computer Science [cs]/WebOther publications
%X Providing quality-aware techniques for reusing data available on the Web is a major concern for today's organizations. High quality data that offers higher added-value to the stakeholders is called smart data. Smart data can be obtained by combining data coming from diverse data sources on the Web such as Web APIs, SPARQL endpoints, Web pages and so on. Generating smart data involves complex data processing tasks, typically realized manually or in a static way in current organizations, with the help of statically configured workflows. In addition , despite the recent advances in this field, transfering large amounts of data to be processed still remains a tedious task due to unreliable transfer conditions or transfer rate/latency problems. In this paper, we propose an adaptive architecture to generate smart data, and focus on a solution to handle volume diversity during data processing. Our approach aims at maintaining good response time performance upon user request. It relies on the use of RESTful resources and remote code execution over temporary data storage where business data is cached. Each resource involved in data processing accesses the storage to process data on-site.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01169292/document
%2 https://hal.archives-ouvertes.fr/hal-01169292/file/DeVettor_ResourceOrientedArchitectureHandleDataVolumeDiversity.pdf
%L hal-01169292
%U https://hal.archives-ouvertes.fr/hal-01169292
%~ CNRS
%~ EC-LYON
%~ LIRIS
%~ UNIV-LYON2
%~ INSA-LYON
%~ UNIV-LYON1
%~ INSA-GROUPE
%~ UNIV-PAU
%~ LABEXIMU
%~ UDL
%~ UNIV-LYON

%0 Conference Proceedings
%T PREDICAT: a semantic service-oriented platform for data interoperability and linking in earth observation and disaster prediction
%+ Multimedia, InfoRmation systems and Advanced Computing Laboratory (MIRACL)
%+ Service Oriented Computing (SOC)
%+ Franche-Comté Électronique Mécanique, Thermique et Optique - Sciences et Technologies (UMR 6174) (FEMTO-ST)
%+ Laboratoire de recherche en Génie Logiciel, Applications distribuées, Systèmes décisionnels et Imagerie intelligente [Manouba] (RIADI)
%+ Laboratoire Génie de Production (LGP)
%+ InnoRenew CoE
%A Masmoudi, Maroua
%A Taktak, Hela
%A Ben Abdallah Ben Lamine, Sana
%A Boukadi, Khouloud
%A Karray, Mohamed Hedi
%A Baazaoui Zghal, Hajer
%A Archimède, Bernard
%A Mrissa, Michael
%A Ghedira, Chirine
%< avec comité de lecture
%( Proceedings of the SOCA 2018 : The 11th IEEE International conference on service oriented computing and applications
%B SOCA 2018 :The 11th IEEE International conference on service oriented computing and applications
%C PARIS, France
%I IEEE
%P 194-201
%8 2018-11-19
%D 2018
%R 10.1109/SOCA.2018.00035
%K Semantics
%K Interoperability
%K Meteorology
%K Earth
%K Big data
%K Ocean temperature
%K Data integration
%Z Environmental Sciences/Global Changes
%Z Engineering Sciences [physics]/OtherConference papers
%X The increasing volume of data generated by earth observation programs such as Copernicus, NOAA, and NASA Earth Data, is overwhelming. Although these programs are very costly, data usage remains limited due to lack of interoperability and data linking. In fact, multi-source and heterogeneous data exploitation could be significantly improved in different domains especially in the natural disaster prediction one. To deal with this issue, we introduce the PREDICAT project that aims at providing a semantic service-oriented platform to PREDIct natural CATastrophes. The PREDICAT platform considers (1) data access based on web service technology; (2) ontology-based interoperability for the environmental monitoring domain; (3) data integration and linking via big data techniques; (4) a prediction approach based on semantic machine learning mechanisms. The focus in this paper is to provide an overview of the PREDICAT platform architecture. A scenario explaining the operation of the platform is presented based on data provided by our collaborators, including the international intergovernmental Sahara and Sahel Observatory (OSS).
%G English
%2 https://hal.archives-ouvertes.fr/hal-01990258/document
%2 https://hal.archives-ouvertes.fr/hal-01990258/file/Karray_22778.pdf
%L hal-01990258
%U https://hal.archives-ouvertes.fr/hal-01990258
%~ CNRS
%~ LABEXIMU
%~ INSA-GROUPE
%~ LIRIS
%~ INSA-LYON
%~ FEMTO-ST
%~ UNIV-LYON1
%~ UNIV-BM-THESE
%~ ENSMM
%~ UNIV-FCOMTE
%~ LYON2
%~ UNIV-BM
%~ UNIV-LYON2
%~ EC-LYON
%~ SDE
%~ GIP-BE
%~ UDL
%~ UNIV-LYON

%0 Conference Proceedings
%T Combining configuration and query rewriting for Web service composition
%+ Base de Données (BD)
%+ Service Oriented Computing (SOC)
%A Mesmoudi, Amin
%A Mrissa, Michael
%A Hacid, Mohand-Said
%< avec comité de lecture
%Z 5113
%B IEEE International Conference on Web Services (ICWS)
%C Washington, DC, United States
%I IEEE
%P 113-120
%8 2011-07-04
%D 2011
%R 10.1109/ICWS.2011.26
%Z Computer Science [cs]Conference papers
%X In this paper, we investigate the combination of configuration and query rewriting for semantic Web service composition.Given a user query and a set of service descriptions, we rely on query rewriting to find services that implement the functionalities expressed in the user query (discovery stage). Then, we use configuration to capture dependencies between services, and to generate a set of composed Web services described as a directed acyclic graph, while maintaining validity with respect to business rules (orchestration stage).Finally, we propose a semantic ranking algorithm to rank results according to user preferences (classification stage).The techniques used in our approach take into account the semantics of concepts utilized to describe the elements (services, business rules, query and user preferences) involved in the composition process.We provide a formal approach and its implementation, together with experiments on Web services from different application domains.
%G English
%L hal-01354437
%U https://hal.archives-ouvertes.fr/hal-01354437
%~ CNRS
%~ EC-LYON
%~ UNIV-LYON2
%~ LIRIS
%~ INSA-LYON
%~ UNIV-LYON1
%~ INSA-GROUPE
%~ LABEXIMU
%~ UDL
%~ UNIV-LYON

%0 Book Section
%T Privacy-Conscious Data Mashup: Concepts, Challenges and Directions
%+ Service Oriented Computing (SOC)
%A Barhamgi, Mahmoud
%A Ghedira, Chirine
%A Tbahriti, Salah-Eddine
%A Mrissa, Michael
%A Benslimane, Djamal
%A Medjahed, Brahim
%Z 5278
%B Handbook of Research on E-Business Standards and Protocols: Documents, Data and Advanced Web Technologies
%I IGI Global
%P 316-340
%8 2012-01
%D 2012
%R 10.4018/978-1-4666-0146-8.ch016
%Z Computer Science [cs]Book sections
%X Modern enterprises across all spectra are increasingly adopting SOA-based data integration architectures to rapidly respond to transient data business needs. In this chapter, we analyze a new class of enterprise data integration application, called Data Mashup, in which data services are composed on the fly to answer new data business demands. We review the different approaches to data mashup, discuss their limitations and identify the main requirements to data mashup. We propose then a declarative data mashup approach addressing the identified requirements. Finally, we present some research directions that must be followed in order for data mashup technology to mature.
%G English
%L hal-01352938
%U https://hal.archives-ouvertes.fr/hal-01352938
%~ CNRS
%~ EC-LYON
%~ UNIV-LYON2
%~ LIRIS
%~ INSA-LYON
%~ UNIV-LYON1
%~ INSA-GROUPE
%~ LABEXIMU
%~ UDL
%~ UNIV-LYON

%0 Conference Proceedings
%T An Ontology-Based Approach for Personalized RESTful Web Service Discovery
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%A Lamine, Sana Ben Abdallah Ben
%A Zghal, Hajer Baazaoui
%A Mrissa, Michael
%A Guegan, Chirine Ghedira
%< avec comité de lecture
%B Knowledge-Based and Intelligent Information & Engineering Systems: Proceedings of the 21st International Conference KES-2017, Marseille, France, 6-8 September 2017
%C Marseille, France
%Y Jain, Lakhmi C.
%I Elsevier
%3 Procedia Computer Science
%V 112
%P 2127-2136
%8 2017-09-06
%D 2017
%R 10.1016/j.procs.2017.08.235
%Z Computer Science [cs]Conference papers
%X Web service discovery is a challenging task that has received a lot of interest in the last decade. Several approaches have been proposed, however, many limitations remain. In this paper, we focus on collaborative semantic discovery of RESTful services according to the HATEOAS principle. We propose an approach based on 1) semantic links between services; 2) user profile clustering to dynamically measure user similarity for queries in a similar context, and 3) a user profile ontology to manage users and corresponding services. A prototype has been implemented to evaluate our proposal and show its effectiveness. (C) 2017 The Authors. Published by Elsevier B.V.
%G English
%L hal-01910014
%U https://hal-univ-pau.archives-ouvertes.fr/hal-01910014
%~ UNIV-PAU
%~ LIUPPA
%~ LABEXIMU
%~ TESTUPPA2
%~ UPPA-OA

%0 Conference Paper
%F Oral 
%T HyLAR: Hybrid Location-Agnostic Reasoning
%+ Traces, Web, Education, Adaptation, Knowledge (TWEAK)
%+ Service Oriented Computing (SOC)
%A Terdjimi, Mehdi
%A Médini, Lionel
%A Mrissa, Michael
%Z ANR
%< avec comité de lecture
%B ESWC Developers Workshop 2015
%C Portoroz, Slovenia
%P 1
%8 2015-05-31
%D 2015
%K mobile reasoning
%K ubiquitous semantic web
%K client-side reasoning
%Z Computer Science [cs]/Web
%Z Computer Science [cs]/Mobile Computing
%Z Computer Science [cs]/Artificial Intelligence [cs.AI]
%Z Computer Science [cs]/Ubiquitous ComputingConference papers
%X The question of client-side reasoning is crucial to semantic web application design as client performances drastically increase. It is an opportunity for ubiquitous devices to use semantic technologies. In this paper, we propose a lightweight, modular and adaptive architecture developed in JavaScript for hybrid client/server side reasoning. We evaluate the performance of the reasoning process with different browsers, devices and network conditions, and discuss the best strategy with respect to the envisioned reasoning tasks.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01154549v2/document
%2 https://hal.archives-ouvertes.fr/hal-01154549v2/file/hylar.pdf
%L hal-01154549
%U https://hal.archives-ouvertes.fr/hal-01154549
%~ CNRS
%~ EC-LYON
%~ LIRIS
%~ INSA-LYON
%~ UNIV-LYON1
%~ INSA-GROUPE
%~ LABEXIMU
%~ UNIV-LYON2
%~ UDL
%~ UNIV-LYON
%~ ANR

%0 Conference Proceedings
%T Combining Proactive and Reactive Approaches in Smart Services for the Web of Things
%+ Laboratoire de Recherche en Informatique de Sidi Bel-Abbes (LabRI-SBA)
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%A Sekkal, Nawel
%A Benslimane, Sidi, Mohamed
%A Mrissa, Michael
%A Boudaa, Boudjemaa
%Z Part 7: Internet of Things and Decision Support Systems
%< avec comité de lecture
%( IFIP Advances in Information and Communication Technology
%B 6th IFIP International Conference on Computational Intelligence and Its Applications (CIIA)
%C Oran, Algeria
%Y Abdelmalek Amine
%Y Malek Mouhoub
%Y Otmane Ait Mohamed
%Y Bachir Djebbar
%I Springer International Publishing
%3 Computational Intelligence and Its Applications
%V AICT-522
%P 509-520
%8 2018-05-08
%D 2018
%R 10.1007/978-3-319-89743-1_44
%Z Computer Science [cs]Conference papers
%X The Web of Things (WoT), facilitates the interconnection of different types of real-world objects, integrating them into the virtual world and ensuring their interoperability through Web services. However, it remains a challenge to automate the tasks connected objects need to deal with. In this paper, we focus on the development of smart web services that automate service tasks, autonomously adapt to context changes in the object’s environment, and to users’ preferences. In this article, we propose a software framework for smart services that relies on a reactive and proactive approach to deal with context and its temporal aspects. Smart web services developed according to these principles can react to current situations and proactively anticipate an unforeseen situation in order to take the right decision.
%G English
%Z TC 5
%2 https://hal.inria.fr/hal-01913867/document
%2 https://hal.inria.fr/hal-01913867/file/467079_1_En_44_Chapter.pdf
%L hal-01913867
%U https://hal.inria.fr/hal-01913867
%~ IFIP
%~ LIUPPA
%~ UNIV-PAU
%~ IFIP-TC
%~ IFIP-TC5
%~ IFIP-LNCS
%~ IFIP-CIIA
%~ IFIP-AICT-522
%~ IFIP-AICT
%~ LABEXIMU
%~ TESTUPPA2
%~ UPPA-OA

%0 Conference Proceedings
%T HyLAR+: Improving Hybrid Location-Agnostic Reasoning with Incremental Rule-based Update
%+ Traces, Web, Education, Adaptation, Knowledge (TWEAK)
%+ Service Oriented Computing (SOC)
%A Terdjimi, Mehdi
%A Médini, Lionel
%A Mrissa, Michael
%Z ANR
%< avec comité de lecture
%( WWW '16: 25th International World Wide Web Conference Companion
%B WWW '16: 25th International World Wide Web Conference Companion
%C Montréal, Canada
%8 2016-04-11
%D 2016
%K rule-based reasoning
%K client-side reasoning
%K semantic web
%K reasoning
%Z Computer Science [cs]/Web
%Z Computer Science [cs]/Artificial Intelligence [cs.AI]
%Z Computer Science [cs]/Ubiquitous Computing
%Z Computer Science [cs]/Mobile ComputingConference papers
%X Web applications that rely on datasets of limited sizes to handle small but frequent updates and numerous queries have no simple way to define where data should be stored and processed. We propose a reasoning framework that can be integrated in Web applications and is able to perform the same reasoning tasks on both client or server sides. This framework embeds a rule-based reasoning engine that uses an algorithm relying on both incremental reasoning and named graphs. We evaluate the performance of our approach and compare the effects of incremental reasoning and named graphs in different experimental conditions. Results show that our reasoner can significantly reduce response times to INSERT and DELETE queries. During the demo we will exhibit how it can be used to perform reasoning tasks based on client-generated information and improve Web applications with location-agnostic reasoning.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01276558/document
%2 https://hal.archives-ouvertes.fr/hal-01276558/file/Demo_www2016.pdf
%L hal-01276558
%U https://hal.archives-ouvertes.fr/hal-01276558
%~ CNRS
%~ EC-LYON
%~ LIRIS
%~ INSA-LYON
%~ UNIV-LYON1
%~ LYON2
%~ INSA-GROUPE
%~ LIUPPA
%~ UNIV-PAU
%~ LABEXIMU
%~ UNIV-LYON2
%~ UDL
%~ UNIV-LYON
%~ ANR
%~ TESTUPPA2
%~ UPPA-OA

%0 Conference Proceedings
%T Automatic K-Resources Discovery for Hybrid Web Connected Environments
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%+ InnoRenew CoE
%A Kallab, Lara
%A Chbeir, Richard
%A Mrissa, Michael
%< avec comité de lecture
%B IEEE Services - ICWS 2019
%C Milan, Italy
%8 2019-07-08
%D 2019
%K Web of Things
%K Web Connected Environments
%K Dynamic Resource
%K Semantic Description
%K Automatic Discovery
%Z Computer Science [cs]/WebConference papers
%X Recently, RESTful services, designed as resources, have seen their popularity rising and have shown their potential in composing reliable Web-scale environments (Web applications, Web platforms, Web of Things (WoT), etc.). However, discovering the necessary resources for a composition is becoming more challenging. This is due to 1) the growing number of published Web-based resources, and 2) the highly dynamic nature of the WoT environment, in which smart devices, connected to the Web platform, are exposed as resources. In this paper, we propose an automatic resource discovery, applicable in hybrid Web-based environments providing static linked resources always available on the Web, and connecting dynamic resources that can be connected to and removed from the Web at different time periods. The solution presents an indexing schema that makes resource discovery process fast, especially in large-scale environments. Experiments were conducted in different environment setups to test the effective performance of our approach.
%G English
%2 https://hal.archives-ouvertes.fr/hal-02265473/document
%2 https://hal.archives-ouvertes.fr/hal-02265473/file/Automatic_K_Resources_Discovery_for_Hybrid_Web_Connected_Environments_from_23__1_%20%283%29.pdf
%L hal-02265473
%U https://hal.archives-ouvertes.fr/hal-02265473
%~ UNIV-PAU
%~ LIUPPA
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T HIT2GAP: Towards a better building energy management
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%+ Nobatek-INEF4
%A Kallab, Lara
%A Chbeir, Richard
%A Bourreau, Pierre
%A Brassier, Pascale
%A Mrissa, Michael
%< avec comité de lecture
%@ 1876-6102
%J Energy Procedia
%I Elsevier
%V 122
%P 895-900
%8 2017
%D 2017
%R 10.1016/j.egypro.2017.07.399
%K Smart Buildings
%K Web-service Composition
%K Ontological Data Model
%K Energy Efficiency
%K Web-based Framework
%Z Computer Science [cs]/WebJournal articles
%X Recent studies show that the Energy Performance Gap (EPGap), defined as the difference between the estimated and actual energy consumption of a building, is significantly high. This is due to various factors encountered in the different phases of the building life cycle, i.e., inaccuracy of the specifications used in the simulation tools during design phase, poor quality of the on-site practices conducted throughout the construction, inadequate verification of the equipment installed in the building during commissioning phase, and limited analysis of the data collected from the equipment during the operational phase. With the aim of reducing the EPGap, we present an energy management framework defined in the context of an EU-funded H2020 project, HIT2GAP1. The proposed solution provides several services from collecting heterogeneous on-site data, to advanced data analysis and visualization tools designed for the different actors of a building (e.g., building/facility/energy manager, occupants, etc.), for building energy performance optimization. In this paper, we give an overview on the proposed framework architecture and detail its different functionalities, with a particular focus on the solution Core Platform, which is in charge of orchestrating the different components, storing and structuring the data, and providing pre-processing services.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01592631/document
%2 https://hal.archives-ouvertes.fr/hal-01592631/file/2017-CISBAT.pdf
%L hal-01592631
%U https://hal.archives-ouvertes.fr/hal-01592631
%~ UNIV-PAU
%~ LIUPPA
%~ LABEXIMU
%~ TESTUPPA
%~ TESTUPPA2

%0 Conference Proceedings
%T Using Colored Petri Nets for Verifying RESTful Service Composition
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%+ Nobatek-INEF4
%A Kallab, Lara
%A Mrissa, Michael
%A Chbeir, Richard
%A Bourreau, Pierre
%< avec comité de lecture
%B OTM Confederated International Conferences "On the Move to Meaningful Internet Systems"
%C Rhodes, Greece
%3 On the Move to Meaningful Internet Systems. OTM 2017 Conferences Confederated International Conferences: CoopIS, C&TC, and ODBASE 2017, Rhodes, Greece, October 23-27, 2017, Proceedings,
%V 10573
%P 505-523
%8 2017-10-25
%D 2017
%R 10.1007/978-3-319-69462-7_32
%K REST architecture
%K Resource Composition
%K Colored Petri Nets
%K Composition properties verification
%Z Computer Science [cs]/WebConference papers
%X RESTful services are an attractive technology for designing and developing web-based applications, as they facilitate reuse, interoperability,and loosely coupled interaction with generic clients (typically web browsers). Building RESTful service composition has received much interest to satisfy complex user requirements. However, verifying the correctness of a composition remains a tedious task. In this paper, we present a formal approach based on Colored Petri Nets (CPNs) to verify RESTful service composition. First, we show how CPNs are utilized for modeling the behavior of resources and their composition. Then, we present how this formal model can be used to verify relevant composition behavior properties. Our solution is illustrated with a scenario built upon an energy management web framework developed within the HIT2GAP H2020 European project (http://www.hit2gap.eu/)
%G English
%2 https://hal.archives-ouvertes.fr/hal-01592920/document
%2 https://hal.archives-ouvertes.fr/hal-01592920/file/Using%20Colored%20Petri-nets%20for%20Verifying%20RESTful%20Service%20Composition%20%281%29.pdf
%L hal-01592920
%U https://hal.archives-ouvertes.fr/hal-01592920
%~ UNIV-PAU
%~ LIUPPA
%~ LABEXIMU
%~ TESTUPPA2
%~ UPPA-OA

%0 Unpublished work
%T Leveraging cyber-physical objects through the concept of avatar
%+ Traces, Web, Education, Adaptation, Knowledge (TWEAK)
%+ Distribution, Recherche d'Information et Mobilité (DRIM)
%+ Service Oriented Computing (SOC)
%+ Laboratoire de Conception et d'Intégration des Systèmes (LCIS)
%+ Disruption-Tolerant Networking & Computing (CASA)
%+ Génération Robots
%A Médini, Lionel
%A Mrissa, Michael
%A Jamont, Jean-Paul
%A Le Sommer, Nicolas
%A Laplace, Jérôme
%9 W3C Workshop on the Web of Things
%8 2014-06-26
%D 2014
%K Internet of Thing
%K Web of Thing
%Z Computer Science [cs]/Ubiquitous Computing
%Z Computer Science [cs]/Software Engineering [cs.SE]
%Z Computer Science [cs]/Web
%Z Computer Science [cs]/Robotics [cs.RO]
%Z Computer Science [cs]/Emerging Technologies [cs.ET]Other publications
%X Connected objects, assistance robots and co-workers (co-Bots) will be a major market in a near future. The Web of Things (WoT) will lead to the development of advanced applications connecting heterogeneous objects. This is an opportunity for hardware and software vendors to occupy a strategic position on new marketplaces such as those envisioned by the Compose project 1 . Such applications shall attract customers with smartinteraction possibilities and seamless deployment on various ranges of devices. The ASAWoO (Adaptive Supervision of Avatar/Object Links for the Web of Objects) project aims at building an architecture to provide users with understandable functionalities exposed as WoT applications. This architecture is designed to fully integrate physical appliances into the Web and to enable collaboration between heterogeneous objects, from the basic sensor to the complex robot.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01074984/document
%2 https://hal.archives-ouvertes.fr/hal-01074984/file/medini.pdf
%L hal-01074984
%U https://hal.archives-ouvertes.fr/hal-01074984
%~ LIRIS
%~ IRISA_SET
%~ CNRS
%~ UNIV-UBS
%~ EC-LYON
%~ IRISA
%~ IRISA-D2
%~ INRIA
%~ UNIV-RENNES1
%~ INSTITUT-TELECOM
%~ UNIV-PMF_GRENOBLE
%~ INPG
%~ LCIS
%~ UR1-MATH-STIC
%~ UNIV-LYON2
%~ UR1-HAL
%~ UR1-UFR-ISTIC
%~ INSA-LYON
%~ UNIV-LYON1
%~ TEST-UNIV-RENNES
%~ TEST-UR-CSS
%~ UNIV-RENNES
%~ INRIA-AUT
%~ INSA-GROUPE
%~ LABEXIMU
%~ UDL
%~ UNIV-LYON
%~ INSTITUTS-TELECOM
%~ IRISA_UBS
%~ UBS
%~ INSA-RENNES
%~ UGA
%~ ANR

