%0 Journal Article
%T The existence of weak solutions to immiscible compressible two-phase flow in porous media: The case of fields with different rock-types
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Narvik University College
%A Amaziane, Brahim
%A Pankratov, Leonid
%A Piatnitski, Andrey
%< avec comité de lecture
%@ 1531-3492
%J Discrete and Continuous Dynamical Systems - Series B
%I American Institute of Mathematical Sciences
%V 18
%N 5
%P 1217-1251
%8 2013
%D 2013
%R 10.3934/dcdsb.2013.18.1217
%Z Mathematics [math]/Numerical Analysis [math.NA]Journal articles
%X We study a model describing immiscible, compressible two-phase flow, such as water-gas, through heterogeneous porous media taking into account capillary and gravity effects. We will consider a domain made up of several zones with different characteristics: porosity, absolute permeability, relative permeabilities and capillary pressure curves. This process can be formulated as a coupled system of partial differential equations which includes a nonlinear parabolic pressure equation and a nonlinear degenerate diffusionconvection saturation equation. Moreover the transmission conditions are nonlinear and the saturation is discontinuous at interfaces separating different media. There are two kinds of degeneracy in the studied system: the first one is the degeneracy of the capillary diffusion term in the saturation equation, and the second one appears in the evolution term of the pressure equation. Under some realistic assumptions on the data, we show the existence of weak solutions with the help of an appropriate regularization and a time discretization. We use suitable test functions to obtain a priori estimates. We prove a new compactness result in order to pass to the limit in nonlinear terms. This passage to the limit is nontrivial due to the degeneracy of the system.
%G English
%L hal-00867084
%U https://hal.archives-ouvertes.fr/hal-00867084
%~ CNRS
%~ LMA-PAU
%~ INSMI
%~ TDS-MACS
%~ UNIV-PAU
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Adaptive Mesh Refinement for a Finite Volume Method for Flow and Transport of Radionuclides in Heterogeneous Porous Media
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Institut de Radioprotection et de Sûreté Nucléaire (IRSN)
%+ Université Ibn Tofaïl (UIT)
%A Amaziane, Brahim
%A Bourgeois, Marc
%A El Fatini, Mohamed
%< avec comité de lecture
%@ 1294-4475
%J Oil & Gas Science and Technology - Revue d'IFP Energies nouvelles
%I Institut Français du Pétrole
%V 69
%N 4
%P 687-699
%8 2014
%D 2014
%R 10.2516/ogst/2013176
%Z Physics [physics]Journal articles
%X In this paper, we consider adaptive numerical simulation of miscible displacement problems in porous media, which are modeled by single phase flow equations. A vertex-centred finite volume method is employed to discretize the coupled system: the Darcy flow equation and the diffusion-convection concentration equation. The convection term is approximated with a Godunov scheme over the dual finite volume mesh, whereas the diffusion-dispersion term is discretized by piecewise linear conforming finite elements. We introduce two kinds of indicators, both of them of residual type. The first one is related to time discretization and is local with respect to the time discretization: thus, at each time, it provides an appropriate information for the choice of the next time step. The second is related to space discretization and is local with respect to both the time and space variable and the idea is that at each time it is an efficient tool for mesh adaptivity. An error estimation procedure evaluates where additional refinement is needed and grid generation procedures dynamically create or remove fine-grid patches as resolution requirements change. The method was implemented in the software MELODIE, developed by the French Institute for Radiological Protection and Nuclear Safety (IRSN, Institut de Radioprotection et de Sûreté Nucléaire). The algorithm is then used to simulate the evolution of radionuclide migration from the waste packages through a heterogeneous disposal, demonstrating its capability to capture complex behavior of the resulting flow.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01933397/document
%2 https://hal.archives-ouvertes.fr/hal-01933397/file/ogst130024.pdf
%L hal-01933397
%U https://hal.archives-ouvertes.fr/hal-01933397
%~ CNRS
%~ UNIV-PAU
%~ LMA-PAU
%~ OGST
%~ IRSN
%~ TESTUPPA2
%~ UPPA-OA

%0 Conference Paper
%F Oral 
%T Numerical modeling of coupled two-phase multicomponent flow with reactive geochemical transport in porous media
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Université de Pau et des Pays de l'Adour (UPPA)
%A Ahusborde, Etienne
%A Amaziane, Brahim
%A El Ossmani, Mustapha
%A Poncet, Philippe
%< avec comité de lecture
%Z Transport dispersifs et réactifs
%B 13èmes Journéess d'études des Milieux Poreux 2016
%C Anglet, France
%8 2016-10-11
%D 2016
%K Reactive transport
%K Code coupling
%K phase multicomponent flow
%K Two
%Z Physics [physics]/Mechanics [physics]Conference papers
%X Numerical modeling of coupled two-phase multicomponent flow with reactive geochemical transport in porous media
%G French
%2 https://hal.archives-ouvertes.fr/hal-01394471/document
%2 https://hal.archives-ouvertes.fr/hal-01394471/file/AHUSBORDE_JEMP2016.pdf
%L hal-01394471
%U https://hal.archives-ouvertes.fr/hal-01394471
%~ JEMP2016
%~ CNRS
%~ UNIV-PAU
%~ LMA-PAU
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Homogenization of immiscible compressible two-phase flow in highly heterogeneous porous media with discontinuous capillary pressures
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Narvik University College
%A Amaziane, Brahim
%A Pankratov, Leonid
%A Piatnitski, Andrey
%Z cited By (since 1996)0
%< avec comité de lecture
%@ 0218-2025
%J Mathematical Models and Methods in Applied Sciences
%I World Scientific Publishing
%V 24
%N 7
%P 1421-1451
%8 2014
%D 2014
%R 10.1142/S0218202514500055
%Z Mathematics [math]/Numerical Analysis [math.NA]Journal articles
%X This paper presents a study of immiscible compressible two-phase, such as water and gas, flow through highly heterogeneous porous media with periodic microstructure. Such models appear in gas migration through engineered and geological barriers for a deep repository for radioactive waste. We will consider a domain made up of several zones with different characteristics: porosity, absolute permeability, relative permeabilities and capillary pressure curves. Consequently, the model involves highly oscillatory characteristics and internal nonlinear interface conditions. The microscopic model is written in terms of the phase formulation, i.e. where the wetting (water) saturation phase and the non-wetting (gas) pressure phase are primary unknowns. This formulation leads to a coupled system consisting of a nonlinear parabolic equation for the gas pressure and a nonlinear degenerate parabolic diffusion-convection equation for the water saturation, subject to appropriate transmission, boundary and initial conditions. The major difficulties related to this model are in the nonlinear degenerate structure of the equations, as well as in the coupling in the system. Moreover, the transmission conditions are nonlinear and the saturation is discontinuous at interfaces separating different media. Under some realistic assumptions on the data, we obtain a nonlinear homogenized coupled system with effective coefficients which are computed via a cell problem and give a rigorous mathematical derivation of the upscaled model by means of the two-scale convergence. © 2014 World Scientific Publishing Company.
%G English
%L hal-00993678
%U https://hal.archives-ouvertes.fr/hal-00993678
%~ CNRS
%~ LMA-PAU
%~ INSMI
%~ TDS-MACS
%~ UNIV-PAU
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Numerical Modeling and Simulation of Fully Coupled Processes of Reactive Multiphase Flow in Porous Media
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Université de Moulay Ismail (UMI)
%A Ahusborde, Etienne
%A Amaziane, Brahim
%A El Ossmani, Mustapha
%A Id Moulay, M
%< avec comité de lecture
%@ 1006-6837
%J Journal of Mathematical Study
%I Global Science Press
%V 52
%N 4
%P 359-377
%8 2019
%D 2019
%R 10.4208/jms.v52n4.19.01
%K permeability-porosity changes
%K coupled PDE and ODE
%K nonlinear degenerate system
%K finite volume
%K fully implicit
%K fully coupled
%K porous media
%K reactive transport
%K Multiphase multicomponent flow
%Z AMS: 76S05, 65M08, 76T10, 74F25
%Z Mathematics [math]/Mathematical Physics [math-ph]
%Z Physics [physics]/Mechanics [physics]/Mechanics of the fluids [physics.class-ph]Journal articles
%X In this paper, we consider a finite volume approach for modelling multiphase flow coupled to geochemistry in porous media. Reactive multiphase flows are modelled by a highly nonlinear system of degenerate partial differential equations coupled with algebraic and ordinary differential equations. We propose a fully implicit scheme using a direct substitution approach (DSA) implemented in the framework of the parallel open-source platform DuMu X. We focus on the particular case where porosity changes due to mineral dissolution/precipitation are taken into account. This alteration of the porosity can have significant effects on the permeability and the tortuosity. The accuracy and effectiveness of the implementation of permeability/porosity and tortuosity/porosity relationships related to mineral dissolution/precipitation for single-phase and two-phase flows are demonstrated through numerical simulations.
%G English
%2 https://hal.archives-ouvertes.fr/hal-02387032/document
%2 https://hal.archives-ouvertes.fr/hal-02387032/file/AHUSBORDE_JMS.pdf
%L hal-02387032
%U https://hal.archives-ouvertes.fr/hal-02387032
%~ UNIV-PAU
%~ LMA-PAU
%~ INSMI
%~ CNRS
%~ TDS-MACS
%~ TESTUPPA2
%~ UPPA-OA

%0 Report
%T State Of the Art Report in the fields of numerical analysis and scientific computing. Final version as of 16/02/2020 deliverable D4.1 of the HORIZON 2020 project EURAD.
%T European Joint Programme on Radioactive Waste Management
%+ Reliable numerical approximations of dissipative systems (RAPSODI )
%A Ahusborde, Etienne
%A Amaziane, Brahim
%A Baksay, Attila
%A Bator, G.
%A Becker, D.
%A Bednar, A.
%A Beres, M.
%A Blaheta, R.
%A Böhti, Z.
%A Bracke, G.
%A Brazda, L.
%A Brendler, V.
%A Brenner, Konstantin
%A Brezina, J.
%A Cancès, Clément
%A Chainais-Hillairet, Claire
%A Chave, Florent
%A Claret, Francis
%A Domesova, S.
%A Havlova, Vaclava
%A Hokr, M.
%A Horak, D.
%A Jacques, D.
%A Jankovsky, F.
%A Kazymyrenko, K.
%A Kolditz, O.
%A Koudelka, T.
%A Kovacs, T.
%A Krejci, T.
%A Kruis, J.
%A Laloy, E.
%A Landa, J.
%A Lipping, T.
%A Lukin, D.
%A Mašín, David
%A Masson, Roland
%A Meeussen, J. C. L.
%A Mollaali, M.
%A Mon, A.
%A Montenegro, L.
%A Montoya, Vanessa
%A Pépin, Guillaume
%A Poonoosamy, J.
%A Prasianakis, N
%A Saâdi, Z.
%A Samper, J.
%A Scaringi, Gianvito
%A Sochala, Pierre
%A Tournassat, Christophe
%A Yoshioka, K.
%A Yuankai, Y.
%I EC Grant agreement no: 847593
%8 2020
%D 2020
%Z Mathematics [math]/Numerical Analysis [math.NA]
%Z Engineering Sciences [physics]
%Z Engineering Sciences [physics]/Reactive fluid environment
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of materials [physics.class-ph]Reports
%X Document information Project Acronym EURAD Project Title European Joint Programme on Radioactive Waste Management Project Type European Joint Programme (EJP) EC grant agreement No. 847593 Project starting / end date 1 st June 2019-30 May 2024 Work Package No. 4 Work Package Title Development and Improvement Of NUmerical methods and Tools for modelling coupled processes Work Package Acronym DONUT Deliverable No. 4.1
%G English
%2 https://hal.archives-ouvertes.fr/hal-03165686/document
%2 https://hal.archives-ouvertes.fr/hal-03165686/file/EURAD-D4.1SotADONUT.pdf
%L hal-03165686
%U https://hal.archives-ouvertes.fr/hal-03165686
%~ INSMI
%~ TDS-MACS
%~ UNIV-LILLE
%~ CNRS
%~ LARA
%~ INRIA_TEST
%~ INRIA-LILLE
%~ INRIA

%0 Journal Article
%T Homogenized Model of Two-Phase Flow with Local Nonequilibrium in Double Porosity Media
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Laboratoire Énergies et Mécanique Théorique et Appliquée (LEMTA )
%A Amaziane, Brahim
%A Panfilov, Mikhail
%A Pankratov, Leonid
%Z ACL
%< avec comité de lecture
%@ 1687-9120
%J Advances in Mathematical Physics
%I Hindawi Publishing Corporation
%V 2016
%P 1-13
%8 2016
%D 2016
%R 10.1155/2016/3058710
%Z Mathematics [math]
%Z Mathematics [math]/Classical Analysis and ODEs [math.CA]
%Z Computer Science [cs]/Discrete Mathematics [cs.DM]
%Z Mathematics [math]/Analysis of PDEs [math.AP]
%Z Mathematics [math]/Differential Geometry [math.DG]
%Z Mathematics [math]/Numerical Analysis [math.NA]Journal articles
%X We consider two-phase flow in a heterogeneous porous medium with highly permeable fractures and low permeable periodic blocks. The flow in the blocks is assumed to be in local capillary disequilibrium and described by Barenblatt's relaxation relationships for the relative permeability and capillary pressure. It is shown that the homogenization of such equations leads to a new macro-scopic model that includes two kinds of long-memory effects: the mass transfer between the blocks and fractures and the memory caused by the microscopic Barenblatt disequilibrium. We have obtained a general relationship for the double nonequilibrium capillary pressure which represents great interest for applications. Due to the nonlinear coupling and the nonlocality in time, the macroscopic model remains incompletely homogenized in general case. The completely homogenized model was obtained for two different regimes. The first case corresponds to a linearized flow in the blocks. In the second case, we assume a low contrast in the block-fracture permeability. Numerical results for the two-dimensional problem are presented for two test cases to demonstrate the effectiveness of the methodology.
%G English
%L hal-01902189
%U https://hal.archives-ouvertes.fr/hal-01902189
%~ LMA-PAU
%~ UNIV-PAU
%~ CNRS
%~ INSMI
%~ TDS-MACS
%~ UNIV-LORRAINE
%~ LEMTA-UL
%~ TESTUPPA2
%~ UPPA-OA

%0 Thesis
%T Homogenization and Numerical Modeling of Flow and Transport in Heterogeneous Porous Media. Applications to Energy and Environmental Studies.
%+ Laboratoire de Mathématiques Appliquées
%A Amaziane, Brahim
%I Université de Pau et des Pays de l'Adour
%Y Amirat Youcef
%8 2005-07-06
%D 2005
%K Numerical Homogenization
%K Porous Media
%K Finite Volume
%K Multiphase Flow
%K Homogénéisation Numérique
%K Milieux Poreux
%K Volumes Finis
%K Ecoulements Multiphasiques
%Z Mathematics [math]Habilitation à diriger des recherches
%X Single and multiphase flow and transport processes in heterogeneous porous media are involved in a wide variety of engineering applications, such as oil recovery, radioactive nuclear waste and groundwater remediation. In this work, we study flow and transport through heterogeneous porous media using homogenization methods and numerical modeling. We are concerned with approximating effective coefficients for such problems using conforming, mixed finite elements and finite volume methods and their implementation. Numerical methods have been developed for the simulation of miscible or immiscible two-phase flow in heterogeneous porous media. Three topics are investigated. The first one deals with the homogenization of single and multiphase flow in porous media. The convergence results are obtained by mean of the two-scale convergence and/or L-convergence. Each homogenization method leads to the definition of a global or effective model of a homogeneous medium defined by the computed effective coefficients. Homogenization methods allow the determination of these effective coefficients from knowledge of the geometrical structure of a basic cell and its heterogeneities by solving appropriate local problems. The technique is based on numeric. We assume that data given on a fine grid fully represents the important physical scales and that a practical computational grid must be somewhat coarser. In the homogenization methods described and implemented in this work we use conforming, mixed finite elements and FV methods to compute approximate solutions of the local problems used in the calculation of the effective coefficients. We have developed a user friendly computational tool, Homogenizer++, for the computation of effective parameters. The platform Homogenizer++ is based on Object Oriented Programming approach. The second topic concerns numerical methods for miscible or immiscible two-phase flow in porous media. The mathematical models used to describe these fluid flow processes are coupled system of partial differential equations. The miscible model under consideration includes an elliptic pressure-velocity equation coupled to a linear convection-diffusion-reaction concentration equation. While the immiscible model is described by an elliptic pressure-velocity equation coupled to a nonlinear, degenerate, convection-diffusion saturation equation. For each model, it is shown that the scheme satisfies a discrete maximum principle. We derive $L^\infty$ and BV estimates under an appropriate CFL condition. Then we prove the convergence of the approximate solutions to a weak solution of the coupled system. Several numerical simulations prove the efficiency of these schemes. A posteriori error estimation for a finite volume scheme on anisotropic meshes for Darcy equation has been derived. The scheme is a analyzed theoretically and numerically. Numerical simulations underline the applicability of the scheme in adaptive computations. Finally, we present some numerical methods applied to groundwater flow problems. These include a meshless method, genetic algorithms, and stochastic boundary element algorithms. A meshless method based on radial basis functions is coupled with genetic algorithms for parameter identification to a diffusion equation with some specific boundary conditions describing the groundwater fluctuation in a leaky confined aquifer system near open tidal water. Then a stochastic boundary element method coupled to a genetic algorithm is employed for the optimization of groundwater pumping in coastal aquifers under the threat of saltwater intrusion. Numerical examples of deterministic and stochastic problems are provided to demonstrate the feasibility of the proposed schemes.
%X Les travaux de recherche présentés dans ce mémoire portent sur des méthodes d'homogénéisation et d'approximation numérique pour des écoulements mono ou multiphasiques en milieux poreux hétérogènes. Les applications visées proviennent des problèmes de l'ingénierie pétrolière, la gestion des déchets radioactifs et la gestion des ressources en eau souterraines. On s'intéresse à des méthodes numériques pour le calcul des coefficients effectifs obtenus par des méthodes asymptotiques de mise à l'échelle, à des méthodes d'éléments finis mixtes, à des méthodes de volumes finis et à leur implémentation. Des méthodes numériques ont été développées pour la simulation des écoulements miscibles ou immiscibles en milieux poreux hétérogènes. Trois thèmes sont abordés. Le premier traite de l'homogénéisation pour des écoulements mono ou multiphasiques en milieux poreux. Les résultats de convergence obtenus sont établis à l'aide de la convergence à deux échelles et/ou la L-convergence. Le calcul des paramètres effectifs nécessite la résolution de problèmes locaux sur une cellule de base. Les méthodes numériques utilisées sont de type éléments finis conformes, éléments finis mixtes et volumes finis. Nous avons développé une plate-forme (Homogenizer++), en Java, de calcul de paramètres effectifs. Homogenizer++ est basée sur une Interface Homme Machine conviviale et utilisée comme un pré-processing à des simulations numériques d'écoulements en milieux poreux hétérogènes. Le deuxième thème porte sur l'approximation numérique de systèmes d'écoulements diphasiques miscibles ou immiscibles en milieux poreux. Le modèle miscible fait intervenir une équation elliptique couplée à une équation de diffusion-convection-réaction linéaire. Tandis que le modèle immiscible fait intervenir une équation elliptique couplée à une équation de diffusion-convection nonlinéaire et dégénérée. On utilise une méthode d'éléments finis mixtes pour l'approximation de l'équation elliptique combinée à un schéma volumes finis pour l'équation de diffusion-convection. Pour chaque système, on montre que le schéma est $L^\infty$ et BV stables, sous une condition CFL, et satisfait le principe du maximum discret. Ensuite, on établit des résultats de convergence vers la solution faible du problème. Les simulations numériques réalisées confirment l'efficacité des schémas numériques proposés. Un estimateur a posteriori d'un schéma volume finis pour l'équation de Darcy a été développé pour des maillages anisotropiques. On montre théoriquement et numériquement l'efficacité de cette méthode d'adaptation de maillage. Enfin le dernier thème concerne des méthodes d'approximation numérique pour des problèmes de ressources en eau souterraines. Une méthode sans maillage couplée à un algorithme génétique a été développée et implémentée pour une équation de diffusion modélisant un écoulement monophasique en milieux poreux. Puis on montre numériquement l'efficacité d'une méthode combinant les éléments frontières et un algorithme génétique pour un problème d'intrusion d'eau marine dans les nappes aquifères.
%G English
%2 https://tel.archives-ouvertes.fr/tel-00010339/document
%2 https://tel.archives-ouvertes.fr/tel-00010339/file/tel-00010339.pdf
%L tel-00010339
%U https://tel.archives-ouvertes.fr/tel-00010339
%~ INSMI
%~ UNIV-PAU
%~ UNIV-TLSE3
%~ TESTUPPA
%~ TESTUPPA2

%0 Journal Article
%T Improvement of numerical approximation of coupled multiphase multicomponent flow with reactive geochemical transport in porous media
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Université de Moulay Ismail (UMI)
%A Ahusborde, Etienne
%A Amaziane, Brahim
%A El Ossmani, Mustapha
%< avec comité de lecture
%@ 1294-4475
%J Oil & Gas Science and Technology - Revue d'IFP Energies nouvelles
%I Institut Français du Pétrole
%V 73
%P 73
%8 2018
%D 2018
%R 10.2516/ogst/2018033
%Z Physics [physics]Journal articles
%X In this paper, we consider a parallel finite volume algorithm for modeling complex processes in porous media that include multiphase flow and geochemical interactions. Coupled flow and reactive transport phenomena often occur in a wide range of subsurface systems such as hydrocarbon reservoir production, groundwater management, carbon dioxide sequestration, nuclear waste repository or geothermal energy production. This work aims to develop and implement a parallel code coupling approach for non-isothermal multiphase multicomponent flow and reactive transport simulation in the framework of the parallel open-source platform DuMuX. Modeling such problems leads to a highly nonlinear coupled system of degenerate partial differential equations to algebraic or ordinary differential equations requiring special numerical treatment. We propose a sequential fully implicit scheme solving firstly a multiphase compositional flow problem and then a Direct Substitution Approach (DSA) is used to solve the reactive transport problem. Both subsystems are discretized by a fully implicit cell-centred finite volume scheme and then an efficient sequential coupling has been implemented in DuMuX. We focus on the stability and robustness of the coupling process and the numerical benefits of the DSA approach. Parallelization is carried out using the DUNE parallel library package based on MPI providing high parallel efficiency and allowing simulations with several tens of millions of degrees of freedom to be carried out, ideal for large-scale field applications involving multicomponent chemistry. As we deal with complex codes, we have tested and demonstrated the correctness of the implemented software by benchmarking, including the MoMaS reactive transport benchmark, and comparison to existing simulations in the literature. The accuracy and effectiveness of the approach is demonstrated through 2D and 3D numerical simulations. Parallel scalability is investigated for 3D simulations with different grid resolutions. Numerical results for long-term fate of injected CO2 for geological storage are presented. The numerical results have demonstrated that this approach yields physically realistic flow fields in highly heterogeneous media and showed that this approach performs significantly better than the Sequential Iterative Approach (SIA).
%G English
%2 https://hal.archives-ouvertes.fr/hal-01952153/document
%2 https://hal.archives-ouvertes.fr/hal-01952153/file/ogst180053.pdf
%L hal-01952153
%U https://hal.archives-ouvertes.fr/hal-01952153
%~ CNRS
%~ UNIV-PAU
%~ LMA-PAU
%~ OGST
%~ TESTUPPA2
%~ UPPA-OA

