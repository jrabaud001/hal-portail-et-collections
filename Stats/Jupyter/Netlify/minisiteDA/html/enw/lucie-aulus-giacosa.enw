%0 Journal Article
%T Tree mortality in the dynamics and management of uneven-aged Norway spruce stands in southern Finland
%+ Natural Resources Institute Finland (LUKE)
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%A Valkonen, Sauli
%A Aulus, Lucie
%A Heikkinen, Juha
%Z Natural Resources Institute Finland (Luke)
%< avec comité de lecture
%@ 1612-4669
%J European Journal of Forest Research
%I Springer Verlag
%V 139
%N 6
%P 989-998
%8 2020
%D 2020
%R 10.1007/s10342-020-01301-8
%K Stand dynamics
%K Picea abies
%K Mortality
%K Silviculture
%K Stand structure
%K Uneven-aged management
%Z Environmental Sciences/Biodiversity and Ecology
%Z Environmental SciencesJournal articles
%X This study focused on tree mortality in spruce-dominated stands managed using the single-tree selection method in southern Finland. Together with regeneration and tree growth, mortality is one of the basic elements of the stand structure and dynamics in selection stands. The study was based on data acquired from a set of 20 permanent experimental plots monitored with repeated measurements for 20 years. The average mortality in the number of stems (N) was 4.45 trees ha −1 a −1 , in basal area (G) 0.07 m 2 ha −1 a −1 , and in stemwood volume (V) 0.56 m 3 ha −1 a −1. In relative terms it was 0.50% of N, 0.30% of G and 0.27% of V, respectively. Wind and snow were the most common causes of mortality, while deaths by biotic causes (mammals, insects, pathogens) were extremely rare. Some 6-10% of the total loss in the number of stems and volume was attributable to the loss or removal of trees that sustained serious damage in harvesting. Most of the mortality occurred in the smallest diameter classes of up to 20 cm. Such a high mortality among small trees can have an adverse influence on the sustainability of selection structures if not successfully checked in harvesting and management.
%G English
%2 https://hal.inrae.fr/hal-03162687/document
%2 https://hal.inrae.fr/hal-03162687/file/2020_Valkonen_EuropeanJForestResearch.pdf
%L hal-03162687
%U https://hal.inrae.fr/hal-03162687
%~ INRAE

%0 Conference Proceedings
%T Evolution of anadromy and its impact on invasion dynamics: the case of long term monitored introduced brown trout (Salmo trutta L.) in the Kerguelen Islands
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%A Aulus, Lucie
%< avec comité de lecture
%B Séminaire Zone Atelier Antarctique ZATA
%C Saint Pée sur Nivelle, France
%P np
%8 2018-10-02
%D 2018
%K introduced species
%K sea trout
%K salmo trutta
%K espèce introduite
%K migration anadrome
%K kerguelen
%K dynamique de colonisation
%Z Environmental Sciences/Biodiversity and EcologyConference papers
%G English
%2 https://hal.archives-ouvertes.fr/hal-02154526/document
%2 https://hal.archives-ouvertes.fr/hal-02154526/file/2018_Aulus_SeminaireZATAStPee_Oral_1.pptx
%L hal-02154526
%U https://hal.archives-ouvertes.fr/hal-02154526
%~ INRAE
%~ TAAF
%~ UNIV-PAU
%~ INRA
%~ SDE
%~ GIP-BE
%~ ECOBIOP
%~ AGREENIUM
%~ TESTUPPA2
%~ UPPA-OA
%~ COLISA

%0 Conference Proceedings
%T Spatio-temporal evolution of life history traits related to dispersal, Brown trout colonization of the sub-antarctic Kerguelen islands
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%A Aulus Giacosa, Lucie
%< avec comité de lecture
%B Journées de l'Ecole Doctorale
%C Pau, France
%P np
%8 2019-06-20
%D 2019
%K sea trout
%K introduced species
%K dissemination
%K salmo trutta
%K espèce introduite
%K kerguelen
%K colonisation
%K dispersion
%K trait de vie
%Z Environmental Sciences/Biodiversity and EcologyConference papers
%X Spatio-temporal evolution of life history traits related to dispersal, Brown trout colonization of the sub-antarctic Kerguelen islands. Journées de l'Ecole Doctorale
%G English
%2 https://hal.archives-ouvertes.fr/hal-02415000/document
%2 https://hal.archives-ouvertes.fr/hal-02415000/file/2019_Aulus_JED_Poster_1.pdf
%L hal-02415000
%U https://hal.archives-ouvertes.fr/hal-02415000
%~ ECOBIOP
%~ INRAE
%~ GIP-BE
%~ SDE
%~ INRA
%~ UNIV-PAU
%~ TAAF
%~ AGREENIUM
%~ TESTUPPA2
%~ UPPA-OA
%~ COLISA

%0 Conference Proceedings
%T Growth models and estimation of migratory reaction norm for invasive brown trout (Salmo trutta L.) in Kerguelen Islands
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%A Aulus, Lucie
%A VIGNON, Matthias
%A Buoro, Mathieu
%A Gaudin, Philippe
%A Gueraud, Francois
%A Aymes, Jean-Christophe
%Z résumé
%< avec comité de lecture
%B International Long Term Ecological Research Network (ILTER) & Zones Ateliers & Critical Zone Observatory Networks (LTER-France) Joint conference
%C Nantes, France
%P np
%8 2017-10-02
%D 2017
%K invasion
%K growth
%K introduced species
%K reaction norm
%K life history trait
%K sea trout
%K réchauffement climatique
%K invasion biologique
%K croissance
%K kerguelen
%K migration
%K espèce introduite
%K salmo trutta
%Z Environmental Sciences/Biodiversity and Ecology
%Z Environmental Sciences/Global ChangesConference papers
%G English
%2 https://hal.archives-ouvertes.fr/hal-02154546/document
%2 https://hal.archives-ouvertes.fr/hal-02154546/file/2017_Aulus_ILTERMeeting_PosterResume_1.pdf
%2 https://hal.archives-ouvertes.fr/hal-02154546/file/2017_Aulus_ILTERMeeting_PosterResume_2.docx
%L hal-02154546
%U https://hal.archives-ouvertes.fr/hal-02154546
%~ INRAE
%~ SDE
%~ ECOBIOP
%~ TAAF
%~ GIP-BE
%~ INRA
%~ UNIV-PAU
%~ AGREENIUM
%~ TESTUPPA2
%~ UPPA-OA
%~ COLISA

%0 Conference Proceedings
%F Poster 
%T The effect of ageing errors on Von Bertalanffy parameters estimation using a Bayesian sensitivity analysis approach
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%A Aulus Giacosa, Lucie
%A Gaudin, Philippe
%A Aymes, Jean-Christophe
%A Gueraud, Francois
%A VIGNON, Matthias
%Z Résumé
%< avec comité de lecture
%B 6. International Otolith Symposium IOS 2018
%C Keelung, Taiwan
%3 Sixth International Otolith Symposium Handbook
%P 295 p.
%8 2018-04-15
%D 2018
%K numerical models
%K age differences
%K population dynamics
%K ageing-error
%K von bertalanffy parameters estimation
%K bayesian sensitivity analysis
%K scale
%K sea trout
%K âge
%K modèle
%K écaille
%K analyse bayésienne
%K dynamique des populations
%K approche bayésienne
%K salmo trutta
%Z Environmental Sciences/Biodiversity and EcologyPoster communications
%G English
%2 https://hal.archives-ouvertes.fr/hal-02154547/document
%2 https://hal.archives-ouvertes.fr/hal-02154547/file/2018_Aulus_IOS2018_Poster.pdf
%L hal-02154547
%U https://hal.archives-ouvertes.fr/hal-02154547
%~ UNIV-PAU
%~ INRA
%~ SDE
%~ GIP-BE
%~ AGREENIUM
%~ ECOBIOP
%~ INRAE
%~ TESTUPPA2
%~ UPPA-OA
%~ COLISA

%0 Journal Article
%T Hierarchical variance decomposition of fish scale growth and age to investigate the relative contributions of readers and scales
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%A Aulus Giacosa, Lucie
%A Aymes, Jean-Christophe
%A Gaudin, Philippe
%A VIGNON, Matthias
%< avec comité de lecture
%@ 1323-1650
%J Marine and Freshwater Research
%I CSIRO Publishing
%V 70
%N 12
%P 1828-1837
%8 2019
%D 2019
%R 10.1071/MF19059
%K scalimetry
%K sea trout
%K introduced species
%K introduced specie
%K measurement error
%K sampling strategy
%K population dynamics
%K salmo trutta
%K espèce introduite
%K dynamique des populations
%K scalimétrie
%K erreur de mesure
%K stratégie d'echantillonnage
%Z Environmental Sciences/Biodiversity and EcologyJournal articles
%X Correct estimation of interindividual variability is of primary importance in models aiming to quantify population dynamics. In a fisheries context, individual information such as age and growth is often extracted using scales; however, the rationale for using a given scalimetric method (i.e. number of scales per individual and number of readers) is rarely discussed, but different sources of variance may affect the results. As a case study, we used scale growth and age of brown trout (Salmo trutta) caught in the Kerguelen Islands. Based on a nested design (readings of four scales per fish by two independent readers), we decomposed variance in growth and age according to fish (interindividual level), scales (intraindividual level) and readers by using repeatability analysis. The results highlight that most variation is attributable to fish. Readers and scales contribute little to interindividual variance, suggesting that inference was insensitive to intraorganism biological variation. Using additional scales or readers was an inefficient use of sampling resources. We argue that variance decomposition should be widely used for studies aimed at modelling natural variability in life history traits. This would improve our knowledge of the implications of measurement error, helping rationalise and define appropriate sampling strategies
%G English
%2 https://hal.archives-ouvertes.fr/hal-02414999/document
%2 https://hal.archives-ouvertes.fr/hal-02414999/file/2019_Aulus_MarineFreshwaterResearch.pdf
%L hal-02414999
%U https://hal.archives-ouvertes.fr/hal-02414999
%~ UNIV-PAU
%~ INRA
%~ SDE
%~ GIP-BE
%~ AGREENIUM
%~ TAAF
%~ ECOBIOP
%~ INRAE
%~ TESTUPPA2
%~ UPPA-OA
%~ COLISA

%0 Thesis
%T Spatio-temporal evolution of life history traits related to dispersal. Brown trout (Salmo trutta L.) colonization of the sub-Antarctic Kerguelen Islands.
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%A Aulus Giacosa, Lucie
%N 2021PAUU3003
%I Université de Pau et des Pays de l'Adour
%Y Philippe Gaudin
%Y Matthias Vignon
%Z Agnès Bardonnet [Président]
%8 2021-01-12
%D 2021
%K Colonization
%K Growth
%K Life-history traits
%K Partial migration
%K Reaction norm
%K Salmo trutta
%K Colonisation
%K Croissance
%K Migration partielle
%K Norme de réaction
%K Salmo trutta
%K Traits d'histoire de vie
%Z Life Sciences [q-bio]/Animal biologyTheses
%X It is an ongoing issue to better understand colonization process, adaptation potential to new environments, and invasiveness of a species. The sub Antarctic Kerguelen Islands are a perfect model to model population dynamics in an invasion context, because it represents a simplified case of invasion by brown trout (Salmo trutta L.), a facultative anadromous fish. Introduced in the 1950s, and thanks to its dispersive and adaptive capacities, the brown trout provides a unique study model for understanding the causes and mechanisms underlying biological invasions. Understanding dispersal mechanisms, through the study of life history traits related to migration (e.g. growth, age at migration) and their temporal evolution in shifting expansion range population, is the core of this thesis work. Through the study of scales collected in this unique framework, the life histories of nearly 5000 fish have been rebuilt. This work demonstrates the importance of the methodology to determine accurate estimates of individual life history traits. Modelling the evolution of freshwater growth, body size at age and age at first migration demonstrates that evolutionary processes are at work according to the time since colonization. In particular, the decrease in growth rate over time and the decrease in body size at age over time and space suggest that the dispersal capacity is decreasing in populations located at the margins. The evolution of the threshold size at first migration confirms this results, and illustrates the importance of phenotypic plasticity and local adaptation in the choice of migratory tactics. However, the approach taken in this manuscript focuses on the evolution of migration, and would benefit from the study of the joint evolution of traits involved in fitness (costs-benefits balance), such as reproduction, or growth at sea.
%X Grâce à ses capacités dispersives et adaptatives, la truite commune (Salmo trutta L.), poisson anadrome facultatif, est un bon candidat à la colonisation de nouveaux milieux. L'introduction de l'espèce dans les années 1950 dans les îles subantarctiques de Kerguelen fournit un modèle d'étude unique pour comprendre les causes et les mécanismes de la dispersion, grâce à l'étude des traits d'histoire de vie en lien avec la migration (e.g. croissance, âge à la migration). L'étude de leur évolution temporelle le long d'un front de colonisation est le cœur de ce travail de thèse. A travers l’étude des écailles recueillies dans le cadre de ce programme à long terme, les histoires de vie de près de 5000 poissons ont été reconstruites. Ce travail démontre l’importance de la méthodologie utilisée afin de reconstruire les traits de vie individuel. La modélisation de l’évolution de la croissance en eau douce, de la taille à l’âge et de l’âge à la première migration démontre que des processus évolutifs sont en œuvre dans les populations en expansion. Notamment, le ralentissement de la croissance avec le temps ainsi que la diminution de la taille à l’âge le long du front de colonisation laissent à penser que la capacité de dispersion diminue à mesure que les populations sont en marges de la zone d’expansion. L’évolution de la valeur seuil à la première migration confirme ce résultat, et illustre l’importance de la plasticité phénotypique et de l’adaptation locale dans le choix de la tactique migratoire. Toutefois, l’approche menée dans ce manuscrit se concentre sur l’évolution de la migration, et bénéficierait de l’étude de l’évolution conjointe des traits impliqués dans la valeur sélective (balance coûts-bénéfices), tels que la reproduction, ou la croissance en mer.
%G English
%2 https://tel.archives-ouvertes.fr/tel-03193970/document
%2 https://tel.archives-ouvertes.fr/tel-03193970/file/thesisaulusgiacosa.pdf
%L tel-03193970
%U https://tel.archives-ouvertes.fr/tel-03193970
%~ STAR
%~ ECOBIOP
%~ UNIV-PAU
%~ TAAF
%~ INRAE
%~ TESTUPPA2

