%0 Conference Proceedings
%T La Grande Guerre du Temps retrouvé
%+ Arts / Langages : Transitions et Relations (ALTER)
%+ Institut d’Histoire des Représentations et des Idées dans les Modernités (IHRIM)
%A Guillaume, Isabelle
%F Invité
%< avec comité de lecture
%( Proust, Alain-Fournier et la guerre
%B Proust, Alain-Fournier et la guerre
%C Paris, France
%Y Mireille Naturel
%I Honoré Champion
%3 Proust et Alain-Fournier. La transgression des genres 1913-1914
%P 203-216
%8 2014-11-15
%D 2014
%Z Humanities and Social Sciences/LiteratureConference papers
%X L’article montre comment l’auteur de la Recherche organise son évocation du conflit à partir de 1916, l’année de Verdun, et transforme Combray en métaphore du territoire national. Loin d’inscrire l’événement dans un cadre idéologique stable, il fait entendre les discours concurrents qui interprètent, et pour certains produisent, l’hécatombe. L’ouverture de la séquence de la guerre, en forme de chronique inspirée par la mode de 1916, donne le ton. En comparant ces pages de la Recherche aux textes que Camille Duguet a publiés pendant la guerre dans sa rubrique « Propos féminins », l’article fait l’hypothèse que Proust a nourri sa verve satirique du souvenir des clichés de la chroniqueuse du Figaro et de ses discours sur la mode qui transcrivent hypocritement l’aspiration au plaisir des civils dans la rhétorique du civisme de guerre. Mais, loin de considérer la mode comme anecdotique, l’écrivain qui dit vouloir bâtir son œuvre « comme une robe » la voit comme un indice du caractère cyclique de l’Histoire. À la manière des étoffes de Mariano Fortuny qui, en réinventant la Renaissance, montrent que tout doit revenir, les collections de 1916 recréent les tenues du Directoire qui, elles-mêmes, citaient les drapés de l’Antiquité égyptienne et gréco-romaine. La Grande Guerre du roman dit, à la fois, « le Temps perdu » et « le Temps retrouvé ». Dans un contexte de conflit mondial qui a déstabilisé la représentation occidentale d’une Histoire orientée vers un progrès continu, l’auteur de la Recherche indique ainsi que le spectaculaire changement historique induit par la guerre recouvre, en réalité, un filigrane ancien et une forme de permanence.
%G French
%2 https://hal.archives-ouvertes.fr/hal-01986557/document
%2 https://hal.archives-ouvertes.fr/hal-01986557/file/Grande_Guerre_Proust_2017.pdf
%L hal-01986557
%U https://hal.archives-ouvertes.fr/hal-01986557
%~ SHS
%~ ALTER
%~ CNRS
%~ UNIV-PAU
%~ ENS-LYON
%~ UNIV-BPCLERMONT
%~ UNIV-ST-ETIENNE
%~ PRES_CLERMONT
%~ UNIV-LYON3
%~ UNIV-LYON2
%~ UDL
%~ UNIV-LYON
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T The French Epic of American Railways. The Pacific Railroad as seen by Jules Verne, Vivien de Saint-Martin and Pierre Larousse
%+ Arts / Langages : Transitions et Relations (ALTER)
%+ Institut d’Histoire des Représentations et des Idées dans les Modernités (IHRIM)
%A Guillaume, Isabelle
%< avec comité de lecture
%@ 0996-9403
%J Revue d’histoire des chemins de fer
%I Association pour l’histoire des chemins de fer
%V 44
%P 183-200
%8 2013
%D 2013
%R 10.4000/rhcf.1656
%Z Humanities and Social SciencesJournal articles
%X The 1869 opening of the Pacific Railroad, the first American transcontinental railway, contributed to contemporary thought and fed the imaginations on the other side of the Atlantic. In this respect, the 1869 Tour du Monde supplement articles written by Vivien de Saint-Martin, the « chemin de fer » entry in the Grand Larousse universel and two novels of Jules Vernes can be addressed as mutual echos. For the geographer, the lexicographer and the novelist, american trains show the advent of a new era in the mastering of space and nature. By detailing unprecedented services, meaning for them a technical advance that should emulate their side of the Ocean, French authors’ perception of the transcontinental and of the entire american network shapes an ideal and saint-simonian conception making of them the omen of progress toward civilisation and universal peace.
%X L’ouverture du premier transcontinental américain en 1869 a nourri la réflexion et inspiré les imaginations françaises. Dans cette perspective, les articles des suppléments du Tour du monde de 1869 signés par Vivien de Saint-Martin, l’entrée « chemins de fer » rédigée par Pierre Larousse dans le Grand Dictionnaire universel du dix-neuvième siècle, Le tour du monde en quatre-vingts jours et Le testament d’un excentrique de Jules Verne se font écho. Pour le géographe, le lexicographe et le romancier, les trains américains annoncent une nouvelle ère dans la maîtrise de l’espace. Derrière l’exploit spectaculaire que représente le Pacific Railroad, les auteurs admirent l’aménagement d’un territoire gigantesque, d’un océan à l’autre. Tournant la page des explorations, ils ouvrent celle du tourisme de masse et prennent acte du rétrécissement de l’espace parcouru. De plus, les trois auteurs construisent un territoire idéal dont les zones d’ombres sont gommées. Leurs États-Unis dessinent un espace théorique destiné à prouver la supériorité d’un pays qui donne toute sa place au rail. Larousse défend cette certitude jusqu’au paradoxe : « Les chemins de fer ont rendu les sécessions difficiles », écrit-il, « on a pu le voir par les récentes guerres d’Amérique ». Cette lecture singulière est dans la logique d’un point de vue qui envisage le train américain sous un angle, non seulement technique, mais surtout moral. Dans cette perspective, les auteurs n’enregistrent pas la ségrégation qui structure désormais les rapports interraciaux, notamment dans les transports. Leurs voies ferrées américaines dessinent les contours d’une utopie et leur description du réseau ferroviaire de « l’Union » invite à fonder, de ce côté-ci de l’Atlantique, une société plus harmonieuse fondée sur la technique et sur la globalisation des échanges.
%G French
%L hal-02501626
%U https://hal.archives-ouvertes.fr/hal-02501626
%~ UNIV-LYON3
%~ UNIV-PAU
%~ ENS-LYON
%~ UNIV-BPCLERMONT
%~ UNIV-ST-ETIENNE
%~ CERHAC
%~ UNIV-LYON2
%~ IHRIM
%~ CNRS
%~ UDL
%~ UNIV-LYON
%~ SHS
%~ ALTER
%~ PRES_CLERMONT
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Les prophéties dans Heart of Darkness de Joseph Conrad
%+ Université de Pau et des Pays de l'Adour (UPPA)
%A Guillaume, Isabelle
%< avec comité de lecture
%@ 2263-4746
%J Babel : Littératures plurielles
%I La Garde : Faculté des lettres et sciences humaines - Université de Toulon et du Var
%N 4
%P 165--187
%8 2000-03
%D 2000
%R 10.4000/babel.2890
%K figure féminine
%K figures de style
%K folie
%K intertextualité
%K prophétie
%Z Humanities and Social Sciences/LiteratureJournal articles
%X L’auteur de cet article se propose de réévaluer la composante de l’action dans le roman de Joseph Conrad, pour se pencher sur la mise en valeur de l’instance énonciatrice. Elle met à jour l’importance du verbe prophétique dans le récit du narrateur Charles Marlow, tout en montrant l’impuissance de la parole de ce dernier à expliquer son geste et l’énigme de toute connaissance. A travers une analyse des figures de style, de l’intertextualité à l’œuvre dans les trois figures féminines des « tricoteuses », elle montre les liens étroits qu’entretiennent prophétie et folie.
%G French
%L hal-01329625
%U https://hal-univ-tln.archives-ouvertes.fr/hal-01329625
%~ UNIV-PAU
%~ SHS
%~ TESTUPPA2
%~ UPPA-OA

