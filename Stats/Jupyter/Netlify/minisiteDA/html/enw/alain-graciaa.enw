%0 Journal Article
%T Measurement of low interfacial tensions from the intensity of the light scattered by liquid interfaces
%A Lachaise, Jean
%A Graciaa, Alain
%A Martinez, A.
%A Rousset, A.
%< avec comité de lecture
%@ 0302-072X
%J Journal de Physique Lettres
%I Edp sciences
%V 40
%N 22
%P 599-602
%8 1979
%D 1979
%R 10.1051/jphyslet:019790040022059900
%K Microemulsions
%K Liquid liquid interface
%K Interfacial tension
%K Optical method
%K Light scattering
%K Toluene
%K Organic solution
%K Aqueous solutions
%Z Physics [physics]/Physics archivesJournal articles
%X We describe a method for the measurement of interfacial tensions in the millidyne/cm range by measuring the diffuse part of the total internally reflected light produced when the interfaces are illuminated close to the critical angle. This method has been used to measure the optimal interfacial tension in toluene-brine microemulsions in simultaneous equilibrium with the oil and aqueous phases.
%G English
%2 https://hal.archives-ouvertes.fr/jpa-00231696/document
%2 https://hal.archives-ouvertes.fr/jpa-00231696/file/ajp-jphyslet_1979_40_22_599_0.pdf
%L jpa-00231696
%U https://hal.archives-ouvertes.fr/jpa-00231696
%~ AJP

%0 Journal Article
%T Light beating spectroscopy measurements of micelles mutual diffusion coefficient within oil in water microemulsions in the presence of sodium chloride
%A Graciaa, Alain
%A Lachaise, Jean
%A Chabrat, P.
%A Letamendia, L.
%A Rouch, J.
%A Vaucamps, C.
%< avec comité de lecture
%@ 0302-072X
%J Journal de Physique Lettres
%I Edp sciences
%V 39
%N 14
%P 235-238
%8 1978
%D 1978
%R 10.1051/jphyslet:019780039014023500
%K colloids
%K diffusion in liquids
%K emulsions
%K light scattering
%K micelles mutual diffusion coefficient
%K oil in water microemulsions
%K added NaCl
%K virial coefficient
%K light beating spectroscopy measurements
%Z Physics [physics]/Physics archivesJournal articles
%X Using light beating spectroscopy we have measured the mutual diffusion coefficient of micelles within oil in water microemulsions in the presence of added NaCl. The experimental results have been explained by replacing the viscosity of the continuous phase, which appears in the diffusion coefficient of systems of interacting particles, by the viscosity of the microemulsion. They lead to a positive second virial coefficient linked with the electric charge of the micelles and, for the highest NaCl concentrations, to a micellar radius which is close to the radius deduced from osmotic pressure.
%G English
%2 https://hal.archives-ouvertes.fr/jpa-00231487/document
%2 https://hal.archives-ouvertes.fr/jpa-00231487/file/ajp-jphyslet_1978_39_14_235_0.pdf
%L jpa-00231487
%U https://hal.archives-ouvertes.fr/jpa-00231487
%~ AJP

%0 Journal Article
%T Light beating spectroscopy measurements of microemulsion diffusion coefficient
%A Graciaa, Alain
%A Lachaise, Jean
%A Chabrat, P.
%A Letamendia, L.
%A Rouch, J.
%A Vaucamps, C.
%A Bourrel, M.
%A Chambu, C.
%< avec comité de lecture
%@ 0302-072X
%J Journal de Physique Lettres
%I Edp sciences
%V 38
%N 13
%P 253-257
%8 1977
%D 1977
%R 10.1051/jphyslet:019770038013025300
%K emulsions
%K organic compounds
%K quantum beat spectra
%K self diffusion in liquids
%K microemulsion diffusion coefficient
%K translational diffusion coefficient
%K water
%K toluol
%K alcohol
%K light beating spectroscopy
%K monodisperse
%K light scattering
%K spherical droplet model
%K electrically charged micelles
%K H sub 2 O
%K sodium dodecyl sulphate
%Z Physics [physics]/Physics archivesJournal articles
%X The translational diffusion coefficient of microemulsions formed from water, toluol, sodium dodecyl-sulfate and alcohol, has been measured using light beating spectroscopy. The experimental results show that the microemulsions are monodisperse, the micelles being electrically charged.
%G English
%2 https://hal.archives-ouvertes.fr/jpa-00231371/document
%2 https://hal.archives-ouvertes.fr/jpa-00231371/file/ajp-jphyslet_1977_38_13_253_0.pdf
%L jpa-00231371
%U https://hal.archives-ouvertes.fr/jpa-00231371
%~ AJP

%0 Journal Article
%T UV-photoelectron spectroscopy of 1,2- and 1,3-azaborines: A combined experimental and computational electronic structure analysis
%+ Institut Pluridisciplinaire de Recherche sur l'Environnement et les Matériaux (IPREM)
%+ Department of Chemistry and Biochemistry
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Chrostowska, Anna
%A Xu, Senmiao
%A Lamn, Ashley, N.
%A Mazière, Audrey
%A Weber, Christopher, D.
%A Dargelos, Alain
%A Baylère, Patrick
%A Graciaa, Alain
%A Liu, Shih, Yuan
%< avec comité de lecture
%@ 0002-7863
%J Journal of the American Chemical Society
%I American Chemical Society
%V 134
%N 24
%P 10279-10285
%8 2012-06-20
%D 2012
%R 10.1021/ja303595z
%Z Physics [physics]/Physics [physics]/Chemical Physics [physics.chem-ph]Journal articles
%X We present a comprehensive electronic structure analysis of structurally simple BN heterocycles using a combined UV-photoelectron spectroscopy (UV-PES)/computational chemistry approach. Gas-phase He I photoelectron spectra of 1,2-dihydro-1,2-azaborine 1, N-Me-1,2-BN-toluene 2, and N-Me-1,3-BN-toluene 3 have been recorded, assessed by density functional theory calculations, and compared with their corresponding carbonaceous analogues benzene and toluene. The first ionization energies of these BN heterocycles are in the order N-Me-1,3-BN-toluene 3 (8.0 eV) < N-Me-1,2-BN-toluene 2 (8.45 eV) < 1,2-dihydro-1,2-azaborine 1 (8.6 eV) < toluene (8.83 eV) < benzene (9.25 eV). The computationally determined molecular dipole moments are in the order 3 (4.577 D) > 2 (2.209 D) > 1 (2.154 D) > toluene (0.349 D) > benzene (0 D) and are consistent with experimental observations. The λ max in the UV-vis absorption spectra are in the order 3 (297 nm) > 2 (278 nm) > 1 (269 nm) > toluene (262 nm) > benzene (255 nm). We also establish that the measured anodic peak potentials and electrophilic aromatic substitution (EAS) reactivity of BN heterocycles 1-3 are consistent with the electronic structure description determined by the combined UV-PES/computational chemistry approach.
%G English
%L hal-00854630
%U https://hal.archives-ouvertes.fr/hal-00854630
%~ CNRS
%~ IPREM
%~ INC-CNRS
%~ UNIV-PAU
%~ LFCR
%~ LFCR-ISD
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T UV-photoelectron spectroscopy of BN indoles: Experimental and computational electronic structure analysis
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Chrostowska, Anna
%A Xu, S.
%A Mazière, A.
%A Boknevitz, K.
%A Li, B.
%A Abbey, E.R.
%A Dargelos, Alain
%A Graciaa, Alain
%A Liu, S.-Y.
%< avec comité de lecture
%@ 0002-7863
%J Journal of the American Chemical Society
%I American Chemical Society
%V 136
%N 33
%P 11813--11820
%8 2014
%D 2014
%R 10.1021/ja5063899
%Z Chemical Sciences/Inorganic chemistry
%Z Chemical Sciences/Organic chemistryJournal articles
%X We present a comprehensive electronic structure analysis of two BN isosteres of indole using a combined UV-photoelectron spectroscopy (UV-PES)/computational chemistry approach. Gas-phase He I photoelectron spectra of external BN indole I and fused BN indole II have been recorded, assessed by density functional theory calculations, and compared with natural indole. The first ionization energies of these indoles are natural indole (7.9 eV), external BN indole I (7.9 eV), and fused BN indole II (8.05 eV). The computationally determined molecular dipole moments are in the order: natural indole (2.177 D) &gt; fused BN indole II (1.512 D) &gt; external BN indole I (0.543 D). The Îmax in the UV-vis absorption spectra are in the order: fused BN indole II (292 nm) &gt; external BN indole I (282 nm) &gt; natural indole (270 nm). The observed relative electrophilic aromatic substitution reactivity of the investigated indoles with dimethyliminium chloride as the electrophile is as follows: fused BN indole II &gt; natural indole &gt; external BN indole I, and this trend correlates with the π-orbital coefficient at the 3-position. Nucleus-independent chemical shifts calculations show that the introduction of boron into an aromatic 6π-electron system leads to a reduction in aromaticity, presumably due to a stronger bond localization. Trends and conclusions from BN isosteres of simple monocyclic aromatic systems such as benzene and toluene are not necessarily translated to the bicyclic indole core. Thus, electronic structure consequences resulting from BN/CC isosterism will need to be evaluated individually from system to system. © 2014 American Chemical Society.
%G English
%L hal-01566324
%U https://hal.archives-ouvertes.fr/hal-01566324
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T 3P209 Complex Chemical Physics XXXVI : Study on Bio-Prove Method for Viscosity of Polymer Solution(Cell biology,The 48th Annual Meeting of the Biophysical Society of Japan)
%+ Thermodynamique et Energétique des fluides complexes (TEFC)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Maki, Daïsuke
%A Yoshimura, Masatoshi
%A SHOJI, Masahiko
%A Hashimoto, Chihiro
%A Graciaa, Alain
%A Chrostowska, Anna
%A Ushiki, Hideharu
%< avec comité de lecture
%@ 0582-4052
%J Seibutsu Butsuri
%I Biophysical Society of Japan
%V 50
%N supplement 2
%P S182
%8 2010
%D 2010
%R 10.2142/biophys.50.S182_1
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Analytical chemistry
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Material chemistryJournal articles
%G English
%L hal-02132343
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02132343
%~ UNIV-PAU
%~ IPREM
%~ CNRS
%~ INC-CNRS
%~ LFCR
%~ LFCR-ISD
%~ TESTUPPA2
%~ UPPA-OA

%0 Conference Proceedings
%T Study of Wood Plastic Composites elastic behaviour using full field measurements
%+ Laboratoire d’Énergétique et des Transferts Thermiques et Massiques [Tunis] (LETTM)
%+ IUT des Pays de l'Adour (IUT)
%+ Institut Clément Ader (ICA)
%+ Université de Limoges (UNILIM)
%A Ben Mbarek, T.
%A ROBERT, Laurent
%A Hugot, Françoise
%A Orteu, Jean-José
%A Sammouda, H.
%A Graciaa, Alain
%A Charrier, B.
%Z Contenu dans : ICEM 14 - 14th International conference on experimental mechanics : advanced developments applied to structures, materials and environmental issues ; thermomechanics, biomechanics, fluids-solids interaction
%< avec comité de lecture
%Z KTK-2147
%Z DOCA-02
%Z MICS
%( EPJ Web of Conferences. - ISBN 978-2-7598-0565-5
%B ICEM 14 - 14th International Conference on Experimental Mechanics
%C Poitiers, France
%Y Bremand, F
%V 6
%P 28005
%8 2010-06-04
%D 2010
%R 10.1051/epjconf/20100628005
%Z Engineering Sciences [physics]Conference papers
%X In this study, the mechanical properties and microstructure of HDPE/wood fibre composites are investigated. The four-point bending and tensile behaviour of Wood Plastic Composite (WPC) with or without additive are studied by using full-field strain measurements by 3-D Digital Image Correlation (3-D DIC). A non-linear behaviour is shown. The modulus of elasticity (MOE) is calculated as the tangent at zero strain of a Maxwell-Bingham model fitted onto experimental data. Four-point bending tests are analyzed thanks to the spatial standard deviation of the longitudinal strain field to determine the degree of heterogeneity. Cyclic tensile tests have been performed in order to analyze the damage of the material. Moreover, Scanning Electron Microscope (SEM) is used to characterize the morphology of the wood fibre/HDPE matrix interface for specimens with maleic anhydride modified polyethylene additive (MAPE).
%G English
%2 https://hal.archives-ouvertes.fr/hal-01644890/document
%2 https://hal.archives-ouvertes.fr/hal-01644890/file/epjconf_ICEM14_28005.pdf
%L hal-01644890
%U https://hal.archives-ouvertes.fr/hal-01644890
%~ UNILIM
%~ INSA-GROUPE
%~ ICA
%~ ICA-ALBI
%~ EMAC
%~ INSA-TOULOUSE
%~ CNRS
%~ MINES-ALBI
%~ UNIV-TLSE3
%~ INSTITUTS-TELECOM
%~ INSTITUT-TELECOM

%0 Journal Article
%T UV-photoelectron spectroscopy of unhindered germylenes and carbon-arsenic multiple-bonded species
%+ Institut Pluridisciplinaire de Recherche sur l'Environnement et les Matériaux (IPREM)
%+ Thermodynamique et Energétique des fluides complexes (TEFC)
%A Chrostowska, Anna
%A Dargelos, Alain
%A Graciaa, Alain
%< avec comité de lecture
%@ 0004-9425
%J Australian Journal of Chemistry
%I CSIRO Publishing
%V 63
%N 12
%P 1608-1614
%8 2010
%D 2010
%R 10.1071/CH10325
%K Ionization of gases
%K Electrons
%K Electronic structure
%K Experimental data
%K Flash vacuum thermolysis
%K Gasphase
%K Germylenes
%K Ionization energies
%K Main group
%K Quantum chemical calculations
%K Research groups
%K Short-lived species
%K UV photoelectron spectroscopy
%K Vacuum gas
%K Well-established techniques Engineering controlled terms: Arsenic
%K Photoelectricity
%K Photons
%K Potential energy surfaces
%K Quantum chemistry
%K Ultraviolet photoelectron spectroscopy
%K Vacuum Engineering main heading: Photoionization
%Z Physics [physics]/Physics [physics]/Chemical Physics [physics.chem-ph]Journal articles
%X Ultraviolet photoelectron spectroscopy (UV-PES) is a well established technique that provides ionization energies of molecules in the gas phase. Flash vacuum thermolysis or vacuum gas-solid reactions coupled with UV-PES are especially suited for the generation and analysis of small amounts of short-lived species in real-time. These experimental data, supported by quantum chemical calculations for the consistency of the assignments of PES spectra, provide fundamental information about electronic structure and bonding that is obtained by no other technique. This paper aims to give some representative original examples chosen from Pau's research group that illustrate the advantages and wide applicability of these techniques. These examples show the selected data and conclusions which focus on the reactivity of low-coordinated of Main Group IV and V elements. Germylenes and simplest carbon-arsenic multiple bonded species ware successfully characterized using UV photoelectron spectroscopy - a very powerful, direct characterization instrument.
%G English
%L hal-00879003
%U https://hal.archives-ouvertes.fr/hal-00879003
%~ CNRS
%~ IPREM
%~ INC-CNRS
%~ UNIV-PAU
%~ LFCR
%~ LFCR-ISD
%~ TESTUPPA
%~ TESTUPPA2

%0 Journal Article
%T Isoselenocyanates versus Isothiocyanates and Isocyanates
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%+ Institut des Sciences Chimiques de Rennes (ISCR)
%A Chrostowska, Anna
%A Darrigan, Clovis
%A Dargelos, Alain
%A Graciaa, Alain
%A Guillemin, Jean-Claude
%Z Centre National d’Etudes Spatiales
%Z Institut National des Sciences de l'Univers, Centre National de la Recherche Scientifique
%Z Commissariat à l'Énergie Atomique et aux Énergies Alternatives
%Z Institut de chimie, Centre National de la Recherche Scientifique
%Z Institut de Physique, Centre National de la Recherche Scientifique
%< avec comité de lecture
%@ 1089-5639
%J Journal of Physical Chemistry A
%I American Chemical Society
%V 122
%N 11
%P 2894-2905
%8 2018
%D 2018
%R 10.1021/acs.jpca.7b12604
%M 29485878
%Z Chemical Sciences/Theoretical and/or physical chemistryJournal articles
%X Alkyl and aryl isoselenocyanates are well known intermediates in the synthesis of various organoselenium compounds, but the knowledge of the physicochemical properties of simple unsaturated derivatives is still fragmentary. Vinyl-, 2-propenyl-, and cyclopropyl isoselenocyanates have been prepared by reaction of selenium in powder with the corresponding isocyanides. The isoselenocyanates of this series, with a variable distance between the N═C═Se group and the unsaturated or pseudounsaturated group, have been studied by UV-photoelectron spectroscopy and quantum-chemical calculations. For each of these three isoselenocyanates, the exploration of conformers and geometrical optimization always converge toward only one local minimum. The vinyl and cyclopropyl derivatives are characterized by similar order of magnitude of interactions between the NCSe group and the substituent, while for allylic compound two noninteracting moieties should be considered. The same conclusions were obtained for vinylic and cyclopropylic sulfur and oxygen derivatives. Thus the type and extent of interactions between the N═C═X (X = O, S, Se) group and an unsaturated (vinyl, allyl, or cyclopropyl) moiety are now clarified.
%G English
%2 https://hal-univ-rennes1.archives-ouvertes.fr/hal-01744285/document
%2 https://hal-univ-rennes1.archives-ouvertes.fr/hal-01744285/file/Isoselenocyanates%20versus%20Isothiocyanates_accepted.pdf
%L hal-01744285
%U https://hal-univ-rennes1.archives-ouvertes.fr/hal-01744285
%~ UNIV-RENNES1
%~ UNIV-RENNES
%~ TEST-UR-CSS
%~ UR1-SDLM
%~ ISCR-CORINT
%~ UR1-HAL
%~ UR1-UFR-SPM
%~ INC-CNRS
%~ UR1-SPM
%~ STATS-UR1
%~ CNRS
%~ UNIV-PAU
%~ ENSC-RENNES
%~ ISCR
%~ SCR-COS
%~ IPREM
%~ IPREM-CAPT
%~ LFCR
%~ INSA-GROUPE
%~ ISCR-CORINT1
%~ LFCR-ISD
%~ UR1-MMS
%~ INSA-RENNES
%~ TESTUPPA2
%~ UPPA-OA
%~ ISCR-CORINT3

%0 Journal Article
%T New formulation of blood substitutes: optimization of novel fluorinated microemulsions
%T Nouvelle stratégie dans la formulation de substituts du sang: optimisation de nouvelles microémulsions fluorées
%+ Interactions moléculaires et réactivité chimique et photochimique (IMRCP)
%+ Université de Pau et des Pays de l'Adour (UPPA)
%+ Ecole Nationale Vétérinaire de Toulouse (ENVT)
%A Cecutti, Marie-Christine
%A Rico-Lattes, Isabelle
%A Lattes, Armand
%A Novelli, Anne
%A Rico, André
%A Marion, Gérard
%A Graciaa, Alain
%A Lachaise, Jean
%< avec comité de lecture
%@ 0223-5234
%J European Journal of Medicinal Chemistry
%I Elsevier
%V 24
%N 5
%P 485-492
%8 1989
%D 1989
%R 10.1016/0223-5234(89)90052-4
%K Oxygen transporting compounds
%K Fluorinated microemulsions
%Z Chemical Sciences/Medicinal Chemistry
%Z Life Sciences [q-bio]/Bioengineering/BiomaterialsJournal articles
%X The synthesis of a novel microemulsion system composed of a mixed fluorinated and hydrogenated oil C8F17CH2CHCHC4H9 with a biocompatible hydrogenated surfactant, Montanox 80 is described. Investigation of the solubility of oxygen in these microemulsions showed that they absorbed more oxygen than Fluosol-DA which is currently used as an oxygen transporter in biomedical applications. Oxygen absorption was similar to that of blood. Light scattering studies showed that the system was composed of small sized aggregates which should in principle be compatible with blood. The toxicity of the microemulsions was tested after intraperitoneal injection in rats, and in mice after intravenous administration. The microemulsions appeared to be well tolerated. These results show promise for the development of oxygen transporting compounds.
%X Pour la première fois, nous décrivons des systèmes de microémulsions d'une huile mixte fluorée et hydrogénée C8F17CH2CHCHC4H9 à l'aide d'un tensioactif hydrogéné biocompatible: le Montanox 80. Les études de solubilité de l'oxygène dans ces microémulsions montrent qu'elles absorbent davantage l'oxygène que l'émulsion Fluosol DA, actuellement utilisée dans le domaine biomédical des transporteurs d'oxygène. Cette absorption s'effectue dans des proportions analogues à celles du sang. Du point de vue de la toxicité de ces systèmes, des mesures de la diffusion de la lumière mettent en évidence des agrégats de petite taille compatibles théoriquement avec le système sanguin. La toxicité des microémulsions a été testée en i.p. chez le rat et en i.v. chez la souris. Les microémulsions semblent bien tolérées. Le travail décrit ici constitue donc une ouverture d'une nouvelle voie de recherche dans le domaine des transporteurs d'oxygène.
%G English
%2 https://hal.archives-ouvertes.fr/hal-02094811/document
%2 https://hal.archives-ouvertes.fr/hal-02094811/file/Cecutti_23549.pdf
%L hal-02094811
%U https://hal.archives-ouvertes.fr/hal-02094811
%~ CNRS
%~ UNIV-PAU
%~ AGREENIUM
%~ INC-CNRS
%~ UNIV-TLSE3
%~ IRD
%~ IMRCP
%~ TESTUPPA2
%~ UPPA-OA
%~ INPT

