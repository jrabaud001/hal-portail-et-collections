%0 Conference Paper
%F Poster 
%T Connaître l'adaptation aux changements climatiques chez les acteurs institutionnels locaux : mise en place d'une méthodologie
%+ SET
%A Heurtier, Pierre-Yves
%A Deletraz, Gaëlle
%A Lompré, Nicole
%A Rebotier, Julien
%Z Conseil Régional d'Aquitaine
%< sans comité de lecture
%B Pratiques et Portée de l'Adaptation
%C Pau - Bayonne, France
%8 2014-01-23
%D 2014
%K Adaptation
%K territoires
%K action publique
%K méthodologie
%Z Humanities and Social Sciences/GeographyPoster communications
%X Ce poster a été réalisé dans le cadre du programme de recherche Changement Environnemental et Stratégies d'Adaptation en Région (CESAR), mené au laboratoire SET sur la période 2012 – 2014, et cofinancé par le Conseil Régional d'Aquitaine. Au moyen d'un regard de sciences humaines et sociales, le programme se concentre sur l'élaboration et l'application en Aquitaine de plans et de stratégies d'adaptation au changement climatique. Il est construit de façon à articuler une perspective descendante de politiques environnementales, et une perspective montante de réception de ces politiques et d'appropriation de la problématique environnementale. Dans le cadre de CESAR, il n'était pas prévu d'effectuer d'enquête de type fermé sur la question de l'adaptation. Il est néanmoins apparu pertinent de réfléchir à une méthodologie à tendance quantitative sur cette question, et particulièrement sur la perspective descendante de mise en place de l'adaptation sur les territoires. Il s'agit donc ici d'un travail exploratoire visant à proposer une méthodologie à parfaire. Cette méthodologie est testée sur un petit échantillon d'une trentaine de personnes qui ont répondu favorablement à l'enquête en ligne adressée aux services déconcentrés de l'Etat qui étaient concernés, aux communes, communautés de communes, parcs naturels, pays et à quelques associations de la région Aquitaine. Il s'agit de trouver un moyen pour mieux connaître l'adaptation au changement climatique, son utilisation, la façon dont elle est représentée et mobilisée, dans les
%G French
%Z CESAR
%2 https://halshs.archives-ouvertes.fr/halshs-01100826/document
%2 https://halshs.archives-ouvertes.fr/halshs-01100826/file/Poster_CESAR_colloque2PAu.pdf
%L halshs-01100826
%U https://halshs.archives-ouvertes.fr/halshs-01100826
%~ SHS
%~ CNRS
%~ UNIV-PAU
%~ AO-GEOGRAPHIE
%~ GIP-BE
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Retour sur images. Les stations touristiques de Seignosse et Gourette sous le regard des touristes
%+ Société, environnement, territoire (SET)
%+ Centre d'Etude et de Recherche Travail Organisation Pouvoir (CERTOP)
%A Hatt, Emeline
%A Vlès, Vincent
%A Clarimont, Sylvie
%A Deletraz, Gaëlle
%Z Conseil général des Landes / Conseil général des Pyrénées-Atlantiques
%< avec comité de lecture
%@ 1777-5477
%J EspacesTemps.net
%I Association Espaces Temps.net
%8 2011
%D 2011
%K stations touristiques
%K urbanisme
%K espaces publics
%K enquêtes photographiques
%Z Humanities and Social Sciences/Architecture, space managementJournal articles
%X Face au constat du vieillissement des stations touristiques « archipels », créées ex nihilo dans les années 1960-1970, des recherches en urbanisme, portant sur leur requalification et menées par l'UMR 5603 du CNRS, ont posé la question de l'image urbaine et de sa représentation. Dans ce champ disciplinaire et professionnel, l'analyse de l'image des destinations touristiques n'a guère été traitée. Dans ces stations coupées de l'habitat traditionnel existant, la faible proportion d'habitants permanents a conduit à donner la parole à des interlocuteurs qui en sont les « destinataires » essentiels : les « touristes », généralement tenus écartés de l'exercice de concertation et de conception lors des choix d'urbanisme fondateurs. À la croisée des recherches en urbanisme et en tourisme, on a tenté, par une enquête photographique, de révéler comment des marqueurs microterritoriaux pouvaient contribuer à mieux saisir les enjeux de la restructuration des espaces publics. Axées sur la « libre » catégorisation de plus de cent trente images (par double classement libre de photographies), ces enquêtes ont été menées à titre expérimental dans une station littorale (Seignosse-Océan dans les Landes) et une station de montagne (Gourette dans les Pyrénées-Atlantiques). Si les perceptions des touristes ont été au coeur de ce dispositif d'analyse urbaine et paysagère, ils ont en retour permis de mettre à l'épreuve les méthodes d'enquêtes habituellement utilisées en urbanisme. Cet article rappelle les fondements de la méthode d'enquête retenue, en identifiant, au regard des résultats, ses apports et ses limites. Créées pour et autour d'une activité touristique unique (le ski ou la baignade), les stations « archipels » contemporaines 1 sont aujourd'hui confrontées à une série de difficultés (forte saisonnalité, vétusté de certains équipements publics, obsolescence du parc de logements, etc.) qui conduisent leurs gestionnaires à envisager des opérations de requalification urbaine destinées — a priori — à prévenir ou à enrayer une baisse de la fréquentation touristique. Poussés par l'État à définir des Projets d'Aménagement et de Développement Durable (PADD)2, les acteurs locaux s'interrogent sur les orientations générales d'urbanisme et d'aménagement de leur station, notamment les fonctions nouvelles et la restructuration de leurs espaces publics3. Le PADD a pour vocation de présenter, de « manière intelligible pour le citoyen », les choix politiques de la commune et d'engager le débat municipal. Cette question appliquée précisément aux stations de tourisme interroge le chercheur en urbanisme. Dans ce champ disciplinaire, l'analyse de l'image des destinations touristiques n'a encore guère été traitée, alors qu'elle fit l'objet de recherches prolixes en ville, dès les années 1960, comme en témoignent les travaux de Kevin Lynch sur l'image de la ville (1960). Elle est alors appréhendée à partir de l'analyse des représentations que les habitants se font de leur lieu de vie. Nous nous sommes proposés de transposer cette démarche aux stations touristiques vieillissantes qui s'interrogent sur leur devenir, et notamment, aux stations « archipels » françaises de Gourette, dans les Pyrénées et de Seignosse-Océan, dans les Landes. Nous avons cherché à évaluer la perception, par les usagers (les touristes), du cadre bâti et de l'attractivité des espaces publics des stations qu'ils fréquentent en identifiant les marqueurs microterritoriaux 4 pouvant enrichir la construction du projet d'aménagement. Nous avons donc donné la parole à des interlocuteurs habituellement absents dans cet exercice en station touristique : les « touristes ». Quelle méthode adopter pour appréhender leurs représentations des espaces publics et de l'urbanisme de leur lieu de vacances ? Comment analyser les représentations des stations dans leur dimension microterritoriale ? À la croisée de deux champs d'étude, urbanistique et touristique, le recours à l'image semblait être un medium pertinent. On aborde ainsi les enjeux indirects qu'a pu soulever la mise en oeuvre de cette recherche urbanistique appliquée aux stations touristiques. Le support photographique employé, notamment pour ses qualités opératoires et pédagogiques dans la prise de décision et la gouvernance, pose également la question du rapport de l'enquêteur au terrain et aux personnes enquêtées. Après avoir rappelé l'apport de la méthode de recherche choisie, on reviendra dans un second temps sur les enjeux soulevés par sa mise en oeuvre.
%G French
%2 https://hal-amu.archives-ouvertes.fr/hal-01558581/document
%2 https://hal-amu.archives-ouvertes.fr/hal-01558581/file/Retour-sur-image-Hatt-Vles-Clarimont.pdf
%L hal-01558581
%U https://hal-amu.archives-ouvertes.fr/hal-01558581
%~ UNIV-AMU
%~ CNRS
%~ UNIV-PAU
%~ AO-ARCHITECTURE
%~ ARCHITECTURE
%~ CERTOP
%~ SHS
%~ SMS
%~ UNIV-TLSE2
%~ UNIV-TLSE3
%~ TESTUPPA2
%~ UPPA-OA

%0 Thesis
%T Géographie des risques environnementaux liés aux transports routiers en montagne. Incidences des emissions d'oxydes d'azote en vallées d'Aspe et de Biriatou (Pyrénées)
%+ Société, environnement, territoire (SET)
%A Deletraz, Gaëlle
%I Université de Pau et des Pays de l'Adour
%Y ETCHELECOU André
%8 2002-12-13
%D 2002
%K Montagne
%K Pyrénées
%K Analyse spatiale
%K Charge critique
%K Oxydes d'azote
%K Acidification
%K Eutrophisation
%K Pollution automobile
%K Transport routier
%K Risque environnemental
%K Géographie
%Z Humanities and Social Sciences/GeographyTheses
%X The purpose of this thesis is to evaluate the impact of road pollution on moutain ecosystems and show that geographic approach is complementary to that of the chemists or biologists. Two valleys have been chosen for field studies : sites at Biriatou/Saint-Jean-de-Luz, an international corridor confronted with considerable growth in heavy vehicle traffic – more than 7500 trucks per day in 2000 ; and in the Aspe valley, to assess the situation before the opening of the new international road tunnel of the Somport. Road atmospheric pollution studies raises many methodological difficulties. To study the effects of this pollution, it must be first charted. We describe the model of spatial diffusion of pollution used. The studied pollutant is nitrogen dioxide (NO2) because it contributes, with the other nitrogen compounds, to acidification and eutrophisation of water and soils. The method integrates topography. We have established a cartography of nitorgen deposits for the two studied sectors. Then, this thesis talk about impacts. The evaluation of the deposition is not sufficient to determine the risk. Each ecosystem has specific characteristics (buffer capacity of soil, type of vegetation). The critical loads differ according to the environmental conditions. The definition of the risks areas needs the comparison of the levels of pollution and environmental conditions. The methodology used showed that the environmental risks of the road traffic is real, even when the traffic is weak, according to dispersion possibility.
%X L'objectif de cette thèse est d'évaluer l'incidence de la pollution azotée des transports sur les écosystèmes de montagne, mais aussi de montrer que la géographie propose une approche complémentaire à celles des chimistes ou des biologistes. Deux vallées Pyrénéennes ont été choisies comme terrain d'étude : le site de Biriatou/Saint-Jean-de-Luz, un axe de passage international (A63) confronté à une progression considérable du trafic, notamment poids lourds avec plus de 7500 PL par jour durant l'année 2000, et la vallée d'Aspe (RN134), pour faire un état de la situation avant l'ouverture du nouveau tunnel routier international du Somport. La recherche dans le domaine de la pollution atmosphérique du trafic routier pose de nombreuses difficultés méthodologiques. Etablir une cartographie de cette pollution constitue pourtant un préalable indispensable à l'étude des risques induits par le trafic. Un modèle de diffusion spatiale de la pollution a été mis en œuvre dans cette optique. Le polluant retenu pour cette étude est le dioxyde d'azote (NO2) car il contribue – avec les autres polluants azotés – à l'acidification et à l'eutrophisation des eaux et des sols. Une méthode intégrant la topographie a permis d'établir une cartographie des dépôts azotés pour les deux secteurs d'étude. Nous montrons que l'évaluation du dépôt polluant (l'aléa) ne permet pas – à elle seule – de déterminer le risque. Chaque écosystème possédant des caractéristiques qui lui sont propres (capacité tampon du sol, nature de la végétation notamment), la vulnérabilité traduite en termes de charges critiques diffère en fonction des conditions environnementales. C'est à travers la mise en relation spatiale des niveaux de pollution avec la connaissance des milieux naturels que les zones à risques peuvent être localisées. La méthodologie proposée dans cette thèse a permis de montrer que les risques environnementaux liés aux transports routiers sont bien réels, même lorsque le trafic est faible selon les conditions de dispersion.
%G French
%2 https://tel.archives-ouvertes.fr/tel-00003245/document
%2 https://tel.archives-ouvertes.fr/tel-00003245/file/tel-00003245.pdf
%L tel-00003245
%U https://tel.archives-ouvertes.fr/tel-00003245
%~ CNRS
%~ UNIV-PAU
%~ SHS
%~ AO-GEOGRAPHIE
%~ GIP-BE
%~ TESTUPPA2
%~ UPPA-OA

%0 Conference Paper
%F Oral 
%T Se référer au passé, se projeter dans l’avenir. Analyses textuelles et fouille spatiale pour appréhender les horizons temporels des contributions au Grand et au Vrai Débat
%+ Passages
%A Deletraz, Gaëlle
%A Le Campion, Grégoire
%A Montagne, Delphine
%A Pierson, Julie
%A Pissoat, Olivier
%< avec comité de lecture
%B Journées d’études "Quels outils pour appréhender et analyser les mobilisations de Gilets Jaunes et les données issues du Débat national ?"
%C Paris, France
%8 2020-01-16
%D 2020
%Z Humanities and Social Sciences/Geography
%Z Humanities and Social SciencesConference papers
%X Se référer au passé, se projeter dans l'avenir. Analyses textuelles et fouille spatiale pour appréhender les horizons temporels des contributions au Grand et au Vrai Débat.
%G English
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-02444836/document
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-02444836/file/Pass%C3%A9-avenir-Grand-D%C3%A9bat-2020.pdf
%L hal-02444836
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02444836
%~ UNIV-PAU
%~ PASSAGES-UPPA
%~ SHS
%~ CNRS
%~ UNIV-BORDEAUX-MONTAIGNE
%~ GIP-BE
%~ UMR5319
%~ AO-GEOGRAPHIE
%~ TESTUPPA2
%~ UPPA-OA

%0 Report
%T Décideurs et Citoyens dans un contexte urbain de Signaux Faibles (Décisif). Rapport final. Ademe, APR TEES : Transition écologique économique et sociale
%+ Université de Pau et des Pays de l'Adour (UPPA)
%+ Transitions Energétiques et Environnementales (TREE)
%+ Passages
%+ Association Ecocène
%+ APESA [Pau]
%A Bouisset, Christine
%A Arnauld De Sartre, Xavier
%A Baggioni, Vincent
%A Bousquet, Aurélie
%A Cousteau, Evelyne
%A Daléas, Jacques
%A Degrémont, Isabelle
%A Deletraz, Gaëlle
%A Douence, Hélène
%A Khamis, Rim
%A Laplace-Treyture, Danièle
%A Montagne, Delphine
%A Pottier, Aude
%A Silva, Magali
%A Vaucelle, Sandrine
%A Wast, Marie
%Z Ademe, Université de Pau et des Pays de l'Adour (UPPA), Agglomération Pau Béarn Pyrénées
%I Ademe
%8 2021-02-15
%D 2021
%K Transition écologique
%K Changement climatique
%K échelle
%K gestion
%Z Humanities and Social Sciences/GeographyReports
%G French
%2 https://hal.archives-ouvertes.fr/hal-03207109/document
%2 https://hal.archives-ouvertes.fr/hal-03207109/file/Decisif_rapport_final-light2.pdf
%L hal-03207109
%U https://hal.archives-ouvertes.fr/hal-03207109
%~ UMR6031TREE
%~ AO-GEOGRAPHIE
%~ UNIV-PAU
%~ GIP-BE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ UMR5319
%~ CNRS
%~ PASSAGES-UPPA
%~ LARA
%~ TESTUPPA2
%~ SHS
%~ UPPA-OA

%0 Conference Proceedings
%T Investigating the isotopic composition of mercury and lead in epiphytic lichens from South-western France (Pyrénées-Atlantiques) to better constrain the spatial variability of their atmospheric transport and deposition
%+ Passages
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Barre, J.P.G.
%A Deletraz, Gaëlle
%A Amouroux, David
%A Bérail, Sylvain
%A Pinaly, Hervé
%A Donard, O.F.X.
%A Tran, T.
%Z cited By 0
%< avec comité de lecture
%B International Conference on Heavy Metals in the Environment
%C Unknown, Unknown Region
%Y Pirrone N.
%I EDP Sciences
%V 1
%8 2013
%D 2013
%R 10.1051/e3sconf/20130129002
%Z Chemical Sciences/Analytical chemistryConference papers
%X The geographical variability of the elemental concentration and isotopic composition of mercury and lead was evaluated in epiphytic lichens collected over a mesoscale area (Pyrenees-Atlantiques, Southwestern France). The sampling points have been selected according to different parameters in order to represent the main land use classification of the investigated territory (urban, industrial, agricultural and forestal zones). Total concentrations of Hg are homogenous over the territory (mean 0.14 mg/kg dry weight) and reflect the European atmospheric anthropogenic background deposition. A significant trend is observed for lead concentrations between anthropogenized areas (∼11 mg/kg) and remote areas (∼6mg/kg), mainly due to local road traffic and industrial inputs. The isotopic composition of Hg reveals a relatively homogeneous signature specific of remote areas, while the lead isotopic composition is distributed along two distinct origins: past leaded gasoline and geogenic background. This study shows that the spatial concentration variability observed is globally consistent with the isotopic signatures of Hg and Pb recorded in lichens which partly explain the origin of these two metallic contaminants. © Owned by the authors, published by EDP Sciences, 2013.
%G English
%L hal-01590283
%U https://hal.archives-ouvertes.fr/hal-01590283
%~ CNRS
%~ UNIV-PAU
%~ IPREM
%~ INC-CNRS
%~ UMR5319
%~ UNIV-BORDEAUX-MONTAIGNE
%~ PASSAGES-UPPA
%~ TESTUPPA2
%~ UPPA-OA

%0 Conference Proceedings
%T Modélisation des durées de transit des eaux usées dans le bassin de collecte d’un réseau d’assainissement unitaire. Une contribution aux diagnostics à l’amont des déversoirs d’orage et des stations d’épuration
%+ Passages
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Deletraz, Gaëlle
%A Bersinger, Thomas
%< avec comité de lecture
%B Conférence Francophone SIG 2016
%C Versailles, France
%I ESRI France
%3 Actes de la Conférence Francophone ESRI SIG 2016
%8 2016-10-05
%D 2016
%K flux de direction
%K Assainissement
%K bassin versant de collecte
%K déversoir d'orage
%K diagnostic à l'amont
%K station d'épuration
%K durée de transit
%Z Chemical Sciences/Analytical chemistry
%Z Humanities and Social Sciences/Methods and statistics
%Z Humanities and Social Sciences/Environmental studies
%Z Humanities and Social Sciences/GeographyConference papers
%X L'objectif du traitement est de calculer les durées de transit des eaux usées dans un bassin versant de collecte d'un réseau d'assainissement jusqu'au déversoir d'orage. Cet outil doit aider à comprendre la dynamique des déversoirs d'orage et s'inscrit dans l'optique de la réalisation de diagnostics à l'amont des stations d'épuration.
%G French
%2 https://hal.archives-ouvertes.fr/hal-01408744/document
%2 https://hal.archives-ouvertes.fr/hal-01408744/file/Communication%20DELETRAZ%20BERSINGER%20ESRI%202016.pdf
%L hal-01408744
%U https://hal.archives-ouvertes.fr/hal-01408744
%~ CNRS
%~ UNIV-PAU
%~ IPREM
%~ AO-GEOGRAPHIE
%~ SHS
%~ INC-CNRS
%~ GIP-BE
%~ UMR5319
%~ UNIV-BORDEAUX-MONTAIGNE
%~ PASSAGES-UPPA
%~ TESTUPPA2
%~ UPPA-OA

%0 Unpublished work
%T Enquête (dé)confinement et COVID-19 - Synthèse des premiers résultats
%+ Passages
%+ Université de Pau et des Pays de l'Adour (UPPA)
%A Barthou, Évelyne
%A Bruna, Yann
%A Deletraz, Gaëlle
%8 2020-05-20
%D 2020
%K coronavirus
%K COVID-19
%K ressenti
%K expérience
%K confinement
%Z Humanities and Social Sciences/Sociology
%Z Humanities and Social Sciences/Library and information sciencesPreprints, Working Papers, ...
%X Ce document de travail est une note de synthèse relative au premier volet de notre enquête portant sur l’expérience et le ressenti des personnes pendant la période de confinement imposée par le coronavirus, quel que soit le niveau d'impact sur les individus. L'objectif est de comprendre les modalités d’organisation et de réaction de chacun·e. Il s'agit de résultats préliminaires : de nombreuses questions sont actuellement (juin 2020) en cours d'exploitation.
%G French
%2 https://hal.archives-ouvertes.fr/hal-02613500/document
%2 https://hal.archives-ouvertes.fr/hal-02613500/file/Resultats_enquete_covid_final.pdf
%L hal-02613500
%U https://hal.archives-ouvertes.fr/hal-02613500
%~ SOCIOLOGIE
%~ AO-SOCIOLOGIE
%~ UNIV-PAU
%~ UNIV-BORDEAUX-MONTAIGNE
%~ UMR5319
%~ CNRS
%~ PASSAGES-UPPA
%~ SHS
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Par-delà le changement climatique, les représentations du changement environnemental : le poids de la tension entre filière et territoire dans les secteurs pastoral et viticole en Aquitaine
%+ Passages
%+ Laboratoire Interdisciplinaire Solidarités, Sociétés, Territoires (LISST)
%A CHARBONNEAU, Marion
%A Deletraz, Gaëlle
%A Rebotier, Julien
%< avec comité de lecture
%@ 1492-8442
%J VertigO : La Revue Électronique en Sciences de l'Environnement
%I VertigO
%V 16
%N 3
%P En ligne
%8 2016-12-20
%D 2016
%R 10.4000/vertigo.18110
%K regulation
%K pastoralism
%K wine production
%K computer-assisted analysis
%K territory
%K climate
%K environmental change
%K analyse assistée par ordinateur
%K pastoralisme
%K viticulture
%K réglementaire
%K climat
%K territoire
%K changement environnemental
%Z Humanities and Social Sciences/GeographyJournal articles
%X In the face of growing concerns linked to climate issue in both the scientific and the political arenas, this contribution addresses the meaning and scope of environmental change for different actors involved in pastoral and wine production sectors in the Aquitaine region of France. It states that the representations of environmental change are closely linked to the specificities of agricultural sectors, the kind of professional activities (such as farming, technics, administration, among others), and the levels of intervention (from the European to the local scale). The paper compares the discourses of different actors in both production sectors. It builds on both computer-assisted and manual analysis of discourses collected during focus-groups. Drawing on a strong thematic research and a didactic presentation of the methodology, a critical epistemological statement allows addressing the apparently normal scientific demonstration process. The latter appears to be a current and conventional process in research activities, implying posterior rearrangements that are not always made visible. Results show at last how important the regulatory dimension of environmental change can be for all actors. Climatic aspects remain a marginal concern, especially in the pastoral sector. Additionally, the study critically engages with the main drivers through which public action is implemented in agriculture. Indeed, the paper stresses how relevant the organization of the productive sectors is (be it a local and place-based organization – pastoral sector ; or a more institutional and vertical one – wine production sector) in framing representations and strategies linked with environmental change.
%X Face à la montée en puissance de la question climatique tant dans la sphère politique que scientifique, cette contribution s’intéresse au sens que recouvre le changement environnemental pour les acteurs du secteur agricole en s’appuyant sur le pastoralisme et la viticulture en Aquitaine (France). Partant de l’idée que les représentations du changement environnemental sont étroitement liées à la filière, au type de profession (producteur, technicien, agent administratif, etc.) ou à l’échelle d’action, elle confronte les discours des agents d’encadrement et des producteurs viticoles et pastoraux d’Aquitaine, afin d’identifier les différentes modalités d’appréciation. L’étude est basée sur une analyse manuelle et semi-automatique des discours collectés durant des sessions de focus-groupe. Grâce à des bases thématiques solides et à une présentation méthodologique très didactique, une forte perspective épistémologique permet de prendre du recul quant aux reconstructions a posteri du raisonnement, qui s’avèrent conventionnelles dans l’activité de recherche. Les résultats consolidés pointent l’importance accordée par tous les acteurs à la dimension réglementaire qui accompagne les problématiques environnementales, alors qu’au contraire la question climatique reste à la marge de leurs préoccupations, notamment dans le secteur pastoral. L’étude invite par ailleurs à questionner les formes et leviers de l’action publique dans le domaine agricole en montrant en quoi l’organisation des secteurs productifs (plus territorialisée – pastoralisme – ou plus verticale – viticulture) constitue un élément fondamental de compréhension des différences de représentations et de stratégies liées au changement environnemental.
%G French
%L hal-01517246
%U https://hal.archives-ouvertes.fr/hal-01517246
%~ SHS
%~ CNRS
%~ UNIV-PAU
%~ EHESS
%~ AO-GEOGRAPHIE
%~ LISST
%~ SMS
%~ UNIV-TLSE2
%~ GIP-BE
%~ UMR5319
%~ UNIV-BORDEAUX-MONTAIGNE
%~ PASSAGES-UPPA
%~ TESTUPPA2
%~ UPPA-OA
%~ MONTAIGNE_TABLE

%0 Conference Paper
%F Poster 
%T SO-MATE, un nouveau réseau métier mais aussi… un groupe local de MATE-Shs
%+ Groupe de Recherche en Economie Théorique et Appliquée (GREThA)
%+ Passages
%A onfroy, karine
%A Deletraz, Gaëlle
%< sans comité de lecture
%B Inauguration du département de recherche CHANGES - Sciences sociales des changements contemporains
%C Bordeaux, France
%8 2019-04-18
%D 2019
%Z Humanities and Social SciencesPoster communications
%G French
%2 https://hal.archives-ouvertes.fr/hal-02175567/document
%2 https://hal.archives-ouvertes.fr/hal-02175567/file/poster_fin.pdf
%L hal-02175567
%U https://hal.archives-ouvertes.fr/hal-02175567
%~ GRETHA
%~ UNIV-PAU
%~ CNRS
%~ UMR5319
%~ SHS
%~ UNIV-BORDEAUX-MONTAIGNE
%~ PASSAGES-UPPA
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Retour sur images. Les stations touristiques de Seignosse et Gourette sous le regard des touristes
%+ Société, environnement, territoire (SET)
%A Vlès, Vincent
%A Emeline, Hatt
%A Clarimont, Sylvie
%A Deletraz, Gaëlle
%< avec comité de lecture
%@ 1777-5477
%J EspacesTemps.net
%I Association Espaces Temps.net
%P http://espacestemps.net/document9036.html
%8 2011-10-10
%D 2011
%K stations touristiques
%Z Humanities and Social Sciences/Architecture, space managementJournal articles
%X Comment analyser les représentations des stations dans leur dimension microterritoriale ? À la croisée de deux champs d'étude, urbanistique et touristique, le recours à l'image semblait être un medium pertinent. On aborde ainsi les enjeux indirects qu'a pu soulever la mise en œuvre de cette recherche urbanistique appliquée aux stations touristiques. Le support photographique employé, notamment pour ses qualités opératoires et pédagogiques dans la prise de décision et la gouvernance, pose également la question du rapport de l'enquêteur au terrain et aux personnes enquêtées. Après avoir rappelé l'apport de la méthode de recherche choisie, on reviendra dans un second temps sur les enjeux soulevés par sa mise en œuvre.
%G French
%2 https://halshs.archives-ouvertes.fr/halshs-00681516/document
%2 https://halshs.archives-ouvertes.fr/halshs-00681516/file/Art-EspTps.def.pdf
%L halshs-00681516
%U https://halshs.archives-ouvertes.fr/halshs-00681516
%~ SHS
%~ CNRS
%~ UNIV-PAU
%~ AO-ARCHITECTURE
%~ ARCHITECTURE
%~ ETUDES-URBAINES
%~ TESTUPPA2

%0 Journal Article
%T Comparison of the Isotopic Composition of Hg and Pb in Two Atmospheric Bioaccumulators in a Pyrenean Beech Forest (Iraty Forest, Western Pyrenees, France/Spain)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Barre, Julien P.G.
%A Queipo-Abad, Silvia
%A Sola-Larrañaga, Cristina
%A Deletraz, Gaëlle
%A Bérail, Sylvain
%A Tessier, Emmanuel
%A Elustondo, David
%A Santamaria, Jesus Miguel
%A de Diego, Alberto
%A Amouroux, David
%< avec comité de lecture
%@ 2296-665X
%J Frontiers in Environmental Science
%I Frontiers
%V 1
%8 2020
%D 2020
%R 10.3389/fenvc.2020.582001
%K soils
%K mosses
%K lichens
%K bioaccumulator
%K mercury
%K lead
%K isotopes
%K forest ecosystem
%Z Environmental Sciences
%Z Environmental Sciences/Global Changes
%Z Chemical SciencesJournal articles
%X Mercury (Hg) and lead (Pb) isotopic compositions were investigated in mosses and lichenscollected in a large mountainous beech forest (Iraty Forest) located on the French-SpanishPyrenean border. Hg isotopic signature in topsoil samples were also analyzed in selectedsampling sites. This is the firstwork that uses the complementary information of both isotopicsystems in two distinct atmospheric bioaccumulators. Mosses and lichens presentcharacteristic accumulation due to their integration times, displaying different on metal pollution over the area. Hg and Pb concentrations in annualmoss shoots represent recent atmospheric accumulation, while whole lichen thalli integrates a process of accumulation over a longer period. Lead isotope ratios in mosses are consistent with reported data corresponding to the actual European atmospheric background (206Pb/207Pb ∼ 1.158), while Hg isotopic composition reflects potential uptake of both dry and wet Hg depositions. For lichens, Pb isotopic composition exhibits the contribution of a longer integration period of both industrial Pb emissions and legacy of leaded gasoline pollution. Hg isotopes in lichens discriminate two main groups: a larger one representing the background atmospheric contribution and a second one corresponding to unexpected higher Hg content. The similarities in odd and even Mass-independent fractionation of Hg isotopes between topsoils and lichens from the larger group, support the idea that foliage uptake is the main input of Hg in soils. The second group of lichens exhibits more negative δ202Hg (down to –4.69‰) suggesting a new source of fractionation in this area, probably related to lichens aging and/or stubble and grass fires due to pastoral activities. This study demonstrates that using both Hg and Pb isotopic signature in lichens and mosses allows to trace atmospheric sources and environmental pathways of these metals in forested ecosystems. This original data set in a remote environment provides also new information on the fate of atmospheric Pb and Hg depositions.
%G English
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-03018945/document
%2 https://hal-univ-pau.archives-ouvertes.fr/hal-03018945/file/fenvc-01-582001.pdf
%L hal-03018945
%U https://hal-univ-pau.archives-ouvertes.fr/hal-03018945
%~ UNIV-PAU
%~ IPREM
%~ IPREM-CME
%~ SDE
%~ GIP-BE
%~ INC-CNRS
%~ CNRS
%~ TESTUPPA2
%~ UPPA-OA
%~ PASSAGES-UPPA
%~ UMR5319
%~ UNIV-BORDEAUX-MONTAIGNE

%0 Report
%T Massif montagneux – Les Pyrénées
%+ Groupement d'Intérêt Scientifique ID 64 - Recherches sur les ovins et le lait de montagne des Pyrénées-Atlantiques (GIS ID 64)
%+ Passages
%+ Université de Lausanne (UNIL)
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Géographie de l'environnement (GEODE)
%+ DIRSO
%+ Parc National des Pyrénées
%A Arranz, Jean-Marc
%A Bargerie,, Nathalie
%A Bouisset, Christine
%A CHARBONNEAU, Marion
%A Chauvin, Sébastien
%A D'Amico, Frank
%A Degrémont, Isabelle
%A Deletraz, Gaëlle
%A Deutsch,, Éloïse
%A Esnault, François
%A Galop, Didier
%A Grimal, Didier
%A Pons, Marc
%A Rouyer,, Emmanuel
%A Sourp, Eric
%P pp 363-397
%I Région Nouvelle-Aquitaine
%8 2018
%D 2018
%Z Humanities and Social Sciences/GeographyReports
%X La montagne est un milieu vulnérable vis-à-vis du changement climatique comme l’indiquentdéjà les évolutions paléoclimatiques.Inédite à l’échelle des Pyrénées-Atlantiques, l’analyse de longues séries homogénéisées dedonnées sur la période 1950-2013 indique une augmentation des températures de +0,2 °C à +0,3 °C par décennie. Les projections climatiques établissent la poursuite du réchauffement :à l'horizon 2071-2100, selon le scénario RCP 8.5, le réchauffement pourrait atteindre 4°C. Enmatière d’enneigement, en dépit d’une forte variabilité inter-annuelle et d’une tendance plusmarquée à basse altitude, une perte de 2 à 3 jours d'enneigement par décennie est observéedepuis le début des années 1980 ; les projections futures présagent d’une accélérationsensible à partir des années 2050. Ces changements physiques ont des conséquencessur la biodiversité et le fonctionnement des écosystèmes ainsi qu’un impact économiqueet social majeur. Les aspects opérationnels de l’appropriation du changement climatique,des stratégies d’adaptation et d’atténuation adoptées, mis en oeuvre à différentes échellesterritoriales témoignent du besoin de mieux comprendre les interactions entre processusenvironnementaux et sociaux dans les espaces montagnards, avec au centre la question desperceptions et des représentations du changement climatique, qui demeure un volet essentielpour saisir les freins et les leviers potentiels à l’adaptation des sociétés locales. En cherchantune meilleure résilience climatique et une planification plus durable de la totalité du modèleéconomique actuel, anticipation et adaptation sont les grands défis auxquels les différentsterritoires de montagne devront faire face. Cela passe nécessairement par une connaissanceaccrue de la question climatique et des risques naturels associés.Concernant la montagne limousine, sa position en tête de bassin et ses ressourcesessentiellement de flux constituent un enjeu particulier vis-à-vis du changement climatique.Le maintien de zones humides et la biodiversité associée est conditionné par la préservationde la ressource en eau tandis que les pratiques agricoles, sylvicoles et d’élevage vont devoirquant à elles s’adapter à davantage de sécheresses. Sur le plan socio-environnemental, lescaractéristiques démographiques (faible densité, ménages modestes) et l’ancienneté du bâtirequièrent une attention particulière dans la mise en place de politiques d’accompagnementdes territoires vers une meilleure résilience.Nous, ou les générations futures, assisterons sans aucun doute à une recomposition despaysages montagnards. Sans réaction adaptée rapide, le risque est grand de laisser semodifier des services écologiques essentiels et d’impacter les activités et le bien-être d’unepartie de la population dépendant des ressources de haute et moyenne altitudes.
%G French
%2 https://hal.archives-ouvertes.fr/hal-01885271/document
%2 https://hal.archives-ouvertes.fr/hal-01885271/file/Rapport-AcclimaTerra.pdf
%L hal-01885271
%U https://hal.archives-ouvertes.fr/hal-01885271
%~ SHS
%~ UMR5319
%~ GIP-BE
%~ GEODE
%~ AO-GEOGRAPHIE
%~ LMA-PAU
%~ UNIV-PAU
%~ CNRS
%~ UNIV-TLSE2
%~ METEO
%~ LARA
%~ UNIV-BORDEAUX-MONTAIGNE
%~ PASSAGES-UPPA
%~ TESTUPPA2
%~ UPPA-OA
%~ MONTAIGNE_TABLE

%0 Unpublished work
%T Exploration dans la recherche en sciences sociales : mise en perspective, revue des pratiques, nouveaux défis. Enjeux scientifiques et prolongements de l'école thématique EXPLO-SHS
%+ École des hautes études en sciences sociales (EHESS)
%+ Transitions Energétiques et Environnementales (TREE)
%+ Centre Émile Durkheim (CED)
%+ Environnement Ville Société (EVS)
%+ Littoral, Environnement, Télédétection, Géomatique UMR 6554 (LETG)
%+ Centre de Sociologie de l'Innovation i3 (CSI i3)
%A Pascal, Cristofoli
%A Deletraz, Gaëlle
%A Le Hay, Viviane
%A Hélène, Mathian
%A Pierson, Julie
%A Frédéric, Vergnaud
%8 2021-01-29
%D 2021
%K Exploration
%K Sciences Humaines et Sociales
%K Pratiques de la recherche
%Z Humanities and Social SciencesPreprints, Working Papers, ...
%X Dans ce document, nous proposons un bilan et une réflexion sur les enjeux scientifiques et les prolongements des échanges et débats ayant eu lieu lors de la tenue de l’école thématique « Exploration dans la recherche en sciences sociales : mise en perspective, revue des pratiques, nouveaux défis »7 (12-16 octobre 2020, La Rochelle).
%G French
%Z Ecole Thématique MATE-SHS / CNRS "EXPLO-SHS" 2020
%2 https://hal.archives-ouvertes.fr/hal-03125855/document
%2 https://hal.archives-ouvertes.fr/hal-03125855/file/Explorer_en_SHS_Bilan_Ecole_Thematique_2020.pdf
%L hal-03125855
%U https://hal.archives-ouvertes.fr/hal-03125855
%~ UR2-HB
%~ UNIV-LYON3
%~ UNIV-BREST
%~ EHESS
%~ ENSMP_CSI
%~ PSL
%~ LETG
%~ UNIV-PAU
%~ ENS-LYON
%~ INSTITUTS-TELECOM
%~ UNIV-ST-ETIENNE
%~ ENSMP
%~ INSA-LYON
%~ ENSMP_DEP_EMS
%~ ENTPE
%~ UNIV-NANTES
%~ UNIV-LYON2
%~ LYON2
%~ UNICAEN
%~ EPHE
%~ TEST-DEV
%~ UNIV-ANGERS
%~ CNRS
%~ UDL
%~ UNIV-LYON
%~ SHS
%~ ENSMP_DR
%~ UNIV-RENNES2
%~ COMUE-NORMANDIE
%~ ENSMP-PSL
%~ EPHE-PSL
%~ INSA-GROUPE
%~ TESTUPPA2
%~ UPPA-OA
%~ INSTITUT-TELECOM
%~ PARISTECH
%~ UNIV-RENNES
%~ UMR6031TREE
%~ CENTREDURKHEIM
%~ EVS_UMR5600
%~ UNIV-BORDEAUX

%0 Report
%T La transcription automatique : un rêve enfin accessible ?
%T Analyse et comparaison d’outils pour les SHS. Nouvelle méthodologie et résultats
%+ Université de Genève (UNIGE)
%+ Maison des Sciences de l'Homme Val de Loire (MSH VL)
%+ Passages
%+ Centre d'études et de recherches sur les qualifications (CEREQ)
%+ Centre Émile Durkheim (CED)
%+ Analyse et Traitement Informatique de la Langue Française (ATILF)
%+ Interactions, Corpus, Apprentissages, Représentations (ICAR)
%A Tancoigne, Elise
%A Corbellini, Jean-Philippe
%A Deletraz, Gaëlle
%A Gayraud, Laure
%A Ollinger, Sandrine
%A Valéro, Daniel
%I MATE-SHS
%8 2020-08-20
%D 2020
%K interview transcription
%K research data
%K methodology
%K software evaluation
%K speech corpora
%K automatic transcription
%K corpus oraux
%K transcription automatique
%K retranscription entretien
%K données de la recherche
%K méthodologie
%K évaluation logiciels
%Z Humanities and Social SciencesReports
%X Le recueil de la parole est au cœur des démarches de recherches qualitatives de nombreusesdisciplines de sciences humaines et sociales. Depuis la démocratisation des outils d’enregistrementdans les années 80 et surtout 90, la pratique de la transcription de l’intégralité de la paroleenregistrée est devenue quasiment la norme, mais elle demande beaucoup de temps ets’avère souvent fastidieuse et un peu décourageante. À l’heure de l’intégration de modulesd’intelligence artificielle aux algorithmes de reconnaissance automatique de la parole, ces derniersprogressent rapidement et le fantasme de pouvoir automatiser cette tâche longue et péniblesemble se rapprocher, voire être déjà accessible.Ce rapport présente le résultat d’un travail de comparaison de 8 outils de transcription automatique(Go Transcribe, Happy Scribe, Headliner, Sonix, Video Indexer, Vocalmatic, Vocapia, You-Tube) effectué par des membres du réseau méthodologique CNRS MATE-SHS. Quatre extraits defichiers audio de langue française ont servi de test, chacun avec ses spécificités propres : untexte lu, un cours magistral enregistré en situation, un entretien avec deux interlocuteurs, uneréunion associative avec de nombreux locuteurs.
%G French
%2 https://halshs.archives-ouvertes.fr/halshs-02917916v2/document
%2 https://halshs.archives-ouvertes.fr/halshs-02917916v2/file/Tancoigne_et_al_2020_v2.pdf
%L halshs-02917916
%U https://halshs.archives-ouvertes.fr/halshs-02917916
%~ INRP
%~ ATILF
%~ UNIV-PAU
%~ ENS-LYON
%~ UNIV-LORRAINE
%~ CEREQ
%~ UNIV-ORLEANS
%~ ICAR
%~ LYON2
%~ UNIV-LYON2
%~ UNIV-TOURS
%~ UNIV-BORDEAUX-MONTAIGNE
%~ UMR5319
%~ CNRS
%~ UDL
%~ PASSAGES-UPPA
%~ LARA
%~ SHS
%~ RNMSH
%~ CENTREDURKHEIM
%~ TESTUPPA2
%~ UPPA-OA
%~ UNIV-BORDEAUX

