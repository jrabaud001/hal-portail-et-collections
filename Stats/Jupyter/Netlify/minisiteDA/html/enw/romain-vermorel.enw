%0 Book Section
%T Poromechanics of saturated isotropic nanoporous materials
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Vermorel, Romain
%A Pijaudier-Cabot, Gilles
%A Miqueu, Christelle
%A Mendiboure, Bruno
%B Damage Mechanics of Cementitious Materials and Structures
%E Gilles Pijaudier-Cabot and Frédéric Dufour
%I John Wiley & Sons, Inc.
%P 19-50
%8 2013
%D 2013
%R 10.1002/9781118562086.ch2
%Z Physics [physics]/Mechanics [physics]/Mechanics of the solides [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of the solides [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Materials and structures in mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Materials and structures in mechanics [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Mechanics of materials [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of materials [physics.class-ph]Book sections
%X Poromechanics offers a consistent theoretical framework for describing the mechanical response of porous solids. When dealing with fully saturated nanoporous materials, which exhibit pores of the nanometer size, additional effects due to adsorption and confinement of the fluid molecules in the smallest pores must be accounted for. From the mechanical point of view, these phenomena result into volumetric deformations of the porous solid, the so-called "swelling" phenomenon, and into a change of the apparent permeability. The present work investigates how poromechanics may be refined in order to capture adsorption and molecular-packing-induced effects in nanoporous solids. The revisited formulation introduces an effective pore pressure, defined as a thermodynamic variable at the representative volume element scale (mesoscale), which is related to the mechanical work of the fluid at the pore scale (nanoscale). Accounting for the thermodynamic equilibrium of the system, this effective pore pressure is obtained as a function of the bulk fluid pressure, the temperature, and the total and excess adsorbed masses of fluid. We derive the analytical swelling strains due to sorption and molecular packing. A good agreement in the comparison with experimental data dealing with the swelling of coal due to methane and carbon dioxide sorption is observed, as a preliminary stage toward modeling partially saturated solids and applications to cement paste.
%G English
%2 https://hal.archives-ouvertes.fr/hal-00856277/document
%2 https://hal.archives-ouvertes.fr/hal-00856277/file/Chap-2-final.pdf
%L hal-00856277
%U https://hal.archives-ouvertes.fr/hal-00856277
%~ CNRS
%~ LFCR-GEOMECA
%~ UNIV-PAU
%~ LFCR
%~ LFCR-PTP
%~ LFCR-ISD
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Revisiting poromechanics in the context of microporous materials
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Pijaudier-Cabot, Gilles
%A Vermorel, Romain
%A Miqueu, Christelle
%A Mendiboure, Bruno
%Z TOTAL Tight Gas
%< avec comité de lecture
%@ 1631-0721
%J Comptes Rendus Mécanique
%I Elsevier Masson
%V 339
%P 770-778
%8 2011-10-01
%D 2011
%R 10.1016/j.crme.2011.09.003
%K Porous media
%K Poromechanics
%K Microporous materials
%K Swelling
%K Adsorption
%K Fluid confinement
%K Poroelasticity
%Z Physics [physics]/Mechanics [physics]/Mechanics of materials [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of materials [physics.class-ph]Journal articles
%X Poromechanics offers a consistent theoretical framework for describing the mechanical response of porous solids, fully or partially saturated with a fluid phase. When dealing with fully saturated microporous materials, which exhibit pores of the nanometre size, aside from the fluid pressure acting on the pore walls additional effects due to adsorption and confinement of the fluid molecules in the smallest pores must be accounted for. From the mechanical point of view, these phenomena result into volumetric deformations of the porous solid: the so-called "swelling" phenomenon. The present work investigates how the poromechanical theory should be refined in order to describe adsorption and confinement induced swelling in microporous solids. Firstly, we report molecular simulation results that show that the pressure and density of the fluid in the smallest pores are responsible for the volumetric deformation of the material. Secondly, poromechanics is revisited in the context of a microporous material with a continuous pore size distribution. Accounting for the thermodynamic equilibrium of the fluid phase in the overall pore space, the new formulation introduces an apparent porosity and an interaction free energy. We use a prototype constitutive relation relating these two quantities to the Gibbs adsorption isotherm, and then calculate the induced deformation of the solid matrix. Agreement with experimental data found in the literature is observed. As an illustrating example, we show the predicted strains in the case of adsorption of methane on activated carbon.
%G English
%2 https://hal.archives-ouvertes.fr/hal-00643305/document
%2 https://hal.archives-ouvertes.fr/hal-00643305/file/art-poro-ads-corrigeI_.pdf
%L hal-00643305
%U https://hal.archives-ouvertes.fr/hal-00643305
%~ CNRS
%~ LFCR-GEOMECA
%~ UNIV-PAU
%~ LFCR
%~ LFCR-PTP
%~ LFCR-ISD
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Impact of Thermodiffusion on the Initial Vertical Distribution of Species in Hydrocarbon Reservoirs
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%+ TOTAL-Scientific and Technical Center Jean Féger (CSTJF)
%A Galliero, Guillaume
%A Bataller, Henri
%A Croccolo, Fabrizio
%A Vermorel, Romain
%A Artola, P.-A.
%A Rousseau, B.
%A Vesovic, V.
%A Bou-Ali, M.
%A Zárate, J.M., Ortiz de
%A Xu, S.
%A Zhang, K.
%A Montel, F.
%Z ACL
%< avec comité de lecture
%@ 0938-0108
%J Microgravity Science and Technology
%I Springer
%V 28
%N 2
%P 79-86
%8 2016
%D 2016
%R 10.1007/s12217-015-9465-6
%K Segregation
%K Oil and gas
%K Thermodiffusion
%K Molecular dynamics
%K Multicomponent mixtures
%Z Sciences of the Universe [physics]/Earth Sciences/Geophysics [physics.geo-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Fluids mechanics [physics.class-ph]Journal articles
%X In this work we propose a methodology, based on molecular dynamics simulations, to quantify the influence of segregation and thermodiffusion on the initial state distribution of the fluid species in hydrocarbon reservoirs. This convection-free approach has been applied to a synthetic oil composed of three normal alkanes and to a real acid gas. It has been found that the thermodiffusion effect induced by the geothermal gradient is similar (but opposite in sign) to that due to segregation for both mixtures. In addition, because of the combined effect of thermal expansion and thermodiffusion, it has been observed that the density gradient can be reversed, in the presence of a geothermal gradient. These numerical results emphasize the need of improving our quantification of thermodiffusion in multicomponent mixtures. The SCCO-SJ10 experiments will be a crucial step towards this goal. © 2015, Springer Science+Business Media Dordrecht.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01804384/document
%2 https://hal.archives-ouvertes.fr/hal-01804384/file/SCCO-SJ10_MST.pdf
%L hal-01804384
%U https://hal.archives-ouvertes.fr/hal-01804384
%~ LFCR
%~ CNRS
%~ UNIV-PAU
%~ LFCR-PTP
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Thermodiffusion in multicomponent n-alkane mixtures
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%+ TOTAL-Scientific and Technical Center Jean Féger (CSTJF)
%A Galliero, Guillaume
%A Bataller, Henri
%A Bazile, Jean-Patrick
%A Diaz, Joseph
%A Croccolo, Fabrizio
%A Hoang, H.
%A Vermorel, Romain
%A Artola, P.-A.
%A Rousseau, B.
%A Vesovic, V.
%A Bou-Ali, M.
%A Zárate, J.M., Ortiz de
%A Xu, S.
%A Zhang, K.
%A Montel, F.
%A Verga, A.
%A Minster, O.
%Z ACL
%< avec comité de lecture
%J NPJ Microgravity
%I Nature Publishing Group
%V 3
%P 22
%8 2017
%D 2017
%R 10.1038/s41526-017-0026-8
%Z Physics [physics]
%Z Sciences of the Universe [physics]/Earth Sciences/Geophysics [physics.geo-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Fluids mechanics [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Mechanics of materials [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Mechanical engineering [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of materials [physics.class-ph]Journal articles
%X Compositional grading within a mixture has a strong impact on the evaluation of the pre-exploitation distribution of hydrocarbons in underground layers and sediments. Thermodiffusion, which leads to a partial diffusive separation of species in a mixture due to the geothermal gradient, is thought to play an important role in determining the distribution of species in a reservoir. However, despite recent progress, thermodiffusion is still difficult to measure and model in multicomponent mixtures. In this work, we report on experimental investigations of the thermodiffusion of multicomponent n-alkane mixtures at pressure above 30 MPa. The experiments have been conducted in space onboard the Shi Jian 10 spacecraft so as to isolate the studied phenomena from convection. For the two exploitable cells, containing a ternary liquid mixture and a condensate gas, measurements have shown that the lightest and heaviest species had a tendency to migrate, relatively to the rest of the species, to the hot and cold region, respectively. These trends have been confirmed by molecular dynamics simulations. The measured condensate gas data have been used to quantify the influence of thermodiffusion on the initial fluid distribution of an idealised one dimension reservoir. The results obtained indicate that thermodiffusion tends to noticeably counteract the influence of gravitational segregation on the vertical distribution of species, which could result in an unstable fluid column. This confirms that, in oil and gas reservoirs, the availability of thermodiffusion data for multicomponent mixtures is crucial for a correct evaluation of the initial state fluid distribution.
%G English
%2 https://hal.archives-ouvertes.fr/hal-01804368/document
%2 https://hal.archives-ouvertes.fr/hal-01804368/file/NPJMUG_3_20_2017.pdf
%L hal-01804368
%U https://hal.archives-ouvertes.fr/hal-01804368
%~ LFCR
%~ CNRS
%~ UNIV-PAU
%~ LFCR-PTP
%~ LFCR-ISD
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Transport of Multicomponent Hydrocarbon Mixtures in Shale Organic Matter by Molecular Simulations
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%+ Material Design SARL
%+ Centre scientifique et Technique Jean Feger (CSTJF)
%A Collell, Julien
%A Galliero, Guillaume
%A Vermorel, Romain
%A Ungerer, Philippe
%A Yiannourakou, Marianna
%A Montel, François
%A Pujol, Magali
%< avec comité de lecture
%@ 1932-7447
%J Journal of Physical Chemistry C
%I American Chemical Society
%V 119
%N 39
%P 22587-22595
%8 2015-09-11
%D 2015
%R 10.1021/acs.jpcc.5b07242
%K Transport coefficient Engineering main heading: Gases
%K Oil and Gas Industry
%K Natural-gas production
%K Molecular simulations
%K Molecular dynamics simulations
%K Micro-porous structure
%K Micro porous membranes
%K Paraffins
%K Organic compounds
%K Oil shale
%K Natural gas well production
%K Shale oil Autocorrelation coefficient
%K Permeation
%K Shale
%K Engineering controlled terms: Autocorrelation
%K Biogeochemistry
%K Biological materials
%K Carbon
%K Gas condensates
%K Gas industry
%K Hydrocarbons
%K Kerogen
%K Membranes
%K Microporosity
%K Microporous materials
%K Molecular dynamics
%K Molecular structure
%K Natural gas
%Z Engineering Sciences [physics]Journal articles
%X During the past decade, gas recovered from shale reservoirs has jumped from 2 to 40% of natural gas production in the United States. However, in response to the drop of gas prices, the oil and gas industry has set its sights on the oil-prone shale plays, potentially more lucrative. This shift from dry to condensate-rich gas has raised the need for a better understanding of the transport of hydrocarbon mixtures through organic-rich shale reservoirs. At the micrometer scale, hydrocarbons in shales are mostly located in amorphous microporous nodules of organic matter, the so-called kerogen, dispersed in an heterogeneous mineral matrix. In such multiscale materials, a wide range of physical mechanisms might affect the composition of the recovered hydrocarbon mixtures. More specifically, kerogen nodules are likely to act as selective barriers due to their amorphous microporous structure. In this work, we study the transport of hydrocarbon mixtures through kerogen by means of molecular simulations. We performed molecular dynamics simulations of hydrocarbons permeating through a molecular model representative of oil-prone type II kerogen. Our results show that the permeation mechanisms through this type of material is purely diffusive. Consequently, we have computed the Onsager's species-specific transport coefficients of a typical condensate-rich gas mixture within kerogen. Interestingly, we have observed that the transport coefficients matrix can be reasonably approximated by its diagonal terms, the so-called Onsager's autocorrelation coefficients. Inspired by the classical Rouse model of polymer dynamics and surface diffusion theory, we propose a simple scaling law to predict the transport coefficient of linear alkanes in the mixture. In good agreement with our simulations results, the Onsager's autocorrelation coefficients scale linearly with the adsorption loading and inversely with the alkane chain length. We believe our results and predictions are applicable to other materials, such as carbon-based synthetic microporous membranes, with structural properties close to that of kerogen.
%G English
%L hal-01278630
%U https://hal.archives-ouvertes.fr/hal-01278630
%~ CNRS
%~ UNIV-PAU
%~ LORIA2
%~ LFCR
%~ LFCR-PTP
%~ TESTUPPA2
%~ UPPA-OA

%0 Conference Paper
%F Oral 
%T Effects of surfaces on the mechanical properties of nanoscale materials. A simulation study.
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Hantal, Gyorgy
%A Vermorel, Romain
%A Perrier, Laurent
%A Grégoire, David
%A Galliero, Guillaume
%A Pijaudier-Cabot, Gilles
%< avec comité de lecture
%Z Milieux micro-poreux
%B 13èmes Journéess d'études des Milieux Poreux 2016
%C Anglet, France
%8 2016-10-11
%D 2016
%K surface effects
%K elasticity
%K molecular simulations
%K nanomaterials
%Z Physics [physics]/Mechanics [physics]Conference papers
%G English
%2 https://hal.archives-ouvertes.fr/hal-01394474/document
%2 https://hal.archives-ouvertes.fr/hal-01394474/file/hantal_JEMP_mod.pdf
%L hal-01394474
%U https://hal.archives-ouvertes.fr/hal-01394474
%~ JEMP2016
%~ CNRS
%~ UNIV-PAU
%~ LFCR
%~ LFCR-PTP
%~ TESTUPPA2
%~ UPPA-OA

%0 Conference Paper
%F Oral 
%T Perméation de gaz à travers une constriction nanoporeuse : théorie et simulations
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Oulebsir, Fouad
%A Vermorel, Romain
%A Galliero, Guillaume
%< avec comité de lecture
%Z Milieux micro-poreux
%B 13èmes Journéess d'études des Milieux Poreux 2016
%C Anglet, France
%8 2016-10-11
%D 2016
%K Matériaux nanoporeux
%K Propriétés de transport
%K Dynamique Moléculaire
%K Diffusion moléculaire
%Z Physics [physics]/Mechanics [physics]Conference papers
%X Perméation de gaz à travers une constriction nanoporeuse : théorie et simulations
%G French
%2 https://hal.archives-ouvertes.fr/hal-01394557/document
%2 https://hal.archives-ouvertes.fr/hal-01394557/file/Abstract_JEMP_Oulebsir.pdf
%L hal-01394557
%U https://hal.archives-ouvertes.fr/hal-01394557
%~ JEMP2016
%~ CNRS
%~ UNIV-PAU
%~ LFCR
%~ LFCR-PTP
%~ TESTUPPA2
%~ UPPA-OA

%0 Conference Paper
%F Oral 
%T Transport of Multicomponent Hydrocarbon Mixtures in Shale Organic Matter by Molecular Simulations
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Collell, Julien
%A Galliero, Guillaume
%A Vermorel, Romain
%A Ungerer, Philippe
%A Yiannourakou, Marianna
%A Montel, François
%A Pujol, Magali
%A Gelin, François
%< avec comité de lecture
%Z Génie Pétrolier
%B 13èmes Journéess d'études des Milieux Poreux 2016
%C Anglet, France
%8 2016-10-11
%D 2016
%K Modeling
%K Molecular Simulations
%K Nanoporous Membrane
%K Multicomponent Transport
%Z Physics [physics]/Mechanics [physics]Conference papers
%G English
%2 https://hal.archives-ouvertes.fr/hal-01394503/document
%2 https://hal.archives-ouvertes.fr/hal-01394503/file/Abstract_COLLELL_JEMP.pdf
%L hal-01394503
%U https://hal.archives-ouvertes.fr/hal-01394503
%~ JEMP2016
%~ CNRS
%~ UNIV-PAU
%~ LFCR
%~ LFCR-PTP
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T European Space Agency experiments on thermodiffusion of fluid mixtures in space
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%+ Manufacturing Department (MGEP)
%+ Instituto de Fisica Corpuscular (IFIC)
%+ Deutsches Elektronen-Synchrotron [Zeuthen] (DESY)
%+ TOTAL-Scientific and Technical Center Jean Féger (CSTJF)
%+ Institut de recherches sur la catalyse (IRC)
%+ Microgravity Research Center
%+ China Jiliang University (CJLU)
%+ department of polymer science and engineering
%A Braibanti, M.
%A Artola, P. -A.
%A Baaske, P.
%A Bataller, Henri
%A Bazile, J. -P.
%A Bou-Ali, M.
%A Cannell, D.
%A Carpineti, M.
%A Cerbino, R.
%A Croccolo, F.
%A Diaz, Joseph
%A Donev, A.
%A Errarte, A.
%A Ezquerro, J.
%A Frutos-Pastor, A.
%A Galand, Q.
%A Galliero, Guillaume
%A Gaponenko, Y.
%A García-Fernández, L.
%A Gavaldá, J.
%A Giavazzi, F.
%A Giglio, M.
%A Giraudet, C.
%A Hoang, H.
%A Kufner, E.
%A Köhler, W.
%A Lapeira, E.
%A Laverón-Simavilla, A.
%A Legros, J. -C.
%A Lizarraga, I.
%A Lyubimova, T.
%A Mazzoni, S.
%A Melville, N.
%A Mialdun, A.
%A Minster, O.
%A Montel, F.
%A Molster, F.
%A Ortiz de Zárate, J.
%A Rodríguez, J.
%A Rousseau, B.
%A Ruiz, X.
%A Ryzhkov, I.
%A Schraml, M.
%A Shevtsova, V.
%A Takacs, C.
%A Triller, T.
%A Van Vaerenbergh, S.
%A Vailati, A.
%A Verga, A.
%A Vermorel, R.
%A Vesovic, V.
%A Yasnou, V.
%A Xu, S.
%A Zapf, D.
%A Zhang, K.
%< avec comité de lecture
%@ 1292-8941
%J European Physical Journal E: Soft matter and biological physics
%I EDP Sciences: EPJ
%V 42
%N 7
%8 2019-07
%D 2019
%R 10.1140/epje/i2019-11849-0
%Z Physics [physics]/Physics [physics]/Chemical Physics [physics.chem-ph]Journal articles
%G English
%L hal-02418855
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02418855
%~ UNIV-PAU
%~ IRC
%~ LFCR
%~ CNRS
%~ LFCR-PTP
%~ LFCR-ISD
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Communication: A method to compute the transport coefficient of pure fluids diffusing through planar interfaces from equilibrium molecular dynamics simulations
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Vermorel, Romain
%A Oulebsir, F.
%A Galliero, Guillaume
%Z ACL
%< avec comité de lecture
%@ 0021-9606
%J Journal of Chemical Physics
%I American Institute of Physics
%V 147
%N 10
%P 101102
%8 2017
%D 2017
%R 10.1063/1.4997865
%Z Physics [physics]/Condensed Matter [cond-mat]/Materials Science [cond-mat.mtrl-sci]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Fluids mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of materials [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Mechanical engineering [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Mechanics of materials [physics.class-ph]
%Z Sciences of the Universe [physics]/Earth Sciences/Geophysics [physics.geo-ph]Journal articles
%X The computation of diffusion coefficients in molecular systems ranks among the most useful applications of equilibrium molecular dynamics simulations. However, when dealing with the problem of fluid diffusion through vanishingly thin interfaces, classical techniques are not applicable. This is because the volume of space in which molecules diffuse is ill-defined. In such conditions, non-equilibrium techniques allow for the computation of transport coefficients per unit interface width, but their weak point lies in their inability to isolate the contribution of the different physical mechanisms prone to impact the flux of permeating molecules. In this work, we propose a simple and accurate method to compute the diffusional transport coefficient of a pure fluid through a planar interface from equilibrium molecular dynamics simulations, in the form of a diffusion coefficient per unit interface width. In order to demonstrate its validity and accuracy, we apply our method to the case study of a dilute gas diffusing through a smoothly repulsive single-layer porous solid. We believe this complementary technique can benefit to the interpretation of the results obtained on single-layer membranes by means of complex non-equilibrium methods. © 2017 Author(s).
%G English
%L hal-01816742
%U https://hal.archives-ouvertes.fr/hal-01816742
%~ CNRS
%~ UNIV-PAU
%~ LFCR
%~ LFCR-PTP
%~ TESTUPPA2
%~ UPPA-OA

%0 Book Section
%T SCCO: Thermodiffusion for the Oil and Gas Industry
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%+ Laboratoire de Chimie Physique D'Orsay (LCPO)
%+ Cooperative Institute for Mesoscale Meteorological Studies (CIMMS)
%+ Centre scientifique et Technique Jean Feger (CSTJF)
%A Galliero, Guillaume
%A Bataller, Henri
%A Bazile, Jean-Patrick
%A Diaz, Joseph
%A Croccolo, Fabrizio
%A Hoang, Hai
%A Vermorel, Romain
%A Artola, Pierre-Arnaud
%A Rousseau, Bernard
%A Vesovic, Velisa
%A Bou-Ali, M. Mounir
%A de Zárate, José
%A Xu, Shenghua
%A Zhang, Ke
%A Montel, François
%A Verga, Antonio
%A Minster, Olivier
%B Physical Science Under Microgravity: Experiments on Board the SJ-10 Recoverable Satellite
%P 171-190
%8 2019-10-17
%D 2019
%R 10.1007/978-981-13-1340-0_8
%Z Physics [physics]/Physics [physics]/Space Physics [physics.space-ph]
%Z Physics [physics]/Physics [physics]/Chemical Physics [physics.chem-ph]Book sections
%G English
%2 https://hal.archives-ouvertes.fr/hal-02354282/document
%2 https://hal.archives-ouvertes.fr/hal-02354282/file/book_SCCO_vf.pdf
%L hal-02354282
%U https://hal.archives-ouvertes.fr/hal-02354282
%~ CNRS
%~ LFCR
%~ UNIV-PSUD-SACLAY
%~ UNIV-PARIS-SACLAY
%~ UNIV-PSUD
%~ UNIV-PAU
%~ INC-CNRS
%~ LFCR-PTP
%~ LFCR-ISD
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Molecular Simulations of Supercritical Fluid Permeation through Disordered Microporous Carbons
%+ Massachusetts Institute of Technology (MIT)
%+ Multiscale Material Science for Energy and Environment (MSE 2)
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%+ Centre Interdisciplinaire de Nanoscience de Marseille (CINaM)
%A Botan, Alexandru
%A Vermorel, Romain
%A Ulm, Franz-Josef
%A Pellenq, Roland J.-M.
%< avec comité de lecture
%Z UPR3118_1142
%@ 0743-7463
%J Langmuir
%I American Chemical Society
%V 29
%N 32
%P 9985-9990
%8 2013-07-25
%D 2013
%R 10.1021/la402087r
%Z Physics [physics]/Physics [physics]/Chemical Physics [physics.chem-ph]
%Z Physics [physics]/Physics [physics]/Atomic and Molecular Clusters [physics.atm-clus]
%Z Physics [physics]/Condensed Matter [cond-mat]/Materials Science [cond-mat.mtrl-sci]
%Z Physics [physics]/Mechanics [physics]/Mechanics of the fluids [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Fluids mechanics [physics.class-ph]
%Z Physics [physics]/Physics [physics]/Fluid Dynamics [physics.flu-dyn]
%Z Engineering Sciences [physics]/Chemical and Process Engineering
%Z Engineering Sciences [physics]/Materials
%Z Engineering Sciences [physics]/Reactive fluid environmentJournal articles
%X Fluid transport through microporous carbon-based materials is inherent in numerous applications, ranging from gas separation by carbon molecular sieves to natural gas production from coal seams and gas shales. The present study investigates the steady-state permeation of supercritical methane in response to a constant cross-membrane pressure drop. We performed dual control volume grand canonical molecular dynamics (DCV-GCMD) simulations to mimic the conditions of actual permeation experiments. To overcome arbitrary assumptions regarding the investigated porous structures, the membranes were modeled after the CS1000a and CS1000 molecular models, which are representative of real microporous carbon materials. When adsorption-induced molecular trapping (AIMT) mechanisms are negligible, we show that the permeability of the microporous material, although not significantly sensitive to the pressure gradient, monotonically decreases with temperature and reservoir pressures, consistent with diffusion theory. However, when AIMT occurs, the permeability increases with temperature in agreement with experimental data found in the literature.
%G English
%2 https://hal.archives-ouvertes.fr/hal-00856240/document
%2 https://hal.archives-ouvertes.fr/hal-00856240/file/ep4_lang.pdf
%L hal-00856240
%U https://hal.archives-ouvertes.fr/hal-00856240
%~ CNRS
%~ UNIV-AMU
%~ CINAM
%~ UNIV-PAU
%~ LFCR
%~ LFCR-PTP
%~ TESTUPPA2
%~ UPPA-OA

%0 Journal Article
%T Impacts on thin elastic sheets
%+ Institut de Recherche sur les Phénomènes Hors Equilibre (IRPHE)
%A Vermorel, Romain
%A Vandenberghe, Nicolas
%A Villermaux, Emmanuel
%Z DGA
%< avec comité de lecture
%@ 1364-5021
%J Proceedings of the Royal Society A: Mathematical, Physical and Engineering Sciences
%I Royal Society, The
%N 465
%P 823-842
%8 2009
%D 2009
%R 10.1098/rspa.2008.0297
%K membrane
%K impact
%K buckling
%K wrinkling
%Z Physics [physics]/Mechanics [physics]/Mechanics of the solides [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of the solides [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Mechanics of materials [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of materials [physics.class-ph]
%Z Physics [physics]/Physics [physics]/Classical Physics [physics.class-ph]
%Z Physics [physics]/Mathematical Physics [math-ph]
%Z Mathematics [math]/Mathematical Physics [math-ph]
%Z Physics [physics]/Mechanics [physics]/Materials and structures in mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Materials and structures in mechanics [physics.class-ph]
%Z Physics [physics]/Physics [physics]/General Physics [physics.gen-ph]Journal articles
%X We study transverse impacts of rigid objects on a free elastic membrane, using thin circular sheets of natural rubber as experimental models. After impact, two distinct axisymmetric waves propagate in and on the sheet. First a tensile wave travels at sound speed leaving behind the wave front a stretched domain. Then, a transverse wave propagates on the stretched area at a lower speed. In the stretched area, geometrical confinement induces compressive circumferential stresses leading to a buckling instability, giving rise to radial wrinkles. We report on a set of experiments and theoretical remarks on the conditions of occurrence of these wrinkles, their dynamics and wavelength.
%G English
%2 https://hal.archives-ouvertes.fr/hal-00857254/document
%2 https://hal.archives-ouvertes.fr/hal-00857254/file/Impacts.pdf
%L hal-00857254
%U https://hal.archives-ouvertes.fr/hal-00857254
%~ CNRS
%~ INSMI
%~ EC-MARSEILLE
%~ UNIV-AMU
%~ IRPHE
%~ TDS-MACS
%~ ANR

%0 Journal Article
%T Rubber Band Recoil
%+ Institut de Recherche sur les Phénomènes Hors Equilibre (IRPHE)
%A Vermorel, Romain
%A Vandenberghe, Nicolas
%A Villermaux, Emmanuel
%< avec comité de lecture
%@ 0370-1662
%J Proceedings of the Royal Society of London
%I Royal Society, The
%V 463
%N 2079
%P 641-658
%8 2007
%D 2007
%R 10.1098/rspa.2006.1781
%K Rubber band
%K elastic instability
%K dynamic buckling
%Z Physics [physics]/Mechanics [physics]/Mechanics of the fluids [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Fluids mechanics [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Mechanics of the solides [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of the solides [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Mechanics of materials [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of materials [physics.class-ph]Journal articles
%X When an initially stretched rubber band is suddenly released at one end, an axial-stress front propagating at the celerity of sound separates a free and a stretched domain of the elastic material. As soon as it reaches the clamped end, the front rebounds and a compression front propagates backward. When the length of the compressed area exceeds Euler critical length, a dynamic buckling instability develops. The rebound is analysed using Saint-Venant's theory of impacts and we use a dynamical extension of the Euler–Bernoulli beam equation to obtain a relation between the buckled wavelength, the initial stretching and the rubber band thickness. The influence of an external fluid medium is also considered: owing to added mass and viscosity, the instability growth rate decreases. With a high viscosity, the axial-stress front spreads owing to viscous frictional forces during the release phase. As a result, the selected wavelength increases significantly.
%G English
%2 https://hal.archives-ouvertes.fr/hal-00087503/document
%2 https://hal.archives-ouvertes.fr/hal-00087503/file/RubberBand.pdf
%L hal-00087503
%U https://hal.archives-ouvertes.fr/hal-00087503
%~ IRPHE
%~ CNRS
%~ EC-MARSEILLE
%~ UNIV-AMU
%~ TEST-AMU

%0 Journal Article
%T Star shaped crack pattern of broken windows
%+ Institut de Recherche sur les Phénomènes Hors Equilibre (IRPHE)
%A Vandenberghe, Nicolas
%A Vermorel, Romain
%A Villermaux, Emmanuel
%Z DGA
%< avec comité de lecture
%@ 0031-9007
%J Physical Review Letters
%I American Physical Society
%V 110
%N 17
%P 174302
%8 2013-04-26
%D 2013
%R 10.1103/PhysRevLett.110.174302
%K fracture energy
%K impacts
%K fragmentation
%K brittle fracture
%K radial cracks
%K forensics
%K plates
%Z Physics [physics]/Mechanics [physics]/Mechanics of the solides [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of the solides [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Mechanics of materials [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of materials [physics.class-ph]
%Z Physics [physics]/Physics [physics]/Classical Physics [physics.class-ph]
%Z Physics [physics]/Mathematical Physics [math-ph]
%Z Mathematics [math]/Mathematical Physics [math-ph]
%Z Physics [physics]/Mechanics [physics]/Materials and structures in mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Materials and structures in mechanics [physics.class-ph]
%Z Physics [physics]/Physics [physics]/General Physics [physics.gen-ph]Journal articles
%X Broken thin brittle plates like windows and windshields are ubiquitous in our environment. When impacted locally, they typically present a pattern of cracks extending radially outwards from the impact point. We study the variation of the pattern of cracks by performing controlled transverse impacts on brittle plates over a broad range of impact speed, plate thickness and material properties, and we establish from experiments a global scaling law for the number of radial cracks incorporating all these parameters. A model based on Griffith's theory of fracture combining plate bending elastic energy and fracture energy accounts for our observations. These findings indicate how the post-mortem shape of broken samples are related to material properties and impact parameters, a procedure relevant to forensic science, archaeology or astrophysics.
%G English
%2 https://hal.archives-ouvertes.fr/hal-00857239/document
%2 https://hal.archives-ouvertes.fr/hal-00857239/file/window.pdf
%L hal-00857239
%U https://hal.archives-ouvertes.fr/hal-00857239
%~ CNRS
%~ INSMI
%~ EC-MARSEILLE
%~ UNIV-AMU
%~ IRPHE
%~ TDS-MACS
%~ TEST-AMU

%0 Journal Article
%T Impact on thin elastic sheets
%+ Institut de Recherche sur les Phénomènes Hors Equilibre (IRPHE)
%A Vermorel, Romain
%A Vandenberghe, Nicolas
%A Villermaux, Emmanuel
%< avec comité de lecture
%J Proc. Roy. Soc. A
%I Royal Society Publishing
%V 465
%N 2103
%P 823-842
%8 2009
%D 2009
%R 10.1098/rspa.2008.0297Journal articles
%G English
%L hal-00462960
%U https://hal.archives-ouvertes.fr/hal-00462960
%~ CNRS
%~ IRPHE
%~ EC-MARSEILLE
%~ UNIV-AMU
%~ TEST-AMU

%0 Journal Article
%T Radial cracks in perforated thin sheets
%+ Institut de Recherche sur les Phénomènes Hors Equilibre (IRPHE)
%A Vermorel, Romain
%A Vandenberghe, Nicolas
%A Villermaux, Emmanuel
%< avec comité de lecture
%@ 0031-9007
%J Physical Review Letters
%I American Physical Society
%N 104
%P 175502
%8 2010-04-30
%D 2010
%R 10.1103/PhysRevLett.104.175502
%K radial cracks
%K thin sheets
%K elasticity
%K fragmentation
%K perforation
%K fracture energy
%Z Physics [physics]/Mechanics [physics]/Mechanics of the solides [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of the solides [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Mechanics of materials [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of materials [physics.class-ph]
%Z Physics [physics]/Physics [physics]/Classical Physics [physics.class-ph]
%Z Physics [physics]/Mathematical Physics [math-ph]
%Z Mathematics [math]/Mathematical Physics [math-ph]
%Z Physics [physics]/Mechanics [physics]/Materials and structures in mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Materials and structures in mechanics [physics.class-ph]
%Z Physics [physics]/Physics [physics]/General Physics [physics.gen-ph]Journal articles
%X When a rigid cone is slowly pushed through a thin elastic sheet, the material breaks, exhibiting a network of cracks expanding in the radial direction. Experiments conducted with aluminum sheets show that the number of cracks is selected at the beginning of the perforation process and then remains stable. A simple model predicts the number of cracks as the result of a competition between the elastic energy stored in the sheet, and the energy dissipated during crack extension. We also evidence the subtle rearrangements of randomly distributed cracks into uniform radial patterns with fewer cracks. In that respect, this study exemplifies how relaxation mechanisms in fragmenting solids can attenuate the influence of defects in the material.
%G English
%2 https://hal.archives-ouvertes.fr/hal-00566658/document
%2 https://hal.archives-ouvertes.fr/hal-00566658/file/Radial-cracks.pdf
%L hal-00566658
%U https://hal.archives-ouvertes.fr/hal-00566658
%~ CNRS
%~ INSMI
%~ EC-MARSEILLE
%~ UNIV-AMU
%~ IRPHE
%~ TDS-MACS
%~ ANR

