%0 Conference Proceedings
%T Effet de l’adsorption de gaz sur la perméabilité d’un charbon naturel dans le contexte du stockage souterrain de dioxyde de carbone
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Perrier, Laurent
%A Grégoire, David
%< avec comité de lecture
%B 7ème journées de l'Association Française de l'Adsorption
%C Marseille, France
%8 2018-01-25
%D 2018
%Z Physics [physics]/Mechanics [physics]/Materials and structures in mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Materials and structures in mechanics [physics.class-ph]Conference papers
%G French
%L hal-02289202
%U https://hal.archives-ouvertes.fr/hal-02289202
%~ UNIV-PAU
%~ CNRS
%~ LFCR
%~ TESTUPPA

%0 Conference Paper
%F Oral 
%T Evolution of the permeability of a natural coal under the effect of gas adsorption in the context of underground storage
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Grégoire, David
%A Perrier, Laurent
%< avec comité de lecture
%B 14èmes Journées d'études des Milieux Poreux 2018
%C Nantes, France
%8 2018-10-08
%D 2018
%Z Physics [physics]/Mechanics [physics]Conference papers
%G English
%L hal-02289213
%U https://hal.archives-ouvertes.fr/hal-02289213
%~ UNIV-PAU
%~ CNRS
%~ LFCR
%~ TESTUPPA

%0 Conference Proceedings
%T Déformations induites par adsorption dans les milieux poreux à double porosité : modélisation et validation expérimentale
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Perrier, Laurent
%A Grégoire, David
%A Pijaudier-Cabot, Gilles
%A Plantier, Frédéric
%< avec comité de lecture
%B 5ème journées de l'Association Française de l'Adsorption
%C Paris, France
%8 2016-02-04
%D 2016
%Z Physics [physics]/Mechanics [physics]/Materials and structures in mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Materials and structures in mechanics [physics.class-ph]Conference papers
%G French
%L hal-01348999
%U https://hal.archives-ouvertes.fr/hal-01348999
%~ LFCR-GEOMECA
%~ CNRS
%~ UNIV-PAU
%~ LFCR
%~ TESTUPPA

%0 Conference Proceedings
%T Couplages adsorption-déformation-transport en milieux nanoporeux - Application au stockage de CO2 dans des veines de charbon
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Grégoire, David
%A Perrier, Laurent
%< avec comité de lecture
%B 24ème Congrès Français de Mécanique (CFM2019)
%C Brest, France
%8 2019-08-26
%D 2019
%Z Physics [physics]/Mechanics [physics]/Materials and structures in mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Materials and structures in mechanics [physics.class-ph]Conference papers
%G French
%L hal-02289291
%U https://hal.archives-ouvertes.fr/hal-02289291
%~ UNIV-PAU
%~ CNRS
%~ LFCR
%~ TESTUPPA

%0 Conference Proceedings
%T Swelling Due To Adsorption In Porous Media Presenting Different And Distinct Porosities: Model And Experimental Validation
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Perrier, Laurent
%A Plantier, Frédéric
%A Pijaudier-Cabot, Gilles
%A Grégoire, David
%< avec comité de lecture
%B 6th Biot Conference on Poromechanics (Biot 6)
%C Paris, France
%8 2017-07-09
%D 2017
%Z Physics [physics]/Mechanics [physics]/Materials and structures in mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Materials and structures in mechanics [physics.class-ph]Conference papers
%G English
%L hal-01541089
%U https://hal.archives-ouvertes.fr/hal-01541089
%~ CNRS
%~ UNIV-PAU
%~ LFCR-GEOMECA
%~ LFCR
%~ TESTUPPA

%0 Conference Proceedings
%T Poromechanics of swelling in nanoporous materials: motivations and introduction of strain effects on adsorption
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Pijaudier-Cabot, Gilles
%A Grégoire, David
%A Vermorel, Romain
%A Perrier, Laurent
%< avec comité de lecture
%( Poromechanics V: Proceedings of the Fifth Biot Conference on Poromechanics
%B 5th Biot Conference on Poromechanics (Biot 5)
%C Vienne, Austria
%P 729-738
%8 2013-07-10
%D 2013
%R 10.1061/9780784412992.087
%Z Physics [physics]/Mechanics [physics]/Materials and structures in mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Materials and structures in mechanics [physics.class-ph]Conference papers
%G English
%L hal-00828490
%U https://hal.archives-ouvertes.fr/hal-00828490
%~ CNRS
%~ LFCR-GEOMECA
%~ UNIV-PAU
%~ LFCR
%~ LFCR-PTP
%~ TESTUPPA

%0 Conference Paper
%F Oral 
%T Enhanced poromechanics for the modeling of swelling in microporous materials: Coupled effects, size effects and upscaling issues
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Pijaudier-Cabot, Gilles
%A Perrier, Laurent
%A Grégoire, David
%< avec comité de lecture
%B 2014 Workshop of the International Research Network GdRi - M2UN
%C San Sebastian, Spain
%8 2014-09-28
%D 2014
%Z Physics [physics]/Mechanics [physics]/Materials and structures in mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Materials and structures in mechanics [physics.class-ph]Conference papers
%G English
%L hal-01138814
%U https://hal.archives-ouvertes.fr/hal-01138814
%~ CNRS
%~ UNIV-PAU
%~ LFCR-GEOMECA
%~ LFCR
%~ TESTUPPA

%0 Conference Proceedings
%T Enhanced poromechanics for the modeling of swelling in microporous materials: Coupled effects, size effects and upscaling issues
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Pijaudier-Cabot, Gilles
%A Perrier, Laurent
%A Grégoire, David
%< avec comité de lecture
%( Proceeding book
%B 11th World Congress on Computational Mechanics (WCCM XI)
%C Barcelona, Spain
%P --
%8 2014-07-20
%D 2014
%Z Physics [physics]/Mechanics [physics]/Materials and structures in mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Materials and structures in mechanics [physics.class-ph]Conference papers
%G English
%L hal-01005961
%U https://hal.archives-ouvertes.fr/hal-01005961
%~ CNRS
%~ LFCR-GEOMECA
%~ UNIV-PAU
%~ LFCR
%~ TESTUPPA

%0 Conference Paper
%F Oral 
%T Fracture and fluid-solid coupled effects in porous quasi-brittle materials
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Pijaudier-Cabot, Gilles
%A Grégoire, David
%A Perrier, Laurent
%A Khaddour, Fadi
%A Lefort, Vincent
%A Nouailletas, Olivier
%< avec comité de lecture
%B The 5th International Workshop on CRYStallisation in POrous Media
%C Toulouse, France
%8 2016-06-06
%D 2016
%Z Physics [physics]/Mechanics [physics]/Materials and structures in mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Materials and structures in mechanics [physics.class-ph]Conference papers
%G English
%L hal-01541084
%U https://hal.archives-ouvertes.fr/hal-01541084
%~ CNRS
%~ UNIV-PAU
%~ LFCR-GEOMECA
%~ LFCR
%~ TESTUPPA

%0 Conference Proceedings
%T Couplage adsorption/gonflement en milieu microporeux: nouvelle technique expérimentale et modèle poromécanique associé
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Perrier, Laurent
%A Grégoire, David
%A Plantier, Frédéric
%A Pijaudier-Cabot, Gilles
%< avec comité de lecture
%( Recueil de résumés des présentations orales
%B 3ème journées de l'Association Française de l'Adsorption
%C Paris, France
%P 11
%8 2014-02-11
%D 2014
%Z Physics [physics]/Mechanics [physics]/Materials and structures in mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Materials and structures in mechanics [physics.class-ph]Conference papers
%X Ce travail vise à obtenir une meilleure compréhension du couplage entre adsorption et déformation dans les milieux microporeux. On considère ici des solides poreux entièrement saturés dont la taille de pores est inférieure à 2nm. Le ciment, les roches tight, les charbons actifs ou les charbons naturels répondent à cette définition. Expérimentalement, différents auteurs ont essayé de combiner des résultats d'adsorption de gaz avec des données de déformations volumiques, particulièrement pour les charbons naturels bitumineux. Cependant, les mesures d'adsorption et de déformation sont rarement réalisées simultanément, ni même sur le même échantillon [1]. Les quelques résultats de mesures simultanées présentes des déformations volumiques extrapolées à partir de mesures locales ou en suivant l'expansion bidimensionnelle de la silhouette [2]. Le but de ce papier est de présenter un nouveau montage expérimental où les quantités adsorbées et le champs surfacique complet de déformations mécaniques sont mesurés in-situ et simultanément, par manométrie et corrélation d'images respectivement. Des expériences sur charbons actifs à 30°C, saturés en CH4 et CO2, respectivement jusqu'à une pression bulk de 120 et 50 bars, sont présentées. Des déformations irréversibles sont observées. D'autre part, un modèle poromécanique, récemment proposé [3], est étendu afin de tenir compte de l'évolution de porosité avec la déformation. Un nouveau schéma incrémental non-linéaire est proposé où les grandeurs poromécaniques évoluent à chaque incrément de pression en fonction des variations de porosité. Les interactions entre déformations et isothermes d'adsorption sont examinées. Une correction du formalisme classique de Gibbs est proposée. Les déformations prédites sont comparées aux résultats de la littérature et à nos propres données expérimentales.
%G French
%L hal-00991725
%U https://hal.archives-ouvertes.fr/hal-00991725
%~ CNRS
%~ LFCR-GEOMECA
%~ UNIV-PAU
%~ LFCR
%~ TESTUPPA

%0 Conference Paper
%F Oral 
%T Fracture, deformation, and fluid-solid coupled effects in porous quasi-brittle materials
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Pijaudier-Cabot, Gilles
%A Grégoire, David
%A Miqueu, Christelle
%A Plantier, Frédéric
%A Lefort, Vincent
%A Ecay, Lionel
%A Malheiro, Carine
%A Khaddour, Fadi
%A Perrier, Laurent
%A Rojas Solano, Laura
%A Nouailletas, Olivier
%F Invité
%< avec comité de lecture
%B 2016 EMI International Conference of ASCE
%C Metz, France
%8 2016-10-25
%D 2016
%Z Physics [physics]/Mechanics [physics]/Materials and structures in mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Materials and structures in mechanics [physics.class-ph]Conference papers
%G English
%L hal-01541085
%U https://hal.archives-ouvertes.fr/hal-01541085
%~ CNRS
%~ UNIV-PAU
%~ LFCR-GEOMECA
%~ LFCR
%~ TESTUPPA

%0 Conference Proceedings
%T Adsorption-induced strain in meso and microporous media: modeling and experimental validation
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Grégoire, David
%A Perrier, Laurent
%A Plantier, Frédéric
%A Pijaudier-Cabot, Gilles
%F Invité
%< avec comité de lecture
%B 5th International Symposium on Energy Challenges & Mechanics (ISECM)
%C Inverness, United Kingdom
%8 2016-07-10
%D 2016
%Z Physics [physics]/Mechanics [physics]/Materials and structures in mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Materials and structures in mechanics [physics.class-ph]Conference papers
%G English
%L hal-01349022
%U https://hal.archives-ouvertes.fr/hal-01349022
%~ CNRS
%~ UNIV-PAU
%~ LFCR-GEOMECA
%~ LFCR
%~ TESTUPPA

%0 Conference Proceedings
%T Experimental and modeling investigations of adsorption-induced swelling and damage in microporous materials
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Perrier, Laurent
%A Grégoire, David
%A Plantier, Frédéric
%A Pijaudier-Cabot, Gilles
%< avec comité de lecture
%( Procedia Materials Science
%B The 20th European Conference on Fracture
%C Trondheim, Norway
%P --
%8 2014-06-30
%D 2014
%Z Physics [physics]/Mechanics [physics]/Materials and structures in mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Materials and structures in mechanics [physics.class-ph]Conference papers
%G English
%L hal-01005946
%U https://hal.archives-ouvertes.fr/hal-01005946
%~ CNRS
%~ LFCR-GEOMECA
%~ UNIV-PAU
%~ LFCR
%~ TESTUPPA

%0 Conference Proceedings
%T Coupling between adsorption and swelling in microporous materials: in-situ full-field measurements and poromechanical modeling
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Perrier, Laurent
%A Grégoire, David
%A Plantier, Frédéric
%A Pijaudier-Cabot, Gilles
%< avec comité de lecture
%( Proceeding book
%B The 6th International Conference on Porous Media and Annual Meeting of the International Society of Porous Media
%C Milwaukee, United States
%P cdrom
%8 2014-05-27
%D 2014
%Z Physics [physics]/Mechanics [physics]/Materials and structures in mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Materials and structures in mechanics [physics.class-ph]Conference papers
%G English
%L hal-01005904
%U https://hal.archives-ouvertes.fr/hal-01005904
%~ CNRS
%~ LFCR-GEOMECA
%~ UNIV-PAU
%~ LFCR
%~ TESTUPPA

%0 Conference Paper
%F Oral 
%T Enhanced poromechanics for the modeling of swelling in microporous materials: Coupled effects, size effects and upscaling issues
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Perrier, Laurent
%A Pijaudier-Cabot, Gilles
%A Grégoire, David
%< avec comité de lecture
%B GdR Mege Atelier thématique "Poromécanique et géomatériaux : changement d'échelle, modélisations et simulations numériques"
%C Anglet, France
%8 2014-01-29
%D 2014
%Z Physics [physics]/Mechanics [physics]/Materials and structures in mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Materials and structures in mechanics [physics.class-ph]Conference papers
%G French
%L hal-01005887
%U https://hal.archives-ouvertes.fr/hal-01005887
%~ CNRS
%~ LFCR-GEOMECA
%~ UNIV-PAU
%~ LFCR
%~ TESTUPPA

%0 Conference Paper
%F Oral 
%T Nouvelle technique expérimentale pour la mesure simultanée de l’adsorption et du gonflement induit en milieu microporeux et modélisation poromécanique associée
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Perrier, Laurent
%A Plantier, Frédéric
%A Pijaudier-Cabot, Gilles
%A Grégoire, David
%< avec comité de lecture
%B 24e Réunion des Sciences de la Terre
%C Pau, France
%8 2014-10-27
%D 2014
%Z Physics [physics]/Mechanics [physics]/Materials and structures in mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Materials and structures in mechanics [physics.class-ph]Conference papers
%G French
%L hal-01138824
%U https://hal.archives-ouvertes.fr/hal-01138824
%~ CNRS
%~ UNIV-PAU
%~ LFCR-GEOMECA
%~ LFCR
%~ TESTUPPA

%0 Conference Paper
%F Oral 
%T Adsorption‐induced instantaneous deformation in double porosity media: modeling and experimental validations
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Grégoire, David
%A Perrier, Laurent
%A Plantier, Frédéric
%A Pijaudier-Cabot, Gilles
%< avec comité de lecture
%B Bilateral French‐Italy Workshop, GDR MeGe & MEMOCS
%C Arpino, Italy
%8 2015-05-04
%D 2015
%Z Physics [physics]/Mechanics [physics]/Materials and structures in mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Materials and structures in mechanics [physics.class-ph]Conference papers
%G English
%L hal-01214712
%U https://hal.archives-ouvertes.fr/hal-01214712
%~ CNRS
%~ UNIV-PAU
%~ LFCR-GEOMECA
%~ LFCR
%~ TESTUPPA

%0 Conference Proceedings
%T Swelling of coals due to CO2/ CH4 adsorption: experimental and modeling investigations
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Perrier, Laurent
%A Grégoire, David
%A Plantier, Frédéric
%A Pijaudier-Cabot, Gilles
%< avec comité de lecture
%( Proceeding book
%B 10th Euroconference on Rock Physics and Rock Mechanics
%C Aussois, France
%P --
%8 2014-05-12
%D 2014
%Z Physics [physics]/Mechanics [physics]/Materials and structures in mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Materials and structures in mechanics [physics.class-ph]Conference papers
%G English
%L hal-01005894
%U https://hal.archives-ouvertes.fr/hal-01005894
%~ CNRS
%~ LFCR-GEOMECA
%~ UNIV-PAU
%~ LFCR
%~ TESTUPPA

