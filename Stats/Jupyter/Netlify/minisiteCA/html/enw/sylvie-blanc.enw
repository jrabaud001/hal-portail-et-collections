%0 Conference Paper
%F Oral 
%T End-capping of low band gap conjugated polymer for metal-oxide nanoparticles surface modification
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Gapin, Adèle
%A Blanc, Sylvie
%A Bousquet, Antoine
%A Lartigau-Dagron, Christine
%Z BEST ORAL AWARDS
%< avec comité de lecture
%B 17th International Conference on Chemistry and the Environment
%C Thessaloniki, Greece
%8 2019-06-16
%D 2019
%Z Chemical Sciences
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Theoretical and/or physical chemistryConference papers
%G English
%L hal-02183308
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02183308
%~ UNIV-PAU
%~ IPREM
%~ IPREM-PCM
%~ CNRS
%~ INC-CNRS

%0 Conference Paper
%F Oral 
%T Formation of reactive oxygen and nitrogen species including peroxynitrite in physiological buffer exposed to cold atmospheric plasmas
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Institut des Sciences Moléculaires (ISM)
%A Girard, Fanny
%A Badets, Vasilica
%A Blanc, Sylvie
%A Sojic, Neso
%A Clement, Franck
%A Arbault, Stéphane
%< avec comité de lecture
%B International Conference on Plasmas with Liquids (ICPL 2017)
%C Prague, France
%8 2017-03-05
%D 2017
%Z Chemical Sciences/Analytical chemistry
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Material chemistry
%Z Engineering Sciences [physics]
%Z Engineering Sciences [physics]/PlasmasConference papers
%G English
%L hal-02271089
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02271089
%~ UNIV-PAU
%~ IPREM
%~ IPREM-PCM
%~ UNIV-BORDEAUX
%~ CNRS
%~ INC-CNRS
%~ ANR

%0 Conference Paper
%F Oral 
%T Cold Atmospheric Plasmas in interactions with materials and liquids for biological applications
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Université de Pau et des Pays de l'Adour (UPPA)
%A Clement, Franck
%A Held, Bernard
%A Gazeli, Kristacq
%A Marlin, Laurent
%A Morel, Erwan
%A Girard-Sahun, Fanny
%A Blanc, Sylvie
%A Grassl, Bruno
%A Reynaud, Stephanie
%A Marcasuzaa, Pierre
%A Veclin, Cloé
%A Lebouachera, Seif El Islam
%A Léonardi, Frédéric
%A Hoqui, Quentin
%A Nardin, Corinne
%A Callies, Xavier
%A Arbault, Stéphane
%A Badets, Vasilica
%A Lefrançois, Pauline
%F Invité
%< avec comité de lecture
%B 7 th International Symposium on Surfaces and Interfaces of Biomaterials
%C Québec, Canada
%8 2019-07-22
%D 2019
%Z Chemical Sciences/Analytical chemistry
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Material chemistry
%Z Engineering Sciences [physics]/PlasmasConference papers
%X Cold Atmospheric Plasmas (CAPs) are now currently used for numerous applications concerning the surface treatment of solid materials at the macro-, micro- and nano- scales. Surface modifications produced lead to new functionnalities induced by the interactions of the materials with the different energy components (electrons, ions, photons, reactive species) of these partially ionised gases, produced at atmospheric pressure and temperature closed to the room one. Another large area of applications is related to the treatment of liquids in which it is possible to increase the chemical reactivity, thus allowing nanoparticles synthetisis or bacterial decontamination. In this presentation we will first discuss on the physical and chemical properties of these specific plasmas. Their principles of manufacturing will be summarized and particular attentions will be indicated on the environment around the plasma-surface interaction zone. A non-exhaustive state of the art will provide a better understanding of the physical mechanisms associated with their propagation up to the material to be treated. A large biological applications field will be then shown, illustrated by the analyses of modifications produced on steels, polymers but also on physiological liquids and living cells. Methods of the interactions characterizations are numerous and we will present some known techniques but with an innovative approach as for instance the IR-ATR spectroscopy for tracking of modifications of polymers surface. The insitu electrochemistry1 has been also developed during the plasma exposure of liquids and results obtained will be given, showing that it is possible to measure very low current in the nA range and in the presence of high electric fields used for the plasma formation. Bio-chemically Reactive Oxygen and Nitrogen Species (RONS) production (H2O2, NO2-) can thus be followed and quantified, as well as the detection of short-living species.
%G English
%L hal-02191333
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02191333
%~ UNIV-PAU
%~ IPREM
%~ CNRS
%~ INC-CNRS
%~ IPREM-PCM
%~ IPREM-CAPT
%~ ANR

%0 Conference Paper
%F Oral 
%T Formation of rons including ONOO- and O2 .- in physiological buffers exposed to cold atmospheric plasmas
%+ Institut des Sciences Moléculaires (ISM)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Girard-Sahun, Fanny
%A Badets, Vasilica
%A Lefrançois, Pauline
%A Blanc, Sylvie
%A Sojic, Neso
%A Arbault, Stephane
%A Clement, Franck
%< avec comité de lecture
%B 6th International Workshop on Plasma for Cancer Treatment
%C Antwerp, Belgium
%8 2019-04-01
%D 2019
%Z Chemical Sciences/Material chemistry
%Z Environmental Sciences
%Z Engineering Sciences [physics]/PlasmasConference papers
%G English
%L hal-02183237
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02183237
%~ UNIV-PAU
%~ IPREM
%~ IPREM-PCM
%~ SDE
%~ UNIV-BORDEAUX
%~ CNRS
%~ GIP-BE
%~ INC-CNRS
%~ ANR

