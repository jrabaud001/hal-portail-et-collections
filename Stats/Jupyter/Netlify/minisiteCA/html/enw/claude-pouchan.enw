%0 Conference Proceedings
%T Comparative quantum mechanical static and dynamic approaches to modelling vibrational spectra
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Pouchan, Claude
%A Carbonnière, Philippe
%Z cited By 0
%< avec comité de lecture
%B American Institute of Physics Conference Series
%C Unknown, Unknown Region
%I American Institute of Physics
%V 1108
%P 82-89
%8 2009
%D 2009
%R 10.1063/1.3117143
%Z Chemical Sciences/Theoretical and/or physical chemistryConference papers
%X We present quantum mechanical (QM) vibrational computations beyond the harmonic approximation from an effective second order perturbative (VPT2), a variational (VCI) and a variation-perturbation (VCI-P) treatments - defined as static approaches- as well as vibrational analysis from DFT molecular dynamics trajectories at 300K and 600K. The four schemes are compared in terms of prediction of fundamental transitions, simulation of the corresponding medium infrared (MIR) spectrum at the same level of theory using the B3LYP/6-31+G(d,p) description of the electronic structure for the formamide that exhibits both torsional and double-well type modes. © 2009 American Institute of Physics.
%G English
%L hal-01598709
%U https://hal.archives-ouvertes.fr/hal-01598709
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA

%0 Conference Proceedings
%T Vibrational analysis from Quantum Mechanic molecular dynamics trajectories
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Carbonnière, Philippe
%A Dargelos, Alain
%A Pouchan, Claude
%Z cited By 0
%< avec comité de lecture
%B Computational Methods in Science and Engineering:
%C Unknown, Unknown Region
%V 963
%N 1
%P 329-336
%8 2007
%D 2007
%R 10.1063/1.2827016
%Z Chemical Sciences/Theoretical and/or physical chemistryConference papers
%X In this paper we report a vibrational analysis from Quantum Mechanic dynamic DFT based approaches. After a brief presentation of the dynamic simulation using the Atom Centered Density Matrix Propagation (ADMP) formalism, we compare in the case of methyl amidogen the vibrational spectra obtained from a Vibrational Configuration Interaction formalism and the molecular Dynamic approach. Except for the CH stretching modes, the results converge giving a good agreement with the experimental data. The second example concerns a larger sized molecular system, the Glycine radical. An interpretation of all the fundamental bands is given in the MIR area between 150 to 4000 cm-1. © 2007 American Institute of Physics.
%G English
%L hal-01598728
%U https://hal.archives-ouvertes.fr/hal-01598728
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA

%0 Conference Proceedings
%T Vibrational treatment from a variation-perturbation scheme: The VCI-P method. Application to the glycolaldehyde
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Carbonnière, P.
%A Pouchan, Claude
%Z cited By 0
%< avec comité de lecture
%B Computational Methods in Sciences and Engineering
%C Unknown, Unknown Region
%V 1504
%P 152-159
%8 2012
%D 2012
%R 10.1063/1.4771710
%Z Chemical Sciences/Theoretical and/or physical chemistryConference papers
%X An iterative variation-perturbation algorithm allowing a anharmonic vibrational treatment of polyatomic molecules is proposed. This state specific process consists on an iterative construction of small 3N-5 Vibrational Configuration Interation (VCI) matrixes (N being the number of atoms) with the most pertinent couplings and includes a perturbative treatment of the weakest contributions. Thus, this scheme allow to massively reduce the size of the CI matrixes with a minimum loss of correlation energy. Through the example of H2CO, the results stemmed from the VCI-P process are compared to their full VCI counterpart. Moreover, the computations of anharmonic intensities are also implemented. As illustration, the modelization of the medium infrared (MIR) spectrum of the glycolaldehyde is reported. © 2012 American Institute of Physics.
%G English
%L hal-01598689
%U https://hal.archives-ouvertes.fr/hal-01598689
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA

%0 Conference Proceedings
%T Is there any connection between the (Hyper) polarizabilities of the ground state structures of clusters and those of their low lying isomers? a case study of aluminum doped silicon clusters
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Karamanis, Panaghiotis
%A Marchal, Rémi
%A Carbonnière, Philippe
%A Pouchan, Claude
%Z cited By 0
%< avec comité de lecture
%B International Conference of Computational Methods in Sciences and Engineering
%C Unknown, Unknown Region
%V 1504
%P 679-682
%8 2012
%D 2012
%R 10.1063/1.4771786
%Z Chemical Sciences/Theoretical and/or physical chemistryConference papers
%X The (hyper)polarizabilities of the global minima and of low lying isomers of ground doped aluminum clusters of the AlSin type (n=3-9) have been studied within the density functional framework. Our results show that the polarizabilities and first hyperpolarizabilities per atom of these doped Al doped clusters rabidly degrease with the cluster size. Also by tracing a significant number of stable low lying isomers we demonstrate that both the average values of the mean polarizabilities per atom and of the total fist hyperpolarizabilities of those species follow closely the evolution that is observed in the case of their ground state structures. © 2012 American Institute of Physics.
%G English
%L hal-01598691
%U https://hal.archives-ouvertes.fr/hal-01598691
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA

%0 Conference Proceedings
%T Comparative theoretical study of poly[3-(4-octylphenyl)thiophenes] and poly[3-(4-octylphenoxy)thiophenes] as promising photovoltaic cells materials
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Dkhissi, A.
%A Pouchan, Claude
%Z cited By 0
%< avec comité de lecture
%B Computation in Modern Science and Engineering
%C Unknown, Unknown Region
%V 963
%N 2
%P 35-39
%8 2007
%D 2007
%R 10.1063/1.2836085
%Z Chemical Sciences/Theoretical and/or physical chemistryConference papers
%X The geometric and electronic structure of [3-(4-Octylphenyl)]Thiophenes (POPT) and [3-(4-octylphenoxy)thiophenes] (POPOT) oligomers, ranging in size up to the octamer (240 atoms), have been calculated with density functional theory (B3LYP/6-31G*). The knowledge of the geometries for these polymers is of utmost importance in order to understand their optical properties in different phase (solution and condensed phase). The calculations indicate that, in opposition to the first polymer where the introduction of the pendant phenyl disturb the planarity for the backbone of the conjugated segment, the second polymer has a nice organisation where the conjugated chain still planar even the presence of octylphenyl groups. The optical spectra are calculated with TDDFT and Zindo approaches on the basis of the geometries provided by DFT, and compared to experimental data. © 2007 American Institute of Physics.
%G English
%L hal-01598726
%U https://hal.archives-ouvertes.fr/hal-01598726
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA

%0 Conference Proceedings
%T Computational aspects of the modelling of vibrational properties of gases, liquids and solids
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Carbonnière, P.
%A Pouchan, Claude
%Z cited By 0
%< avec comité de lecture
%B International Conference of Computational Methods in Sciences and Engineering
%C Unknown, Unknown Region
%V 1504
%P 920
%8 2012
%D 2012
%R 10.1063/1.4771845
%Z Chemical Sciences/Theoretical and/or physical chemistryConference papers
%X no abstract
%G English
%L hal-01598690
%U https://hal.archives-ouvertes.fr/hal-01598690
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA

%0 Conference Proceedings
%T Modeling the IR spectra of acetaldehyde from a new vibrational configuration interaction method
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Bégué, Didier
%A Pouchan, Claude
%Z cited By 0
%< avec comité de lecture
%B Computation in Modern Science and Engineering
%C Unknown, Unknown Region
%V 963
%N 2
%P 14-18
%8 2007
%D 2007
%R 10.1063/1.2836025
%Z Chemical Sciences/Theoretical and/or physical chemistryConference papers
%X In this paper we present a new vibrational configuration interaction method known as a parallel vibrational multiple window configuration interaction P-VMWCI which generates several VCI matrices and enables the variational treatment of medium size molecular systems. Application to acetaldehyde gives a new interpretation of the MIR experimental data. © 2007 American Institute of Physics.
%G English
%L hal-01598727
%U https://hal.archives-ouvertes.fr/hal-01598727
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA

%0 Conference Proceedings
%T Global search algorithm of minima exploration to find low lying isomers of clusters in which spheroidal generation and raking optimization appear as an original process
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Marchal, Rémi
%A Carbonnière, P.
%A Pouchan, Claude
%Z cited By 1
%< avec comité de lecture
%B AIP Conference Proceedings
%C Unknown, Unknown Region
%V 1504
%P 660-663
%8 2012
%D 2012
%R 10.1063/1.4771782
%Z Chemical Sciences/Theoretical and/or physical chemistryConference papers
%X The first and most important step in the study of properties and dynamics of atomic and molecular clusters is the determination of their microscopic structures. In that sense, we have developped a new algorithm[1], called Global Search Algorithm of Minima exploration (GSAM), which include two original schemes: the spheroidal generation, allowing the generation of rings, spheres, m rings cylinders and planar structures, and the raking optimization process allowing to discard step by step the conformations that become physically irreasonable during the optimization process. Using such an algorithm, the microscopic structure of the well-known Silicon clusters have been revisited as a test case. The more interesting case of the binary GanAs m will be presented. © 2012 American Institute of Physics.
%G English
%L hal-01598692
%U https://hal.archives-ouvertes.fr/hal-01598692
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA

