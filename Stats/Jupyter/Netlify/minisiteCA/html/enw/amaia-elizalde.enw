%0 Conference Paper
%F Oral 
%T The Obsessions of Terror: Literary Motifs
%+ Centre de recherche sur la langue et les textes basques (IKER)
%+ Universidad del Pais Vasco / Euskal Herriko Unibertsitatea [Espagne] (UPV/EHU)
%A Elizalde, Amaia
%A Ayerbe, Mikel
%< avec comité de lecture
%B „Heimat Und Gedächtnis Heute“: IV. Fachtagung Zu Literarischen Repräsentationen von Heimat in Der Aktuellen Deutschsprachigen Literatur
%C Gasteiz, Spain
%8 2019-09-25
%D 2019
%Z Humanities and Social Sciences/LinguisticsConference papers
%X The aim of this communication is to explore the most frequented common places by Basque literature that deal with the issue of armed conflicts in the Basque context, conflicts that are a central and constant element in many representations of aberria or Heimat. Many obsessions with terror in the context of Basque literature are closely related to the search for and explanation of both the root of the conflict and the reason for its continuation and derivation, especially linked to ETA’s terrorism for national liberation and the Basque Heimat. There are certain literary motives that have worked as recurrent themes when writing the literary "story" of the cultural memory that touches the political-armed conflict, including the following three points: the search for the origins that trigger terror; the woman-mother as victim and/or perpetrator of the terrorist legacy; and, finally, the collectivization of guilt and socialization of responsibilities, which transform the chain into a network of links. The narrative that is articulated through these nuclei is not homogeneous, nor do its more constant approaches lack interferences, and it is this dialectic that is intended to be examined through multiple genre literary texts, written at different periods and by different Basque canonical authors, such as A. Urretabizkia’s The red Notebook, B. Atxaga’s The Accordionist's son, J. Muñoz’s Antzararen bidea, R. Saizarbitoria’s Martutene, K. Agirre’s Atertu ate itxaron, among others
%G English
%L hal-02551576
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02551576
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS

%0 Conference Paper
%F Oral 
%T Canciones y poemas de los 60-70 : revitalización y reivindicación
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Elizalde, Amaia
%< avec comité de lecture
%B Convivencia y conflicto lingüístico en el Estado Español
%C Konstanz, Germany
%8 2015-11-10
%D 2015
%Z Humanities and Social Sciences/LinguisticsConference papers
%G Spanish
%L hal-02551616
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02551616
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS

%0 Conference Paper
%F Oral 
%T Remembering Prometheus and Antigone: Rebellion and Mercy. Basque Rewritings of Greek Myths during Times of Contemporary Political Conflict
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Elizalde, Amaia
%< avec comité de lecture
%B 9th Annual International Conference on Literature
%C Athens, Greece
%8 2020-06-06
%D 2020
%Z Humanities and Social Sciences/LinguisticsConference papers
%X This paper aims to explain Basque artistic interpretations of the myths of Prometheus and Antigone and their close relationship to the political and ideological reality of the 60s and 70s in the Basque Country. Both Greek myths have been an endless source of artistic creation down to the present, mainly interpreting the heroes as representations of rebellion against the authority. Thus, it is not surprising that during Franco’s dictatorship some Basque nonconformist artists identified with these symbols of rebellion and rewrote them. However, as we will try to show by comparing Xabier Lete’s poetry and Jorge Oteiza’s sculpture, these Basque artistic interpretations of what originaly was rebellion underwent profound modifications that were closely connected to gender and religious issues.
%G English
%L hal-02551577
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02551577
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS

%0 Conference Paper
%F Oral 
%T “Till They Leave Us Alone”, Not yet: Arts as a Weapon of Resistance for Basque Subaltern Memories
%+ Centre de recherche sur la langue et les textes basques (IKER)
%+ Universidad del Pais Vasco / Euskal Herriko Unibertsitatea [Espagne] (UPV/EHU)
%A Elizalde, Amaia
%A Manterola, Ismael
%< avec comité de lecture
%B Postcolonial Spain? Contexts, Politics, Cultural Practices
%C Bangor, Wales, Unknown Region
%8 2017-12-01
%D 2017
%Z Humanities and Social Sciences/LinguisticsConference papers
%G English
%L hal-02551578
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02551578
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS

%0 Conference Proceedings
%T Memories of Forgetting in Conflictive Social Contexts. The Competition of Memorial Cultures and Its Reflection in the Arts: A Case Study from the Basque Country
%+ Centre de recherche sur la langue et les textes basques (IKER)
%+ Universidad del Pais Vasco / Euskal Herriko Unibertsitatea [Espagne] (UPV/EHU)
%A Elizalde, Amaia
%A Manterola, Ismael
%< avec comité de lecture
%( IJSSIS International Journal of Social Sciences and Interdisciplinary Studies, vol. 3, nº 1
%B Memory Studies '17/ Conference on Memory and Past
%C Istambul, Turkey
%I DAKAM (Eastern Mediterranean Academic Research Center)
%V 3
%P 37--48
%8 2017-09-22
%D 2017
%Z Humanities and Social Sciences/LinguisticsConference papers
%X After Franco’s death in 1975 there was a transitional process to a democratic system in Spain. However, the struggle between the Spanish state and, especially, left-wing Basque nationalists, based on identity and ideological grounds, persisted in the Basque Country. This conflict includes what H. Grabes (2008) calls the “competition of memorial cultures”. In order to preserve its memory canon, from the 1970s on, a part of the Basque society has spontaneously constructed its subaltern memory (J. Colmeiro, 2011) through art, thus creating a subaltern cultural memory. Numerous artistic initiatives took place outside the public institutional sphere based on their own conceptions of memory sites (P. Nora, 1989), most of them directly related to the conflict. This cultural memory reflects the political problematic in many different ways, not only regarding the content of the representations themselves or to their subaltern nature, but also because of the policies developed out of them by public institutions. A. Assmann (2008) theorizes about cultural institutions of active and passive memory and associates institutions of forgetting with totalitarian states. Nonetheless, some public policies implemented in Spain with regard to the subaltern memorial culture of a part of Basque society show that in some cases it is not only being ignored, but also negated. Beyond the exclusion that the formation of the canon and archive carry with them, many different examples of interventions carried out regarding artworks forming part of that memorial culture allow us to further develop the concept of “cultural institution” connected to active and passive forgetting, which converts memory sites into spaces of struggle (Ch. Dupláa, 2000). This need to expand the conceptual framework in order to describe the situation suggests that the Spanish transition to democracy might not have been fully completed.
%G English
%L hal-02549916
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02549916
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS

%0 Conference Paper
%F Oral 
%T Zentsura Jon Miranderen kasuan. Marko teorikoa zabaltzeko beharraz.
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Elizalde, Amaia
%< avec comité de lecture
%B Zentsura eta literatura. Memoria eztabaidatuak. Nazioarteko biltzarra.
%C Donostia, Spain
%8 2018-06-21
%D 2018
%Z Humanities and Social Sciences/LinguisticsConference papers
%G Basque
%L hal-02551580
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02551580
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS

%0 Conference Paper
%F Oral 
%T El eterno retorno, resistencia y revolución en La mujer habitada (1988) de Gioconda Belli
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Elizalde, Amaia
%< avec comité de lecture
%B Seminario Internacional «Literatura y compromiso. Experiencias de Oriente-Occidente».
%C Teheran, Iran
%8 2016-02-23
%D 2016
%Z Humanities and Social Sciences/LinguisticsConference papers
%G Spanish
%L hal-02551574
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02551574
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS

%0 Conference Paper
%F Oral 
%T La evolución del estudio de la censura franquista en la literatura vasca: nuevos retos y perspectivas
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Elizalde, Amaia
%A Gandara, Ana
%< avec comité de lecture
%B Congreso Internacional Franquismo y Literatura: Ortodoxias y Heterodoxias
%C Alcala de Henares, Spain
%8 2019-11-20
%D 2019
%Z Humanities and Social Sciences/LinguisticsConference papers
%G Spanish
%L hal-02551618
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02551618
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS

%0 Conference Paper
%F Oral 
%T Reapropiaciones didácticas del Lazarillo en euskera
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Elizalde, Amaia
%A Ibarluzea, Miren
%< avec comité de lecture
%B Simposio Internacional Los continuadores del Lazarillo
%C A Coruña, Spain
%8 2019-10-10
%D 2019
%Z Humanities and Social Sciences/LinguisticsConference papers
%G Spanish
%L hal-02551620
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02551620
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS

%0 Conference Paper
%F Oral 
%T Zentsura ikerketak gaur egun. Aplikazioa euskal literaturan
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Elizalde, Amaia
%< avec comité de lecture
%B FLV Nazioarteko Biltzarra, 50 urte: metodo eta joera berriak (euskal) hizkuntzalaritzan.
%C Iruñea, Spain
%8 2019-03-06
%D 2019
%Z Humanities and Social Sciences/LinguisticsConference papers
%X Hamarkada pare bat igaro da Joan Mari Torrealdaik euskal literatura eta zentsuraren inguruko lehen lanak argitaratu zituenetik. Lan horiek francopeko erregimenaren aparatu zentsore instituzionalaren jardunari erreparatu zioten, Manuel Luis Abellán eta Maria Josepa Gallofré i Virgilik gaztelaniaz eta katalanez idatzitako literaturaren gainean egindako azterketa garaikideen ikuspegiaren ildotik. Orduz geroztik, zentsuraren gaineko teoriak nazioartean garapen nabarmena izan duen arren, euskal eremuan oso urriak izan dira zentsura ikasketen arloko ekarpen teoriko orokorrak, bai eta euskal literaturaren esparru zehatzera egindakoak ere. Frakismoarekin hasi zen euskal literatur eremuko zentsura? Francopeko garaian zein zen erbesteko eta Ipar Euskal Herriko egoera zentsorea? Zein izan zen itzulpengintzaren rola? Zein eragin izan zuen zentsurak euskaldunon ondare sinbolikoan? Eta klasikoen irakurketan? Galdera horiek guztiak erantzuteko, eta zentsura ikasketen berritze teorikorako beharra atzematearekin batera, iker eremu honek euskal literaturarekiko harremanean irekitzen dituen lerroak arakatzeari ekin zaio berriki.
%G Basque
%L hal-02551579
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02551579
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS

%0 Conference Paper
%F Oral 
%T Censura, Memoria y Arte Púbico: un acercamiento al contexto vasco contemporáneo
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Elizalde, Amaia
%< avec comité de lecture
%B Congrés Internacional Postguerres / Aftermaths of War
%C Barcelona, Spain
%8 2019-06-05
%D 2019
%Z Humanities and Social Sciences/LinguisticsConference papers
%G Spanish
%L hal-02551617
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02551617
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS

%0 Conference Paper
%F Oral 
%T Las obsesiones de terror: los motivos literarios
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Elizalde, Amaia
%A Ayerbe, Mikel
%< avec comité de lecture
%B Memory Studies Association: Third Annual Conference
%C Madrid, Spain
%8 2019-06-25
%D 2019
%Z Humanities and Social Sciences/LinguisticsConference papers
%G Spanish
%L hal-02551619
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02551619
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS

%0 Conference Paper
%F Oral 
%T Gerra Zibila Euskal Herrian memoria leku(ar)en arte publikoaz = The Civil War in Euskal Herria through the Public Art or the Places of Memory
%+ Centre de recherche sur la langue et les textes basques (IKER)
%+ Universidad del Pais Vasco / Euskal Herriko Unibertsitatea [Espagne] (UPV/EHU)
%A Elizalde, Amaia
%A Manterola, Ismael
%Z ISSN 2445-0782 Sancho el Sabio nº Extra 1 Sancho el Sabio Fundazioa Euskal Herriko Unibertsitatea
%< avec comité de lecture
%B Gerra Zibila Euskal Herrian. Historia eta memoria. Monografikoa
%C Gasteiz, Spain
%P 263--282
%8 2017-05-25
%D 2017
%Z Humanities and Social Sciences/LinguisticsConference papers
%X Artikulu honen helburua da memoria eta arte publikoaren inguruan sortu diren eztabaida nahiz teoria kritiko nazioartekoen argitan «Gerra Zibila Euskal Herrian» memoria lekuaren inguruan guren garatutako arte publikoaren politikei begirada kritiko bat zuzentzea eta, hortik abiatuta, hauen balorazio orokor bat egitea. Azterketa memoria kulturalaren (A. Erll, 2008) esparrutik abiatuko da, aztergai den memoria lekura eta monumentu eskultorikoetara mugatuz, gehienbat; baina, azken helburua memoriari lotutako arte publikoaren politiken prozeduren demokratizaziorako oinarrizko gogoeta-proposamen orokorragoa zabaltzea da. The objective of this article is to direct a critical look, and, from there, to make a general assessment about the public art policies developed in our country regarding the place of memory “The Civil War in Euskal Herria” (P. Nora, 1997), in light of the debates and critical theories that have arisen at international level regarding memory and public art. The analysis will start from the field of cultural memory (A. Erll, 2008), focusing particularly on sculptural monuments and the place of memory “the Civil War in Euskal Herria”, but with the ultimate aim of being the opening up of a proposal for a more general basic reflection on the democratisation of public art policy procedures related to memory. El objetivo de este artículo es dirigir una mirada crítica y realizar una valoración general en relación a las políticas de arte público desarrolladas en nuestro país sobre el lugar de memoria «la Guerra Civil en Euskal Herria», a la luz de los debates y las teorías críticas que ha surgido a nivel internacional al respecto de la memoria y el arte público. El análisis partirá del ámbito de la memoria cultural (A. Erll, 2008), centrándose particularmente en los monumentos escultóricos y en el lugar de la memoria en cuestión, pero teniendo como último fin la apertura de una propuesta de reflexión básica más general para la democratización de los procedimientos de las políticas de arte público relacionadas con la memoria.
%G Basque
%L hal-02551575
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02551575
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS

