%0 Conference Paper
%F Oral 
%T 15-years record of molecular and isotopic signature of Hg in salmon tissues from southwestern France
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Geology and Oceanography Department
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%A Julien, Barre, P.G.
%A Bolliet, Valérie
%A Bérail, Sylvain
%A Bareille, Gilles
%A Tessier, Emmanuel
%A Amouroux, David
%A Donard, O.F.X.
%A Monperrus, Mathilde
%< avec comité de lecture
%B 12. International Conference on Mercury as a Global Pollutant
%C Jeju, South Korea
%8 2016-06-14
%D 2016
%Z Life Sciences [q-bio]/Animal biologyConference papers
%G English
%L hal-02454545
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02454545
%~ UNIV-PAU
%~ ECOBIOP
%~ IPREM
%~ INRA
%~ INC-CNRS
%~ CNRS
%~ IPREM-CME
%~ INRAE
%~ AGREENIUM

%0 Conference Proceedings
%T Compact, high performance femtosecond laser ablation system for trace element analysis
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Mottay, E.
%A Chabassier, P.
%A Pecheyran, C.
%A Claverie, Fanny
%A Donard, O.F.X.
%< avec comité de lecture
%B Lasers and Electro-Optics/Quantum Electronics and Laser Science Conference and Photonic Applications Systems Technologies
%C Baltimore, United States
%I Optical Society of America
%8 2007-05-06
%D 2007
%Z Chemical Sciences/Analytical chemistryConference papers
%X We present a compact, industrial laser ablation system for trace element analysis. The system uses a high repetition rate femtosecond laser for material ablation and an inductively coupled mass spectrometer for analysis. © 2007 Optical Society of America.
%G English
%L hal-01590325
%U https://hal.archives-ouvertes.fr/hal-01590325
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS

%0 Conference Proceedings
%T DIRECT EVIDENCE OF COATINGS ON PARTICULATE MATTER AND IMPLICATIONS FOR HEAVY METAL DISTRIBUTIONS IN AN ESTUARINE ENVIRONMENT.
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%A Donard, O.F.X.
%A Bourg, Alain
%A Etcheber, H.
%A Le Ribault, L.
%Z cited By 2
%< avec comité de lecture
%B International Conference - Heavy Metals in the Environment.
%C Unknown, Unknown Region
%I CEP Consultants Ltd, Edinburgh, Sc
%P 1009-1012
%8 1983
%D 1983
%Z Chemical Sciences/Analytical chemistryConference papers
%X Particulate matter is a significant support for heavy metal fluxes in aquatic systems. Selective chemical extraction methods are commonly used to investigate the associations (speciation) of heavy metals with various solid phases. The use of a scanning electron microscope together with a microprobe can provide a new insight in the significance of these chemical associations.
%G English
%L hal-01590180
%U https://hal.archives-ouvertes.fr/hal-01590180
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA

%0 Conference Proceedings
%T HYDRODYNAMICS AS A MAIN CONTROL OF PHYSICO-CHEMICAL PARAMETERS IN DIFFERENT SEA/RIVER WATER INTERFACES. IMPACT ON TRACE METAL BEHAVIOR.
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Jouanneau, J.M.
%A Donard, O.F.X.
%A Latouche, C.
%Z cited By 0
%< avec comité de lecture
%B International Conference - Heavy Metals in the Environment.
%C Unknown, Unknown Region
%I CEP Consultants Ltd, Edinburgh, Sc
%P 1028-1031
%8 1983
%D 1983
%Z Chemical Sciences/Analytical chemistryConference papers
%X The term 'estuary' covers a variety of interface zones in which the residence time (R. T. ) of waters and suspended matter loads fluctuate highly; different categories of physico-chemical gradients considered include a continuous and brief gradient in systems of short R. T. ; a discontinuous and slow gradient showing numerous lower periodic cycles leading to repetitive 'physico-chemical shocks' on particles; and a faster unidirectional and complete gradient in microtidal sedimentary unfilled systems such as those of the NE coast of the U. S. A. Suspended matter is almost totally trapped in the estuary.
%G English
%L hal-01590181
%U https://hal.archives-ouvertes.fr/hal-01590181
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA

%0 Conference Proceedings
%T Investigation on mercury contamination pathways using model aquatic ecosystems
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%+ Institut National de l'Environnement Industriel et des Risques (INERIS)
%A Tessier, Emmanuel
%A Rodriguez Martin-Doimeadios, Rosa C.
%A Amouroux, David
%A Morin, Anne
%A Thybaud, Eric
%A Vindimian, Eric
%A Donard, Olivier F.X.
%< avec comité de lecture
%B 221. ACS National Meeting - Division of Environmental Chemistry
%C San Diego, United States
%P 521-524
%8 2001-04-01
%D 2001
%Z Environmental SciencesConference papers
%G English
%L ineris-00969774
%U https://hal-ineris.archives-ouvertes.fr/ineris-00969774
%~ INERIS
%~ CNRS
%~ SDE
%~ GIP-BE
%~ IPREM
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA

%0 Conference Paper
%F Oral 
%T Investigations on mercury transformations in seawater using isotopically enriched species
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%A Monperrus, Mathilde
%A Tessier, Emmanuel
%A Amouroux, David
%A Leynaert, Aude
%A De Wit, Rutger
%A Donard, O.F.X.
%< avec comité de lecture
%3 RMZ - Materials and Geoenvironment
%B 7 th International Conference on Mercury as a Global Pollutant
%C Ljubjana, Slovenia
%V 51
%N 2
%P 1221-1225
%8 2004-06-28
%D 2004
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Analytical chemistry
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Material chemistryConference papers
%X The aim of this work was to developed field incubations of seawater using isotopically enriched inorg. mercury (199IHg) and monomethylmercury (201MMHg) to understand processes involved in mercury transformations. Expts. were carried out for different sites of the Mediterranean Sea, at coastal and open water stations as a contribution to the MERCYMS project in the framework of the EU ELOISE program. Water incubations were conducted under various controlled conditions (dark and light, filtered and unfiltered water) to assess the major biogeochem. pathways regulating mercury transformations. Biol. characteristics of the incubated samples were also monitored through phytoplankton and bacteria diversity. Isotopically enriched species were used as chem. tracers and have allowed the direct detn. of specific methylation and demethylation yields. This exptl. approach demonstrates that the use of isotopically labeled mercury species offers a unique potential to examine mercury transformation processes in seawater.
%G English
%L hal-01981919
%U https://hal.archives-ouvertes.fr/hal-01981919
%~ IPREM
%~ UNIV-PAU
%~ CNRS
%~ INC-CNRS
%~ TESTUPPA

%0 Conference Proceedings
%T SPECIATION OF METHYL- AND BUTYLTIN COMPOUNDS IN THE GREAT BAY ESTUARY (N. H. ).
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Weber, James H.
%A Donard, Olivier F.X.
%A Randall, Louise
%A Han, Jennie X.
%Z cited By 5
%< avec comité de lecture
%B Oceans 86
%C Unknown, Unknown Region
%I IEEE, New York, NY, USA
%P 1280-1282
%8 1986
%D 1986
%Z Chemical Sciences/Analytical chemistryConference papers
%X The authors describe speciation of methyltin and butyltin compounds by a chromatographic-atomic absorption spectrometric technique at the low picogram level. Its application to estuarine water and sediment samples is presented. Major results are that methyltin and butyltin aquatic concentrations in the Great Bay Estuary of New Hampshire vary with the season, and that their measurable sediment concentrations (as Sn) range from 2 to 80 ng/g.
%G English
%L hal-01590172
%U https://hal.archives-ouvertes.fr/hal-01590172
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA

%0 Conference Proceedings
%T Direct trace and ultra-trace metals determination in crude oil and fractions by inductively coupled plasma mass spectrometry
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Dreyfus, S.
%A Pécheyran, Christophe
%A Magnier, C.
%A Prinzhofer, A.
%A Lienemann, C.P.
%A Donard, O.F.X.
%Z cited By 13
%< avec comité de lecture
%B Journal of ASTM International
%C Unknown, Unknown Region
%I American Society for Testing and Materials
%N 1468
%P 51-58
%8 2005
%D 2005
%Z Chemical Sciences/Analytical chemistryConference papers
%X A method was developed to analyze direct trace and ultra-trace metal elements in crude oil and its fractions (maltenes-asphaltenes) by ICP-MS after sample dilution in xylene. Efficient introduction of organic compounds requires addition of O2 for complete combustion of the sample; carbon deposit on cones (interface) and extraction lenses was minimized by optimization of argon to oxygen ratio in the plasma. A PFA-100 (100 μ1.mn-1) MicroFlow Nebulizer and a Scott "double pass" chilled spray chamber were associated for an optimal introduction of petroleum products. The Q-ICP/MS was equipped with platinum cones to limit drifts caused by cone corrosion during an organic analysis. A standard addition method was realized for the calibration procedure in order to control the matrix effects. Method validation was completed by analyzing three certified reference materials from the National Institute for Standards and Technology (NIST). NIST 1085b, NIST 1084a, and NIST 1634c accuracies were approximately 10 %. Detection limit of ultra trace elements in xylene were at pg.g-1 level. A clean PFA filter system was developed to separate the oil fractions (maltenes and asphaltenes) by precipitation of asphaltenes (heavy fraction) in n-heptane. For optimal detection conditions, the samples were diluted in xylene according to their pre-estimated element concentration range. Total mass balance shows a recovery close to 100 % for Ni, V, Cu, Mo, Ag, Sn, Ba, and Pb. These results show that elements analyzed are highly concentrated in the asphaltenic fraction. Copyright © 2005 by ASTM International.
%G English
%L hal-01590039
%U https://hal.archives-ouvertes.fr/hal-01590039
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA

%0 Conference Proceedings
%T OCCURENCE AND BEHAVIOUR OF MERCURY AND CADMIUM IN THE SEDIMENTS OF AN ESTUARINE SYSTEM: THE GIRONDE.
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%A Donard, O.F.X.
%A Latouche, C.
%A Bourg, Alain
%A Vernet, J.-P.
%Z cited By 3
%< avec comité de lecture
%B International Conference - Heavy Metals in the Environment.
%C Unknown, Unknown Region
%I CEP Consultants Ltd, Edinburgh, Sc
%P 960-963
%8 1983
%D 1983
%Z Chemical Sciences/Analytical chemistryConference papers
%X Sediments of the Gironde estuary were analysed for total mercury and cadmium concentrations. Results show a general downstream decrease for both metals. Correlations with others geochemical parameters of the sediments indicates that mercury present a good affinity with the finest fraction of the sediments, POC content and phosphorus. Cadmium show poorer correlations with the coarser fraction of the sediment, the POC content and zirconium. The degree of contamination determined according to natural background level indicates that the anthropogenic contribution is more important for Cd (95%) than for Hg (75%).
%G English
%L hal-01590179
%U https://hal.archives-ouvertes.fr/hal-01590179
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA

%0 Conference Proceedings
%T Determination of volatile species of metals and metalloids in an urban atmosphere
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Pécheyran, Christophe
%A Donard, O.F.X.
%A Hocquellet, P.
%Z cited By 0
%< avec comité de lecture
%B The Interface between Developing and Developed Countries
%C Unknown, Unknown Region
%V 1
%8 1998
%D 1998
%Z Chemical Sciences/Analytical chemistryConference papers
%X The distribution of different volatile metal species after three campaigns during September 1995, April 1996, and May 1996 detected in the vicinity of Bordeaux, France, was studied. Thirteen sites were selected including indoor and outdoor locations. The sampling sites represented industrial activities and evaluated the magnitude of the automotive emissions. Volatile species identified by comparing the isotopic match and retention time from injection of standards were tetraethyllead compounds, elemental mercury Hg°, dimethylselenide, and tetramethyltin. The total alkyl lead (TAL) concentrations were 0.3-2.9 ng/cu m in industrial or suburban areas, 2.0-43.8 ng/cu m in the urban samples, and 110.4-341.3 ng/cu m in the underground car park. The average TAL values measured during September 1995 yielded higher TAL concentrations compared to those of May 1996 with similar traffic density conditions.
%G English
%L hal-01590110
%U https://hal.archives-ouvertes.fr/hal-01590110
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA

%0 Conference Proceedings
%T CRITICAL ASPECTS OF SELECTIVE EXTRACTIONS OF TRACE METALS FROM ESTUARINE SUSPENDED MATTER. Fe AND Mn HYDROXIDES AND ORGANIC MATTER INTERACTIONS.
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Etcheber, H.
%A Bourg, Alain
%A Donard, O.F.X.
%Z cited By 15
%< avec comité de lecture
%B International Conference - Heavy Metals in the Environment.
%C Unknown, Unknown Region
%I CEP Consultants Ltd, Edinburgh, Sc
%P 1200-1203
%8 1983
%D 1983
%Z Chemical Sciences/Analytical chemistryConference papers
%X Sequential extractions are used for the determination of physico-chemical associations and bioavailability of particulate heavy metals. For the extraction of a given fraction several leaching agents can be employed with varying efficiency. In addition to these problems of a purely chemical nature of 'power' of desorption and dissolution, it is proposed here that the heterogeneity and physical associations (e. g. , coatings) of the various solid fractions may bring additional complications.
%G English
%L hal-01590178
%U https://hal.archives-ouvertes.fr/hal-01590178
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA

%0 Conference Paper
%T 15-years record of molecular and isotopic signature of Hg in salmon tissues from southwestern France
%+ Institut des Sciences Analytiques et Physico-Chimique pour l'Environnement et les Matériaux
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%A Barre, Julien P.G.
%A Bolliet, Valérie
%A Berail, Sylvain
%A Bareille, Gilles
%A Tessier, Emmanuel
%A Amouroux, David
%A Donard, Olivier F.X.
%A Monperrus, Mathilde
%< avec comité de lecture
%B 12. International Conference on Mercury as a Global Pollutant ICMGP 2015
%C Jeiju, South Korea
%8 2015-06-14
%D 2015
%Z Life Sciences [q-bio]/Animal biology
%Z Environmental Sciences/Biodiversity and EcologyConference papers
%G English
%L hal-02795411
%U https://hal.inrae.fr/hal-02795411
%~ INRAE
%~ INRA
%~ SDE
%~ UNIV-PAU
%~ GIP-BE
%~ ECOBIOP

%0 Conference Paper
%F Oral 
%T Mercury transformation by anaerobic bacterial communities selected from estuarine sediments
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%A Goñi, Marisol
%A Dias, M
%A Monperrus, Mathilde
%A Salvado, J.C.
%A Amouroux, David
%A Duran, Robert
%A Donard, O.F.X.
%A Caumette, Pierre
%A Guyoneaud, Remy
%< avec comité de lecture
%3 RMZ - Materials and Geoenvironment
%B 7th International Conference on mercury as a global pollutant
%C Ljubjana, Slovenia
%V 51
%N 2
%P 1006-1009
%8 2004-06-28
%D 2004
%K sulphate reducing bacteria
%K sediments
%K estuary
%K mercury
%K methylation
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Analytical chemistry
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Material chemistryConference papers
%X The fate of mercury species in the environment strongly depends on microbial activities and environmental conditions. Methylation esp. is one of the major processes, which transform the inorg. forms of the metal into toxic and easily bioaccumulated org. forms. Thus, we designed an exptl. setup to study mercury biotransformation in estuarine ecosystems. Sediments were incubated in slurries and polluted with inorg. mercury (HgCl2)​. Mercury methylation was obsd. only in slurries maintained under anoxic conditions. The main microbial populations were isolated and identified by partial sequencing of 16S rDNA. The methylation, studied by gas chromatog.-​inductively coupled plasma mass spectrometry, demonstrated that only sulfate-​reducing bacteria were the methylators among this community, which also comprise denitrifiers and fermentative bacteria.
%G English
%L hal-01981895
%U https://hal.archives-ouvertes.fr/hal-01981895
%~ IPREM
%~ UNIV-PAU
%~ CNRS
%~ INC-CNRS
%~ TESTUPPA

%0 Conference Proceedings
%T Transfer of metallic contaminants at the sediment-water interface in a coastal lagoon: role of the biological and microbial activity
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%A Amouroux, David
%A Monperrus, Mathilde
%A Point, D.
%A Tessier, Emamnuel
%A Bareille, G.
%A Donard, O.
%A Chauvaud, Laurent
%A Thouzeau, Gérard
%A Jean, F.
%A Grall, J.
%A Leynaert, Aude
%A Clavier, Jacques
%A Guyonneaud, R.
%A Duran, R.
%A Goni, M.
%A Caumette, P.
%< avec comité de lecture
%( Proceedings XIIth international conference on Heavy metals in the environment
%B XIIth international conference on Heavy metals in the environment, ICHMET
%C Grenoble, France
%Y C. Boutron & C. Ferrari
%3 J. Phys. IV France
%V 107
%N 1
%P 41-44
%8 2003-05-26
%D 2003
%Z Environmental Sciences/Global ChangesConference papers
%G English
%L hal-00524823
%U https://hal.univ-brest.fr/hal-00524823
%~ SDE
%~ CNRS
%~ UNIV-PAU
%~ UNIV-BREST
%~ GIP-BE
%~ IPREM
%~ TESTUPPA

%0 Conference Proceedings
%T Trophic bioaccumulation of mercury species in macrobenthic organisms from French coastal sites
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%+ Laboratoire des Sciences de l'Environnement Marin (LEMAR) (LEMAR)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Monperrus, Mathilde
%A Grall, J.
%A Rodriguez, L.
%A Thouzeau, Gérard
%A Jean, F.
%A Chauvaud, Laurent
%A Amouroux, David
%A Donard, O.F.X.
%< avec comité de lecture
%( Proceedings 7th International Conference on Mercury as a Global Pollutant
%B 7th International Conference on Mercury as a Global Pollutant
%C Ljubljana, Slovenia
%3 RMZ-Material and Geoenvironment
%V 51
%P 1226-1230
%8 2004
%D 2004
%Z Environmental Sciences/Global ChangesConference papers
%G English
%L hal-00524829
%U https://hal.univ-brest.fr/hal-00524829
%~ SDE
%~ CNRS
%~ IUEM
%~ UNIV-BREST
%~ UNIV-PAU
%~ GIP-BE
%~ IFREMER
%~ THESES_IUEM
%~ IPREM
%~ CMM
%~ LEMAR
%~ INSU
%~ INC-CNRS

