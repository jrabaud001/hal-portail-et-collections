%0 Conference Proceedings
%T Investigation of karst by combined analysis of seismic and electrical resistivity anisotropy
%+ Géosciences Paris Sud (GEOPS)
%+ Institut pluridisciplinaire de recherche appliquée dans le domaine du génie pétrolier (IPRADDGP)
%+ Laboratoire Souterrain à Bas Bruit (LSBB)
%A Bereš, J.
%A Zeyen, H.
%A Sénéchal, G.
%A Rousset, Dominique
%A Gaffet, Stéphane
%< avec comité de lecture
%( Geophysical Research Abstracts
%B EGU
%C Vienne, Austria
%V 15
%P 455:Poster
%8 2013-04-08
%D 2013
%Z Sciences of the Universe [physics]/Earth SciencesConference papers
%G English
%L hal-00817292
%U https://hal.archives-ouvertes.fr/hal-00817292
%~ CNRS
%~ UNIV-PAU
%~ OCA
%~ UNICE
%~ UNIV-AMU
%~ GEOPS
%~ UNIV-PSUD
%~ UCA-TEST
%~ UNIV-COTEDAZUR
%~ UNIV-AVIGNON
%~ LSBB

%0 Conference Proceedings
%T Seismic time reversal experiment in the Low Noise Underground Laboratory, LSBB (France)
%+ Interactions et dynamique des environnements de surface (IDES)
%A Gaffet, Stéphane
%A Monfret, T.
%A Sénéchal, G.
%A Rousset, Dominique
%A Zeyen, H.
%< avec comité de lecture
%( Geophysical Research Abstracts
%B EGU
%C Vienne, Austria
%V 7
%N 08256
%P 1607-7962/gra/EGU05-A-08256 & Poster
%8 2005-04
%D 2005
%Z Sciences of the Universe [physics]/Earth SciencesConference papers
%G English
%L hal-00412497
%U https://hal.archives-ouvertes.fr/hal-00412497
%~ INSU
%~ CNRS
%~ GEOPS
%~ UNIV-PSUD

%0 Conference Proceedings
%T Detecting faults and stratigraphy in limestone with Ground-Penetrating Radar: A case study in Rustrel
%+ Géosciences Paris Sud (GEOPS)
%+ Institut pluridisciplinaire de recherche appliquée dans le domaine du génie pétrolier (IPRADDGP)
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Saintenoy, A.
%A Sénéchal, G.
%A Rousset, Dominique
%A Brigaud, B.
%A Pessel, M.
%A Zeyen, H.
%Z ACT
%< avec comité de lecture
%B 2017 9th International Workshop on Advanced Ground Penetrating Radar, IWAGPR 2017 - Proceedings
%C Unknown, Unknown Region
%8 2017
%D 2017
%R 10.1109/IWAGPR.2017.7996053
%Z Physics [physics]/Condensed Matter [cond-mat]/Materials Science [cond-mat.mtrl-sci]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Fluids mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of materials [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Mechanical engineering [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Mechanics of materials [physics.class-ph]
%Z Sciences of the Universe [physics]/Earth Sciences/Geophysics [physics.geo-ph]Conference papers
%X Surface Ground-Penetrating Radar (GPR) data have been acquired along the floor as well as along the vertical walls of a tunnel inside a karstic limestone reservoir in Rustrel. Geological study previously demonstrated the existence of stratification planes with an average dip of 25° to the south and numerous subvertical fault planes. The mono-offset GPR profile analysis acquired along the vertical wall of the tunnel demonstrates the presence of dipping reflectors that can be followed as deep as 16 m from the acquisition surface with 250 MHz nominal antennas. The position of these reflectors coincides with observations of faults recorded in a report written during the tunnel excavations. © 2017 IEEE.
%G English
%L hal-01816737
%U https://hal.archives-ouvertes.fr/hal-01816737
%~ CNRS
%~ UNIV-PAU
%~ UNIV-PSUD
%~ GEOPS
%~ UNIV-PSUD-SACLAY
%~ UNIV-PARIS-SACLAY
%~ LFCR
%~ LFCR-CRG

%0 Conference Proceedings
%T Ultra-Wideband GPR Imaging of the Vaucluse Karst Aquifer
%+ Laboratoire d'Electronique, Antennes et Télécommunications (LEAT)
%+ Modélisation et Imagerie en Géosciences - Pau (MIGP)
%+ Géoazur (GEOAZUR 6526)
%+ Géologie et gestion des ressources minérales et énergétiques (G2R)
%A Dauvignac, Jean-Yves
%A Fortino, Nicolas
%A Sénéchal, G.
%A Cresp, A.
%A Yedlin, M.
%A Gaffet, Stéphane
%A Rousset, Dominique
%A Pichot, Christian
%< avec comité de lecture
%B American Geophysical Union (AGU) Fall Meeting
%C San Francisco, United States
%P Paper NS51A-08
%8 2008-12-15
%D 2008
%Z Engineering Sciences [physics]/ElectromagnetismConference papers
%G English
%L hal-00524917
%U https://hal.archives-ouvertes.fr/hal-00524917
%~ INSU
%~ CNRS
%~ GEOAZUR
%~ OCA
%~ UPMC
%~ UNICE
%~ UNIV-PAU
%~ INPL
%~ UNIV-LORRAINE
%~ UCA-TEST
%~ UNIV-COTEDAZUR
%~ SORBONNE-UNIVERSITE

%0 Conference Proceedings
%T Comparative study using an UWB measurement system and a RAMAC GPR system for subsurface imaging of the Vaucluse karst aquifer
%+ Laboratoire d'Electronique, Antennes et Télécommunications (LEAT)
%+ Modélisation et Imagerie en Géosciences - Pau (MIGP)
%+ Géologie et gestion des ressources minérales et énergétiques (G2R)
%+ Géoazur (GEOAZUR 6526)
%A Yedlin, M.
%A Sénéchal, G.
%A Rousset, Dominique
%A Fortino, Nicolas
%A Dauvignac, Jean-Yves
%A Gaffet, Stéphane
%A Monfret, T.
%A Pichot, Christian
%F Invité
%< avec comité de lecture
%B 2010 IEEE International Conference on Wireless Information Technology and Systems (IEEE ICWITS 2010). Special Session on "Microwave Imaging".
%C Honolulu, United States
%P Paper WITS1254.pdf
%8 2010-08-28
%D 2010
%Z Engineering Sciences [physics]/ElectromagnetismConference papers
%G English
%L hal-00519300
%U https://hal.archives-ouvertes.fr/hal-00519300
%~ INSU
%~ CNRS
%~ UNIV-PAU
%~ GEOAZUR
%~ OCA
%~ UPMC
%~ UNICE
%~ INPL
%~ UNIV-LORRAINE
%~ UCA-TEST
%~ UNIV-COTEDAZUR
%~ SORBONNE-UNIVERSITE
%~ LFCR

%0 Conference Proceedings
%T Imaging the lsbb environment from GPR data, along the tunnel and from the surface
%+ Institut pluridisciplinaire de recherche appliquée dans le domaine du génie pétrolier (IPRADDGP)
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%+ Environnement Méditerranéen et Modélisation des Agro-Hydrosystèmes (EMMAH)
%A Sénéchal, G.
%A Rousset, Dominique
%A Carriere, S.
%A Chalikakis, K.
%A Danquigny, C.
%A Emblanch, C.
%Z ACT
%< avec comité de lecture
%B E3S Web of Conferences - 5th I-DUST conference
%C Apt, France
%V 4
%8 2014
%D 2014
%Z Physics [physics]/Condensed Matter [cond-mat]/Materials Science [cond-mat.mtrl-sci]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Fluids mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of materials [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Mechanical engineering [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Mechanics of materials [physics.class-ph]
%Z Sciences of the Universe [physics]/Earth Sciences/Geophysics [physics.geo-ph]Conference papers
%G English
%L hal-01816792
%U https://hal.archives-ouvertes.fr/hal-01816792
%~ CNRS
%~ UNIV-PAU
%~ LFCR
%~ UNIV-AVIGNON
%~ LFCR-CRG
%~ INRAE
%~ EMMAH

%0 Conference Paper
%F Oral 
%T Imaging the LSBB environment form GPR Data, along the tunnel and from the surface
%+ Institut de Recherches Interdisciplinaires de Géologie et de mécanique
%+ Modélisation et Imagerie en Géosciences - Pau (MIGP)
%+ Institut des Sciences de la Terre (ISTerre)
%+ Environnement Méditerranéen et Modélisation des Agro-Hydrosystèmes (EMMAH)
%A Sénéchal, G.
%A Rousset, Dominique
%A Carrière, Simon
%A CHALIKAKIS, Konstantinos
%A Danquigny, Charles
%A EMBLANCH, christophe
%< avec comité de lecture
%B I-Dust - Inter-Disciplinary Underground Science & Technology
%C APT, France
%8 2014-05-05
%D 2014
%Z Sciences of the Universe [physics]/Earth Sciences
%Z Sciences of the Universe [physics]/Earth Sciences/Geophysics [physics.geo-ph]
%Z Sciences of the Universe [physics]/Earth Sciences/HydrologyConference papers
%G English
%L hal-02309722
%U https://hal-univ-avignon.archives-ouvertes.fr/hal-02309722
%~ UNIV-AVIGNON
%~ UNIV-GRENOBLE1
%~ UNIV-PAU
%~ OSUG
%~ CNRS
%~ IFSTTAR
%~ GIP-BE
%~ INSU
%~ UNIV-SAVOIE
%~ ISTERRE
%~ INRAE
%~ USMB-COMUE
%~ UGA
%~ TESTUPPA2
%~ UPPA-OA
%~ EMMAH

%0 Conference Paper
%F Oral 
%T ANR MAXWELL: presentation, evolution of the measurement set-up and results
%+ Laboratoire d'Electronique, Antennes et Télécommunications (LEAT)
%+ Laboratoire Souterrain à Bas Bruit (LSBB)
%+ Géoazur (GEOAZUR 6526)
%+ Institut pluridisciplinaire de recherche appliquée dans le domaine du génie pétrolier (IPRADDGP)
%+ Department of Electrical and Computer Engineering [Vancouver]
%A Dauvignac, Jean-Yves
%A Fortino, Nicolas
%A Perret, F.
%A Gaffet, Stéphane
%A Auguste, M.
%A Monfret, T.
%A Sénéchal, G.
%A Rousset, Dominique
%A Yedlin, M.
%A Pichot, Christian
%< avec comité de lecture
%B ANR MAXWELL Workshop Ultra-Wide Band Subsurface Imaging
%C Saignon, France
%8 2011-10-13
%D 2011
%Z Engineering Sciences [physics]/ElectromagnetismConference papers
%G French
%L hal-00780537
%U https://hal.archives-ouvertes.fr/hal-00780537
%~ INSU
%~ UPMC
%~ CNRS
%~ GEOAZUR
%~ OCA
%~ UNICE
%~ UNIV-AMU
%~ UNIV-PAU
%~ UCA-TEST
%~ UNIV-COTEDAZUR
%~ SORBONNE-UNIVERSITE
%~ UNIV-AVIGNON
%~ LSBB

%0 Conference Paper
%F Oral 
%T Development of an ultra-wideband measurement system for subsurface imaging. Application to the Vaucluse Kazrst Aquifer
%+ Laboratoire d'Electronique, Antennes et Télécommunications (LEAT)
%+ Laboratoire Souterrain à Bas Bruit (LSBB)
%+ Géoazur (GEOAZUR 6526)
%+ Institut pluridisciplinaire de recherche appliquée dans le domaine du génie pétrolier (IPRADDGP)
%+ Department of Electrical and Computer Engineering [Vancouver]
%A Dauvignac, Jean-Yves
%A Fortino, Nicolas
%A Perret, F.
%A Gaffet, Stéphane
%A Auguste, M.
%A Monfret, T.
%A Sénéchal, G.
%A Rousset, Dominique
%A Yedlin, M.
%A Pichot, Christian
%< avec comité de lecture
%B International Workshop "Non Destructive Testing and Evaluation: Physics, Sensors, Materials and Information" ECND-PdL,
%C Nantes, France
%8 2011-11-21
%D 2011
%Z Engineering Sciences [physics]/ElectromagnetismConference papers
%G English
%L hal-00780541
%U https://hal.archives-ouvertes.fr/hal-00780541
%~ INSU
%~ UPMC
%~ CNRS
%~ GEOAZUR
%~ OCA
%~ UNICE
%~ UNIV-AMU
%~ UNIV-PAU
%~ UCA-TEST
%~ UNIV-COTEDAZUR
%~ SORBONNE-UNIVERSITE
%~ UNIV-AVIGNON
%~ LSBB

