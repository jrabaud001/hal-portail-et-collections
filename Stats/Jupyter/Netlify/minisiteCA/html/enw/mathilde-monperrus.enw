%0 Conference Paper
%F Oral 
%T 15-years record of molecular and isotopic signature of Hg in salmon tissues from southwestern France
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Geology and Oceanography Department
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%A Julien, Barre, P.G.
%A Bolliet, Valérie
%A Bérail, Sylvain
%A Bareille, Gilles
%A Tessier, Emmanuel
%A Amouroux, David
%A Donard, O.F.X.
%A Monperrus, Mathilde
%< avec comité de lecture
%B 12. International Conference on Mercury as a Global Pollutant
%C Jeju, South Korea
%8 2016-06-14
%D 2016
%Z Life Sciences [q-bio]/Animal biologyConference papers
%G English
%L hal-02454545
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02454545
%~ UNIV-PAU
%~ ECOBIOP
%~ IPREM
%~ INRA
%~ INC-CNRS
%~ CNRS
%~ IPREM-CME
%~ INRAE
%~ AGREENIUM

%0 Conference Paper
%F Oral 
%T Imaging differential mercury and selenium species bioaccumulation by glass eels using isotopic tracers and laser ablation
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%A Monperrus, Mathilde
%A Pécheyran, Christophe
%A Pedrero, Zoyne
%A Bolliet, Valérie
%< avec comité de lecture
%B 12. International Conference on Mercury as a Global Pollutant
%C Jeju, South Korea
%8 2016-06-14
%D 2016
%Z Life Sciences [q-bio]/Animal biologyConference papers
%G English
%L hal-02454553
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02454553
%~ UNIV-PAU
%~ ECOBIOP
%~ IPREM
%~ IPREM-CAPT
%~ INRA
%~ INC-CNRS
%~ CNRS
%~ INRAE
%~ AGREENIUM

%0 Conference Paper
%F Oral 
%T Investigations on mercury transformations in seawater using isotopically enriched species
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%A Monperrus, Mathilde
%A Tessier, Emmanuel
%A Amouroux, David
%A Leynaert, Aude
%A De Wit, Rutger
%A Donard, O.F.X.
%< avec comité de lecture
%3 RMZ - Materials and Geoenvironment
%B 7 th International Conference on Mercury as a Global Pollutant
%C Ljubjana, Slovenia
%V 51
%N 2
%P 1221-1225
%8 2004-06-28
%D 2004
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Analytical chemistry
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Material chemistryConference papers
%X The aim of this work was to developed field incubations of seawater using isotopically enriched inorg. mercury (199IHg) and monomethylmercury (201MMHg) to understand processes involved in mercury transformations. Expts. were carried out for different sites of the Mediterranean Sea, at coastal and open water stations as a contribution to the MERCYMS project in the framework of the EU ELOISE program. Water incubations were conducted under various controlled conditions (dark and light, filtered and unfiltered water) to assess the major biogeochem. pathways regulating mercury transformations. Biol. characteristics of the incubated samples were also monitored through phytoplankton and bacteria diversity. Isotopically enriched species were used as chem. tracers and have allowed the direct detn. of specific methylation and demethylation yields. This exptl. approach demonstrates that the use of isotopically labeled mercury species offers a unique potential to examine mercury transformation processes in seawater.
%G English
%L hal-01981919
%U https://hal.archives-ouvertes.fr/hal-01981919
%~ IPREM
%~ UNIV-PAU
%~ CNRS
%~ INC-CNRS
%~ TESTUPPA

%0 Conference Paper
%F Oral 
%T Methylmercury in European glass eels from the Bay of Biscay : concentration level and impact on the migratory behaviour
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%A Claveau, Julie
%A Monperrus, M.
%A Pinaly, Hervé
%A Bardonnet, Agnes
%A Amouroux, David
%A Bolliet, Valérie
%< sans comité de lecture
%B 13. International symposium on oceanography of the Bay of Biscay
%C Santander, Spain
%S 13. International symposium on oceanography of the Bay of Biscay, ISOBAY13
%P 207 p.
%8 2012-04-11
%D 2012
%K glass eel
%K methylmercury
%K migration
%Z Life Sciences [q-bio]Conference papers
%G English
%L hal-02746020
%U https://hal.inrae.fr/hal-02746020
%~ INRAE
%~ INRA
%~ UNIV-PAU
%~ CNRS
%~ ECOBIOP
%~ IPREM
%~ AGREENIUM

%0 Conference Paper
%T Methylmercury in European glass eel from the Biscay bay (Atlantic coast) : concentration level, assimilation and impact on the migratory behaviour
%+ Université de Pau et des Pays de l'Adour (UPPA)
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%A Monperrus, Mathilde
%A Bolliet, Valérie
%A Navarro, Patricia
%A Bardonnet, Agnes
%A Amouroux, David
%< avec comité de lecture
%B 10. International Conference on Mercury as a Global Pollutant
%C Halifax, Canada
%S 10th International conference on mercury as a global pollutant. Abstract volume
%P 350 p.
%8 2011-07-24
%D 2011
%Z Environmental Sciences/Biodiversity and EcologyConference papers
%G English
%L hal-02748277
%U https://hal.inrae.fr/hal-02748277
%~ INRAE
%~ INRA
%~ SDE
%~ UNIV-PAU
%~ GIP-BE
%~ ECOBIOP

%0 Conference Paper
%T 15-years record of molecular and isotopic signature of Hg in salmon tissues from southwestern France
%+ Institut des Sciences Analytiques et Physico-Chimique pour l'Environnement et les Matériaux
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%A Barre, Julien P.G.
%A Bolliet, Valérie
%A Berail, Sylvain
%A Bareille, Gilles
%A Tessier, Emmanuel
%A Amouroux, David
%A Donard, Olivier F.X.
%A Monperrus, Mathilde
%< avec comité de lecture
%B 12. International Conference on Mercury as a Global Pollutant ICMGP 2015
%C Jeiju, South Korea
%8 2015-06-14
%D 2015
%Z Life Sciences [q-bio]/Animal biology
%Z Environmental Sciences/Biodiversity and EcologyConference papers
%G English
%L hal-02795411
%U https://hal.inrae.fr/hal-02795411
%~ INRAE
%~ INRA
%~ SDE
%~ UNIV-PAU
%~ GIP-BE
%~ ECOBIOP

%0 Conference Paper
%T Imaging differential mercury and selenium species bioaccumulation by glass eels using isotopic tracers and laser ablation
%+ Institut des Sciences Analytiques et Physico-Chimique pour l'Environnement et les Matériaux
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%A Monperrus, Mathilde
%A Pécheyran, Christophe
%A Pedredo, Zoyne
%A Bolliet, Valérie
%< avec comité de lecture
%B 12. International Conference on Mercury as a Global Pollutant ICMGP 2015
%C Jeiju, South Korea
%8 2015-06-14
%D 2015
%Z Life Sciences [q-bio]/Animal biology
%Z Environmental Sciences/Biodiversity and EcologyConference papers
%G English
%L hal-02794897
%U https://hal.inrae.fr/hal-02794897
%~ INRAE
%~ INRA
%~ SDE
%~ UNIV-PAU
%~ GIP-BE
%~ ECOBIOP

%0 Conference Paper
%F Oral 
%T Etude des taux de contamination des civelles de l'Adour par le méthyl-mercure à l'échelle individuelle. Fluctuations saisonnières et annuelles, relation avec la condition énergétique des individus
%+ Ecologie Comportementale et Biologie des Populations de Poissons (ECOBIOP)
%+ Université de Pau et des Pays de l'Adour (UPPA)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Claveau, Julie
%A Monperrus, M.
%A Pinaly, Hervé
%A Bardonnet, Agnes
%A Amouroux, David
%A Bolliet, Valérie
%A Jarry, Marc
%F Invité
%< sans comité de lecture
%B Rencontres Anguille 2012
%C Rochefort sur Mer, France
%S Programme et résumés
%8 2012-04-03
%D 2012
%K méthyl-mercure
%Z Life Sciences [q-bio]Conference papers
%G French
%L hal-02749883
%U https://hal.inrae.fr/hal-02749883
%~ INRAE
%~ INRA
%~ UNIV-PAU
%~ ECOBIOP
%~ IPREM
%~ INC-CNRS
%~ CNRS
%~ AGREENIUM

%0 Conference Paper
%F Oral 
%T A first estimation of uncertainties related to microplastic sampling in rivers
%+ laboratoire Eau Environnement et Systèmes Urbains (LEESU)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Bruge, Antoine
%A Dhamelincourt, Marius
%A Gasperi, Johnny
%A Lanceleur, Laurent
%A Monperrus, Mathilde
%A Tassin, Bruno
%< avec comité de lecture
%B MICRO 2018 Fate and Impact of Microplastics: Knowledge, Actions and Solutions, Lanzarote (Espagne), novembre 2018.
%C Arrecife (Lanzarote), Spain
%8 2018-11-19
%D 2018
%Z Environmental SciencesConference papers
%G English
%L hal-01929292
%U https://hal-enpc.archives-ouvertes.fr/hal-01929292
%~ ENPC
%~ IPREM
%~ UNIV-PAU
%~ IPREM-CME
%~ CNRS
%~ SDE
%~ UPEC-UPEM
%~ GIP-BE
%~ PARISTECH
%~ INC-CNRS
%~ AGROPARISTECH
%~ ENPC-LEESU
%~ UPEC

%0 Conference Paper
%F Oral 
%T Mercury transformation by anaerobic bacterial communities selected from estuarine sediments
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%A Goñi, Marisol
%A Dias, M
%A Monperrus, Mathilde
%A Salvado, J.C.
%A Amouroux, David
%A Duran, Robert
%A Donard, O.F.X.
%A Caumette, Pierre
%A Guyoneaud, Remy
%< avec comité de lecture
%3 RMZ - Materials and Geoenvironment
%B 7th International Conference on mercury as a global pollutant
%C Ljubjana, Slovenia
%V 51
%N 2
%P 1006-1009
%8 2004-06-28
%D 2004
%K sulphate reducing bacteria
%K sediments
%K estuary
%K mercury
%K methylation
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Analytical chemistry
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Material chemistryConference papers
%X The fate of mercury species in the environment strongly depends on microbial activities and environmental conditions. Methylation esp. is one of the major processes, which transform the inorg. forms of the metal into toxic and easily bioaccumulated org. forms. Thus, we designed an exptl. setup to study mercury biotransformation in estuarine ecosystems. Sediments were incubated in slurries and polluted with inorg. mercury (HgCl2)​. Mercury methylation was obsd. only in slurries maintained under anoxic conditions. The main microbial populations were isolated and identified by partial sequencing of 16S rDNA. The methylation, studied by gas chromatog.-​inductively coupled plasma mass spectrometry, demonstrated that only sulfate-​reducing bacteria were the methylators among this community, which also comprise denitrifiers and fermentative bacteria.
%G English
%L hal-01981895
%U https://hal.archives-ouvertes.fr/hal-01981895
%~ IPREM
%~ UNIV-PAU
%~ CNRS
%~ INC-CNRS
%~ TESTUPPA

%0 Conference Proceedings
%T A Combined Numerical/Experimental Approach to Understand Stratification and Mixing Processes in the Adour Estuary
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%+ Institut méditerranéen d'océanologie (MIO)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Defontaine, Sophie
%A Morichon, Denis
%A Sous, Damien
%A Monperrus, Mathilde
%Z COM
%< avec comité de lecture
%B XVIth International Symposium on Oceanography of the Bay of Biscay (ISOBAY 16)
%C anglet, France
%8 2018-06-05
%D 2018
%K numerical modelling
%K stratification
%K hydrodynamics
%K estuary
%K field experimentation
%Z Physics [physics]
%Z Chemical Sciences
%Z Chemical Sciences/Analytical chemistryConference papers
%X River plumes play a central role in the input and transport of dissolved and particulate matters that can lead to the dispersion of pollutants into coastal waters. Understanding the dynamics of river plumes is thus essential to evaluate the potential threat to coastal-marine ecosystems, in order to improve management and mitigation policies. The Adour estuary morphology combined with the competition between the tide and the river flow result in a partially stratified estuary. A dual approach, coupling field experiments and numerical modeling, has been carried out to better understand the physical processes that govern the Adour river plume hydrodynamics. A field campaign was performed inside the estuary in September 2017. Velocity profilers and high frequency point current-meters were moored, at two different stations inside the estuary, during one month. Series of salinity profiles were realized at four stations including mooring stations. Meanwhile, a numerical model has been developed based on the open source code TELEMAC-3D to investigate the interaction between light continental waters and heavy salty marine waters, focusing on the salt-wedge intrusion inside the estuary. The resulting density stratification playing a major role in the transfer of water masses and the transport of dissolved and particulate matters, its precise reproduction isone of the cornerstones of our study. Inside the estuary, a competition between density gradient and velocity/bottom shear regulates mixing processes. First comparisons between model and field data show a good agreement between measured and simulated water levels and velocity profiles. The correct representation of density stratification processes by the numerical model remains, as expected, a challenging issue. The choice of the turbulence model, which is of the foremost importance for a good representation of the interaction between water masses, will be discussed within the physical framework provided by field measurements
%G English
%L hal-02154268
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02154268
%~ UNIV-PAU
%~ SIAME
%~ IPREM
%~ CNRS
%~ INC-CNRS
%~ UNIV-TLN
%~ UNIV-AMU
%~ IPREM-CAPT
%~ MIO
%~ OSU-INSTITUT-PYTHEAS
%~ TESTUPPA
%~ MIO-OPLC
%~ INSU

%0 Conference Proceedings
%T Turbulence Measurements in a Stratified Man-Controlled Estuary, the Adour Case
%+ Institut méditerranéen d'océanologie (MIO)
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Sous, Damien
%A Defontaine, Sophie
%A Morichon, Denis
%A Bhairy, Nagib
%A Lanceleur, Laurent
%A Monperrus, Mathilde
%Z COM
%< avec comité de lecture
%B XVIth International Symposium on Oceanography of the Bay of Biscay (ISOBAY 16)
%C anglet, France
%8 2018-06-05
%D 2018
%K estuary
%K turbulence
%K stratification
%K tide
%Z Physics [physics]
%Z Chemical Sciences
%Z Chemical Sciences/Analytical chemistryConference papers
%X The Adour river is a partially stratified river flowing into the Basque Country coastal waters. The dynamics of estuarine waters and suspended matters, including density stratification, mixing, residence and renewal times, is controlled by the competition between the input of terrestrial waters with strongly varying discharge and suspended load and the meso-tidal fluctuations of the ocean level. Since pycnoclines can form a decisive barrier for exchange processes within a water body, it is important to better understand the processes that govern their evolution in particular for water quality studies. A turbulence-dedicated field campaign was performed in the lower estuary in September 2017. The instrumentation is based on fixed bottom moorings with high frequency point currentmeters and velocity profilers. During the well-mixed phases of the tidal cycles, the flow properties are in good agreement with the standard hypothesis of the canonical turbulent boundary layer in open channel flows. Measurements carried out during the arrival of the salt wedge at rising tide demonstrate the strong influence of density stratification on the flow dynamics. Both turbulent kinetic energy and momentum fluxes are damped by the density stratification, which consequently affect the mixing and resuspension processes near the river bed. A second series of experiments has been performed in order to identify the turbulent properties away from the bed. Instrumented lagrangian drifters have been deployed during the two-layers flow pattern in the estuary. The results again show the important effect of density stratification. The local measurements of small-scale turbulent processes will be placed and discussed in a more general contextaiming to describe the intra-estuary exchanges between river and ocean waters and the fate of dissolved and suspended matters
%G English
%L hal-02154236
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02154236
%~ SIAME
%~ UNIV-PAU
%~ IPREM
%~ CNRS
%~ INC-CNRS
%~ UNIV-TLN
%~ UNIV-AMU
%~ IPREM-CAPT
%~ OSU-INSTITUT-PYTHEAS
%~ MIO
%~ TESTUPPA
%~ IPREM-CME
%~ MIO-OPLC
%~ INSU

%0 Conference Proceedings
%T Transfer of metallic contaminants at the sediment-water interface in a coastal lagoon: role of the biological and microbial activity
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%A Amouroux, David
%A Monperrus, Mathilde
%A Point, D.
%A Tessier, Emamnuel
%A Bareille, G.
%A Donard, O.
%A Chauvaud, Laurent
%A Thouzeau, Gérard
%A Jean, F.
%A Grall, J.
%A Leynaert, Aude
%A Clavier, Jacques
%A Guyonneaud, R.
%A Duran, R.
%A Goni, M.
%A Caumette, P.
%< avec comité de lecture
%( Proceedings XIIth international conference on Heavy metals in the environment
%B XIIth international conference on Heavy metals in the environment, ICHMET
%C Grenoble, France
%Y C. Boutron & C. Ferrari
%3 J. Phys. IV France
%V 107
%N 1
%P 41-44
%8 2003-05-26
%D 2003
%Z Environmental Sciences/Global ChangesConference papers
%G English
%L hal-00524823
%U https://hal.univ-brest.fr/hal-00524823
%~ SDE
%~ CNRS
%~ UNIV-PAU
%~ UNIV-BREST
%~ GIP-BE
%~ IPREM
%~ TESTUPPA

%0 Conference Proceedings
%T Mercury species benthic fluxes measurements in coastal environments as influenced by the biological activity
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%+ Laboratoire des Sciences de l'Environnement Marin (LEMAR) (LEMAR)
%A Amouroux, David
%A Monperrus, Mathilde
%A Point, D.
%A Tessier, E.
%A Chauvaud, Laurent
%A Thouzeau, Gérard
%A Jean, F.
%A Leynaert, Aude
%A Clavier, Jacques
%A Grall, J.
%A Amice, Erwan
%< avec comité de lecture
%( Proceedings 7th International Conference on Mercury as a Global Pollutant
%B 7th International Conference on Mercury as a Global Pollutant
%C Ljubljana, France
%3 RMZ-Material and Geoenvironment
%V 51
%P 770-773
%8 2004
%D 2004
%Z Sciences of the Universe [physics]/Earth Sciences/Oceanography
%Z Environmental SciencesConference papers
%G English
%L hal-00524827
%U https://hal.univ-brest.fr/hal-00524827
%~ CNRS
%~ IUEM
%~ UNIV-BREST
%~ UNIV-PAU
%~ GIP-BE
%~ IFREMER
%~ THESES_IUEM
%~ IPREM
%~ CMM
%~ LEMAR
%~ INSU
%~ SDE
%~ INC-CNRS

%0 Conference Proceedings
%T Trophic bioaccumulation of mercury species in macrobenthic organisms from French coastal sites
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%+ Laboratoire des Sciences de l'Environnement Marin (LEMAR) (LEMAR)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Monperrus, Mathilde
%A Grall, J.
%A Rodriguez, L.
%A Thouzeau, Gérard
%A Jean, F.
%A Chauvaud, Laurent
%A Amouroux, David
%A Donard, O.F.X.
%< avec comité de lecture
%( Proceedings 7th International Conference on Mercury as a Global Pollutant
%B 7th International Conference on Mercury as a Global Pollutant
%C Ljubljana, Slovenia
%3 RMZ-Material and Geoenvironment
%V 51
%P 1226-1230
%8 2004
%D 2004
%Z Environmental Sciences/Global ChangesConference papers
%G English
%L hal-00524829
%U https://hal.univ-brest.fr/hal-00524829
%~ SDE
%~ CNRS
%~ IUEM
%~ UNIV-BREST
%~ UNIV-PAU
%~ GIP-BE
%~ IFREMER
%~ THESES_IUEM
%~ IPREM
%~ CMM
%~ LEMAR
%~ INSU
%~ INC-CNRS

%0 Conference Proceedings
%T Biogeoquímica del mercurio en el lago tropical de altura Uru Uru (Altiplano boliviano)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Hydrosciences Montpellier (HSM)
%+ Polllutions Minières Environnement et Santé (PoMES)
%+ Géosciences Environnement Toulouse (GET)
%A Alanoca, Lucia
%A Guedron, Stephane
%A Monperrus, Mathilde
%A Amouroux, David
%A Tessier, Emmanuel
%A Seyler, Patrick
%A Goñi, Marisol
%A Guyoneaud, Remy
%A Achá Cordero, Darío
%A Audry, Stephane
%A Garcia, Maria Eugenia
%A Quintanilla, J
%A Point, David, E
%< avec comité de lecture
%B International colloquium on current and ancient contamination in Andes aquatic ecosystems = Coloquio internacional sobre la contaminación actual y histórica en los ecosistemas acuáticos Andinos = Colloque international sur la contamination actuelle et historique des écosystèmes aquatiques andins
%C La Paz, Bolivia
%8 2016-05-03
%D 2016
%Z Sciences of the Universe [physics]/Earth Sciences/HydrologyConference papers
%X En la región del Altiplano boliviano, de condiciones ambientales extremas (3700 m.s.n.m. de altitud, elevada radiación UV, menor disponibilidad de O2), se encuentra el lago Uru Uru. De aguas poca profundas (0-1 m), elevada gradiente en la salinidad de sus aguas y alta productividad primaria, es impactado por descargas de afluentes mineros y urbanos. Las elevadas concentraciones de monometilmercurio (MMHg), un neurotóxico muy potente, medidas en peces y aves (Molina et al., 2012), ha cuestionando la fuente y transformaciones del Hg para la producción de MMHg en los ecosistemas de altura. La producción de MMHg es un proceso clave que dirige la bioacumulación del Hg en la cadena alimentaria y los procesos de metilación y demetilación, son los mayores patrones de transformación que regulan la producción neta de MMHg. Para dar respuesta a estas preguntas concentraciones de metales, metaloides (Fe, Mn, Sb, Ti y W), elementos mayoritarios, especies de mercurio (mercurio inorgánico (IHg), MMHg, mercurio vapor (Hg°) y dimetilmercurio (DMHg)) fueron medidas en muestras de aguas; MMHg y mercurio total (THg) en sedimentos y aguas intersticiales de sedimentos del lago Uru Uru. Al mismo tiempo se realizó un seguimiento de la distribución espacio temporal de Norte a Sur del lago, para todos estos parámetros, durante las épocas seca y húmeda (octubre, 2010 y mayo 2011). Además se determinó los potenciales de metilación (M) y demetilación (D) en situ en muestras de sedimentos, aguas, periphyton y agregados bio-orgánicos flotantes en los sitios norte (NS) y sur (SS) del lago Uru Uru, utilizando trazadores isotópicos estables de 199Hg y MM201Hg. Entre los resultados más importantes se encuentra la elevada concentración de metales pesados y de MMHg en forma disuelta, la cual llega hasta el 49 ± 11 % con respecto al mercurio total disuelto (THgd). Se identifica a los sedimentos como la fuente principal de MMHg, cuyo flujo difusivo alcanzan hasta 227 ng m-2 day-1 de MMHg durante la época seca. Esta aseveración es sustentada con la medición de los potenciales de M/D en las diferentes matrices. Los potenciales Netos de M, medidos en el sitio Norte, indican que los efluentes mineros y urbanos promueven la producción de MMHg en las aguas y en los sedimentos (3,4±1,2 ng g−1 day−1) durante la época seca. Si bien se identifica a los agregados bio-orgánicos como los mayores productores de MMHg (5,8 ng MMHg g−1 day−1, época seca), son los sedimentos los mayores representantes de producción de esta sustancia tóxica, tomando en cuenta la diferencia de masa total de cada matriz en el lago. Se determina también que el rol del periphyton de las totoras es el de la descomposición de MMHg (-2,1 ng MMHg g−1 day−1), pero también el de almacenamiento de MMHg. En suma se demuestra que existe un efecto sinérgico de los drenajes ácidos mineros (DAM) según la temporada y los efluentes urbanos en los ecosistemas productivos, al cual la evaporación superficial promueven las elevadas emisiones de contaminantes organometálicos como MMHg en la columna de agua, cuya producción y la estabilidad se nutre de la abundante materia orgánica y de los ligandos presentes en el medio.
%G English
%L hal-02128299
%U https://hal.umontpellier.fr/hal-02128299
%~ UNIV-MONTPELLIER
%~ IPREM
%~ UNIV-PAU
%~ UNIV-TLSE3
%~ CNRS
%~ OMP-LMTG
%~ OMP-GET
%~ AGROPOLIS
%~ GIP-BE
%~ OMP
%~ B3ESTE
%~ HSM
%~ INC-CNRS
%~ CNES
%~ IPREM-CME
%~ INSU
%~ METEO

