%0 Conference Proceedings
%T Regards croisés, anglais, américain et français, sur la guerre de 1870-1871
%+ Arts / Langages : Transitions et Relations (ALTER)
%+ Institut d’Histoire des Représentations et des Idées dans les Modernités (IHRIM)
%A Guillaume, Isabelle
%< avec comité de lecture
%( Actes du XXXIXe congrès de la SFLGC publiés en ligne sous la direction de Michèle Finck, Tatiana Victoroff, Enrica Zanin, Pascal Dethurens, Guy Ducrey, Yves-Michel Ergal et Patrick Werly
%B Littérature et expériences croisées de la guerre. Apports comparatistes
%C Strasbourg, France
%Y Michèle Finck
%Y Tatiana Victoroff
%Y Enrica Zanin
%Y Pascal Dethurens
%Y Guy Ducrey
%Y Yves-Michel Ergal
%Y Patrick Werly
%3 Littérature et expériences croisées de la guerre. Apports comparatistes
%8 2014-11-13
%D 2014
%K Guerre de 1870. Littérature générale. Livres pour enfants
%Z Humanities and Social Sciences/LiteratureConference papers
%X La guerre de 1870-1871 a inspiré des romanciers anglais et américains, comme George Alfred Henty dans The Young Franc Tireurs (Londres : Griffith and Farrar, 1872) et Elizabeth Williams Champney dans Three Vassar Girls in France (Boston : Estes and Lauriat, 1888). En retour des écrivains français ont placé des Anglo-Saxons aux côtés des combattants qui ont échoué à repousser l’invasion du sol national. Elle a donc fait se croiser les regards au-dessus, non seulement du Rhin, mais aussi de la Manche et de l’Atlantique. Et elle a nourri un imaginaire romanesque rempli de ballons et de pigeons voyageurs, de francs-tireurs et d’espions. Cette circulation de thèmes communs recouvre une disparité dans les enjeux de l’écriture du conflit. Du côté anglais et américain, celle-ci reste anecdotique et elle sert, tout au plus, à transmettre un message pacifiste (Champney) et patriotique (Champney, Henty). Du côté français, elle remplit deux fonctions sociales : édulcorer la défaite, transmettre un enseignement dans le but de redresser le pays vaincu.
%G French
%L halshs-01986486
%U https://halshs.archives-ouvertes.fr/halshs-01986486
%~ SHS
%~ CNRS
%~ UNIV-PAU
%~ ENS-LYON
%~ UNIV-BPCLERMONT
%~ UNIV-ST-ETIENNE
%~ PRES_CLERMONT
%~ ALTER
%~ UNIV-LYON3
%~ UNIV-LYON2
%~ UDL
%~ UNIV-LYON

%0 Conference Proceedings
%T Joséphine-Blanche Colomb : les jeunes filles modèles acquièrent une profession
%+ Arts / Langages : Transitions et Relations (ALTER)
%+ Institut d’Histoire des Représentations et des Idées dans les Modernités (IHRIM)
%A Guillaume, Isabelle
%< avec comité de lecture
%( La littérature en bas-bleus. Tome III. Les romancières en France de 1870 à 1914
%B La littérature en bas-bleus. Les romancières en France de 1870 à 1914
%C Lille, France
%Y Andrea Del Lungo, Brigitte Louichon
%I Classiques Garnier
%3 Masculin/féminin dans l’Europe moderne
%P 113-130
%8 2013-10-15
%D 2013
%Z Humanities and Social Sciences/LiteratureConference papers
%X Pendant les vingt ans qui ont suivi la guerre de 1870, Joséphine-Blanche Colomb (1833-1891) a écrit une œuvre abondante publiée par la Librairie Hachette à l’époque où la maison d’édition du boulevard Saint-Germain développe son secteur destiné aux adolescents en créant l’hebdomadaire Le journal de la jeunesse et des collections comme la « Nouvelle collection pour la jeunesse » et « La bibliothèque des écoles et des familles ». La romancière a collaboré à ces différentes entreprises et perçu, pour toutes ses publications en volume, un droit d’auteur proportionnel au prix de vente et au nombre d’exemplaires. Elle offre ainsi l’exemple d’une romancière pour la jeunesse bénéficiant de contrats lucratifs. Ce n’est pas son seul point commun avec Zénaïde Fleuriot. Les deux romancières que Hachette a rééditées jusque dans les années 1930 ont construit leurs romans à partir des personnages et des thèmes à la mode dans l’édition de leur temps. Elles partagent, aussi, une conception identique de la fonction de la femme auteur écrivant pour transmettre des idées, sur le modèle de George Sand. Leur démarche recoupe parfaitement les enjeux de la littérature pour la jeunesse qui vise à dispenser des valeurs. Leur positionnement idéologique apparemment opposé a conditionné la réception de leurs romans. Joséphine-Blanche Colomb a accompagné, voire anticipé, les mutations des modèles féminins en incitant les jeunes filles de la bourgeoisie à s’assurer une source de revenus par leur travail. Protestante, comme les principaux réformateurs qui ont fondé l’école républicaine, elle situe sa réflexion, non pas sur le terrain des études, mais sur celui de la conquête d’une indépendance financière. Romancière, et non idéologue, Joséphine-Blanche Colomb a transmis son point de vue en créant des personnages qui suscitent l’identification et en transformant les visées professionnelles de ses héroïnes en aliment du rêve, à la manière des écrivains américains du self made man.
%G French
%L hal-01987117
%U https://hal.archives-ouvertes.fr/hal-01987117
%~ CNRS
%~ UNIV-PAU
%~ ENS-LYON
%~ UNIV-BPCLERMONT
%~ UNIV-ST-ETIENNE
%~ SHS
%~ PRES_CLERMONT
%~ ALTER
%~ UNIV-LYON3
%~ UNIV-LYON2
%~ UDL
%~ UNIV-LYON

%0 Conference Proceedings
%T The Secret Garden de Frances Hodgson Burnett : mort et résurrection dans un jardin anglais
%+ Arts / Langages : Transitions et Relations (ALTER)
%+ Institut d’Histoire des Représentations et des Idées dans les Modernités (IHRIM)
%A Guillaume, Isabelle
%F Invité
%< avec comité de lecture
%( Jardin et intimité dans la littérature européenne entre 1750 et 1920
%B Jardin et intimité dans la littérature européenne entre 1750 et 1920
%C Clermont-Ferrand, France
%Y Simone Bernard-Griffiths
%Y Françoise Le Borgne
%Y Daniel Madalénat
%I Presses universitaires Blaise Pascal, « Révolutions et romantismes »
%3 Jardin et intimité dans la littérature européenne entre 1750 et 1920
%P 483-502
%8 2006-03-22
%D 2006
%Z Humanities and Social SciencesConference papers
%X The Secret Garden que la romancière anglo-américaine Frances Hodgson Burnett a publié en feuilletons dans The American Magazine en 1909, en volume en 1911 répond à un projet ambitieux. Des classiques de la littérature pour enfants et de la littérature anglaise comme Jane Eyre et Wuthering Heights apparaissent en filigrane dans le « jardin secret » du roman. Burnett situe ainsi une histoire de deuil et de renaissance dans le Yorkshire gothique et romantique des sœurs Brontë. Territoire fertile de l’émulation littéraire, The Secret Garden permet à son auteur de formuler un propos original et optimiste. Théâtre de métamorphoses où la végétation change au rythme des saisons, où la mort apparente ouvre toujours sur un nouveau cycle, le jardin est, à la fois, le reflet de l’évolution intérieure des personnages du roman et le modèle de toute individualité épanouie qui refuse la fixité comme indice de mort.
%G French
%L hal-02898361
%U https://hal.archives-ouvertes.fr/hal-02898361
%~ UNIV-LYON3
%~ UNIV-PAU
%~ ENS-LYON
%~ UNIV-BPCLERMONT
%~ UNIV-ST-ETIENNE
%~ CERHAC
%~ UNIV-LYON2
%~ IHRIM
%~ CNRS
%~ UDL
%~ UNIV-LYON
%~ ALTER
%~ PRES_CLERMONT
%~ SHS

