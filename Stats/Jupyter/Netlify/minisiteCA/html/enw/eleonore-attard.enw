%0 Conference Paper
%T Biological ice nucleation activity in cloud water
%+ Université d'Auvergne - Clermont-Ferrand I (UdA)
%+ Unité de Pathologie Végétale (PV)
%A Joly, Muriel
%A Amato, Pierre
%A Deguillaume, Laurent
%A Attard, Eleonore
%A Morris, Cindy E.
%A Monier, Marie
%A Sancelme, Martine
%A Delort, Anne-Marie
%< avec comité de lecture
%B 32. AAAR Annual conference
%C Portland, Oregon, United States
%8 2013-09-30
%D 2013
%Z Environmental Sciences/Biodiversity and Ecology
%Z Environmental Sciences/Global ChangesConference papers
%G English
%L hal-02806971
%U https://hal.inrae.fr/hal-02806971
%~ INRAE
%~ INRA
%~ SDE
%~ AGROPOLIS
%~ GIP-BE
%~ UNIV-CLERMONT1
%~ PRES_CLERMONT
%~ AGREENIUM
%~ PV

%0 Conference Proceedings
%T Cloudborne microbial populations at the puy de Dôme (France) and their putative participation to atmospheric physico-chemical processes
%+ Synthèse et étude de systèmes à intêret biologique (SEESIB)
%A Amato, Pierre
%A Vaitilingom, Mickaël
%A Attard, Eléonore
%A Vinatier, Virginie
%A Sancelme, Martine
%A Gaiani, Nicolas
%A Delort, A.M.
%F Invité
%< avec comité de lecture
%B University of Innsbruck, Institute of Ecology
%C Innsbruck, Austria
%8 2010-06-07
%D 2010Conference papers
%X Recent observations have raised the hypothesis that biochemical pathways could be involved in cloud chemistry and physics. Indeed, cloud water carries about ~104 microorganisms mL-1, and a metabolic activity has been detected in cloud water samples. Since 2003, more than 34 cloud events have been sampled from the puy de Dôme summit (1465 m a.s.l., France) using droplet samplers. These have been characterized for their chemical and microbiological contents, and viable bacterial strains have been isolated by culture. In laboratory, we are currently focusing on the capacity of isolates in (i) initiating the formation of ice crystals and act as ice nuclei (IN), (ii) producing biosurfactants and initiate the formation of cloud droplets (cloud condensation nuclei (CCN)) and (iii) transforming the organic compounds present in the atmosphere (formate, acetate, lactate, succinate, oxalate, formaldehyde and methanol). Experimental setups (microcosms) consist of bacterial suspensions incubated in liquid solutions of which the chemical composition reflects that of our cloud water samples: these have been designed for mimicking the conditions encountered under either oceanic or anthropic influence. The capacity of selected isolates to catalyze the formation of ice was tested by immersion freezing and in cloud chamber (AIDA, Karlsruhe). Ice nucleation active bacteria at -5°C were found in cloud samples; in cloud water, such cells could be responsible for the freezing of droplets into crystals, which often initiates precipitations. Preliminary analysis of the data showed that the IN efficiency of the isolates was dependent on the chemical composition of cloud water. Isolates have also been screened for biosurfactant production, by emulsification, oil spreading and surface tension meniscus tests. A majority of strains were positive to at least one test, suggesting that such cells would be efficient CCN in the atmosphere and could participate to the formation of liquid clouds. In order to investigate the implication of microbes in the chemistry of cloud water, the rates of biotransformation of atmospheric organic compounds by isolated strains under microcosm conditions have been determined. They were in the range of ~10-2 pmol-1 cell-1 d-1 at 5°C and were in general decreased in the “anthropic” versus “oceanic” medium. The presence of free radicals such as OH● was also detrimental. A basic numeric model of atmospheric chemistry taking into account realistic concentrations of organic compounds, of bacteria, and of OH● and NO3● radicals in a liquid cloud at 5°C for daytime and nighttime and their respective reaction rates towards the tested organic acids was applied on the results. This confirmed that the presence of OH● radicals was largely responsible for the oxidation of organic species during the day, but it is noteworthy that during the night, bacterial activity would dominate the reactivity of formic, acetic and succinic acids by contributing to more than 90 % of their degradation. Overall, our results suggest that cloudborne microorganisms are actors of the physico-chemical processes occurring in the atmosphere. The composition of cloud water modulates these roles, and this raises the question of the negative impact that human emissions could have on the capacity of microbes to interact with atmospheric processes and disseminate by the atmosphere.
%G English
%L hal-00493211
%U https://hal.archives-ouvertes.fr/hal-00493211
%~ CNRS
%~ UNIV-BPCLERMONT
%~ ICC
%~ PRES_CLERMONT

%0 Conference Paper
%F Oral 
%T Cloud condensation and ice nucleation activity of bacteria isolated from cloud water
%+ Synthèse et étude de systèmes à intêret biologique (SEESIB)
%+ Forschungszentrum Karlsruhe (FZK)
%+ Unité de Pathologie Végétale (PV)
%+ Karlsruhe Institute of Technology (KIT)
%+ Eidgenössische Technische Hochschule - Swiss Federal Institute of Technology in Zürich [Zürich] (ETH Zürich)
%+ Institute for Atmospheric and Climate Science [Zürich] (IAC)
%+ Institute for Meteorology and Climate Research (IMK)
%+ Institute of Environmental Physics [Heidelberg] (IUP)
%A Attard, Eléonore
%A Delort, Anne-Marie
%A Möhler, Ottmar
%A Morris, Cindy E.
%A Oehm, Caroline
%A Chou, Cédric
%A Stetzer, Olaf
%A Leisner, Thomas
%Z Communication orale
%< sans comité de lecture
%B IUGG 2011, Earth on the Edge Science for a Sustainable Planet
%C Melbourne, Australia
%8 2011-06-28
%D 2011
%Z Life Sciences [q-bio]Conference papers
%G English
%L hal-00609987
%U https://hal.archives-ouvertes.fr/hal-00609987
%~ CNRS
%~ INRA
%~ ICC
%~ UNIV-BPCLERMONT
%~ PRES_CLERMONT
%~ INRAE
%~ AGROPOLIS
%~ AGREENIUM
%~ PV

%0 Conference Paper
%F Oral 
%T Living microorganisms in clouds: their identifi cation and possible roles in cloud physicochemical processes
%+ Synthèse et étude de systèmes à intêret biologique (SEESIB)
%+ Laboratoire de météorologie physique (LaMP)
%A Vaitilingom, Mickaël
%A Amato, Pierre
%A Gaiani, Nicolas
%A Deguillaume, Laurent
%A Attard, Eléonore
%A Sancelme, Martine
%A Delort, A.M.
%Z Poster
%< avec comité de lecture
%B IUGG 2011, Earth on the Edge Science for a Sustainable Planet
%C Melbourne, Australia
%8 2011-06-28
%D 2011Conference papers
%G English
%L hal-00609967
%U https://hal.archives-ouvertes.fr/hal-00609967
%~ INSU
%~ CNRS
%~ ICC
%~ UNIV-BPCLERMONT
%~ LAMP
%~ PRES_CLERMONT

%0 Conference Proceedings
%T Les microorganismes des nuages: Jouent-ils un rôle dans la chimie atmosphérique et les précipitations ?
%+ Synthèse et étude de systèmes à intêret biologique (SEESIB)
%+ Institute of Chemistry
%+ Laboratoire de météorologie physique (LaMP)
%A Delort, A.M.
%A Vaitilingom, Mickaël
%A Amato, Pierre
%A Attard, Eléonore
%A Vinatier, Virginie
%A Husarova, Slavomira
%A Sancelme, Martine
%A Gaiani, Nicolas
%A Traïkia, Mounir
%A Mourguy, Muriel
%A Matulova, Maria
%A Deguillaume, Laurent
%F Invité
%< avec comité de lecture
%B 8ème rencontre des microbiologistes du pôle clermontois
%C Clermont-Ferrand, France
%8 2011-04-12
%D 2011Conference papers
%G French
%L hal-00589270
%U https://hal.archives-ouvertes.fr/hal-00589270
%~ INSU
%~ CNRS
%~ ICC
%~ UNIV-BPCLERMONT
%~ LAMP
%~ PRES_CLERMONT

%0 Conference Paper
%F Oral 
%T Effets des conditions environnementales atmosphériques sur les capacités de glaciation et condensation de pseudomonas isolées d'eau de nuage et de la phyllosphère
%+ Synthèse et étude de systèmes à intêret biologique (SEESIB)
%+ Unité de Pathologie Végétale (PV)
%+ Laboratoire de météorologie physique (LaMP)
%+ Forschungszentrum Karlsruhe (FZK)
%A Attard, Eléonore
%A Vaitilingom, Mickaël
%A Glaux, Catherine
%A Amato, Pierre
%A Sancelme, Martine
%A Deguillaume, Laurent
%A Möhler, Ottmar
%A Delort, A.M.
%A Morris, Cindy E.
%Z Communication orale
%< avec comité de lecture
%B 8ème Congrès national de la SFM
%C Marseille, France
%8 2010-06-02
%D 2010
%Z Life Sciences [q-bio]Conference papers
%G English
%L hal-00493319
%U https://hal.archives-ouvertes.fr/hal-00493319
%~ INSU
%~ CNRS
%~ ICC
%~ UNIV-BPCLERMONT
%~ LAMP
%~ PRES_CLERMONT
%~ INRA
%~ INRAE
%~ AGROPOLIS
%~ AGREENIUM
%~ PV

%0 Conference Proceedings
%T Clouds as atmospheric oases for microbes
%+ Synthèse et étude de systèmes à intêret biologique (SEESIB)
%+ Laboratoire de météorologie physique (LaMP)
%A Amato, Pierre
%A Mourguy, Muriel
%A Vaitilingom, Mickaël
%A Attard, Eléonore
%A Vinatier, Virginie
%A Gaiani, Nicolas
%A Sancelme, Martine
%A Deguillaume, Laurent
%A Delort, A.M.
%F Invité
%< avec comité de lecture
%B 11th General Meeting of the American Society for Microbiology (ASM)
%C New-Orleans, United States
%8 2011-05-20
%D 2011Conference papers
%G English
%L hal-00597368
%U https://hal.archives-ouvertes.fr/hal-00597368
%~ INSU
%~ CNRS
%~ ICC
%~ UNIV-BPCLERMONT
%~ LAMP
%~ PRES_CLERMONT

%0 Conference Paper
%T Impacts of land-use on the soil C pools and denitrifying community: the case of grassland/arable crop conversion
%+ Laboratoire d'Ecologie Microbienne - UMR 5557 (LEM)
%+ Unité Expérimentale Fourrages et Environnement de Lusignan (UEFE)
%+ Unité d'Agronomie de Laon-Reims-Mons (AGRO-LRM)
%A Attard, Eléonore
%A Le Roux, Xavier
%A Guillaumaud, N.
%A Poly, F.
%A Chabbi, Abad
%A Charrier, Xavier, X.
%A Delfosse, Olivier
%A Recous, Sylvie
%< sans comité de lecture
%B International symposium on organic matter dynamics in agro-ecosystems
%C Poitiers, France
%S Organic matter dynamics in agro-ecosystems
%P 576 p.
%8 2007-07-16
%D 2007
%Z Life Sciences [q-bio]
%Z Environmental SciencesConference papers
%G English
%L hal-02753949
%U https://hal.inrae.fr/hal-02753949
%~ INRAE
%~ INRA
%~ SDE
%~ BIOENVIS
%~ GIP-BE
%~ UNIV-LYON1
%~ CNRS
%~ UNIV-LYON
%~ UDL
%~ ECOMIC
%~ AGREENIUM

%0 Conference Paper
%F Oral 
%T Etude des noyaux glaçogènes microbiens dans l'eau de nuage au Puy de Dôme (France)
%+ Institut de Chimie de Clermont-Ferrand (ICCF)
%+ Laboratoire de Météorologie Physique (LaMP)
%+ Karlsruhe Institute of Technology (KIT)
%+ Unité de Pathologie Végétale (PV)
%A Joly, Muriel
%A Amato, Pierre
%A Deguillaume, Laurent
%A Attard, E.
%A Hoose, C.
%A Monnier, M.
%A Morris, Cindy E.
%A Sancelme, Martine
%A Delort, Anne-Marie
%< sans comité de lecture
%B MicrobAERO 2013
%C La Bourboule, France
%S Colloque National "Microbiologie des aérosols" 7-9 octobre 2013 - La Bourboule
%8 2013-10-07
%D 2013
%Z Life Sciences [q-bio]/Microbiology and Parasitology
%Z Environmental Sciences/Global ChangesConference papers
%X Les particules glaçogènes biologiques, en particulier les microorganismes, ont été étudiés dans l'eau de nuage. Douze échantillons de nuage ont été collectés sur une période de 16 mois au sommet du puy de Dôme (1465 m ; France) à l'aide d'impacteurs à gouttelettes. Les échantillons ont été caractérisés biologiquement (cultures, dénombrement cellulaire...) et les paramètres physico-chimiques ont été déterminés (pH, contenu ionique, carbone organique total...). La concentration en noyaux glaçogènes a été mesurée par la méthode du gel des gouttes entre -3°C et -13°C. La concentration en particules glaçogènes dans cette gamme de température varie entre ˷1et ˷ 100 par mL d'eau de nuage. En ce qui concerne les particules glaçogènes biologiques, les valeurs mesurées sont supérieures de plusieurs ordres de grandeur à celle détectées dans les précipitations. A -12°C, au minimum 76% des noyaux glaçogènes sont d'origine biologique, i.e. ils sont inactivés par le chauffage à 95°c pendant 10 min, et pour des températures supérieures à 8°C, seul le matériel biologique a la capacité d'induire le gel. Par des méthodes de culture, 44 souches microbiennes apparentées au genre <em>Pseudomonas</em> ont été isolées d'eau de nuage. Parmi elles, 16% ont été identifiées comme glaçogènes à la température de -8°C. Elles appartiennent aux espèces <em>Pseudomonas syringae</em>, <em>Xanthomonas</em> sp. and <em>Pseudoxanthomonas</em> sp.. Deux des souches sont glaçogènes dès -2°C, les plaçant parmi les plus efficaces décrites à ce jour.
%G French
%L hal-02750330
%U https://hal.inrae.fr/hal-02750330
%~ INRAE
%~ INRA
%~ SDE
%~ ICC
%~ AGROPOLIS
%~ GIP-BE
%~ SIGMA-CLERMONT
%~ INC-CNRS
%~ LAMP
%~ CNRS
%~ PRES_CLERMONT
%~ INSU
%~ AGREENIUM
%~ PV

