%0 Book Section
%T Differential Object Marking in Basque Varieties
%+ Universidad del Pais Vasco / Euskal Herriko Unibertsitatea [Espagne] (UPV/EHU)
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Fernández, Beatriz
%A Rezac, Milan
%B Microparameters in the Grammar of Basque [Language Faculty and Beyond]
%E Fernández, Beatriz
%E Ortiz de Urbina, Jon
%I John Benjamins: Amsterdam
%P 93--138
%8 2016
%D 2016
%Z Humanities and Social Sciences/LinguisticsBook sections
%G English
%L hal-02551854
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02551854
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS

%0 Book Section
%T Tester les noms collectifs en Breton, enquête sur le nombre et la numérosité
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Jouitteau, Mélanie
%A Rezac, Milan
%@ 978-2-36758-074-6
%B Mélanges en l'honneur de Francis Favereau = Pennadoù dibabet en enor da Frañsez Favereau = A Festschrift in honour of Professor Favereau
%E ar C'hoadic, Ronan
%I Skol Vreizh
%P 331-364
%8 2018
%D 2018
%Z Humanities and Social Sciences/LinguisticsBook sections
%G French
%L hal-02551855
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02551855
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS

%0 Book Section
%T Gaps and Stopgaps in Basque Finite Verb Agreement
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Rezac, Milan
%B Microparameters in the Grammar of Basque [Language Faculty and Beyond]
%E Fernández, Beatriz
%E Ortiz de Urbina, Jon
%I John Benjamins
%C Amsterdam
%P 139--192
%8 2016
%D 2016
%Z Humanities and Social Sciences/LinguisticsBook sections
%G English
%L hal-02551856
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02551856
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS

%0 Book Section
%T On tough-movement
%+ Structures Formelles du Langage (SFL)
%A Rezac, Milan
%B Minimalist Essays
%E Cedric Boeckx
%I John Benjamins
%S Linguistik Aktuell
%P 288-325
%8 2006
%D 2006
%K tough movement
%Z Humanities and Social Sciences/LinguisticsBook sections
%G English
%L halshs-00605612
%U https://halshs.archives-ouvertes.fr/halshs-00605612
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-PARIS-LUMIERES

%0 Book Section
%T Datibo osagarri bitxiak eta Datiboaren Lekualdatzea: ari nai diyot eta kanta egin nazu bidegurutzean
%+ Structures Formelles du Langage (SFL)
%A Rezac, Milan
%A Fernandez, Beatriz
%B Euskara eta euskarak: Aldakortasun sintaktikoa aztergai
%E Beatriz Fernandez
%E Pablo Albizu
%E Ricardo Etxepare
%I UPV-EHU
%S Supplements of Anuario del Seminario de Filología Vasca "Julio de Urquijo"
%P 113-150
%8 2010
%D 2010
%K basque
%Z Humanities and Social Sciences/LinguisticsBook sections
%G Basque
%L halshs-00605617
%U https://halshs.archives-ouvertes.fr/halshs-00605617
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-PARIS-LUMIERES

%0 Book Section
%T The forms of dative displacement: From Basauri to Itelmen
%+ Structures Formelles du Langage (SFL)
%A Rezac, Milan
%B Gramatika jaietan: Patxi Goenagaren omenez
%E Xabier Artiagoitia, Joseba A. Lakarra
%I UPV-EHU
%P 709-724
%8 2008
%D 2008
%K dative
%Z Humanities and Social Sciences/LinguisticsBook sections
%G English
%L halshs-00605619
%U https://halshs.archives-ouvertes.fr/halshs-00605619
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-PARIS-LUMIERES

%0 Book Section
%T On the unifiability of repairs of the Person Case Constraint: French, Basque, Georgian, and Chinook
%+ Structures Formelles du Langage (SFL)
%A Rezac, Milan
%B Beñat Oihartzabali Gorazarre -- Festschrift for Beñat Oyharçabal
%E Ricardo Etxepare
%E Ricardo Gómez
%E Joseba A. Lakarra
%I UPV-EHU
%S Special issue of Anuario del Seminario de Filología Vasca "Julio de Urquijo" XLIII: 1-2
%P 769-790
%8 2010
%D 2010
%K person case constraint
%Z Humanities and Social Sciences/LinguisticsBook sections
%G English
%L halshs-00605618
%U https://halshs.archives-ouvertes.fr/halshs-00605618
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-PARIS-LUMIERES

%0 Book Section
%T The EPP In Breton: An uninterpretable categorial feature
%+ Structures Formelles du Langage (SFL)
%A Rezac, Milan
%B Triggers
%E Anne Breitbarth; Henk van Riemsdijk
%I de Gruyter
%S Studies in Generative Grammar
%P 451-492
%8 2004
%D 2004
%K Breton
%Z Humanities and Social Sciences/LinguisticsBook sections
%G English
%L halshs-00605614
%U https://halshs.archives-ouvertes.fr/halshs-00605614
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-PARIS-LUMIERES

%0 Journal Article
%T The French ethical dative: 13 syntactic tests
%+ Structures Formelles du Langage (SFL)
%+ Laboratoire de Linguistique de Nantes (LLING)
%A Rezac, Milan
%A Jouitteau, Mélanie
%< sans comité de lecture
%@ 2069-9239
%J Bucharest Working Papers in Linguistics
%I University of Bucharest
%V 9
%N 1
%P 97-108
%8 2008
%D 2008
%K dative
%Z Humanities and Social Sciences/LinguisticsJournal articles
%G English
%L halshs-00605620
%U https://halshs.archives-ouvertes.fr/halshs-00605620
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-NANTES
%~ UNIV-PARIS-LUMIERES

%0 Book Section
%T The syntax of clitic climbing in Czech
%+ Structures Formelles du Langage (SFL)
%A Rezac, Milan
%B Clitic and Affix Combinations
%E Lorie Heggie, Francisco Ordonez
%I John Benjamins
%S Linguistik Aktuell
%P 103-140
%8 2005
%D 2005
%K clitics
%Z Humanities and Social Sciences/LinguisticsBook sections
%G English
%L halshs-00605611
%U https://halshs.archives-ouvertes.fr/halshs-00605611
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-PARIS-LUMIERES

%0 Book Section
%T Person licensing and the derivation of PCC effects
%+ Structures Formelles du Langage (SFL)
%A Rezac, Milan
%A Susana, Béjar
%B Romance Linguistics: Theory and acquisition
%E Ana Teresa Pérez-Leroux, Yves Roberge
%I John Benjamins
%S Current Issues in Linguistic Theory
%P 49-62
%8 2003
%D 2003
%K clitics
%Z Humanities and Social Sciences/LinguisticsBook sections
%G English
%L halshs-00605615
%U https://halshs.archives-ouvertes.fr/halshs-00605615
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-PARIS-LUMIERES

%0 Book Section
%T Building and interpreting nonthematic A-positions : A-resumption in English and Breton
%+ Structures Formelles du Langage (SFL)
%A Rezac, Milan
%B Resumptive Pronouns at the Interfaces
%E Alain Rouveret
%I John Benjamins
%S Language Faculty and Beyond
%P 241-286
%8 2011
%D 2011
%K resumption
%Z Humanities and Social Sciences/LinguisticsBook sections
%G English
%L halshs-00605606
%U https://halshs.archives-ouvertes.fr/halshs-00605606
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-PARIS-LUMIERES

%0 Book Section
%T Phi-Agree and theta-related Case
%+ Structures Formelles du Langage (SFL)
%A Rezac, Milan
%B Phi-Features Across Modules and Interfaces
%E Daniel Harbour
%E David Adger
%E Susana Béjar
%I Oxford University Press
%S Oxford Studies in Theoretical Linguistics
%P 83-126
%8 2008
%D 2008
%K Agree
%K phi-features
%K Case
%Z Humanities and Social Sciences/LinguisticsBook sections
%G English
%L halshs-00605609
%U https://halshs.archives-ouvertes.fr/halshs-00605609
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-PARIS-LUMIERES

%0 Journal Article
%T Le verbe 'avoir' à travers les dialectes du breton
%+ Structures Formelles du Langage (SFL)
%+ Laboratoire de Linguistique de Nantes (LLING)
%A Rezac, Milan
%A Jouitteau, Mélanie
%< sans comité de lecture
%@ 1270-2412
%J La Bretagne Linguistique
%I Centre de Recherche Bretonne et Celtique
%V 14
%P 115-142
%8 2009
%D 2009
%K breton
%Z Humanities and Social Sciences/LinguisticsJournal articles
%G French
%L halshs-00605604
%U https://halshs.archives-ouvertes.fr/halshs-00605604
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-NANTES
%~ UNIV-PARIS-LUMIERES

%0 Book Section
%T Ineffability through modularity: Gaps in French clitic clusters
%+ Structures Formelles du Langage (SFL)
%A Rezac, Milan
%B Defective paradigms: Missing forms and what they tell us
%E Matthew Baerman
%E Greville G. Corbett
%E and Dunstan Brown
%I Oxford University Press
%S Proceedings of the British Academy
%P 151-180
%8 2010
%D 2010
%K clitics
%Z Humanities and Social Sciences/LinguisticsBook sections
%G French
%L halshs-00605608
%U https://halshs.archives-ouvertes.fr/halshs-00605608
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-PARIS-LUMIERES

%0 Book
%T Phi-features and the modular architecture of language
%+ Structures Formelles du Langage (SFL)
%A Rezac, Milan
%Y Springer
%I Springer
%S Studies in Natural Language and Linguistic Theory
%P 326
%8 2011
%D 2011
%R 10.1007/978-90-481-9698-2
%K Modularity
%K Morphology
%K Person hierachies
%K Phi-features
%K Repair strategies
%K Case
%K Cross-linguistic
%K Minimalist program
%K Modular architecture
%Z Humanities and Social Sciences/LinguisticsBooks
%X This monograph investigates the modular architecture of language through the nature of "uninterpretable" phi-features: person, number, gender, and Case. It provides new tools and evidence for the modular architecture of the human language faculty, a foundational topic of linguistic research. At the same time it develops a new theory for one of the core issues posed by the Minimalist Program: the relationship of syntax to its interfaces and the nature of uninterpretable features. The work sets out to establish a new cross-linguistic phenomenon to study the foregoing, person-governed last-resort repairs, which provides new insights into the nature of ergative/accusative Case and of Case licensing itself. This is the first monograph that explicitly addresses the syntactic vs. morphological status of uninterpretable phi-features and their relationship to interface systems in a similar way, drawing on person-based interactions among arguments as key data-base.
%G English
%L halshs-00605587
%U https://halshs.archives-ouvertes.fr/halshs-00605587
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-PARIS-LUMIERES

