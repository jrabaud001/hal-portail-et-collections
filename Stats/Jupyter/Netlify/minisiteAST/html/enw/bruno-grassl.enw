%0 Book
%T Microwave-assisted controlled radical polymerization
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Reynaud, Stéphanie
%A Grassl, B.
%Z DOI: 10.1007/12\₂014\₃02
%@ 00653195 (ISSN)
%I Springer New York LLC
%S Advances in Polymer Science
%V 274
%8 2016
%D 2016
%K Controlled radical polymerization
%K Irradiation mode
%K Microwaveassisted synthesis
%K Molecular weight control and distribution
%K Temperaturek control
%Z Chemical SciencesBooks
%X Use of microwave energy to heat and drive chemical reactions has been an increasingly popular theme in the scientific community. Historically, since the 1950s microwave energy has become an essential part of many technical applications in chemical and related industries, particularly in organic chemistry. However, microwave heating was not introduced to polymer chemistry until the mid-1980s. Regardless of the exact nature of the still-debated microwave effects, microwave synthesis has now truly matured and has moved from a laboratory curiosity to an established technique in both academia and industry. Among the polymerization techniques, controlled radical polymerizations (CRPs) are known to produce high-value products with controlled architecture and low dispersity. Original physicochemical properties and promising applications are reachable. This chapter provides, in a concise form, a current picture of three types of microwave-assisted controlled radical polymerization (RAFT, NMP, ATRP) with regard to the irradiation mode and the activation energy values. © Springer International Publishing Switzerland 2014.
%G English
%L hal-01495774
%U https://hal.archives-ouvertes.fr/hal-01495774
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ IPREM-PCM

%0 Patent
%T Method for determining the weight-average molecular weight of a water-soluble high molecular weight polymer.
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Jouenne, Stephane
%A Loriau, Matthieu
%A Grassl, Bruno
%A Andreu, Nathalie.
%N WO 2017042603 A1 2017031
%C Unknown Region
%P 20pp.
%8 2017-03-16
%D 2017
%K av mol wt water soluble polymer size exclusion chromatog
%K multi angle light scattering differential refractive index detector polymer
%Z Chemical SciencesPatents
%X The invention concerns a method for detg. the wt.- av. mol. wt. of a water-sol. high mol. wt. polymer, said method comprising the following steps: (a) Providing a Size Exclusion Chromatog. column connected to a Multi Angle Light Scattering and Differential Refractive Index detectors; (b) Providing water as mobile phase of said Size Exclusion Chromatog. column; (c) Injecting said polymer into said mobile phase of said Size Exclusion Chromatog. column; (d) Measuring the wt.- av. mol. wt. of said polymer with the Multi Angle Laser Light Scattering and Differential Refractive Index detectors; wherein the flow rate of said mobile phase is lower than 0.20 mL/min, in particular is from 0.05 mL/min to 0.20 mL/min, more particularly from 0.10 mL/min to 0.15 mL/min. [on SciFinder(R)]
%G English
%L hal-01617273
%U https://hal.archives-ouvertes.fr/hal-01617273
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ IPREM-PCM
%~ TESTUPPA

%0 Patent
%T Gel process for polymerization via ultrasound initiation.
%+ Institut Pluridisciplinaire de Recherche sur l'Environnement et les Matériaux (IPREM)
%+ Physico-chimie des polymères (PCP)
%A Grassl, Bruno
%A Rigolini, Julien
%N WO 2012042157 A1 20120405
%C Unknown Region
%P 26pp.; Chemical Indexing Equivalent to 156:421970 (FR)
%8 2012-05-04
%D 2012
%K acrylamide ultrasound assisted radical polymn
%K azo initiator polymn acrylamide
%Z Chemical SciencesPatents
%X The present application relates to a process for the ultrasound-initiated radical (co)polymn., in the aq. gel phase, of one or more water-sol. monomers in the presence of an initiator. Thus, initiating the polymn. of a gel compn. contg. acrylamide and Vazo 56 (azo initiator) by ultrasound gave a polymer. [on SciFinder(R)]
%G English
%L hal-01617285
%U https://hal.archives-ouvertes.fr/hal-01617285
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA

