%0 Book Section
%T Systèmes diphasiques et contrôle thermique - Thermosiphons et caloducs
%+ Centre de Thermique de Lyon (CETHIL)
%A Bonjour, Jocelyn
%A Lefèvre, Frédéric
%A Sartre, Valerie
%A Bertin, Yves
%A Romestan, Cyril
%A Ayel, Vincent
%A Platel, Vincent
%B Les Techniques de l'Ingénieur
%S Traité de Génie Energétique
%P Dossier [BE9545]
%8 2011
%D 2011
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Thermics [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Thermics [physics.class-ph]Book sections
%G French
%L hal-01025932
%U https://hal.archives-ouvertes.fr/hal-01025932
%~ CNRS
%~ CETHIL
%~ INSA-LYON
%~ UNIV-LYON1
%~ INSA-GROUPE
%~ UDL
%~ UNIV-LYON

%0 Book Section
%T Systèmes diphasiques de contrôle thermique. Thermosiphons et caloducs.
%+ Centre de Thermique de Lyon (CETHIL)
%+ Institut Pprime (PPRIME)
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Bonjour, J.
%A Lefèvre, F.
%A Sartre, V.
%A Bertin, Y.
%A Romestant, C.
%A Ayel, V.
%A Platel, Vincent
%B Les Techniques de l’Ingénieur - Traité de Génie Energétique
%S Les Techniques de l’Ingénieur
%V BE9545
%P 26 pages
%8 2010-10
%D 2010
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Thermics [physics.class-ph]Book sections
%X Various systems of heat conduction are available, the use of metals such as copper and aluminum. However, heat pipes, the functioning of which is based upon the principle of thermal transfer through a phase transition of a fluid (latent heat), allows for obtaining a significant yield in the transport of heat fluxes. The two mostly used main types of heat pipes are capillary heat pipes and two-phase thermosyphons. Although it was discovered in the 30s, the principle of the heat pipe was not adopted until the last few decades, notably in the airspace, rail and power electronics sectors. This article presents this state-of-the-art technology, its functioning principles, dimensioning methods and several application cases.
%X Pour la conduction de chaleur, différents systèmes sont possibles. On peut citer par exemple l'utilisation de métaux tels que le cuivre et l'aluminium. Toutefois, les caloducs, dont le système de fonctionnement est basé sur le principe du transfert thermique par transition de phase d'un fluide (chaleur latente), permet d'avoir un rendement particulièrement intéressant dans le transport des flux thermiques. Les deux principaux types de caloducs généralement utilisés sont les caloducs capillaires et les thermosiphons diphasiques. Bien que découvert dans les années 30, le principe du caloduc n'a réellement été adopté que depuis quelques dizaines d'années, notamment dans les secteurs de l'aérospatial, du ferroviaire ou de l'électronique de puissance.
%G French
%L hal-01287771
%U https://hal.archives-ouvertes.fr/hal-01287771
%~ CNRS
%~ UNIV-PAU
%~ UNIV-POITIERS
%~ CETHIL
%~ INSA-LYON
%~ UNIV-LYON1
%~ INSA-GROUPE
%~ LATEP
%~ PPRIME
%~ UDL
%~ UNIV-LYON

%0 Book Section
%T Systèmes diphasiques de contrôle thermique - Microcaloducs et caloducs oscillants
%+ Centre de Thermique de Lyon (CETHIL)
%A Bonjour, Jocelyn
%A Lefèvre, Frédéric
%A Sartre, Valérie
%A Bertin, Yves
%A Romestan, Cyril
%A Ayel, Vincent
%A Platel, Vincent
%B Les Techniques de l'Ingénieur
%S Traité de Génie Energétique
%P Dossier [BE9547]
%8 2011
%D 2011
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Thermics [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Thermics [physics.class-ph]Book sections
%G French
%L hal-00996043
%U https://hal.archives-ouvertes.fr/hal-00996043
%~ CNRS
%~ CETHIL
%~ INSA-LYON
%~ UNIV-LYON1
%~ INSA-GROUPE
%~ UDL
%~ UNIV-LYON

%0 Book Section
%T Systèmes diphasiques de contrôle thermique - Boucles diphasiques capillaires et gravitaires
%+ Centre de Thermique de Lyon (CETHIL)
%A Bonjour, Jocelyn
%A Lefèvre, Frédéric
%A Sartre, Valerie
%A Bertin, Yves
%A Romestan, Cyril
%A Ayel, Vincent
%A Platel, Vincent
%B Les Techniques de l'Ingénieur
%S Traité de Génie Energétique
%P Dossier [BE9546]
%8 2011
%D 2011
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Thermics [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Thermics [physics.class-ph]Book sections
%G English
%L hal-01025929
%U https://hal.archives-ouvertes.fr/hal-01025929
%~ CNRS
%~ CETHIL
%~ INSA-LYON
%~ UNIV-LYON1
%~ INSA-GROUPE
%~ UDL
%~ UNIV-LYON

%0 Unpublished work
%T Améliorer la qualité de l’air intérieur dans un bâtiment tertiaire – Impact énergétique d’un prototype d’épuration de l’air (techniques de filtration, d’adsorption et de photocatalyse)
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ Département Systèmes énergétiques et environnement (IMT Atlantique - DSEE)
%+ Traitement Eau Air Métrologie (GEPEA-TEAM)
%A Platel, Vincent
%A Tazzit, A.
%A Hort, Cecile
%A Moynault, L.
%A Hallemans, E.
%A Coulbaux, G.
%A Binet, M.
%A Andres, Y.
%A V., HEQUET
%9 Rapport ADEME
%P Rapport ADEME n°5, 20 pages
%8 2018-04
%D 2018
%Z Engineering Sciences [physics]/Chemical and Process EngineeringOther publications
%G English
%L hal-02162950
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02162950
%~ UNIV-PAU
%~ LATEP
%~ GEPEA-TEAM
%~ GEPEA
%~ IMT-ATLANTIQUE
%~ IMTA_DSEE
%~ GEPEA-TEAM-IMTA
%~ UNAM
%~ GEPEA-IMTA
%~ UNIV-NANTES
%~ CNRS
%~ INSTITUTS-TELECOM
%~ INSTITUT-TELECOM

%0 Unpublished work
%T ADEME, 2018 Améliorer la qualité de l’air intérieur dans un bâtiment tertiaire – De la conception à l’intégration d’un prototype d’épuration d’air (techniques de filtration, d’adsorption et de photocatalyse) - CUBAIR : Confort des usagers des bâtiments tertiaires par l'usage de techniques de traitement de l'air. Rapport n°3, 52 pages.
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Hallemans, E.
%A Coulbaux, G.
%A Binet, M.
%A Hort, Cecile
%A Platel, Vincent
%A Moynault, L.
%A Andres, Y.
%A Hequet, Valérie
%9 Rapport
%8 2018
%D 2018
%Z Engineering Sciences [physics]/Chemical and Process EngineeringOther publications
%G English
%L hal-02162946
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02162946
%~ UNIV-PAU
%~ LATEP
%~ GEPEA-TEAM
%~ GEPEA
%~ TESTUPPA

%0 Conference Paper
%F Poster 
%T CUBAIR, Confort des Usagers des Bâtiments tertiaires par l’usage de techniques de traitement de l’AIR
%+ Centre d'Etudes et d'Expertise sur les Risques, l'Environnement, la Mobilité et l'Aménagement - Direction Ile-de-France (Cerema Direction Ile-de-France)
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ Traitement Eau Air Métrologie (GEPEA-TEAM)
%+ Département Systèmes énergétiques et environnement (IMT Atlantique - DSEE)
%+ Mines Nantes (Mines Nantes)
%+ Centre d'Etudes et d'Expertise sur les Risques, l'Environnement, la Mobilité et l'Aménagement (Cerema)
%A Petit, Jean-François
%A Coulbaux, Guillaume
%A GALLIS, Didier
%A Bruno, Paolo
%A Binet, Maureen
%A Moynault, Laurent
%A Platel, Vincent
%A Hort, Cecile
%A Hequet, Valérie
%A Andres, Yves
%A Hallemans, Elise
%< avec comité de lecture
%B Colloque Adebiotech
%C paris, France
%8 2017-06-27
%D 2017
%Z Environmental Sciences/Environmental Engineering
%Z Engineering Sciences [physics]/Chemical and Process EngineeringPoster communications
%G French
%L hal-02363966
%U https://hal.archives-ouvertes.fr/hal-02363966
%~ UNIV-PAU
%~ SDE
%~ UNAM
%~ CNRS
%~ GIP-BE
%~ CEREMA
%~ IMT-ATLANTIQUE
%~ GEPEA-TEAM-IMTA
%~ UNIV-NANTES
%~ IMTA_DSEE
%~ LATEP
%~ GEPEA
%~ GEPEA-TEAM
%~ GEPEA-IMTA
%~ INSTITUTS-TELECOM
%~ INSTITUT-TELECOM
%~ POLITIQUES-PUBLIQUES

%0 Unpublished work
%T Syntivia/UPPA Impact de la pollution de l'air intérieur sur des modèles biologiques de peau. 9 pages
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Hort, Cecile
%A M., Norlund
%A Platel, Vincent
%9 rapport
%8 2017
%D 2017
%Z Engineering Sciences [physics]/Chemical and Process EngineeringOther publications
%G English
%L hal-02162948
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02162948
%~ UNIV-PAU
%~ LATEP
%~ TESTUPPA

%0 Unpublished work
%T ADEME, 2018 Améliorer la qualité de l’air intérieur dans un bâtiment tertiaire -évaluation sur bancs expérimentaux de laboratoire (techniques d’adsorption et de photocatalyse)- CUBAIR : Confort des usagers des bâtiments tertiaires par l’usage de techniques de traitement de l’air. Rapport n°2, 42 pages.
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Hequet, Valérie
%A Andres, Y.
%A Hort, Cecile
%A Platel, Vincent
%A Moynault, L.
%A Bruno, P.
%A Binet, M.
%A Petit, J.F.
%A Coulbaux, G.
%A Hallemans, E.
%9 Rapport
%8 2018
%D 2018
%Z Engineering Sciences [physics]/Chemical and Process EngineeringOther publications
%G English
%L hal-02162944
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02162944
%~ UNIV-PAU
%~ LATEP
%~ GEPEA-TEAM
%~ GEPEA
%~ TESTUPPA

%0 Unpublished work
%T ADEME, 2018. Améliorer la qualité de l’air intérieur dans un bâtiment tertiaire – Évaluation in situ des performances d’un prototype d’épuration de l’air (techniques de filtration, d’adsorption et de photocatalyse) – CUBAIR : Confort des usagers des bâtiments tertiaires par l'usage de techniques de traitement de l'air. Rapport n°4, 46 pages.
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Hallemans, E.
%A Coulbaux, G.
%A Binet, M.
%A Hort, Cecile
%A Platel, Vincent
%A Moynault, L.
%A Andres, Y.
%A Hequet, Valérie
%9 Rapport
%8 2018
%D 2018
%Z Engineering Sciences [physics]/Chemical and Process EngineeringOther publications
%G English
%L hal-02162945
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02162945
%~ UNIV-PAU
%~ LATEP
%~ GEPEA-TEAM
%~ GEPEA
%~ TESTUPPA

%0 Unpublished work
%T Projet HyPAIR - Adsorption du dioxyde de soufre sur des filtres de charbon actif Rapport n°2, 14 pages
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Hort, Cecile
%A Luengas, A.
%A Platel, Vincent
%9 Projet Hypair
%8 2016
%D 2016
%Z Engineering Sciences [physics]/Chemical and Process EngineeringOther publications
%G English
%L hal-02162949
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02162949
%~ UNIV-PAU
%~ LATEP
%~ TESTUPPA

%0 Unpublished work
%T Projet HyPAIR - Adsorption du dioxyde de soufre sur des filtres de charbon actif Rapport n°1, 35 pages
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Hort, Cecile
%A Bachelard, Julie
%A Platel, Vincent
%9 projet Hypair
%8 2015
%D 2015
%Z Engineering Sciences [physics]/Chemical and Process EngineeringOther publications
%G English
%L hal-02162947
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02162947
%~ UNIV-PAU
%~ LATEP
%~ TESTUPPA

%0 Conference Paper
%F Poster 
%T Impact énergétique d'un couplage de procédés - adsorption-photocatalyse -pour traiter l'air intérieur
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ Traitement Eau Air Métrologie (GEPEA-TEAM)
%+ Département Systèmes énergétiques et environnement (IMT Atlantique - DSEE)
%+ Centre d'Etudes et d'Expertise sur les Risques, l'Environnement, la Mobilité et l'Aménagement (Cerema)
%A Tazzit, Amine
%A Platel, Vincent
%A Hort, Cecile
%A Moynault, Laurent
%A Hequet, Valérie
%A Andres, Yves
%A Hallemans, Élise
%A Coulbaux, Guillaume
%A Bruno, Paolo
%A Binet, Maureen
%< avec comité de lecture
%B 26e Congres Francais de Thermique
%C pau, France
%8 2018-05-29
%D 2018
%Z Environmental Sciences/Environmental Engineering
%Z Engineering Sciences [physics]/Chemical and Process EngineeringPoster communications
%X Dans le cadre de l'appel à projet "Vers des bâtiments responsables à l'horizon 2020" réalisé par l'ADEME, le projet CUBAIR a été lancé en 2014. Un des objectifs de ce projet concerne l'impact énergétique d'un prototype installé dans un bâtiment à Paris (le CVRH). Un bilan thermique sur la salle concernée a permis de caractériser le comportement thermique de celle-ci, en l'absence du système de dépollution dans un premier temps, puis en sa présence. L'impact énergétique est principalement déterminé par un calcul de puissance de chauffage et/ou de climatisation.
%G French
%L hal-02363747
%U https://hal.archives-ouvertes.fr/hal-02363747
%~ UNIV-PAU
%~ SDE
%~ UNAM
%~ GIP-BE
%~ CEREMA
%~ IMT-ATLANTIQUE
%~ GEPEA-TEAM-IMTA
%~ UNIV-NANTES
%~ IMTA_DSEE
%~ LATEP
%~ GEPEA
%~ GEPEA-TEAM
%~ CNRS
%~ GEPEA-IMTA
%~ INSTITUTS-TELECOM
%~ INSTITUT-TELECOM
%~ POLITIQUES-PUBLIQUES

%0 Book Section
%T Systèmes diphasiques de contrôle thermique - Boucles capillaires et gravitaires
%+ Centre de Thermique de Lyon (CETHIL)
%+ Institut Pprime (PPRIME)
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Bonjour, J.
%A Lefèvre, F.
%A Sartre, V.
%A Bertin, Y.
%A Romestant, C.
%A Ayel, V.
%A Platel, Vincent
%B Les Techniques de l’Ingénieur - Traité de Génie Energétique
%V BE9546
%P 22 pages
%8 2011-01-10
%D 2011
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Thermics [physics.class-ph]Book sections
%X The aim of this article is to present a synthesis on the state-of-the art technology of two-phase fluid loops used as thermal control devices i.e. loop heat pipes and capillary-pumped loops. These loops differ from heat pipes in that the vapour and fluid circulate inside separate pipes. The article presents elements of dimensioning for fluid loops and focuses on the description of two-phase thermohydraulic phenomena occurring in the evaporator, condenser and pipes. It then provides a detailed description of the various components of the capillary loops in order to highlight the dimensioning issues of such systems as well as their behavior during transitory start-up phases. Finally it describes a certain number of practical applications, often related to the space industry sector.
%X Cet article vise à synthétiser l'état de l'art relatif à deux types de boucles à fluide diphasique utilisées comme dispositifs de contrôle thermique, à savoir les boucles diphasiques gravitaires et les boucles diphasiques à pompage thermocapillaire. Ces boucles se distinguent des caloducs par le fait que la vapeur et le liquide circulent dans des canalisations distinctes. Des éléments de dimensionnement des boucles gravitaires sont présentés : il s'agit principalement de décrire les phénomènes thermohydrauliques diphasiques se produisant à l'évaporateur, au condenseur et dans les canalisations. Une description détaillée des différents organes constituant les boucles capillaires est ensuite proposée. Elle éclaire les difficultés de dimensionnement de ces systèmes, ainsi que leur comportement lors de phases transitoires de démarrage.
%G French
%L hal-01287827
%U https://hal.archives-ouvertes.fr/hal-01287827
%~ CNRS
%~ UNIV-PAU
%~ UNIV-POITIERS
%~ CETHIL
%~ INSA-LYON
%~ UNIV-LYON1
%~ INSA-GROUPE
%~ LATEP
%~ PPRIME
%~ UDL
%~ UNIV-LYON

