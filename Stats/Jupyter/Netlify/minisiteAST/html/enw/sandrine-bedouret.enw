%0 Book Section
%T « “Les nuages... Les nuages qui passent... Là-bas... Là-bas... Les merveilleux nuages !”
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Bédouret-Larraburu, Sandrine
%@ 978-2-406-08963-6.p.0159
%B L’Utopie de l’art. Mélanges offerts à GérardDessons
%E Bernadet Arnaud
%E Kachler Olivier
%E Laplantine Chloé
%8 2020
%D 2020
%K Baudelaire
%K poème en prose
%K Spleen de Paris
%K rêverie
%K je-ne-saisquoi
%Z Humanities and Social Sciences/Linguistics
%Z Computer Science [cs]/General Literature [cs.GL]Book sections
%X Cet article établit que le nuage est constitutif d’une poétiquebaudelairienne. En effet, dans le Spleen de Paris, le nuage s’associe à la rêverie.Il relève de l’indéfini, et suggère un je ne sais quoi. Le poème en prose peut sepenser comme un nuage, à la fois insignifiant et menaçant.
%G French
%L hal-02919638
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02919638
%~ UNIV-PAU
%~ AO-LINGUISTIQUE
%~ ALTER
%~ SHS

%0 Edited Book
%T Emile Benveniste : vers une poétique générale
%+ HTL - Histoire des Théories Linguistiques - UMR 7597 (HTL)
%A Laplantine, Chloé
%A Bédouret-Larraburu, Sandrine
%@ 2-35311-059-2
%C France
%I PUPPA
%C Pau
%S Emile Benveniste : vers une poétique générale
%8 2015
%D 2015
%K Linguistique générale
%K Littérature
%K Benveniste
%Z Humanities and Social Sciences/Literature
%Z Humanities and Social Sciences/LinguisticsDirections of work or proceedings
%X Émile Benveniste (1902-1976), grand linguiste français du xxe siècle, spécialiste avant tout du domaine indo-européen et auteur d’une linguistique générale qui reste un fondement pour la réflexion sur le langage et les langues, s’est intéressé toute sa vie au langage poétique. Cet intérêt apparaît ponctuellement dans ses travaux sur le langage, les langues et cultures ou encore dans quelques textes plus « littéraires ». Ses travaux de linguistique générale ouvrent déjà, en eux-mêmes, sur le problème du langage poétique, et permettent de faire du poème un champ de réflexion possible pour le linguiste. On sait maintenant, grâce à la publication de ses manuscrits sur « la langue de Baudelaire », qu’il avait engagé l’écriture d’un important travail critique sur cette question : « La théorie de la langue poétique est encore à venir . Le présent essai a pour but d’en hâter un peu l’avènement ».Ce volume cherche à mettre en regard les travaux inachevés, parvenus jusqu’à nous sous forme de notes manuscrites, avec les Problèmes de linguistique générale. Il constitue les actes du colloque « Émile Benveniste et la littérature » qui s’est tenu en avril 2013 à Bayonne. Les contributions rassemblées ici questionnent de manières diverses le rapport d’un linguiste avec le langage poétique, ses méthodes d’investigation, ses préoccupations terminologiques, et poursuivent en même temps, avec lui, la recherche actuelle d’une poétique.
%G French
%L halshs-01414621
%U https://halshs.archives-ouvertes.fr/halshs-01414621
%~ SHS
%~ UNIV-PARIS3
%~ CNRS
%~ HTL
%~ AO-LINGUISTIQUE
%~ USPC
%~ UNIV-PARIS7
%~ UNIV-PARIS
%~ UP-SOCIETES-HUMANITES

%0 Book Section
%T "Vers une poétique générale"
%+ HTL - Histoire des Théories Linguistiques - UMR 7597 (HTL)
%A Laplantine, Chloé
%A Bédouret-Larraburu, Sandrine
%B Emile Benveniste : vers une poétique générale
%E PUPPA
%C Pau
%P p. 13-24
%8 2015
%D 2015
%K Poétique
%K Benveniste
%Z Humanities and Social Sciences/Literature
%Z Humanities and Social Sciences/LinguisticsBook sections
%G French
%L hal-01424893
%U https://hal.archives-ouvertes.fr/hal-01424893
%~ UNIV-PARIS3
%~ CNRS
%~ HTL
%~ AO-LINGUISTIQUE
%~ SHS
%~ USPC
%~ UNIV-PARIS7
%~ UNIV-PARIS
%~ UP-SOCIETES-HUMANITES

%0 Unpublished work
%T Compte-rendu de ANCET Jacques : Amnésie du présent (Publie.net)
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Bédouret-Larraburu, Sandrine
%9 n° 1092 - Jean Racine
%@ 0014-2751
%J Europe. Revue littéraire mensuelle
%I Europe. Revue
%P 329-331
%8 2020
%D 2020
%Z Humanities and Social Sciences/LiteratureOther publications
%G French
%L hal-02920271
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02920271
%~ UNIV-PAU
%~ ALTER
%~ SHS

%0 Unpublished work
%T Compte-rendu de Casanova Jean-Yves : Robert Marteau. mesure du ciel et de la terre (Léo Scheer)
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Bédouret-Larraburu, Sandrine
%9 n° 1092 - Jean Racine
%@ 0014-2751
%J Europe. Revue littéraire mensuelle
%I Europe. Revue
%P 334-335
%8 2020
%D 2020
%Z Humanities and Social Sciences/LiteratureOther publications
%G French
%L hal-02920277
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02920277
%~ UNIV-PAU
%~ ALTER
%~ SHS

%0 Book Section
%T le Pont Mirabeau : un poème générateur de patrimonialisation
%+ Arts / Langages : Transitions et Relations (ALTER)
%+ Géographie de l'environnement (GEODE)
%A Bédouret-Larraburu, Sandrine
%A Bédouret, David
%@ 978-2-304-04-844-5
%B Lire les villes
%E Maria de Jesus Cabral
%E Maria Herminia Laurel
%E Franc Schuerewegen
%I Editions Le Manuscrit
%C Paris
%8 2020
%D 2020
%Z Humanities and Social Sciences/Linguistics
%Z Computer Science [cs]/General Literature [cs.GL]
%Z Humanities and Social Sciences
%Z Humanities and Social Sciences/GeographyBook sections
%G French
%Z APEF
%L hal-02920309
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02920309
%~ UNIV-PAU
%~ AO-GEOGRAPHIE
%~ AO-LINGUISTIQUE
%~ GEODE
%~ UNIV-TLSE2
%~ GIP-BE
%~ CNRS
%~ ALTER
%~ SHS

%0 Edited Book
%T En quoi Saussure peut-il nous aider à penser la littérature ?
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Bédouret-Larraburu, Sandrine
%A Prignitz, Gisèle
%@ 978-2353110308
%I Presses universitaires de Pau et des Pays de l'Adour
%8 2012
%D 2012
%Z Humanities and Social Sciences/Linguistics
%Z Computer Science [cs]/General Literature [cs.GL]Directions of work or proceedings
%G French
%L hal-02015756
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02015756
%~ UNIV-PAU
%~ AO-LINGUISTIQUE
%~ SHS
%~ ALTER
%~ TESTUPPA

%0 Edited Book
%T Robert Marteau, Arpenteur en vers et en proses
%+ Patrimoine, Littérature, Histoire (PLH)
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Casanova, Jean-Yves
%A Bédouret-Larraburu, Sandrine
%@ 978-2-7535-4214-3
%@ 0398-9992
%J La Licorne - Revue de langue et de littérature française
%C Rennes, France
%I Presses Universitaires de Rennes
%C Poitiers – Rennes
%N n°117
%P 182
%8 2015
%D 2015
%Z Humanities and Social Sciences/LiteratureDirections of work or proceedings
%X Robert Marteau est l’auteur d’une œuvre littéraire, poésie et proses, variée et foisonnante, expression d’une voix singulière et ample de ce début de siècle. Il est traducteur, de Shakespeare et de Chaucer notamment, il a aussi longtemps travaillé dans une galerie d’art et son intérêt pour la peinture ne s’est jamais démenti. Dans ce volume, un cahier central comprend des inédits du poète, quelques reproductions de papiers et tableaux de Jean- Louis Fauthoux, et des poèmes-hommages.
%G French
%L hal-02288947
%U https://hal.archives-ouvertes.fr/hal-02288947
%~ UNIV-PAU
%~ PLH
%~ SMS
%~ UNIV-TLSE2
%~ SHS
%~ ALTER
%~ TESTUPPA

%0 Conference Proceedings
%T Benveniste : vers une poétique générale
%+ Arts / Langages : Transitions et Relations (ALTER)
%+ HTL - Histoire des Théories Linguistiques - UMR 7597 (HTL)
%A Bédouret-Larraburu, Sandrine
%A Laplantine, Chloé
%@ 978-2-35311-059-9
%B Émile Benveniste et la littérature
%C Bayonne, France
%I PUPPA
%C Pau
%3 Linguiste et littérature
%8 2015
%D 2015
%Z Humanities and Social Sciences/Literature
%Z Humanities and Social Sciences/LinguisticsDirections of work or proceedings
%X Le présent volume constitue les actes du colloque « Émile Benveniste et la littérature » qui s'est tenu les 2 et 3 avril 2013 à Bayonne. La collection « Linguiste et littérature » engage ainsi, après un premier volume consacré à Ferdinand de Saussure (En quoi Saussure peut-il nous aider à penser la littérature ?), un deuxième temps de son examen avec Émile Benveniste. Benveniste partage avec Saussure un champ d'étude privilégié, la grammaire comparée des langues indo-européennes, et une méthode d'analyse dont il hérite. Son ouvrage Origines de la formation des noms en indo-européen publié en 1935, s'inscrit ainsi directement dans la suite du Mémoire sur le système primitif des voyelles publié par Saussure en 1879. La recherche et l'enseignement de la grammaire comparée tient quantitati-vement chez les deux linguistes la plus grande place : Benveniste publie environ 300 articles qui, très majoritairement, appartiennent à ce domaine. En même temps, ils ont tous les deux développé une linguistique générale, qu'ils ont enseignée (ce qui n'allait pas de soi), et qui est devenue dans les deux cas un fondement pour la réflexion sur le langage et les langues. De ce point de vue, Benveniste est encore une fois le continuateur (revendiqué 1) de Saussure : la conception de la langue comme système de valeurs se poursuit chez lui dans sa théorie de l'énonciation, et le projet d'« une science qui étudie la vie des signes au sein de la vie sociale » deviendra la « sémiologie de la langue » et la « culturologie ». Par ailleurs, ces deux linguistes ont en commun leur intérêt pour le « langage poétique ». La littérature fait partie de la matière sur laquelle ils travaillent en tant que comparatistes, et en même temps elle a constitué une recherche spécifique chez chacun d'eux : le travail infini sur les paragrammes pour Saussure et celui sur « la langue de Baudelaire » pour Benveniste. Ces recherches ont ensemble la particularité 1-Voir « Saussure après un demi-siècle », Problèmes de linguistique générale 1, Gallimard, 1966, p. 32-45. Ensuite abrégé PLG1.
%G French
%L hal-02356338
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02356338
%~ UNIV-PAU
%~ AO-LINGUISTIQUE
%~ CNRS
%~ SHS
%~ HTL
%~ ALTER
%~ UNIV-PARIS3
%~ UNIV-PARIS7
%~ USPC
%~ UNIV-PARIS
%~ UP-SOCIETES-HUMANITES

%0 Book Section
%T Du vers au poème : transformer l’enseignement de la poésie au lycée
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Bédouret-Larraburu, Sandrine
%B Où en est la didactique du français ? Le point de vue de la poétique et de la théorie du langage avec Henri Meschonnic
%E Serge, Martin
%I Atelier du Grand Tétras
%P 73-84
%8 2011
%D 2011
%Z Humanities and Social Sciences/LiteratureBook sections
%G French
%L hal-02180411
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02180411
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Book Section
%T Au-delà du poème et du récit : le genre Hocquard
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Bédouret-Larraburu, Sandrine
%@ 978-2-35311-100-8
%B Labilité des genres : Le désir du hors genre
%E Buisson, Françoise
%E Schmitt, Arnaud
%I PUPPA
%S Espaces, Frontières, Métissages
%V 10
%P 65-76
%8 2018
%D 2018
%Z Humanities and Social Sciences/LiteratureBook sections
%G French
%L hal-02495661
%U https://hal.archives-ouvertes.fr/hal-02495661
%~ UNIV-PAU
%~ SHS
%~ ALTER
%~ TESTUPPA

%0 Book Section
%T Les figures de répétition : à l'origine du nombre oratoire français
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Bédouret-Larraburu, Sandrine
%@ 978-2-915797-52-7
%B Re, Répéter, Répétition,
%E Rutigliano
%E Sandrine and Piffaré
%E Alexandra-Flora
%I Université de Savoie
%S Ecole doctorale
%N 5
%P 23-32
%8 2010
%D 2010
%Z Humanities and Social Sciences/LiteratureBook sections
%G French
%L hal-02180410
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02180410
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Unpublished work
%T Chronique « linguistique » Emile Benveniste pour vivre langage
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Bédouret-Larraburu, Sandrine
%< avec comité de lecture
%@ 0184-7732
%J Le Français Aujourd'hui
%3 Continuités et ruptures dans l’enseignement de la littérature
%I Armand Colin / Dunod ; Association française des professeurs de français ; Association française des enseignants de français (AFEF)
%P 125-129
%8 2010
%D 2010
%R 10.3917/lfa.168.0125
%Z Humanities and Social Sciences/LiteratureOther publications
%G French
%L hal-02350906
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02350906
%~ UNIV-PAU
%~ SHS
%~ ALTER
%~ TESTUPPA

%0 Unpublished work
%T Le nombre en musique & en rhétorique, un même concept ? Compte-rendu de Sueur Agathe, <i>Le Frein et l’Aiguillon. Éloquence musicale et nombre oratoire (XVIe-XVIIIe siècle)</i> (Classiques Garnier, Paris, 2014, 423 p.)
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Bédouret-Larraburu, Sandrine
%9 vol. 16, n° 5 - Musique ! On lit
%@ 2115-8037
%J Acta fabula : Revue des parutions pour les études littéraires
%I École normale supérieure
%P https://www.fabula.org/revue/document9377.php
%8 2015
%D 2015
%Z Humanities and Social Sciences/LiteratureOther publications
%G French
%L hal-02351193
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02351193
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Unpublished work
%T Jacques Ancet : "Ode au recommencement"
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Bédouret-Larraburu, Sandrine
%9 n° 1020 - Julio Cortázar
%< avec comité de lecture
%@ 0014-2751
%J Europe. Revue littéraire mensuelle
%I Europe. Revue
%P 350-351
%8 2014
%D 2014
%Z Humanities and Social Sciences/LiteratureOther publications
%G French
%L hal-02351197
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02351197
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Book Section
%T Donner du sens à l’enseignement de la langue
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Bédouret-Larraburu, Sandrine
%@ 978-2-86781-733-5
%B Sens de la langue, sens du langage, Grammaire, Poésie, Traduction
%E Poulin
%E Isabelle and Roger
%E Jérôme
%I Presses universitaires de Bordeaux
%S Modernités
%P 97-108
%8 2011
%D 2011
%Z Humanities and Social Sciences/LiteratureBook sections
%G French
%L hal-02180409
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02180409
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Unpublished work
%T Compte-rendu de Caplan David, <i>Questions de possibilité et poésie contemporaine</i> (Presses universitaires de Liège, 2016, 154)
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Bédouret-Larraburu, Sandrine
%9 n° 1057 - Pierre Bergounioux
%@ 0014-2751
%J Europe. Revue littéraire mensuelle
%I Europe. Revue
%P 338-340
%8 2017
%D 2017
%Z Humanities and Social Sciences/LiteratureOther publications
%G French
%L hal-02351189
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02351189
%~ ALTER
%~ UNIV-PAU
%~ SHS
%~ TESTUPPA

%0 Unpublished work
%T Compte-rendu de Leclercq Armelle, <i>Les Équinoxiales</i> (Le Corridor bleu, 2015, 112 p.)
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Bédouret-Larraburu, Sandrine
%9 n° 1029-1030 - Max Frisch - Ludwig Hohl
%@ 0014-2751
%J Europe. Revue littéraire mensuelle
%I Europe. Revue
%P 348-350
%8 2015
%D 2015
%Z Humanities and Social Sciences/LiteratureOther publications
%G French
%L hal-02351191
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02351191
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Book
%T <i>Gaspard de la Nuit</i> d’Aloysius Bertrand
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Marcandier-Colard, Christine
%A Bédouret-Larraburu, Sandrine
%@ 978-2-35030-143-3
%I Atlande
%S Clefs Concours Lettres XIXe siècle
%8 2010
%D 2010
%K OS
%Z Humanities and Social Sciences/LiteratureBooks
%G French
%L hal-02331537
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02331537
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Unpublished work
%T Du roman à l’histoire de l’Oulipo [compte-rendu de Camille Bloomfield, Raconter l’Oulipo (1960‑2000). Histoire et sociologie d’un groupe, Paris : Honoré Champion, 2017]
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Bédouret-Larraburu, Sandrine
%9 vol. 19, n° 6
%< avec comité de lecture
%@ 2115-8037
%J Acta fabula : Revue des parutions pour les études littéraires
%I École normale supérieure
%P https://www.fabula.org:443/revue/document11147.php
%8 2018
%D 2018
%Z Humanities and Social Sciences/LiteratureOther publications
%G French
%L hal-02351194
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02351194
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Edited Book
%T Jacques Ancet ou la voix traversée
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Bédouret-Larraburu, Sandrine
%A Pouilloux, Jean-Yves
%@ 978-2-911648-45-8
%Y Bédouret-Larraburu
%Y Sandrine and Pouilloux
%Y Jean-Yves
%I Atelier du Grand Tétras
%S Résonance générale
%N 3
%8 2011
%D 2011
%Z Humanities and Social Sciences/LiteratureDirections of work or proceedings
%X no abstract
%G French
%L hal-02187690
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02187690
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Book
%T Le Spleen de Paris de Charles Baudelaire
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Boneu, Violaine
%A Bédouret-Larraburu, Sandrine
%@ 978-2-35030-291-1
%I Atlande
%S Clefs Concours Lettres XIXe siècle
%8 2014
%D 2014
%K OS
%Z Humanities and Social Sciences/LiteratureBooks
%X Tous les titres sont organisés autour d'une structure commune : - des repères : un rappel du contexte historique et littéraire. - les grandes "problématiques", indispensables à la compréhension des enjeux de l'œuvre, le "travail du texte" consacré aux questions de langue. de stylistique et de grammaire, des outils méthodologiques, notamment bibliographiques, un système de circulation entre les fiches et les références bibliographiques.
%G French
%L hal-02331523
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02331523
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Edited Book
%T L'épilinguistique sous le voile littéraire : Antoine Culioli et la TO(P)E
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Bédouret-Larraburu, Sandrine
%A Copy, Christine
%@ 978-2-35311-089-6
%I PUPPA
%S Linguiste et littérature
%V 3
%8 2018-02-09
%D 2018
%Z Humanities and Social Sciences/LiteratureDirections of work or proceedings
%X Le troisième volume de linguiste et littérature consacré à le to(p)e d'Antoine Culioli est paru aux PUPPA (Presses Universitaires de Pau et des Pays de l'Adour). Antoine Culioli, linguiste contemporain qui aura marqué de façon majeure les études linguistiques en France au XXe siècle, a peu travaillé le texte littéraire et a souvent exprimé son attachement à ce qu’il nomme « la langue ordinaire » comme lieu où se dévoile de façon privilégiée le voilé de l’activité de langage. Pourtant, chez Culioli, le texte littéraire, et en particulier le texte poétique, est souvent convoqué pour expliquer des phénomènes langagiers complexes. Antoine Culioli confie ainsi aux écrivains le soin d’illustrer des questions théoriques qui concernent l’activité langagière totale. Et même, interrogé sur sa théorie du langage par Claudine Normand, Antoine Culioli se tourne vers le domaine de l’art quand, partant d’une citation d’Alfred Brendel qui définit l’œuvre d’art comme le lieu où le chaos doit scintiller sous le voile de l’ordre, il décrit l’activité de langage comme celle où le texte fait apparaître sous le voile de l’ordre linguistique le foisonnement épilinguistique. Cette généralisation, de l’œuvre d’art vers le texte met au jour le lien particulier entre « langue littéraire » et « langue ordinaire » dans la pensée d’Antoine Culioli. Dans la littérature aussi, l’activité épilinguistique, définie comme « une activité dont nous n’avons pas conscience et qui sans arrêt travaille sur ces mises en relation entre le caché, le pas dire non, le ceci ou le cela », permet de révéler, à travers le travail sur la langue qu’opèrent les écrivains, la tension sous-jacente, le différé de l’accès au sens, inhérents à toutes pratiques langagières, et le texte littéraire peut alors bien devenir, pour les linguistes, un terrain privilégié pour observer la mise en œuvre du langage.
%G French
%L hal-02187689
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02187689
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Unpublished work
%T Compte-rendu de Dessons Gérard, <i>La voix juste. Essai sur le bref</i> (Manucius, 2015, 160 p.)
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Bédouret-Larraburu, Sandrine
%9 n° 1046-48 - Jean-Christophe Bailly
%@ 0014-2751
%J Europe. Revue littéraire mensuelle
%I Europe. Revue
%P 344-346
%8 2016
%D 2016
%Z Humanities and Social Sciences/LiteratureOther publications
%G French
%L hal-02351190
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02351190
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Book
%T Rimes et vers français
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Bédouret-Larraburu, Sandrine
%@ 978-2-35184-173-0
%I Editions Garnier
%S Les petits guides de la langue française
%N 5
%8 2017
%D 2017
%K OS
%Z Humanities and Social Sciences/LiteratureBooks
%X Si chacun connaît un poème, quelques vers, appris à l'école ou lus par plaisir, il est souvent plus délicat de dire ce qu'est la poésie. Qu'est-ce qu'un vers ? Un mètre ? Que sont les rimes léonines et leurs cousines dites "embrassées" ? Comment distinguer un rondeau, reconnaître une ballade, démasquer un poème en prose ? Sandrine Bédouret-Larraburu brosse le portrait de la versification française et guide le lecteur dans le dédale de la création poétique. Initié aux secrets de cet art subtil où la langue française brille de mille feux, il pourra d'autant plus savourer les textes poétiques, du Moyen Age à nos jours. Une nécessité si l'on pense, comme Baudelaire dans L'Art romantique, que "tout homme bien portant peut se passer de manger pendant deux jours, — de poésie, jamais" !
%G French
%L hal-02331520
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02331520
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Unpublished work
%T Compte-rendu de Martin Serge, <i>Voix et relation. Une poétique de l’art littéraire où tout se rattache</i> (Marie Delarbre, 2017, 353 p.)
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Bédouret-Larraburu, Sandrine
%9 n° 1067 - Yves Bonnefoy
%@ 0014-2751
%J Europe. Revue littéraire mensuelle
%I Europe. Revue
%P 340-341
%8 2018
%D 2018
%Z Humanities and Social Sciences/LiteratureOther publications
%G French
%L hal-02351192
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02351192
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Unpublished work
%T <i>Aubes</i> de Bernadette Engel-Roux
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Bédouret-Larraburu, Sandrine
%9 n° 5
%J Résonance générale. Cahiers pour la poétique
%I L'Atelier du Grand Tétras
%P 109-115
%8 2012
%D 2012
%Z Humanities and Social Sciences/LiteratureOther publications
%G French
%L hal-02351195
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02351195
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Unpublished work
%T Émile Benveniste pour vivre langage
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Bédouret-Larraburu, Sandrine
%9 n° 971 - Claude Esteban - Bernard Manciet
%< avec comité de lecture
%@ 0014-2751
%J Europe. Revue littéraire mensuelle
%I Europe. Revue
%P 475-476
%8 2010
%D 2010
%Z Humanities and Social Sciences/LiteratureOther publications
%G French
%L hal-02351196
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02351196
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Book Section
%T Ambiguïtés du numerus latin chez Cicéron et Quintilien
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Bédouret-Larraburu, Sandrine
%B L'art de la parole : pratiques et pouvoirs du discours dans l'Antiquité gréco-latine
%E Voisin, Patrick
%I L'Harmattan, Association Kubaba
%S Kubaba
%P 199-211
%8 2011
%D 2011
%K Civilization
%K Greco-Roman
%K Classical literature
%K Congresses
%K History and criticism
%K Oratory
%K Ancient
%K Rhetoric
%K Speeches
%K addresses
%K etc.
%K Greek
%K Latin
%Z Humanities and Social Sciences/LiteratureBook sections
%G French
%L hal-02180408
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02180408
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

