%0 Book Section
%T La reinvención del detective privado
%+ Arts / Langages : Transitions et Relations (ALTER)
%A GUYARD, Emilie
%@ 978-84-8408-753-3
%B La (re)invención del género negro
%E Martín Escribà
%E Àlex and Sánchez Zapatero
%E Javier
%I Andavira Ed
%P 123-131
%8 2014
%D 2014
%Z Humanities and Social Sciences/LiteratureBook sections
%G Spanish
%L hal-02180136
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02180136
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Book Section
%T L’identité personnelle comme lieu de l’invention: "Camino de ida" de Carlos Salem
%+ Arts / Langages : Transitions et Relations (ALTER)
%A GUYARD, Emilie
%@ 978-2-36441-040-4
%B Identité-Altérité dans la culture hispanique au XXe-XXIe siècles: hommage à Eliane et Jean-Marie Lavaud
%E Lavaud
%E Eliane and Lavaud
%E Jean-Marie and Orsini-Saillet
%E Catherine and Palau
%E Alexandra
%I Éditions universitaires de Dijon
%S Hispanistica XX
%N 29
%P 269-282
%8 2012
%D 2012
%K 20th century
%K 21st century
%K Foreign countries
%K History and criticism
%K Other (Philosophy) in literature
%K Spanish literature
%Z Humanities and Social Sciences/LiteratureBook sections
%G French
%L hal-02180226
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02180226
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Edited Book
%T L'imaginaire social dans le roman noir espagnol et portugais du XXIe siècle
%+ Arts / Langages : Transitions et Relations (ALTER)
%A GUYARD, Emilie
%@ 978-2-36783-091-9
%C Binges, France
%I Orbis tertius
%S Universitas
%N 28
%8 2017
%D 2017
%K Roman noir espagnol − 1990-.... − Aspect social
%K Roman noir portugais − 1990-.... − Aspect social
%Z Humanities and Social Sciences/LiteratureDirections of work or proceedings
%G French
%L hal-02182226
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02182226
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Edited Book
%T Roman Noir: espaces urbains et grands espaces
%+ Arts / Langages : Transitions et Relations (ALTER)
%A GUYARD, Emilie
%Y Guyard, Émilie
%I PUPPA
%S Líneas
%N 10
%8 2017
%D 2017
%Z Humanities and Social Sciences/LiteratureDirections of work or proceedings
%X Le lien entre la ville et le genre policier est si étroit que l’on pourrait presque se demander, à l’instar de Joan Ramón Resina, «~si no es la novela criminal un modo específico de reflexión sobre lo urbano~»1. A cet égard, le titre du récit considéré unanimement par tous les critiques comme le texte fondateur du genre revêt une valeur programmatique~: «~Double assassinat dans la Rue Morgue~» instaure dès 1848 la rue comme le lieu du crime~; il inscrit l’urbanité dans les gènes du genre policier, dans son acte de naissance. Or, ce lien entre la grande ville moderne et le genre policier naissant possède une double explication. L’urbanité du texte policier obéit tout d’abord à une logique purement mimétique. Si ce genre choisit de mettre en scène la rupture de l’ordre social provoqué par le crime, alors la ville apparaît comme son scénario idéal~: la grande ville moderne, née de la Révolution industrielle, avec sa cohorte d’ouvriers mal payés, son accroissement démesuré et incontrôlé, voit surgir de nouvelles formes de délinquance et de criminalité qui fournissent un matériau idéal à ce nouveau genre littéraire. Comme le signale Marc Lits, c’est «~la civilisation industrielle et son urbanisation qui vont permettre le développement d’une faune interlope hantant les rues des grandes villes et, pour lutter contre celle-ci, la création d’une police organisée~»2. Ce n’est d’ailleurs pas un hasard si Edgar Allan Poe choisit de situer les aventures de son Chevalier Auguste Dupin à P
%G French
%L hal-02187700
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02187700
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Book Section
%T <i>El Tigre Blanco</i> de Carlos Salem : un cycle romanesque édifiant
%+ Arts / Langages : Transitions et Relations (ALTER)
%A GUYARD, Emilie
%@ 978-2-8071-0164-7
%B Grands auteurs pour petits lecteurs. 2: la littérature de jeunesse des grands romanciers
%E Pérès, Christine
%I Lansman éditeur
%S Hispania
%N 21
%P 171-186
%8 2017
%D 2017
%K Littérature pour la jeunesse espagnole − Histoire et critique
%K Écrivains pour la jeunesse espagnols − Critique et interprétation
%Z Humanities and Social Sciences/LiteratureBook sections
%G French
%L hal-02179836
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02179836
%~ ALTER
%~ SHS
%~ UNIV-PAU
%~ TESTUPPA

%0 Book Section
%T La Gorgona de Gonzalo Torrente Ballester : un espacio intertextual y metaficcional
%+ Arts / Langages : Transitions et Relations (ALTER)
%A GUYARD, Emilie
%@ 978-84-15175-20-9
%B Miradas sobre Gonzalo Torrente Ballester en su centenario, 1910-2010
%E Becerra, Carmen
%I Academia del Hispanismo
%S Biblioteca Gonzalo Torrente Ballester
%N 8
%P 83-96
%8 2011
%D 2011
%K Congresses
%K Criticism and interpretation
%K Torrente Ballester
%K Gonzalo
%Z Humanities and Social Sciences/LiteratureBook sections
%G Spanish
%L hal-02180437
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02180437
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Book Section
%T La novela negra disparatada de Carlos Salem
%+ Arts / Langages : Transitions et Relations (ALTER)
%A GUYARD, Emilie
%@ 978-84-8408-648-2
%B El género negro: el fin de la frontera
%E Congreso de novela y cine negro and Sánchez Zapatero
%E Javier and Martín Escribà
%E Àlex
%I Andavira Editora
%P 311-318
%8 2012
%D 2012
%Z Humanities and Social Sciences/LiteratureBook sections
%G Spanish
%L hal-02180227
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02180227
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Book Section
%T Para un acercamiento a la recepción de los textos de metaficción
%+ Arts / Langages : Transitions et Relations (ALTER)
%A GUYARD, Emilie
%@ 978-3-643-80125-8
%B Metanarrativas hispánicas
%E Álvarez
%E Marta and Gil González
%E Antonio Jesús and Kunz
%E Marco
%I Lit Verlag
%S LIT Ibéricas
%N vol. 2
%P 47-59
%8 2012
%D 2012
%K 1900-2099
%K 20th century
%K 21st century
%K Criticism
%K interpretation
%K etc
%K History and criticism
%K Latin American fiction
%K Spanish fiction
%Z Humanities and Social Sciences/LiteratureBook sections
%G Spanish
%L hal-02180228
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02180228
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Edited Book
%T Formas de lo fantástico en la literatura de Gonzalo Torrente Ballester
%+ Arts / Langages : Transitions et Relations (ALTER)
%A GUYARD, Emilie
%Y Guyard, Émilie
%I Universidad de Vigo
%S La Tabla Redonda. Anuario de Estudios Torrentinos
%N 12
%8 2014
%D 2014
%Z Humanities and Social Sciences/LiteratureDirections of work or proceedings
%X no abstract
%G Spanish
%L hal-02187699
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02187699
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Book Section
%T El cine negro como referente en la novela negra española actual
%+ Arts / Langages : Transitions et Relations (ALTER)
%A GUYARD, Emilie
%@ 978-3-0352-0260-1 978-3-0343-1541-8
%B La narrativa española de hoy: (2000 - 2013); la imagen en el texto. 3: ...
%E Noyaret, Natalie
%I Lang
%S Liminaires - Passages interculturels
%N 32
%P 39-54
%8 2014
%D 2014
%Z Humanities and Social Sciences/LiteratureBook sections
%G Spanish
%L hal-02180135
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02180135
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

