%0 Conference Paper
%F Poster 
%T Biological aerosol particles in the atmosphere and their impact clouds (bioclouds project)
%+ Synthèse et étude de systèmes à intêret biologique (SEESIB)
%+ Laboratoire de météorologie physique (LaMP)
%+ Forschungszentrum Karlsruhe
%+ Unité de Pathologie Végétale (PV)
%A Attard, Eléonore
%A Vaitilingom, Mickaël
%A Sancelme, Martine
%A Flossmann, Andrea
%A Möhler, Ottmar
%A Morris, Cindy E.
%A Delort, Anne-Marie
%Z Poster
%< sans comité de lecture
%B Colloque national Microbiologie des Aérosols, MicrobAERO 2009
%C Narbonne, France
%8 2009-10-06
%D 2009
%Z Life Sciences [q-bio]
%Z Environmental Sciences/Global Changes
%Z Environmental Sciences/Biodiversity and EcologyPoster communications
%G English
%L hal-00506443
%U https://hal.archives-ouvertes.fr/hal-00506443
%~ INSU
%~ CNRS
%~ INRA
%~ ICC
%~ LAMP
%~ UNIV-BPCLERMONT
%~ PRES_CLERMONT
%~ INRAE
%~ SDE
%~ AGROPOLIS
%~ GIP-BE
%~ AGREENIUM
%~ PV

%0 Book Section
%T Soil microbial diversity in grasslands and its importance for grassland functioning and services
%+ Laboratoire d'Ecologie Microbienne - UMR 5557 (LEM)
%+ Fractionnement des AgroRessources et Environnement (FARE)
%A Le Roux, Xavier
%A Recous, Sylvie
%A Attard, Eléonore
%@ 978-1-84593-809-3
%B Grassland productivity and ecosystem services
%I CAB International
%C Oxon (united kingdom)
%8 2011
%D 2011
%Z Environmental Sciences/Biodiversity and EcologyBook sections
%X This chapter contributes to understanding the role and impact of soil microbe populations and communities, in the light of the new molecular technologies that allow analysis of the diversity of these microbes and their functional role in C and N cycles. It also discusses the importance of soil biodiversity for grassland functioning (species assemblage and species removal) and representation of grassland soil biodiversity in models of grassland functioning and service delivery.
%G English
%L hal-02809015
%U https://hal.inrae.fr/hal-02809015
%~ INRAE
%~ UNIV-PICARDIE
%~ INRA
%~ SDE
%~ GIP-BE
%~ UNIV-LYON1
%~ URCA
%~ FARE
%~ CNRS
%~ UDL
%~ UNIV-LYON
%~ BIOENVIS
%~ ECOMIC
%~ AGREENIUM
%~ U-PICARDIE

%0 Book Section
%T Soil microbial diversity in grasslands and its importance for grassland functioning and services
%+ Laboratoire d'Ecologie Microbienne - UMR 5557 (LEM)
%+ Unité d'Agronomie de Laon-Reims-Mons (AGRO-LRM)
%A Le Roux, X.
%A Recous, Sylvie
%A Attard, E.
%@ 9781845938093
%B Grassland Productivity and Ecosystems Services
%E CABI
%I Lemaire, G., Hodgson, J., Chabbi, A.
%P 158-165
%8 2011
%D 2011
%R 10.1079/9781845938093.0158
%K biodiversity
%K carbon cycle
%K ecosystem services
%K genetic diversity
%K grassland soils
%K grasslands
%K microbial flora
%K molecular genetics techniques
%K nitrogen cycle
%K soil types
%Z Life Sciences [q-bio]Book sections
%X This book contains 28 chapters with emphasis on the interactive nature of the relationships between the soil, plant, animal and environmental components of grassland systems, both natural and managed. It analyses the present knowledge and the future trends of research for combining the classical view of grasslands, as a resource for secure feeding of an increasing human population, with the more recent perspective of the contribution of grasslands to the mitigation of environmental impacts and biodiversity erosion as consequences of human society activities. The chapters are organized within five sections dealing with the different functions and the main ecosystem services expected from grasslands: (i) domestic herbivore feeding and animal production; (ii) the regulation of biogeochemical cycles and its consequences for the environment; (iii) dynamics of biodiversity hosted by grasslands; (iv) integration of grasslands within sustainable animal production systems; and (v) interactions of grassland areas with other land use systems at the landscape level.
%G English
%L hal-01631829
%U https://hal.archives-ouvertes.fr/hal-01631829
%~ CNRS
%~ INRA
%~ BIOENVIS
%~ UNIV-LYON1
%~ UDL
%~ UNIV-LYON
%~ INRAE
%~ ECOMIC
%~ AGREENIUM

%0 Thesis
%T Study of the response of soil bacterial communities after changes in land-use in agrosystems
%T Etude de la réponse des communautés microbiennes aux changements de mode de gestion des agrosystèmes
%+ Fractionnement des AgroRessources et Environnement (FARE)
%A Attard, Eléonore
%Z Diplôme : Dr. d'Université
%P 367 p.
%I Université de Poitiers
%Y André Amblès
%Y Xavier Le Roux
%Y Sylvie Recous
%8 2008
%D 2008
%K these
%K ECOLOGIE
%Z Life Sciences [q-bio]/Food engineering
%Z Engineering Sciences [physics]/Chemical and Process Engineering
%Z Life Sciences [q-bio]
%Z Environmental SciencesTheses
%X Malgré leur rôle fondamental dans le fonctionnement du sol, les communautés bactériennes et leurs réponses à des changements de gestion des agrosystèmes ont très peu été étudiées. Notre travail a porté sur 3 situations (intensification/désintensification du pâturage ; conversion prairie/culture ; et application ou arrêt du labour de sols cultivés). L’objectif a été de déterminer après ces changements (i) la réponse de communautés bactériennes fonctionnelles du sol en terme d’activité, d’effectif et de structure, (ii) l’influence des gestions passée et actuelle sur ces caractéristiques bactériennes, (iii) les différences de réponses observées entre différentes communautés bactériennes, et (iv) les variables environnementales importantes pour comprendre ces réponses. Une intensification de la gestion génére des effets plus prononcés et plus rapides sur les communautés bactériennes qu’une désintensification. Après intensification, les caractéristiques des communautés bactériennes sont rapidement et parfois entièrement pilotées par la nouvelle gestion ; après désintensification, ces caractéristiques restent au moins plusieurs années dépendantes des gestions passée et actuelle. Après changements des gestions, les changements d’activités bactériennes sont peu corrélés aux modifications de la structure des communautés correspondantes. Par contre, les changements d’effectifs bactériens et de paramètres environnementaux expliquent fortement les changements d’activité bactérienne, les mécanismes sous-jacents différant entre communautés étudiées. Nous discutons l’implication de nos résultats pour une meilleure prise en compte de la réponse du système sol aux changements de gestion.
%G French
%L tel-02816395
%U https://hal.inrae.fr/tel-02816395
%~ INRAE
%~ UNIV-PICARDIE
%~ INRA
%~ SDE
%~ GIP-BE
%~ URCA
%~ FARE
%~ CNRS
%~ AGREENIUM
%~ U-PICARDIE

%0 Book Section
%T Anaerobic microbial communities and processes involved in the methane cycle in freshwater lakes - a focus on lake Pavin.
%+ Laboratoire Microorganismes : Génome et Environnement (LMGE)
%A Lehours, Anne Catherine
%A Borrel, Guillaume
%A Morel-Desrosiers, Nicole
%A Bardot, Corinne
%A Grossi, Vincent
%A Keraval, Benoit
%A Attard, Eleonore
%A Morel, Jean-Pierre
%A Amblard, Christian, C.
%A Fonty, Gérard
%B Lake Pavin: History, biogeochemistry and sedimentology of a deep meromictic crater lake
%8 2016-06
%D 2016
%Z Life Sciences [q-bio]/Ecology, environment/EcosystemsBook sections
%G English
%L hal-01623844
%U https://hal.archives-ouvertes.fr/hal-01623844
%~ CNRS
%~ GIP-BE
%~ PRES_CLERMONT
%~ LMGE

