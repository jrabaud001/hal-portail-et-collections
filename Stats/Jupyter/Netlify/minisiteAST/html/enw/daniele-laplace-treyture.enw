%0 Book Section
%T Jardins productifs en ville. Enjeux et difficultés d'une territorialisation de la critique sociale
%+ Passages
%A Laplace-Treyture, Danièle
%A Douence, Hélène
%B Reprendre la terre, agriculture et critique sociale. (dir E. Doidy et M Gateau) Nancy. Ed. Kairos
%8 2018
%D 2018
%Z Humanities and Social Sciences
%Z Humanities and Social Sciences/GeographyBook sections
%G French
%L halshs-02176192
%U https://halshs.archives-ouvertes.fr/halshs-02176192
%~ SHS
%~ UNIV-PAU
%~ AO-GEOGRAPHIE
%~ CNRS
%~ GIP-BE
%~ UMR5319
%~ UNIV-BORDEAUX-MONTAIGNE
%~ PASSAGES-UPPA
%~ TESTUPPA

%0 Book Section
%T A propos de quelques enjeux du jardinage urbain
%+ Passages
%A Laplace-Treyture, Danièle
%A Douence, Hélène
%B El paisaje y sus simbolos (dir. E. Martinez de Pison, N. Ortega Cantero). Madrid. FDS-UAM Ediciones
%8 2016
%D 2016
%Z Humanities and Social Sciences
%Z Humanities and Social Sciences/GeographyBook sections
%G French
%L halshs-02176171
%U https://halshs.archives-ouvertes.fr/halshs-02176171
%~ SHS
%~ UNIV-PAU
%~ AO-GEOGRAPHIE
%~ CNRS
%~ GIP-BE
%~ UMR5319
%~ UNIV-BORDEAUX-MONTAIGNE
%~ PASSAGES-UPPA
%~ TESTUPPA

%0 Book Section
%T Quand le terroir a lieu. Ce que la musique fait au vigneron
%+ Passages
%A Douence, Hélène
%A Laplace, Danièle
%A Tesson, Frédéric
%@ 978-2-271-08225-1
%B La petite musique des territoires : art, espace et sociétés
%E Canova, Nicolas
%E Bourreau, Philippe
%E Soubeyran, Olivier
%I CNRS
%C Paris
%P 85-94
%8 2014
%D 2014
%K musique
%K territoire
%K viticulture
%Z Humanities and Social Sciences/GeographyBook sections
%G French
%L halshs-01569296
%U https://halshs.archives-ouvertes.fr/halshs-01569296
%~ SHS
%~ CNRS
%~ UNIV-PAU
%~ AO-GEOGRAPHIE
%~ GIP-BE
%~ UMR5319
%~ UNIV-BORDEAUX-MONTAIGNE
%~ PASSAGES-UPPA
%~ TESTUPPA

%0 Unpublished work
%T Interpréter l’espace urbain en marchant
%+ Passages
%A CHARLIER, Bruno
%A Douence, Hélène
%A Laplace-Treyture, Danièle
%A Miaux, Sylvie
%Z Démarche Qualité et Service Universitaire de Pédagogie (SUP) de l’UPPA.
%9 Ce film a été réalisé dans le cadre du Workshop « Es-tu Laü ? Je suis Laü ! A la recherche des traces d’un ruisseau urbain » organisé par le Master Géographie, Aménagement, Environnement, Développement (GAED) du département de Géographie et Aménagement de l’Université de Pau et des Pays de l’Adour (UPPA). Ce Workshop a été réalisé avec le soutien financier de la Démarche Qualité et du Service Universitaire de Pédagogie (SUP) de l’UPPA.
%@ 2428-1387
%J Mondes sociaux
%I Labex Structurations des mondes sociaux
%8 2017-10
%D 2017
%K Marche
%K Hodologie
%K Hendrick Sturm
%K pédagogie
%K expérience des lieux
%Z Humanities and Social Sciences/GeographyOther publications
%X Avez-vous déjà essayé de lire et d’explorer l’espace urbain par l’expérience de co-création procurée par une promenade collective ? C’est cette expérience réalisée avec Hendrik Sturm, artiste « marcheur-sculpteur d’espace » selon Élise Olmédo, qui a été proposée aux étudiants du Master de Géographie et Aménagement de l’Université de Pau et des Pays de l’Adour. Le film (Dé)marche retrace et reconstitue cette démarche d’interprétation des lieux où l’enseignant joue le rôle de pédagogue au sens premier de celui qui accompagne l’élève à l’école en cheminant à ses côtés.La marche comme forme particulière de découverte, de lecture et d’appropriation des lieux, contient aussi un potentiel de (re)découverte de soi, des autres et de ce qui nous entoure. Elle relève bien ici de l’hodologie, cette science des cheminements qui permet une « conduite de l’individu (…) selon un ensemble de ‘détours’ et de chemins privilégiés, relatifs à ses investissements psychiques dans le monde (Besse, 2004) ». En termes géographiques, cela revient à mettre en œuvre une géographie des territorialités, au sens de « manières de vivre le territoire », et des espaces vécus.L’artiste Hendrik Sturm (2010) déploie son art de la marche comme une pratique de l’in situ, qui facilite un double engagement sensible et cognitif de l’individu. Ces promenades au cours desquelles on marche ensemble jouent à la fois sur le registre de l’expérience sensible où le corps est convoqué pour saisir l’espace et sur celui d’une appréhension des lieux à travers leur connaissance. La démarche de Sturm offre cette capacité à mettre en intrigue les lieux par la recherche d’indices – à partir de la lecture de traces – qui ouvrent sur leur histoire, leurs usages et leurs représentations. Et donc à aller au-delà de apparences et des évidences.Le fil conducteur du film (Dé)marches est un ruisseau suivi par les marcheurs, filmé tantôt in situ, tantôt en carte (ce qui permet de suivre l’évolution des marcheurs). Trois séquences ont particulièrement été retenues pour illustrer trois temps forts de la promenade comme « moment-lieu » précis où s’est nouée une enquête particulière : la localisation d’un émissaire déversant des eaux usées et malodorantes dans le ruisseau, la découverte d’un sac d’écolier et d’un carnet scolaire dans une friche militaire, l’interpellation virulente sur notre présence par une riveraine lors de notre traversée. Les séquences-transitions rappellent la diversité de nos étonnements et le côté ressenti/vécu de la marche dans un ruisseau urbain. La séquence finale va faire comprendre non seulement qu’on peut toujours continuer à marcher, à s’interroger (un groupe s’éloigne en marchant…) mais qu’on a fait l’expérience de la superposition des territoires révélée par la marche (chacun habite et vit le territoire à sa manière).Ce film a été réalisé dans le cadre du Workshop « Es-tu Laü ? Je suis Laü ! A la recherche des traces d’un ruisseau urbain » organisé par le Master Géographie, Aménagement, Environnement, Développement (GAED) du département de Géographie et Aménagement de l’Université de Pau et des Pays de l’Adour (UPPA). Ce Workshop a été réalisé avec le soutien financier de la Démarche Qualité et du Service Universitaire de Pédagogie (SUP) de l’UPPA.
%G French
%L halshs-01703843
%U https://halshs.archives-ouvertes.fr/halshs-01703843
%~ SHS
%~ CNRS
%~ UNIV-PAU
%~ AO-GEOGRAPHIE
%~ GIP-BE
%~ UMR5319
%~ UNIV-BORDEAUX-MONTAIGNE
%~ PASSAGES-UPPA
%~ TESTUPPA

