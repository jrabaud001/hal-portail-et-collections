%0 Journal Article
%T Membrane Interacting Peptides: From Killers to Helpers.
%+ Chimie et Biologie des Membranes et des Nanoobjets (CBMN)
%+ Université Pierre et Marie Curie - Paris 6 (UPMC)
%+ Plateau technique, Biologie Structurale, RMN liquide/solide - Institut Européen de Chimie et Biologie
%A Dufourc, Erick J
%A Buchoux, Sébastien
%A Toupé, Jeannot
%A Sani, Marc-Antoine
%A Jean-François, Frantz
%A Khemtémourian, Lucie
%A Grélard, Axelle
%A Loudet-Courrèges, Cécile
%A Laguerre, Michel
%A Elezgaray, Juan
%A Desbat, Bernard
%A Odaert, Benoit
%< avec comité de lecture
%@ 1389-2037
%J Current Protein and Peptide Science
%I Bentham Science Publishers
%V 13
%N 7
%P 620-631
%8 2012-10-30
%D 2012
%R 10.2174/138920312804142138
%M 23116443
%Z Chemical Sciences/Organic chemistryJournal articles
%X Membrane interacting peptides are reviewed in terms of structure and mode of action on lipid membranes. Helical, β-stranded, peptides containing both helices and strands, cyclic, lipopeptides and short linear peptides are seen to considerably modulate membrane function. Among peptides that lead to membrane alteration or permeation, antimicrobial peptides play an important role and some of them may be foreseen as potential new antibiotics. Alternatively, peptides that do not destroy the membrane are also very important in modulating the structure and dynamics of the lipid bilayer and play important roles in membrane protein functions. Peptide lipid complexes are shown to be very variable in structure and dynamics: "carpet", "barrel stave", toroid and disordered pores, electrostatic wedge and molecular electroporation models are discussed. Their assembly is reviewed in terms of electric, amphipathic and dynamic properties of both lipids and peptides.
%G English
%L hal-00748185
%U https://hal.archives-ouvertes.fr/hal-00748185
%~ CNRS
%~ UPMC
%~ INC-CNRS
%~ LBM-E3
%~ SORBONNE-UNIVERSITE

%0 Journal Article
%T Structural evidence of a phosphoinositide-binding site in the Rgd1-RhoGAP domain
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Martinez, David
%A d’Estaintot, B.L.
%A Granier, T.
%A Tolchard, J.
%A Courreges, Cecile
%A Prouzet-Mauléon, V.
%A Hugues, M.
%A Gallois, B.
%A Doignon, F.
%A Odaert, B.
%< avec comité de lecture
%@ 0264-6021
%J Biochemical Journal
%I Portland Press
%V 474
%N 19
%P 3307--3319
%8 2017
%D 2017
%R 10.1042/BCJ20170331
%Z Chemical SciencesJournal articles
%X Phosphoinositide lipids recruit proteins to the plasma membrane involved in the regulation of cytoskeleton organization and in signalling pathways that control cell polarity and growth. Among those, Rgd1p is a yeast GTPase-activating protein (GAP) specific for Rho3p and Rho4p GTPases, which control actin polymerization and stress signalling pathways. Phosphoinositides not only bind Rgd1p, but also stimulate its GAP activity on the membrane-anchored form of Rho4p. Both F-BAR (F-BAR FCH, and BAR) and RhoGAP domains of Rgd1p are involved in lipid interactions. In the Rgd1p–F-BAR domain, a phosphoinositide-binding site has been recently characterized. We report here the X-ray structure of the Rgd1p–RhoGAP domain, identify by NMR spectroscopy and confirm by docking simulations, a new but cryptic phosphoinositide-binding site, comprising contiguous A1, A10 and B helices. The addition of helix A10, unusual among RhoGAP domains, seems to be crucial for lipid interactions. Such a site was totally unexpected inside a RhoGAP domain, as it was not predicted from either the protein sequence or its three-dimensional structure. Phosphoinositide-binding sites in RhoGAP domains have been reported to correspond to polybasic regions, which are located at the unstructured flexible termini of proteins. Solid-state NMR spectroscopy experiments confirm the membrane interaction of the Rgd1p–RhoGAP domain upon the addition of PtdIns(4,5)P2 and indicate a slight membrane destabilization in the presence of the two partners. © 2017 The Author(s).
%G English
%L hal-01617275
%U https://hal.archives-ouvertes.fr/hal-01617275
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ IPREM-CAPT
%~ TESTUPPA

%0 Journal Article
%T New insights into the characterization of the electrode/electrolyte interfaces within LiMn 2 O 4 /Li 4 Ti 5 O 12 cells, by X-ray photoelectron spectroscopy, scanning Auger microscopy and time-of-flight secondary ion mass spectrometry
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ SAFT [Bordeaux]
%A Gieu, Jean-Baptiste
%A Winkler, Volker
%A Courreges, Cecile
%A El Ouatani, Loubna
%A Tessier, Cécile
%A Martinez, Hervé
%< avec comité de lecture
%@ 2050-7488
%J Journal of Materials Chemistry A
%I Royal Society of Chemistry
%V 5
%N 29
%P 15315 - 15325
%8 2017
%D 2017
%R 10.1039/c7ta02529g
%Z Chemical Sciences/Analytical chemistry
%Z Chemical Sciences/Material chemistryJournal articles
%X This work aims to study the electrode/electrolyte interfaces in a Li 4 Ti 5 O 12 (LTO)/LiMn 2 O 4 (LMO) cell assembled with a VC-containing electrolyte and operating at 60 C. LMO and LTO electrodes were mainly analyzed by X-ray Photoelectron Spectroscopy (XPS) after the first and tenth galvanostatic cycles. The XPS results indicate that both electrodes are covered by surface layers during the first charge, coming from the degradation of electrolyte species, inducing irreversible capacity losses. Although the compositions of both layers are similar, the one formed on LTO electrodes is thicker than the one formed on LMO electrodes and contains small amounts of MnF 2 , homogeneously spread over the surface, as revealed by the fluorine elemental mapping obtained by a complementary scanning Auger microscopy experiment. An additional measurement by time-of-flight secondary ion mass spectrometry indicates that the MnF 2 is located on top of the surface layer. XPS analysis also indicates that during the first discharge, the thickness of the LTO electrode surface layer slightly decreases, due to a partial dissolution, while no changes are observed on the LMO electrode. After the tenth charge, the layers do not present any noticeable changes compared to the first charge. Interfacial layers in the LMO/LTO cell are mainly formed during the first charge, inducing an irreversible capacity loss. During the following cycles, the surface layer on LMO electrodes is stable, while it is slightly dissolved and reformed in each cycle on LTO electrodes, as suggested by the electrochemical data showing smaller and decreasing capacity losses, characteristic of the gradual passivation of these electrodes.
%G English
%L hal-01622272
%U https://hal.archives-ouvertes.fr/hal-01622272
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ IPREM-PCM
%~ TESTUPPA

%0 Journal Article
%T Influence of the Positive Electrode on Li 4 Ti 5 O 12 (LTO) Electrode/Electrolyte Interfaces in Li-Ion Batteries
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ SAFT [Bordeaux]
%A Gauthier, Nicolas
%A Courrèges, Cécile
%A Goubault, Lionel
%A Demeaux, Julien
%A Tessier, Cécile
%A Martinez, Hervé
%< avec comité de lecture
%@ 0013-4651
%J Journal of The Electrochemical Society
%I Electrochemical Society
%V 165
%N 13
%P A2925-A2934
%8 2018-09-15
%D 2018
%R 10.1149/2.0261813jes
%K LTO
%K SEI
%K Li-ion Batteries
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Analytical chemistry
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Material chemistryJournal articles
%X Li4Ti5O12(LTO)-based negative electrode for lithium-ion batteries is of interest for electrical vehicles due to its safety, low cost and cycling stability. In this study, the effect of the positive electrode on the electrochemical performances of LTO electrodes, in relation with the Solid Electrolyte Interphase (SEI) properties, has been investigated. Full cells LTO/LiNi3/5Co1/5Mn1/5O2 (NMC) and LTO/LiMn2O4 (LMO) were cycled at 40°C over 100 cycles and the electrodes were analyzed by XPS and Scanning Auger Microscopy (SAM) after one and 100 cycles. For both systems, LTO electrodes are homogeneously covered by surface layers since the first cycle which induces an irreversible capacity loss. This latter is more important for LTO/LMO compared to LTO/NMC. Both SEI layers are composed of organic (polyethylene oxides, oxalates) and inorganic species (LiF, phosphates and fluorophosphates) but in different proportions: more fluorine species are detected for LTO/LMO for instance. Moreover, the SEI is thicker on the LTO electrode when cycled versus LMO compared to NMC and contains small amounts of manganese, homogeneously spread over the surface. In conclusion, a thick SEI associated with the presence of metallic species could alter the passivating role of the SEI and explain the less efficient electrochemical performance of LTO/LMO cells.
%G English
%L hal-01999897
%U https://hal.archives-ouvertes.fr/hal-01999897
%~ UNIV-PAU
%~ IPREM
%~ IPREM-PCM
%~ CNRS
%~ INC-CNRS

%0 Journal Article
%T Influence of Vinylene Carbonate Additive on the Li4Ti5O12 Electrode/Electrolyte Interface for Lithium-Ion Batteries.
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ SAFT [Bordeaux]
%A Gieu, Jean-Baptiste
%A Courreges, Cecile
%A El Ouatani, Loubna
%A Tessier, Cecile
%A Martinez, Hervé
%< avec comité de lecture
%@ 0013-4651
%J Journal of The Electrochemical Society
%I Electrochemical Society
%V 164
%P A1314-A1320
%8 2017
%D 2017
%R 10.1149/2.0111707jes
%K vinylene carbonate
%K LTO
%K SAM
%K SEI
%K batteries
%K XPS
%Z Chemical Sciences/Analytical chemistryJournal articles
%X The effect of Vinylene Carbonate (VC) additive upon the properties of the Solid Electrolyte Interphase (SEI) formed on top of Li4Ti5O12 (LTO) electrodes at 60°C, in relation with electrochem. data has been studied. Half cells LTO/Li were cycled at 60°C with VC-free and VC-contg. electrolytes over 30 cycles and electrodes were analyzed by XPS after the first and tenth cycles. Scanning Auger Microscopy (SAM) anal. have also been performed after the first lithiation. XPS results show that for both electrolytes, a SEI is formed during lithiation of LTO electrodes and partially dissolved during delithiation. The use of VC promotes the formation of a thicker SEI since the first cycle, more stable toward dissoln. and enhances the amt. of org. compds. at the expense of LiF. SAM chem. mappings confirm the formation of a thicker and homogeneously distributed SEI at the first cycle for a VC-contg. electrolyte. The dissoln. process obsd. during delithiation mainly involves LiF without VC and Li2CO3 with VC. The formation of a thicker SEI implies higher capacity losses, which is consistent with electrochem. data showing higher capacity losses for a VC-contg. electrolyte. VC additive is nonetheless expected to enhance the passivation of LTO electrodes over long cycling.
%G English
%L hal-01530950
%U https://hal.archives-ouvertes.fr/hal-01530950
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ IPREM-PCM

%0 Journal Article
%T Probing the in-depth distribution of organic/inorganic molecular species within the SEI of LTO/NMC and LTO/LMO batteries: A complementary ToF-SIMS and XPS study
%+ SAFT [Bordeaux]
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Gauthier, Nicolas
%A Courreges, Cecile
%A Demeaux, Julien
%A Tessier, Cécile
%A Martinez, Hervé
%< avec comité de lecture
%@ 0169-4332
%J Applied Surface Science
%I Elsevier
%V 501
%P 144266
%8 2020
%D 2020
%R 10.1016/j.apsusc.2019.144266
%K XPS (Al and Ag sources)
%K Depth-profile
%K ToF-SIMS
%K LiMn2O4 (LMO)
%K Li-ion Batteries
%K SEI
%K Li4Ti5O12 (LTO)
%K LiN3/5iCo1/5Mn1/5O2 (NMC)
%Z Chemical Sciences/Material chemistry
%Z Chemical Sciences/Analytical chemistryJournal articles
%X Spinel Li4Ti5O12 (LTO) is an attractive candidate for negative electrode materials of Li-ion batteries because of its outstanding safety characteristics. In this paper, the influence of common high voltage cathodes, LiNi3/5Co1/5Mn1/5O2 (NMC) and LiMn2O4 (LMO), upon the electrochemical performances of LTO/LMO and LTO/NMC cells, in relation with the Solid Electrolyte Interphase (SEI) properties formed over the LTO surface, is studied. After cycling, the electrodes were analyzed by ToF-SIMS and XPS with two X-ray sources (Ag and Al) to investigate both the chemical composition and the in-depth distribution of specific SEI species at the electrode surface. Facing both counter-electrodes, LTO electrodes are covered by surface layers due to the degradation of electrolyte components inducing an irreversible capacity loss, more important for LTO/LMO cells. The chemical composition of both layers is similar: organic and inorganic species but the SEI formed on LTO electrode cycled facing LMO electrode is thicker and contains small amounts of manganese compounds from the positive electrode. Moreover, 3D mappings reconstructed from ToF-SIMS depth-profile experiments, display different in-depth spatial distributions of SEI species, which are in agreement with XPS results. Consequently, the impact of interactions between electrodes on the formation of surface films is discussed.
%G English
%L hal-02429722
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02429722
%~ UNIV-PAU
%~ IPREM
%~ INC-CNRS
%~ CNRS
%~ IPREM-PCM
%~ ANR

%0 Journal Article
%T Thermoresponsive gold nanoshell@mesoporous silica nano-assemblies: an XPS/NMR survey
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%+ Laboratoire de physique et chimie des nano-objets (LPCNO)
%+ Laboratoire de chimie de coordination (LCC)
%A Soulé, Samantha
%A Allouche, Joachim
%A Dupin, Jean-Charles
%A Courreges, Cecile
%A Plantier, Frédéric
%A Ojo, Wilfried-Solo
%A Coppel, Yannick
%A Nayral, Céline
%A Delpech, Fabien
%A Martinez, Hervé
%< avec comité de lecture
%@ 1463-9076
%J Physical Chemistry Chemical Physics
%I Royal Society of Chemistry
%V 17
%N 43
%P 28719-28728
%8 2015
%D 2015
%K Surface properties
%K Bridged Compounds
%K X ray photoelectron spectroscopy
%K imidazole derivative
%K Nanoshells
%Z Chemical SciencesJournal articles
%X This work provides a detailed study on the physico-chemical characterization of a mechanized silver–gold alloy@mesoporous silica shell/pseudorotaxane nano-assembly using two main complementary techniques: XPS and NMR (liquid- and solid-state). The pseudorotaxane nanovalve is composed of a stalk (N-(6-aminohexyl)-aminomethyltriethoxysilane)/macrocycle (cucurbit[6]uril (CB6)) complex anchored to the silica shell leading to a silica/nanovalve hybrid organic–inorganic interface that has been fully characterized. The stalk introduction in the silica network was clearly demonstrated by XPS measurements, with the Si 2p peak shifting to lower energy after grafting, and through the analysis of the C 1s and N 1s core peaks, which indicated the presence of CB6 on the nanoparticle surface. For the first time, the complex formation on nanoparticles was proved by high speed 1H MAS NMR experiments. However, these solid state NMR analyses have shown that the majority of the stalk does not interact with the CB6 macrocycle when formulated in powder after removing the solvent. This can be related to the large number of possible organizations and interactions between the stalk, the CB6 and the silica surface. These results highlight the importance of using a combination of adapted and complementary highly sensitive surface and volume characterization techniques to design tailor-made hybrid hierarchical structured nano-assemblies with controlled and efficient properties for potential biological purposes.
%G English
%L hal-02020380
%U https://hal.insa-toulouse.fr/hal-02020380
%~ INSA-TOULOUSE
%~ IRSAMC
%~ CNRS
%~ UNIV-PAU
%~ IPREM
%~ INC-CNRS
%~ LPCNO
%~ LFCR
%~ INSA-GROUPE
%~ LCC
%~ UNIV-TLSE3
%~ IPREM-PCM
%~ INPT

%0 Journal Article
%T Impact of the cycling temperature on electrode/electrolyte interfaces within Li4Ti5O12 vs LiMn2O4 cells
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ SAFT [Bordeaux]
%A Gauthier, Nicolas
%A Courreges, Cecile
%A Demeaux, Julien
%A Tessier, Cécile
%A Martinez, Hervé
%< avec comité de lecture
%@ 0378-7753
%J Journal of Power Sources
%I Elsevier
%V 448
%P 227573
%8 2020-02
%D 2020
%R 10.1016/j.jpowsour.2019.227573
%K XPS
%K ToF-SIMS
%K Temperature
%K Li-ion batteries
%K LMO
%K LTO
%K SAM
%K SEI
%Z Chemical Sciences/Material chemistry
%Z Chemical Sciences/Analytical chemistryJournal articles
%X Reliable development of Li-ion Batteries requires a good understanding of the correlation between electrochemical performances and accurate aging interfaces phenomena. The present study focuses on the SEI characterization formed at both electrodes surface in LiMn2O4/Li4Ti5O12 (LMO/LTO) cells, depending on the cycling temperature, which is one of the major stress factors for batteries. LMO/LTO cells were cycled at 25 °C, 40 °C and 60 °C over 100 cycles and the chemical composition of surface layers was investigated by XPS, SAM and ToF-SIMS at the end of the 100th cycles. LTO electrodes are covered by surface layers since the first cycle (inducing an irreversible capacity loss) and the SEI thickness increases with the cycling temperature; moreover, organic (alkyl-carbonates, polyethylene oxides, oxalates) and inorganic species (LiF and (fluoro)-phosphates) of the solid interphase are present in different proportions depending on the temperature; more fluorophosphates are especially observed at 60 °C due to a higher chemical degradation of LiPF6 salt. Finally, small amounts of manganese, heterogeneously spread over the LTO electrode surface, are found at different oxidation states at higher temperatures; for the first time, metallic Mn0 was detected at the LTO surface after cycling at 60 °C which could explain the important capacity loss of the system at this temperature.
%G English
%L hal-02430603
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02430603
%~ UNIV-PAU
%~ IPREM
%~ INC-CNRS
%~ CNRS
%~ IPREM-PCM
%~ ANR

%0 Journal Article
%T Temperature effects on Li 4 Ti 5 O 12 electrode/electrolyte interfaces at the first cycle: A X-ray Photoelectron Spectroscopy and Scanning Auger Microscopy study
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ SAFT [Bordeaux]
%A Gieu, Jean-Baptiste
%A Courreges, Cecile
%A El Ouatani, L.
%A Tessier, C.
%A Martinez, Hervé
%Z Région Aquitaine and SAFT company
%< avec comité de lecture
%@ 0378-7753
%J Journal of Power Sources
%I Elsevier
%V 318
%P 291 - 301
%8 2016-06
%D 2016
%R 10.1016/j.jpowsour.2016.04.007
%K LTO
%K Temperature effect
%K SEI
%K XPS
%K SAM
%Z Chemical Sciences/Analytical chemistry
%Z Chemical Sciences/Material chemistryJournal articles
%X Li4Ti5O12-based negative electrodes for Lithium-ion batteries are of interest because of the high reversibility of Li+ insertion/extraction. In this study, the surface of cycled electrodes is analysed by X-ray Photoelectron Spectroscopy (XPS) and Scanning Auger Microscopy (SAM) to investigate the effects of cycling temperature (room temperature, 60 °C and 85 °C) upon the solid electrolyte interphase (SEI) formation, which plays a major role in batteries electrochemical performances. Half-cells, with a vinylene carbonate containing electrolyte, are galvanostatically cycled at different steps of the first cycle: the mid-plateau during the first discharge, the end of the first discharge at 1.2 V and the end of the first charge at 2.0 V. XPS analysis evidences that higher temperatures promote the formation of a thicker SEI, which can explain the increase of the irreversible capacity with temperature. SAM mappings (allowing high spatial resolution ∼10–100 nm) evidence that this SEI homogeneously covers the electrode surface, regardless of the cycling temperature. During charge, the SEI is partially dissolved at room temperature, more slightly at 60 °C whereas at 85 °C, no clear evidence of layer thinning is observed. The SEI chemical composition is also investigated and reveals a majority of organic species and an increasing proportion of LiF with the temperature.
%G English
%L hal-01622408
%U https://hal.archives-ouvertes.fr/hal-01622408
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ IPREM-PCM
%~ TESTUPPA

%0 Journal Article
%T Membrane Interacting Peptides: From Killers to Helpers
%+ Chimie et Biologie des Membranes et des Nanoobjets (CBMN)
%+ CBMN-IPB
%+ Laboratoire de Physico-Chimie Moléculaire (LPCM)
%A Dufourc, Erick
%A Buchoux, Sebastien
%A Toupe, Jeannot
%A Sani, Marc-Antoine
%A Jean-Francois, Frantz
%A Khemtémourian, Lucie
%A Grélard, Axelle
%A Loudet-Courrèges, Cécile
%A Laguerre, Michel
%A Elezgaray, Juan
%A Desbat, Bernard
%A Odaert, Benoit
%< avec comité de lecture
%J Current protein & peptide science
%V 13
%N 7
%P 620 - 631
%8 2012-11-01
%D 2012
%R 10.2174/138920312804142138
%Z Chemical Sciences/Analytical chemistry
%Z Chemical Sciences/Material chemistryJournal articles
%G English
%L hal-01622431
%U https://hal.archives-ouvertes.fr/hal-01622431
%~ CNRS
%~ INC-CNRS
%~ GEC-UPJV

