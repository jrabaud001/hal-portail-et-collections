%0 Journal Article
%T Fourteen Tests for Breton Collectives, an Inquiry on Number an Numerotisity
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Jouitteau, Mélanie
%A Rezac, Milan
%< avec comité de lecture
%@ 1273-3830
%J Lapurdum
%I Centre de recherche sur la langue et les textes basques IKER UMR 5478 CNRS
%S Mélanges En l'honneur de Xarles Videgain
%N 19
%P 357-389
%8 2016
%D 2016
%Z Humanities and Social Sciences/LinguisticsJournal articles
%G English
%L hal-02553882
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02553882
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS

%0 Journal Article
%T The Breton Inflectional Impersonal
%+ Centre de recherche sur la langue et les textes basques (IKER)
%A Jouitteau, Mélanie
%A Rezac, Milan
%Z Xulio Sousa, Carlota de Benito & Víctor Lara (éds.)
%< avec comité de lecture
%@ 2013-2247
%J Dialectologia
%I Publicacions i Edicions UB
%S Syntactic variation in western european languages, from the noun phrase to clause structure
%N 5
%P 261--292
%8 2015
%D 2015
%Z Humanities and Social Sciences/LinguisticsJournal articles
%X This article provides a first attempt of a syntactic characterization of the different Breton varieties spoken in the twenty-first century. Standard Breton is addressed as one of the modern dialects spoken in Brittany, and its syntax is compared with that of traditional varieties. I first establish a baseline and inventory the syntactic parameters that differentiate the traditional dialects from each other: Kerne, Leon, Goelo, Treger (KLT in the West) and Gwenedeg (South East). I show that a robust body of syntactic variation characterizes traditional dialects. I next compare these with the Standard variety that emerged during the twentieth century, and show that if Standard Breton has original features of its own, it varies less with respect to traditional varieties than do traditional varieties among themselves.
%G English
%L hal-02553895
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02553895
%~ UNIV-PAU
%~ IKER
%~ AO-LINGUISTIQUE
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ SHS

%0 Journal Article
%T Mihi est from Brythonic to Breton I
%+ Centre de recherche sur la langue et les textes basques (IKER)
%+ Centre National de la Recherche Scientifique (CNRS)
%A Rezac, Milan
%< avec comité de lecture
%@ 0019-7262
%J Indogermanische Forschungen
%I De Gruyter
%V 125
%N 1
%P 313-362
%8 2020-11-01
%D 2020
%R 10.1515/if-2020-013
%Z Humanities and Social Sciences/LinguisticsJournal articles
%G English
%L hal-03092404
%U https://hal.archives-ouvertes.fr/hal-03092404
%~ AO-LINGUISTIQUE
%~ UNIV-PAU
%~ UNIV-BORDEAUX-MONTAIGNE
%~ CNRS
%~ IKER
%~ SHS

%0 Journal Article
%T The syntax of eccentric agreement : The Person Case Constraint and Absolutive Displacement in Basque
%+ Structures Formelles du Langage (SFL)
%A Rezac, Milan
%< avec comité de lecture
%@ 0167-806X
%J Natural Language and Linguistic Theory
%I Springer Verlag
%P 61-106
%8 2008
%D 2008Journal articles
%G English
%L hal-01021276
%U https://hal-univ-paris8.archives-ouvertes.fr/hal-01021276
%~ UNIV-PARIS8
%~ CNRS
%~ SFLTAMP
%~ UNIV-PARIS-LUMIERES

%0 Journal Article
%T Escaping the Person Case Constraint: Referential computation in the φ-system
%+ Structures Formelles du Langage (SFL)
%A Rezac, Milan
%< avec comité de lecture
%@ 1568-1483
%J Linguistic Variation Yearbook
%I John Benjamins Publishing
%P 97-138
%8 2007
%D 2007Journal articles
%G English
%L hal-01021273
%U https://hal-univ-paris8.archives-ouvertes.fr/hal-01021273
%~ UNIV-PARIS8
%~ CNRS
%~ SFLTAMP
%~ UNIV-PARIS-LUMIERES

%0 Journal Article
%T Deriving the Complementarity Effect: Relativized Minimality in Breton agreement
%+ Structures Formelles du Langage (SFL)
%+ Laboratoire de Linguistique de Nantes (LLING)
%A Rezac, Milan
%A Jouitteau, Mélanie
%< avec comité de lecture
%@ 0024-3841
%J Lingua
%I Elsevier
%V 116
%N 11
%P 1915-1945
%8 2006
%D 2006
%R 10.1016/j.lingua.2005.03.012
%K Locality
%K Breton
%K Agreement
%Z Humanities and Social Sciences/LinguisticsJournal articles
%X Breton φ-agreement is characterized by the Complementarity Effect, which allows pro-dropped but not lexical DPs to control φ-agreement. We contrast verbal and prepositional systems: a lexical DP co-occurs with the root form of a preposition, but with a 3rd.sg. (frozen agreement) form of a verb. We argue that frozen agreement arises through φ-relativized locality: the Breton vP independently shows nominal properties, and thus intervenes for agreement. The φ-probe of T Agrees with the vP for 3rd.sg. rather than the vP-internal subject. In the prepositional system on the other hand, lexical DPs occur with bare stems and φ-inflection spells out affixed pronouns. The mechanics predict that in verbal constructions where the subject originates outside the vP, it is local enough to control the agreement of T, which correctly yields Have under a prepositional analysis as the sole verb immune to the Complementarity Effect. Finally, we propose a typology of Complementarity Effects in agreement depending on the interaction of intervention (frozen agreement) and syntactic incorporation past the intervener.
%G English
%L halshs-00605599
%U https://halshs.archives-ouvertes.fr/halshs-00605599
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-NANTES
%~ UNIV-PARIS-LUMIERES

%0 Journal Article
%T From mihi est to have across Breton dialects
%+ Structures Formelles du Langage (SFL)
%+ Laboratoire de Linguistique de Nantes (LLING)
%A Rezac, Milan
%A Jouitteau, Mélanie
%< avec comité de lecture
%J Rivista di Grammatica Generativa
%V 33
%P 161-178
%8 2008
%D 2008
%K have
%K diachronic syntax
%K Breton
%K agreement
%Z Humanities and Social Sciences/LinguisticsJournal articles
%X We address the syntax and parametric variation underlying the verb 'have' from the empirical domain of Breton diachronic and dialectal variation. We introduce the Celtic agreement system characterized by Complementarity Effects and show why Breton 'have' is special among the Celtic language and at the same time unique among Breton verbs. We propose syntactic tests for decomposing the structure of 'have'. With these tools in hand, we show that 'have' across Breton dialects appears to trace an ordered path in UG parameter space from a strictly analytic mihi est to a fully lexicalized 'have'. We develop the predictions our proposal makes internally to each dialect, and list predictions to be checked in future data collection.
%G English
%L halshs-00605595
%U https://halshs.archives-ouvertes.fr/halshs-00605595
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-NANTES
%~ UNIV-PARIS-LUMIERES

%0 Journal Article
%T Escaping the Person Case Constraint: Referential computation in the φ-system
%+ Structures Formelles du Langage (SFL)
%A Rezac, Milan
%< avec comité de lecture
%@ 1568-1483
%J Linguistic Variation Yearbook
%I John Benjamins Publishing
%V 6
%P 97-138
%8 2007
%D 2007
%R 10.1075/livy.6.06rez
%K Basque
%K Finnish
%K French
%K Georgian
%K Person Case Constraint
%K reference set computation
%Z Humanities and Social Sciences/LinguisticsJournal articles
%X The Person Case Constraint (PCC) blocks a 1st/2nd person DP from Agree and Case assignment if it is separated from a probe by an intervener. I examine four separate strategies that circumvent the PCC: through giving the blocked DP case and agreement that would not otherwise be possible (absolutive displacement Basque; Jahnsson's Rule in Finnish), by realizing the intervener elsewhere (3 to 5 Demotion in French), or by realizing the DP's person features differently (Object Camouflage in Georgian). The striking feature these strategies share is that they are restricted to PCC contexts and not freely available. This makes it impossible to view them as paraphrase. Stating the conditions on their distribution requires reference to the failed PCC derivation, that is trans-derivational comparison. I extend the reference set computation of Fox (1995, 2000) and Reinhart (1995, 1999) to account for these strategies as the addition of a φ-probe, and suggest an extension to dependent Case.
%G English
%L halshs-00605597
%U https://halshs.archives-ouvertes.fr/halshs-00605597
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-PARIS-LUMIERES

%0 Journal Article
%T Agreement restrictions (review)
%+ Structures Formelles du Langage (SFL)
%A Rezac, Milan
%< avec comité de lecture
%@ 0097-8507
%J Language
%I Linguistic Society of America
%V 86
%N 4
%P 948-953
%8 2010
%D 2010
%K review
%Z Humanities and Social Sciences/LinguisticsJournal articles
%G English
%L halshs-00605602
%U https://halshs.archives-ouvertes.fr/halshs-00605602
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-PARIS-LUMIERES

%0 Journal Article
%T The fine structure of cyclic Agree
%+ Structures Formelles du Langage (SFL)
%A Rezac, Milan
%< avec comité de lecture
%@ 1368-0005
%J Syntax
%I Wiley-Blackwell
%V 6
%N 2
%P 156-182
%8 2003
%D 2003
%R 10.1111/1467-9612.00059
%K Agree
%K cyclicity
%K Basque
%K ergative
%Z Humanities and Social Sciences/LinguisticsJournal articles
%X This paper argues for an implementation of cyclicity through a natural economy condition, the Earliness Principle (EP), which requires a feature to be eliminated as early as possible. EP predicts that the search space for feature checking should increase throughout the derivation with the growth of the phrase marker. This prediction is shown to be manifested in "agreement displacement," where agreement morphology under certain conditions cross-references a noncanonical argument. Agreement displacement shows that (phiv-features of a head may seek a DP outside the complement of that head, after the cycle on which search exhausts the complement, as predicted by EP. A detailed look at Basque ergative displacement, which differentially affects person and number, leads to the conclusion that agreement may happen only once per interpretable feature. The blocking of multiple Agree relationships is implemented as a locality effect, based on the construal of Case as a functional category introduced by Agree.
%G English
%L halshs-00605601
%U https://halshs.archives-ouvertes.fr/halshs-00605601
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-PARIS-LUMIERES

%0 Journal Article
%T The syntax of eccentric agreement: The Person Case Constraint and Absolutive Displacement in Basque
%+ Structures Formelles du Langage (SFL)
%A Rezac, Milan
%< avec comité de lecture
%J Natural Language & Linguistic Theory
%V 26
%N 1
%P 61-106
%8 2008
%D 2008
%R 10.1007/s11049-008-9032-6
%K Agreement
%K Person Case Constraint
%K Ergativity
%K Case
%Z Humanities and Social Sciences/LinguisticsJournal articles
%X This article explores a syntactic approach to the Person Case Constraint, a ban on 1st/2nd person agreement caused by a dative. The approach proposes that the constraint is due to the interference in person Agree of a head H and its expected controller α by a dative between the two (H > DAT > α, where > is c-command). This predicts that it is absent if the dative does not intervene (α > DAT), or if α moves past the dative (α > DAT > t α). Both predictions are correct. The latter is developed at length from Basque "absolutive displacement" and Icelandic "long raising", which show the predicted repair of the constraint by movement, through anomalous ergative morphology and overt displacement respectively. A further correct consequence is that the constraint is repaired undetectably in the unaccusatives of accusative languages, except when movement past the dative is unavailable. Morphology does not provide the right tools, since it collapses the required structural distinctions, and the saving effect of movement on agreement is unpredicted. Finally, an independent argument is developed to show that the Person Case Constraint is visible to "narrow syntax".
%G English
%L halshs-00605593
%U https://halshs.archives-ouvertes.fr/halshs-00605593
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-PARIS-LUMIERES

%0 Journal Article
%T The interaction of Th/Ex and locative inversion
%+ Structures Formelles du Langage (SFL)
%A Rezac, Milan
%< avec comité de lecture
%@ 0024-3892
%J Linguistic Inquiry
%I Massachusetts Institute of Technology Press (MIT Press)
%V 37
%N 4
%P 685-697
%8 2006
%D 2006
%K expletive constructions
%K Locative Inversion
%Z Humanities and Social Sciences/LinguisticsJournal articles
%G English
%L halshs-00605598
%U https://halshs.archives-ouvertes.fr/halshs-00605598
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-PARIS-LUMIERES

%0 Journal Article
%T ɸ-Agree Versus ɸ-Feature Movement: Evidence From Floating Quantifiers
%+ Structures Formelles du Langage (SFL)
%A Rezac, Milan
%< avec comité de lecture
%@ 0024-3892
%J Linguistic Inquiry
%I Massachusetts Institute of Technology Press (MIT Press)
%P 496-508
%8 2010
%D 2010Journal articles
%G English
%L hal-01021283
%U https://hal-univ-paris8.archives-ouvertes.fr/hal-01021283
%~ UNIV-PARIS8
%~ CNRS
%~ SFLTAMP
%~ UNIV-PARIS-LUMIERES

%0 Journal Article
%T Phi-Agree vs. movement: Evidence from floating quantifiers.
%+ Structures Formelles du Langage (SFL)
%A Rezac, Milan
%< avec comité de lecture
%@ 0024-3892
%J Linguistic Inquiry
%I Massachusetts Institute of Technology Press (MIT Press)
%V 41
%N 3
%P 496-508
%8 2010
%D 2010
%K phi-Agree
%K floating quantifiers
%Z Humanities and Social Sciences/LinguisticsJournal articles
%G English
%L halshs-00605590
%U https://halshs.archives-ouvertes.fr/halshs-00605590
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-PARIS-LUMIERES

%0 Journal Article
%T Cyclic Agree
%+ Structures Formelles du Langage (SFL)
%A Rezac, Milan
%A Susana, Béjar
%< avec comité de lecture
%@ 0024-3892
%J Linguistic Inquiry
%I Massachusetts Institute of Technology Press (MIT Press)
%V 41
%N 1
%P 45-73
%8 2009
%D 2009
%R 10.1162/ling.2009.40.1.35
%K Agree
%K person hierarchies
%K agreement displacement
%K features
%K cyclicity
%Z Humanities and Social Sciences/LinguisticsJournal articles
%X We propose that agreement displacement phenomena sensitive to person hierarchies arise from the mechanism of Agree operating on articulated -feature structures in a cyclic syntax. Cyclicity and locality derive a preference for agreement control by the internal argument. Articulation of the probe determines (a) when the agreement controller cyclically displaces to the external argument and (b) differences in crosslinguistic sensitivity to person hierarchies. The system characterizes two classes of derivations corresponding empirically to direct and inverse contexts, and predicts the existence and nature of repair strategies in the latter. The properties of agreement displacement thus reduce to properties of syntactic dependency formation by Agree.
%G French
%L halshs-00605592
%U https://halshs.archives-ouvertes.fr/halshs-00605592
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-PARIS-LUMIERES

%0 Journal Article
%T Deriving Complementarity Effects
%T Relativized Minimality in Breton agreement
%+ Laboratoire de Linguistique de Nantes (LLING)
%+ Structures Formelles du Langage (SFL)
%A Jouitteau, Mélanie
%A Rezac, Milan
%< avec comité de lecture
%@ 0024-3841
%J Lingua
%I Elsevier
%V 116
%P 1915-1945
%8 2006
%D 2006
%K avoir
%K incorporation
%K clitiques
%K être
%K accord verbal
%K agreement
%Z Humanities and Social Sciences/LinguisticsJournal articles
%G English
%L hal-00605656
%U https://hal.archives-ouvertes.fr/hal-00605656
%~ SHS
%~ CNRS
%~ UNIV-PARIS8
%~ SFLTAMP
%~ AO-LINGUISTIQUE
%~ UNIV-NANTES
%~ UNIV-PARIS-LUMIERES

