%0 Journal Article
%T Bank capital regulation: are local or central regulators better?
%+ Centre d'Analyse Théorique et de Traitement des données économiques (CATT)
%+ Laboratoire d'Analyse et de Prospective Economique (LAPE)
%+ University of Birmingham [Birmingham]
%A Haritchabalet, Carole
%A Lepetit, Laetitia
%A Spinassou, Kévin
%A Strobel, Frank
%< avec comité de lecture
%@ 1042-4431
%J Journal of International Financial Markets, Institutions and Money
%I Elsevier
%V 49
%P 103-114
%8 2017-07
%D 2017
%K Bank regulation
%K Financial architecture
%K Capital requirement
%K Spillover
%K Regulatory capture
%Z Humanities and Social Sciences/Economics and FinanceJournal articles
%X Using a simple two-region model where local or central regulators set bank capital requirements as risk sensitive capital or leverage ratios, we demonstrate the importance of capital requirements being set centrally when cross-region spillovers are large and local regulators suffer from substantial regulatory capture. We show that local regulators may want to surrender regulatory power only when spillover effects are large but the degree of supervisory capture is relatively small, and that bank capital regulation at central rather than local levels is more beneficial the larger the impact of systemic risk and the more asymmetric is regulatory capture at the local level.
%G English
%L hal-01486546
%U https://hal-unilim.archives-ouvertes.fr/hal-01486546
%~ UNILIM
%~ UNIV-PAU
%~ AO-ECONOMIE
%~ SHS
%~ LAPE
%~ CATT
%~ TESTUPPA

