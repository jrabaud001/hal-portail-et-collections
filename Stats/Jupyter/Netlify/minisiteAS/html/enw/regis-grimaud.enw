%0 Journal Article
%T Cytoplasmic wax ester accumulation during biofilm-driven substrate assimilation at the alkanewater interface by Marinobacter hydrocarbonoclasticus SP17.
%+ PaleoEnvironnements et PaleobioSphere (PEPS)
%A Klein, B.
%A Grossi, Vincent
%A Bouriat, Patrick
%A Goulas, P.
%A Grimaud, R.
%< avec comité de lecture
%@ 0923-2508
%J Research in Microbiology
%I Elsevier
%V 159
%P 137-144
%8 2008
%D 2008
%Z Sciences of the Universe [physics]/Earth Sciences/Geochemistry
%Z Environmental Sciences/Global Changes
%Z Sciences of the Universe [physics]/Earth Sciences/Oceanography
%Z Sciences of the Universe [physics]/Ocean, Atmosphere
%Z Sciences of the Universe [physics]/Continental interfaces, environment
%Z Life Sciences [q-bio]/Microbiology and Parasitology/Bacteriology
%Z Life Sciences [q-bio]/Ecology, environment/Ecosystems
%Z Environmental Sciences/Biodiversity and EcologyJournal articles
%G English
%L hal-00336027
%U https://hal.archives-ouvertes.fr/hal-00336027
%~ INSU
%~ SDE
%~ CNRS
%~ LGL-TPE
%~ GIP-BE
%~ UNIV-LYON1
%~ UDL
%~ UNIV-LYON

%0 Journal Article
%T Biofilm formation as a microbial strategy to assimilate particulate substrates
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Sivadon, Pierre
%A Barnier, Claudie
%A Urios, Laurent
%A Grimaud, Régis
%Z Conseil Général des Pyrénées Atlantique
%< avec comité de lecture
%@ 1758-2229
%J Environmental Microbiology Reports
%I Wiley
%8 2019-08-09
%D 2019
%R 10.1111/1758-2229.12785
%Z Chemical Sciences/Analytical chemistry
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Material chemistry
%Z Life Sciences [q-bio]/Biochemistry, Molecular Biology/Molecular biology
%Z Life Sciences [q-bio]/Biotechnology
%Z Life Sciences [q-bio]/Microbiology and Parasitology/Bacteriology
%Z Life Sciences [q-bio]/Ecology, environment/SymbiosisJournal articles
%X In most ecosystems, a large part of the organic carbon is not solubilized in the water phase. Rather, it occurs as particles made of aggregated hydrophobic and/or polymeric natural or man‐made organic compounds. These particulate substrates are degraded by extracellular digestion/solubilization implemented by heterotrophic bacteria that form biofilms on them. Organic particle‐degrading biofilms are widespread and have been observed in aquatic and terrestrial natural ecosystems, in polluted and man‐driven environments and in the digestive tracts of animals. They have central ecological functions as they are major players in carbon recycling and pollution removal. The aim of this review is to highlight bacterial adhesion and biofilm formation as central mechanisms to exploit the nutritive potential of organic particles. It focuses on the mechanisms that allow access and assimilation of non‐dissolved organic carbon, and considers the advantage provided by biofilms for gaining a net benefit from feeding on particulate substrates. Cooperative and competitive interactions taking place in biofilms feeding on particulate substrates are also discussed.
%G English
%L hal-02289355
%U https://hal.archives-ouvertes.fr/hal-02289355
%~ UNIV-PAU
%~ IPREM
%~ CNRS
%~ INC-CNRS
%~ GIP-BE
%~ IPREM-CME

%0 Journal Article
%T Bacteriophage-Mu Repressor - a Target Protein for the Escherichia-Coli Atp-Dependent Clp Protease
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Grimaud, Régis
%< avec comité de lecture
%@ 0730-2312
%J Journal of Cellular Biochemistry
%I Wiley
%P 119-119
%8 1995-01-01
%D 1995
%Z Life Sciences [q-bio]/Microbiology and Parasitology/Bacteriology
%Z Life Sciences [q-bio]/Biochemistry, Molecular BiologyJournal articles
%G English
%L hal-01152468
%U https://hal.archives-ouvertes.fr/hal-01152468
%~ CNRS
%~ UNIV-PAU
%~ IPREM
%~ INC-CNRS
%~ TESTUPPA

%0 Journal Article
%T Biosynthesis, purification and analysis of selenomethionyl calmodulin by gel electrophoresis-laser ablation-ICP-MS and capillary HPLC-ICP-MS peptide mapping following in-gel tryptic digestion
%+ Institut Pluridisciplinaire de Recherche sur l'Environnement et les Matériaux (IPREM)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Donard, O.
%A Grimaud, Régis
%A Lobinski, Ryszard
%A Ballihaut, G.
%A Tastet, L.
%A Pécheyran, Christophe
%A Bouyssière, Brice
%< avec comité de lecture
%@ 0267-9477
%J Journal of Analytical Atomic Spectrometry
%I Royal Society of Chemistry
%V 20
%N 6
%P 493--499
%8 2005
%D 2005
%R 10.1039/b500719d
%Z Chemical Sciences/Analytical chemistryJournal articles
%X A biosynthesis method was developed to produce a standard of a selenium-containing protein. It consisted of the expression of calmodulin in Escherichia coli culture in the presence of selenomethionine, which allowed the replacement of all methionine residues by selenomethionine. The resulting 17 kDa protein containing 8 selenomethionine residues was purified by two-step hydrophobic interaction chromatography. The selenomethionyl calmodulin was subsequently used to develop a method for the characterization of selenium-containing proteins (detected in the polyacrylamide gel by laser ablation-ICP-MS) by means of peptide mapping using capillary HPLC-ICP-MS. The monitoring of the 80Se isotope using an ICP mass spectrometer equipped with a collision cell allowed as little as 0.3 pg as Se (1.3 ng ml -1 in the analysed solution) to be detected in the gel. The band containing the protein of interest was excised, the protein was digested with trypsin and the Se-containing peptides were analyzed by capillary HPLC-ICP-MS. The sensitivity of the method was at least a factor of 5 higher than that of capillary LC-electrospray MS/MS in similar conditions. Some of the selenopeptides detected by capillary LC-ICP MS could nevertheless be identified by retention time matching using a set of peptides generated by trypsin digestion from the concentrated selenomethionyl calmodulin standard. © The Royal Society of Chemistry 2005.
%G English
%L hal-01560781
%U https://hal.archives-ouvertes.fr/hal-01560781
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA

%0 Journal Article
%T Description of Palleronia rufa sp. nov., a biofilm-forming and AHL-producing Rhodobacteraceae, reclassification of Hwanghaeicola aestuarii as Palleronia aestuarii comb. nov., Maribius pontilimi as Palleronia pontilimi comb. nov., Maribius salinus as Palleronia salina comb. nov., Maribius pelagius as Palleronia pelagia comb. nov. and emended description of the genus Palleronia
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Centre de recherches insulaires et observatoire de l'environnement (CRIOBE)
%+ Laboratoire de Biodiversité et Biotechnologies Microbiennes (LBBM)
%+ Observatoire océanologique de Banyuls (OOB)
%A Barnier, Claudie
%A Clerissi, Camille
%A Lami, Raphaël
%A Intertaglia, Laurent
%A Lebaron, Philippe
%A Grimaud, Régis
%A Urios, Laurent
%Z BIOPIC platform
%Z Conseil Général des Pyrénées-Atlantiques
%< avec comité de lecture
%@ 0723-2020
%J Systematic and Applied Microbiology
%I Elsevier
%V 43
%N 1
%P 126018
%8 2020
%D 2020
%R 10.1016/j.syapm.2019.126018
%K Roseobacter group
%K Rhodobacteraceae
%K Maribius
%K Biofilm
%K Palleronia
%K Quorum sensing
%Z Life Sciences [q-bio]/Biodiversity/Systematics, Phylogenetics and taxonomy
%Z Environmental Sciences/Biodiversity and EcologyJournal articles
%X Strain MOLA 401T was isolated from marine waters in the southwest lagoon of New Caledonia and was shown previously to produce an unusual diversity of quorum sensing signaling molecules. This strain was Gram-negative, formed non-motile cocci and colonies were caramel. Optimum growth conditions were 30 °C, pH 8 and 3% NaCl (w/v). Based on 16S rRNA gene sequence analysis, this strain was found to be closely related to Pseudomaribius aestuariivivens NBRC 113039T (96.9% of similarity), Maribius pontilimi DSM 104950T (96.4% of similarity) and Palleronia marisminoris LMG 22959T (96.3% of similarity), belonging to the Roseobacter group within the family Rhodobacteraceae. As its closest relatives, strain MOLA 401T is able to form a biofilm on polystyrene, supporting the view of Roseobacter group strains as prolific surface colonizers. An in-depth genomic study allowed us to affiliate strain MOLA 401T as a new species of genus Palleronia and to reaffiliate some of its closest relatives in this genus. Consequently, we describe strain MOLA 401T (DSM 106827T = CIP 111607T = BBCC 401T) for which we propose the name Palleronia rufa sp. nov. We also propose to emend the description of the genus Palleronia and to reclassify Maribius and Hwanghaeicola species as Palleronia species
%G English
%L hal-02390411
%U https://hal.archives-ouvertes.fr/hal-02390411
%~ IPREM
%~ UNIV-PAU
%~ SORBONNE-UNIV
%~ UNIV-PERP
%~ AGROPOLIS
%~ INC-CNRS
%~ EDF
%~ SORBONNE-UNIVERSITE
%~ LBBM
%~ SU-SCI
%~ UMS-2348
%~ CNRS
%~ SDE
%~ GIP-BE
%~ CRIOBE
%~ EPHE
%~ TEST-DEV
%~ SU-SCIENCES
%~ EPHE-PSL
%~ PSL
%~ IPREM-CME

%0 Journal Article
%T Bacteriophage Mu repressor as a target for the Escherichia coli ATP-dependent Clp protease
%+ National Cancer Institute [Bethesda] (NCI-NIH)
%+ Département de mathématiques Université Libre de Bruxelles
%A Laachouch, J.E.
%A Desmet, L.
%A Geuskens, V.
%A Grimaud, R.
%Z cited By 46
%< avec comité de lecture
%@ 0261-4189
%J EMBO Journal
%I EMBO Press
%V 15
%N 2
%P 437-444
%8 1996
%D 1996
%R 10.1002/j.1460-2075.1996.tb00374.x/full
%Z Life Sciences [q-bio]/Biochemistry, Molecular Biology/Biomolecules [q-bio.BM]
%Z Life Sciences [q-bio]/Microbiology and Parasitology/BacteriologyJournal articles
%X Bacteriophage Mu repressor, which is stable in its wildtype form, can mutate to become sensitive to its Escherichia coli host ATP-dependent ClpXP protease. We further investigated the determinants of the mutant repressor's sensitivity to Clp. We show the crucial importance of a C-terminal, seven amino acid long sequence in which a single change is sufficient to decrease the rate of degradation of the protein. The sequence was fused at the C-terminal end of the CcdB and CcdA proteins encoded by plasmid F. CcdB, which is naturally stable, was unaffected, while CcdA, which is normally degraded by the Lon protease, became a substrate for ClpXP while remaining a substrate for Lon. In agreement with the current hypothesis on the mechanism of recognition of their substrates by energy- dependent proteases, these results support the existence, on the substrate polypeptides, of separate motifs responsible for recognition and cleavage by the protease.
%G English
%L hal-01631824
%U https://hal.archives-ouvertes.fr/hal-01631824

%0 Journal Article
%T Cytoplasmic wax ester accumulation during biofilm-driven substrate assimilation at the alkane–water interface by Marinobacter hydrocarbonoclasticus SP17
%+ Institut Pluridisciplinaire de Recherche sur l'Environnement et les Matériaux (IPREM)
%+ PaleoEnvironnements et PaleobioSphere (PEPS)
%+ Thermodynamique et Energétique des fluides complexes (TEFC)
%A Klein, Benjamin
%A Grossi, Vincent
%A Bouriat, Patrick
%A Goulas, Philippe
%A Grimaud, Régis
%< avec comité de lecture
%@ 0923-2508
%J Research in Microbiology
%I Elsevier
%V 159
%N 2
%P 137-144
%8 2007-11-13
%D 2007
%R 10.1016/j.resmic.2007.11.013
%K Alkane degradation
%K Wax esters
%K Biofilm
%K Marinobacter hydrocarbonoclasticus
%K Alkane uptake
%Z Physics [physics]/Physics [physics]/Chemical Physics [physics.chem-ph]Journal articles
%X During growth on n-alkanes, the marine bacterium Marinobacter hydrocarbonoclasticus SP17 formed a biofilm at the alkane–water interface. We showed that hexadecane degradation was correlated with biofilm development and that alkane uptake is localized in the biofilm but not in the bulk medium. Biofilms were observed in cultures on metabolizable n-alkanes (C8–C28) and n-alcohols (C12 and C16), but were formed neither on non-metabolizable alkanes (pristane, heptamethylnonane and n-C32) nor on inert substrata (glass, polystyrene and Permanox®). This substratum specificity indicates that biofilm formation is determined by the presence of an interface between an insoluble substrate and the aqueous phase. Simultaneously with biofilm growth, planktonic cells were released from the biofilm. Detached cells were in a non-growing state, implying that the growing population was exclusively located within the biofilm. Planktonic and sessile cells exhibited differences in their ultrastructure and lipid content. Biofilm cells contained a large amount of wax esters (0.47 mg/mg protein) in rounded or irregularly shaped cytoplasmic inclusions, whereas detached cells displayed rod-shaped inclusions and contained 5 times fewer wax esters (0.10 mg/mg protein) than their sessile counterparts. This study points out the inter-relationship between biofilm formation, insoluble substrate uptake and lipid storage.
%G English
%L hal-00340868
%U https://hal.archives-ouvertes.fr/hal-00340868
%~ INSU
%~ CNRS
%~ UNIV-PAU
%~ LGL-TPE
%~ IPREM
%~ INC-CNRS
%~ UNIV-LYON1
%~ UDL
%~ UNIV-LYON
%~ LFCR
%~ LFCR-ISD
%~ TESTUPPA

%0 Journal Article
%T Cells dispersed from Marinobacter hydrocarbonoclasticus SP17 biofilm exhibit a specific protein profile associated with a higher ability to reinitiate biofilm development at the hexadecane-water interface
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Vaysse, P.-J.
%A Sivadon, Pierre
%A Goulas, Philippe
%A Grimaud, Régis
%Z cited By 14
%< avec comité de lecture
%@ 1462-2912
%J Environmental Microbiology
%I Society for Applied Microbiology and Wiley-Blackwell
%V 13
%N 3
%P 737-746
%8 2011
%D 2011
%R 10.1111/j.1462-2920.2010.02377.x
%Z Life Sciences [q-bio]/Biochemistry, Molecular Biology/Genomics [q-bio.GN]Journal articles
%X Biofilm formation by marine hydrocarbonoclastic bacteria is commonly observed and has been recognized as an important mechanism for the biodegradation of hydrocarbons. In order to colonize new oil-water interfaces, surface-attached communities of hydrocarbonoclastic bacteria must release cells into the environment. Here we explored the physiology of cells freshly dispersed from a biofilm of Marinobacter hydrocarbonoclasticus developing at the hexadecane-water interface, by combining proteomic and physiological approaches. The comparison of the dispersed cells' proteome with those of biofilm, logarithmic- and stationary-phase planktonic cells indicated that dispersed cells had lost most of the biofilm phenotype and expressed a specific proteome. Two proteins involved in cell envelope maturation, DsbA and CtpA, were exclusively detected in dispersed cells, suggesting a reshaping of the cell envelopes during biofilm dispersal. Furthermore, dispersed cells exhibited a higher affinity for hexadecane and initiated more rapidly biofilm formation on hexadecane than the reference planktonic cells. Interestingly, storage wax esters were rapidly degraded in dispersed cells, suggesting that their observed physiological properties may rely on reserve mobilization. Thus, by promoting oil surface colonization, cells emigrating from the biofilm could contribute to the success of marine hydrocarbonoclastic bacteria in polluted environments. © 2010 Society for Applied Microbiology and Blackwell Publishing Ltd.
%G English
%L hal-01565044
%U https://hal.archives-ouvertes.fr/hal-01565044
%~ CNRS
%~ UNIV-PAU
%~ IPREM
%~ INC-CNRS

%0 Journal Article
%T Degradation of the "Erika" oil
%+ Laboratoire de Physico -& Toxico Chimie des systèmes naturels (LPTC)
%A Bordenave, S.
%A Jézéquel, R.
%A Fourçans, A.
%A Budzinski, H.
%A Merlin, F.X.
%A Fourel, T.
%A Goñi-Urriza, M.
%A Guyonaud, R.
%A Grimaud, R.
%A Caumette, P.
%A Duran, R.
%< avec comité de lecture
%@ 0990-7440
%J Aquatic Living Resources
%I EDP Sciences
%V 17
%P 261-267
%8 2004
%D 2004Journal articles
%G English
%L hal-00022642
%U https://hal.archives-ouvertes.fr/hal-00022642
%~ CNRS
%~ UNIV-PAU
%~ IPREM

%0 Journal Article
%T Multimode detection (LA-ICP-MS, MALDI-MS and nanoHPLC-ESI-MS2) in 1D and 2D gel electrophoresis for selenium-containing proteins
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Ballihaut, G.
%A Pecheyran, C.
%A Mounicou, S.
%A Preud'Homme, Hugues
%A Grimaud, R.
%A Lobinski, Ryszard
%Z cited By 34
%< avec comité de lecture
%@ 0165-9936
%J Trends in Analytical Chemistry
%I Elsevier
%V 26
%N 3
%P 183-190
%8 2007
%D 2007
%R 10.1016/j.trac.2007.01.007
%Z Chemical Sciences/Analytical chemistryJournal articles
%X Speciation of heteroelement-containing proteins essential for human health is one of the most rapidly developing areas in bio-inorganic analytical chemistry (metallomics). We discuss recent advances in mass spectrometry (MS) techniques for detection and identification of selenium (Se)-containing proteins separated by gel electrophoresis (GE). These advances include location of Se-containing bands in 1D GE or spots in 2D GE by laser-ablation inductively coupled plasma MS (LA-ICP-MS) followed by protein identification by matrix-assisted laser-desorption/ionization MS (MALDI-MS) and nano-high-performance liquid chromatography-electrospray-tandem MS (nanoHPLC-ESI-MS2). We highlight the differences with the classical proteomics approach resulting from the presence of the Se atom. © 2007 Elsevier Ltd. All rights reserved.
%G English
%L hal-01560354
%U https://hal.archives-ouvertes.fr/hal-01560354
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS

%0 Journal Article
%T Analysis of the adaptation to alkanes of the marine bacterium Marinobacter hydrocarbonoclasticus sp 17 by two dimensional gel electrophoresis
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Ballihaut, G.
%A Klein, Benjamin
%A Goulas, P.
%A Duran, R.
%A Caumette, Pierre
%A Grimaud, Regis.
%< avec comité de lecture
%@ 0990-7440
%J Aquatic Living Resources
%I EDP Sciences
%V 17
%N 3
%P 269-272
%8 2004
%D 2004
%R 10.1051/alr:2004030
%Z Chemical SciencesJournal articles
%X To better understand the molecular mechanisms involved in the biodegradation of hydrocarbon compounds from the “Erika” oil-spill, we have studied the ability of Marinobacter hydrocarbonoclasticus strain sp 17 to cope with hexadecane as sole carbon and energy source. Growth kinetics of cultures shifted from acetate to hexadecane revealed the presence of a 20 hours adaptation phase. Changes in global protein expression in response to hexadecane was analyzed by two-dimensional gel electrophoresis. Of the 370 proteins detected 42 had their expression level altered in presence of hexadecane indicating that alkane adaptation may involve many cellular functions.
%G English
%L hal-01841792
%U https://hal.archives-ouvertes.fr/hal-01841792
%~ IPREM
%~ UNIV-PAU
%~ CNRS
%~ INC-CNRS

%0 Journal Article
%T Behavior of Marinobacter hydrocarbonoclasticus SP17 cells during initiation of biofilm formation at the alkane-water interface
%+ Laboratoire de microbiologie des environnements extrêmophiles (LM2E)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Thermodynamique et Energétique des fluides complexes (TEFC)
%A Klein, Benjamin
%A Bouriat, Patrick
%A Goulas, Philippe
%A Grimaud, Régis
%< avec comité de lecture
%@ 0006-3592
%J Biotechnology and Bioengineering
%I Wiley
%V 105
%N 3
%P 461-468
%8 2010-02-15
%D 2010
%R 10.1002/bit.22577
%K Rheology
%K Marinobacter hydrocarbonoclasticus
%K Biofilm
%K Biosurfactant
%K Interfacial tension
%K Alkanes
%Z Physics [physics]/Physics [physics]/Chemical Physics [physics.chem-ph]Journal articles
%X Hexadecane assimilation by Marinobacter hydrocarbonoclasticus SP17 occurs through the formation of a biofilm at the alkane-water interface. In this study we focused on the interactions of cells with the alkane-water interface occurring during initiation of biofilm development. The behavior of cells at the interface was apprehended by investigating alterations of the mechanical properties of the interface during cell adsorption, using dynamic drop tensiometry measurements. It was found that after having reached the hexadecane-water interface, by a purely thermal diffusion process, cells released surface-active compounds (SACs) resulting in the formation of an interfacial viscoelastic film. Release of SACs was an active process requiring protein synthesis. This initial interaction occurred on metabolizable as well as non-metabolizable alkanes, indicating that at this stage cells are not affected by the nature of the alkane forming the interface. In contrast, at a later stage, the nature of the interface turned out to exert control over the behavior of the cells. The availability of a metabolizable alkane at the interface influenced cell activity, as revealed by cell cluster formation and differences in the interfacial elasticity.
%G English
%L hal-00682162
%U https://hal.archives-ouvertes.fr/hal-00682162
%~ CNRS
%~ UNIV-PAU
%~ IUEM
%~ LM2E_UBO
%~ UNIV-BREST
%~ IPREM
%~ INC-CNRS
%~ IFREMER
%~ THESES_IUEM
%~ CMM
%~ LFCR
%~ LFCR-ISD

%0 Journal Article
%T Bacteriophage Mu head assembly
%+ Institut Pluridisciplinaire de Recherche sur l'Environnement et les Matériaux (IPREM)
%A Grimaud, Régis
%Z cited By 12
%< avec comité de lecture
%@ 0042-6822
%J Virology
%I Elsevier
%V 217
%N 1
%P 200-210
%8 1996
%D 1996
%R 10.1006/viro.1996.0107
%K virus protein
%K virus assembly
%K virus mutant
%K Enterobacteria phage Mu
%Z Life Sciences [q-bio]Journal articles
%X The protein composition of defective particles produced by various bacteriophage Mu head-gene mutants was analyzed by SDS-PAGE. An abundant 20-kDa protein was detected in only one type of defective head. This protein exhibits properties of a scaffolding protein. A 50-kDa structural protein present in most defective heads was shown to be produced by cleavage of the C-terminus of the 64-kDa polypeptide encoded by gene H. Cleavage occurs during head assembly at a site which, according to earlier results, might separate two different functional domains in gpH. A fraction of the gpH molecules produced upon Mu induction sediment in a 25 S complex, suggesting that gpH participates in the formation of an early intermediate of Mu head assembly. Characteristics of gpH suggest that it may be the Mu portal protein.
%G English
%L hal-01614795
%U https://hal.archives-ouvertes.fr/hal-01614795
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ TESTUPPA

%0 Journal Article
%T The extracellular matrix of the oleolytic biofilms of Marinobacter hydrocarbonoclasticus comprises cytoplasmic proteins and T2SS effectors that promote growth on hydrocarbons and lipids.
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ MICrobiologie de l'ALImentation au Service de la Santé  (MICALIS)
%+ Centre Génomique Fonctionnelle Bordeaux [Bordeaux] (CGFB)
%+ Faculté des Sciences de Bizerte
%A Ennouri, Habiba
%A D'Abzac, Paul
%A Hakil, Florence
%A Branchu, Priscilla
%A Naitali, Murielle
%A Lomenech, Anne-Marie
%A Oueslati, Ridha
%A Desbrieres, Jacques
%A Sivadon, Pierre
%A Grimaud, Regis.
%Z French National Research Agency ANR [11 BSV7 002 01]; Tunisian Ministry of Higher Education and Scientific Research
%< avec comité de lecture
%@ 1462-2912
%J Environmental Microbiology
%I Society for Applied Microbiology and Wiley-Blackwell
%V 19
%P 159--173
%8 2017
%D 2017
%R 10.1111/1462-2920.13547
%M 27727521
%K Marinobacter biofilm extracellular matrix cytoplasmic protein T2SS effector
%Z Chemical Sciences/Analytical chemistryJournal articles
%X Summary : The assimilation of the nearly water insol. substrates hydrocarbons and lipids by bacteria entails specific adaptations such as the formation of oleolytic biofilms. The present article reports that the extracellular matrix of an oleolytic biofilm formed by Marinobacter hydrocarbonoclasticus at n-hexadecane-water interfaces is largely composed of proteins typically cytoplasmic such as translation factors and chaperones, and a lesser amt. of proteins of unknown function that are predicted extra-cytoplasmic. Matrix proteins appear to form a structured film on hydrophobic interfaces and were found mandatory for the development of biofilms on lipids, alkanes and polystyrene. Exo-proteins secreted through the type-2 secretion system (T2SS) were shown to be essential for the formation of oleolytic biofilms on both alkanes and triglycerides. The T2SS effector involved in biofilm formation on triglycerides was identified as a lipase. In the case of biofilm formation on n-hexadecane, the T2SS effector is likely involved in the mass transfer, capture or transport of alkanes. We propose that M. hydrocarbonoclasticus uses cytoplasmic proteins released by cell lysis to form a proteinaceous matrix and dedicated proteins secreted through the T2SS to act specifically in the assimilation pathways of hydrophobic substrates.
%G English
%L hal-01481292
%U https://hal.archives-ouvertes.fr/hal-01481292
%~ CNRS
%~ UNIV-PAU
%~ INRA
%~ INC-CNRS
%~ AGROPARISTECH
%~ UNIV-PARIS-SACLAY
%~ AGROPARISTECH-SACLAY
%~ INRA-SACLAY
%~ AGROPARISTECH-ORG
%~ AGROPARISTECH-SPAB
%~ IPREM
%~ INRAE
%~ IPREM-CME
%~ AGREENIUM
%~ GS-BIOSPHERA

%0 Journal Article
%T Sensitive detection of selenoproteins in gel electrophoresis by high repetition rate femtosecond laser ablation-inductively coupled plasma mass spectrometry
%+ Laboratoire de Chimie Analytique Bio-Inorganique et Environnement (LCABIE)
%A Ballihaut, G.
%A Claverie, F.
%A Pecheyran, C.
%A Mounicou, S.
%A Grimaud, R.
%A Lobinski, Ryszard
%< avec comité de lecture
%@ 0003-2700
%J Analytical Chemistry
%I American Chemical Society
%V 79
%N 17
%P 6874-6880
%8 2007
%D 2007
%R 10.1021/ac0709145
%Z Chemical Sciences/Analytical chemistryJournal articles
%X A laser ablation-ICPMS method using an infrared (1030 nm), low-energy (39 ?J/pulse), high repetition rate (10 kHz), femtosecond laser was developed to improve the sensitivity of detection of heteroatom-containing proteins in 1D polyacrylamide gels. A 2-mm-wide lane was ablated by ultrafast (10 cm s -1) back-and-forth movement of a 20-?m laser beam parallel to the protein bands while the gel advanced perpendicularly. This procedure resulted in a considerable increase in detection sensitivity (> 40-fold) compared to the nanosecond 266-nm laser ablation-ICPMS, mainly because of the much larger amount of ablated material introduced into the plasma on the time scale of the dwell time of the mass spectrometer. The method was applied to the specific detection in the gel of formate dehydrogenase expressed in Escherichia coli and of selenoproteins in Desulfococcus multivorans with detection limits at the low-femtomolar levels. © 2007 American Chemical Society.
%G English
%L hal-00291961
%U https://hal.archives-ouvertes.fr/hal-00291961
%~ CNRS
%~ UNIV-PAU
%~ IPREM
%~ TESTUPPA

%0 Journal Article
%T Assembly of both the head and tail of bacteriophage Mu is blocked in Escherichia coli groEL and groES mutants
%+ Université libre de Bruxelles (ULB)
%+ Département de mathématiques Université Libre de Bruxelles
%A Grimaud, Régis
%A Toussaint, A.
%Z cited By 8
%< avec comité de lecture
%@ 0021-9193
%J Journal of Bacteriology
%I American Society for Microbiology
%V 180
%N 5
%P 1148-1153
%8 1998
%D 1998
%K article
%K bacterial genetics
%K bacterium mutant
%K gene mutation
%K nonhuman
%K priority journal
%K Bacteriophage mu
%K Escherichia coli
%K GroEL Protein
%K GroES Protein
%K Lysogeny
%K Morphogenesis
%K Mutation
%K Viral Proteins
%K Virus Assembly
%K Bacteria (microorganisms)
%K Enterobacteria phage Mu
%K Negibacteria
%K unidentified bacteriophage
%Z Life Sciences [q-bio]Journal articles
%X Like several other Escherichia coli bacteriophages, transposable phage Mu does not develop normally in groE hosts (M. Pato, M. Banerjee, L. Desmet, and A. Toussaint, J. Bacteriol. 169:5504-5509, 1987). We show here that lysates obtained upon induction of groE Mu lysogens contain free inactive tails and empty heads. GroEL and GroES are thus essential for the correct assembly of both Mu heads and Mu tails. Evidence is presented that groE mutations inhibit processing of the phage head protein gpH as well as the formation of a 25S complex suspected to be an early Mu head assembly intermediate.
%G English
%L hal-01614794
%U https://hal.archives-ouvertes.fr/hal-01614794

