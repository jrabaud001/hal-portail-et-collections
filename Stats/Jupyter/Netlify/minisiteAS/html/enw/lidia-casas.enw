%0 Journal Article
%T Microcalorimetric study of the growth of Enterococcus faecalis in an enriched culture medium
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Rivero, Natividad Lago
%A Legido Soto, José
%A Casas, Lidia
%A Santos, Isaac Arias
%< avec comité de lecture
%@ 1388-6150
%J Journal of Thermal Analysis and Calorimetry
%I Springer Verlag
%V 108
%N 2
%P 665-670
%8 2012-05
%D 2012
%R 10.1007/s10973-011-2052-1
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%G English
%L hal-02157900
%U https://hal.archives-ouvertes.fr/hal-02157900
%~ UNIV-PAU
%~ LATEP

%0 Journal Article
%T Microcalorimetric study of the growth of Enterococcus faecalis, Klebsiella pneumoniae and their mixtures in an enriched culture medium
%+ Electronics Technology Department, Carlos III University of Madrid
%+ Neurorestoration Group
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Vázquez, C.
%A Lago, N.
%A Legido, J.
%A Arias, I.
%A Casas, Lidia
%A Mato, M.
%< avec comité de lecture
%@ 1388-6150
%J Journal of Thermal Analysis and Calorimetry
%I Springer Verlag
%V 113
%N 3
%P 1415-1420
%8 2013-09
%D 2013
%R 10.1007/s10973-013-3287-9
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%G English
%L hal-02157876
%U https://hal.archives-ouvertes.fr/hal-02157876
%~ UNIV-PAU
%~ LATEP

%0 Journal Article
%T Microcalorimetric performance of the growth in culture of Escherichia coli, Proteus mirabilis and their mixtures in different proportions
%+ Electronics Technology Department, Carlos III University of Madrid
%+ Neurorestoration Group
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Vázquez, C.
%A Lago, N.
%A Mato, M.
%A Casas, Lidia
%A Esarte, L.
%A Legido, L.
%A Arias, I.
%< avec comité de lecture
%@ 1388-6150
%J Journal of Thermal Analysis and Calorimetry
%I Springer Verlag
%V 116
%N 1
%P 107-112
%8 2014-04
%D 2014
%R 10.1007/s10973-013-3535-z
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%G English
%L hal-02157874
%U https://hal.archives-ouvertes.fr/hal-02157874
%~ UNIV-PAU
%~ LATEP

%0 Journal Article
%T New methodology for simultaneous volumetric and calorimetric measurements: Direct determination of αp and Cp for liquids under pressure
%+ Departamento de Física Aplicada
%+ Thermodynamique et Energétique des fluides complexes (TEFC)
%A Casas, Lidia
%A Plantier, Frédéric
%A Bessieres, David
%< avec comité de lecture
%@ 0034-6748
%J Review of Scientific Instruments
%I American Institute of Physics
%V 80
%N 12
%P Article number 124902
%8 2009
%D 2009
%R 10.1063/1.3270258
%K 1-hexanol
%K 1-octanol
%K 3-Pentanol
%K Batch cells
%K Calorimetric measurements
%K Direct determination
%K Experimental values
%K Heat capacities
%K Isobaric heat capacity
%K Linear displacements
%K Temperature range
%K Thermo-physical property Engineering controlled terms: Atmospheric pressure
%K Calorimetry
%K Specific heat
%K Thermal stress Engineering main heading: Thermal expansion
%Z Physics [physics]/Physics [physics]/Chemical Physics [physics.chem-ph]Journal articles
%X A new batch cell has been developed to measure simultaneously both isobaric thermal expansion and isobaric heat capacity from calorimetric measurements. The isobaric thermal expansion is directly proportional to the linear displacement of an inner flexible below and the heat capacity is calculated from the calorimetric signal. The apparatus used was a commercial Setaram C-80 calorimeter and together with this type of vessels can be operated up to 20 MPa and in the temperature range of 303.15-523.15 K, In this work, calibration was carried out using 1-hexanol and subsequently both thermophysical properties were determined for 3-pentanol, 3-ethyl-3-pentanol, and 1-octanol at atmospheric pressure, 5 and 10 MPa, and from 303.15 to 423.15 K in temperature. Finally experimental values were compared with the literature in order to validate this new methodology, which allows a very accurate determination of isobaric thermal expansion and isobaric heat capacity.
%G English
%L hal-00706024
%U https://hal.archives-ouvertes.fr/hal-00706024
%~ CNRS
%~ UNIV-PAU
%~ LFCR-GEOMECA
%~ LFCR
%~ LFCR-ISD

%0 Journal Article
%T Thermal behavior of mixtures of bentonitic clay and saline solutions
%+ Departamento de Física Aplicada
%+ Departamento de Geología y Geoquímica
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Casas, Lidia
%A Pozo, M.
%A Gómez, C. P.
%A Pozo, Eduardo
%A Bessieres, David
%A Plantier, Frédéric
%A Legido, Jose Luis
%< avec comité de lecture
%@ 0169-1317
%J Applied Clay Science
%I Elsevier
%V 72
%P 18-25
%8 2013-02-01
%D 2013
%R 10.1016/j.clay.2012.12.009
%K Bentonitic clay
%K Salinity
%K Seawater
%K Specific heat capacity
%K Thermophysical properties
%K Thermotherapy
%Z Physics [physics]/Physics [physics]/Chemical Physics [physics.chem-ph]Journal articles
%X Thermophysical characterization is a useful tool for studying mixtures of clay and saline solutions and evaluating their suitability from a thermotherapic point of view. This work focuses on studying the specific heat capacity, density, thermal conductivity and viscosity of a bentonitic clay (composed mainly of 55% Na+-saturated trioctahedral smectite, 28% sepiolite and 15% illite) mixed with different saline solutions (0, 1, 2, 3 and 3.5%) in the temperature range 293.15 to 317.15K. The results indicate meaningful differences in the thermal behavior of the studied pastes in relation to thermotherapy and pelotherapy.
%G English
%L hal-00830624
%U https://hal.archives-ouvertes.fr/hal-00830624
%~ CNRS
%~ LFCR-GEOMECA
%~ UNIV-PAU
%~ LFCR
%~ LFCR-ISD

%0 Journal Article
%T Specific heat of mixtures of kaolin with sea water or distilled water for their use in thermotherapy
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ Universidade de Vigo
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Mato, M.M.
%A Casas, Lidia
%A Legido, J.L.
%A Gómez, C.
%A Mourelle, L.
%A Bessieres, David
%A Plantier, Frédéric
%Z ACL
%< avec comité de lecture
%@ 1388-6150
%J Journal of Thermal Analysis and Calorimetry
%I Springer Verlag
%V 130
%N 1
%P 479-484
%8 2017
%D 2017
%R 10.1007/s10973-017-6227-2
%Z Physics [physics]/Condensed Matter [cond-mat]/Materials Science [cond-mat.mtrl-sci]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Fluids mechanics [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of materials [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Mechanical engineering [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Mechanics of materials [physics.class-ph]
%Z Sciences of the Universe [physics]/Earth Sciences/Geophysics [physics.geo-ph]Journal articles
%X Mixtures of clays and waters of different mineralization have been used for thermotherapy therapies since ancient times. These mixtures are the basis of the most so-called thermal peloids, which are used for therapeutic purposes in the main thermal centres of the world. The thermal properties of peloids are very important to establish their applicability and determine whether they are appropriate for use in thermotherapy. This work focuses on the study of the behaviour of the specific heat capacity of different mixtures of kaolin with sea water or distilled water as a function of water concentration. Sea water is equivalent to high mineralized water, and distilled water corresponds to zero mineralization. Specific heat capacity was measured at atmospheric pressure and in the temperature interval from 293.15 to 317.15 K, using a commercial SETARAM BT 2.15 calorimeter. This device is based on the principle of Calvet calorimetry with temperature control. Furthermore, experimental results were compared to those obtained from mixtures with other clays. © 2017, Akadémiai Kiadó, Budapest, Hungary.
%G English
%L hal-01815864
%U https://hal.archives-ouvertes.fr/hal-01815864
%~ CNRS
%~ UNIV-PAU
%~ LFCR
%~ LATEP
%~ LFCR-ISD

%0 Journal Article
%T Specific heat of mixtures of bentonitic clay with sea water or distilled water for their use in thermotherapy
%+ Departamento de Física Aplicada
%+ Departamento de Geología y Geoquímica
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Casas, Lidia
%A Luis Legido, Jose
%A Pozo, M.
%A Mourelles, L.
%A Plantier, Frédéric
%A Bessieres, David
%< avec comité de lecture
%@ 0040-6031
%J Thermochimica Acta
%I Elsevier
%V 524
%N 1-2
%P 68-73
%8 2011-09-20
%D 2011
%R 10.1016/j.tca.2011.06.016
%K Bentonitic clay
%K Mineralogy
%K Sea water
%K Specific heat
%K Thermotherapy
%Z Physics [physics]/Physics [physics]/Chemical Physics [physics.chem-ph]Journal articles
%X In thermotherapy, an appropriate and detailed chemical and physical characterization of the different clays and thermal muds is necessary for the use of these products. In order to predict the thermal behaviour of a mud the analysis of its physical properties is required; among these, those that have bigger influence are the specific heat capacity, the thermal conductivity and the density. This work focuses on the study of the behaviour of the specific heat of a bentonitic clay (composed mainly of 55% Na-saturated smectite, 28% sepiolite, and 15% illite) mixed with distilled water or sea water as a function of the water concentration in the temperature range 293.15-317.15 K. The experimental data were measured using a Calvet calorimeter, SETARAM BT 2.15, which permits studying with high precision all kind of substances and materials (oils, polymers, hydrates, powders) and simulate almost any process condition (reactions, crystallisation, freezing, etc.). The obtained specific heat values in the studied temperature range are between 851 and 889 J kg -1 K -1 for mineral phase, 3479 and 3505 J kg -1 K -1 for 19.9% bentonite + distilled water, and 2724 and 2758 J kg -1 K -1 for 39.8% bentonite + sea water. The results of this study can be used in the spas or thermal centres to determine the suitability and quality of the bentonitic muds for pelotherapy.
%G English
%L hal-00683706
%U https://hal.archives-ouvertes.fr/hal-00683706
%~ CNRS
%~ UNIV-PAU
%~ LFCR-GEOMECA
%~ LFCR
%~ LFCR-ISD

%0 Journal Article
%T Influence of dilution on the thermophysical properties of Dax peloid (TERDAX ®)
%+ Institut du Thermalisme
%+ Departamento de Física Aplicada
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Knorst-Fouran, Auriane
%A Casas, Lidia
%A Legido, Jose Luis
%A Coussine, C.
%A Bessieres, David
%A Plantier, Frédéric
%A Lagière, J.
%A Dubourg, K.
%< avec comité de lecture
%@ 0040-6031
%J Thermochimica Acta
%I Elsevier
%V 539
%P 34-38
%8 2012-07-10
%D 2012
%R 10.1016/j.tca.2012.03.024
%K Dax peloid
%K Density
%K Specific heat capacity
%K Thermal and mineralized water
%K Thermal conductivity
%K Thermotherapy
%Z Physics [physics]/Physics [physics]/Chemical Physics [physics.chem-ph]Journal articles
%X In thermotherapy, the efficiency of a peloid depends in particular on its thermal quality and an appropriate and detailed chemical and physical characterization is necessary to understand the thermal behaviour of these products. The most influential physical properties are the specific heat capacity, the thermal conductivity and the density. This work focuses on the study of the evolution of those properties of Dax peloid mixed with thermal and mineral water as a function of the mud concentration. The experimental specific heat capacity data were measured using a Calvet calorimeter, SETARAM BT 2.15, which permits studying with high precision all kind of substances and materials (oils, polymers, hydrates, powders) and simulate almost any process condition (reactions, crystallisation, freezing, etc.) and in the temperature range from 298.15 to 314.15 K. The pycnometer method and an Anton-Paar DMA-4500 vibrating-tube densimeter were used to measure density values while the thermal conductivity was determined by the Model KD2 Pro, Decagon Devices Inc. The experimental data show the expected behaviour: the specific heat capacity increases with temperature and decreases with concentration whereas density decreases with temperature and increases with concentration. Besides the thermal conductivity increases with both temperature and concentration. The results of this study will be used to determine the best proportion between peloid and mineral water for an individual mud bath.
%G English
%L hal-00706011
%U https://hal.archives-ouvertes.fr/hal-00706011
%~ CNRS
%~ UNIV-PAU
%~ LFCR-GEOMECA
%~ LFCR
%~ LFCR-ISD

%0 Journal Article
%T Experimental and Modeling Density and Surface Tension of 1, 2-Dimethylbenzene with Alkanes at 298.15 K
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Andreatta, A.E.
%A Martini, R.E.
%A Legido, J.L
%A Casas, Lidia
%Z ACL
%< avec comité de lecture
%J International Journal of Engineering Research & Science
%V 2
%P 51--61
%8 2016
%D 2016
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%G English
%L hal-02129499
%U https://hal.archives-ouvertes.fr/hal-02129499
%~ UNIV-PAU
%~ LATEP
%~ TESTUPPA

%0 Journal Article
%T Experimental and Nitta–Chao model prediction of high pressure density of p-xylene with dialkyl carbonates or n-alkanes
%+ Planta Piloto de Ingeniería Química [Bahía Blanca] (PLAPIQUI)
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Gayol, Ana
%A Martini, Raquel
%A Andreatta, Alfonsina
%A Legido, José
%A Casas, Lidia
%< avec comité de lecture
%@ 0021-9614
%J Journal of Chemical Thermodynamics
%I Elsevier
%V 69
%P 193-200
%8 2014-02
%D 2014
%R 10.1016/j.jct.2013.09.039
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%G English
%L hal-02157870
%U https://hal.archives-ouvertes.fr/hal-02157870
%~ UNIV-PAU
%~ LATEP
%~ TESTUPPA

%0 Journal Article
%T Volumetric properties of (dialkyl carbonate+n-alkane) mixtures at high pressures: Experimental measurement and Nitta–Chao model prediction
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ Planta Piloto de Ingeniería Química [Bahía Blanca] (PLAPIQUI)
%A Gayol, Ana
%A Casas, Lidia
%A Martini, Raquel
%A Andreatta, Alfonsina
%A Legido, José
%< avec comité de lecture
%@ 0021-9614
%J Journal of Chemical Thermodynamics
%I Elsevier
%V 58
%P 245-253
%8 2013-03
%D 2013
%R 10.1016/j.jct.2012.11.011
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%G English
%L hal-02157892
%U https://hal.archives-ouvertes.fr/hal-02157892
%~ UNIV-PAU
%~ LATEP
%~ TESTUPPA

%0 Journal Article
%T Calibration of a low temperature calorimeter and application in the determination of isobaric heat capacity of 2-propanol
%+ Departamento de Física Aplicada
%+ Thermodynamique et Energétique des fluides complexes (TEFC)
%A Casas, Lidia
%A Plantier, Frédéric
%A Pineiro, Manuel
%A Luis Legido, Jose
%A Bessieres, David
%< avec comité de lecture
%@ 0040-6031
%J Thermochimica Acta
%I Elsevier
%V 507-508
%P 123-126
%8 2010-08-10
%D 2010
%R 10.1016/j.tca.2010.05.012
%K 2-Propanol
%K Calibration procedure
%K Isobaric heat capacity
%K Low temperature calorimeter
%Z Physics [physics]/Physics [physics]/Chemical Physics [physics.chem-ph]Journal articles
%X Nowadays, the experimental thermodynamic characterization of solvents in an extended range of temperatures and pressures is essential for the development of a wide variety of industrial applications (refrigeration, reactors, pumping, etc.). Moreover, accurate experimental data are also the key for the successful development and subsequent benchmarking of thermodynamic theoretical models. In the particular case of isobaric heat capacities, there are quite a lot of reported experimental high temperature data but, on the other hand, low temperature data are practically inexistent for most compounds. Bearing this limitation in mind, the present work is focused on the development of a new calibration methodology for calorimetric determination of isobaric heat capacities in liquid state at low temperatures. For this purpose, a Calvet calorimeter, SETARAM BT 2.15 has been used. By means of the calibration procedure explained below, this calorimeter allows to determine phase transitions and thermodynamic properties in a wide range of temperature (233.15-473.15 K) and pressure (0.1-100 MPa).
%G English
%L hal-00685649
%U https://hal.archives-ouvertes.fr/hal-00685649
%~ CNRS
%~ UNIV-PAU
%~ LFCR-GEOMECA
%~ LFCR
%~ LFCR-ISD

%0 Journal Article
%T Differentiation Between Staphylococcus aureus and Staphylococcus epidermidis Using Microcalorimetry
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Rivero, Natividad Lago
%A Soto, José
%A Santos, Isaac Arias
%A Casas, Lidia
%< avec comité de lecture
%@ 0195-928X
%J International Journal of Thermophysics
%I Springer Verlag
%V 34
%N 6
%P 1039-1048
%8 2013-06
%D 2013
%R 10.1007/s10765-013-1448-5
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%G English
%L hal-02157887
%U https://hal.archives-ouvertes.fr/hal-02157887
%~ UNIV-PAU
%~ LATEP
%~ TESTUPPA

%0 Journal Article
%T Environmentally Friendly Process for Producing Magnesium-Enriched Salt
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ Centre d'océanologie de Marseille (COM)
%+ Laboratoire de Microbiologie et Biotechnologie des Environnements Chauds
%A Coussine, Charlotte
%A Casas, Lidia
%A Serin, Jean-Paul
%A Contamine, François
%A Cézac, Pierre
%A Dubourg, Karine
%A Cambar, Jean
%< avec comité de lecture
%@ 0888-5885
%J Industrial and engineering chemistry research
%I American Chemical Society
%V 57
%N 43
%P 14680-14688
%8 2018-10-05
%D 2018
%R 10.1021/acs.iecr.8b03114
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%G English
%L hal-02157850
%U https://hal.archives-ouvertes.fr/hal-02157850
%~ UNIV-PAU
%~ UMS_COM
%~ LMBEC
%~ CNRS
%~ INSU
%~ TEST-AMU
%~ UNIV-AMU
%~ LATEP
%~ TESTUPPA

%0 Journal Article
%T Corrigendum to “Experimental and Nitta–Chao model prediction of high pressure density of p-xylene with dialkyl carbonates or n-alkanes” [J. Chem. Thermodyn. 69 (2014) 193–200]
%+ Planta Piloto de Ingeniería Química [Bahía Blanca] (PLAPIQUI)
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Gayol, Ana
%A Martini, Raquel
%A Andreatta, Alfonsina
%A Legido, José
%A Casas, Lidia
%< avec comité de lecture
%@ 0021-9614
%J Journal of Chemical Thermodynamics
%I Elsevier
%V 89
%N 1
%P 314-315
%8 2015-10
%D 2015
%R 10.1016/j.jct.2015.05.017
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%G English
%L hal-02157862
%U https://hal.archives-ouvertes.fr/hal-02157862
%~ UNIV-PAU
%~ LATEP
%~ TESTUPPA

%0 Journal Article
%T Surface Tension of Dialkyl Carbonates + (Alkanes or 1,4-Dimethylbenzene) and 1,4-Dimethylbenzene + Alkanes Binary Mixtures at T = 308.15 K
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ Planta Piloto de Ingeniería Química [Bahía Blanca] (PLAPIQUI)
%A Gayol, Ana
%A Casas, Lidia
%A Andreatta, Alfonsina
%A Martini, Raquel
%A Legido, José
%< avec comité de lecture
%@ 0021-9568
%J Journal of Chemical and Engineering Data
%I American Chemical Society
%V 58
%N 3
%P 758-763
%8 2013-02-12
%D 2013
%R 10.1021/je301282p
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%G English
%L hal-02157884
%U https://hal.archives-ouvertes.fr/hal-02157884
%~ UNIV-PAU
%~ LATEP
%~ TESTUPPA

