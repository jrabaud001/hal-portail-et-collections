%0 Journal Article
%T Bataille absurde : de l'angoisse au rire
%+ Arts / Langages : Transitions et Relations (ALTER)
%A ROUSSEAU, Guillaume
%< avec comité de lecture
%@ 2105-0864
%J Implications philosophiques
%I Implications philosophiques
%S L'Absurde
%8 2015
%D 2015
%Z Humanities and Social Sciences/LiteratureJournal articles
%X Dans cet article, nous nous proposons de mettre en lumière le caractère spécifiquement littéraire du sentiment de l'absurde dans les romans et récits de Georges Bataille (spécialement Madame Edwarda et Le Bleu du ciel). Par cet exemple, la fiction littéraire, en marge de la philosophie, se donne pour rôle de représenter une expérience impensable. Dans les øeuvres romanesques de Bataille, l'absurde se place d'abord sous le signe de l'angoisse. En effet, Bataille s'inspire de Nietzsche en prenant acte de la mort de Dieu qu'il représente par l'image récurrente d'un ciel désormais vide. Désorientés, les personnages batailliens en proie à l'angoisse se retrouvent dans une situation d'impasse où ils ne peuvent que crier leur désespoir. Cependant, cette angoisse est finalement dépassée par un rire souverain qui non seulement admet le non-sens mais l'approuve.
%G French
%L hal-02170617
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02170617
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T Houellebecq lecteur de Guénon. La \guillemotleft fibre guénonienne \guillemotright de "Soumission"
%+ Arts / Langages : Transitions et Relations (ALTER)
%A ROUSSEAU, Guillaume
%< avec comité de lecture
%@ 0295-5024
%J Roman 20-50 : Revue d'étude du roman du XXe siècle
%I Presses Universitaires du Septentrion
%V 3
%N 66
%P 117-126
%8 2018
%D 2018
%R 10.3917/r2050.066.0117
%Z Humanities and Social Sciences/LiteratureJournal articles
%G French
%L hal-02170664
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02170664
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ REVUES_LILLE3
%~ REVUES_LILLE
%~ TESTUPPA

%0 Journal Article
%T La petite musique de Milan Kundera et le motif du quatuor
%+ Arts / Langages : Transitions et Relations (ALTER)
%A ROUSSEAU, Guillaume
%< avec comité de lecture
%@ 1243-3721
%J Op. Cit. Revue des littératures et des arts
%I CRPHLL, Université de Pau
%S Quatuor, littérature et cinéma
%N 18
%8 2018
%D 2018
%Z Humanities and Social Sciences/LiteratureJournal articles
%X Le présent article interroge le sens du motif du quatuor dans l'øeuvre de Milan Kundera. Pour ce faire, nous mettons en regard le discours critique de l'écrivain et sa pratique romanesque. En nous appuyant sur L'Art du roman, nous soulignons le rôle essentiel joué par le quatuor dans le cadre de la définition kundérienne du roman polyphonique. Néanmoins, dès lors que l'on se plonge au cøeur du roman avec L'Insoutenable légèreté de l'être, le motif du quatuor (en l'occurrence le quatuor n° 16 de Beethoven) fait les frais de l'ironie du romancier qui en dénonce l'esprit de sérieux. Ainsi, sous la plume de Kundera, l'øeuvre musicale qu'est le quatuor sert tout à la fois de modèle et de contre-modèle à la création romanesque.
%G French
%L hal-02170665
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02170665
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T Le mythe à l’épreuve de la littérature : <i>Gueule de pierre</i>, R. Queneau
%+ Arts / Langages : Transitions et Relations (ALTER)
%A ROUSSEAU, Guillaume
%< avec comité de lecture
%@ 1843-8539
%J Mélanges francophones
%I Galaţi University Press
%8 2017-12
%D 2017
%Z Humanities and Social Sciences/LiteratureJournal articles
%G French
%L hal-02069261
%U https://hal.archives-ouvertes.fr/hal-02069261
%~ UNIV-PAU
%~ SHS
%~ ALTER
%~ TESTUPPA

