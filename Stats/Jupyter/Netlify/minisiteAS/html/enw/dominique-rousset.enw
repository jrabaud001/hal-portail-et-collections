%0 Journal Article
%T Crosshole reflection imaging with ground-penetrating radar data: Applications in near-surface sedimentary settings
%+ Institut des Sciences de la Terre (ISTerre)
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%+ Institute of Earth and Environmental Science [Potsdam]
%A Allroggen, Niklas
%A Garambois, Stéphane
%A Sénéchal, Guy
%A Rousset, Dominique
%A Tronicke, Jens
%< avec comité de lecture
%@ 0016-8033
%J Geophysics
%I Society of Exploration Geophysicists
%V 85
%N 4
%P H61-H69
%8 2020-07-01
%D 2020
%R 10.1190/geo2019-0558.1
%Z Sciences of the Universe [physics]/Earth Sciences/Geophysics [physics.geo-ph]Journal articles
%G English
%L hal-02899118
%U https://hal.archives-ouvertes.fr/hal-02899118
%~ OSUG
%~ UNIV-PAU
%~ UNIV-SAVOIE
%~ CNRS
%~ LFCR
%~ LFCR-CRG
%~ INSU
%~ ISTERRE
%~ IFSTTAR
%~ UGA-EPE
%~ UGA

%0 Journal Article
%T Late Pleistocene folding above the Mail Arrouy Thrust, North-Western Pyrenees (France)
%+ Institut de biologie et chimie des protéines [Lyon] (IBCP)
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%A Lacan, P.
%A Nivière, Bertrand
%A Rousset, Dominique
%A Sénéchal, P.
%< avec comité de lecture
%@ 0040-1951
%J Tectonophysics
%I Elsevier
%V 541-543
%P 57-68
%8 2012-05
%D 2012
%R 10.1016/j.tecto.2012.03.022
%K Neotectonic
%K Pleistocene
%K Pyrenees
%K Seismic risk
%K Seismogenetic sources Compressional
%K Extensional stress
%K Late Pleistocene
%K Normal faults
%K Plate kinematics
%K Seismic event
%K Seismological data
%K Stress regime
%K Tectonic activity Engineering controlled terms: Tectonics Engineering main heading: Earthquakes GEOBASE Subject Index: deformation
%K earthquake
%K earthquake catalogue
%K folding
%K neotectonics
%K normal fault
%K plate tectonics
%K seismicity
%K seismology
%K tectonic phenomena
%K thrust Regional Index: Pyrenees
%Z Sciences of the Universe [physics]/Earth SciencesJournal articles
%X The Western Pyrenees have experienced several major earthquakes in the last 400. years. Herein, we investigate the ongoing tectonic activity of the Arudy area affected by one of the largest earthquakes (M = 5.1; 1980) registered in the instrumental seismicity catalogue in the Western Pyrenees. Folding of alluvial terraces younger than 17 +/- 3. ky above the Mail Arrouy Thrust (MAT), Attest from a Late Pleistocene tectonic activity. This deformation resulted in a shallow fold with a wavelength of 2800. m and amplitude of 7 to 8. m. The MAT is rooted above a former normal fault of the Cretaceous Iberian margin, which is a potential seismogenic source.Such observations suggest that the northern flank of the Western Pyrenees is still subjected to shortening in response to a compressional stress regime. This compression is consistent with the African-Eurasian plate kinematics and the Arudy 1980 earthquake. Nonetheless, recent seismological data suggest the presence of an extensional stress regime in the range. This extension is indicated by some seismic events mostly observed in the Pyrenean high chain. Our hypothesis is that these extension-driven events could be due to a local stress-field induced by the elevation of the range. On the contrary, the compression, associated with the regional stress regime, could prevail in the outermost domain of the range.
%G English
%L hal-00871702
%U https://hal.archives-ouvertes.fr/hal-00871702
%~ CNRS
%~ UNIV-PAU
%~ UNIV-LYON1
%~ LFCR
%~ UDL
%~ UNIV-LYON
%~ LFCR-CRG
%~ TESTUPPA

%0 Journal Article
%T Seismic anisotropy analysis at the Low-Noise Underground Laboratory (LSBB) of Rustrel (France),
%+ Géosciences Paris Sud (GEOPS)
%+ Institut pluridisciplinaire de recherche appliquée dans le domaine du génie pétrolier (IPRADDGP)
%+ Géoazur (GEOAZUR 6526)
%A Beres, J.
%A Zeyen, H.
%A Sénéchal, G.
%A Rousset, Dominique
%A Gaffet, Stéphane
%< avec comité de lecture
%@ 0926-9851
%J Journal of Applied Geophysics
%I Elsevier
%V 94
%P 59-71
%8 2013-07
%D 2013
%R 10.1016/j.jappgeo.2013.04.008
%Z Sciences of the Universe [physics]/Earth SciencesJournal articles
%X Seismic anisotropy of a fractured karstic limestone massif in sub-parallel underground galleries is studied. As the fractures are mostly vertically oriented, the seismic properties of the massif are approximated by horizontal transverse isotropy (HTI). Several data inversion methods were applied to a seismic dataset of arrival-times of P and S-waves. The applied methods include: isotropic tomography, simple cosine function fit, homogeneous Monte-Carlo anisotropic inversion for the parameters of horizontal transverse isotropy and anisotropic tomography for tilted transversely isotropic bodies. All methods lead to the conclusion that there is indeed an anisotropy present in the rock massif and confirm the direction of maximum velocity parallel to the direction of fracturing. Strong anisotropy of about 15% is found in the studied area. Repeated measurements show variations of the P-wave parameters, but not of the S-wave parameters, which is reflecting a change in water saturation.
%G English
%L hal-00830200
%U https://hal.archives-ouvertes.fr/hal-00830200
%~ INSU
%~ UPMC
%~ CNRS
%~ OCA
%~ GEOAZUR
%~ UNIV-PAU
%~ UNICE
%~ GEOPS
%~ UNIV-PSUD
%~ UCA-TEST
%~ UNIV-COTEDAZUR
%~ SORBONNE-UNIVERSITE

%0 Journal Article
%T Ground-penetrating radar investigation inside a karstified limestone reservoir,
%+ Institut pluridisciplinaire de recherche appliquée dans le domaine du génie pétrolier (IPRADDGP)
%+ Géoazur (GEOAZUR 7329)
%A Sénéchal, G.
%A Rousset, Dominique
%A Gaffet, Stéphane
%< avec comité de lecture
%@ 1569-4445
%J Near Surface Geophysics
%I European Association of Geoscientists and Engineers (EAGE)
%V 11
%N 3
%P 283-291
%8 2013-06
%D 2013
%R 10.3997/1873-0604.2013008
%Z Sciences of the Universe [physics]/Earth SciencesJournal articles
%X In this paper, we present ground-penetrating radar (GPR) investigations performed along a 3.7 km long tunnel located inside a lower Cretaceous limestone massif of south-eastern France. This fractured massif is mainly characterized by water circulation and karstic structures. This kind of geological formation contains a large part of the fresh underground water resources of the world and is also considered as an analogue of Middle East oil reservoirs. Since tunnel walls are covered by thick reinforced concrete, direct geological observations are impossible. After some preliminary tests, the entire tunnel was investigated using 250 MHz shielded antennas. Data are generally of very good quality, with reflection time up to 400 ns (down to 18 m under the tunnel floor with a velocity of 9 cm/ns). We correlate the GPR signal along the tunnel with surface geological observations: the upper part of the investigated formation (Bedoulian) displays prominent stratigraphic reflectors while the lower part (Barremian) does not. Numerous diffractions are observed in both formations and can be related to karstic features. These investigations allow to better constrain the geological context along the tunnel, necessary for future hydrogeological studies. We conclude that this tunnel offers a unique opportunity of performing GPR measurements within a karstified limestone massif.
%G English
%L hal-01050927
%U https://hal.archives-ouvertes.fr/hal-01050927
%~ INSU
%~ CNRS
%~ OCA
%~ UNIV-PAU
%~ UCA-TEST
%~ UNIV-COTEDAZUR
%~ GEOAZUR

%0 Journal Article
%T Georadar and seismic investigations over the Glacier de la Girose (French Alps).
%+ Laboratoire de Géophysique Interne et Tectonophysique (LGIT)
%A Sénéchal, G.
%A Rousset, Dominique
%A Salom, A.-L.
%A Grasso, Jean-Robert
%< avec comité de lecture
%@ 1569-4445
%J Near Surface Geophysics
%I European Association of Geoscientists and Engineers (EAGE)
%V 1
%P 5-12
%8 2003
%D 2003
%Z Sciences of the Universe [physics]/Earth Sciences/Geophysics [physics.geo-ph]Journal articles
%G English
%L hal-00109915
%U https://hal.archives-ouvertes.fr/hal-00109915
%~ INSU
%~ CNRS
%~ UNIV-GRENOBLE1
%~ UNIV-SAVOIE
%~ IFSTTAR
%~ IRSTEA
%~ INPG
%~ INRAE
%~ USMB-COMUE
%~ UGA

