%0 Journal Article
%T L'évolution du paysage urbain d'Oloron-Iluro durant l'Antiquité et le début du Moyen-Âge
%+ Identités, Territoires, Expressions, Mobilités (ITEM)
%+ Institut national de recherches archéologiques préventives (Inrap)
%+ Service Régional de l'Archéologie d'Aquitaine (SRA Aquitaine)
%A Réchin, François
%A Wozny, Luc
%A Pichonneau, Jean-François
%A Scuiller, Christian
%A Artigau, Gregory
%A Dumonteil, Jacques
%A Javierre, Cédric
%A Leroy, Fabrice
%A Ortega, Daniel
%< avec comité de lecture
%@ 0758-9670
%J Aquitania
%I Pessac : Fédération Aquitania
%S D'Iluro à Oloron-Sainte-Marie. Un millénaire d'histoire
%N 29
%P 179-267
%8 2013
%D 2013
%K Archéologie
%K Oloron-Sainte-Marie
%K Urbanisation
%K Histoire antique
%K Morphologie urbaine
%Z Humanities and Social Sciences/History, Philosophy and Sociology of Sciences
%Z Humanities and Social Sciences/Archaeology and PrehistoryJournal articles
%X Parmi les agglomérations antiques du piémont pyrénéen, Oloron-Iluro est assurément l’une de celles dont le paysage urbain antique a commencé à être dévoilé le plus tard. Ainsi, la somme des connaissances disponibles avait à peine augmenté, entre le premier bilan des connaissances que G. Fabre avait pu établir lors du Colloque Aquitania tenu à Bordeaux en 1990 et celui que l’un d’entre nous avait tenté de réaliser en 1996, à la différence des agglomérations voisines de Lescar ou de Tarbes. Les développements les plus récents de l’archéologie préventive ont, en revanche, permis d’étoffer considérablement ce dossier à partir du milieu des années 2000. C’est sur la base de l’ensemble de ces acquis que nous voudrions faire porter en priorité notre réflexion sur l’évolution de la morphologie urbaine d’Oloron antique. Dans ce cadre, l’analyse des sources écrites que mène J.-P. Bost dans ce même volume nous dispensera de porter une attention directe à cette partie de la documentation et permettra de concentrer notre propos sur les sources archéologiques.
%G French
%L hal-01656089
%U https://hal.archives-ouvertes.fr/hal-01656089
%~ ITEM
%~ UNIV-PAU
%~ INRAP
%~ HIPHISCITECH
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T La vaisselle céramique non tournée d'Aquitaine méridionale à l'époque romaine. Pourquoi tant d'obstination?
%+ Identités, Territoires, Expressions, Mobilités (ITEM)
%A Réchin, François
%< avec comité de lecture
%@ 0220-6617
%J Revue Archéologique du Centre de la France
%I FERACF
%S Les céramiques non tournées en Gaule romaine dans leur contexte social, économique et culturel : entre tradition et innovation
%N 55
%P 65-83
%8 2015
%D 2015
%K Archéologie
%K Histoire romaine
%K Céramique non tournée
%K Aquitaine méridionale
%Z Humanities and Social Sciences/History
%Z Humanities and Social Sciences/Archaeology and PrehistoryJournal articles
%G French
%L hal-02533574
%U https://hal.archives-ouvertes.fr/hal-02533574
%~ AO-HISTOIRE
%~ HISTOIRE
%~ UNIV-PAU
%~ ITEM
%~ SHS

%0 Journal Article
%T Les thermes publics de Lescar-Beneharnum et d'Oloron-Sainte-Marie-Iluro (Pyrénées Atlantiques). Découvertes récentes
%+ Identités, Territoires, Expressions, Mobilités (ITEM)
%A Roudier, Matthieu
%A Pichonneau, Jean-François
%A Réchin, François
%< avec comité de lecture
%@ 0758-9670
%J Aquitania
%I Pessac : Fédération Aquitania
%S L'eau : usages, risques et représentations dans le Sud-Ouest de la Gaule et le Nord de la péninsule Ibérique, de la finde l'âge du Fer à l'Antiquité tardive (IIe s. a. C. -VIe s. p. C.)
%N 21
%P 493-518
%8 2012
%D 2012
%K Archéologie
%K Pyrénées
%K Oloron-Sainte-Marie
%K Lescar
%K Eau
%K Equipement public
%K Thermes antiques
%Z Humanities and Social Sciences/History
%Z Humanities and Social Sciences/Archaeology and PrehistoryJournal articles
%X Les équipements et monuments publics, notamment ceux qui sont liés à l’utilisation de l’eau, sont généralement, et sans doute à juste titre, considérés comme un marqueur fiable du niveau de développement urbain et de ses inégalités chronologiques et spatiales. Pour l’heure, il faut admettre qu’à cet aune, les agglomérations du piémont occidental des Pyrénées font pâle figure et renvoient, à quelques exceptions près, une image bien médiocre. C’est pour cette raison que les découvertes successives, en 2008 et en 2009, de thermes publics dans les agglomérations antiques de Lescar- Beneharnum et d’Oloron- Iluro prennent un relief particulier.
%G French
%L hal-01654942
%U https://hal.archives-ouvertes.fr/hal-01654942
%~ ITEM
%~ UNIV-PAU
%~ AO-HISTOIRE
%~ HISTOIRE
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T Habiter et aménager l’espace au bord de l’eau dans le piémont occidental des Pyrénées durant l’Antiquité. Quelques points de repères
%+ Identités, Territoires, Expressions, Mobilités (ITEM)
%+ Département d'Histoire
%A Réchin, François
%A Callegarin, Laurent
%A DARLES, Christian
%A Martin, Jean-Marie
%A Sartou, Aurélien
%< avec comité de lecture
%@ 0758-9670
%J Aquitania
%I Pessac : Fédération Aquitania
%S L'eau : usages, risques et représentations dans le Sud-Ouest de la Gaule et le Nord de la péninsule Ibérique, de la finde l'âge du Fer à l'Antiquité tardive (IIe s. a. C. -VIe s. p. C.)
%N 21
%P 217-254
%8 2012
%D 2012
%K Archéologie
%K Piémont pyrénéen
%K Antiquité
%K Aménagement Urbain
%K Habitat
%Z Humanities and Social Sciences/History
%Z Humanities and Social Sciences/Archaeology and PrehistoryJournal articles
%X Dans le cadre de cette rencontre, il nous a paru intéressant d’interroger les rapports que les habitats des Pays de l’Adour pouvaient entretenir avec l’élément aquatique, tant du point de vue de leur localisation, que des aménagements dont ils ont bénéficié. Dans ce cadre, nous avons choisi de rassembler quelques exemples concrets d’insertions d’établissements antiques dans l’hydrosystème, afin d’avancer dans la réflexion que l’on peut mener aujourd’hui sur les interactions existant entre les espaces et les sociétés qui les ont modelés. Certes, dans cette région, la documentation existante, notamment celle qui se rapporte aux données hydro-sédimentaires, n’a pas une ampleur comparable à celle qui a pu être établie dans le sud-est de la France, grâce aux grands travaux d’infrastructures qui y ont été réalisés ces dernières années et à la qualité des équipes impliquées. Aussi, l’absence de véritables travaux géomorphologiques à visée archéologique en Aquitaine méridionale nous empêchera à plusieurs reprises d’approfondir nos analyses. Mais on ne devra justement pas s’arrêter à ces difficultés si l’on veut améliorer nos connaissances en la matière, et les données que nous tenterons de réunir permettent de porter l’attention sur une région océanique où la gestion de l’eau pose des problèmes très différents de ceux qui apparaissent dans les espaces méditerranéens. Cette contribution insistera sur les profits scientifiques que peut procurer cette démarche, à la suite des travaux entrepris dans ce sens-là lors des chantiers archéologiques préventifs liés à la construction du tronçon de l’autoroute A 65 entre Pau et Langon. Pour limitées que puissent paraître les observations dont nous voudrions faire état, celles-ci se rapportent à un échantillon d’établissements assez variés dans leurs fonctions et assez bien répartis dans le territoire considéré pour que l’on puisse leur accorder une certaine exemplarité : agglomérations urbaines (Lescar-Beneharnum et Oloron-Iluro dans les Pyrénées-Atlantiques, Tarbes-Turba dans les Hautes-Pyrénées), villae (Lalonquette et Jurançon,) et établissements plus modestes (structure d’accès à un ruisseau à Miossens, Pyrénées-Atlantiques).
%G French
%L hal-01654954
%U https://hal.archives-ouvertes.fr/hal-01654954
%~ ITEM
%~ UNIV-PAU
%~ AO-HISTOIRE
%~ HISTOIRE
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T Une grotte pyrénéenne occupée au début de l’époque romaine : le site d’Apons à Sarrance (Pyrénées-Atlantiques)
%+ Chercheur indépendant
%+ Identités, Territoires, Expressions, Mobilités (ITEM)
%A Dumontier, Patrice
%A Réchin, François
%< avec comité de lecture
%@ 0991-5281
%J Aquitania - Supplément
%I Pessac: Fédération Aquitania
%N 29
%P 97-142
%8 2013
%D 2013
%Z Humanities and Social Sciences/History, Philosophy and Sociology of SciencesJournal articles
%X no abstract
%G French
%L hal-01656105
%U https://hal.archives-ouvertes.fr/hal-01656105
%~ ITEM
%~ UNIV-PAU
%~ HIPHISCITECH
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T Transferts de technologie en Aquitaine méridionale à la fin du second âge du Fer et au début de l'époque romaine. L'exemple des céramiques communes
%+ Identités, Territoires, Expressions, Mobilités (ITEM)
%A Réchin, François
%< avec comité de lecture
%@ 0076-230X
%J Mélanges de la Casa de Velázquez
%I Casa de Velázquez (E. de Boccard auparavant)
%S Mélanges de la Cas de Velazquez
%N 43.1
%P 141-172
%8 2013
%D 2013
%K Cuisson oxydante
%K Fin du second Âge du Fer
%K Tour
%K Antiquité
%K Aquitaine méridionale
%K Céramiques communes
%K Transferts technologiques
%Z Humanities and Social Sciences/History, Philosophy and Sociology of Sciences
%Z Humanities and Social Sciences/Archaeology and PrehistoryJournal articles
%X Bien éloignés des schémas diffusionnistes, les différents travaux présentés dans ce dossier confortent la thèse d’un transfert technologique non linéaire entre le monde méditerranéen classique et les territoires sud-occidentaux de l’Europe, tant d’un point de vue chronologique que géographique, en insistant sur la diversité des acteurs qui peuvent y prendre part. Les articles sont organisés en trois ensembles chrono-thématiques. Le premier concerne les débuts du Ier millénaire av. J.-C. et porte sur les objets issus soit du travail du métal, soit de la transformation de matières organiques naturelles dures. Le deuxième groupe s’interroge sur l’évolution des techniques dans la production céramique durant le second âge du Ferdans les sociétés ibériques et aquitaines. Le troisième, enfin, s’intéresse à l’adoption et à l’adaptation de nouvelles pratiques en lien avec l’usage de l’écriture durant la République romaine.les sociétés ibériques et aquitaines. Le troisième, enfin, s’intéresse à l’adoption et à l’adaptation de nouvelles pratiques en lien avec l’usage de l’écriture durant la République romaine.
%G French
%L hal-01656086
%U https://hal.archives-ouvertes.fr/hal-01656086
%~ ITEM
%~ UNIV-PAU
%~ HIPHISCITECH
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T De la uilla aquitano-romaine à la seigneurie médiévale : le cas du quartier saint-Michel à Lescar
%+ Identités, Territoires, Expressions, Mobilités (ITEM)
%A Bidot-Germa, Dominique
%A Réchin, François
%A Clavet, A.
%< avec comité de lecture
%J Studies on the rural world in the Roman Period
%I Girona : Universitat de Girona
%S Uillae and domain at the end of Antiquity and the begining of Middle Age
%N 8
%P 161-178
%8 2015
%D 2015
%Z Humanities and Social Sciences/History
%Z Humanities and Social SciencesJournal articles
%G French
%L hal-02525503
%U https://hal.archives-ouvertes.fr/hal-02525503
%~ AO-HISTOIRE
%~ HISTOIRE
%~ UNIV-PAU
%~ SHS
%~ ITEM
%~ SHMESP

%0 Journal Article
%T Habitat urbain, habitat rural et usages dans le piémont occidental des Pyrénées durant la fin de l'Antiquité
%+ Identités, Territoires, Expressions, Mobilités (ITEM)
%A Réchin, François
%< avec comité de lecture
%@ 0778-9777
%J Antiquité Tardive
%I Brepols Publishers
%N 22
%P 50-61
%8 2013
%D 2013
%K Archéologie
%K Histoire antique
%K Oloron-Saint-Marie
%K Lescar
%K Morphologie urbaine
%K Habitat
%K Pyrénées Occidentales
%Z Humanities and Social Sciences/Archaeology and Prehistory
%Z Humanities and Social SciencesJournal articles
%X Les paysages urbains et ruraux de la fin de l’Antiquité ont longtemps été présentés sous des jours contrastés et souvent caricaturaux, aussi il est parfois malaisé de tenir la barre droite entre les visions extrêmes qui ont été développées. C’est pourtant ce que je tenterai de faire en m’intéressant d’une part aux changements qui ont été apportés, durant la fin de l’Antiquité, à l’habitat et d’autre part à ce que cela révèle de la façon dont les sociétés de cette époque ont aménagé leur espace et réglé leurs rapports sociaux. Mon propos sera prioritairement centré sur la période qui s’étend du dernier quart du IIIe au premier quart du Ve siècle, mais il sera nécessaire de procéder à quelques rappels ponctuels concernant la période précédente pour mieux mesurer les caractères originaux de la région considérée et l’évolution qu’elle a subie dans le domaine de l’habitat privé. Sur le plan géographique, au sein de la Novempopulanie, je m’intéresserai plus précisément au bassin de l’Adour, d’abord en raison de la cohérence géographique et culturelle particulière de cet espace. Ensuite, parce qu’il s’agit d’une zone où les relations existant entre les espaces montagnards et le piémont semblent avoir joué à plein et que l’on peut à certains égards considérer comme périphérique. Cette situation originale permet d’étudier avec intérêt la dialectique spécifique existant ici entre ces contraintes locales et les évolutions globales des sociétés antiques. A l’intérieur de cette zone, il sera surtout question des cités de Beneharnum et Iluro pour lesquelles je dispose de données de première main. Je tenterai de définir les principales innovations repérables en matière d’habitat à la fin de l’Antiquité, pour évoquer ensuite les caractères les plus saillants des paysages urbains et ruraux dans lesquels prennent place ces évolutions.
%G French
%L hal-01656093
%U https://hal.archives-ouvertes.fr/hal-01656093
%~ ITEM
%~ UNIV-PAU
%~ SHS
%~ TESTUPPA

