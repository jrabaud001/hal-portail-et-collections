%0 Journal Article
%T Energy resolution of plastic scintillation detector for beta rays
%+ Osaka University, Grad Sch Sci, Dept Phys
%+ Centre d'Etudes Nucléaires de Bordeaux Gradignan (CENBG)
%+ Wakasa Wan Energy Res Ctr, Fukui
%A Vo, H.H.
%A Kanamaru, S.
%A Marquet, Catherine
%A Nakamura, H.
%A Nomachi, M.
%A Piquemal, F.
%A Ricol, J.S.
%A Sugaya, Y.
%A Yasuda, K.
%< avec comité de lecture
%@ 0018-9499
%J IEEE Transactions on Nuclear Science
%I Institute of Electrical and Electronics Engineers
%V 55
%P 3717-3724
%8 2008-12
%D 2008
%R 10.1109/TNS.2008.2005718
%K Energy resolution
%K scintillation detectors
%K DECAYS
%K NEUTRINOS
%K MOON
%Z Physics [physics]/Nuclear Experiment [nucl-ex]Journal articles
%X Many experiments on neutrinoless double beta decays have been proposed recently for determining the effective mass of neutrinos. Some of them use a plastic scintillation for measuring the beta-ray energy. Achieving a high energy resolution is critical for neutrinoless double beta decay experiments (e.g., MOON and superNEMO). The motivation of this study is to investigate the energy resolution of a plastic scintillation detector in terms of ldquocomponentsrdquo in the MeV-electron region. It is known that the total energy resolution of a plastic scintillation detector for a monoenergetic spectrum consists of two components: statistical and intrinsic energy resolution. In this paper, these two components were investigated for electrons. Experiments on protons were also performed for the purpose of comparison. Three additional experiments were performed to determine (1) the uniformity of the plastic scintillator materials by scanning a 2.8-MeV proton microbeam, (2) the energy spread of electrons and protons, and (3) the effect of the electron-beam size on the set-up. These additional experiments were performed to ensure that these factors did not affect measurements of the two above-mentioned components.
%G English
%L in2p3-00374161
%U http://hal.in2p3.fr/in2p3-00374161
%~ IN2P3
%~ CENBG
%~ CNRS

%0 Journal Article
%T Gamma-ray flux in the Frejus underground laboratory measured with NaI detector
%+ Centre d'Etudes Nucléaires de Bordeaux Gradignan (CENBG)
%+ Institut de Recherches Subatomiques (IReS)
%+ Laboratoire de l'Accélérateur Linéaire (LAL)
%+ Laboratoire de physique corpusculaire de Caen (LPCC)
%A Ohsumi, H.
%A Gurriaran, R.
%A Hubert, P.
%A Arnold, R.
%A Augier, C.
%A Baker, J.
%A Barabash, A.
%A Bing, O.
%A Brudanin, V.
%A Caffrey, A.J.
%A Campagne, Jean-Eric
%A Caurier, E.
%A Dassie, D.
%A Egorov, V., A.
%A Errahmane, K.
%A Eschbach, R.
%A Filipova, T.
%A Guyonnet, J.L.
%A Hubert, F.
%A Jollet, C.
%A Jullian, Sophie
%A Kisel, I.
%A Klimenko, A.
%A Kochetov, O.
%A Kornoukhov, V.N.
%A Kovalenko, V., V.
%A Kuzichev, V.
%A Lalanne, D.
%A Laplanche, F.
%A Leccia, F.
%A Linck, I.
%A Longuemare, C.
%A Marquet, Catherine
%A Mauger, F.
%A Nicholson, H.W.
%A Nikolic-Audit, I.
%A Piquemal, F.
%A Reyss, J.L.
%A Sarazin, X.
%A Smolnikov, A.
%A Stekl, I.
%A Suhonen, J.
%A Sutton, C.S.
%A Szklarz, G.
%A Timkin, V.
%A Tretyak, V.
%A Umatov, V.
%A Vala, L.
%A Vanyushin, I.
%A Vareille, A.
%A Vasiliev, V.
%A Vasiliev, S.
%A Vorobel, V.
%A Vylov, Ts.
%< avec comité de lecture
%Z LAL 02-59
%@ 0168-9002
%J Nuclear Instruments and Methods in Physics Research Section A: Accelerators, Spectrometers, Detectors and Associated Equipment
%I Elsevier
%V 482
%P 832-839
%8 2002
%D 2002
%Z Physics [physics]/Nuclear Experiment [nucl-ex]Journal articles
%G English
%Z NEMO
%L in2p3-00019831
%U http://hal.in2p3.fr/in2p3-00019831
%~ LAL
%~ CENBG
%~ LPC-CAEN
%~ CNRS
%~ UNIV-STRASBG1
%~ ENSI-CAEN
%~ CGE
%~ IN2P3
%~ UNIV-STRASBG
%~ COMUE-NORMANDIE
%~ GRIFON
%~ UNIV-PSUD
%~ ENSICAEN
%~ UNICAEN
%~ SITE-ALSACE

%0 Journal Article
%T Testing the Pauli exclusion principle with the NEMO-2 detector
%+ Institut de Recherches Subatomiques (IReS)
%+ Laboratoire de l'Accélérateur Linéaire (LAL)
%+ Centre d'Etudes Nucléaires de Bordeaux Gradignan (CENBG)
%+ Laboratoire de physique corpusculaire de Caen (LPCC)
%A Arnold, R.
%A Augier, C.
%A Baker, J.
%A Barabash, A.
%A Blum, D.
%A Brudanin, V.
%A Caffrey, A.J.
%A Campagne, Jean-Eric
%A Caurier, E.
%A Dassie, D.
%A Egorov, V., A.
%A Filipova, T.
%A Gurriaran, R.
%A Guyonnet, J.L.
%A Hubert, F.
%A Hubert, P.
%A Jullian, Sophie
%A Kochetov, O.
%A Kisel, I.
%A Kornoukhov, V.N.
%A Kovalenko, V., V.
%A Lalanne, D.
%A Laplanche, F.
%A Leccia, F.
%A Linck, I.
%A Longuemare, C.
%A Marquet, Catherine
%A Mauger, F.
%A Nicholson, H.W.
%A Pilugin, I.
%A Piquemal, F.
%A Reyss, J.L.
%A Sarazin, X.
%A Scheibling, F.
%A Suhonen, J.
%A Sutton, C.S.
%A Szklarz, G.
%A Timkin, V.
%A Torres, R.
%A Tretyak, V.I.
%A Umatov, V.
%A Vanyushin, I.
%A Vareille, A.
%A Vasilyev, Y.
%< avec comité de lecture
%Z LAL 99-98
%@ 1434-6001
%J European Physical Journal A
%I EDP Sciences
%V 6
%P 361-366
%8 1999
%D 1999
%Z Physics [physics]/Nuclear Experiment [nucl-ex]Journal articles
%G English
%Z NEMO
%L in2p3-00007704
%U http://hal.in2p3.fr/in2p3-00007704
%~ LAL
%~ CENBG
%~ LPC-CAEN
%~ CNRS
%~ UNIV-STRASBG1
%~ ENSI-CAEN
%~ CGE
%~ IN2P3
%~ UNIV-STRASBG
%~ COMUE-NORMANDIE
%~ GRIFON
%~ UNIV-PSUD
%~ ENSICAEN
%~ UNICAEN
%~ SITE-ALSACE

%0 Journal Article
%T Double-beta decay of $^{82}$Se
%+ Institut de Recherches Subatomiques (IReS)
%+ Laboratoire de l'Accélérateur Linéaire (LAL)
%+ Centre d'Etudes Nucléaires de Bordeaux Gradignan (CENBG)
%+ Laboratoire de physique corpusculaire de Caen (LPCC)
%A Arnold, R.
%A Augier, C.
%A Baker, J.
%A Barabash, A.
%A Blum, D.
%A Brudanin, V.
%A Caffrey, A.J.
%A Campagne, Jean-Eric
%A Caurier, E.
%A Dassie, D.
%A Egorov, V., A.
%A Eschbach, R.
%A Filipova, T.
%A Gurriaran, R.
%A Guyonnet, J.L.
%A Hubert, F.
%A Hubert, P.
%A Jullian, Sophie
%A Kisel, I.
%A Kochetov, O.
%A Kornoukhov, V.N.
%A Kovalenko, V., V.
%A Lalanne, D.
%A Laplanche, F.
%A Leccia, F.
%A Linck, I.
%A Longuemare, C.
%A Marquet, Catherine
%A Mauger, F.
%A Mennrath, P.
%A Nicholson, H.W.
%A Pilugin, I.
%A Piquemal, F.
%A Purtov, O.
%A Reyss, J-L.
%A Sarazin, X.
%A Scheibling, F.
%A Suhonen, J.
%A Sutton, C.S.
%A Szklarz, G.
%A Timkin, V.
%A Torres, R.
%A Tretyak, V.I.
%A Umatov, V.
%A Vanyushin, I.
%A Vareille, A.
%A Vaslyev, Y.
%A Vylov, T.
%A Zerkin, V.
%< avec comité de lecture
%Z LAL 98-84
%@ 0375-9474
%J Nuclear Physics A
%I Elsevier
%V 636
%P 209-223
%8 1998
%D 1998
%Z Physics [physics]/Nuclear Experiment [nucl-ex]Journal articles
%G English
%Z NEMO
%L in2p3-00005055
%U http://hal.in2p3.fr/in2p3-00005055
%~ LAL
%~ CENBG
%~ LPC-CAEN
%~ CNRS
%~ UNIV-STRASBG1
%~ ENSI-CAEN
%~ CGE
%~ IN2P3
%~ UNIV-STRASBG
%~ COMUE-NORMANDIE
%~ GRIFON
%~ UNIV-PSUD
%~ ENSICAEN
%~ UNICAEN
%~ SITE-ALSACE

%0 Journal Article
%T Influence of neutrons and $\gamma$-rays in the Frejus underground laboratory on the NEMO experiment
%+ Centre d'Etudes Nucléaires de Bordeaux Gradignan (CENBG)
%+ Institut de Recherches Subatomiques (IReS)
%+ Laboratoire de l'Accélérateur Linéaire (LAL)
%+ Laboratoire de physique corpusculaire de Caen (LPCC)
%A Marquet, Catherine
%A Piquemal, F.
%A Arnold, R.
%A Augier, C.
%A Baker, J.
%A Barabash, A.
%A Bing, O.
%A Blum, D.
%A Brudanin, V.
%A Caffrey, J.
%A Campagne, Jean-Eric
%A Caurier, E.
%A Dassie, D.
%A Egorov, V., A.
%A Errahmane, K.
%A Eschbach, R.
%A Filipova, T.
%A Guyonnet, J.L.
%A Jollet, C.
%A Jullian, Sophie
%A Hubert, F.
%A Hubert, P.
%A Kisel, I.
%A Kochetov, O.
%A Kornoukhov, V.N.
%A Kovalenko, V., V.
%A Lalanne, D.
%A Laplanche, D.
%A Leccia, F.
%A Linck, I.
%A Longuemare, C.
%A Mauger, F.
%A Nicholson, H.W.
%A Nikolic-Audit, I.
%A Ohsumi, H.
%A Pilugin, I.
%A Reyss, J.L.
%A Sarazin, X.
%A Scheibling, F.
%A Stekl, I.
%A Suhonen, J.
%A Sutton, C.S.
%A Szklarz, G.
%A Timkin, V.
%A Tretyak, V.
%A Umatov, V.
%A Vala, L.
%A Vanyushin, I.
%A Vareille, A.
%A Vorobel, V.
%A Vylov, T.
%< avec comité de lecture
%Z LAL 01-08
%@ 0168-9002
%J Nuclear Instruments and Methods in Physics Research Section A: Accelerators, Spectrometers, Detectors and Associated Equipment
%I Elsevier
%V 457
%P 487-498
%8 2001
%D 2001
%Z Physics [physics]/Nuclear Experiment [nucl-ex]Journal articles
%G English
%Z NEMO
%L in2p3-00018889
%U http://hal.in2p3.fr/in2p3-00018889
%~ LAL
%~ CENBG
%~ LPC-CAEN
%~ CNRS
%~ UNIV-STRASBG1
%~ ENSI-CAEN
%~ CGE
%~ IN2P3
%~ UNIV-STRASBG
%~ COMUE-NORMANDIE
%~ GRIFON
%~ UNIV-PSUD
%~ ENSICAEN
%~ UNICAEN
%~ SITE-ALSACE

%0 Journal Article
%T Double beta decay of $^{96}$Zr
%+ Institut de Recherches Subatomiques (IReS)
%+ Laboratoire de l'Accélérateur Linéaire (LAL)
%+ Centre d'Etudes Nucléaires de Bordeaux Gradignan (CENBG)
%+ Laboratoire de physique corpusculaire de Caen (LPCC)
%A Arnold, R.
%A Augier, C.
%A Baker, J.
%A Barabash, A.
%A Blum, D.
%A Brudanin, V.
%A Caffrey, A.J.
%A Campagne, Jean-Eric
%A Caurier, E.
%A Dassie, D.
%A Egorov, V., A.
%A Filipova, T.
%A Gurriaran, R.
%A Guyonnet, J.L.
%A Hubert, F.
%A Hubert, P.
%A Jullian, Sophie
%A Kisel, I.
%A Kochetov, O.
%A Kornoukhov, V.N.
%A Kovalenko, V., V.
%A Lalanne, D.
%A Laplanche, F.
%A Leccia, F.
%A Linck, I.
%A Longuemare, C.
%A Marquet, Catherine
%A Mauger, F.
%A Nicholson, H.W.
%A Pilugin, I.
%A Piquemal, F.
%A Reyss, J.L.
%A Sarazin, X.
%A Scheibling, F.
%A Suhonen, J.
%A Sutton, C.S.
%A Szklarz, G.
%A Timkin, V.
%A Torres, R.
%A Tretyak, V.I.
%A Umatov, V.
%A Vanyushin, I.
%A Vareille, A.
%A Vasilyev, Y.
%A Vylov, T.
%< avec comité de lecture
%Z LAL 99-99
%@ 0375-9474
%J Nuclear Physics A
%I Elsevier
%V 658
%P 299-312
%8 1999
%D 1999
%Z Physics [physics]/High Energy Physics - Experiment [hep-ex]Journal articles
%G English
%Z NEMO
%L in2p3-00009756
%U http://hal.in2p3.fr/in2p3-00009756
%~ LAL
%~ CENBG
%~ LPC-CAEN
%~ CNRS
%~ UNIV-STRASBG1
%~ ENSI-CAEN
%~ CGE
%~ IN2P3
%~ UNIV-STRASBG
%~ COMUE-NORMANDIE
%~ GRIFON
%~ UNIV-PSUD
%~ ENSICAEN
%~ UNICAEN
%~ SITE-ALSACE

%0 Journal Article
%T Limits on different Majoron decay modes of $^{100}$Mo, $^{116}$Cd, $^{82}$Se and $^{96}$Zr for neutrinoless double beta decays in the NEMO-2 experiment
%+ Institut de Recherches Subatomiques (IReS)
%+ Laboratoire de l'Accélérateur Linéaire (LAL)
%+ Centre d'Etudes Nucléaires de Bordeaux Gradignan (CENBG)
%+ Laboratoire de physique corpusculaire de Caen (LPCC)
%A Arnold, R.
%A Augier, C.
%A Baker, J.
%A Barabash, A.
%A Blum, D.
%A Brudanin, V.
%A Caffrey, A.J.
%A Campagne, Jean-Eric
%A Caurier, E.
%A Dassie, D.
%A Egorov, V., A.
%A Filipova, T.
%A Gurriaran, R.
%A Guyonnet, J.L.
%A Hubert, F.
%A Hubert, P.
%A Jullian, Sophie
%A Kisel, I.
%A Kochetov, O.
%A Kornoukhov, V.N.
%A Kovalenko, V., V.
%A Lalanne, D.
%A Laplanche, F.
%A Leccia, F.
%A Linck, I.
%A Longuemare, C.
%A Marquet, Catherine
%A Mauger, F.
%A Nicholson, H.W.
%A Pilugin, I.
%A Piquemal, F.
%A Reyss, J-L.
%A Sarazin, X.
%A Scheibling, F.
%A Suhonen, J.
%A Sutton, C.S.
%A Szklarz, G.
%A Timkin, V.
%A Torres, R.
%A Umatov, V.
%A Vanyushin, I.
%A Vareille, A.
%A Vasilyev, V.
%A Vylov, Ts.
%< avec comité de lecture
%Z LAL 00-116
%@ 0375-9474
%J Nuclear Physics A
%I Elsevier
%V 678
%P 341-352
%8 2000
%D 2000
%Z Physics [physics]/Nuclear Experiment [nucl-ex]Journal articles
%G English
%Z NEMO
%L in2p3-00017744
%U http://hal.in2p3.fr/in2p3-00017744
%~ LAL
%~ CENBG
%~ LPC-CAEN
%~ CNRS
%~ UNIV-STRASBG1
%~ ENSI-CAEN
%~ CGE
%~ IN2P3
%~ UNIV-STRASBG
%~ COMUE-NORMANDIE
%~ GRIFON
%~ UNIV-PSUD
%~ ENSICAEN
%~ UNICAEN
%~ SITE-ALSACE

%0 Journal Article
%T Limits on different majoron decay modes of $^{100}$Mo, $^{116}$Cd, $^{82}$Se and $^{96}$Zr for neutrinoless double beta decays in the NEMO-2 experiment
%+ Institut de Recherches Subatomiques (IReS)
%+ Laboratoire de l'Accélérateur Linéaire (LAL)
%+ Centre d'Etudes Nucléaires de Bordeaux Gradignan (CENBG)
%+ Laboratoire de physique corpusculaire de Caen (LPCC)
%A Vasilev, V.
%A Arnold, R.
%A Augier, C.
%A Baker, J.
%A Barabash, A.
%A Blum, D.
%A Brudanin, V.
%A Caffrey, A.J.
%A Campagne, Jean-Eric
%A Caurier, E.
%A Dassie, D.
%A Egorov, V., A.
%A Filipova, T.
%A Gurriaran, R.
%A Guyonnet, J.L.
%A Hubert, F.
%A Hubert, P.
%A Jullian, Sophie
%A Kisel, I.
%A Kochetov, O.
%A Kornoukhov, V.N.
%A Kovalenko, V., V.
%A Lalanne, D.
%A Laplanche, F.
%A Leccia, F.
%A Linck, I.
%A Longuemare, C.
%A Marquet, Catherine
%A Mauger, F.
%A Nicholson, H.W.
%A Pilugin, I.
%A Piquemal, F.
%A Reyss, J.L.
%A Sarazin, X.
%A Scheibling, F.
%A Suhonen, J.
%A Sutton, C.S.
%A Szklarz, G.
%A Timkin, V.
%A Torres, R.
%A Umatov, V.
%A Vanyushin, I.
%A Vareille, A.
%A Vylov, Ts.
%< avec comité de lecture
%Z LAL 01-117
%J Particles and Nuclei, Letters
%V 108
%P 68-79
%8 2001
%D 2001
%Z Physics [physics]/Nuclear Experiment [nucl-ex]Journal articles
%G English
%Z NEMO
%L in2p3-00019985
%U http://hal.in2p3.fr/in2p3-00019985
%~ LAL
%~ CENBG
%~ CNRS
%~ UNIV-STRASBG1
%~ CGE
%~ IN2P3
%~ UNIV-STRASBG
%~ LPC-CAEN
%~ ENSI-CAEN
%~ COMUE-NORMANDIE
%~ GRIFON
%~ UNIV-PSUD
%~ ENSICAEN
%~ UNICAEN
%~ SITE-ALSACE

%0 Journal Article
%T On the stability of hematopoietic model with feedback control
%+ Multi-scale modelling of cell dynamics : application to hematopoiesis (DRACULA)
%+ Institut Camille Jordan [Villeurbanne] (ICJ)
%+ LMA
%A Adimy, Mostafa
%A Marquet, Catherine
%< avec comité de lecture
%@ 1631-073X
%J Comptes Rendus Mathématique
%I Elsevier Masson
%V 350
%N 3-4
%P 173-176
%8 2012-02-01
%D 2012
%R 10.1016/j.crma.2012.01.014Journal articles
%X We propose and analyze a mathematical model of the production and regulation of blood cell population in the bone marrow (hematopoiesis). This model includes the primitive hematopoietic stem cells (PHSC), the three lineages of their progenitors and the corresponding mature blood cells (red blood cells, white cells and platelets). The resulting mathematical model is a nonlinear system of differential equations with several delays corresponding to the cell cycle durations for each type of cells. We investigate the local asymptotic stability of the trivial steady state by analyzing the roots of the characteristic equation. We also prove by a Lyapunov function the global asymptotic stability of this steady state. This situation illustrates the extinction of the cell population in some pathological cases.
%X Nous proposons et étudions un modèle mathématique de production et régulation des cellules sanguines dans la moelle osseuse (hématopoïèse). Ce modèle décrit la dynamique des cellules souches hématopoïétiques primitives (PHSC), les trois lignées de cellules souches progéniteurs quʼelles génèrent ainsi que les cellules matures correspondantes (globules rouges, globules blancs et plaquettes). Le modèle mathématique obtenu est un système non linéaire dʼéquations différentielles avec plusieurs retards représentant les durées de cycles cellulaires de chaque type de cellules. Nous étudions la stabilité locale du point dʼéquilibre trivial par lʼétude de lʼéquation caractéristique, puis nous prouvons sa stabilité globale par la méthode de Lyapunov. Ce résultat illustre lʼextinction de la population des cellules dans certains cas pathologiques.
%G English
%L hal-00763154
%U https://hal.inria.fr/hal-00763154
%~ CNRS
%~ INRIA
%~ INRIA-RHA
%~ LMA-PAU
%~ ICJ
%~ UNIV-ST-ETIENNE
%~ EC-LYON
%~ CGPHIMC
%~ UNIV-PAU
%~ INRIA_TEST
%~ INRIA2
%~ INSA-LYON
%~ UNIV-LYON1
%~ INRIA-RENGRE
%~ INSA-GROUPE
%~ UDL
%~ UNIV-LYON

%0 Journal Article
%T Center manifolds for some partial functional differential equations with infinite delay in fading memory spaces
%+ Multi-scale modelling of cell dynamics : application to hematopoiesis (DRACULA)
%+ Institut Camille Jordan [Villeurbanne] (ICJ)
%+ Department of Mathematics [Marrakech] ([MRC-CAD])
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%A Adimy, Mostafa
%A Ezzinbi, Khalil
%A Marquet, Catherine
%< avec comité de lecture
%@ 1559-176X
%J Journal of Concrete and Applicable Mathematics
%I Eudoxus Press
%V 10
%N 3-4
%P 168-185
%8 2012
%D 2012Journal articles
%G English
%L hal-00763166
%U https://hal.inria.fr/hal-00763166
%~ CNRS
%~ INRIA
%~ INRIA-RHA
%~ LMA-PAU
%~ ICJ
%~ UNIV-ST-ETIENNE
%~ EC-LYON
%~ INRIA2
%~ UNIV-PAU
%~ CGPHIMC
%~ INRIA_TEST
%~ INSA-LYON
%~ UNIV-LYON1
%~ INRIA-RENGRE
%~ INSA-GROUPE
%~ UDL
%~ UNIV-LYON

%0 Journal Article
%T Ergodic and weighted pseudo-almost periodic solutions for partial functional differential equations in fading memory spaces
%+ Multi-scale modelling of cell dynamics : application to hematopoiesis (DRACULA)
%+ Institut Camille Jordan [Villeurbanne] (ICJ)
%+ Modélisation mathématique, calcul scientifique (MMCS)
%+ Department of Mathematics [Marrakech] ([MRC-CAD])
%+ LMA
%A Adimy, Mostafa
%A Ezzinbi, Khalil
%A Marquet, Catherine
%< avec comité de lecture
%@ 1865-2085
%J Journal of Applied Mathematics and Computing
%I Springer
%V 44
%N 1-2
%P 19
%8 2014
%D 2014
%R 10.1007/s12190-013-0686-9
%K Fading memory spaces
%K Weighted pseudo almost periodic functions
%K Ergodic functions
%K Inﬁnite delay
%K Partial functional differential equations
%K Exponential dichotomy
%Z 34K30 ; 34K14 ; 34K20 ; 47D06
%Z Mathematics [math]/Dynamical Systems [math.DS]Journal articles
%X We use a new concept of weighted ergodic function based on the measure theory to investigate the existence and uniqueness of weighted pseudo almost periodic solution for a class of partial functional differential equations with inﬁnite delay in fading memory spaces. We illustrate our theoretical results by studying some Lotka-Voltera reaction-diffusion systems with inﬁnite delay.
%G English
%L hal-00846883
%U https://hal.inria.fr/hal-00846883
%~ CNRS
%~ INRIA
%~ INRIA-RHA
%~ LMA-PAU
%~ INSMI
%~ ICJ
%~ UNIV-ST-ETIENNE
%~ EC-LYON
%~ CGPHIMC
%~ UNIV-PAU
%~ INRIA_TEST
%~ TDS-MACS
%~ INRIA2
%~ INSA-LYON
%~ UNIV-LYON1
%~ INRIA-RENGRE
%~ INSA-GROUPE
%~ ICJ-MMCS
%~ UDL
%~ UNIV-LYON

%0 Journal Article
%T Energy resolution of plastic scintillation counters for beta rays
%+ Centre d'Etudes Nucléaires de Bordeaux Gradignan (CENBG)
%A Vo, H.H.
%A Kanamaru, S.
%A Marquet, Catherine
%A Nakamura, H.
%A Nomachi, N.
%A Piquemal, F.
%A Ricol, J.S.
%A Sugaya, Y.
%A Yasuda, K.
%< avec comité de lecture
%J ieee
%V 2
%P 1378-1385
%8 2007-10-26
%D 2007
%R 10.1109/NSSMIC.2007.4437257
%Z Physics [physics]/High Energy Physics - Experiment [hep-ex]Journal articles
%X Recent experiments of neutrino-less double beta decays have been developing for studying the effective mass of neutrinos. Some of them use plastic scintillator for detecting the beta ray energy. Good energy resolution is a key for the neutrino- less double beta decay experiments (MOON, superNEMO, etc.). The motivation in this work is to study the energy resolution of a plastic scintillation counter in terms of "components" in the MeV-electron region. It is known that total energy resolution of a scintillation counter for a mono-energetic spectrum is separated into two components (statistical and non-statistical components). This paper presents for studying these two components on a plastic scintillation counter for electrons. Experiments on protons are also performed to compare to electron data. Experimental results point out that the statistical component is inversely proportional to the square root of the number of photo-electrons. In the case of electrons, data set of the non-statistical component described as approximately inversely proportional to the square root of electron energies. Three additional experiments are needed to carry out, (such as measuring (1) the uniformity of the plastic scintillator materials, BC-408, by scanning a 2.8-MeV proton micro-beam, (2) the energy spread of electrons and protons, and (3) the effect of electron-beam size on the set-up), to be sure that they do not affect measuring these two components. This present work is useful for investigating as well as improving the energy resolution of a plastic scintillation counter through statistical and non-statistical components.
%G English
%L in2p3-00330659
%U http://hal.in2p3.fr/in2p3-00330659
%~ CENBG
%~ IN2P3
%~ CNRS

%0 Journal Article
%T Monte-Carlo simulations of neutron shielding for the ATLAS forward region
%+ Centre d'Etudes Nucléaires de Bordeaux Gradignan (CENBG)
%A Stekl, I.
%A Pospisil, S.
%A Kovalenko, V.E.
%A Vorobel, V.
%A Leroy, C.
%A Piquemal, F.
%A Eschbach, R.
%A Marquet, Catherine
%< avec comité de lecture
%@ 0168-9002
%J Nuclear Instruments and Methods in Physics Research Section A: Accelerators, Spectrometers, Detectors and Associated Equipment
%I Elsevier
%V 452
%P 458-469
%8 2000
%D 2000
%Z Physics [physics]/Astrophysics [astro-ph]/Cosmology and Extra-Galactic Astrophysics [astro-ph.CO]Journal articles
%G English
%L in2p3-00022453
%U http://hal.in2p3.fr/in2p3-00022453
%~ CENBG
%~ CNRS
%~ IN2P3

%0 Journal Article
%T Monte-carlo study of different concepts for the atlas experiment forward region shielding
%+ Centre d'Etudes Nucléaires de Bordeaux Gradignan (CENBG)
%A Stekl, I.
%A Bedajanek, I.
%A Eschbach, R.
%A Kovalenko, V.E.
%A Leroy, C.
%A Marquet, Catherine
%A Palla, J.
%A Piquemal, F.
%A Pospisil, S.
%A Shupe, M.
%A Sodomka, J.
%A Tourneur, S.
%A Vorobel, V.
%< avec comité de lecture
%@ 0168-9002
%J Nuclear Instruments and Methods in Physics Research Section A: Accelerators, Spectrometers, Detectors and Associated Equipment
%I Elsevier
%V 493
%P 199-207
%8 2002
%D 2002
%Z Physics [physics]/Astrophysics [astro-ph]/Cosmology and Extra-Galactic Astrophysics [astro-ph.CO]Journal articles
%G English
%L in2p3-00022323
%U http://hal.in2p3.fr/in2p3-00022323
%~ CENBG
%~ CNRS
%~ IN2P3

%0 Journal Article
%T Improved limits on β+EC and ECEC processes in Sn112
%+ Institute of Theoretical and Experimental Physics (ITEP)
%+ Centre d'Etudes Nucléaires de Bordeaux Gradignan (CENBG)
%A Barabash, A.S.
%A Hubert, P.
%A Marquet, Catherine
%A Nachab, A.
%A Konovalov, S. I.
%A Perrot, F.
%A Piquemal, F.
%A Umatov, V.
%< avec comité de lecture
%@ 0556-2813
%J Physical Review C
%I American Physical Society
%V 83
%P 045503
%8 2011-04-21
%D 2011
%R 10.1103/PhysRevC.83.045503
%K Beta decay
%K double beta decay
%K electron and muon capture
%K Neutrino mass and mixing
%K 90<=A<=149
%Z 23.40.-s, 14.60.Pq, 27.60.+j
%Z Physics [physics]/Nuclear Experiment [nucl-ex]Journal articles
%X Limits on β+EC and ECEC processes in Sn112 have been obtained using a 380 cm3 HPGe detector and an external source consisting of 100 g enriched tin (94.32% of Sn112). A limit with 90% C.L. on the Sn112 half-life of 1.3×1021 yr for the ECEC(0ν) transition to the 03+ excited state in Cd112 (1871 keV) has been established. This transition has been discussed in the context of a possible enhancement of the decay rate. The limits on other β+EC and ECEC processes in Sn112 have also been obtained on the level of (0.1-1.6)×1021 yr at the 90% C.L.
%G English
%L in2p3-00605516
%U http://hal.in2p3.fr/in2p3-00605516
%~ IN2P3
%~ CENBG
%~ CNRS

%0 Journal Article
%T Asymptotic behavior and stability switch for a mature-immature model of cell differentiation
%+ Multi-scale modelling of cell dynamics : application to hematopoiesis (DRACULA)
%+ Institut Camille Jordan [Villeurbanne] (ICJ)
%+ Laboratoire de Mathématiques appliquées de Pau (LMAP)
%A Adimy, Mostafa
%A Crauste, Fabien
%A Marquet, Catherine
%< avec comité de lecture
%@ 1468-1218
%J Nonlinear Analysis: Real World Applications
%I Elsevier
%V 11
%N 4
%P 2913-2929
%8 2010
%D 2010
%R 10.1016/j.nonrwa.2009.11.001
%Z Life Sciences [q-bio]/Quantitative Methods [q-bio.QM]
%Z Computer Science [cs]/Bioinformatics [q-bio.QM]
%Z Mathematics [math]/Analysis of PDEs [math.AP]Journal articles
%X We consider a nonlinear age-structured model, inspired by hematopoiesis modelling, describing the dynamics of a cell population divided into mature and immature cells. Immature cells, that can be either proliferating or non-proliferating, differentiate in mature cells, that in turn control the immature cell population through a negative feedback. We reduce the system to two delay differential equations, and we investigate the asymptotic stability of the trivial and the positive steady states. By constructing a Lyapunov function, the trivial steady state is proven to be globally asymptotically stable when it is the only equilibrium of the system. The asymptotic stability of the positive steady state is related to a delay-dependent characteristic equation. Existence of a Hopf bifurcation and stability switch for the positive steady state is established. Numerical simulations illustrate the stability results.
%G English
%L hal-00542644
%U https://hal.archives-ouvertes.fr/hal-00542644
%~ CNRS
%~ INRIA
%~ UNIV-PAU
%~ INRIA-RHA
%~ INSMI
%~ ICJ
%~ UNIV-ST-ETIENNE
%~ EC-LYON
%~ TDS-MACS
%~ CGPHIMC
%~ INRIA_TEST
%~ INRIA2
%~ INSA-LYON
%~ UNIV-LYON1
%~ INRIA-RENGRE
%~ INSA-GROUPE
%~ UDL
%~ UNIV-LYON

