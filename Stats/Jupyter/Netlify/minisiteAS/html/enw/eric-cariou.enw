%0 Journal Article
%T Adaptation d'exécution de Modèles Par Contrats
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%+ Département Informatique (INF)
%+ Services répartis, Architectures, MOdélisation, Validation, Administration des Réseaux (SAMOVAR)
%+ Institut Supérieur d'Informatique et de Mathématiques de Monastir (ISIMM)
%+ Centre National de la Recherche Scientifique (CNRS)
%A Cariou, Eric
%A La Goaer, Olivier
%A Barbier, Franck
%A Pierre, Samson
%A Graiet, Mohamed
%< avec comité de lecture
%@ 0752-4072
%J Revue des Sciences et Technologies de l'Information - Série TSI : Technique et Science Informatiques
%I Lavoisier
%V 34
%N 6
%P 703-730
%8 2015-12
%D 2015
%R 10.3166/tsi.34.703-730
%K IDM
%K exécution de modèles
%K adaptation
%K contrats
%K machines à états
%Z Computer Science [cs]Journal articles
%X Dans le contexte de l’ingénierie des modèles, l’exécution de modèles est un des moyens principaux pour supprimer le fossé entre le code, c’est-à-dire le système développé, et le modèle. Pour adapter une exécution de modèles, nous proposons de directement adapter le modèle en cours d’exécution. Cela oblige à rajouter des éléments nécessaires à l’adaptation dans le modèle mais évite d’avoir un second modèle dédié à l’adaptation comme c’est le cas pour les méthodes de type models@run.time. Cet article présente une approche d’adaptation directe d’exécution de modèle par contrats. Si des conditions du contrat ne sont pas respectées, une action d’adaptation doit être entreprise. L’approche est illustrée par un i-DSML (interpreted Domain Speciﬁc Modeling Language) de machines à états et un exemple de signalisation ferroviaire avec comme problématique de s’assurer qu’un modèle est adapté à un environnement d’exécution donné.
%G French
%L hal-01906772
%U https://hal-univ-pau.archives-ouvertes.fr/hal-01906772
%~ UNIV-PAU
%~ LIUPPA
%~ CNRS
%~ INSTITUT-TELECOM
%~ TELECOM-SUDPARIS
%~ TELECOM-SUDPARIS-SACLAY
%~ UNIV-PARIS-SACLAY
%~ INSTITUTS-TELECOM

%0 Journal Article
%T Ingénierie des modèles : panorama et tendances
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%A Cariou, Eric
%A Barbier, Franck
%< avec comité de lecture
%J Génie Logiciel GL & IS
%P ;
%8 2008
%D 2008Journal articles
%G French
%L hal-00384399
%U https://hal.archives-ouvertes.fr/hal-00384399
%~ UNIV-PAU
%~ LIUPPA
%~ TESTUPPA

%0 Journal Article
%T Software Adaptation: Classification and a Case Study with State Chart XML
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%A Barbier, Franck
%A Cariou, Eric
%A Le Goaer, Olivier
%A Pierre, Samson
%< avec comité de lecture
%@ 0740-7459
%J IEEE Software
%I Institute of Electrical and Electronics Engineers
%V 32
%N 5
%P 68-76
%8 2015
%D 2015
%R 10.1109/MS.2014.130
%Z Computer Science [cs]Journal articles
%X Software adaptation has become prominent owing to the proliferation of software in everyday devices. In particular, computing with the Internet of Things requires adaptability. Traditional software maintenance, which involves long, energy-consuming cycles, is no longer satisfactory. Adaptation is a lightweight software evolution that provides more transparent maintenance for users. This article classifies types of adaptation and describes an implementation of it. \textcopyright 1984-2012 IEEE.
%G English
%L hal-01906773
%U https://hal-univ-pau.archives-ouvertes.fr/hal-01906773
%~ UNIV-PAU
%~ LIUPPA
%~ TESTUPPA

%0 Journal Article
%T OCL Contracts for the Verification of Model Transformations
%+ Laboratoire Informatique de l'Université de Pau et des Pays de l'Adour (LIUPPA)
%A Cariou, Eric
%A Belloir, Nicolas
%A Barbier, Franck
%A Djemam, Nidal
%< avec comité de lecture
%@ 1863-2122
%J Electronic Communications of the EASST
%I European Association of Software Science and Technology e.V
%V 24
%8 2009
%D 2009
%Z Computer Science [cs]Journal articles
%G English
%L hal-01906776
%U https://hal-univ-pau.archives-ouvertes.fr/hal-01906776
%~ UNIV-PAU
%~ LIUPPA
%~ TESTUPPA

