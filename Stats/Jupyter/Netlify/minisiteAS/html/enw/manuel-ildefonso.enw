%0 Journal Article
%T A droplet-based millifluidic method for studying ice and gas hydrate nucleation
%+ Laboratoire des Fluides Complexes et leurs Réservoirs (LFCR)
%+ Université de Pau et des Pays de l'Adour (UPPA)
%A Atig, D.
%A Touil, A.
%A Ildefonso, Manuel
%A Marlin, L.
%A Bouriat, Patrick
%A Broseta, Daniel
%Z ACL
%< avec comité de lecture
%@ 0009-2509
%J Chemical Engineering Science
%I Elsevier
%V 192
%P 1189--1197
%8 2018
%D 2018
%Z Engineering Sciences [physics]
%Z Physics [physics]/Physics [physics]/Chemical Physics [physics.chem-ph]
%Z Physics [physics]/Mechanics [physics]
%Z Sciences of the Universe [physics]/Earth Sciences/Applied geology
%Z Sciences of the Universe [physics]/Earth Sciences/Geophysics [physics.geo-ph]Journal articles
%G English
%L hal-02073856
%U https://hal.archives-ouvertes.fr/hal-02073856
%~ CNRS
%~ UNIV-PAU
%~ GIP-BE
%~ LFCR
%~ LFCR-ISD
%~ TESTUPPA

%0 Journal Article
%T Small-volumes nucleation
%+ Centre Interdisciplinaire de Nanoscience de Marseille (CINaM)
%A Hammadi, Z.
%A Candoni, N.
%A Grossier, R.
%A Ildefonso, Manuel
%A Morin, R.
%A Veesler, S.
%< avec comité de lecture
%@ 1631-0705
%J Comptes Rendus Physique
%I Centre Mersenne
%V 14
%P 192-198
%8 2013
%D 2013Journal articles
%G English
%L hal-00810510
%U https://hal.archives-ouvertes.fr/hal-00810510
%~ CNRS
%~ UNIV-AMU
%~ CINAM

%0 Journal Article
%T Using microfluidics for fast, accurate measurement of lysozyme nucleation kinetics
%+ Centre Interdisciplinaire de Nanoscience de Marseille (CINaM)
%A Ildefonso, Manuel
%A Candoni, N.
%A Veesler, S.
%< avec comité de lecture
%Z UPR3118_885
%@ 1528-7483
%J Crystal Growth & Design
%I American Chemical Society
%V 11
%N 5
%P 1527-1530
%8 2011
%D 2011Journal articles
%G English
%L hal-00603265
%U https://hal.archives-ouvertes.fr/hal-00603265
%~ CNRS
%~ UNIV-AMU
%~ CINAM

%0 Journal Article
%T Heterogeneous nucleation in droplet-based nucleation measurements
%+ Centre Interdisciplinaire de Nanoscience de Marseille (CINaM)
%A Ildefonso, Manuel
%A Candoni, N.
%A Veesler, S.
%< avec comité de lecture
%Z UPR3118_1109
%@ 1528-7483
%J Crystal Growth & Design
%I American Chemical Society
%V 13
%P 2107-2110
%8 2013
%D 2013Journal articles
%G English
%L hal-00843742
%U https://hal.archives-ouvertes.fr/hal-00843742
%~ CNRS
%~ UNIV-AMU
%~ CINAM

%0 Journal Article
%T Nucleation Control and Rapid Growth of KDP Crystals in Stationary Conditions
%+  Optique et Matériaux (OPTIMA)
%+ SSP
%+ European Synchrotron Radiation Facility (ESRF)
%A Leroudier, Julien
%A Zaccaro, Julien
%A Ildefonso, Manuel
%A Veesler, S.
%A Baruchel, Jose
%A Ibanez, Alain
%< avec comité de lecture
%Z UPR3118_908
%@ 1528-7483
%J Crystal Growth & Design
%I American Chemical Society
%V 11
%N 6
%P 2592-2598
%8 2011-05-02
%D 2011
%R 10.1021/cg200342w
%Z Physics [physics]/Condensed Matter [cond-mat]/Materials Science [cond-mat.mtrl-sci]
%Z Chemical Sciences/Material chemistryJournal articles
%X Thanks to an improved control of the nucleation, an original crystal growth method operating in stationary thermodynamic conditions has been developed. It allows rapid growth of crystals at constant temperature and supersaturation. As a case study, large KH2PO4 (KDP) crystals of high purity have been reproducibly obtained at constant temperatures and growth rates of 1 cm/day. Recorded X-ray diffraction topographs of as-grown crystals showed high crystal quality. This method opens the way for the rapid growth of high quality crystals for homogeneous intermediate compositions of solid solutions, doped crystals, and compounds exhibiting a weak solubility.
%G English
%L hal-00852289
%U https://hal.archives-ouvertes.fr/hal-00852289
%~ CNRS
%~ NEEL-MCMF-MATONLP
%~ NEEL
%~ NEEL-MCMF
%~ UNIV-AMU
%~ NEEL-OPTIMA
%~ CINAM
%~ UNIV-GRENOBLE1
%~ INPG
%~ UGA

%0 Journal Article
%T Nucleation and polymorphism explored via an easy-to-use microfluidic tool
%+ Centre Interdisciplinaire de Nanoscience de Marseille (CINaM)
%A Ildefonso, Manuel
%A Revalor, E.
%A Punniam, P.
%A Salmon, Jean-Baptiste
%A Candoni, N.
%A Veesler, S.
%< avec comité de lecture
%@ 0022-0248
%J Journal of Crystal Growth
%I Elsevier
%V 342
%N 1
%P 9-12
%8 2012
%D 2012Journal articles
%G English
%L hal-00697511
%U https://hal.archives-ouvertes.fr/hal-00697511
%~ CNRS
%~ UNIV-AMU
%~ CINAM

%0 Journal Article
%T A Cheap, Easy Microfluidic Crystallization Device Ensuring Universal Solvent Compatibility
%+ Centre Interdisciplinaire de Nanoscience de Marseille (CINaM)
%A Ildefonso, Manuel
%A Candoni, N.
%A Veesler, S.
%< avec comité de lecture
%@ 1083-6160
%J Organic Process Research and Development
%I American Chemical Society
%V 16
%N 4
%P 556-560
%8 2012
%D 2012
%R 10.1021/op200291z
%Z Chemical SciencesJournal articles
%X Microfluidic devices are increasingly used for the screening of crystallization conditions. Their advantage is the generation of droplets, every droplet being an independent crystallizer with volumes in the nanoliter range. This enables a large number of experiments to be carried out under identical conditions necessitating only small quantities of materials. However, classic microfluidic crystallization devices are made of poly(dimethylsiloxane), only compatible with an aqueous medium. In addition, they generally involve very complicated setups, often inaccessible to nonmicrofluidics specialists. In this paper, we overcome these drawbacks, presenting a cheap and universally applicable microfluidic crystallization tool. This thermostatted device makes it possible to study nucleation in both aqueous and organic solvents, rendering microfluidic devices applicable to organic molecules such as APIs, explosives, and metal oxide nanoparticles.
%G English
%L hal-00697535
%U https://hal.archives-ouvertes.fr/hal-00697535
%~ CNRS
%~ UNIV-AMU
%~ CINAM

%0 Journal Article
%T Nanotechnologies dedicated to nucleation control
%+ Centre Interdisciplinaire de Nanoscience de Marseille (CINaM)
%A Candoni, N.
%A Grossier, R.
%A Ildefonso, Manuel
%A Revalor, E.
%A Ferté, N.
%A Okutsu, T.
%A Morin, R.
%A Veesler, S.
%< avec comité de lecture
%@ 1475-7435
%J International Journal of Nanotechnology
%I Inderscience
%V 9
%N 3-7
%P 439-459
%8 2012
%D 2012Journal articles
%G English
%L hal-00696846
%U https://hal.archives-ouvertes.fr/hal-00696846
%~ CNRS
%~ UNIV-AMU
%~ CINAM
%~ TEST-AMU

