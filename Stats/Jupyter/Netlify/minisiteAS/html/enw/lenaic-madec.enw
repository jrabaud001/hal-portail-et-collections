%0 Journal Article
%T Modification of activated carbons based on diazonium ions in situ produced from aminobenzene organic acid without addition of other acid
%+ Institut des Matériaux Jean Rouxel (IMN)
%+ Laboratoire de Génie des Matériaux et Procédés Associés (LGMPA)
%+ Chimie, Ingénierie Moléculaire et Matériaux d'Angers (CIMMA)
%+ Unité de chimie organique moléculaire et macromoléculaire (UCO2M)
%A Lebegue, Estelle
%A Madec, Lenaic
%A Brousse, Thierry
%A Gaubicher, Joël
%A Levillain, Eric
%A Cougnon, Charles
%< avec comité de lecture
%@ 0959-9428
%J Journal of Materials Chemistry
%I Royal Society of Chemistry
%V 21
%N 33
%P 12221
%8 2011
%D 2011
%R 10.1039/c1jm11538c
%Z Physics [physics]/Condensed Matter [cond-mat]/Materials Science [cond-mat.mtrl-sci]Journal articles
%X Activated carbon products modified with a benzene sulfonic acid group were prepared based on the spontaneous reduction of diazonium salts in situ generated in water without addition of an external acid. The diazotization reaction assisted by the organic acid substituent, produced at once amine, diazonium and triazene functionalities that maximize the grafting yield by a chemical cooperation effect.
%G English
%L hal-00849538
%U https://hal.archives-ouvertes.fr/hal-00849538
%~ CNRS
%~ UNIV-ANGERS
%~ UNIV-LEMANS
%~ UNIV-NANTES
%~ IMN
%~ ST2E
%~ INC-CNRS

%0 Journal Article
%T Impact of the metal electrode size in half-cells studies: Example of graphite/Li coin cells
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Madec, Lénaïc
%A Martinez, Hervé
%< avec comité de lecture
%@ 1388-2481
%J Electrochemistry Communications
%I Elsevier
%V 90
%P 61-64
%8 2018-05
%D 2018
%K Li metal electrode size
%K Graphite electrode
%K Electrochemical performance
%K Interphase formation
%K XPS
%Z IF : 4.396
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Analytical chemistry
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Material chemistryJournal articles
%X When half-cells are used to evaluate and understand electrochemical performance and interface properties of a given electrode, the metal electrode size is rarely mentioned. To evaluate such impact on the electrochemical performance and interphase formation, graphite/Li coin cells were used. Undersizing the Li metal electrode led to significantly lower charge/discharge capacities even at C/20 rate due to incomplete lithiation/delithiation processes of the graphite electrode edge. It also led to the formation of non-uniform Li metal deposits as well as to a graphite SEI film with a thickness and composition gradient across the electrode. Oversized Li led, however to homogeneous graphite SEI film. Overall, these results highlight the critical role of the metal electrode size in half-cells and should apply to all half-cells studies using other metal electrodes such as Na, K, Mg.
%G English
%L hal-02048484
%U https://hal.archives-ouvertes.fr/hal-02048484
%~ CNRS
%~ UNIV-PAU
%~ IPREM
%~ INC-CNRS
%~ IPREM-PCM
%~ TESTUPPA

%0 Journal Article
%T In situ redox functionalization of composite electrodes for high power-high energy electrochemical storage systems via a non-covalent approach
%+ Institut des Matériaux Jean Rouxel (IMN)
%+ Chimie, Ingénierie Moléculaire et Matériaux d'Angers (CIMMA)
%A Madec, Lenaic
%A Bouvree, Audrey
%A Blanchard, Philippe
%A Cougnon, Charles
%A Brousse, Thierry
%A Lestriez, Bernard
%A Guyomard, Dominique
%A Gaubicher, Joël
%< avec comité de lecture
%@ 1754-5692
%J Energy & Environmental Science
%I Royal Society of Chemistry
%V 5
%N 1
%P 5379
%8 2012
%D 2012
%R 10.1039/c1ee02490f
%Z Physics [physics]/Condensed Matter [cond-mat]/Materials Science [cond-mat.mtrl-sci]Journal articles
%X The growing demand for new global resources of clean and sustainable energy emerges as the greatest challenge in today's society. For numerous applications such as hybrid vehicles, electrochemical storage systems simultaneously require high energy and high power. For this reason, intensive researches focus on proposing alternative devices to conventional Li battery and supercapacitors. Here, we report a proof of concept based on non-covalent redox functionalization of composite electrodes that may occur either during the calendar life or during the device functioning. The active material, a multi-redox pyrene derivative, is initially contained in the electrolyte. No additional benchmarking step is therefore required, and it can, in principle, be readily applied to any type of composite electrode (supercapacitors, battery, semi-solid flow cell etc.). Accordingly, a practical carbon fiber electrode that is 10 mg cm−2 loaded can deliver up to 130 kW kgelectrode−1 and 130 Wh kgelectrode−1 with negligible capacity loss over the first 60[thin space (1/6-em)]000 charge/discharge cycles.
%G English
%L hal-00865774
%U https://hal.archives-ouvertes.fr/hal-00865774
%~ CNRS
%~ UNIV-ANGERS
%~ UNIV-NANTES
%~ IMN
%~ ST2E
%~ INC-CNRS

%0 Journal Article
%T Interest of molecular functionalization for electrochemical storage
%+ Institut des Matériaux Jean Rouxel (IMN)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Anothumakkool, Bihag
%A Guyomard, Dominique
%A Gaubicher, Joël
%A Madec, Lénaïc
%< avec comité de lecture
%@ 1998-0124
%J Nano Research
%I Springer
%V 10
%N 12
%P 4175 - 4200
%8 2017-12
%D 2017
%R 10.1007/s12274-017-1797-7
%K electrochemical storage
%K molecular functionalization
%K covalent diazonium chemistry
%K non-covalent π–π interactions
%Z Physics [physics]/Condensed Matter [cond-mat]/Materials Science [cond-mat.mtrl-sci]Journal articles
%X Despite great interests in electrochemical energy storage systems for numerous applications, considerable challenges remain to be overcome. Among the various approaches to improving the stability, safety, performance, and cost of these systems, molecular functionalization has recently been proved an attractive method that allows the tuning of material surface reactivity while retaining the properties of the bulk material. For this purpose, the reduction of aryldiazonium salt, which is a versatile method, is considered suitable; it forms robust covalent bonds with the material surface, however, with the formation of multilayer structures and sp3 defects (for carbon substrate) that can be detrimental to the electronic conductivity. Alternatively, non-covalent molecular functionalization based on π–π interactions using aromatic ring units has been proposed. In this review, the various advances in molecular functionalization concerning the current limitations in lithium-ion batteries and electrochemical capacitors are discussed. According to the targeted applications and required properties, both covalent and non-covalent functionalization methods have proved to be very efficient and versatile. Fundamental aspects to achieve a better understanding of the functionalization reactions as well as molecular layer properties and their effects on the electrochemical performance are also discussed. Finally, perspectives are proposed for future implementation of molecular functionalization in the field of electrochemical storage.
%G English
%L hal-01705370
%U https://hal.archives-ouvertes.fr/hal-01705370
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ IMN
%~ ST2E
%~ INC-CNRS
%~ UNIV-NANTES
%~ IPREM-PCM
%~ TESTUPPA

%0 Journal Article
%T Variabilite de la reproduction examinee au laboratoire entre populations naturelles d'Helix aspersa Muller de la region Bretagne
%+ Laboratoire de recherches de la chaire de zoologie
%A Madec, L.
%A DAGUZAN, J.
%< avec comité de lecture
%@ 0926-5287
%J Reproduction Nutrition Development
%I EDP Sciences
%V 31
%P 551-559
%8 1991
%D 1991
%K LOCALISATION GEOGRAPHIQUE
%K COLLECTE
%Z Life Sciences [q-bio]/Agricultural sciences/Animal production studiesJournal articles
%G French
%L hal-02713067
%U https://hal.inrae.fr/hal-02713067
%~ INRAE
%~ INRA

%0 Journal Article
%T Exploring Interactions between Electrodes in Li[Ni x Mn y Co 1-xy ]O 2 /Graphite Cells through Electrode/Electrolyte Interfaces Analysis
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Madec, Lénaïc
%A Ellis, Leah
%< avec comité de lecture
%@ 0013-4651
%J Journal of The Electrochemical Society
%I Electrochemical Society
%V 164
%N 14
%P A3718 - A3726
%8 2017-12-07
%D 2017
%R 10.1149/2.1011714jes
%K Interactions between electrodes
%K Li[NixMnyCo1-xy]O2/graphite cells
%K Solid electrolyte interphase
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Analytical chemistry
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Material chemistryJournal articles
%X Interactions occurring between positive and negative electrodes during constant current-constant voltage cycling in Li[NixMnyCo1-xy]O2/graphite pouch cells at various upper cutoff voltages was investigated. Below 4.3 V, good capacity retention, high/stable coulombic efficiency (CE) and low gas production were observed. At 4.5 V, however, extensive capacity loss, dramatic/unstable CE and large gas generation were observed due to extensive electrolyte degradation at the NMC electrodes. XPS experiments highlighted that solvents (-CO containing species) and salt (mostly LiF) degradation products including gas and NMC dissolution products are produced at the NMC electrodes then migrate to the graphite electrodes surface where they are either reduced or simply deposited. Although these phenomena were greatly accelerated at high voltage, the dominant failure mechanism was the formation of a Li-ion insulator/electron conductor surface layer (maybe rock salt) due to irreversible structural change of the NMC particles surface. At low voltage, the failure mechanism was explained by accumulation of SEI at the graphite electrode surface that hinder the ionic transport through the electrode porosity. Overall, both failure mechanisms are driven by oxidative parasitic reactions at the positive electrode and interaction with the negative electrode. Passivation of the positive electrode appears therefore crucial to promote long lifetime of Li-ion cells.
%G English
%L hal-01785685
%U https://hal.archives-ouvertes.fr/hal-01785685
%~ CNRS
%~ UNIV-PAU
%~ IPREM
%~ INC-CNRS

%0 Journal Article
%T Cross-Section Auger/XPS Imaging of Conversion Type Electrodes: How Their Morphological Evolution Controls the Performance in Li-Ion Batteries
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Institut Charles Gerhardt Montpellier - Institut de Chimie Moléculaire et des Matériaux de Montpellier (ICGM ICMMM)
%A Madec, Lenaic
%A Ledeuil, Jean-Bernard
%A Coquil, Gaël
%A Monconduit, Laure
%A Martinez, Hervé
%Z ADEME
%< avec comité de lecture
%@ 2574-0962
%J ACS Applied Energy Materials
%I ACS
%V 2
%N 7
%P 5300-5307
%8 2019-06-14
%D 2019
%R 10.1021/acsaem.9b01115
%K Li-ion battery
%K conversion material
%K morphological evolution
%K cross-section
%K Auger
%K XPS
%Z Chemical Sciences/Analytical chemistry
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Material chemistryJournal articles
%X Conversion type reactions have revolutionized the field of Li-ion batteries and beyond in terms of electrochemical performance and fundamental aspects. However, direct evidence of this reaction over long-term cycling still has to be demonstrated. Indeed, investigating the morphological conversion mechanism at both the electrode and nanometer scales remains very challenging. Here, the use of advanced Auger/μXPS imaging of electrode cross-sections, prepared by ion-milling, is proposed. Through the study of TiSnSb-based electrodes, the role of the inactive element (here Ti) in the long-term reversibility of the conversion reaction is highlighted. Importantly, Ti, Sn, and Sb are found together at the nanometer scale despite a total spreading over tens of microns which directly proves the electrochemical conversion of the TiSnSb material even after 400 cycles. Moreover, a gradual shell to core expansion/break-up of TiSnSb particles is revealed during the continuous conversion reactions and leads to highly porous structures after 400 cycles. This phenomenon, more uniform at 60 °C, is at the origin of the higher electrochemical performance at this temperature. Overall, the innovative approach proposed in this work will benefit the the study of not only conversion/alloy-based batteries but also all-solid-state batteries for which buried interfaces have to be reached.
%G English
%L hal-02289318
%U https://hal.archives-ouvertes.fr/hal-02289318
%~ UNIV-PAU
%~ IPREM
%~ IPREM-PCM
%~ ENSC-MONTPELLIER
%~ CNRS
%~ INC-CNRS
%~ CHIMIE
%~ UNIV-MONTPELLIER
%~ ICG
%~ ANR

%0 Journal Article
%T Paving the Way for K-Ion Batteries: Role of Electrolyte Reactivity through the Example of Sb-Based Electrodes
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Réseau sur le stockage électrochimique de l'énergie (RS2E)
%+ Institut Charles Gerhardt Montpellier - Institut de Chimie Moléculaire et des Matériaux de Montpellier (ICGM ICMMM)
%+ Laboratoire réactivité et chimie des solides - UMR CNRS 7314 (LRCS)
%A Madec, Lenaic
%A Gabaudan, Vincent
%A Gachot, Grégory
%A Stievano, Lorenzo
%A Monconduit, Laure
%A Martinez, Hervé
%< avec comité de lecture
%@ 1944-8244
%J ACS Applied Materials & Interfaces
%I Washington, D.C. : American Chemical Society
%V 10
%N 40
%P 34116 - 34122
%8 2018-09-12
%D 2018
%R 10.1021/acsami.8b08902
%K electrolyte
%K GC/MS
%K interaction
%K potassium-ion
%K XPS
%Z Chemical Sciences/Material chemistryJournal articles
%X Developing potassium-ion batteries remains a challenge so far due to the lack of efficient electrolytes. Moreover, the high reactivity of K metal and the use of half-cells may greatly alter both the electrochemical performance and the solid electrolyte interphase formation. Here, it is shown that in K metal/Sb half-cells, Coulombic efficiency improvement is achieved by the addition of fluoroethylene carbonate + vinylene carbonate to propylene carbonate (PC), the replacement of PC by ethylene carbonate/diethyl carbonate, and the replacement of KPF6 by potassium bis(fluorosulfonyl)imide. Surprisingly, however, storage of cells containing K metal leads to the coloration of K metal, separators, and Sb electrodes, whereas no change occurs for cells prepared without K metal. These results demonstrate that for all electrolytes, the high electrolyte reactivity with K metal also influences the Sb/electrolyte interface via a cross-talk mechanism. This observation is supported by gas chromatography/mass spectrometry analysis of electrolytes and X-ray photoelectron spectroscopy analysis of Sb electrodes. In summary, these results indicate that the search for efficient electrolytes for potassium-ion batteries must be carried out in full cells if one wants to obtain meaningful correlations between electrochemical performance and electrode/electrolyte interfacial properties. Overall, the results presented here are also likely to benefit the development of other emerging Na- and Mg-ion cell chemistries.
%G English
%L hal-01896379
%U https://hal.archives-ouvertes.fr/hal-01896379
%~ IPREM
%~ IPREM-PCM
%~ UNIV-PAU
%~ CNRS
%~ UNIV-PICARDIE
%~ ENSC-MONTPELLIER
%~ RS2E
%~ INC-CNRS
%~ CHIMIE
%~ UNIV-MONTPELLIER
%~ ICG
%~ CDF
%~ ENSC-PARIS
%~ INPG
%~ ENSCP
%~ PSL
%~ PARISTECH
%~ UNIV-NANTES
%~ UNIV-AMU
%~ UGA
%~ SITE-ALSACE
%~ SORBONNE-UNIV
%~ ENSCP-PSL
%~ CDF-PSL
%~ UGA-COMUE
%~ SORBONNE-UNIVERSITE
%~ U-PICARDIE
%~ LRCS
%~ INPT

%0 Journal Article
%T Toward efficient Li-ion cells at high temperatures: Example of TiSnSb material
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Laboratoire réactivité et chimie des solides - UMR CNRS 7314 (LRCS)
%+ Institut Charles Gerhardt Montpellier - Institut de Chimie Moléculaire et des Matériaux de Montpellier (ICGM ICMMM)
%+ Réseau sur le stockage électrochimique de l'énergie (RS2E)
%A Madec, Lénaïc
%A Gachot, Grégory
%A Coquil, Gaël
%A Martinez, Hervé
%A Monconduit, Laure
%< avec comité de lecture
%@ 0378-7753
%J Journal of Power Sources
%I Elsevier
%V 391
%P 51 - 58
%8 2018-07
%D 2018
%R 10.1016/j.jpowsour.2018.04.068
%K TiSnSb electrodes
%K High temperature
%K XPS
%K GC/MS
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Analytical chemistry
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Material chemistryJournal articles
%X Developing efficient Li-ion cells with long lifetime for high temperature applications remains a challenging problem due to severe electrolyte degradation. Here, it is showed that superior lifetime, CE and polarization could be obtained at 60 °C compared to 25 °C when TiSnSb/Li coin cells filled with 1 M LiPF6 EC:PC:3DMC +5% FEC +1% VC electrolyte were used. To understand such unexpected phenomenon, a thorough correlation of the electrochemical performance with complementary gas chromatography coupled with electron impact mass spectrometry (GC/MS) and X-ray photoelectron spectroscopy (XPS) analysis was performed. XPS results showed the formation of a more stable and passivating SEI film at the TiSnSb electrode surface at 60 °C due to a specific reactivity of the electrolyte. In good agreement, the GC/MS analysis showed only a partial consumption/reactivity of the FEC/VC additives at 60 °C while a full consumption of the additives and a further degradation of the EC solvent were observed at 25 °C. As a whole, the XPS and GC/MS results allowed to identify the origin of the superior electrochemical performance of the TiSnSb/coin cells at 60 °C.
%G English
%L hal-01790727
%U https://hal.archives-ouvertes.fr/hal-01790727
%~ CNRS
%~ UNIV-PICARDIE
%~ UNIV-PAU
%~ ENSC-MONTPELLIER
%~ RS2E
%~ IPREM
%~ INC-CNRS
%~ CHIMIE
%~ UNIV-MONTPELLIER
%~ ICG
%~ IPREM-PCM
%~ ANR
%~ CDF
%~ ENSC-PARIS
%~ INPG
%~ ENSCP
%~ PSL
%~ PARISTECH
%~ UNIV-NANTES
%~ UNIV-AMU
%~ UGA
%~ SITE-ALSACE
%~ SORBONNE-UNIV
%~ ENSCP-PSL
%~ CDF-PSL
%~ UGA-COMUE
%~ SORBONNE-UNIVERSITE
%~ U-PICARDIE
%~ LRCS
%~ INPT

%0 Journal Article
%T Formulation of flowable anolyte for redox flow batteries: Rheo-electrical study
%+ Qatar University
%+ Réseau sur le stockage électrochimique de l'énergie (RS2E)
%+ Institut des Matériaux Jean Rouxel (IMN)
%A Youssry, Mohamed
%A Madec, Lénaïc
%A Soudan, Patrick
%A Cerbelaud, Manuella
%A Guyomard, Dominique
%A Lestriez, Bernard
%< avec comité de lecture
%@ 0378-7753
%J Journal of Power Sources
%I Elsevier
%V 274
%P 424-431
%8 2015-01
%D 2015
%R 10.1016/j.jpowsour.2014.10.076
%K Rheo-electrical properties
%K Suspension electrodes
%K Carbon nanofibers
%K Redox flow batteries
%Z Chemical SciencesJournal articles
%G English
%L hal-01115510
%U https://hal.archives-ouvertes.fr/hal-01115510
%~ UNIV-NANTES
%~ CNRS
%~ IMN
%~ RS2E
%~ ST2E
%~ INC-CNRS
%~ CDF
%~ UNIV-PICARDIE
%~ ENSC-MONTPELLIER
%~ ENSC-PARIS
%~ INPG
%~ ENSCP
%~ PSL
%~ UNIV-PAU
%~ PARISTECH
%~ UNIV-MONTPELLIER
%~ UNIV-AMU
%~ UGA
%~ SITE-ALSACE
%~ ENSCP-PSL
%~ CDF-PSL
%~ SU-INF-2018
%~ SORBONNE-UNIVERSITE
%~ U-PICARDIE
%~ INPT

%0 Journal Article
%T Non-aqueous carbon black suspensions for lithium-based redox flow batteries: rheology and simultaneous rheo-electrical behavior
%+ Institut des Matériaux Jean Rouxel (IMN)
%+ Réseau sur le stockage électrochimique de l'énergie (RS2E)
%+ Department of Chemistry
%A Youssry, Mohamed
%A Madec, Lenaic
%A Soudan, Patrick
%A Cerbelaud, Manuella
%A Guyomard, Dominique
%A Lestriez, Bernard
%< avec comité de lecture
%@ 1463-9076
%J Physical Chemistry Chemical Physics
%I Royal Society of Chemistry
%V 15
%N 34
%P 14476
%8 2013
%D 2013
%R 10.1039/c3cp51371h
%Z Physics [physics]/Condensed Matter [cond-mat]/Materials Science [cond-mat.mtrl-sci]Journal articles
%X We report on the rheological and electrical properties of non-aqueous carbon black (CB) suspensions at equilibrium and under steady shear flow. The smaller the primary particle size of carbon black is, the higher the magnitude of rheological parameters and the conductivity are. The electrical percolation threshold ranges seem to coincide with the strong gel rather than the weak gel rheological threshold ones. The simultaneous measurements of electrical properties under shear flow reveal the well-known breaking-and-reforming mechanism that characterises such complex fluids. The small shear rate breaks up the network into smaller agglomerates, which in turn transform into anisometric eroded ones at very high shear rates, recovering the network conductivity. The type of carbon black, its concentration range and the flow rate range are now precisely identified for optimizing the performance of a redox flow battery. A preliminary electrochemical study for a composite anolyte (CB/Li4Ti5O12) at different charge-discharge rates and thicknesses is shown.
%G English
%L hal-00980350
%U https://hal.archives-ouvertes.fr/hal-00980350
%~ CNRS
%~ IMN
%~ UNIV-NANTES
%~ RS2E
%~ ST2E
%~ INC-CNRS
%~ CDF
%~ UNIV-PICARDIE
%~ ENSC-MONTPELLIER
%~ ENSC-PARIS
%~ INPG
%~ ENSCP
%~ PSL
%~ UNIV-PAU
%~ PARISTECH
%~ UNIV-MONTPELLIER
%~ UNIV-AMU
%~ UGA
%~ SITE-ALSACE
%~ ENSCP-PSL
%~ CDF-PSL
%~ SU-INF-2018
%~ SORBONNE-UNIVERSITE
%~ U-PICARDIE
%~ INPT

%0 Journal Article
%T Surfactant for Enhanced Rheological, Electrical, and Electrochemical Performance of Suspensions for Semisolid Redox Flow Batteries and Supercapacitors
%+ Institut des Matériaux Jean Rouxel (IMN)
%+ Réseau sur le stockage électrochimique de l'énergie (RS2E)
%+ Qatar University
%A Madec, Lénaïc
%A Youssry, Mohamed
%A Cerbelaud, Manuella
%A Soudan, Patrick
%A Guyomard, Dominique
%A Lestriez, Bernard
%< avec comité de lecture
%@ 2192-6506
%J ChemPlusChem
%I Wiley
%V 80
%N 2
%P 396-401
%8 2015-02
%D 2015
%R 10.1002/cplu.201402042
%Z Chemical SciencesJournal articles
%G English
%L hal-01115624
%U https://hal.archives-ouvertes.fr/hal-01115624
%~ UNIV-NANTES
%~ CNRS
%~ IMN
%~ RS2E
%~ ST2E
%~ INC-CNRS
%~ CDF
%~ UNIV-PICARDIE
%~ ENSC-MONTPELLIER
%~ ENSC-PARIS
%~ INPG
%~ ENSCP
%~ PSL
%~ UNIV-PAU
%~ PARISTECH
%~ UNIV-MONTPELLIER
%~ UNIV-AMU
%~ UGA
%~ SITE-ALSACE
%~ ENSCP-PSL
%~ CDF-PSL
%~ SU-INF-2018
%~ SORBONNE-UNIVERSITE
%~ U-PICARDIE
%~ INPT

%0 Journal Article
%T Synergistic Effect in Carbon Coated LiFePO4 for High Yield Spontaneous Grafting of Diazonium Salt. Structural Examination at the Grain Agglomerate Scale
%+ Institut des Matériaux Jean Rouxel (IMN)
%+ Réseau sur le stockage électrochimique de l'énergie (RS2E)
%+ Laboratoire d'Innovation pour les Technologies des Energies Nouvelles et les nanomatériaux (LITEN)
%+ Laboratoire d'Etude des Matériaux par Microscopie Avancée (LEMMA )
%A Madec, Lenaic
%A Robert, Donatien
%A Moreau, Philippe
%A Bayle-Guillemaud, Pascale
%A Guyomard, Dominique
%A Gaubicher, Joël
%< avec comité de lecture
%@ 0002-7863
%J Journal of the American Chemical Society
%I American Chemical Society
%V 135
%N 31
%P 11614
%8 2013
%D 2013
%R 10.1021/ja405087x
%Z Physics [physics]/Condensed Matter [cond-mat]/Materials Science [cond-mat.mtrl-sci]Journal articles
%X Molecular grafting of p-nitrobenzene diazonium salt at the surface of (Li)FePO4-based materials was thoroughly investigated. The grafting yields obtained by FTIR, XPS, and elemental analysis for core shell LiFePO4-C are found to be much higher than the sum of those associated with either the LiFePO4 core or the carbon shell alone, thereby revealing a synergistic effect. Electrochemical, XRD, and EELS experiments demonstrate that this effect stems from the strong participation of the LiFePO4 core that delivers large amounts of electrons to the carbon substrate at a constant energy, above the Fermi level of the diazonium salt. Correspondingly large multilayer anisotropic structures that are associated with outstanding grafting yields could be observed from TEM experiments. Results therefore constitute strong evidence of a grafting mechanism where homolytic cleavage of the N2+ species occurs together with the formation and grafting of radical nitro-aryl intermediates. Although the oxidation and concomitant Li deintercalation of LiFePO4 grains constitute the main driving force of the functionalization reaction, EFTEM EELS mapping shows a striking lack of spatial correlation between grafted grains and oxidized ones.
%G English
%L hal-00981952
%U https://hal.archives-ouvertes.fr/hal-00981952
%~ CNRS
%~ IMN
%~ UNIV-NANTES
%~ RS2E
%~ CEA
%~ DSM-INAC
%~ INAC-SP2M
%~ ST2E
%~ INC-CNRS
%~ DRT
%~ UNIV-SAVOIE
%~ CEA-DRF
%~ LITEN
%~ IRIG
%~ CEA-GRE
%~ USMB-COMUE
%~ UGA
%~ CDF
%~ UNIV-PICARDIE
%~ ENSC-MONTPELLIER
%~ ENSC-PARIS
%~ INPG
%~ ENSCP
%~ PSL
%~ UNIV-PAU
%~ PARISTECH
%~ UNIV-MONTPELLIER
%~ UNIV-AMU
%~ SITE-ALSACE
%~ ENSCP-PSL
%~ CDF-PSL
%~ SU-INF-2018
%~ SORBONNE-UNIVERSITE
%~ U-PICARDIE
%~ INPT

%0 Journal Article
%T Electronic vs Ionic Limitations to Electrochemical Performance in Li4Ti5O12-Based Organic Suspensions for Lithium-Redox Flow Batteries
%+ Institut des Matériaux Jean Rouxel (IMN)
%+ Réseau sur le stockage électrochimique de l'énergie (RS2E)
%+ Qatar University
%A Madec, L.
%A Youssry, M.
%A Cerbelaud, Manuella
%A Soudan, P.
%A Guyomard, Dominique
%A Lestriez, B.
%< avec comité de lecture
%@ 0013-4651
%J Journal of The Electrochemical Society
%I Electrochemical Society
%V 161
%N 5
%P A693-A699
%8 2014-03
%D 2014
%R 10.1149/2.035405jes
%Z Chemical SciencesJournal articles
%G English
%L hal-01115562
%U https://hal.archives-ouvertes.fr/hal-01115562
%~ UNIV-NANTES
%~ CNRS
%~ IMN
%~ RS2E
%~ ST2E
%~ INC-CNRS
%~ CDF
%~ UNIV-PICARDIE
%~ ENSC-MONTPELLIER
%~ ENSC-PARIS
%~ INPG
%~ ENSCP
%~ PSL
%~ UNIV-PAU
%~ PARISTECH
%~ UNIV-MONTPELLIER
%~ UNIV-AMU
%~ UGA
%~ SITE-ALSACE
%~ ENSCP-PSL
%~ CDF-PSL
%~ SU-INF-2018
%~ SORBONNE-UNIVERSITE
%~ U-PICARDIE
%~ INPT

%0 Journal Article
%T Covalent vs. non-covalent redox functionalization of C-LiFePO4 based electrodes
%+ Institut des Matériaux Jean Rouxel (IMN)
%+ Chimie, Ingénierie Moléculaire et Matériaux d'Angers (CIMMA)
%A Madec, L.
%A Humbert, Bernard
%A Lestriez, B.
%A Brousse, Thierry
%A Cougnon, C.
%A Guyomard, Dominique
%A Gaubicher, Joël
%< avec comité de lecture
%@ 0378-7753
%J Journal of Power Sources
%I Elsevier
%V 232
%P 246
%8 2013
%D 2013
%R 10.1016/j.jpowsour.2012.10.100
%K Pyrene
%K Diazonium
%K Redox functionalization
%K Carbon additive
%K LiFePO4
%K Power performance
%Z Physics [physics]/Condensed Matter [cond-mat]/Materials Science [cond-mat.mtrl-sci]Journal articles
%X During high rate utilization of porous Li battery, Li+ refuelling from the electrolyte limits the discharge kinetics of positive electrodes. In the case of thick electrodes a strategy to buffer the resulting sharp drop of Li+ concentration gradient would be to functionalize the electrode with anionic based redox molecules (RMR) that would be therefore able to relay intercalation process. The occurrence of these RMR in the electrode should not however, induce adverse effect on Li intercalation processes. In this respect, this work studies the effect of functionalizing LFPC based electrodes by either covalent or non-covalent chemistry, on Li intercalation kinetics. To do so, model molecules containing a nitro group were introduced at the surface of both carbon conducting additives and active material (C-LiFePO4). It is shown that presumably due to formation of sp3 defects, covalent anchoring using diazonium chemistry inhibits the intercalation kinetics in C-FePO4. On the contrary, if molecules such as pyrene derivatives are immobilized by pi-staking interactions, Li intercalation is not impeded. Therefore non-covalent functionalization of pyrene based RMR appears as a promising route to relay Li intercalation reaction during high power demand. The framework for future development of this strategy is discussed.
%G English
%L hal-00961242
%U https://hal.archives-ouvertes.fr/hal-00961242
%~ CNRS
%~ UNIV-ANGERS
%~ UNIV-NANTES
%~ IMN
%~ PMN
%~ ST2E
%~ INC-CNRS

