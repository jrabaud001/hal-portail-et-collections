%0 Journal Article
%T Non-reflecting boundary condition on ellipsoidal boundary
%+ Advanced 3D Numerical Modeling in Geophysics (Magique 3D)
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Basque Center for Applied Mathematics (BCAM)
%A Barucq, Hélène
%A Dupouy St-Guirons, Anne-Gaëlle
%A Tordeux, Sébastien
%< avec comité de lecture
%@ 1995-4239
%J Numerical Analysis and Applications
%I Springer
%V 5
%N 2
%P 109-115
%8 2012
%D 2012
%Z Mathematics [math]/Analysis of PDEs [math.AP]
%Z Mathematics [math]/Numerical Analysis [math.NA]Journal articles
%G English
%L hal-00760458
%U https://hal.inria.fr/hal-00760458
%~ CNRS
%~ INRIA
%~ UNIV-PAU
%~ LMA-PAU
%~ INRIA-BORDEAUX
%~ INSMI
%~ TESTBORDEAUX
%~ INRIA_TEST
%~ TDS-MACS
%~ INRIA2

%0 Journal Article
%T Asymptotic expansion of highly conductive thin sheets
%+ Propagation des Ondes : Étude Mathématique et Simulation (POEMS)
%A Schmidt, Kersten
%A Tordeux, Sébastien
%< avec comité de lecture
%@ 1617-7061
%J PAMM
%I Wiley-VCH Verlag
%V 7
%P 2040011-2040012
%8 2008-07
%D 2008Journal articles
%X Sensitive measurement and control equipment are protected from disturbing electromagnetic fields by thin shielding sheets. Alternatively to discretisation of the sheets, the electromagnetic fields are modeled only in the surrounding of the layer taking them into account with the so called Generalised Impedance Boundary Conditions. We study the shielding effect by means of the model problem of a diffusion equation with additional dissipation in the curved thin sheet. We use the asymptotic expansion techniques to derive a limit problem, when the thickness of the sheet $\varepsilon$ tends to zero, as well as the models for contribution to the solution of higher order in $\varepsilon$. These problems are posed in limit area of vanishing $\varepsilon$ with condition for the jump of the solution and it's normal derivative, which avoid to mesh the computational domain, even locally, at the scale of $\varepsilon$. We derive the problems for arbitrary order and show their existence and uniqueness. Numerical experiments for the problems up to second order show the asymptotic convergence of the solution of right order in mean of the thickness parameter $\varepsilon$.
%G English
%L hal-00974802
%U https://hal-ensta-paris.archives-ouvertes.fr//hal-00974802
%~ ENSTA
%~ CNRS
%~ INRIA
%~ UMA_ENSTA
%~ INRIA_TEST
%~ INRIA2
%~ INRIA-SACLAY

%0 Journal Article
%T Matched Asymptotic Expansions of the Eigenvalues of a 3-D boundary-value problem relative to two cavities linked by a hole of small size
%+ Institut de Mathématiques de Toulouse UMR5219 (IMT)
%+ Centre Européen de Recherche et de Formation Avancée en Calcul Scientifique (CERFACS)
%+ Advanced 3D Numerical Modeling in Geophysics (Magique 3D)
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%A Bendali, Abderrahmane
%A Fares, M'Barek
%A Tizaoui, Abdelkader
%A Tordeux, Sébastien
%< avec comité de lecture
%@ 1815-2406
%J Communications in Computational Physics
%I Global Science Press
%V 11
%N 2
%P 456-471
%8 2012
%D 2012
%Z Mathematics [math]/Analysis of PDEs [math.AP]Journal articles
%G English
%L hal-00644699
%U https://hal.inria.fr/hal-00644699
%~ CNRS
%~ INRIA
%~ LMA-PAU
%~ INRIA-BORDEAUX
%~ INSMI
%~ IFR140
%~ UNIV-PAU
%~ TESTBORDEAUX
%~ INRIA_TEST
%~ IMT
%~ INRIA2
%~ UNIV-TLSE2
%~ TDS-MACS
%~ UNIV-TLSE3
%~ UT1-CAPITOLE
%~ INSA-TOULOUSE
%~ INSA-GROUPE
%~ TESTUPPA

%0 Journal Article
%T Scattering of a scalar time-harmonic wave by N small spheres by the method of matched asymptotic expansions
%+ Advanced 3D Numerical Modeling in Geophysics (Magique 3D)
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Institut de Mathématiques de Toulouse UMR5219 (IMT)
%+ Centre Européen de Recherche et de Formation Avancée en Calcul Scientifique (CERFACS)
%+ ONERA - The French Aerospace Lab [Toulouse]
%A Tordeux, Sébastien
%A Bendali, Abderrahmane
%A Cocquet, Pierre-Henri
%< avec comité de lecture
%@ 1995-4239
%J Numerical Analysis and Applications
%I Springer
%V 5
%N 2
%P 116-123
%8 2012
%D 2012
%R 10.1134/S1995423912020036
%Z Mathematics [math]/Numerical Analysis [math.NA]
%Z Mathematics [math]/Analysis of PDEs [math.AP]Journal articles
%G English
%L hal-00760457
%U https://hal.inria.fr/hal-00760457
%~ CNRS
%~ INRIA
%~ UNIV-PAU
%~ ONERA
%~ LMA-PAU
%~ INRIA-BORDEAUX
%~ INSMI
%~ IFR140
%~ TESTBORDEAUX
%~ INRIA_TEST
%~ IMT
%~ INRIA2
%~ UNIV-TLSE2
%~ TDS-MACS
%~ ONERA-MIP
%~ UNIV-TLSE3
%~ UT1-CAPITOLE
%~ INSA-TOULOUSE
%~ INSA-GROUPE

%0 Journal Article
%T Mathematical justification of the Rayleigh conductivity model for perforated plates in acoustics
%+ Institut de Mathématiques de Toulouse UMR5219 (IMT)
%+ Centre Européen de Recherche et de Formation Avancée en Calcul Scientifique (CERFACS)
%+ ONERA - The French Aerospace Lab [Toulouse]
%+ Advanced 3D Numerical Modeling in Geophysics (Magique 3D)
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%A Bendali, Abderrahmane
%A Fares, M'Barek
%A Piot, Estelle
%A Tordeux, Sébastien
%< avec comité de lecture
%@ 0036-1399
%J SIAM Journal on Applied Mathematics
%I Society for Industrial and Applied Mathematics
%V 73
%N 1
%P 438-459
%8 2013-02-14
%D 2013
%R 10.1137/120867123Journal articles
%X This paper is devoted to the mathematical justification of the usual models predicting the effective reflection and transmission of an acoustic wave by a low porosity multiperforated plate. Some previous intuitive approximations require that the wavelength be large compared with the spacing separating two neighboring apertures. In particular, we show that this basic assumption is not mandatory. Actually, it is enough to assume that this distance is less than a half-wavelength. The main tools used are the method of matched asymptotic expansions and lattice sums for the Helmholtz equations. Some numerical experiments illustrate the theoretical derivations.
%G English
%L hal-00860712
%U https://hal.inria.fr/hal-00860712
%~ CNRS
%~ INRIA
%~ LMA-PAU
%~ INRIA-BORDEAUX
%~ ONERA
%~ IFR140
%~ UNIV-PAU
%~ TESTBORDEAUX
%~ INRIA_TEST
%~ IMT
%~ INRIA2
%~ UNIV-TLSE2
%~ ONERA-MIP
%~ UNIV-TLSE3
%~ UT1-CAPITOLE
%~ INSA-TOULOUSE
%~ INSA-GROUPE

%0 Journal Article
%T Asymptotic expansions of the eigenvalues of a 2-D boundary-value problem relative to two cavities linked by a hole of small size
%+ Institut de Mathématiques de Toulouse UMR5219 (IMT)
%A Bendali, Abderrahmane
%A Huard, Alain
%A Tizaoui, Abdelkader
%A Tordeux, Sébastien
%A Vila, Jean-Paul
%< avec comité de lecture
%@ 0764-4442
%J Comptes rendus de l'Académie des sciences. Série I, Mathématique
%I Elsevier
%V 347
%N 19-20
%P 1147--1152
%8 2009
%D 2009
%R 10.1016/j.crma.2009.09.005
%Z Mathematics [math]/Numerical Analysis [math.NA]
%Z Mathematics [math]/Analysis of PDEs [math.AP]Journal articles
%X This note presents the derivation of the second-order asymptotic expansion of the eigenvalues and the eigenfunctions of the operator associated to an interior elliptic equation supplemented by a Dirichlet boundary condition on a domain consisting of two cavities linked by a hole of small size. The asymptotic expansion is carried out with respect to the size of the hole. The main feature of the method is to yield a robust numerical procedure making it possible to compute the eigenvalues without resorting to a refined mesh around the hole.
%G English
%L inria-00527437
%U https://hal.inria.fr/inria-00527437
%~ CNRS
%~ INSMI
%~ IMT
%~ UNIV-TLSE2
%~ TDS-MACS
%~ UNIV-TLSE3
%~ UT1-CAPITOLE
%~ INSA-TOULOUSE
%~ INSA-GROUPE
%~ ANR

%0 Journal Article
%T Effect of micro-defects on structure failure : coupling asymptotic analysis and strong discontinuity approach
%+ Institut de Recherche Mathématique de Rennes (IRMAR)
%+ Laboratoire de Mécanique et Technologie (LMT)
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%+ Advanced 3D Numerical Modeling in Geophysics (Magique 3D)
%A Bonnaillie-Noël, Virginie
%A Brancherie, Delphine
%A Dambrine, Marc
%A Tordeux, Sébastien
%A Vial, Grégory
%< avec comité de lecture
%@ 1779-7179
%J Revue Européenne de Mécanique Numérique/European Journal of Computational Mechanics
%I Hermès / Paris : Lavoisier
%V 19
%N 1-3
%P 165-175
%8 2010
%D 2010
%R 10.3166/ejcm.19.165-175
%K multi-scale asymptotic analysis
%K singular perturbation
%K strong discontinuity
%K failure
%Z Mathematics [math]/Analysis of PDEs [math.AP]
%Z Mathematics [math]/Numerical Analysis [math.NA]
%Z Physics [physics]/Mechanics [physics]/Mechanics of the structures [physics.class-ph]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Mechanics of the structures [physics.class-ph]Journal articles
%X This work aims at taking into account the influence of geometrical defects on the behavior till complete failure of structures. This is achieved without any fine description of the exact geometry of the perturbations. The proposed strategy is based on two approaches : asymptotic analysis of Navier equations and strong discontinuity approach.
%X L'objectif de ce travail est de prendre en compte l'influence de la présence de défauts géométriques sur le comportement à rupture des structures et ce, sans description fine de la géométrie particulière des perturbations. L'approche proposée s'appuie sur deux outils : une analyse asymptotique des équations de Navier et l'utilisation de modèles à discontinuité forte.
%G English
%L inria-00527619
%U https://hal.inria.fr/inria-00527619
%~ CNRS
%~ UNIV-RENNES1
%~ IRMAR
%~ LMA-PAU
%~ INSMI
%~ INRIA
%~ INRIA-BORDEAUX
%~ UPMC
%~ UNIV-PAU
%~ INRIA_TEST
%~ TESTBORDEAUX
%~ INRIA2
%~ TDS-MACS
%~ UNAM
%~ ENS-CACHAN
%~ UR1-HAL
%~ UR1-MATH-STIC
%~ UNIV-RENNES2
%~ UR2-HB
%~ TEST-UNIV-RENNES
%~ TEST-UR-CSS
%~ UNIV-RENNES
%~ INSA-GROUPE
%~ SORBONNE-UNIVERSITE
%~ INSA-RENNES
%~ LMT
%~ SU-SCIENCES
%~ FARMAN
%~ UR1-MATH-NUM
%~ AGREENIUM
%~ ENS-PARIS-SACLAY

