%0 Journal Article
%T Molecular Cartography of A1 and A2 Asphaltene Subfractions from Classical Molecular Dynamics Simulations
%+ Faculté des sciences de Caracas
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Villegas, Orlando
%A Vallverdu, Germain
%A Bouyssière, Brice
%A Acevedo, Sócrates
%A Castillo, Jimmy
%A Baraille, Isabelle
%Z Franco-Venezuelan Project RMR
%< avec comité de lecture
%@ 2398-4902
%J Sustainable Energy & Fuels
%I Royal Society of Chemistry
%V 34
%N 11
%P 13954-13965
%8 2020-11-19
%D 2020
%R 10.1021/acs.energyfuels.0c02744
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical SciencesJournal articles
%X The physical chemistry of the heavyweight fraction of crude oil is still a subject of vivid discussions due to the complications which arise from the tendency of this fraction to aggregate and finally hinder the refining processes. Asphaltene molecules, that compose the largest part of this fraction, can be separated into two subfractions, A1 and A2, through a treatment with p-nitrophenol. In this paper, starting from the molecule models suggested by Acevedo et al. (Acevedo et al. Energy Fuels 2018, 32, 6669−6677.), we screened the chemical structure and composition of the two subfractions and investigated their aggregation mechanism using classical molecular dynamics simulations. The results show that oxygen atoms, present as hydroxyl or carboxylic groups, are a key factor in the formation of large aggregate. From the analysis of the simulations, we estimated the size of the aggregates and showed how the flexibility of the molecules may affect the size of the aggregates. Finally we showed that in addition to the structural differences (such as the H/C ratio and DBE) that distinguish the A1 and A2 subfractions, the solubility of the subfractions is also strongly dependent on the ability of the molecules to bind through hydrogen bonds.
%G English
%L hal-03043502
%U https://hal-univ-pau.archives-ouvertes.fr/hal-03043502
%~ UNIV-PAU
%~ IPREM
%~ IPREM-CAPT
%~ INC-CNRS
%~ CNRS

%0 Journal Article
%T Excited state dynamics of the green fluorescent protein on the nanosecond time scale
%+ Laboratoire de Chimie Physique D'Orsay (LCPO)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Hôpital Lariboisière
%A Jonasson, G.
%A Teuler, J.-M.
%A Vallverdu, Germain
%A Mérola, F.
%A Ridard, J.
%A Levy, B.
%A Demachy, I.
%Z cited By 18
%< avec comité de lecture
%@ 1549-9618
%J Journal of Chemical Theory and Computation
%I American Chemical Society
%V 7
%N 6
%P 1990-1997
%8 2011
%D 2011
%R 10.1021/ct200150r
%Z Chemical Sciences
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Material chemistryJournal articles
%X We have introduced a new algorithm in the parallel processing PMEMD module of the AMBER suite that allows MD simulations with a potential involving two coupled torsions. We have used this modified module to study the green fluorescent protein. A coupled torsional potential was adjusted on high accuracy quantum chemical calculations of the anionic chromophore in the first excited state, and several 15-ns-long MD simulations were performed. We have obtained an estimate of the fluorescence lifetime (2.2 ns) to be compared to the experimental value (3 ns), which is, to the best of our knowledge, the first theoretical estimate of that lifetime.
%G English
%L hal-01550017
%U https://hal.archives-ouvertes.fr/hal-01550017
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ UNIV-PARIS7
%~ UNIV-PSUD
%~ APHP
%~ INC-CNRS
%~ USPC
%~ UNIV-PARIS

%0 Journal Article
%T Cyan fluorescent protein: Molecular dynamics, simulations, and electronic absorption spectrum
%+ Laboratoire de Chimie Physique D'Orsay (LCPO)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Demachy, I.
%A Ridard, J.
%A Laguitton-Pasquier, H.
%A Durnerin, E.
%A Vallverdu, Germain
%A Archirel, P.
%A Lévy, B.
%Z cited By 30
%< avec comité de lecture
%@ 1520-6106
%J Journal of Physical Chemistry B
%I American Chemical Society
%V 109
%N 50
%P 24121-24133
%8 2005
%D 2005
%R 10.1021/jp054656w
%K Proteins
%K Electronic transition
%K X-ray conformations
%K Absorption
%K Conformations
%K Fluorescence
%K Hydrogen bonds
%K Molecular dynamics
%K Quantum theory
%K Charge cloud
%K Electronic absorption
%Z Chemical Sciences
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Material chemistryJournal articles
%X The dynamics and electronic absorption spectrum of enhanced cyan fluorescent protein (ECFP), a mutant of green fluorescent protein (GFP), have been studied by means of a 1 ns molecular dynamics (MD) simulation. The two X-ray conformations A′ and B′ of ECFP were considered. The chromophore was assumed to be neutral, and all titratable residues were taken in their standard protonation state at neutral pH. The protein was embedded in a box of water molecules (and counterions). The first result is that the two conformations A′ and B′ are found to be stable all along the simulation. Then, an analysis of the hydrogen-bond networks shows strong differences between the two conformations in the surroundings of the nitrogen atom of the indolic part of the chromophore. This is partly due to the imperfection in the β barrel near the His148 residue, which allows the access of one solvent molecule inside the protein in conformation A′. Finally, quantum mechanical calculations of the electronic transition energies of the chromophore in the charge cloud of the protein and solvent water molecules were performed using the TDDFT method on 160 snapshots extracted every 5 ps of the MD trajectories. It is found that conformations A′ and B′ exhibit very similar spectra despite different H-bond networks involving the chromophore. This similarity is related to the weak charge transfer involved in the electronic transition and the weak electrostatic field created by ECFP near the chromophore, within the hypotheses made in the present simulation.
%G English
%L hal-01550021
%U https://hal.archives-ouvertes.fr/hal-01550021
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ UNIV-PSUD
%~ INC-CNRS

%0 Journal Article
%T Using biased molecular dynamics and Brownian dynamics in the study of fluorescent proteins
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Laboratoire de Chimie Physique D'Orsay (LCPO)
%+ Hôpital Lariboisière
%A Vallverdu, Germain
%A Demachy, I.
%A Ridard, J.
%A Levy, B.
%Z cited By 7
%< avec comité de lecture
%@ 0166-1280
%J Journal of Molecular Structure: THEOCHEM
%I Elsevier
%V 898
%N 1-3
%P 73-81
%8 2009
%D 2009
%R 10.1016/j.theochem.2008.07.012
%Z Chemical Sciences
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Material chemistryJournal articles
%X The present article presents a theoretical study of the dynamics of the chromophore of the Green Fluorescent Protein in its excited state on a long time scale (a few ten nanoseconds) in order to help the interpretation of time resolved experiments. The starting hypothesis is that the quenching of fluorescence is related to the internal motion of the chromophore, usually called 'φ torsion'. In fact, that motion is hindered by the protein and cannot be studied by standard molecular dynamics. Therefore we have developed a different approach involving three steps. First the potential of mean force (PMF) along the considered torsion is obtained by biased molecular dynamics (umbrella sampling). Then, a long time scale, single particle Brownian dynamics is performed using that PMF and an appropriate diffusion constant. Finally we determine the nth passage time (NPT) distribution functions at geometries or regions where nonradiative ground state recovery may occur. The NPT distributions generalize the more usual 'mean first passage time' and allow determining different quantities like fluorescence decay profiles, mean fluorescence lifetime, quantum yield etc. These quantities are used here in a qualitative discussion of the fluorescence decay in Green Fluorescent Protein.
%G English
%L hal-01550019
%U https://hal.archives-ouvertes.fr/hal-01550019
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ UNIV-PARIS7
%~ UNIV-PSUD
%~ APHP
%~ INC-CNRS
%~ USPC
%~ UNIV-PARIS

%0 Journal Article
%T Relation between pH, structure, and absorption spectrum of Cerulean: A study by molecular dynamics and TD DFT calculations
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Laboratoire de Chimie Physique D'Orsay (LCPO)
%+ Hôpital Lariboisière
%A Vallverdu, Germain
%A Demachy, I.
%A Mérola, F.
%A Pasquier, H.
%A Ridard, J.
%A Levy, B.
%Z cited By 6
%< avec comité de lecture
%@ 0887-3585
%J Proteins - Structure, Function and Bioinformatics
%I Wiley
%V 78
%N 4
%P 1040-1054
%8 2010
%D 2010
%R 10.1002/prot.22628
%K Hydrogen-Ion Concentration
%K Molecular Dynamics Simulation
%K Cerulean
%K unclassified drug
%K absorption
%K calculation
%K chromatophore
%K crystal structure
%K crystallography
%K fluorescence analysis
%K hydrogen bond
%K molecular dynamics
%K pH measurement
%K protein conformation
%K proton transport
%K simulation
%K structure analysis
%K chemical structure
%K chemistry
%K protein secondary structure
%K Green Fluorescent Proteins
%Z Chemical Sciences
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Material chemistryJournal articles
%X Molecular dynamics (MD) and quantum mechanical calculations of the Cerulean green fluorescent protein (a variant of enhanced cyan fluorescent protein ECFP) at pH 5.0 and 8.0 are presented, addressing two questions arising from experimental results (Malo et al., Biochemistry 2007;46:9865 - 9873): the origin of the blue shift of absorption spectrum when the pH is decreased from 8.0 to 5.0, and the lateral chain orientation of the key residue Asp 148. We demonstrate that the blue shift is reproduced assuming that a rotation around the single bond of the exocydie ring of the chromophore takes place when the pH changes from 5.0 to 8.0. We find that Asp 148 is protonated and inside the barrel at pH 5.0 in agreement with crystallographic data. However, the hydrogen bond pattern of Asp 148 is different in simulations of the solvated protein and in the crystal structure. This difference is explained by a partial closing of the cleft between strands 6 and 7 in MD simulations. This study provides also a structure at pH 8.0: the Asp 148 carboxylate group is exposed to the solvent and the chromophore is stabilized in the trans conformation by a tighter hydrogen bond network. This work gives some insight into the relationship between the pH and the chromophore conformation and suggests an interpretation of the very similar fluorescent properties of ECFP and ECFP/ H148D.
%G English
%L hal-01550018
%U https://hal.archives-ouvertes.fr/hal-01550018
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ UNIV-PARIS7
%~ UNIV-PSUD
%~ APHP
%~ INC-CNRS
%~ USPC
%~ UNIV-PARIS

%0 Journal Article
%T Impact of H-Bonds and Porphyrins on Asphaltene Aggregation As Revealed by Molecular Dynamics Simulations
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Santos Silva, Hugo
%A Alfarra, A.
%A Vallverdu, Germain
%A Bégué, D.
%A Bouyssiere, B.
%A Baraille, Isabelle
%< avec comité de lecture
%@ 0887-0624
%J Energy and Fuels
%I American Chemical Society
%V 32
%N 11
%P 11153-11164
%8 2018-10-05
%D 2018
%R 10.1021/acs.energyfuels.8b01901
%Z Chemical Sciences
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Analytical chemistryJournal articles
%X The presence of metalloporphyrins alongside asphaltenes in heavy fractions of crude oil is a key issue in petroleum exploration and upgrading. These compounds are also expected to display interfacial activity in water/toluene mixtures, but the origin of this phenomenon remains uncertain. In this work, we use molecular dynamics simulations to investigate complex asphaltene mixtures constituted of 10 different molecules, under also multifaceted solvation conditions (toluene/n-heptane/water). We add nickel and vanadium (under the form of vanadyl) porphyrins with occasionally grafted polar lateral chains, in these mixtures. The aggregation behavior and interaction with water molecules (as a model to have insights from the interfacial activity of such molecules) are intimately linked to the type of porphyrin and to the molecular properties of the asphaltenes (mainly the presence of polar lateral chains). Vanadium porphyrins, even without polar lateral chains, can form H-bonds that might contribute to their presence within asphaltene nanoaggregates. Moreover, when polar lateral chains are present in asphaltene molecules, the systems display a supramolecular organization with several distinct interactions at the same time. The shapes of these systems do not totally agree with the traditional Yen–Mullins model. In the first part of this work, we finally propose that complex asphaltene systems in complex solvent mixtures seem to have a supramolecular behavior with non-negligent colloidal behavior as well. This should be indicative that Yen–Mullins and Gray’s models of asphaltene self-assembly are neither conflictual nor antagonistic. They are two facets of a scale- and molecular structure-dependent complex mechanism.
%G English
%L hal-02095392
%U https://hal.archives-ouvertes.fr/hal-02095392
%~ UNIV-PAU
%~ IPREM
%~ IPREM-CAPT
%~ CNRS
%~ INC-CNRS

%0 Journal Article
%T Sensitivity of Asphaltene Aggregation toward the Molecular Architecture under Desalting Thermodynamic Conditions
%+ Instituto de Física
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Santos Silva, H.
%A Alfarra, A.
%A Vallverdu, Germain
%A Bégué, D.
%A Bouyssiere, B.
%A Baraille, Isabelle
%< avec comité de lecture
%@ 0887-0624
%J Energy and Fuels
%I American Chemical Society
%V 32
%N 3
%P 2681 - 2692
%8 2018-02-15
%D 2018
%R 10.1021/acs.energyfuels.7b02728
%K Desalination
%K Molecular dynamics
%K Shale oil
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Analytical chemistry
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Material chemistryJournal articles
%X The challenges faced by the oil and shale industries include the comprehension of the physical-chemical behavior of heavy-weight phases. These phases have a high tendency to aggregate, mainly as a result of the presence of asphaltenes, the composition of which has been fairly unclear up to now. The chemical composition of this phase is one of the driving forces behind the physical-chemical behavior in oil, and the structure-property relations of these systems are key in the development of improved refining techniques, including the design of new catalysts. In this paper, the aggregation of asphaltene molecules is studied with regard to molecular architecture and variations in the size of the aromatic core and lateral chain length using classical molecular dynamics simulations. How these characteristics are impacted by the temperature and pressure is also examined. This analysis provides a general overview of the factors that have the strongest impact on the formation and stability of nanoaggregates and clusters of nanoaggregates.
%G English
%L hal-01781969
%U https://hal.archives-ouvertes.fr/hal-01781969
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ IPREM-CAPT

%0 Journal Article
%T Molecular Dynamics Study of Nanoaggregation in Asphaltene Mixtures: Effects of the N, O, and S Heteroatoms
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Santos Silva, Hugo
%A Sodero, A.C.R.
%A Bouyssière, Brice
%A Hervé, Carrier
%A Korb, J.-P.
%A Alfarra, A.
%A Vallverdu, Germain
%A Bégué, Didier
%A Baraille, Isabelle
%< avec comité de lecture
%@ 0887-0624
%J Energy and Fuels
%I American Chemical Society
%V 30
%N 7
%P 5656-5664
%8 2016
%D 2016
%R 10.1021/acs.energyfuels.6b01170
%K Interaction energies
%K Heteroatoms
%K Chemical characteristic
%K Chains
%K Chain-ends
%K Asphaltenes
%K Relative concentration
%K Nanoaggregation
%K Molecular dynamics
%K Molecular dynamics simulations
%K Molecules
%K Lateral chains
%Z Chemical SciencesJournal articles
%X In this paper the aggregation of asphaltenes is studied for two asphaltene molecule families, namely, PA3 and CA22 analogues, on the basis of the work of Schuler et al. (J. Am. Chem. Soc., 2015, 137, 9870). The chemical characteristics of these molecules were screened by changing the heteroatoms on the backbone and the lateral chain-ends. These molecules were mixed together with different relative concentrations, and for the first time, the aggregation of different asphaltenes was determined using molecular dynamics simulations (MDS). The results show that the interaction energies vary for different heteroatom arrangement within a given structure and depend on the type of asphaltene. Moreover, we showed that the chain-ends have a crucial role on this phenomenon.
%G English
%L hal-01495764
%U https://hal.archives-ouvertes.fr/hal-01495764
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ IPREM-CAPT

%0 Journal Article
%T Possible Existence of a Monovalent Coordination for Nitrogen Atoms in LixPOyNz Solid Electrolyte: Modeling of X-ray Photoelectron Spectroscopy and Raman Spectra.
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Guille, Emilie
%A Vallverdu, Germain
%A Tison, Yann
%A Bégué, Didier
%A Baraille, Isabelle
%< avec comité de lecture
%@ 1932-7447
%J Journal of Physical Chemistry C
%I American Chemical Society
%V 119
%P 23379--23387
%8 2015
%D 2015
%R 10.1021/acs.jpcc.5b08427
%K lithium nitride phosphate electrolyte nitrogen atom monovalent coordination model
%Z Chemical Sciences/Theoretical and/or physical chemistryJournal articles
%X LixPOyNz is an amorphous solid electrolyte widely used in microbattery devices. The present study, based on a confrontation between expt. and theory, aims at providing new knowledge regarding the ionic cond. model of such systems in correlation with its structure. The computational strategy involved mol. dynamic simulations and first-principle calcns. on mol. and periodic models. The exptl. target data involve electronic and vibrational properties and were considered through the simulation of Raman and X-ray photoemission spectra in order to identify characteristic patterns of LixPOyNz. In particular, the presence of short phosphate chains is suggested by mol. dynamics calcns., and the simulation of Raman spectra clearly evidenced a new coordination for nitrogen atoms in the amorphous state, not considered until now in the exptl. structural model of the electrolyte and initially hypothesized based on core level binding energy computations. Monovalent nitrogen atoms together with short phosphate chains were used to build a structural model of the electrolyte and appeared to lead to a better reprodn. of the target exptl. results, while it implies a necessary refinement of the diffusion schemes commonly considered for lithium ions.
%G English
%L hal-01536057
%U https://hal.archives-ouvertes.fr/hal-01536057
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS
%~ IPREM-CAPT
%~ IPREM-PCM

%0 Journal Article
%T First-principle calculation of core level binding energies of LixPOyNz solid electrolyte.
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Guille, Emilie
%A Vallverdu, Germain
%A Baraille, Isabelle
%< avec comité de lecture
%@ 0021-9606
%J Journal of Chemical Physics
%I American Institute of Physics
%V 141
%P 244703/1-244703/9
%8 2014
%D 2014
%R 10.1063/1.4904720
%K core level energy XPS lithium nitride phosphate electrolyte DFT
%Z Chemical Sciences/Theoretical and/or physical chemistryJournal articles
%X We present first-principle PAW-DFT-PBE calcns. of core-level binding energies for the study of insulating, bulk phase, compds., based on the Slater-Janak transition state model. Those calcns. were performed in order to find a reliable model of the amorphous LixPOyNz solid electrolyte which is able to reproduce its electronic properties gathered from X-ray photoemission spectroscopy (XPS) expts. As a starting point, Li2PO2N models were investigated. These models, proposed by Du et al. on the basis of thermodn. and vibrational properties, were the first structural models of LixPOyNz. Thanks to chem. and structural modifications applied to Li2PO2N structures, which allow to demonstrate the relevance of our computational approach, we raise an issue concerning the possibility of encountering a non-bridging kind of nitrogen atoms ( = N-) in LixPOyNz compds.
%G English
%L hal-01536062
%U https://hal.archives-ouvertes.fr/hal-01536062
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ INC-CNRS

%0 Journal Article
%T First principles calculations of solid-solid interfaces: An application to conversion materials for lithium-ion batteries
%+ Laboratoire SUBATECH Nantes (SUBATECH)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Commissariat à l'énergie atomique et aux énergies alternatives - Laboratoire d'Electronique et de Technologie de l'Information (CEA-LETI)
%A Martin, L.
%A Vallverdu, Germain
%A Martinez, Hervé
%A Le Cras, F.
%A Baraille, Isabelle
%< avec comité de lecture
%J J. Mater. Chem.
%V 22
%N 41
%P 22063--22071
%8 2012
%D 2012
%R 10.1039/c2jm35078e
%K Periodic density functional theory
%K Model structures
%K Lithium-ion battery
%K Interfaces (materials)
%K Structural models
%K Solid-solid interfaces
%K Stable systems
%K Chemical compositions
%K Conversion reactions
%K Density functional theory
%K Diffusion in solids
%K Electrode material
%K First-principles calculation
%K Grand potential
%Z Chemical SciencesJournal articles
%X Using periodic density functional theory approaches, the thermodynamic stability of solid-solid interfaces generated during the conversion reaction of copper oxide which is a promising electrode material is investigated. Previous experimental results showed that conversion reactions generate a huge proportion of solid-solid interfaces among Cu 2O-Cu, Li 2O-Cu and Cu 2O-Li 2O. Interface grand potentials as a function of the voltage against LiLi + were computed in order to determine the chemical composition of the most stable interfaces. Then a structural model of the electrode material is proposed, based on the works of adhesion of the most stable systems identified in the first step.
%G English
%L hal-01499190
%U https://hal.archives-ouvertes.fr/hal-01499190
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ UNIV-NANTES
%~ IN2P3
%~ SUB
%~ CEA
%~ SUBATECH
%~ DRT
%~ INC-CNRS
%~ MINES-NANTES
%~ LETI
%~ IMTA_SUBATECH
%~ IMT-ATLANTIQUE
%~ SUBATECH-PLASMA
%~ CEA-GRE

%0 Journal Article
%T Mesoscopic simulations of shock-to-detonation transition in reactive liquid high explosive
%+ DAM Île-de-France (DAM/DIF)
%+ Institut Pluridisciplinaire de Recherche sur l'Environnement et les Matériaux (IPREM)
%+ Centre d'Enseignement et de Recherche en Mathématiques et Calcul Scientifique (CERMICS)
%A Maillet, Jean-Bernard
%A Bourasseau, Emeric
%A Desbiens, Nicolas
%A Vallverdu, Germain
%A Stoltz, Gabriel
%< avec comité de lecture
%@ 0295-5075
%J EPL - Europhysics Letters
%I European Physical Society/EDP Sciences/Società Italiana di Fisica/IOP Publishing
%V 96
%N 6
%P 68007
%8 2011-12-14
%D 2011
%R 10.1209/0295-5075/96/68007
%Z 05.10.-a,82.40.Fp,05.70.Ln
%Z Physics [physics]/Physics [physics]/Computational Physics [physics.comp-ph]
%Z Physics [physics]/Physics [physics]/Chemical Physics [physics.chem-ph]Journal articles
%X An extension of the model described in a previous work (see Maillet J. B. et al., EPL, 78 (2007) 68001) based on Dissipative Particle Dynamics is presented and applied to a liquid high explosive (HE), with thermodynamic properties mimicking those of liquid nitromethane. Large scale nonequilibrium simulations of reacting liquid HE with model kinetic under sustained shock conditions allow a better understanding of the shock-to-detonation transition in homogeneous explosives. Moreover, the propagation of the reactive wave appears discontinuous since ignition points in the shocked material can be activated by the compressive waves emitted from the onset of chemical reactions.
%G English
%L hal-00676470
%U https://hal-enpc.archives-ouvertes.fr/hal-00676470
%~ CEA
%~ ENPC
%~ CERMICS
%~ PARISTECH
%~ UNIV-PAU
%~ IPREM
%~ CNRS
%~ INC-CNRS
%~ DAM

%0 Journal Article
%T Cyan fluorescent protein : molecular dynamics, simulations, and electronic absorption spectrum
%+ Laboratoire de Chimie Physique D'Orsay (LCPO)
%A Demachy, I.
%A Ridard, J.
%A Laguitton-Pasquier, H.
%A Durnerin, E.
%A Vallverdu, Germain
%A Archirel, P.
%A Levy, B.
%< avec comité de lecture
%Z Publications dans des revues à comité de lecture
%@ 1520-6106
%J Journal of Physical Chemistry B
%I American Chemical Society
%V 109 (50)
%P 24121-24133
%8 2005
%D 2005
%Z Physics [physics]/Physics [physics]/Chemical Physics [physics.chem-ph]Journal articles
%G English
%L hal-00108327
%U https://hal.archives-ouvertes.fr/hal-00108327
%~ CNRS
%~ INC-CNRS
%~ UNIV-PSUD

%0 Journal Article
%T Understanding the vanadium−asphaltene nanoaggregate link with silver triflate complexation and GPC ICP-MS analysis
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%A Moulian, Rémi
%A Fang, Zheng
%A Vallverdu, Germain
%A Barrere-Mangote, Caroline
%A Shi, Quan
%A Giusti, Pierre
%A Bouyssière, Brice
%Z Conseil Régional d’Aquitaine
%< avec comité de lecture
%@ 0887-0624
%J Energy and Fuels
%I American Chemical Society
%V 34
%N 11
%P 13759-13766
%8 2020
%D 2020
%K Asphaltenes Gel permeation chromatography Inductively coupled plasma Inductively coupled plasma mass spectrometry Mass spectrometers Molecular weight Porphyrins Vanadium compounds
%Z Chemical Sciences
%Z Chemical Sciences/Analytical chemistry
%Z Chemical Sciences/Theoretical and/or physical chemistryJournal articles
%X The effect of silver triflate (AgOTf) on the interaction between vanadium and asphaltene nanoaggregates was investigated by gel permeation chromatography inductively coupled plasma high-resolution mass spectrometry (GPC ICP-HR-MS). The results showed that disaggregation of some vanadium compounds linked to asphaltene nanoaggregates occurred when silver triflate was added. Ag+ can partially move some porphyrins from the high-molecular-weight (HMW) region to the low-molecular-weight (LMW) region. It was inferred that the interaction between Ag+ and the porphyrins surroundings led to a decrease in the size of the nanoaggregates (HMW region) and an increase in the “free” V porphyrin compounds (MMW and LMW regions). Asphaltenes from a similar origin, presenting the same vanadium GPC ICP-MS profile, gave different GPC ICP-MS profiles after AgOTf addition, which could be linked to the difference in geochemistry of the samples.
%G English
%L hal-03021936
%U https://hal-univ-pau.archives-ouvertes.fr/hal-03021936
%~ UNIV-PAU
%~ IPREM
%~ IPREM-CAPT
%~ INC-CNRS
%~ CNRS
%~ ANR

%0 Journal Article
%T Relating the molecular topology and local geometry: Haddon’s pyramidalization angle and the Gaussian curvature
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Laboratoire de Mathématiques et de leurs Applications [Pau] (LMAP)
%A Sabalot-Cuzzubbo, Julia
%A Vallverdu, Germain
%A Bégué, Didier
%A Cresson, Jacky
%< avec comité de lecture
%@ 0021-9606
%J Journal of Chemical Physics
%I American Institute of Physics
%V 152
%N 24
%P 244310
%8 2020-06-28
%D 2020
%R 10.1063/5.0008368
%Z Chemical Sciences/Material chemistry
%Z Chemical Sciences/Polymers
%Z Chemical Sciences/Theoretical and/or physical chemistry
%Z Chemical Sciences/Analytical chemistryJournal articles
%X The pyramidalization angle and spherical curvature are well-known quantities used to characterize the local geometry of a molecule and to provide a measure of regio-chemical activity of molecules. In this paper, we give a self-contained presentation of these two concepts and discuss their limitations. These limitations can bypass, thanks to the introduction of the notions of angular defect and discrete Gauss curvature coming from discrete differential geometry. In particular, these quantities can be easily computed for arbitrary molecules, trivalent or not, with bond of equal lengths or not. All these quantities have been implemented. We then compute all these quantities over the Tománek database covering an almost exhaustive list of fullerene molecules. In particular, we discuss the interdependence of the pyramidalization angle with the spherical curvature, angular defect, and hybridization numbers. We also explore the dependence of the pyramidalization angle with respect to some characteristics of the molecule, such as the number of atoms, the group of symmetry, and the geometrical optimization process.
%G English
%L hal-02902664
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02902664
%~ UNIV-PAU
%~ IPREM
%~ IPREM-CAPT
%~ INC-CNRS
%~ CNRS
%~ LMA-PAU

%0 Journal Article
%T The role of metalloporphyrins on the physical-chemical properties of petroleum fluids.
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Laboratoire de physique de la matière condensée (LPMC)
%+ TOTAL Refining and Chemical
%A Santos Silva, Hugo
%A Sodero, Ana C. R.
%A Korb, Jean-Pierre
%A Alfarra, Ahmad
%A Giusti, Pierre
%A Vallverdu, Germain
%A Bégué, Didier
%A Baraille, Isabelle
%A Bouyssière, Brice
%< avec comité de lecture
%@ 0016-2361
%J Fuel
%I Elsevier
%V 188
%P 374-381
%8 2017
%D 2017
%R 10.1016/j.fuel.2016.10.065
%K metalloporphyrin phys chem property petroleum fluid
%Z Chemical Sciences/Analytical chemistryJournal articles
%X The presence of metalloporphyrins in crude oil was known by many years now but their role on the phys.-chem. properties of petroleum fluids, such as the aggregation of the high-mol. wt. phases, remains unknown. In this paper, these properties are studied using different mol. modeling techniques (Mol. Dynamics, Semi-empirical PM7 and D. Functional Theory). This combined methodol. allowed us characterizing the nature of these interactions, how it dominates the electronic structure of the stacked mols. and what is their participation on the formation of the nano-, micro- and macro-aggregates.
%G English
%L hal-01481296
%U https://hal.archives-ouvertes.fr/hal-01481296
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ X
%~ X-PMC
%~ INC-CNRS
%~ X-SACLAY
%~ UNIV-PARIS-SACLAY
%~ IPREM-CAPT
%~ X-DEP
%~ X-DEP-PHYS

%0 Journal Article
%T First principle study of the surface reactivity of layered lithium oxides LiMO2 (M = Ni, Mn, Co)
%+ Institut des sciences analytiques et de physico-chimie pour l'environnement et les materiaux  (IPREM)
%+ Advanced Lithium Energy Storage Systems - ALISTORE-ERI (ALISTORE-ERI)
%+ Réseau sur le stockage électrochimique de l'énergie (RS2E)
%A Vallverdu, Germain
%A Minvielle, M.
%A Andreu, N.
%A Gonbeau, Danielle
%A Baraille, Isabelle
%< avec comité de lecture
%@ 0039-6028
%J Surface Science
%I Elsevier
%V 649
%P 46-55
%8 2016
%D 2016
%R 10.1016/j.susc.2016.01.004
%K Surface reactivity
%K Adsorption
%K DFT
%K NMC
%K Positive electrode materials
%K First principle calculations
%Z Chemical Sciences
%Z Chemical Sciences/Theoretical and/or physical chemistryJournal articles
%X LiNixMnyCo1 - x - yO2 compounds (NMC) are layered oxides widely used in commercial lithium-ion batteries at the positive electrode. Nevertheless surface reactivity of this material is still not well known. As a first step, based on first principle calculations, this study deals with the electronic properties and the surface reactivity of LiMO2 (M = Co, Ni, Mn) compounds, considering the behavior of each transition metal separately in the same R3m α-NaFeO2-type structure, the one of LiCoO2 and NMC. For each compound, after a brief description of the bare slab electronic properties, we explored the acido-basic and redox properties of the (110) and (104) surfaces by considering the adsorption of a gaseous probe. The chemisorption of SO2 produces both sulfite or sulfate species associated respectively to an acido-basic or a reduction process. These processes are localized on the transition metals of the first two layers of the surface. Although sulfate species are globally favored, a different behavior is obtained depending on both the surface and the transition metal considered. We conclude with a simple scheme which describes the reduction processes on the both surfaces in terms of formal oxidation degrees of transition metals
%G English
%L hal-01495814
%U https://hal.archives-ouvertes.fr/hal-01495814
%~ IPREM
%~ CNRS
%~ UNIV-PAU
%~ RS2E
%~ INC-CNRS
%~ IPREM-CAPT
%~ CDF
%~ UNIV-PICARDIE
%~ ENSC-MONTPELLIER
%~ ENSC-PARIS
%~ INPG
%~ ENSCP
%~ PSL
%~ PARISTECH
%~ UNIV-MONTPELLIER
%~ UNIV-NANTES
%~ UNIV-AMU
%~ UGA
%~ SITE-ALSACE
%~ ENSCP-PSL
%~ CDF-PSL
%~ SU-INF-2018
%~ UGA-COMUE
%~ SORBONNE-UNIVERSITE
%~ U-PICARDIE
%~ ALISTORE-ERI
%~ INPT

