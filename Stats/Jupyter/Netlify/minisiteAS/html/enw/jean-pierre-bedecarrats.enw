%0 Journal Article
%T Comparison of different modelings of pure substances during melting in a DSC experiment
%A Gibout, Stéphane
%A Franquet, Erwin
%A Bedecarrats, Jean-Pierre
%A Dumas, Jean-Pierre
%< avec comité de lecture
%@ 0040-6031
%J Thermochimica Acta
%I Elsevier
%V 528
%P 1-8
%8 2012-01
%D 2012
%R 10.1016/j.tca.2011.10.019
%Z Physics [physics]/Mechanics [physics]/Mechanics of the fluids [physics.class-ph]
%Z Engineering Sciences [physics]/Reactive fluid environment
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Thermics [physics.class-ph]Journal articles
%G English
%L hal-02101435
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02101435
%~ UNIV-PAU
%~ LATEP

%0 Journal Article
%T Predictive approach of heat transfer for the modelling of large-scale latent heat storages
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ Département des Technologies Biomasse et Hydrogène (DTBH)
%A Beust, Clément
%A Franquet, Erwin
%A Bedecarrats, Jean-Pierre
%A Garcia, Pierre
%< avec comité de lecture
%@ 0960-1481
%J Renewable Energy
%I Elsevier
%8 2020-05
%D 2020
%R 10.1016/j.renene.2020.04.135
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%G English
%L hal-02566401
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02566401
%~ UNIV-PAU
%~ CEA
%~ UNIV-SAVOIE
%~ DRT
%~ LITEN
%~ LATEP
%~ CNRS
%~ CEA-GRE

%0 Journal Article
%T Inverse method for the identification of the enthalpy of phase change materials from calorimetry experiments
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Franquet, Erwin
%A Gibout, Stéphane
%A Bedecarrats, Jean-Pierre
%A Haillot, Didier
%A Dumas, Jean-Pierre
%< avec comité de lecture
%@ 0040-6031
%J Thermochimica Acta
%I Elsevier
%V 546
%P 61-80
%8 2012-10
%D 2012
%R 10.1016/j.tca.2012.07.015
%Z Engineering Sciences [physics]/Chemical and Process Engineering
%Z Engineering Sciences [physics]/Materials
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Thermics [physics.class-ph]Journal articles
%G English
%L hal-02096196
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02096196
%~ UNIV-PAU
%~ LATEP
%~ TESTUPPA

%0 Journal Article
%T Optimization of solar DHW system including PCM media
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ Computational Approximation with discontinous Galerkin methods and compaRison with Experiments (CAGIRE)
%A Haillot, Didier
%A Franquet, Erwin
%A Gibout, Stéphane
%A Bedecarrats, Jean-Pierre
%< avec comité de lecture
%@ 0306-2619
%J Applied Energy
%I Elsevier
%V 109
%P 470-475
%8 2013-09
%D 2013
%R 10.1016/j.apenergy.2012.09.062
%Z Engineering Sciences [physics]/Materials
%Z Engineering Sciences [physics]/Chemical and Process Engineering
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Thermics [physics.class-ph]Journal articles
%G English
%L hal-02096226
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02096226
%~ UNIV-PAU
%~ LATEP
%~ CNRS
%~ LMA-PAU
%~ INRIA_TEST
%~ TESTBORDEAUX
%~ INRIA2
%~ INRIA
%~ INRIA-BORDEAUX

%0 Journal Article
%T Sodium nitrate thermal behavior in latent heat thermal energy storage: A study of the impact of sodium nitrite on melting temperature and enthalpy
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ Computational Approximation with discontinous Galerkin methods and compaRison with Experiments (CAGIRE)
%+ Université de Pau et des Pays de l'Adour (UPPA)
%A Lomonaco, Adrien
%A Haillot, Didier
%A Pernot, Eric
%A Franquet, Erwin
%A Bedecarrats, Jean-Pierre
%< avec comité de lecture
%@ 0927-0248
%J Solar Energy Materials and Solar Cells
%I Elsevier
%V 149
%P 81-87
%8 2016-05
%D 2016
%R 10.1016/j.solmat.2015.12.043
%K Differential scanning calorimetry
%K Isothermal step method
%K Latent thermal energy storage NaNO3+NaNO2 compounds
%K Phase change materials
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%X Using direct steam generation in concentrated solar power plant leads to the development of new storage systems, which may include a latent heat thermal energy storage module. With this module, the behavior of the storage system matches the thermal behavior of the water used as heat transfer fluid. Sodium nitrate, which is already used in such lab-scale systems, is able to fulfill the conditions to assume the role of phase change material to store thermal energy. However, this material may evolve during its lifetime, either due to thermal decomposition or by corrosion phenomena, both leading to the reduction of nitrates to nitrites. Such evolution has been reported for the so-called "solar–salt" (NaNO3–KNO3 60:40 wt%), driving to the reduction of the melting temperature and latent heat. It is thus important to study the evolution of these properties in the case of sodium nitrate. The work proposed here aims to characterize sodium nitrate–nitrite mixture by differential scanning calorimetry for comparison with pure sodium nitrate (laboratory grade and industrial grade). Since the considered compounds are not at the eutectic composition, their melting does not occur at a fixed and single temperature. Thus, a calorimetric method using isothermal steps has been used in order to determine the evolution of involved energy during the melting of samples versus temperature for each sample. The results show the spreading of the melting over a range of temperature and the shifting of the liquidus to lower temperature when the sodium nitrite part increases. Moreover, a decrease in latent heat up to nearly 15% was observed.
%G English
%L hal-02089608
%U https://hal.archives-ouvertes.fr/hal-02089608
%~ CNRS
%~ UNIV-PAU
%~ LMA-PAU
%~ INRIA_TEST
%~ TESTBORDEAUX
%~ INRIA2
%~ INRIA
%~ INRIA-BORDEAUX
%~ LATEP

%0 Journal Article
%T Description of a 3D transient model to predict the performance of an experimental Thermoelectric Generator under varying inlet gas conditions
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%A Jimenez Aispuro, J.E.
%A Bedecarrats, Jean-Pierre
%A Gibout, Stéphane
%A Champier, Daniel
%< avec comité de lecture
%@ 2214-7853
%J Materials Today: Proceedings
%I Elsevier
%V 8
%P 632-641
%8 2019
%D 2019
%R 10.1016/j.matpr.2019.02.063
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%G English
%L hal-02103974
%U https://hal.archives-ouvertes.fr/hal-02103974
%~ UNIV-PAU
%~ SIAME
%~ LATEP
%~ TESTUPPA

%0 Journal Article
%T Numerical optimization of the occupancy rate of thermoelectric generators to produce the highest electrical power
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%A Favarel, Camille
%A Bedecarrats, Jean-Pierre
%A Kousksou, Tarik
%A Champier, Daniel
%< avec comité de lecture
%@ 0360-5442
%J Energy
%I Elsevier
%V 68
%P 104-116
%8 2014-04
%D 2014
%Z Engineering Sciences [physics]/Electric powerJournal articles
%G English
%L hal-02014922
%U https://hal.archives-ouvertes.fr/hal-02014922
%~ UNIV-PAU
%~ LATEP
%~ SIAME
%~ TESTUPPA

%0 Journal Article
%T Simulation of the thermal transfer during an eutectic melting of a binary solution
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ Université de Pau et des Pays de l'Adour (UPPA)
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%A Jaafar, Mohamad Ali
%A Rousse, Daniel
%A Bedecarrats, Jean-Pierre
%A Jamil, A.
%A Kousksou, Tarik
%A Zeraouli, Youssef
%A Gibout, Stéphane
%A Dumas, Jean-Pierre
%< avec comité de lecture
%@ 0040-6031
%J Thermochimica Acta
%I Elsevier
%V 441
%N 1
%P 30-34
%8 2006-02
%D 2006
%R 10.1016/j.tca.2005.11.010
%Z Engineering Sciences [physics]/Materials
%Z Engineering Sciences [physics]/Chemical and Process Engineering
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Thermics [physics.class-ph]Journal articles
%G English
%L hal-02096231
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02096231
%~ UNIV-PAU
%~ LATEP
%~ SIAME
%~ TESTUPPA

%0 Journal Article
%T Study of a TE (thermoelectric) generator incorporated in a multifunction wood stove
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%+ Université de Pau et des Pays de l'Adour (UPPA)
%A Champier, Daniel
%A Bedecarrats, Jean-Pierre
%A Kousksou, Tarik
%A Rivaletto, Marc
%A Strub, F.
%A Pignolet, P.
%< avec comité de lecture
%@ 0360-5442
%J Energy
%I Elsevier
%V 36
%N 3
%P 1518-1526
%8 2011-03
%D 2011
%Z Engineering Sciences [physics]/Electric powerJournal articles
%G English
%L hal-02014932
%U https://hal.archives-ouvertes.fr/hal-02014932
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA

%0 Journal Article
%T Thermoelectric power generation from biomass cook stoves
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%+ Université de Pau et des Pays de l'Adour (UPPA)
%A Champier, Daniel
%A Bedecarrats, Jean-Pierre
%A Rivaletto, Marc
%A Strub, F.
%< avec comité de lecture
%@ 0360-5442
%J Energy
%I Elsevier
%V 35
%N 2
%P 935-942
%8 2010-02
%D 2010
%Z Engineering Sciences [physics]/Electric powerJournal articles
%G English
%L hal-02014935
%U https://hal.archives-ouvertes.fr/hal-02014935
%~ UNIV-PAU
%~ SIAME
%~ TESTUPPA

%0 Journal Article
%T Experimental analysis with numerical comparison for different thermoelectric generators configurations
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%A Favarel, Camille
%A Bedecarrats, Jean-Pierre
%A Kousksou, Tarik
%A Champier, Daniel
%< avec comité de lecture
%@ 0196-8904
%J Energy Conversion and Management
%I Elsevier
%V 107
%P 114-122
%8 2016-01
%D 2016
%Z Engineering Sciences [physics]/Electric powerJournal articles
%G English
%L hal-02014921
%U https://hal.archives-ouvertes.fr/hal-02014921
%~ UNIV-PAU
%~ LATEP
%~ SIAME
%~ TESTUPPA

%0 Journal Article
%T Material screening and compatibility for thermocline storage systems using thermal oil
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ Commissariat à l'énergie atomique et aux énergies alternatives (CEA)
%A Molina, Sophie
%A Haillot, Didier
%A Deydier, Alexandre
%A Bedecarrats, Jean-Pierre
%< avec comité de lecture
%@ 1359-4311
%J Applied Thermal Engineering
%I Elsevier
%V 146
%P 252-259
%8 2019-01
%D 2019
%R 10.1016/j.applthermaleng.2018.09.094
%Z Engineering Sciences [physics]
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%G English
%L hal-02086314
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02086314
%~ UNIV-PAU
%~ CEA
%~ LATEP

%0 Journal Article
%T A review of dendritic growth during solidification: Mathematical modeling and numerical simulations
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Jaafar, Mohamad Ali
%A Rousse, Daniel
%A Gibout, Stéphane
%A Bedecarrats, Jean-Pierre
%< avec comité de lecture
%@ 1364-0321
%J Renewable and Sustainable Energy Reviews
%I Elsevier
%V 74
%P 1064-1079
%8 2017-07
%D 2017
%R 10.1016/j.rser.2017.02.050
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%G English
%L hal-02089606
%U https://hal.archives-ouvertes.fr/hal-02089606
%~ UNIV-PAU
%~ LATEP
%~ TESTUPPA

%0 Journal Article
%T Ice slurry production using supercooling phenomenon
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ STMicroelectronics [Crolles] (ST-CROLLES)
%+ Physique et Ingénierie Mathématique pour l'Énergie, l'environnemeNt et le bâtimenT (PIMENT)
%A Bedecarrats, Jean-Pierre
%A David, Thomas
%A Castaing-Lasvignottes, Jean
%< avec comité de lecture
%@ 0140-7007
%J International Journal of Refrigeration
%I Elsevier
%V 33
%N 1
%P alainbastide
%8 2010-01
%D 2010
%R 10.1016/j.ijrefrig.2009.08.012
%Z Engineering Sciences [physics]Journal articles
%G English
%L hal-01153021
%U https://hal.archives-ouvertes.fr/hal-01153021
%~ UNIV-PAU
%~ UNIV-REUNION
%~ PIMENT
%~ PAPANGUE
%~ LATEP
%~ TESTUPPA

%0 Journal Article
%T Impact of the enthalpy function on the simulation of a building with phase change material wall
%+ Centre d'Energétique et de Thermique de Lyon (CETHIL)
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ Laboratoire Génie Civil et Géo-Environnement [Béthune] (LGCgE)
%A Kuznik, F.
%A Johannes, Kevyn
%A Franquet, Erwin
%A Zalewski, L.
%A Gibout, Stéphane
%A Tittelein, P.
%A Dumas, Jean-Pierre
%A David, D.
%A Bedecarrats, Jean-Pierre
%A Lassue, S.
%< avec comité de lecture
%@ 0378-7788
%J Energy and Buildings
%I Elsevier
%V 126
%P 220-229
%8 2016-08
%D 2016
%R 10.1016/j.enbuild.2016.05.046
%Z Engineering Sciences [physics]Journal articles
%G English
%L hal-02095682
%U https://hal.archives-ouvertes.fr/hal-02095682
%~ CNRS
%~ UNIV-PAU
%~ UNIV-LYON1
%~ CETHIL
%~ INSA-LYON
%~ INSA-GROUPE
%~ LATEP
%~ UNIV-ARTOIS
%~ UDL
%~ UNIV-LYON
%~ LGCGE

%0 Journal Article
%T Modeling phase change materials behavior in building applications: Comments on material characterization and model validation
%+ Laboratoire Génie Civil et Géo-Environnement [Béthune] (LGCgE)
%+ Centre d'Energétique et de Thermique de Lyon (CETHIL)
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ Departament d'Informatica i Enginyeria Industrial
%A Dutil, Yvan
%A Rousse, Daniel
%A Lassue, Stéphane
%A Zalewski, Laurent
%A Joulin, Annabelle
%A Virgone, Joseph
%A Kuznik, Frederic
%A Johannes, Kévyn
%A Dumas, Jean-Pierre
%A Bedecarrats, Jean-Pierre
%A Castell, Albert
%A Cabeza, Luisa F.
%< avec comité de lecture
%@ 0960-1481
%J Renewable Energy
%I Elsevier
%N 61
%P 132-135
%8 2014
%D 2014
%R 10.1016/j.renene.2012.10.027
%K Phase change material
%K PCM characterization
%K Mathematical model
%K Model validation
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Thermics [physics.class-ph]Journal articles
%X In a recent meeting of IEA’s Annex 23, several members presented their conclusions on the modeling of phase change materials behavior in the context of building applications. These conclusions were in agreement with those of a vast review, involving the survey of more than 250 journal papers, undertaken earlier by the group of École de technologie supérieure. In brief, it can be stated that, at this point, the confidence in reviewed models is too low to use them to predict the future behavior of a building with confidence. Moreover, it was found that overall thermal behaviors of phase change material are poorly known, which by itself creates an intrinsic unknown in any model. Models themselves are most of time suspicious as they are often not tested in a very stringent or exhaustive way. In addition, it also appears that modeling parameters are somewhat too simplified to realistically describe the complete physics needed to predict the real life performance of PCMs added to a building. As a result, steps are now taken to create standard model benchmarks that will improve the confidence of the users. Hopefully, following these efforts, confidence will increase and usage of PCM in buildings should be eased.
%G English
%L hal-01289471
%U https://hal.archives-ouvertes.fr/hal-01289471
%~ CNRS
%~ UNIV-PAU
%~ UNIV-ARTOIS
%~ CETHIL
%~ INSA-LYON
%~ UNIV-LYON1
%~ LABEXIMU
%~ INSA-GROUPE
%~ LATEP
%~ UDL
%~ UNIV-LYON
%~ LGCGE

%0 Journal Article
%T Interpretation of calorimetry experiments to characterise phase change materials
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ Laboratoire Génie Civil et Géo-Environnement [Béthune] (LGCgE)
%+ Centre de Thermique de Lyon (CETHIL)
%+ Computational Approximation with discontinous Galerkin methods and compaRison with Experiments (CAGIRE)
%A Dumas, Jean-Pierre
%A Gibout, Stéphane
%A Zalewski, Laurent
%A Johannes, Kévyn
%A Franquet, Erwin
%A Bedecarrats, Jean-Pierre
%A Tittelein, Pierre
%A Kuznik, Frédéric
%< avec comité de lecture
%@ 1290-0729
%J International Journal of Thermal Sciences
%I Elsevier
%V 78
%P 48-55
%8 2014-04
%D 2014
%R 10.1016/j.ijthermalsci.2013.11.014
%K Phase change material (PCM)
%K Energy storage
%K DSC
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Thermics [physics.class-ph]
%Z Physics [physics]/Mechanics [physics]/Thermics [physics.class-ph]Journal articles
%X In the building field, the topic of thermal storage is generally studied with assistance from dedicated software programs. To generate transient thermal simulations, these software programs use enthalpy functions h (T) to describe the thermal behaviour of the different parts of a modelled structure. Unfortunately, the mathematical form of these functions is often extremely unrealistic due to an erroneous interpretation of the calorimetric experiments that were performed to determine these functions. The purpose of this study was to evaluate the energy-related errors that occur if a misinterpreted enthalpy function is used and to thereby assess the impact that these inaccurate functions generate with respect to thermal simulations of buildings.
%G English
%L hal-01016397
%U https://hal.archives-ouvertes.fr/hal-01016397
%~ UNIV-ARTOIS
%~ CNRS
%~ INRIA
%~ INRIA-BORDEAUX
%~ TESTBORDEAUX
%~ UNIV-PAU
%~ LMA-PAU
%~ INRIA_TEST
%~ CETHIL
%~ INRIA2
%~ INSA-LYON
%~ UNIV-LYON1
%~ INSA-GROUPE
%~ LATEP
%~ UDL
%~ UNIV-LYON
%~ LGCGE

%0 Journal Article
%T Simulation of the thermal and energy behaviour of a composite material containing encapsulated- PCM : influence of the thermodynamical modelling
%+ Laboratoire Génie Civil et Géo-Environnement [Béthune] (LGCgE)
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ Computational Approximation with discontinous Galerkin methods and compaRison with Experiments (CAGIRE)
%+ Centre d'Energétique et de Thermique de Lyon (CETHIL)
%A Tittelein, Pierre
%A Gibout, Stéphane
%A Franquet, Erwin
%A Johannes, Kévyn
%A Zalewski, Laurent
%A Kuznik, Frederic
%A Dumas, Jean-Pierre
%A Lassue, Stéphane
%A Bedecarrats, Jean-Pierre
%A David, Damien
%< avec comité de lecture
%@ 0306-2619
%J Applied Energy
%I Elsevier
%V 140
%N 15
%P 269-274
%8 2015
%D 2015
%R 10.1016/j.apenergy.2014.11.055
%K Phase change material
%K Phase change modelling
%K Building material
%K Thermodynamics and heat transfer simulation
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Thermics [physics.class-ph]Journal articles
%X The objective of this study is to evaluate three different phase change models used to predict the energy behaviour of a PCM cement mortar sample. Reference data are measured on large samples of composite material using a special experimental set-up. The phase change models tested are: the apparent specific capacity method, the enthalpy method assuming a pure body and the enthalpy method assuming a binary mixture. Numerical results are compared to the reference data of heat flux and energy stored/released. The main conclusions of the study are: (1) the thermodynamically inconsistent apparent specific capacity method is not suitable, (2) the enthalpy method gives better results than the specific capacity method and (3) the enthalpy method gives better results with an appropriate guess of the enthalpy curve.
%G English
%L hal-01287517
%U https://hal.archives-ouvertes.fr/hal-01287517
%~ CNRS
%~ UNIV-PAU
%~ UNIV-ARTOIS
%~ LMA-PAU
%~ INRIA-BORDEAUX
%~ INRIA
%~ INRIA_TEST
%~ TESTBORDEAUX2
%~ TESTBORDEAUX
%~ INRIA2
%~ CETHIL
%~ INSA-LYON
%~ UNIV-LYON1
%~ LABEXIMU
%~ INSA-GROUPE
%~ LATEP
%~ UDL
%~ UNIV-LYON
%~ LGCGE

%0 Journal Article
%T Study of a phase change energy storage using spherical capsules. Part I: Experimental results
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ Physique et Ingénierie Mathématique pour l'Énergie, l'environnemeNt et le bâtimenT (PIMENT)
%+ Geometry and Lighting (ALICE)
%A Bedecarrats, Jean-Pierre
%A Castaing-Lasvignottes, Jean
%A Strub, Françoise
%A Dumas, Jean-Pierre
%< avec comité de lecture
%@ 0196-8904
%J Energy Conversion and Management
%I Elsevier
%V 50
%N 10
%P 2527–2536
%8 2009-10
%D 2009
%R 10.1016/j.enconman.2009.06.004
%Z Physics [physics]
%Z Engineering Sciences [physics]Journal articles
%G English
%L hal-01153023
%U https://hal.archives-ouvertes.fr/hal-01153023
%~ CNRS
%~ UNIV-PAU
%~ UNIV-REUNION
%~ INRIA-LORRAINE
%~ INRIA-NANCY-GRAND-EST
%~ PIMENT
%~ LORIA2
%~ INRIA
%~ UNIV-LORRAINE
%~ INRIA_TEST
%~ LORIA-ALGO-TEST5
%~ INRIA2
%~ LORIA-ALGO
%~ LORIA
%~ PAPANGUE
%~ LATEP
%~ TESTUPPA

%0 Journal Article
%T Study of a phase change energy storage using spherical capsules. Part II: Numerical modelling
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ Physique et Ingénierie Mathématique pour l'Énergie, l'environnemeNt et le bâtimenT (PIMENT)
%+ Geometry and Lighting (ALICE)
%A Bedecarrats, Jean-Pierre
%A Castaing-Lasvignottes, Jean
%A Strub, Françoise
%A Dumas, Jean-Pierre
%< avec comité de lecture
%@ 0196-8904
%J Energy Conversion and Management
%I Elsevier
%V 50
%N 10
%P alainbastide
%8 2009-10
%D 2009
%R 10.1016/j.enconman.2009.06.003
%Z Engineering Sciences [physics]
%Z Physics [physics]Journal articles
%G English
%L hal-01153022
%U https://hal.archives-ouvertes.fr/hal-01153022
%~ CNRS
%~ UNIV-PAU
%~ UNIV-REUNION
%~ INRIA-LORRAINE
%~ INRIA-NANCY-GRAND-EST
%~ PIMENT
%~ LORIA2
%~ INRIA
%~ UNIV-LORRAINE
%~ INRIA_TEST
%~ LORIA-ALGO-TEST5
%~ INRIA2
%~ LORIA-ALGO
%~ LORIA
%~ PAPANGUE
%~ LATEP
%~ TESTUPPA

%0 Journal Article
%T Numerical study of thermoelectric power generation for an helicopter conical nozzle
%+ Laboratoire des Sciences de l'Ingénieur Appliquées à la Mécanique et au génie Electrique (SIAME)
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Kousksou, Tarik
%A Bedecarrats, Jean-Pierre
%A Champier, Daniel
%A Pignolet, Pascal
%A Brillet, Christophe
%< avec comité de lecture
%@ 0378-7753
%J Journal of Power Sources
%I Elsevier
%V 196
%N 8
%P 4026-4032
%8 2011-04
%D 2011
%Z Engineering Sciences [physics]/Electric powerJournal articles
%G English
%L hal-02014930
%U https://hal.archives-ouvertes.fr/hal-02014930
%~ UNIV-PAU
%~ LATEP
%~ SIAME

