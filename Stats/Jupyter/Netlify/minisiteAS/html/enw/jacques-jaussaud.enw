%0 Journal Article
%T Activisme des actionnaires et responsabilité sociale des entreprises : une comparaison Espagne - France - Etats-Unis - Japon
%+ Université Toulouse III - Paul Sabatier (UT3)
%+ IAE Paris - Sorbonne Business School
%+ Centre de recherche et d'études en gestion (CREG)
%+ Université de Pau et des Pays de l'Adour (UPPA)
%+ Universidade de Santiago de Compostela [Spain] (USC )
%A Bruno, Amann
%A Caby, Jérôme
%A Jaussaud, Jacques
%A Pineiro-Chousa, Juan
%< avec comité de lecture
%@ 1951-0187
%J Revue de l'Organisation Responsable
%I ESKA
%N 2
%8 2007
%D 2007
%K Assemblée générale AG
%K Environnement
%K Activisme actionnarial
%K Résolution
%Z Humanities and Social Sciences/Economics and Finance
%Z Humanities and Social Sciences/Business administrationJournal articles
%X L’activisme des actionnaires est l’expression d’un mécanisme de gouvernance des entreprises qui permet à tous les actionnaires d’exprimer leur voix (Voice au sens de Hirschman, 1973) dans de nombreux domaines et ceci, quelle que soit leur participation au capital. Ce papier étudie les manifestations de cet activisme en Espagne, aux Etats-Unis, en France et au Japon à travers l’examen des résolutions déposées en Assemblée Générale et des principaux mécanismes permettant aux actionnaires de faire entendre leur voix.
%G French
%L halshs-02025141
%U https://halshs.archives-ouvertes.fr/halshs-02025141
%~ SHS
%~ UNIV-PAU
%~ AO-ECONOMIE
%~ IAEPARIS
%~ CREG-EA4580
%~ UNIV-TLSE3

%0 Journal Article
%T L’emploi des seniors au Japon : enjeux et pratiques
%+ Centre de recherche sur les civilisations de l'Asie Orientale (CRCAO)
%+ Centre de recherche et d'études en gestion (CREG)
%A MARTINE, Julien
%A Jaussaud, Jacques
%< avec comité de lecture
%@ 2259-2490
%J RIMHE : Revue Interdisciplinaire Management, Homme(s) & Entreprise
%I Association pour la recherche interdisciplinaire sur le management des entreprises
%8 2017
%D 2017
%R 10.3917/rimhe.027.0103
%Z Humanities and Social Sciences/Business administration
%Z Humanities and Social Sciences/Sociology
%Z Humanities and Social Sciences/History
%Z Humanities and Social Sciences/Economics and Finance
%Z Humanities and Social Sciences/LawJournal articles
%G French
%L halshs-01649102
%U https://halshs.archives-ouvertes.fr/halshs-01649102
%~ SHS
%~ CNRS
%~ UNIV-PAU
%~ UNIV-PARIS7
%~ AO-ECONOMIE
%~ SOCIOLOGIE
%~ CDF
%~ AO-HISTOIRE
%~ AO-DROIT
%~ AO-SOCIOLOGIE
%~ HISTOIRE
%~ EPHE
%~ USPC
%~ CRCAO
%~ CREG-EA4580
%~ TEST-DEV
%~ CONDORCET1
%~ CAMPUS-CONDORCET
%~ UNIV-PARIS
%~ UP-SOCIETES-HUMANITES
%~ CDF-PSL
%~ EPHE-PSL
%~ PSL

%0 Journal Article
%T At the heart of psychosocial risks: Critical organizational factors for managers’ Burnout
%+ Centre d'Analyse Théorique et de Traitement des données économiques (CATT)
%+ Université Toulouse III - Paul Sabatier (UT3)
%A PICART, Ludovic
%A Jaussaud, Jacques
%A Amann, Bruno
%< avec comité de lecture
%@ 2259-6372
%J Recherches en sciences de gestion
%I ISEOR
%V 5
%N 134
%P 259-281
%8 2019-10
%D 2019
%R 10.3917/resg.134.0259
%K Burnot
%K depression
%K stress
%K organization
%K work
%K Burnout
%K depresion
%K estres
%K organizacion
%K trabajo
%K Burnot
%K dépression
%K stress
%K organisation
%K travail
%Z Humanities and Social Sciences/Business administration
%Z Humanities and Social Sciences/Economics and FinanceJournal articles
%X Cases of burnout are on the rise. It is therefore urgent to analyze the causes of this syndrome in order to carry out effective prevention. These causes are often seen as individual. On the contrary, our research endeavors, as some researchers suggest, highlighting the factors relating to work organization. The in-depth qualitative study of a real case allows us to propose a model that highlights, in particular, the relation to the immediate supervisor.
%X Los casos de Burnout se multiplican. Por lo tanto, es urgente analizar las causas de este síndrome para llevar a cabo una prevención eficaz. Estas causas son vistas por muchos como individuales. Nuestro trabajo de investigación intenta, al contrario de otros investigadores, destacar los factores relacionados con la organización del trabajo. El estudio cualitativo detallado de un caso real permite proponer un modelo que destaca, en particular, la relación con el superior jerárquico directo.
%X Les cas de Burnout, ou épuisement professionnel, se multiplient. Il est urgent d’en analyser les causes afin de mener une prévention efficace. Ces causes sont souvent vues comme individuelles. Notre recherche s’efforce au contraire, comme le préconisent certains chercheurs, de mettre en évidence les facteurs liés à l’organisation du travail. L’étude qualitative approfondie d’un cas réel nous permet de proposer un modèle qui met en évidence notamment le rôle du N+1.
%G French
%L hal-02389812
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02389812
%~ UNIV-PAU
%~ AO-ECONOMIE
%~ CATT
%~ SHS
%~ UNIV-TLSE3

%0 Journal Article
%T Elderly Workers in Japan The need for a New Approach
%+ Centre d'Analyse Théorique et de Traitement des données économiques (CATT)
%+ Centre de recherche sur les civilisations de l'Asie Orientale (CRCAO)
%A Jaussaud, Jacques
%A MARTINE, Julien
%A DEBROUX, Philippe
%< avec comité de lecture
%@ 1614-4007
%J Transition Studies Review
%I Springer Verlag
%V 24
%N 2
%P 31-43
%8 2017
%D 2017
%R 10.14665/1614-4007-24-2-004
%K Population Ageing
%K Employment
%K Elderly workers
%K Human Resources management
%K Japan
%Z Humanities and Social Sciences/Economics and Finance
%Z Humanities and Social Sciences/Business administrationJournal articles
%X Under demographic pressures, efforts to delay labor market withdrawal have replaced early retirement policies as a management tool of labor supply in many countries. In Japan, as the country is facing a dramatic demographic transition, the employment of elderly workers up to the age of 65 has become mandatory since 2006.This article discusses the recent developments in this field in Japan and their impact on the employment of elderly workers. It focuses more specifically on how the traditional win-win way of managing elderly employment is evolving under the impact of demographics, the socio-cultural context, the regulatory environment and the consequential need for changes in the social security system. Based on a case study of five Japanese companies, and on other research findings, this paper examines empirically how organisations in that country develop human resource management practices to cope with these environmental transformations and prolong employees’ work-life.
%G English
%L hal-02389612
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02389612
%~ UNIV-PAU
%~ AO-ECONOMIE
%~ CDF
%~ CONDORCET1
%~ CRCAO
%~ CATT
%~ UNIV-PARIS7
%~ EPHE
%~ TEST-DEV
%~ CAMPUS-CONDORCET
%~ CNRS
%~ SHS
%~ USPC
%~ UNIV-PARIS
%~ UP-SOCIETES-HUMANITES
%~ CDF-PSL
%~ EPHE-PSL
%~ PSL

%0 Journal Article
%T Sustainable purchasing practices in road freight transport: the case of the French programme FRET 21
%+ Centre d'Analyse Théorique et de Traitement des données économiques (CATT)
%A Jaussaud, Jacques
%A TOURATIER, Nathalie
%< avec comité de lecture
%@ 1250-7970
%J Logistique & Management
%I Taylor & Francis
%V 25
%N 4
%P 277-288
%8 2017
%D 2017
%R 10.1080/12507970.2017.1392262
%K FRET 21
%K Sustainable purchasing practices
%K sustainable transport
%K FRET 21
%K transport durable
%K Achats de transport durable
%Z Humanities and Social Sciences/Economics and FinanceJournal articles
%X This contribution analyses the organisational and decision-making processes of purchasing sustainable transport. It provides a better understanding of the endogenous and exogenous incentives that drive companies to integrate sustainable development. The implementation of the voluntary programme FRET 21, favoured by shippers aware of environmental issues, allows us to conduct exploratory research by studying 10 cases to better understand their motivation. Our results suggest that their environmental approach derives essentially from internal dynamics. They also indicate that the supply chain department plays a leading role in making decisions regarding the purchasing of transport services.
%X Cette contribution analyse les processus organisationnels et décisionnels qui régissent l’achat de transport durable. Elle permet de mieux comprendre les facteurs de motivation conduisant les entreprises à intégrer le développement durable dans le domaine de l’achat de transport. La mise en place du programme volontaire FRET 21, destiné aux chargeurs sensibilisés par les questions d’environnement, nous permet de mener une étude exploratoire reposant sur l’étude de 10 cas d’entreprise. Nos résultats suggèrent que leur démarche environnementale provient d’une dynamique essentiellement interne. Par ailleurs, il s’avère que le service logistique joue un rôle moteur, de par son poids dans la décision d’achats des prestations de transport.
%G English
%L hal-01881903
%U https://hal-univ-pau.archives-ouvertes.fr/hal-01881903
%~ UNIV-PAU
%~ CATT
%~ AO-ECONOMIE
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T Impact of French governmental policies to reduce freight transportation CO2 emissions on small-and medium-sized companies
%+ ESC Pau
%+ Centre de recherche et d'études en gestion (CREG)
%+ Centre d'Analyse Théorique et de Traitement des données économiques (CATT)
%A Touratier-Muller, Nathalie
%A Machat, Karim
%A Jaussaud, Jacques
%< avec comité de lecture
%@ 0959-6526
%J Journal of Cleaner Production
%I Elsevier
%V 215
%P 721-729
%8 2019-04-01
%D 2019
%R 10.1016/j.jclepro.2019.01.052
%K Carbon information sharing Small- and medium-sized enterprises
%K Sustainability
%K Freight transport policies
%K Shippers and carriers' coordination
%Z Humanities and Social Sciences/Economics and Finance
%Z Humanities and Social Sciences/Business administration
%Z Quantitative Finance [q-fin]Journal articles
%X This article explores the responses of small- and medium-sized enterprises (SMEs) to mandatory and voluntary policies introduced by the French government to reduce carbon dioxide (CO2) emissions generated by freight transportation. The behaviour of small and medium-sized enterprises regarding the deployment of freight environmental programmes has seldom been studied to-date while remaining relevant in supporting and enhancing governmental policies.In-depth, semi-structured interviews conducted with 14 companies (shippers and carriers) throughout France provide clear insights and enhanced understanding of small and medium-sized enterprises' attitudes, as well as demonstrating the extent to which they are willing to pursue transportation sustainability and adopt methods to measure and reduce their transportation-related carbon dioxide emissions. The results suggest that most small and medium-sized enterprises, driven by internal initiatives and customers’ expectations, are adopting sustainable strategies, with various measures being implemented to achieve a greener supply chain. However, an accurate, standardised method for measuring transportation carbon dioxide emissions is still lacking and does not appear to be a priority requirement demanded by shippers. Carriers also appear more inclined to participate in voluntary rather than mandatory programmes, suggesting that legal requirements have little impact on the firms in this sample, as far as carbon dioxide emissions are concerned. This paper is original in its research area since the existing literature on freight transportation programmes provides findings predominantly from large companies. Our study contributes to the intersection of green freight transportation and governmental programmes application, especially from small- and medium-sized shippers and carriers points of view.
%G English
%L hal-02389572
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02389572
%~ UNIV-PAU
%~ AO-ECONOMIE
%~ CATT
%~ SHS
%~ CREG-EA4580
%~ TESTUPPA

%0 Journal Article
%T Projets Big Data des entreprises : quelles transformations organisationnelles ?
%+ Centre d'Analyse Théorique et de Traitement des données économiques (CATT)
%A BOUAFIA, Soumaya
%A Jaussaud, Jacques
%< avec comité de lecture
%@ 2495-5906
%J Revue COSSI : communication, organisation, société du savoir et information
%I Groupe de recherche international en information, communication et documentation durables
%V 4
%N 1
%8 2018
%D 2018
%K Big Data
%K Use patterns
%K Strategy
%K Organizational transformation
%K Big Data
%K modèles d’usage
%K stratégie
%K transformations organisationnelles
%Z Humanities and Social Sciences/Economics and FinanceJournal articles
%X The concept of Big Data raises many questions on its adoption. Some are technological and human, and to date largely answered. According to Walker (2015), the Big Data phenomenon is now mature and raises new questions, notably on how to leverage these voluminous data. The value derived from Big Data differs from one company to another, depending on the aim and the use that is made of it. Some organizations emphasize automation systems and algorithms to exploit their data (Davenport, 2014; Bénavent, 2014). Others add Big Data technologies to existing systems to improve the performance of their organization. There are therefore several approaches to the use of Big Data by companies (Bénavent 2014). What are the nature and extent of the organizational transformations required by each of these approaches? In this contribution, we try to answer this question, based on an original database that we have built, of 46 Big Data projects of American, European and Asian companies from different sectors of activity.
%X Le concept de Big Data suscite de nombreuses questions quant à son appropriation. Certaines relèvent des aspects technologiques et humains, et sont à ce jour en grande partie maitrisées. Selon Walker (2015) le phénomène Big Data arrive en effet aujourd’hui à maturité et pose de nouvelles questions, notamment comment valoriser ces masses de données. La valeur tirée du Big Data diffère d’une entreprise à l’autre, selon la finalité qui lui est donnée, et l’usage qui en est réalisé. Certaines organisations privilégient les systèmes d’automatisation et les algorithmes pour exploiter leurs données (Davenport, 2014 ; Bénavent, 2014). D’autres ajoutent les technologies du Big Data aux systèmes existants, pour améliorer la performance de leur organisation. Il existe donc plusieurs modèles d’usage et d’analyse des données du Big Data par les entreprises (Bénavent 2014). Quelles sont la nature et l’ampleur des transformations organisationnelles requises par chacune de ces approches ? Nous tentons dans cette contribution de répondre à partir d’une base de données originale que nous avons constituée, de 46 projets Big Data d’entreprises américaines, européennes et asiatiques issues de différents secteurs d’activité.
%G English
%L hal-01881899
%U https://hal-univ-pau.archives-ouvertes.fr/hal-01881899
%~ UNIV-PAU
%~ CATT
%~ AO-ECONOMIE
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T Clusters and Regional Management Structures of Japanese Multinational Companies in Europe
%+ Centre d'Analyse Théorique et de Traitement des données économiques (CATT)
%A Jaussaud, Jacques
%A FUTAGAMI, Shiho
%A SCHAAPER, Johannes
%A Bruno, Amann
%A MIZOGUCHI, Shuji
%A Hiroyuki, Nakamura
%Z しごと能力研究 (Shigoto Nohryoku Kenkyu) Research on Working Competency
%< avec comité de lecture
%J しごと能力研究
%I Society for Research on Working Competency, Japan
%8 2019-07
%D 2019
%Z Humanities and Social Sciences/Economics and Finance
%Z Quantitative Finance [q-fin]
%Z Humanities and Social Sciences/Business administrationJournal articles
%G English
%L hal-02454767
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02454767
%~ UNIV-PAU
%~ AO-ECONOMIE
%~ CATT
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T Control in Subsidiary Networks in Asia: Toward an Extension of the Centralisation–Formalisation–Socialisation (CFS) Model
%+ Université Toulouse III - Paul Sabatier (UT3)
%+ Centre de recherche et d'études en gestion (CREG)
%+ Université de Bordeaux (UB)
%A Amann, Bruno
%A Jaussaud, Jacques
%A SCHAAPER, Johannes
%< avec comité de lecture
%@ 1206-1697
%J Management international
%I HEC Montréal
%V 21
%N 4
%P 89-108
%8 2017
%D 2017
%Z Humanities and Social Sciences/Business administrationJournal articles
%G English
%L hal-02396924
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02396924
%~ UNIV-PAU
%~ CREG-EA4580
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T International Firm Strategies: Is Cultural Distance a Main Determinant?
%+ Université Toulouse III - Paul Sabatier (UT3)
%+ Centre de recherche et d'études en gestion (CREG)
%A ABDELLATIF, Mahamat
%A Amann, Bruno
%A Jaussaud, Jacques
%Z x-hal_journal_id=19717
%< avec comité de lecture
%@ 1614-4007
%J Transition Studies Review
%I Springer Verlag
%V 17
%N 4
%P 611-623
%8 2010
%D 2010
%R 10.1007/s11300-010-0177-8
%Z Humanities and Social Sciences/Business administrationJournal articles
%G English
%L hal-02431082
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02431082
%~ CREG-EA4580
%~ UNIV-PAU
%~ UNIV-TLSE3
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T Family versus Nonfamily Business: A Comparison of International Strategies
%+ Centre de recherche et d'études en gestion (CREG)
%+ Université Toulouse III - Paul Sabatier (UT3)
%A Jaussaud, Jacques
%A Amann, Bruno
%A Abdellatif, Mahamat
%< avec comité de lecture
%@ 1877-8585
%J Journal of Family Business Strategy
%I Elsevier
%8 2010
%D 2010
%Z Humanities and Social Sciences/Economics and Finance
%Z Humanities and Social Sciences/Business administration
%Z Humanities and Social SciencesJournal articles
%G English
%L hal-02395297
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02395297
%~ UNIV-PAU
%~ UNIV-TLSE3
%~ AO-ECONOMIE
%~ SHS
%~ CREG-EA4580
%~ TESTUPPA

%0 Journal Article
%T Corporate Social Responsibility in Japan: Family versus Non Family Business differences and determinants
%+ Centre de recherche et d'études en gestion (CREG)
%+ Université Toulouse III - Paul Sabatier (UT3)
%+ Laboratoire de Gestion et de Cognition (LGC)
%A Jaussaud, Jacques
%A Amann, Bruno
%A Martinez, Isabelle
%< avec comité de lecture
%@ 1472-4782
%J Asian Business and Management
%I Palgrave Macmillan
%8 2012
%D 2012
%Z Humanities and Social Sciences/Economics and Finance
%Z Humanities and Social Sciences
%Z Humanities and Social Sciences/Business administrationJournal articles
%G English
%L hal-02393431
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02393431
%~ UNIV-PAU
%~ UNIV-TLSE3
%~ AO-ECONOMIE
%~ SHS
%~ CREG-EA4580
%~ TESTUPPA

%0 Journal Article
%T Choix des mutualisations d’activités des Organismes d’Assurance Maladie – Application à la région Midi-Pyrénées
%+ Centre de recherche et d'études en gestion (CREG)
%A Jaussaud, Jacques
%A Cargnello-Charles, Emmanuelle
%< avec comité de lecture
%@ 1969-6574
%J Management et Avenir
%I Paris : INSEEC
%8 2011
%D 2011
%Z Humanities and Social Sciences/Economics and Finance
%Z Humanities and Social Sciences/Business administration
%Z Humanities and Social SciencesJournal articles
%G English
%L hal-02393443
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02393443
%~ UNIV-PAU
%~ AO-ECONOMIE
%~ CREG-EA4580
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T Coordination et contrôle des filiales à l’étranger : une enquête qualitative auprès de filiales françaises et japonaises en Chine
%+ Centre de recherche et d'études en gestion (CREG)
%+ Université de Bordeaux (UB)
%+ Yokohama National University
%A Jaussaud, Jacques
%A SCHAAPER, Johannes
%A MIZOGUCHI, Shuji
%A Nakamura, Hiroyuki
%< avec comité de lecture
%@ 1287-1141
%J Finance Contrôle Stratégie
%I Association FCS
%V Vol. 15
%N 1/2
%8 2012
%D 2012
%Z Humanities and Social Sciences/Economics and Finance
%Z Humanities and Social Sciences/Business administration
%Z Humanities and Social SciencesJournal articles
%G French
%L hal-02391677
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02391677
%~ UNIV-PAU
%~ AO-ECONOMIE
%~ SHS
%~ CREG-EA4580
%~ TESTUPPA

%0 Journal Article
%T Corporate Social Responsability in Japan: Family and Non-Family Business Differences and Determinants
%+ Université Toulouse III - Paul Sabatier (UT3)
%+ Centre de recherche et d'études en gestion (CREG)
%+ Departamento Ciencias del Medio Natural, ETSI
%A Amann, Bruno
%A Jaussaud, Jacques
%A Martinez, I
%Z x-hal_journal_id=10825
%< avec comité de lecture
%@ 1472-4782
%J Asian Business and Management
%I Palgrave Macmillan
%V 11
%N 3
%P 329-345
%8 2012
%D 2012
%Z Humanities and Social Sciences/Business administrationJournal articles
%G English
%L hal-02431085
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02431085
%~ CREG-EA4580
%~ UNIV-PAU
%~ UNIV-TLSE3
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T When in China... The HRM practices of Chinese and foreign-owned enterprises during a global crisis
%+ Groupe Sup de Co La Rochelle
%+ Centre de recherche et d'études en gestion (CREG)
%A Liu, Xueming
%A Jaussaud, Jacques
%< avec comité de lecture
%@ 1360-2381
%J Asia Pacific Business Review
%I Taylor & Francis (Routledge)
%V 17
%N N° 4
%P P. 473 - 491
%8 2011-10-04
%D 2011
%K China
%K Human resource management
%K labour law
%K local adaptation
%K multinational corporations
%K ongoing global crisis
%K transfert of practices.
%Z Humanities and Social Sciences/Business administrationJournal articles
%X Human resource management practices change rapidly in China, at the initiative of both multinational corporations and local enterprises. Yet multinational corporations have greater experience transferring their practices across various cultural and institutional contexts. Thus, despite some differences, local environment considerations and mimetic mechanisms lead to the convergence of practices between multinational corporations and local enterprises. The ongoing global crisis context also affects both types of firms similarly, though its effects do not reach the same level of influence as the enforcement of a new labour law, which began in January 2008.
%G English
%L hal-00828023
%U https://hal.archives-ouvertes.fr/hal-00828023
%~ SHS
%~ UNIV-PAU
%~ CREG-EA4580
%~ TESTUPPA

%0 Journal Article
%T Les relations fournisseurs distributeurs dans la grande distribution en Chine : les pratiques comparées de deux grandes enseignes : Hualian et Carrefour
%+ Groupe Sup de Co La Rochelle
%+ La Rochelle - Management, Organisation & Société (LR-MOS)
%+ Centre de recherche et d'études en gestion (CREG)
%A BAUDET, Isabelle
%A Duyck, Jean-Yves
%A Jaussaud, Jacques
%A Liu, Xueming
%Z x-hal_journal_id=143335
%< avec comité de lecture
%@ 1969-6574
%J Management et Avenir
%I Paris : INSEEC
%N 21
%P 172-191
%8 2009
%D 2009
%Z Humanities and Social Sciences/Business administrationJournal articles
%G French
%L hal-02431093
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02431093
%~ UNIV-PAU
%~ CREG-EA4580
%~ UNIV-POITIERS
%~ CEREGE-LAB
%~ UNIV-ROCHELLE
%~ SHS

%0 Journal Article
%T Valorisation boursière comparée des entreprises familiales et non familiales au Japon
%+ CEntre de REcherche en GEstion - EA 1722 (CEREGE)
%+ Centre de Recherche sur l'Intégration Economique et Financière (CRIEF)
%+ Université Toulouse III - Paul Sabatier (UT3)
%+ Centre de recherche et d'études en gestion (CREG)
%A Nivoix, Sophie
%A Bruno, Amann
%A Jaussaud, Jacques
%Z x-hal_journal_id=42462
%< avec comité de lecture
%@ 1340-3656
%J Ebisu - Études Japonaises
%I Maison franco-japonaise
%N 50
%P 7-25
%8 2014
%D 2014
%Z Quantitative Finance [q-fin]/General Finance [q-fin.GN]Journal articles
%G French
%L hal-01069648
%U https://hal.archives-ouvertes.fr/hal-01069648
%~ UNIV-POITIERS
%~ AO-ECONOMIE
%~ CRIEF
%~ MSHS-POITIERS
%~ SHS
%~ UNIV-ROCHELLE
%~ CEREGE-LAB
%~ UNIV-PAU
%~ UNIV-TLSE3
%~ CREG-EA4580

%0 Journal Article
%T Business process outsourcing : the foundation for future virtual organizations in India
%+ Département Management, Marketing et Stratégie (MMS)
%+ Centre de recherche et d'études en gestion (CREG)
%A Gupta, Bhumika
%A Dzever, Sam
%A Jaussaud, Jacques
%< avec comité de lecture
%Z 17053
%@ 0323-9004
%J Tsenov economic journal
%N 113
%P 7 - 38
%8 2010
%D 2010
%K India
%K Business process outsourcing
%K Virtual companies
%K Core competencies
%K Efficiency
%K BPO industry
%Z Humanities and Social Sciences/Business administrationJournal articles
%X The present research explores the possibilities and likelihood that business process outsourcing (BPO) may soon emerge as the foundation for entirely virtual organizations of the future in India. The paper suggests totally virtual companies of the future in India that adopt a new business model whose functions and activities of management are spread globally, communicate, share and disseminate information required to operate successfully on a regular basis in order to reduce the rate of employee attrition which is prevalent in the industry in India. Although BPO is a rapidly growing phenomenon in the Indian market it has also demonstrated a very high rate of employee attrition (for the reasons indicated in the paper). And it is very clear that this high rate of employee attrition needs to be reduced in order to minimize the overall cost to the organization and indeed society at large. Our research has attempted to appraise various aspects of BPO activity in order to propose solutions in reducing the rate of employee attrition. This is achieved by using different parameters of Traditional and Virtual Project Teams. This research explores the contention that Motivation plays an important role in retaining human resources (HR) in any organization and particularly with regard to the BPO industry in India. Literature review of key theories of Motivation reveals that motivation is closely related to team performance. Drawing upon this review, it is theoretically argued that the commonalities pertaining to motivation and team performance may be categorized into three dimensions, namely: Nature of Work, Rewards, and Communication. Variables related to the indicated dimensions are used as a scale to compare Traditional and Virtual project teams in BPO firms in India using Principle Component Analysis.
%G English
%L hal-02447044
%U https://hal.archives-ouvertes.fr/hal-02447044
%~ INSTITUT-TELECOM
%~ TELECOM-MANAGEMENT
%~ IMT-BS
%~ UNIV-PAU
%~ INSTITUTS-TELECOM
%~ SHS
%~ CREG-EA4580

%0 Journal Article
%T Les tensions global-local : l'organisation et la coordination des activités internationales
%+ Centre de recherche et d'études en gestion (CREG)
%+ Centre de Recherche Magellan
%A Jaussaud, Jacques
%A Mayrhofer, Ulrike
%< avec comité de lecture
%@ 1206-1697
%J Management international
%I HEC Montréal
%V 18
%N 1
%P 18-25
%8 2013
%D 2013
%K Globalization
%K Foreign Direct Investments (FDI)
%K global factory
%K organizational structures
%K control.
%K Globalisation
%K investissement direct à l'étranger (IDE)
%K local
%K usine mondiale
%K structures organisationnelles
%K expatriation
%K coordination
%K contrôle.
%Z Humanities and Social Sciences/Business administrationJournal articles
%X The diversification of direct investment flows and the rebalancing in favor of emerging markets increase the heterogeneity of environments faced by companies at the international scale. Thus comes back with strength the question of the need to take account of specific local conditions, even when the company seeks to promote a global approach of markets. Structures, organization, coordination and control mechanisms, as the article investigates, are considerably affected.
%X La diversification des flux d'investissements directs et leur rééquilibrage en faveur des pays émergents augmentent l'hétérogénéité des environnements auxquels sont confrontées les entreprises à l'international. Se trouve ainsi renouvelée la question de la nécessaire prise en compte des particularités locales, y compris lorsque l'entreprise s'efforce de promouvoir une approche globale de ses marchés. Les structures, l'organisation, les mécanismes de coordination et de contrôle, comme le souligne cet article, en sont considérablement affectés.
%G French
%L halshs-00910359
%U https://halshs.archives-ouvertes.fr/halshs-00910359
%~ SHS
%~ MAGELLAN
%~ UNIV-PAU
%~ UNIV-LYON3
%~ CREG-EA4580
%~ UDL
%~ TESTUPPA

%0 Journal Article
%T When in China … The HRM practices of Chinese and foreign-owned enterprises during a global crisis
%+ Centre de recherche et d'études en gestion (CREG)
%+ Ecole Supérieure de Commerce de la Rochelle (Sup de Co La Rochelle)
%A Jaussaud, Jacques
%A Liu, Marc Xueming
%< avec comité de lecture
%@ 1360-2381
%J Asia Pacific Business Review
%I Taylor & Francis (Routledge)
%8 2011
%D 2011
%Z Humanities and Social Sciences/Economics and Finance
%Z Humanities and Social Sciences/Business administration
%Z Humanities and Social SciencesJournal articles
%G English
%L hal-02394952
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02394952
%~ UNIV-PAU
%~ AO-ECONOMIE
%~ SHS
%~ CREG-EA4580
%~ TESTUPPA

%0 Journal Article
%T Expatriation, Careers and Resource Based View ; a Critical Study based on Power
%+ Centre de recherche et d'études en gestion (CREG)
%A Jaussaud, Jacques
%A Freitas Gouveia de Vasconcelos, Isabella
%A Hidalgo Nunes, Leni
%Z January-June
%< avec comité de lecture
%@ 2238-8893
%J Revista AOS - Amazônia, Organizações e Sustentabilidade
%I Universidade da Amazônia, Mestrado em Administração
%V 1
%N 1
%P 61-78
%8 2012
%D 2012
%Z Humanities and Social Sciences/Economics and Finance
%Z Humanities and Social Sciences/Business administration
%Z Humanities and Social Sciences
%Z Quantitative Finance [q-fin]Journal articles
%G English
%L hal-02394938
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02394938
%~ UNIV-PAU
%~ AO-ECONOMIE
%~ SHS
%~ CREG-EA4580
%~ TESTUPPA

%0 Journal Article
%T Family and Non-Family Business Resilience in an Economic Downturn
%+ Université Toulouse III - Paul Sabatier (UT3)
%+ Centre de recherche et d'études en gestion (CREG)
%A Amann, Bruno
%A Jaussaud, Jacques
%< avec comité de lecture
%@ 1360-2381
%J Asia Pacific Business Review
%I Taylor & Francis (Routledge)
%V 18
%N 2
%P 203-223
%8 2012
%D 2012
%R 10.1080/13602381.2010.537057
%Z Humanities and Social Sciences/Business administrationJournal articles
%G English
%L hal-02431086
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02431086
%~ UNIV-PAU
%~ CREG-EA4580
%~ UNIV-TLSE3
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T Clusters and Regional Management Structures by Western MNCs in Asia: Overcoming the Distance Challenge
%+ Université Toulouse III - Paul Sabatier (UT3)
%+ Centre de recherche et d'études en gestion (CREG)
%+ Université de Bordeaux (UB)
%A Amann, Bruno
%A Jaussaud, Jacques
%A SCHAAPER, Johannes
%Z x-hal_journal_id=6929
%< avec comité de lecture
%@ 0025-181X
%J Management International Review
%I Management International Review
%V 54
%N 6
%P 879-906
%8 2014
%D 2014
%R 10.1007/s11575-014-0222-7
%Z Humanities and Social Sciences/Business administrationJournal articles
%G English
%L hal-02431084
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02431084
%~ CREG-EA4580
%~ UNIV-PAU
%~ UNIV-TLSE3
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T Coordination et contrôle des filiales à l'étranger; une enquête qualitative auprès de filiales françaises et japonaises en Chine
%+ Centre de recherche et d'études en gestion (CREG)
%+ Yokohama National University
%+ Université de Bordeaux (UB)
%A Jaussaud, Jacques
%A MIZOGUCHI, Shuji
%A Nakamura, Hiroyuki
%A SCHAAPER, Johannes
%A YAMASHITA, S
%Z x-hal_journal_id=20680
%< avec comité de lecture
%@ 1287-1141
%J Finance Contrôle Stratégie
%I Association FCS
%V 15
%N 1/2
%P 2-21
%8 2012
%D 2012
%Z Humanities and Social Sciences/Business administrationJournal articles
%G French
%L hal-02431143
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02431143
%~ CREG-EA4580
%~ UNIV-PAU
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T Human Resource Management in Asian Subsidiaries: Comparison of French and Japanese MNCs
%+ Centre de recherche et d'études en gestion (CREG)
%+ Université de Bordeaux (UB)
%+ Université Toulouse III - Paul Sabatier (UT3)
%+ Yokohama National University
%A Jaussaud, Jacques
%A SCHAAPER, Johannes
%A Amann, Bruno
%A Hiroyuki, Nakamura
%A MIZOGUCHI, Shuji
%< avec comité de lecture
%@ 1465-6612
%J International Journal of Human Resources Development and Management
%I Inderscience
%8 2013
%D 2013
%Z Humanities and Social Sciences/Economics and Finance
%Z Humanities and Social Sciences/Business administration
%Z Humanities and Social SciencesJournal articles
%G English
%L hal-02394898
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02394898
%~ UNIV-PAU
%~ UNIV-TLSE3
%~ AO-ECONOMIE
%~ SHS
%~ CREG-EA4580
%~ TESTUPPA

%0 Journal Article
%T Senkaku, Diaoyu… per eissubliar Fukushima ?
%+ Newcastle University [Newcastle]
%+ Centre de recherche et d'études en gestion (CREG)
%A DRIFTE, Reinhard
%A Jaussaud, Jacques
%Z x-hal_journal_id=148456
%< avec comité de lecture
%@ 1265-5619
%J La Setmana : jornau occitan d'informacions
%I Vistedit
%N 919
%8 2013
%D 2013
%Z Humanities and Social Sciences/Business administration
%Z Humanities and Social Sciences
%Z Humanities and Social Sciences/Political scienceJournal articles
%G Occitan
%L hal-02431179
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02431179
%~ CREG-EA4580
%~ UNIV-PAU
%~ SHS
%~ AO-SCIENCEPOLITIQUE
%~ TESTUPPA

%0 Journal Article
%T Les politiques de GRH internationale dans les firmes multinationales : Entre expatriation et localisation des postes
%+ Université de Bordeaux (UB)
%+ Centre de recherche et d'études en gestion (CREG)
%A SCHAAPER, Johannes
%A Jaussaud, Jacques
%Z x-hal_journal_id=107759
%< avec comité de lecture
%@ 2170-1687
%J Business Management Review
%I Alger : MDI Business School
%V 2
%N 2
%P 20-41
%8 2012
%D 2012
%Z Humanities and Social Sciences/Business administrationJournal articles
%G French
%L hal-02432217
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02432217
%~ UNIV-PAU
%~ CREG-EA4580
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T Stratégie internationale des entreprises japonaises : étude de l'incidence potentielle du caractère familial
%+ Université Toulouse III - Paul Sabatier (UT3)
%+ Centre de recherche et d'études en gestion (CREG)
%A ABDELLATIF, Mahamat
%A Amann, Bruno
%A Jaussaud, Jacques
%Z x-hal_journal_id=42462
%< avec comité de lecture
%@ 1340-3656
%J Ebisu - Études Japonaises
%I Maison franco-japonaise
%N 43
%8 2010
%D 2010
%Z Humanities and Social Sciences/Business administrationJournal articles
%G French
%L hal-02431083
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02431083
%~ CREG-EA4580
%~ UNIV-PAU
%~ UNIV-TLSE3
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T Quelles configurations pour les Business Model du Big Data ?
%+ Centre d'Analyse Théorique et de Traitement des données économiques (CATT)
%A BOUAFIA, Soumaya
%A Jaussaud, Jacques
%< avec comité de lecture
%@ 2555-7017
%J Management & Data Science
%I Management & Data Science
%V 3
%N 1
%8 2019-01
%D 2019
%Z Humanities and Social Sciences/Economics and Finance
%Z Humanities and Social Sciences/Business administrationJournal articles
%X L’intérêt porté au Big Data est aujourd’hui considérable. La littérature, académique comme professionnelle, annonce un potentiel important et de nouvelles sources de profits. Paradoxalement, peu de travaux ont été consacrés à l’aspect business de ces données massives. Cette contribution s’efforce d’explorer les business model du Big Data. Nous avons pour cela réalisé un travail empirique basé sur des études de cas, nous conduisant à l’identification de cinq catégories de modèles d’affaires liées en premier lieu à la finalité d’usage du projet Big Data.
%G French
%L hal-02140255
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02140255
%~ CATT
%~ UNIV-PAU
%~ AO-ECONOMIE
%~ SHS
%~ MDS

%0 Journal Article
%T The Great East Japan Earthquake and Stock Prices
%+ Centre de recherche et d'études en gestion (CREG)
%+ Axe 2 : « Marchés, Cultures de consommation, Autonomie et Migrations » (MSHS Poitiers)
%+ Centre de Recherche sur l'Intégration Economique et Financière (CRIEF)
%+ CEntre de REcherche en GEstion - EA 1722 (CEREGE)
%+ CPER "INnovation Sociale, Economique et Culturelle dans des Territoires en mutation" (MSHS Poitiers) (CPER INSECT)
%+ Centre d'Analyse Théorique et de Traitement des données économiques (CATT)
%A Jaussaud, Jacques
%A Nivoix, Sophie
%A Rey, Serge
%< avec comité de lecture
%@ 1545-2921
%J Economics Bulletin
%I Economics Bulletin
%V 35
%N 2
%P 1237-1261
%8 2015
%D 2015
%K electric utility companies
%K stock market
%K Japan
%K risk
%K volatility
%K earthquake
%Z Humanities and Social Sciences/Economics and FinanceJournal articles
%X The Great East Japan Earthquake of March 11, 2011, which led to a massive tsunami and the nuclear accident at Fukushima, moved Japanese authorities to close most of the country's nuclear reactors for inspection (only 2 of 54 total currently are working), as well as to reassess its national energy policy. This article investigates the volatility of stock prices before and after the disaster. The evolution of stock prices of electric utility companies differs greatly, compared with those of firms in other industries.
%G English
%L hal-01885285
%U https://hal-univ-pau.archives-ouvertes.fr/hal-01885285
%~ UNIV-PAU
%~ CATT
%~ CNRS
%~ UNIV-POITIERS
%~ UNIV-ROCHELLE
%~ AO-ECONOMIE
%~ CRIEF
%~ MSHS-POITIERS-AXE2
%~ SHS
%~ MSHS-POITIERS
%~ CEREGE-LAB
%~ CREG-EA4580
%~ MSHS-POITIERS-CPER-INSECT

%0 Journal Article
%T Prévenir les risques psychosociaux : envisager le burnout comme un processus organisationnel
%+ Centre d'Analyse Théorique et de Traitement des données économiques (CATT)
%A Jaussaud, Jacques
%A PICART, Ludovic
%< avec comité de lecture
%@ 2259-2490
%J RIMHE : Revue Interdisciplinaire Management, Homme(s) & Entreprise
%I Association pour la recherche interdisciplinaire sur le management des entreprises
%V 4
%N 33
%P 34-52
%8 2018
%D 2018
%Z Humanities and Social Sciences/Economics and FinanceJournal articles
%G French
%L hal-02140256
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02140256
%~ CATT
%~ UNIV-PAU
%~ AO-ECONOMIE
%~ SHS

%0 Journal Article
%T Prolonging working life in Japan: Issues and practices for elderly employment in an aging society
%+ Centre de recherche sur les civilisations de l'Asie Orientale (CRCAO)
%+ Centre d'Analyse Théorique et de Traitement des données économiques (CATT)
%A MARTINE, Julien
%A Jaussaud, Jacques
%< avec comité de lecture
%@ 1869-2729
%J Contemporary Japan
%I De Gruyter
%V 30
%N 2
%P 227-274
%8 2018
%D 2018
%R 10.1080/18692729.2018.1504530
%K Elderly workers
%K elderly employment
%K aging workforce
%K human resources management practices
%K Japanese companies
%Z Humanities and Social Sciences/Economics and FinanceJournal articles
%X As a result of the rapidly aging society, Japan’s public policies and related academic research have sought to address issues related to the employment of older workers. However, these efforts do not sufficiently consider how human resources management practices might facilitate effective policies. In response, this study investigates both the challenges and the solutions that employers have adopted to deal with an aging workforce. In particular, the authors argue that practices developed mainly by smaller enterprises might offer insights for larger companies that attempt to support the employment of elderly workers. An empirical analysis of the human resources management practices employed by 27 Japanese companies provides novel insights into how companies can ensure the prolonged employment of older workers while also motivating their work performance.
%G English
%L hal-02140252
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02140252
%~ UNIV-PAU
%~ CATT
%~ AO-ECONOMIE
%~ CDF
%~ CNRS
%~ SHS
%~ CRCAO
%~ UNIV-PARIS7
%~ EPHE
%~ TEST-DEV
%~ USPC
%~ CONDORCET1
%~ CAMPUS-CONDORCET
%~ UNIV-PARIS
%~ UP-SOCIETES-HUMANITES
%~ CDF-PSL
%~ EPHE-PSL
%~ PSL
%~ TESTUPPA2
%~ UPPA-OA

