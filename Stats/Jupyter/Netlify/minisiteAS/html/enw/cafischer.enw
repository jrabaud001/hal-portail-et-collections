%0 Journal Article
%T <i>Vénus à la fourrure</i> ou la remédialisation d’une ambiguïté. Léopold von Sacher-Masoch (1870) et David Ives (2010)
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Fischer, Caroline
%< avec comité de lecture
%J Bibliothèque comparatiste de la SFLGC
%I Société française de littérature générale et comparée
%8 2019
%D 2019
%Z Humanities and Social Sciences/LiteratureJournal articles
%X The morality of Leopold von Sacher-Masoch’s (1836-1895) <i>Venus in Furs</i>, published in 1870, is just as ambiguous as the one of Laclos’s <i>Dangerous Liaisons</i> – on the one hand glorification or condemnation of libertinage, on the other hand adoration or execration of the beautiful cruel? The question has not yet been resolved. It, therefore, seems productive to analyze how this crucial aspect of the story is remediated by David Ives in his piece <i>Venus in Furs</i>, created on Broadway in 2010, by genre-specific methods, such as theater in the theater, by the duplication of the characters, but also by expressing in the dialogues a certain number of criticisms made about the text of Sacher-Masoch, expatiating on its ambiguous character. We are also interested in the way in which this transposition of the literary genre allows the author to deal with the question of the relationship between the genres of these two characters, between man and woman, between the dominator and the dominated.
%X La morale de <i>Vénus à la fourrure</i> de Leopold von Sacher-Masoch (1836-1895), publié en 1870, est tout aussi ambiguë que celle des <i>Liaisons dangereuses</i> – glorification ou condamnation du libertinage pour l’un, adoration ou exécration de la belle cruelle chez l’autre ? La question n’a pas pu être tranchée à ce jour. Aussi nous semble-t-il pertinent d’analyser comment cet aspect crucial de la nouvelle est remédialisée par David Ives dans sa pièce <i>Venus in Furs</i>, créée à Broadway en 2010, par des procédés propres au genre, comme le théâtre dans le théâtre, par le dédoublement des personnages, mais surtout en explicitant dans les dialogues un certain nombre de critiques émises à propos du texte de Sacher-Masoch, dont le caractère ambigu. Nous nous intéressons aussi à la manière dont cette transposition du genre littéraire permet à l’auteur de traiter la question des rapports entre les genres de ces deux personnages, entre homme et femme, entre dominant(e) et dominé(e).
%G French
%L halshs-02435475
%U https://halshs.archives-ouvertes.fr/halshs-02435475
%~ SHS
%~ UNIV-PAU
%~ ALTER
%~ TESTUPPA

%0 Journal Article
%T La Marche de Radetzky et Le Guépard : romans de famille
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Fischer, Caroline
%< avec comité de lecture
%@ 1634-4073
%J Méthode ! : revue de littératures française et comparée
%I Éditions de Vallongues
%S Agrégations de lettres 2015
%N 24
%P 253-261
%8 2014
%D 2014
%Z Humanities and Social Sciences/LiteratureJournal articles
%G French
%L hal-02171628
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02171628
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T Barbares attendris vs. citoyens enragés. Représentation, motivation et justification de la violence chez Shakespeare, Botho Strauss et Corneille
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Fischer, Caroline
%< avec comité de lecture
%@ 1634-4073
%J Méthode ! : revue de littératures française et comparée
%I Éditions de Vallongues
%S Agrégations de lettres 2011
%N 18
%P 345-353
%8 2010
%D 2010
%Z Humanities and Social Sciences/LiteratureJournal articles
%G French
%L hal-02171744
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02171744
%~ ALTER
%~ UNIV-PAU
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T Traduction et réception de la poésie française renaissante en Allemagne
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Fischer, Caroline
%< avec comité de lecture
%@ 0170-3803
%J Lendemains - Études comparées sur la France
%I Francke/Narr
%N 158/159
%P 154-167
%8 2015
%D 2015
%Z Humanities and Social Sciences/LiteratureJournal articles
%G French
%L hal-02170592
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02170592
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T Avant-propos
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Fischer, Caroline
%< avec comité de lecture
%@ 0170-3803
%J Lendemains - Études comparées sur la France
%I Francke/Narr
%N 158/159
%P 141-142
%8 2015
%D 2015
%Z Humanities and Social Sciences/LiteratureJournal articles
%G French
%L hal-02170591
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02170591
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T Stimmung als ästhetische Kategorie ?
%+ Arts / Langages : Transitions et Relations (ALTER)
%A Fischer, Caroline
%A Hölter, Achim
%< avec comité de lecture
%@ 1432-5306
%J Comparative Arts : neue Ansätze zu einer universellen Ästhetik
%I Synchron
%P 349-356
%8 2011
%D 2011
%Z Humanities and Social Sciences/LiteratureJournal articles
%G German
%L hal-02171745
%U https://hal-univ-pau.archives-ouvertes.fr/hal-02171745
%~ UNIV-PAU
%~ ALTER
%~ SHS
%~ TESTUPPA

%0 Journal Article
%T Controlled ingestion of kaolinite (5%) modulates enteric nitrergic innervation in rats
%+ Institut Pluridisciplinaire Hubert Curien (IPHC)
%+ Département Ecologie, Physiologie et Ethologie (DEPE-IPHC)
%+ Département Sciences Analytiques et Interactions Ioniques et Biomoléculaires (DSA-IPHC)
%A Voinot, Florian
%A Fischer, Caroline
%A Schmitt, Camille
%A Ehret-Sabatier, Laurence
%A Angel, Fabielle
%< avec comité de lecture
%@ 0767-3981
%J Fundamental and Clinical Pharmacology
%I Wiley
%V 28
%N 4
%P 405-413
%8 2014
%D 2014
%R 10.1111/fcp.12040
%Z Environmental Sciences
%Z Life Sciences [q-bio]Journal articles
%X We have previously shown that kaolinite slowed down gastric emptying and intesti- nal transit and induced changes in enteric mechanical activities. As gastric emptying and intestinal transit have been shown to be regulated by nitric oxide (NO), the effect of an imposed ingestion of kaolinite on enteric nitrergic innervation was determined. Kaolinite has also been shown to increase plasmatic levels of leptin. Therefore, the responses of enteric neurons in the presence of leptin after kaolinite ingestion were determined, and a possible role of nitrergic neurons was evaluated in rats using organ bath technique. Our results showed that kaolinite modulates activities of enteric nerves at 14 days of ingestion. Exogenous L-NNA produced a decrease in nerve stimulation (NS)-induced relaxation in both jejunum and colon of control groups. At 14 days of kaolinite ingestion, this effect of L-NNA was significantly reduced only in the jejunum. Although L-NNA did not affect NS-induced contraction in jejunum and colon of control animals, it increased the amplitude of the NS- induced contraction in the colon of rats at 14 days of kaolinite ingestion. Leptin inhibitory effects on ENS in the jejunum were also altered at 14 days of ingestion. These differences were masked in the presence of L-NNA. Our data give evidence that changes in mechanical activities induced by kaolinite might be due to alterations in inhibitory (nitrergic and/or other) innervation at 14 days of kaolinite ingestion and to modifications of leptin effects on the responses to intramural nerve stimulation.
%G English
%L hal-00835753
%U https://hal.archives-ouvertes.fr/hal-00835753
%~ DRS-IPHC
%~ IPHC
%~ CNRS
%~ SDE
%~ GIP-BE
%~ DSA-IPHC
%~ DEPE-IPHC
%~ UNIV-STRASBG
%~ SITE-ALSACE
%~ IN2P3

%0 Journal Article
%T Effects of controlled ingestion of kaolinite (5%) on food intake, gut morphology and in vitro motility in rats.
%+ Département Ecologie, Physiologie et Ethologie (DEPE-IPHC)
%+ Département Sciences Analytiques et Interactions Ioniques et Biomoléculaires (DSA-IPHC)
%+ Lehrstuhl fur Humanbiologie
%A Voinot, Florian
%A Fischer, Caroline
%A Bœuf, Amandine
%A Schmidt, Camille
%A Delval-Dubois, Véronique
%A Reichardt, François
%A Liewig, Nicole
%A Chaumande, Bertrand
%A Ehret-Sabatier, Laurence
%A Lignot, Jean-Hervé
%A Angel, Fabielle
%< avec comité de lecture
%@ 0767-3981
%J Fundamental and Clinical Pharmacology
%I Wiley
%V 26
%N 5
%P 565-576
%8 2012
%D 2012
%R 10.1111/j.1472-8206.2011.00978.x
%M 21801202
%K adipokines
%K cholinergic nerves
%K clay
%K colon
%K intestine
%K smooth muscle motility
%Z Environmental SciencesJournal articles
%X Geophagia is found in various animal species and in humans. We have previously shown that spontaneously ingested kaolinite interacts with the intestinal mucosa modifies nutrient absorption and slows down gastric emptying and intestinal transit in rats in vivo. However, the precise mechanisms involved are not elucidated. The aim of this work was to investigate the effects of controlled kaolinite ingestion on food intake, gut morphology and in vitro motility in rats. Male Wistar rats were fed with 5% kaolinite in standard food pellets during 7, 14 and 28 days. Body mass and food consumption were measured daily. Intestinal morphological and proteomic analyses were conducted. The length of mucosal lacteals was evaluated. Plasmatic levels of leptin and adiponectin were determined. Finally, organ bath studies were conducted to evaluate smooth muscle contractility. Food consumption was significantly increased during the first two weeks of kaolinite ingestion without any mass gain compared to controls. Kaolinite induced weak variations in proteins that are involved in various biological processes. Compared to control animals, the length of intestinal lacteals was significantly reduced in kaolinite group whatever the duration of the experiment. Leptin and adiponectin plasmatic levels were significantly increased after 14 days of kaolinite consumption. Changes in spontaneous motility and responses to electrical nerve stimulation of the jejunum and proximal colon were observed at day 14. Altogether, the present data give evidence for a modulation by kaolinite-controlled ingestion on satiety and anorexigenic signals as well as on intestinal and colonic motility.
%G English
%L hal-00614434
%U https://hal.archives-ouvertes.fr/hal-00614434
%~ SDE
%~ DRS-IPHC
%~ CNRS
%~ GIP-BE
%~ IPHC
%~ DSA-IPHC
%~ DEPE-IPHC
%~ UNIV-STRASBG
%~ SITE-ALSACE
%~ IN2P3

