%0 Journal Article
%T Feasibility Limits and Performance of an Absorption Cooling Machine Using Light Alkane Mixtures
%+ École Nationale d’Ingénieurs de Monastir (ENIM)
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Dardour, H.
%A Mazouz, S.
%A Reneaume, Jean-Michel
%A Cézac, Pierre
%A Bellagi, A.
%Z ACL
%< avec comité de lecture
%@ 1359-4311
%J Applied Thermal Engineering
%I Elsevier
%V 78
%P 24--29
%8 2015
%D 2015
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%G English
%L hal-02129482
%U https://hal.archives-ouvertes.fr/hal-02129482
%~ UNIV-PAU
%~ LATEP

%0 Journal Article
%T Nouveaux fluides dans les machines à absorption : cas des hydrocarbures
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Scolan, S.
%A Sochard, Sabine
%A Serra, Sylvain
%A Reneaume, Jean-Michel
%Z ACL
%< avec comité de lecture
%J Revue Générale du Froid
%V 1161
%N Janvier/Février
%P 25--31
%8 2017
%D 2017
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%G French
%L hal-02129461
%U https://hal.archives-ouvertes.fr/hal-02129461
%~ UNIV-PAU
%~ LATEP
%~ TESTUPPA

%0 Journal Article
%T Dynamic simulation and optimal operation of district cooling networks via 2D orthogonal collocation
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Nova-Rincon, Arley
%A Sochard, Sabine
%A Serra, Sylvain
%A Reneaume, Jean-Michel
%< avec comité de lecture
%@ 0196-8904
%J Energy Conversion and Management
%I Elsevier
%V 207
%P 112505
%8 2020-03
%D 2020
%R 10.1016/j.enconman.2020.112505
%Z Computer Science [cs]/Modeling and Simulation
%Z Mathematics [math]/Optimization and Control [math.OC]
%Z Engineering Sciences [physics]Journal articles
%G English
%L hal-02480878
%U https://hal.archives-ouvertes.fr/hal-02480878
%~ INSMI
%~ UNIV-PAU
%~ TDS-MACS
%~ LATEP
%~ TESTUPPA

%0 Journal Article
%T Simultaneous optimization of the district heating network topology and the Organic Rankine Cycle sizing of a geothermal plant
%+ Thermophysique et Ecoulements
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Marty, Fabien
%A Serra, Sylvain
%A Sochard, Sabine
%A Reneaume, Jean-Michel
%< avec comité de lecture
%@ 0360-5442
%J Energy
%I Elsevier
%V 159
%P 1060-1074
%8 2018-09
%D 2018
%R 10.1016/j.energy.2018.05.110
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Thermics [physics.class-ph]
%Z Computer Science [cs]/Modeling and Simulation
%Z Mathematics [math]/Optimization and Control [math.OC]
%Z Engineering Sciences [physics]/Mechanics [physics.med-ph]/Fluids mechanics [physics.class-ph]Journal articles
%X Geothermal power plant Combined heat and power (CHP) District heating network (DHN) Organic Rankine cycle (ORC) Mixed integer non-linear programming (MINLP) a b s t r a c t This contribution presents the optimization of parallel distribution between electricity and heat production for a geothermal plant. The geothermal fluid is split into two streams, one used for an Organic Rankine Cycle (ORC) system, and the other for a District Heating Network (DHN). The superstructure to be used for the optimization problem includes the ORC components, one of which is an optional internal heat exchanger which allows exchange between the outlet streams of the turbine and the pump. Each of the components' characteristic dimensions (used in the installation cost) is an optimization variable. The operating cost of the ORC is proportional to the installation cost. The superstructure also includes the DHN topology constituted by a definite consumer and optional consumers. A Mixed Integer Non-Linear Programming (MINLP) optimization problem is formulated and solved using the GAMS software. The strategy used to overcome the critical point of the initialization of the MINLP problem is presented. It consists in dividing the general problem into sub-problems which are solved successively. Three different academic study cases are compared to a reference case. The results validate the stability and the robustness of this optimization tool. A sensitivity analysis is performed in geothermal source conditions. All these results highlight the relevance of the simultaneous approach.
%G English
%L hal-02102241
%U https://hal.archives-ouvertes.fr/hal-02102241
%~ CNRS
%~ UNIV-PAU
%~ INSMI
%~ UNIV-PERP
%~ PROMES
%~ LATEP
%~ TDS-MACS
%~ TESTUPPA

%0 Journal Article
%T Modelling a Solar Absorption Chiller Using Positive Flash to Estimate the Physical State of Streams and Theoretical Plate Concept for the Generator
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Sochard, Sabine
%A Castillo Garcia, L.
%A Serra, Sylvain
%A Vitupier, Y.
%A Reneaume, Jean-Michel
%Z ACL
%< avec comité de lecture
%J Renewable Energy
%V 109
%P 121-134
%8 2017
%D 2017
%R 10.1016/j.renene.2017.03.015
%K scopusₗatep
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%X In this paper a general model for the steady state simulation of a solar absorption chiller is proposed. The novelty of this model is to calculate the physical state of all the streams rather than fix them (especially at the outlet of the condenser and evaporator). The thermodynamic properties of the mixture can be calculated by suitable predictive thermodynamic models, hence the working pair can be changed easily. Using this thermodynamic model, a general Positive Flash model is able to describe all the possible states (sub-cooled, super-heated, biphasic) of the various streams which are all considered as multicomponent mixtures. In the positive flash the same set of governing equations is valid for all phase regions. Another originality of the present study is that the generator is modelled as a distillation column, using the theoretical plate concept: MESH equations (Mass balance, Equilibrium, Summation, Heat balance) were written for each stage. Despite the modular structure of the software, a global solution strategy was implemented, using a Newton-Raphson method. This model is successfully compared to an example in the literature which deals with a GAX (Generator-Absorber heat eXchange) configuration absorption chiller using ammonia/water as the working pair. \textcopyright 2017 Elsevier Ltd
%G English
%L hal-02129460
%U https://hal.archives-ouvertes.fr/hal-02129460
%~ UNIV-PAU
%~ LATEP
%~ TESTUPPA

%0 Journal Article
%T Catalytic distillation modeling
%+ Department of Analytical Chemistry
%A Druart, Florence
%A Rouzineau, David
%A Reneaume, Jean-Michel
%A Meyer, Michel
%< avec comité de lecture
%Z LEPMI-00536
%@ 1570-7946
%J Computer Aided Chemical Engineering
%I Elsevier
%V 10
%P pp. 181-186
%8 2002
%D 2002
%R 10.1016/S1570-7946(02)80058-X
%Z Chemical Sciences/Material chemistryJournal articles
%X A non-equilibrium (NEQ) model including heterogeneous reaction is developed. Mass transfer rates are described using Stefan-Maxwell equations. In a first section, model equations are described. Then the resolution strategy is presented in more details. In the third section, an illustrative example is considered: acetic acid esterification. The system is solved and results are presented: molar liquid fraction profiles in the column. Those results are compared to those obtained with a more classical model (equilibrium model): fraction profiles are quite different. Reaction rates are influenced by this variation.
%G English
%L hal-00418081
%U https://hal.archives-ouvertes.fr/hal-00418081

%0 Journal Article
%T Optimization of Plate Fin Heat Exchangers: A Continuous Formulation
%+ Laboratoire de Génie Electrique (LGE)
%+ Laboratoire de génie chimique [ancien site de Basso-Cambo] (LGC)
%+ Nordon Cryogénie
%A Reneaume, Jean-Michel
%A Pingaud, Hervé
%A Niclout, N.
%< avec comité de lecture
%@ 0263-8762
%J Chemical Engineering Research and Design
%I Elsevier
%V 78
%N 6
%P 849 -859
%8 2000-09
%D 2000
%R 10.1205/026387600528058
%K plate fin heat exchanger
%K non linear programming
%K optimization
%Z Engineering Sciences [physics]Journal articles
%X An industrial application of mathematical programming is presented. The objective is to propose a tool for computer aided design of compact plate fin heat exchangers. A proprietary sizing procedure, COLETH, is used to initialize the optimization problem and to evaluate the objective function and the constraints during the optimization problem resolution. The main features of COLETH are presented. The partial infeasible path strategy is discussed: at each optimization step, duty requirements are satisfied, but pressure drop constraints are not. Intermediate information can be used and the cpu time is quite low. A non linear programming optimization problem is formulated. Then the problem is solved using a reduced Hessien Successive Quadratic Programming algorithm, which can take advantage of the system sparcity. Other features of this algorithm are discussed. Three test problems are presented and the program abilities are illustrated.
%G English
%L hal-01803382
%U https://hal.archives-ouvertes.fr/hal-01803382
%~ CNRS
%~ UNIV-TLSE3
%~ UNIV-PAU
%~ LGC
%~ LORIA2

%0 Journal Article
%T Dynamic optimization of the operation of a solar thermal plant
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%+ Physiopathologie des Osteopathies Fragilisantes
%A Scolan, Simon
%A Serra, Sylvain
%A Sochard, Sabine
%A Delmas, Pierre
%A Reneaume, Jean-Michel
%< avec comité de lecture
%@ 0038-092X
%J Solar Energy
%I Elsevier
%V 198
%P 643-657
%8 2020-03
%D 2020
%R 10.1016/j.solener.2020.01.076
%Z Computer Science [cs]/Modeling and Simulation
%Z Mathematics [math]/Optimization and Control [math.OC]
%Z Engineering Sciences [physics]Journal articles
%G English
%L hal-02480881
%U https://hal.archives-ouvertes.fr/hal-02480881
%~ INSMI
%~ UNIV-PAU
%~ TDS-MACS
%~ UNIV-LYON1
%~ LATEP
%~ UDL
%~ UNIV-LYON
%~ TESTUPPA

%0 Journal Article
%T Simultaneous Optimal Design and Control of an Extractive Distillation System for the Production of Fuel Grade Ethanol Using a Mathematical Program with Complementarity Constraints
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Ramos, Manuel
%A Gómez, Jorge
%A Reneaume, Jean-Michel
%< avec comité de lecture
%@ 0888-5885
%J Industrial and engineering chemistry research
%I American Chemical Society
%V 53
%N 2
%P 752-764
%8 2014-01-06
%D 2014
%R 10.1021/ie402232w
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%G English
%L hal-02162953
%U https://hal.archives-ouvertes.fr/hal-02162953
%~ UNIV-PAU
%~ LATEP
%~ TESTUPPA

%0 Journal Article
%T A MINLP Optimization of the Configuration and the Design of a District Heating Network: Academic Study Cases
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Mertz, T.
%A Serra, Sylvain
%A HENON, A.
%A Reneaume, Jean-Michel
%Z ACL
%< avec comité de lecture
%J Energy
%V 117
%P 450-464
%8 2016
%D 2016
%R 10.1016/j.energy.2016.07.106
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%X The aim of this work is to propose a tool for the design assistance of District Heating Network (DHN). Two goals of DHN optimization are handled simultaneously: the optimization of the configuration and its design. The optimization objective is to minimize the global cost of the DHN over 30 years. It includes both operating costs (heating and pumping cost, including thermal losses and pressure drop) and investment costs (line, trench, heating plant, heat exchanger). The formulation leads to a mixed integer non-linear programming (MINLP) problem in steady state. The model is solved with DICOPT within GAMS (around 5s for this study cases). One of the outputs of these academic study cases is the layout of the DHN, supplied in parallel or in cascade: a consumer with hot temperature requirement can supply another consumer with lower temperature requirement. Even a looped network in cascade is optimal (-4.6% total cost reduction) when the cost of the trench is lower than 500m. Furthermore, different structures are optimal (between -4 and -8% of total cost reduction) depending on whether the heat production(s) are decentralized, centralized, isolated collective, renewable or not. Finally the balance between heat loss and pressure drop is detailed. \textcopyright 2016 Elsevier Ltd
%G English
%L hal-02129508
%U https://hal.archives-ouvertes.fr/hal-02129508
%~ UNIV-PAU
%~ LATEP
%~ TESTUPPA

%0 Journal Article
%T Simulation de la distillation catalytique par un nouveau modèle de transfert - Application à la production d'acétate de méthyle
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Druart, Florence
%A Rouzineau, David
%A Reneaume, Jean-Michel
%A Meyer, Michel
%< avec comité de lecture
%Z LEPMI-00779
%@ 0008-4034
%J Canadian Journal of Chemical Engineering
%I Wiley
%V 82
%N 5
%P pp. 1014-1028
%8 2004
%D 2004
%K distillation catalytique
%K transfert multiconstituant
%K catalyseur poreux
%K modèle de transfert
%K étude de sensibilité
%Z Chemical Sciences/Material chemistryJournal articles
%X Nous présentons un modèle de transfert pour la distillation catalytique utilisant les équations de Maxwell-Stefan et incluant la diffusion effective dans le catalyseur poreux. L'écriture et la résolution de ce modèle ont été adaptées à son inclusion dans un simulateur commercial. Il est donc robuste et souple. Nous étudions l'influence des caractéristiques du catalyseur, de la configuration de la colonne, de la non-idéalité du mélange et de la qualité de l'échange liquide/vapeur. La conclusion principale de ces essais est l'existence d'un optimum de fonctionnement notamment pour le taux de reflux et l'emplacement du catalyseur. A catalytic distillation model, based on Maxwel-Stefan Equations, is presented. In this model, the effective diffusion in porous catalyst is taken into account. Since this model is developed in a commercial environment (ProSim Plus process simulator), particular attention is paid to the solution strategy: robustness and adaptability are of particular importance for the user. The influence of catalyst characteristic, column design, mixture non – ideality and liquid/vapour interface are discussed. We conclude that an optimum design exists. Reflux ratio and catalyst location are key-parameters.
%G French
%L hal-00420555
%U https://hal.archives-ouvertes.fr/hal-00420555
%~ UNIV-PAU
%~ LATEP
%~ TESTUPPA

%0 Journal Article
%T Multi-objective approach for a combined heat and power geothermal plant optimization
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Marty, Fabien
%A Sochard, Sabine
%A Serra, Sylvain
%A Reneaume, Jean-Michel
%< avec comité de lecture
%@ 1934-2659
%J Chemical Product and Process Modeling
%I De Gruyter
%8 2020-08-06
%D 2020
%R 10.1515/cppm-2020-0008
%Z Computer Science [cs]/Modeling and Simulation
%Z Mathematics [math]/Optimization and Control [math.OC]
%Z Engineering Sciences [physics]Journal articles
%G English
%L hal-02968725
%U https://hal.archives-ouvertes.fr/hal-02968725
%~ INSMI
%~ UNIV-PAU
%~ TDS-MACS
%~ LATEP

%0 Journal Article
%T A global MINLP approach for phase equilibrium calculations
%+ INPT ENSIACET
%+ Poudres et procédés - Ecole des Mines Albi-Carmaux
%A Reneaume, Jean-Michel
%A Meyer, M
%A Letourneau, Jean‐Jacques
%A Joulia, X
%Z European Symposium on Computer Aided Process Engineering ‐ 6 (ESCAPE‐6), RHODES, GREECE, MAY 26‐29, 1996
%< avec comité de lecture
%Z KTK-28
%Z DOCA-01
%@ 0098-1354
%J Computers & Chemical Engineering
%I Elsevier
%V 20
%N A
%P S303-S308
%8 1996
%D 1996
%R 10.1016/0098-1354(96)00061-0
%Z Engineering Sciences [physics]Journal articles
%X The number, nature and composition of phases at physical equilibrium are determined by minimizing Gibbs free energy. The existence of the phases is described by a set of binary variables which lead to the formulation of a MINLP problem. The resolution of the initial MINLP problem leads to the resolution of Non Linear Programming subproblems which contain local extrema. We propose to determine the global optimum of each NLP problem by the;se of a homotopy continuation method. This original resolution is illustrated on a L‐L‐V equilibrium problem.
%G English
%L hal-01668429
%U https://hal.archives-ouvertes.fr/hal-01668429
%~ MINES-ALBI
%~ RAPSODEE
%~ INSTITUTS-TELECOM
%~ INSTITUT-TELECOM

%0 Journal Article
%T Développement d’un outil d’aide à la conception optimale d’une solution de climatisation solaire réversible à haute efficacité énergétique
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Castillo-Garcia, L.
%A Sochard, Sabine
%A Reneaume, Jean-Michel
%A Nghiem, M.
%Z ACL
%< avec comité de lecture
%@ 0151-9093
%J L'Actualité Chimique
%I Société chimique de France
%V 390
%P 45--47
%8 2014
%D 2014
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%G French
%L hal-02129448
%U https://hal.archives-ouvertes.fr/hal-02129448
%~ UNIV-PAU
%~ LATEP
%~ TESTUPPA

%0 Journal Article
%T Modélisation Par Homogénéisation d'une Colonne d'absorption Réactive
%+ Centre Hospitalier Régional Universitaire [Montpellier] (CHRU Montpellier)
%+ Pathogénèse et contrôle des infections chroniques (PCCI)
%+ LABORATOIRE DE THERMIQUE ENERGETIQUE ET PROCEDES (EA1932) (LATEP)
%A Girard, Christian
%A Couture, Frédéric
%A Reneaume, Jean-Michel
%A Cézac, Pierre
%Z ACL
%< avec comité de lecture
%@ 0008-4034
%J Canadian Journal of Chemical Engineering
%I Wiley
%V 93
%N 2
%P 363-377
%8 2015
%D 2015
%R 10.1002/cjce.22130
%K scopusₗatep
%Z Engineering Sciences [physics]/Chemical and Process EngineeringJournal articles
%G English
%L hal-02129484
%U https://hal.archives-ouvertes.fr/hal-02129484
%~ UNIV-PAU
%~ BS
%~ UNIV-MONTPELLIER
%~ LATEP
%~ PCCI

