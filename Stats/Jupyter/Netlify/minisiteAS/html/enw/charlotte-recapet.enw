%0 Journal Article
%T The Sound of Arousal: The Addition of Novel Non-linearities Increases Responsiveness in Marmot Alarm Calls
%+ Institut de biologie de l'Ecole Normale Supérieure (IBENS)
%+ Department of Ecology and Evolutionary Biology 
%A Récapet, Charlotte
%A Blumstein, Daniel, S
%< avec comité de lecture
%@ 0179-1613
%J Ethology
%I Wiley
%V 115
%N 11
%P 1074 - 1081
%8 2009-11
%D 2009
%R 10.1111/j.1439-0310.2009.01691.x
%Z Life Sciences [q-bio]/Animal biology/Vertebrate Zoology
%Z Life Sciences [q-bio]/Biodiversity/Populations and Evolution [q-bio.PE]Journal articles
%X Vocal structure should reflect vocal function. While much attention has focused on quantifying attributes of harmonic vocalizations, the vocalizations of many species also may contain non-linear phenomena such as warbles, subharmonics, biphonation, and deterministic chaos or noise. The function of these non-linearities remains enigmatic. In some species, harmonic vocalizations abruptly become ‘noisy’ when individuals are physiologically aroused and the sudden onset of these non-linearities could signal arousal or fear to receivers. One untested functional hypothesis is that vocalizations containing non-linearities are more variable from one rendition to the next, and thus are harder to habituate to. In some situations, reducing the likelihood of habituation could be important. Signals that are highly evocative are more difficult to habituate to. Thus, we conducted playback experiments to foraging yellow-bellied marmots (Marmota flaviventris) to determine whether the addition of white noise (a non-linear acoustic phenomenon) to alarm calls elicited a greater response than control calls without the non-linearity or control calls with silence, rather than noise, added to them. Marmots spent less time foraging after hearing calls that included noise than after normal or control calls. This result is consistent with the unpredictability hypothesis and suggests that the adaptive value of non-linearities is to prevent habituation.
%G English
%L hal-01802657
%U https://hal.archives-ouvertes.fr/hal-01802657
%~ CNRS
%~ ENS-PARIS
%~ GIP-BE
%~ ENS-PSL
%~ PSL

%0 Journal Article
%T A Potential Role for Parasites in the Maintenance of Color Polymorphism in Urban Birds
%+ Laboratoire Ecologie et évolution
%+ Department of Biology [Montréal]
%+ Evolution, adaptation et comportement
%+ Centre d'Ecologie et des Sciences de la COnservation (CESCO)
%+ Laboratoire Éthologie Cognition Développement (LECD)
%+ Département Ecologie, Physiologie et Ethologie (DEPE-IPHC)
%A Jacquin, Lisa
%A Récapet, Charlotte
%A Prévot-Julliard, Anne-Caroline
%A Leboucher, Gérard
%A Lenouvel, Philippe
%A Erin, Noémie
%A Corbel, Hélène
%A Frantz, Adrien
%A Gasparini, Julien
%< avec comité de lecture
%@ 0029-8549
%J Oecologia
%I Springer Verlag
%V 173
%N 3
%P 1089-1099
%8 2013-11
%D 2013
%R 10.1007/s00442-013-2663-2
%M 23685880
%K Body Weights and Measures
%K Columbidae
%K Haemosporida
%K Urbanization
%K Bird Diseases
%K Animals
%K Paris
%K Linear Models
%K Melanins
%K Age Factors
%K Pigmentation
%K Protozoan Infections
%K Animal
%K Body Constitution
%Z Humanities and Social Sciences/Psychology
%Z Life Sciences [q-bio]Journal articles
%X Urbanization is a major challenge for biodiversity conservation, yet the evolutionary processes taking place in urbanized areas remain poorly known. Human activities in cities set new selective forces in motion which need to be investigated to predict the evolutionary responses of animal species living in urban areas. In this study, we investigated the role of urbanization and parasites in the maintenance of melanin-based color polymorphism in the feral pigeon Columba livia. Using a correlative approach, we tested whether differently colored genotypes displayed alternative phenotypic responses to urbanization, by comparing body condition, blood parasite prevalence and parasite load between colored morphs along an urbanization gradient. Body condition did not vary with urbanization, but paler individuals had a higher body condition than darker individuals. Moreover, paler morphs were less often parasitized than darker morphs in moderately urbanized habitats, but their parasite prevalence increased with urbanization. In contrast, darker morphs had similar parasite prevalence along the urbanization gradient. This suggests that paler morphs did better than darker morphs in moderately urbanized environments but were negatively affected by increasing urbanization, while darker morphs performed equally in all environments. Thus, differently colored individuals were distributed non-randomly across the urban habitat and suffered different parasite risk according to their location (a gene-by-environment interaction). This suggests that melanin-based coloration might reflect alternative strategies to cope with urbanization via different exposure or susceptibility to parasites. Spatial variability of parasite pressures linked with urbanization may, thus, play a central role in the maintenance of plumage color polymorphism in this urban species.
%G English
%L hal-01478471
%U https://hal.parisnanterre.fr//hal-01478471
%~ CNRS
%~ UNIV-PARIS10
%~ UNIV-STRASBG
%~ UPMC
%~ ENS-PARIS
%~ DEPE-IPHC
%~ DRS-IPHC
%~ IPHC
%~ INRIA
%~ BIOENVIS
%~ SHS
%~ LECD
%~ UPN
%~ UPMC_POLE_3
%~ DOCUMENTS-PUBLIES
%~ UNIV-LYON1
%~ INRIA-AUT
%~ LBBE
%~ CESCO
%~ SORBONNE-UNIVERSITE
%~ SU-INF-2018
%~ MNHN
%~ UNIV-STRASBG1
%~ SU-SCIENCES
%~ UNIV-PARIS-LUMIERES
%~ UDL
%~ UNIV-LYON
%~ SITE-ALSACE
%~ PSL
%~ ENS-PSL
%~ UNIV-PARIS-NANTERRE

