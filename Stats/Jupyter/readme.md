Les Dossiers numérotés servent à générer un minisite par liste.

Ces dossiers contiennent un notebook `.ipynb` chacun qui va remplir les dossiers et sous-dossiers du dossier `minisiteXYZ` que l'on copiera ensuite dans le dossier `netlify` qui est la racine du site web final.

Les scripts python des carnets jupyter requêtent l'api de HAL pour les idhal du fichier `Tous.txt` (un idhal par ligne). Réponses en xml (transformé par le fichier `xsl` pour donner le fichier html du chercheur `idhal.html`), bibtex, csv et enw (endnote).  

- AS : articles scientifiques
- CA : COMM (avec et sans actes)
- NP : Non-publiés (REPORT & UNDEFINED)
- AST : autres sans texte
- DA : Documents accessibles