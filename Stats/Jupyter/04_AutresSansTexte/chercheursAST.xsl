<?xml version="1.0"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" doctype-system="about:legacy-compat" encoding="UTF-8" indent="yes" />
    <xsl:template match="/">
        <html lang="fr">

        <head>
            <meta charset="utf-8" />
            <title>HALathon UPPA - Autres publications sans accès libre au texte intégral</title>
            <link rel="icon" type="image/png" href="UPPA_logo-rose.png" />
            <link rel="stylesheet" href="styleChercheur.css" />
        </head>

        <body>
            
            <img id="logo-hal" src="hal-300x207.png" alt="logo-hal" height="80px" />
            <h1><a href="https://halathon2021-hal-e2s-uppa.netlify.app">HALathon 2020-2021 UPPA</a></h1>
            <h2 id="sous-titre">Autres publications que vous pouvez déposer</h2>
            <script>
                var path1 = window.location.pathname;
                var file1 = path1.substring(path1.lastIndexOf('/')+1);
                var idhal1 = file1.split('.').slice(0, -1).join('.');
                var s = "<span> (idHAL : <code>"+ idhal1 +"</code> - liste au <code>10 mai 2021</code>)</span>";
                var k = document.getElementById("sous-titre");
                k.insertAdjacentHTML("beforeend", s);
            </script>
            <xsl:choose>
                <xsl:when test="response/result[@numFound='0']">
                    <hr />
                    <p id="noresult" style="font-size:2em;color:#bdcc19;"><i>Aucune autre publication à déposer trouvée pour cet auteur</i>.</p>
                    
                    <script>
                        var path = window.location.pathname;
                        var file = path.substring(path.lastIndexOf('/')+1);
                        var idhal = file.split('.').slice(0, -1).join('.');
                        var e = '<p style="margin-top:3em;">Autres listes</p><ul><li><a href="https://halathon2021-hal-e2s-uppa.netlify.app/minisiteas/html/' +idhal +'.html">Vos articles à déposer</a></li><li><a href="https://halathon2021-hal-e2s-uppa.netlify.app/minisiteca/html/' +idhal +'.html">Vos comm à déposer</a></li><li><a href="https://halathon2021-hal-e2s-uppa.netlify.app/minisitenp/html/' +idhal +'.html">Vos doc non-publiés à déposer</a></li><li><a href="https://halathon2021-hal-e2s-uppa.netlify.app/minisiteda/html/' +idhal +'.html">Vos pdf déjà dans HAL</a></li></ul>';
                        var h = document.getElementById("noresult");
                        h.insertAdjacentHTML("afterend", e);
                    </script>
                    
                </xsl:when>
                <xsl:otherwise>
                    <script>
                        var path = window.location.pathname;
                        var file = path.substring(path.lastIndexOf('/')+1);
                        var idhal = file.split('.').slice(0, -1).join('.');
                        var e = '<div id="telfichiers"><p>Récuperez ces références</p><ul><li>Bouton Zotero</li><li><a href="bibtex/' + idhal + '.bib" download="COMM_' + idhal + '.bib">Fichier BibTex</a></li><li><a href="csv/' + idhal + '.csv" download="COMM_' + idhal + '.csv">Tableau csv</a></li><li><a href="enw/' + idhal + '.enw" download="COMM_' + idhal + '.enw">EndNote</a></li></ul><p style="margin-top:3em;">Autres listes</p><ul><li><a href="https://halathon2021-hal-e2s-uppa.netlify.app/minisiteas/html/' +idhal +'.html">Vos articles à déposer</a></li><li><a href="https://halathon2021-hal-e2s-uppa.netlify.app/minisiteca/html/' +idhal +'.html">Vos comm à déposer</a></li><li><a href="https://halathon2021-hal-e2s-uppa.netlify.app/minisitenp/html/' +idhal +'.html">Vos doc non-publiés à déposer</a></li><li><a href="https://halathon2021-hal-e2s-uppa.netlify.app/minisiteda/html/' +idhal +'.html">Vos pdf déjà dans HAL</a></li></ul></div>';
                        var h = document.getElementById("sous-titre");
                        h.insertAdjacentHTML("afterend", e);
                    </script>
                    <table class="typdoc" border="1">
                        <tr>
                          <td style="color: #bdcc19;font-weight: 600;letter-spacing: 3px;">Types HAL</td>
                          <td>OUV</td>
                          <td>Ouvrage (y compris édition critique et traduction)</td>
                        </tr>
                        <tr>
                          <td></td>
                          <td>COUV</td>
                          <td>Chapitre d'ouvrage</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>DOUV</td>
                            <td>Direction d'ouvrage, Proceedings, Dossier</td>
                          </tr>
                          <tr>
                            <td></td>
                            <td>ART</td>
                            <td>Article [avec <em>commité de lecture = non</em>]</td>
                          </tr>
                          <tr>
                            <td></td>
                            <td>OTHER</td>
                            <td>Autre publication</td>
                          </tr>
                          <tr>
                            <td></td>
                            <td>LECTURE</td>
                            <td>Cours</td>
                          </tr>
                      </table>
                    <table border="1">
                        <tr>
                            <th>#</th>
                            <th>Type</th>
                            <th>Année</th>
                            <th>Titre</th>
                            <th>doi</th>
                            <th>Notice HAL</th>
                        </tr>
                        <xsl:for-each select="/response/result/doc">
                            <xsl:sort select="str[@name='docType_s']" order="ascending" />
                            <xsl:sort select="int[@name='publicationDateY_i']" order="descending" />
                            <tr>
                                <td style="font-family:monospace;font-size:0.9em;">
                                    <xsl:value-of select="position()" />
                                </td>
                                <td>
                                    <xsl:value-of select="str[@name='docType_s']" />
                                
                                </td>
                                <td>
                                    <xsl:value-of select="int[@name='publicationDateY_i']" />
                                </td>
                                <td>
                                    <xsl:value-of select="arr[@name='title_s']/str" />
                                </td>
                                <td>
                                    <xsl:variable name="doi" select="str[@name='doiId_s']" />
                                    <a href='https://dx.doi.org/{$doi}' target='_blank'>
                                    <xsl:value-of select="str[@name='doiId_s']" />
                                    </a>
                                </td>
                                <td>
                                    <xsl:variable name="docid" select="str[@name='halId_s']" />
                                    <a href='https://hal-univ-pau.archives-ouvertes.fr/{$docid}' target='_blank'>
                                        <xsl:value-of select="str[@name='halId_s']" />
                                    </a>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </table>
                    <h3>Références complètes</h3>
                    <ol id="ref">
                        <xsl:for-each select="/response/result/doc">
                            <xsl:sort select="int[@name='publicationDateY_i']" order="descending" />
                            <li>
                                <div class="csl-bib-body">
                                    <div class="csl-entry">
                                        <xsl:value-of select="str[@name='label_s']" />
                                    </div>
                                    <xsl:copy-of select="str[@name='label_coins']/span" />
                                </div>
                            </li>
                        </xsl:for-each>
                    </ol>
                </xsl:otherwise>
            </xsl:choose>
            
            <div id="footer">
                <hr />
                <table style="border:none;">
                    <td><a href="https://hal-univ-pau.archives-ouvertes.fr" target="_blank"><img src="boutonSCD_HAL.jpg" height="80px" /></a></td>
                    <td><i>Des remarques, des corrections ou des questions ? Contactez-nous </i> <a href='mailto:hal@univ-pau.fr'>::: hal@univ-pau.fr :::</a> (<i>Gaëlle, Marina, Julien</i>)<br/><a href="usage.html">::: HALathon, mode d'emploi :::</a></td>
                </table>
                <hr />
            </div>
            
            <script>
                var str = document.getElementById("ref");
                str.innerHTML = str.innerHTML.replace(/&#x27E8;/gi, '⟨');
                str.innerHTML = str.innerHTML.replace(/&#x27E9;/gi, '⟩');
            </script>

                        
        </body>

        </html>
    </xsl:template>
</xsl:stylesheet>
