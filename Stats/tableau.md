---
title: état des lieux des collections HAL e2s UPPA
author: Julien Rabaud
...


## Tableau type

| Type de doc                                       | File | Notice | Annexe | Total |
|---------------------------------------------------|-----:|-------:|-------:|------:|
| "Article dans une revue" "avec comité de lecture" |      |        |        |       |
| "Communication dans un congrès" "avec actes"      |      |        |        |       |
| "Communication dans un congrès" "sans actes"      |      |        |        |       |
| "Poster"                                          |      |        |        |       |
| "Chapitre d'ouvrage"                              |      |        |        |       |
| "Ouvrage"                                         |      |        |        |       |
| "Autres"                                          |      |        |        |       |
| - dont "Billet de blog"                           |      |        |        |       |
| - dont "Notices encyclo"                          |      |        |        |       |
| - dont "Traduction"                               |      |        |        |       |
| - dont "Compte-rendu"                             |      |        |        |       |
| "Rapport"                                         |      |        |        |       |
| "Pré-publication"                                 |      |        |        |       |
| "Thèses"                                          |      |        |        |       |
| "HDR"                                             |      |        |        |       |


## Requêtes

### simple
`https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=docType_s,submitType_s`

### ajout pivot Date de publication (année)
`https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=publicationDateY_i,docType_s,submitType_s`

### ajout pivot Date de dépôt (année)
`https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=submittedDateY_i,docType_s,submitType_s`


## UNIV-PAU
### simple
<https://api.archives-ouvertes.fr/search/UNIV-PAU/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=docType_s,submitType_s>


### Date Publication
<https://api.archives-ouvertes.fr/search/UNIV-PAU/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=publicationDateY_i,docType_s,submitType_s>


### Date Dépôt
<https://api.archives-ouvertes.fr/search/UNIV-PAU/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=submittedDateY_i,docType_s,submitType_s>



## ALTER
### simple
<https://api.archives-ouvertes.fr/search/ALTER/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=docType_s,submitType_s>


### Date Publication
<https://api.archives-ouvertes.fr/search/ALTER/?q=*:*&rows=0&wt=xml&indent=true&facet=true&facet.pivot=publicationDateY_i,docType_s,submitType_s>


### Date Dépôt
<https://api.archives-ouvertes.fr/search/ALTER/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=submittedDateY_i,docType_s,submitType_s>



## CATT
### simple
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=docType_s,submitType_s>


### Date Publication
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=publicationDateY_i,docType_s,submitType_s>


### Date Dépôt
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=submittedDateY_i,docType_s,submitType_s>



## CRAJ
### simple
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=docType_s,submitType_s>


### Date Publication
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=publicationDateY_i,docType_s,submitType_s>


### Date Dépôt
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=submittedDateY_i,docType_s,submitType_s>



## CREG (CREG-EA4580)
### simple
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=docType_s,submitType_s>


### Date Publication
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=publicationDateY_i,docType_s,submitType_s>


### Date Dépôt
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=submittedDateY_i,docType_s,submitType_s>



## DMEX
### simple
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=docType_s,submitType_s>


### Date Publication
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=publicationDateY_i,docType_s,submitType_s>


### Date Dépôt
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=submittedDateY_i,docType_s,submitType_s>



## ECOBIOP
### simple
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=docType_s,submitType_s>


### Date Publication
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=publicationDateY_i,docType_s,submitType_s>


### Date Dépôt
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=submittedDateY_i,docType_s,submitType_s>



## EXPERICE
### simple
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=docType_s,submitType_s>


### Date Publication
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=publicationDateY_i,docType_s,submitType_s>


### Date Dépôt
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=submittedDateY_i,docType_s,submitType_s>



## IE2IA
### simple
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=docType_s,submitType_s>


### Date Publication
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=publicationDateY_i,docType_s,submitType_s>


### Date Dépôt
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=submittedDateY_i,docType_s,submitType_s>



## IKER
### simple
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=docType_s,submitType_s>


### Date Publication
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=publicationDateY_i,docType_s,submitType_s>


### Date Dépôt
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=submittedDateY_i,docType_s,submitType_s>



## IPREM
### simple
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=docType_s,submitType_s>


### Date Publication
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=publicationDateY_i,docType_s,submitType_s>


### Date Dépôt
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=submittedDateY_i,docType_s,submitType_s>



## ITEM
### simple
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=docType_s,submitType_s>


### Date Publication
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=publicationDateY_i,docType_s,submitType_s>


### Date Dépôt
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=submittedDateY_i,docType_s,submitType_s>



## LATEP
### simple
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=docType_s,submitType_s>


### Date Publication
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=publicationDateY_i,docType_s,submitType_s>


### Date Dépôt
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=submittedDateY_i,docType_s,submitType_s>



## LFCR
### simple
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=docType_s,submitType_s>


### Date Publication
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=publicationDateY_i,docType_s,submitType_s>


### Date Dépôt
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=submittedDateY_i,docType_s,submitType_s>



## LIUPPA
### simple
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=docType_s,submitType_s>


### Date Publication
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=publicationDateY_i,docType_s,submitType_s>


### Date Dépôt
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=submittedDateY_i,docType_s,submitType_s>



## LMAP (LMA-PAU)
### simple
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=docType_s,submitType_s>


### Date Publication
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=publicationDateY_i,docType_s,submitType_s>


### Date Dépôt
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=submittedDateY_i,docType_s,submitType_s>



## MEPS
### simple
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=docType_s,submitType_s>


### Date Publication
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=publicationDateY_i,docType_s,submitType_s>


### Date Dépôt
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=submittedDateY_i,docType_s,submitType_s>



## NuMéA (NUMEA)
### simple
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=docType_s,submitType_s>


### Date Publication
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=publicationDateY_i,docType_s,submitType_s>


### Date Dépôt
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=submittedDateY_i,docType_s,submitType_s>



## Passages (UMR5319)
### simple
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=docType_s,submitType_s>


### Date Publication
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=publicationDateY_i,docType_s,submitType_s>


### Date Dépôt
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=submittedDateY_i,docType_s,submitType_s>



## PDP
### simple
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=docType_s,submitType_s>


### Date Publication
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=publicationDateY_i,docType_s,submitType_s>


### Date Dépôt
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=submittedDateY_i,docType_s,submitType_s>



## SIAME
### simple
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=docType_s,submitType_s>


### Date Publication
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=publicationDateY_i,docType_s,submitType_s>


### Date Dépôt
<https://api.archives-ouvertes.fr/search/TAMP_ID/?q=*:*&rows=0&wt=json&indent=true&facet=true&facet.pivot=submittedDateY_i,docType_s,submitType_s>



