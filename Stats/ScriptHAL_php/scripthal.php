<?php
header("access-control-allow-origin: *");
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);
      function ret_xml_url($url){
    $parse_url=parse_url($url);
	$host = $parse_url['host'];
	$header = array(
		'Accept: image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/x-shockwave-flash, */*',
		'Accept-Language: fr',
		'Accept-Encoding: gzip, deflate',
		'User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)',
		'Host: '.$host,
		'Connection: Keep-Alive'
		);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_ENCODING, "");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
 $value=curl_exec($ch);

	return $value;
        }

//$nlim=$_GET['nlim'];
if ($_GET['nlim']) {$nlim=$_GET['nlim'];}else{$nlim=1000;}
$labo=$_GET['labo'];

$texlab['29209']='LIUPPA';
$texlab['33789']='LATEP';
$texlab['87850']='LMAP';
$texlab['83656']='LFCR';
$texlab['483835']='IPREM';
$texlab['232227']='SIAME';
$texlab['490359']='NUMEA';
$texlab['37936']='ECOBIOP';
$texlab['554131']='DMEX';
$texlab['2561']='IKER';
$texlab['73758']='CREG-EA4580';
$texlab['533317']='ALTER';
$texlab['213755']='ITEM';
$texlab['456102']='UMR5319';
$texlab['43806']='CATT';
$texlab['204841']='EXPERICE';
$texlab['162386']='PDP';
$texlab['407234']='CRAJ';
$texlab['534331']='MEPS';
$texlab['239063']='IE2IA';

?>
<html>
<head>
<!--<script src="jquery-1.11.2.min.js"></script>-->
<link rel="stylesheet" type="text/css" href="jquery.dataTables.min.css"/>
<!--<script type="text/javascript" src="jquery.dataTables.min.js"></script>-->
<link rel="stylesheet" type="text/css" href="colReorder.dataTables.min.css"/>
<script type="text/javascript" src="dataTables.colReorder.min.js"></script>
<link rel="stylesheet" type="text/css" href="buttons.dataTables.min.css"/>
<!--<script type="text/javascript" src="dataTables.buttons.min.js"></script>
<script type="text/javascript" src="buttons.print.min.js"></script>
<script type="text/javascript" src="buttons.html5.min.js"></script>
<script type="text/javascript" src="buttons.flash.min.js"></script>-->
<script type="text/javascript" src="buttons.colVis.min.js"></script>
<!--<script type="text/javascript" src="moment.min.js"></script>
<script type="text/javascript" src="datetime-moment.js"></script>
<script type="text/javascript" src="date_uk.js"></script>-->
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<title>Script HAL <?php echo $texlab[$labo]?></title>
<script>
jQuery.noConflict();
jQuery(document).ready(function() {
jQuery('#latable').DataTable({
colReorder: true,
paging: false,
 dom: 'Bfrtip',
        buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Tous"]],
"language": {
"lengthMenu": "Afficher _MENU_ résultats par page",
"zeroRecords": "Pas de résultats",
"info": "Affichage des résultats _START_ à _END_ sur _TOTAL_",
"infoEmpty": "Pas de résultats",
"search": "Chercher texte",
"infoFiltered": "(liste filtrée sur un total de _MAX_ résultats)",
"paginate": {
"first":      "Début",
"last":       "Fin",
"next":       "Suivant",
"previous":   "Précédent"
},
"emptyTable":     "Pas de résultats avec ces critères de recherche"
}

} );
var table = jQuery('#latable').DataTable();
	jQuery('a.toggle-vis').on( 'click', function (e) {
e.preventDefault();

// Get the column API object
var column = table.column( jQuery(this).attr('data-column') );

// Toggle the visibility
column.visible( ! column.visible() );
} );
} );
</script>
<style>
th{ font-weight:bold;}
caption {font-size:150%; font-weight:bold; border-top: 1px solid black; border-bottom: 1px solid black; padding:2; border-spacing:2;}
</style>
</head>
  <body>
    <title>Script HAL</title>
 <button onclick="javascript:history.back()">Retour</button><br/><br/>
 <table class="display" width="100%" id="latable">
 <CAPTION id='caption'>Script HAL pour le labo <?php echo $texlab[$labo]?> ( Max. <?php echo $nlim?> résultats)</CAPTION>
<thead><tr><td>ID</td><td>Nom</td><td>Nb publications</td></tr></thead>
<tfoot><tr><td>ID</td><td>Nom</td><td>Nb publications</td></tr></tfoot>

<tbody>
<?php
$find = new DOMDocument();
$urly="https://api.archives-ouvertes.fr/search/".$texlab[$labo]."/?q=*:*&rows=0&wt=xml&facet=true&facet.query=structHasAuthIdHal_fs&facet.field=structHasAuthIdHal_fs&facet.prefix=".$labo."_FacetSep_&facet.mincount=1&facet.limit=".$nlim;
$find->loadXML(ret_xml_url($urly));
//$find->loadXML(ret_xml_url("https://api.archives-ouvertes.fr/search/LIUPPA/?q=*:*&rows=0&wt=xml&facet=true&facet.query=structHasAuthIdHal_fs&facet.field=structHasAuthIdHal_fs&facet.prefix=29209_FacetSep_&facet.mincount=1&facet.limit=10"));
$ints=$find->getElementsByTagName('int');
$i=0;
foreach($ints as $int){
$name=$int->getAttribute('name');
$npub=$int->firstChild->nodeValue;

if ($name!='structHasAuthIdHal_fs') {
$i++;
//echo $name.'<br/>';
$name2=substr($name,strpos($name,'JoinSep')+8,strlen($name)-(strpos($name,'JoinSep')+8));
//if (substr($name2,0,1)=="_"){$name2=str_replace("_FacetSep_","0;",$name2);}else {$name2=str_replace("_FacetSep_",";",$name2);}
//echo $name2.''.$npub.'<br/>';
if (substr($name2,0,1)=="_"){$name2=str_replace("_FacetSep_","<td>0</td><td>",$name2);}else {$name2="<td>".str_replace("_FacetSep_","</td><td>",$name2);}
echo '<tr>'.$name2.'</td><td>'.$npub.'</td></tr>';
}
}
//echo $i;
?>
</tbody></table>
</body>
</html>
