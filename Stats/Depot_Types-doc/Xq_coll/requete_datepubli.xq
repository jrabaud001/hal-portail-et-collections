for $i in /response/lst/lst/arr/lst
  let $year := $i/int[@name="value"]/text()
  let $nombre := $i/int[@name="count"]/text()
 
  let $typeND := for $a in $i/arr/lst
    let $type := $a/str[@name="value"]/text()
    let $ntype := $a/int[@name="count"]/text()
    let $depotN :=  for $b in $a//arr/lst
    order by $b/str[@name="value"]/text() descending
    return (<td class="typedepot">{$b/str[@name="value"]/text()}</td>,<td class="nbdepot">{$b/int[@name="count"]/text()}</td>)
    order by $type
    return <tr><td/><td/><td class="typedoc">{$type}</td><td class="nbdoc">{$ntype}</td>{$depotN}</tr>

order by -$year

return (<tr><td class="year">{$year}</td><td class="nbyear">{$nombre}</td></tr>,$typeND)