<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html lang="fr">

        <head>
            <meta charset="utf-8" />
            <title>HTML</title>
        </head>

        <body>
            <h2>Résultat d'une requête api.archives-ouvertes.fr</h2>
            <code>facet.pivot=docType_s,submitType_s</code>
            <p>---</p>
            <table border="1">
                <tr>
                    <th>Type de Doc</th>
                    <th>Nombre</th>
                    <th>Notice</th>
                    <th>File</th>
                    <th>Annexe</th>
                </tr>
                <xsl:for-each select="/response/lst/lst/arr/lst">
                    <tr>
                        <td>
                            <xsl:value-of select="str[@name='value']" />
                        </td>
                        <td>
                            <xsl:value-of select="int[@name='count']" />
                        </td>
                        <td>
                            <xsl:if test="arr/lst/str[contains(text(),'notice')]">
                                <xsl:value-of select="arr/lst/str[contains(text(),'notice')]/following-sibling::int[position()=1]" />
                            </xsl:if>
                        </td>
                        <td>
                            <xsl:if test="arr/lst/str[contains(text(),'file')]">
                                <xsl:value-of select="arr/lst/str[contains(text(),'file')]/following-sibling::int[position()=1]" />
                            </xsl:if>
                        </td>
                        <td>
                            <xsl:if test="arr/lst/str[contains(text(),'annex')]">
                                <xsl:value-of select="arr/lst/str[contains(text(),'annex')]/following-sibling::int[position()=1]" />
                            </xsl:if>
                        </td>
                    </tr>
                </xsl:for-each>
            </table>
        </body>

        </html>
    </xsl:template>
</xsl:stylesheet>
